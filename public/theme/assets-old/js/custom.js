jQuery(function($) {
	 // ===== fixed menu on scroll ==== 
	 
      $(window).scroll(function(){
    if ($(window).scrollTop() >= 100) {
       $('.main-nav').addClass('top_fix');
    }
    else {
       $('.main-nav').removeClass('top_fix');
    }
   });
//Initiat WOW JS
	new WOW().init();
	//smoothScroll
	smoothScroll.init();
  });

//**BACK-TO-TOP-JS
// fade in #back-top


$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#return-to-top').fadeIn();
        } else {
            $('#return-to-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('#return-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 1500);
        return false;
    });
});

$(function(){
	var slider = $('#slider');
	var sliderWrap = $('#slider ul');
	var sliderImg = $('#slider ul li');
	var prevBtm = $('#sliderPrev');
	var nextBtm = $('#sliderNext');
	var length = sliderImg.length;
	var width = sliderImg.width();

	sliderWrap.width(width*length);
	$('#slider ul li:last-child').prependTo('#slider ul');
	sliderWrap.css('margin-left', - width);

	nextBtm.on('click', function(){
		sliderWrap.animate({
    		'margin-left': '-=' + width
  			}, 500, function() {
  				$('#slider ul li:first-child').appendTo('#slider ul');
  				sliderWrap.css('margin-left', - width);
  		});
		return false;
	});

	prevBtm.on('click', function(){
		sliderWrap.animate({
    		'margin-left': '+=' + width
  			}, 500, function() {
  				$('#slider ul li:last-child').prependTo('#slider ul');
  				sliderWrap.css('margin-left', - width);
  		});
		return false;
	});

});
  //Events that reset and restart the timer animation when the slides change
    $("#transition-timer-carousel").on("slide.bs.carousel", function(event) {
        //The animate class gets removed so that it jumps straight back to 0%
        $(".transition-timer-carousel-progress-bar", this)
            .removeClass("animate").css("width", "0%");
    }).on("slid.bs.carousel", function(event) {
        //The slide transition finished, so re-add the animate class so that
        //the timer bar takes time to fill up
        $(".transition-timer-carousel-progress-bar", this)
            .addClass("animate").css("width", "100%");
    });
    
    //Kick off the initial slide animation when the document is ready
    $(".transition-timer-carousel-progress-bar", "#transition-timer-carousel")
        .css("width", "100%");
        
        
        $(document).ready(function(){
    $(".tgl").click(function(){
        $(".bootsnipp-search").toggle();
    });
    
 
});
   if ( $( ".right_caption" ) ) {
    $('ol').addClass('announce');
    $('p').addClass('announce');
}
  