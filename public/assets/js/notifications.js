(function($) {
    $(function() {
        //feesdepartment
        function getNotificationsMoney() {
            //load notifications after very 2 sec
            $.ajax({
                url: /get-notifications-money/,
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotificationsMoney();
                    }, 3000);
                },
                success: function(data) {
                    //alert(data.data);
                  
                //fees
                
                   $('#appro_money_accounts').html("You have received "+data.appro_money_acc+" notifications on approval<br> money<br> from accounts");
                   $('#dec_money_acc').html("You have received "+data.dec_money_acc+" notifications on decline<br> money<br> from accounts");

                }
            });
        }
        getNotificationsMoney();
        //accounts
        function getNotificationsVehicle() {
            //load notifications after very 2 sec
            $.ajax({
                url: /get-notifications-vehicle/,
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotificationsVehicle();
                    }, 3000);
                },
                
                success: function(data) {
                    
                    //alert(data.data);
                  
                //accounts
                
                   $('#approval').html("You have received "+data.approved_vehicle+" notifications on approving<br> vehicles");
                   $('#decline').html("You have received "+data.decline_vehicle+" notifications on decline<br> vehicles");
                   $('#appro_money').html("You have received "+data.appro_money+" notifications on approval<br> hand over money<br> from fees department");

                }
                
            });
        }
        getNotificationsVehicle();
        //principal
        function getNotifications() {
            //load notifications after very 2 sec
            $.ajax({
                url: /get-notifications/,
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotifications();
                    }, 3000);
                },
                success: function(data) {
                    //alert(data.data);
                  //bus route add delete update 
                        $('#media1').html("You have received "+data.busroute+" notifications on adding<br> bus routes");
                        $('#edit').html("You have received "+data.edit+" notifications on editing routes");
                        $('#feest').html("You have received "+data.feest+" notifications on<br> fee status of student");
                        $('#delete').html("You have received "+data.delete+" notifications on<br> deleting route");
                  //dress charge add,up,del
                 $('#dress_charge').html("You have received "+data.dress_charge+" notifications on adding<br> dress charges");
                 $('#dress_edit').html("You have received "+data.dress_edit+" notifications on editing<br> dress charges");
                 $('#dress_delete').html("You have received "+data.dress_delete+" notifications on delteing<br> dress charges");

                  //book charge add,up,del
                  
                 $('#book_add').html("You have received "+data.book_add+" notifications on adding<br> book charges");
                 $('#book_edit').html("You have received "+data.book_edit+" notifications on editing<br> book charges");
                 $('#book_del').html("You have received "+data.book_delete+" notifications on delteing<br> book charges");

                //accounts
                   $('#approved').html("You have received "+data.approved_vehicle+" notifications on approving<br> vehicles");
//new admission
                   //$('#admission').html("You have received "+data.newadmission+" notifications on new<br> admission");


                }
            });
        }

        getNotifications();
      //for principal
        function getNotificationsNumber() {
            $.ajax({
                url: '/get-notifications-number/',
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotificationsNumber();
                    }, 3000);
                },
                success: function(data) {
                    $('#sm_nofifications_number').html(data.number);
                },
            });
        }
        getNotificationsNumber();
        
        function getNotificationsNewAdmission() {
            $.ajax({
                url: '/get-notifications-newadmission/',
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotificationsNewAdmission();
                    }, 3000);
                },
                success: function(data) {
                    $('#admission').html("You have received "+data.newadmission+" notifications on new<br> admission");
                },
            });
        }
        getNotificationsNewAdmission();
        
        //for accounts
        function getNotificationsNumberAccounts() {
            $.ajax({
                url: '/get-notifications-number-accounts/',
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotificationsNumberAccounts();
                    }, 3000);
                },
                success: function(data) {
                    $('#sm_nofifications_number_accounts').html(data.number);
                },
            });
        }
        getNotificationsNumberAccounts();
        //for feesdepart
        function getNotificationsNumberFees() {
            $.ajax({
                url: '/get-notifications-number-fees/',
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotificationsNumberFees();
                    }, 3000);
                },
                success: function(data) {
                    $('#sm_nofifications_number_acc').html(data.number);
                },
            });
        }
        getNotificationsNumberFees();
        //salary feesdepart
        function getNotificationsSalary() {
            //load notifications after very 2 sec
            $.ajax({
                url: /get-notifications-salary/,
                dataType: 'json',
                complete: function() {
                    setTimeout(function() {
                        getNotificationsSalary();
                    }, 3000);
                },
                success: function(data) {
                    //alert(data.data);
                  $("#salary_").html("You have received "+data.salary+" notifications on<br> receiving salary");

                }
            });
        }

        getNotificationsSalary();
    });
})(jQuery);
