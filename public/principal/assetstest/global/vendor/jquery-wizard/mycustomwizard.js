(function($) {

    var form = $("#signup-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
             element.before(error); 
        },
        rules: {
            first_name : {
                required: true,
            },
            last_name : {
                required: true,
            },
            class : {
                required: true,
            },
            /*subjectdata : {
                required: true,
            },*/
            last_school : {
                required: true,
            },
            leavecertificate : {
                required: true,
            },
            lastclass : {
                required: true,
            },
            aadharfront : {
                required: true,
            },
            aadharback : {
                required: true,
            },
            father_name : {
                required: true,
            },
            mother_name : {
                required: true,
            },
            dob : {
                required: true,
            },
            father_contact : {
                required: true,
            },
            mother_contact : {
                required: true,
            },
            country : {
                required: true,
            },
            pin : {
                required: true,
            },
            city : {
                required: true,
            },
            address : {
                required: true,
            },
            /*card_name : {
                required: true,
            },
            card_number : {
                required: true,
            },
            card_approval_code : {
                required: true,
            },*/
            
            email : {
                required: true,
                email: true
            },
            username : {
                required: true,
            },
        },
        messages: {
            first_name : {
                required : "Please enter your first name"
            },
            last_name : {
                required : "Please enter your last name"
            },
            class : {
                required : "Please select"
            },
            /*subjectdata : {
                required : "Please select"
            },*/
             last_school : {
                required : "Please select last school"
            },
            leavecertificate : {
                required : "Please select leave certificate"
            },
            lastclass : {
                required : "Please select last class"
            },
            aadharfront : {
                required : "Please select adhar front"
            },
            aadharback : {
                required : "Please select adhar back"
            },
             father_name : {
                required : "Please select father_name"
            }, 
            mother_name : {
                required : "Please select mother_name"
            },
             dob : {
                required : "Please select dob"
            },
             father_contact : {
                required : "Please select father_contact_name",
                stringLength: {
              min: 6,
            }
            
            },
             mother_contact : {
                required : "Please select mother_contact",
                minlength: 10
            },
             country : {
                required : "Please select country"
            },
             city : {
                required : "Please select city"
            },
             pin : {
                required : "Please select pin"
            },
             address : {
                required : "Please select address"
            },
             card_name : {
                required : "Please select card_name"
            },
             card_number : {
                required : "Please select card_number"
            },
             card_approval_code : {
                required : "Please select card_approval_code"
            },
            username : {
                required : "Please enter your username"
            },
            email : {
                required : "Please enter your first name",
                email: "Please enter a valid email address!"
            }
        },
        onfocusout: function(element) {
            $(element).valid();
        },
        highlight : function(element, errorClass, validClass) {
            $(element).parent().parent().find('.form-group').addClass('form-error');
            $(element).removeClass('valid');
            $(element).addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parent().parent().find('.form-group').removeClass('form-error');
            $(element).removeClass('error');
            $(element).addClass('valid');
        }
    });
    form.steps({
        headerTag: "h5",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        labels: {
            previous : 'Previous',
            next : 'Next',
            finish : 'Finish',
            current : ''
        },
        titleTemplate : '<h5 class="title">#title#</h5>',
        onInit : function (event, currentIndex) { 
            // Suppress (skip) "Warning" step if the user is old enough.
            if(currentIndex === 0) {
                form.find('.actions').addClass('test');
            }
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            //alert('Sumited');
            //$(".btn-paid").show();
            //$('#studendetailconfirmationmodal').modal('show');
            //$('#studendetailconfirmationmodal').show();
            $('.signup-form').submit();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {

         
        }
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "",
        remote: "",
        email: "",
        url: "",
        date: "",
        dateISO: "",
        number: "",
        digits: "",
        creditcard: "",
        equalTo: ""
    });

    // $('#country').parent().append('<ul id="newcountry" class="select-list" name="country"></ul>');
    // $('#country option').each(function(){
    //     $('#newcountry').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
    // });
    // $('#country').remove();
    // $('#newcountry').attr('id', 'country');
    // $('#country li').first().addClass('init');
    // $("#country").on("click", ".init", function() {
    //     $(this).closest("#country").children('li:not(.init)').toggle();
    // });
    
    // var allOptions = $("#country").children('li:not(.init)');
    // $("#country").on("click", "li:not(.init)", function() {
    //     allOptions.removeClass('selected');
    //     $(this).addClass('selected');
    //     $("#country").children('.init').html($(this).html());
    //     allOptions.toggle();
    // });

    // var inputs = document.querySelectorAll( '.inputfile' );
	// Array.prototype.forEach.call( inputs, function( input )
	// {
	// 	var label	 = input.nextElementSibling,
	// 		labelVal = label.innerHTML;

	// 	input.addEventListener( 'change', function( e )
	// 	{
	// 		var fileName = '';
	// 		if( this.files && this.files.length > 1 )
	// 			fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
	// 		else
	// 			fileName = e.target.value.split( '\\' ).pop();

	// 		if( fileName )
	// 			label.querySelector( 'span' ).innerHTML = fileName;
	// 		else
	// 			label.innerHTML = labelVal;
	// 	});

	// 	// Firefox bug fix
	// 	input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
	// 	input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    // });
    
    
})(jQuery);
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.your_picture_image')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}