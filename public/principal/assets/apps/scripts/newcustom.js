$(document).ready(function() {
    $('.dvPassport1').hide();
    $('.dvPassport ').hide();
    $('.transcharge ').hide();
    $('.ocharge').hide();
    $('.discountamounts').hide();
    $(document).on('click','#gt_show_hide_sections', function(e) {
        e.preventDefault();
        $(this).closest('h4').find('.section').toggle();
    });
    //alert('123');
    //staff form checkboxes
    $(".cehck").click(function() {
        $(".cehck").not(this).prop("disabled", this.checked);
        if($("#inputCheckedteacher").prop('checked')){
            $('#inputCheckedclassincharge').prop("disabled", false);
        }
        if( $('#inputCheckedclassincharge').prop( 'checked' ) ) {
            $("#inputCheckedteacher").prop('disabled', false);
        }
    });
    

    //  30/40*100 = 75.
    $('#obtain_marks').on('input', function() { 
    //alert($(this).val()); // get the current value of the input field.
    var total = 100;
    var ob = parseFloat($(this).val());
    var per  = parseFloat(ob/total)*100;
    $("#percentage").val(per);
    //alert(per);
});
    //alert($("#obtain_marks").val());

     var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    
    $("#classid").change(function()
    {
      //  alert($(this).val());
        var classid = $(this).val();
        var sectionid = $("#newsection").val();

        if (classid == '14' || classid == '15') {
            $('#stream').show();
                        $('#stream_').show();

            $('#section').hide();

        }//
        else {

            $('#section').show();
            $('#stream').hide();
                        $('#stream_').hide();

           

        }//

        if (classid == '14' || classid == '15') {
            //alert(classid);

            $.ajax({
                url: '/classstream',
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

  $("#stream").empty();
                    $("#stream").append("<option>Select</option>");
                    for (var i = 0; i < len; i++) {
                        var id = response[i]['hid'];
                        var name = response[i]['highsubname'];

                        $("#stream").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }
            }); // ajax
            
            $.ajax({
                url: '/sstreamsubject?classid=' + classid,
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#subjectdata").empty();
                    $("#subjectdata").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['subjectname'];

                        $("#subjectdata").append("<option value='" + id + "'>" + name + "</option>");

                    }
                    $('#subjectdata').attr("readonly", "readonly"); 

                }

            }); // ajax
            
            //student of streams
            
             $.ajax({
                    url: '/get-studnt-list-stream?classid=' + classid,
                    // alert(url);
                    type: 'get',
                    data: { classid: classid, sectionid: sectionid },
                    dataType: 'json',
                    success: function(response) {

                        var len = response.length;

                        $("#student_list").empty();

                        $("#student_list").append("<option>Select</option>");
                        for (var i = 0; i < len; i++) {
                            var id = response[i]['uid'];
                            var name = response[i]['name'];

                            $("#student_list").append("<option value='" + id + "'>" + name + "</option>");
                        }

                    }

                }); // ajax

        }//
        else {

            $.ajax({
                url: '/sectionclass',
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#newsection").empty();
                    $("#newsection").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['section_display_name'];

                        $("#newsection").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }

            }); // ajax
            $.ajax({
                url: '/classsubject?classid=' + classid,
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#subjectdata").empty();
                    $("#subjectdata").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['subjectname'];

                        $("#subjectdata").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }

            }); // ajax
            
            //student also load
             $('#newsection').change(function() {
               // alert('');
                //$('#student_list').select();
                var sectionid = $("#newsection").val();
                var classid = $("#classid").val();
                var value = $('select#student_list option:selected').val();
                //alert(value);


                //alert(sectionid);
                //alert(classid);


                $.ajax({
                    url: '/get-studnt-list?classid=' + classid + '&sectionid=' + sectionid,
                    // alert(url);
                    type: 'get',
                    data: { classid: classid, sectionid: sectionid },
                    dataType: 'json',
                    success: function(response) {

                        var len = response.length;

                        $("#student_list").empty();

                        $("#student_list").append("<option>Select</option>");
                        for (var i = 0; i < len; i++) {
                            var id = response[i]['uid'];
                            var name = response[i]['name'];

                            $("#student_list").append("<option value='" + id + "'>" + name + "</option>");
                        }

                    }

                }); // ajax
             });
            //student
        }
        });
    $('#staffm').change(function()
    {
       var staffm = $(this).val();
        $.ajax({
                url: '/staffm',
                // alert(url);
                type: 'get',
                data: { staffm: staffm },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $('#staffmsalary').empty();
                    $('#staffmsalary').append('<option>Select</option>');

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['stfid'];
                        var salary = response[i]['sl'];

                        $('#staffmsalary').append('<option value="' + id + '">' + salary + '</option>');

                    }
                }

            }); // ajax
    });
    
    //alert user when logout
            $('#popup').hide();
    $("#logout").click(function()
    {
        //alert('');
        $("#popup").show();
        
    });
    //check if card is click or cash in feesubmit
    $("input:radio[name=pay]").change(function() {
        //alert($('input[name=pay]:checked').val());
        var val_f = $('input[name=pay]:checked').val();
        //alert(val_f);
        if (val_f == "Y") {
            //alert('cash');
            $("#cash").show();
                        $("#card").hide();

        }
        if (val_f == "N") {
            //alert('card');
            $("#card").show();
                        $("#cash").hide();
        }
        
    });
    //fade display message after 5 sec
    $(".alert-success").delay(5000).fadeOut("slow");
    //alert($(".alert-danger").html());
         if($(".alert-success").html()!=null )
         {
             
             toastr.success($(".alert-success").html());
         }
         $(".alert-danger").delay(5000).fadeOut("slow");
         if($(".alert-danger").html()!=null )
         {
             
             toastr.error($(".alert-danger").html());
         }

    //change status
    function changestatus(id, e) {
        var el = $(e);
        var td = el.closest("td");
        var tr = el.closest("tr");
        var titles = el.attr('title');


        var statuson = td.find('.statuson').prop('disabled', true);
        var statusoff = td.find('.statusoff').prop('disabled', true);

        var status = +el.is(':checked');

        $.ajax({
            url: "/" + titles + "/changestatus/" + id,
            method: "POST",
            data: {
                status: status
            },
            success: function() {
                if (1 == status) {
                    tr.css('color', '');
                }
                else {
                    tr.css('color', 'red');
                }
            },
            complete: function() {
                statuson.prop('disabled', false);
                statusoff.prop('disabled', false);
            }
        });
        return;
    }
    $(".sort_pend").change(function() {
        //alert($('select#sort_pend option:selected').val());
       // alert($(this).val());
        var sort_val = $(this).val();
        if (sort_val == "pend_m") {
            //alert(sort_val);
            $.ajax({
                url: '/getpenddata?pen=' + sort_val,
                // alert(url);
                type: 'get',
                data: { pen: sort_val },
                dataType: 'json',
                success: function(response) {
                    var len = response.length;

                    //$("#submis_mon1").empty();
                    //$("#pending_months").empty();

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['id'];
                        //var name = response[i]['monthname'];

                        $("#c1").append("<option value='" + id + "'>" + id + "</option>");
                        //$("#pending_months").append("<option value='"+id+"'>"+name+"</option>");


                    }
                }
            }); // ajax
        }
    });
    //alert('');
    $("#validateButton_feesubmit").click(function() {
        //alert('');
        var val_btn = $(this).is('[disabled=disabled]');
        var classid = $("#class").val();
        var stdid = $("#student_list").val();
        if (val_btn == false && classid != '' && stdid != '') {
            //alert('check class and modal');
            confirmationmodal();
            return false;
        }
        //return false;
    })

    //check the value of feesclear
    $("input[name=pending_c]").click(function() {
        $("input[name=pending_c]").val();
        $(".dvPassport1").toggle(this.checked);
    });

    $(".dressc").click(function() {
        $(".dvPassport").toggle(this.checked);
    });

    $(".transc").click(function() {
        $(".transcharge").toggle(this.checked);
    });
    $(".oth").click(function() {
        $(".ocharge").toggle(this.checked);
    });
    $(".discountamount").click(function() {
        $(".discountamounts").toggle(this.checked);
    });
    /*page load totle*/
    var readytotle = addTotleAmount();
    $('#subtotalamount').val(readytotle);
    //alert(readytotle);
    /*page load totle ends*/

    /*upper input change val*/
    $('.add').blur(function() {
        checkeachcheckboxVal();
    });

    /*upper input change val ends*/


    $(document).on('change', '.checkboxamount_d, #txtothercharge, #txtdiscountcharge, #donate, #transport,#inputRadioscharge ', calc_charges);
    $(document).on('keyup', '#txtothercharge, #txtdiscountcharge, #donate', calc_charges);

    $("input:radio[name=feesclear]").change(function() {
        //alert($('input[name=feesclear]:checked').val());
        var val_f = $('input[name=feesclear]:checked').val();
        //alert(val_f);
        if (val_f == "Y") {
            //alert("show div");
            $("#pending_months").hide();
            //$("#submission_months").show();
            //$("#pend_mon").val(0);
            //alert($("#pend_mon").val());
            $("#pend_mon").val();
        }
        if (val_f == "N") {
            //alert("hide div");
            $("#pending_months").show();
            $("#submission_months").hide();
            //alert($("#pend_mon").val());
            $("#pend_mon").val();

        }
    });
    //form feesubmission

    //on student select
    $('select[name=student_lst]').change(function() {
        //alert($("select#student_list option:selected").val());
        var stdid = $("select#student_list option:selected").val();
        var classid = $("#class").val();

        var authenticateuserid = $('#authenticateuserid').val();
        $.ajax({
            url: '/getStudentdata?student_id=' + stdid + '&classid=' + classid + '&authenticateuserid=' + authenticateuserid,
            method: 'GET',
            dataType: "json",
            success: function(response) {
                if (response != '') {
                    if (response[0]['count_pending'] != 0)

                    {
                        $("#fee_months").show();
                        $("#sub_mont").show();

                        var len = response.length;
                        for (var i = 0; i < len; i++) {
                            var pen = response[0]['pending_months'];
                            var sub = response[0]['submission_months'];
                            var total_am  = response[0]['total_amount'];
                            var total_m  = response[0]['sub_total'];


                            var pending_months = $("#pending_months").val(response[0]['pending_months']);
                            var submission_months = $("#submission_months").val(sub);
                            $("input[name=pendingcharge]").val(response[0]['count_pending']);
                            $("input[name=total_fees]").val(response[0]['total_amount']);
                            $("input[name=amount]").val(response[0]['sub_total']);

                            $("input[name=fee_id]").val(response[0]['id']);

                        } //for 
                        var pend = $.trim(response[0]['pending_months']);
                        var sub = $.trim(response[0]['submission_months']);

                        $("input[name=count_pend]").val(response[0]['count_pending']);
                        $("input[name=total_fees]").val(response[0]['total_amount']);
                        $("input[name=amount]").val(response[0]['sub_total']);
                        $("input[name=pen_data]").val(response[0]['pending_months']);
                        $("input[name=sub_data]").val(response[0]['submission_months']);
                        $("input[name=pend_data]").val(response[0]['pend_m']);

                        //now append only selected months to drop down
                        $.ajax({
                            url: '/getselectmonth?pen=' + pend + '&sub=' + sub,
                            // alert(url);
                            type: 'get',
                            //data: {pen:pend},
                            dataType: 'json',
                            success: function(response) {
                                var len = response.length;
                                $("#submis_mon1").empty();
                                $("#pending_months").empty();

                                for (var i = 0; i < len; i++) {
                                    var id = response[i]['id'];
                                    var name = response[i]['monthname'];

                                    $("#submis_mon1").append("<option value='" + id + "'>" + name + "</option>");
                                    $("#pending_months").append("<option value='" + id + "'>" + name + "</option>");


                                }
                            }
                        }); // ajax                        //drop down

                    } //if count_pending is 0
                    else {
                        $("#submis_mon1").empty();
                        $("#pending_months").empty();
                        $("#fee_months").hide();
                        $("#pending_months").empty();
                        $("#submission_months").empty();
                        $("input[name=pendingcharge]").empty();
                        $("input[name=total_fees]").empty();
                        $("input[name=amount]").empty();


                    }
                } // if response not empty

                /*if(response == '')
                {
                   $("#fee_months").hide();
                   $("#pending_months").empty(); 
                   $("#submission_months").empty();
                }*/
            },
        });
    });

    $('#class').on('change', function() {
        //alert('');

        var classid = $(this).val();
        //alert(classid);
        var sectionid = $("#newsection").val();

        if (classid == '14' || classid == '15') {
            $('#stream_').show();
            $('#section').hide();

        }
        else {

            $('#section').show();
            $('#stream_').hide();
            $("#addsn").show();
            $("#schfee").show();
            $("#book").show();
            $("#subtotal").show();
            $("#dontaion").show();

        }
        //alert($(this).val());
        if (classid == '') {
            //alert(classid);
            $("#addsn").hide();
            $("#schfee").hide();
            $("#book").hide();
            $("#subtotal").hide();
            $("#dontaion").hide();
        }
        var authenticateuserid = $('#authenticateuserid').val();
        var feesub = $('#fe_id').val();
        //alert(feesub);
        $.ajax({
            url: '/getcharge?classid=' + classid + '&authenticateuserid=' + authenticateuserid,
            method: 'GET',
            success: function(response) {
                //console.log(response[0]['dresscharge']);
                $('#txtdresscharge').val(response[0]['dresscharge']);
                $('#txttransportcharge').val(response[0]['transportcharge']);
                $('#sfee').val(response[0]['schoolfee']);
                if(feesub == '')
                {
                    var bookcharge = 0; // not calculating for feesubmit
                var admsn = 0; //already admitted
                }
                else
                {
                var bookcharge = $('#book_charge').val();
                var admsn = $('#adfee').val();
                }
                var schfee = $("#sfee").val();
                var finalamount = parseFloat(bookcharge) + parseFloat(admsn) + parseFloat(schfee);
                $('#subtotalamount').val(finalamount);

                //$this.find( '.modal-body' ).html( response );
            },
        });
        
        


        if (classid == '14' || classid == '15') {
            //alert(classid);

            $.ajax({
                url: '/classstream',
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#stream").empty();
                    for (var i = 0; i < len; i++) {
                        var id = response[i]['hid'];
                        var name = response[i]['highsubname'];

                        $("#stream").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }
            }); // ajax
            $.ajax({
                url: '/sstreamsubject?classid=' + classid,
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#subjectdata").empty();
                    $("#subjectdata").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['subjectname'];

                        $("#subjectdata").append("<option value='" + id + "' selected='selected'>" + name + "</option>");

                    }
                }

            }); // ajax
            
            $.ajax({
                url: '/leftsubject?classid=' + classid,
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#leftsubject").empty();
                    $("#leftsubject").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['subjectname'];

                        $("#leftsubject").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }

            }); // ajax
            
            //student of streams
            
             $.ajax({
                    url: '/get-studnt-list-stream?classid=' + classid,
                    // alert(url);
                    type: 'get',
                    data: { classid: classid, sectionid: sectionid },
                    dataType: 'json',
                    success: function(response) {

                        var len = response.length;

                        $("#student_list").empty();

                        $("#student_list").append("<option>Select</option>");
                        for (var i = 0; i < len; i++) {
                            var id = response[i]['uid'];
                            var name = response[i]['name'];

                            $("#student_list").append("<option value='" + id + "'>" + name + "</option>");
                        }

                    }

                }); // ajax


        }
        else {

            $.ajax({
                url: '/sectionclass',
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#newsection").empty();
                    $("#newsection").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['section_display_name'];

                        $("#newsection").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }

            }); // ajax
            
            $.ajax({
                url: '/classsubject?classid=' + classid,
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#subjectdata").empty();
                    $("#subjectdata").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['subjectname'];
                       $("input[name=sd]").val(response[i]['sid']);


                        $("#subjectdata").append("<option value='" + id + "' selected='selected'>" + name + "</option>");

                    }
$('#subjectdata').attr("readonly", "readonly"); 

                }
            }); // ajax
            
            $.ajax({
                url: '/leftsubject?classid=' + classid,
                // alert(url);
                type: 'get',
                data: { classid: classid },
                dataType: 'json',
                success: function(response) {

                    var len = response.length;

                    $("#leftsubject").empty();
                    $("#leftsubject").append("<option>Select</option>");

                    for (var i = 0; i < len; i++) {
                        var id = response[i]['sid'];
                        var name = response[i]['subjectname'];

                        $("#leftsubject").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }

            }); // ajax
            //alert(classid);
            //alert($("#append_sec").text());


            $('#newsection').change(function() {
               // alert('');
                //$('#student_list').select();
                var sectionid = $("#newsection").val();
                var classid = $("#class").val();
                var value = $('select#student_list option:selected').val();
                //alert(value);


                //alert(sectionid);
                //alert(classid);


                $.ajax({
                    url: '/get-student-list?classid=' + classid + '&sectionid=' + sectionid,
                    // alert(url);
                    type: 'get',
                    data: { classid: classid, sectionid: sectionid },
                    dataType: 'json',
                    success: function(response) {

                        var len = response.length;

                        $("#student_list").empty();

                        $("#student_list").append("<option>Select</option>");
                        for (var i = 0; i < len; i++) {
                            var id = response[i]['uid'];
                            var name = response[i]['name'];

                            $("#student_list").append("<option value='" + id + "'>" + name + "</option>");
                        }

                    }

                }); // ajax
                
                 $.ajax({
                    url: '/get-student-list-attendance?classid=' + classid + '&sectionid=' + sectionid,
                    // alert(url);
                    type: 'get',
                    data: { classid: classid, sectionid: sectionid },
                    dataType: 'json',
                    success: function(response) {

                        var len = response.length;
                        var store = [];
                       
                    $("#student_lst1").empty();
                    $("#submt").show();
                    $("#student_lst1").append("<tr></tr>");
                        for (var i = 0; i < len; i++) {
                            var id = response[i]['uid'];
                            var name = response[i]['name'];
                            var uname = response[i]['username'];

                            var fname = response[i]['father_name'];
                            var mname = response[i]['mother_name'];

                            $("#nme").val(name);

                        $("#student_lst1").append(
   "<tr><td>"+name+"</td><td>"+uname+"</td><td>"+fname+"</td><td>"+mname+"</td><td><div class='funkyradio'><div class='funkyradio-success'><input type='radio' id='attendance"+i+id+"1' show='present' name='attendance["+id+"]' value='present' checked><label for='attendance"+i+id+"1'>Present</label></div></div></td><td><div class='funkyradio'><div class='funkyradio-danger'><input type='radio' id='attendance"+i+id+"2' show='absent' name='attendance["+id+"]' value='absent'><label for='attendance"+i+id+"2'>Absent</label></div></div></td><td><div class='funkyradio'><div class='funkyradio-warning'><input type='radio' id='attendance"+i+id+"3' show='leave' name='attendance["+id+"]' value='leave'><label for='attendance"+i+id+"3'>Leave</label></div></div></td><td><div class='funkyradio'><div class='funkyradio-info'><input type='radio' id='attendance"+i+id+"4' show='holiday'  name='attendance["+id+"]' value='holiday'><label for='attendance"+i+id+"4'>Holiday</label></div></div></td></tr>"
                            );
                             
                        }
                        

                    }

                }); // ajax

            }); //on change of section give student name


        }

    }); // change 

    $("select#submis_mon1").change(function() {
        
        var months = [];
        var paid_m = [];
        $.each($("select#submis_mon1 option:selected"), function() {
            months.push($(this).val());
            paid_m.push($(this).text());

        });
        $.each($("select#submis_mon1 option:selected"), function() {
            //months.push($(this).text());

        });
        
        $("input[name=paid_m]").val(paid_m.join(", "));
        $("input[name=pendingcharge]").val(months.length);
        //$("input[name=pendingcharge]").val(months.length);


    });


    //pend_mon

    $("select#pend_mon").change(function() {

        var pen_months = [];
        $.each($("select#pend_mon option:selected"), function() {
            //months.push($(this).val());
            pen_months.push($(this).text());

        });
        //alert("You have selected the country - " + months.join(", "));
        //alert(pen_months.length);
        //alert(pen_months.join(", "))
        $("input[name=pend_m]").val(pen_months.join(", "));

    });


    //country state city
    $('#country_list').change(function() {
        var countryID = $(this).val();

        if (countryID) {

            $.ajax({

                type: "GET",

                url: '/get-state-list?country_id=' + countryID,

                dataType: "json",

                success: function(res) {

                    if (res) {

                        $("#state_list").empty();

                        $("#state_list").append('<option>Select</option>');

                        $.each(res, function(key, value) {

                            $("#state_list").append('<option value=' + key + '>' + value + '</option>');

                        });


                    }
                    else {

                        $("#state_list").empty();

                    }

                }

            });

        }
        else {

            $("#state_list").empty();

            $("#city_list").empty();

        }

    });
    $('#state_list').on('change', function() {

        var stateID = $(this).val();

        if (stateID) {

            $.ajax({

                type: "GET",

                url: '/get-city-list?state_id=' + stateID,

                dataType: "json",

                success: function(res) {

                    if (res) {

                        $("#city_list").empty();

                        $.each(res, function(key, value) {

                            $("#city_list").append('<option value="' + key + '">' + value + '</option>');

                        });



                    }
                    else {

                        $("#city_list").empty();

                    }

                }

            });

        }
        else {

            $("#city_list").empty();

        }

    }); //end of city state change

    //  Auto complete address
    var input = document.getElementById('address');
    var autocomplete = new google.maps.places.Autocomplete(input);
}); //document.ready

/*check and change the dresscharge */
function calc_charges() {
        var feesub = $('#fe_id').val();
//alert(feesub);
    var total = 0;

    // subtotalamount
    var subtotalamount = $('#subtotalamount');
if(feesub != '')
                {
                    var book_charge = +0; // not calculating for feesubmit
                var adfee = +0; //already admitted
                }
                if(feesub == '' || feesub == null)

                {
                var book_charge = +$('#book_charge').val();
                var adfee = +$('#adfee').val();
                }
    // book charge
    //var book_charge = +0;

    // adfee
    //var adfee = +0;

    // sfee
    var sfee = +$('#sfee').val();

    total = book_charge + adfee + sfee;


    // check if the dress charge is checked
    var inputChecked_dress = $('#inputChecked_dress');
    if (inputChecked_dress.is(':checked')) {
        var dress_val = +$('#txtdresscharge').val();
        total = total + dress_val;
    }

    //check pending fees
    var inputChecked_pending = $('#inputChecked_pending');
    if (inputChecked_pending.is(':checked')) {
        var pend_val = +$('input[name=pendingcharge]').val();
        //alert(pend_val);
        var mult = sfee * pend_val;
        //alert(mult);
        total = total + (sfee * pend_val);
    }

    // check if the transport charge is checked
    var inputChecked_trans = $('#inputChecked_trans');
    var transcheck = $('input[name=inputRadios]').val();
    var fare = 0;
    var txttransportcharge;
    if (inputChecked_trans.is(':checked')) {
        fare = +$('#inputRadioscharge').val();
        total = total + fare;

    }
    //alert($('#inputChecked_trans').val());
    
    
    

    // check if the other charge is checked
    var inputChecked_other = $('#inputChecked_other');
    if (inputChecked_other.is(':checked')) {
        var txtothercharge = +$('#txtothercharge').val();
        total = total + txtothercharge;
    }

    // check if the discount is checked
    var inputChecked_discount = $('#inputChecked_discount');
    if (inputChecked_discount.is(':checked')) {
        var txtdiscountcharge = +$('#txtdiscountcharge').val();
        total = total - txtdiscountcharge;
    }

    // donate
    var donate = +$('#donate').val();
    if (donate) total = total + donate;

    subtotalamount.val(total);
    $('#subfinaltotalnew').val(total);
    $('#finalsubtotalamountcard').val(total);
}

function checkeachcheckboxValother() {
    var total = 0;
    var upperinputTotal = addTotleAmount();
    $('.checkboxamount').each(function() {
        $(this).val(this.checked);
        if (this.checked) {
            var showidinput = $(this).attr('show');
            var id = '.' + showidinput;
            $(document).find(id).css('display', 'block');
            var idamount = $(id).find('input').val();
            total = parseInt(total) + parseInt(idamount);
        }
    });
    var othercharge = $('#txtothercharge').val();
    var final = parseFloat(upperinputTotal) + parseFloat(othercharge);
    // console.log(parseInt(total));
    $('#subtotalamount').val(final);

}

function checkeachcheckboxValdis() {
    var total = 0;
    var upperinputTotal = addTotleAmount();
    $('.checkboxamount').each(function() {
        $(this).val(this.checked);
        if (this.checked) {
            var showidinput = $(this).attr('show');
            var id = '.' + showidinput;
            $(document).find(id).css('display', 'block');
            var idamount = $(id).find('input').val();
            total = parseInt(total) + parseInt(idamount);
        }
    });
    var discount = $('#txtdiscountcharge').val();
    var final = parseFloat(upperinputTotal) - parseFloat(discount);
    // console.log(parseInt(total));
    $('#subtotalamount').val(final);

}

function getdatastudent(id)
{
    //alert(id);
    var classid = id;
    $('#mydetail-' + classid).find("#re").html('');
    $.ajax({
        url: '/get-student-data',
        type: 'get',
        data: { classid: classid },
        dataType: 'json',
        success: function(response) {
            var len = response.length;
            if( !len ) {
                $('#mydetail-' + classid).find("#re").append('<p><i>No Data.</i></p>');
                return;
            }
            $('#mydetail-' + classid).find("#re").append('<table class="table table-hover dataTable table-striped w-full table-responsive" data-plugin="dataTable"><thead><tr><th>Name</th><th>User Name</th><th>Gender</th><th>DOJ</th><th>Father Name</th><th>Father Contact</th><th>Mother Name</th><th>Mother contact</th><th>Residential contact</th><th>Email</th><th>Address</th></tr></thead><tbody>')
            for (var i = 0; i < len; i++) {
                var id = response[i]['id'];
                var name = response[i]['name'];
                var uname = response[i]['username'];
                var gn = response[i]['gender'];
                var fc = response[i]['father_contact_number'];
                 var mc = response[i]['mother_contact_number'];
               var dj = response[i]['school_joining_date'];
                  var ht = response[i]['home_tel'];
              var em = response[i]['email'];
              var ad = response[i]['address'];
                var fname = response[i]['father_name'];
               var mname = response[i]['mother_name'];
                $('#mydetail-' + classid).find("#re tbody").append
                ('<tr><td>'+name+'</td><td>'+uname+'</td><td><span class="badge badge-'+('M' == gn ? 'primary' : ('F' == gn ? 'info' : 'danger'))+'">'+('M' == gn ? 'Male' : ('F' == gn ? 'Female' : 'Not Specified'))+'</span></td><td>'+dj+'</td><td>'+fname+'</td><td>'+fc+'</td><td>'+mname+'</td><td>'+mc+'</td><td>'+ht+'</td><td>'+em+'</td><td>'+ad+'</td></tr>');
            }
           $('#mydetail-' + classid).find("#re").append('</tbody></table>');

        }
    });
}
/*all inputs amounts*/
function addTotleAmount() {
    var total = 0;
    //var bookcharge  = $('#book_charge').val();
    //var admissionfee  = $('#adfee').val();
    var bookcharge = 0;
    var admissionfee = 0;
    var schfee = $('#sfee').val();

    //total = parseInt(bookcharge) + parseInt(admissionfee) + parseInt(schfee);
    total = parseInt(schfee);
    //console.log(total);
    //$('#subtotalamount').val(total);
    return total;
}
/*all inputs amounts ends*/


///feestotal
function mainfee() {
    return;
    var dresscharge = 0;
    var bookcharge = '';
    var admsn = '';
    var schfee = '';
    var discountcharge = 0;
    $('#txtdresscharge').change(function() {
        if ($(this).val() != '') {
            dresscharge = $(this).val();

        }
    })

    var pendcharge = 0;
    $('#txtpendcharge').change(function() {
        if ($(this).val() != '') {
            pendcharge = $(this).val();

        }
    })

    var transportcharge = 0;
    $('#txttransportcharge').change(function() {
        if ($(this).val() != '') {
            transportcharge = $(this).val();
        }
    })

    var othercharge = 0;
    if ($('#txtothercharge').val() != '') {
        othercharge = $('#txtothercharge').val();
    }

    if ($('#txtdiscountcharge').val() != '') {
        discountcharge = $('#txtdiscountcharge').val();
    }
    // var bookcharge = 0;
    // if($('#book_charge').val()!=''){
    //     bookcharge = $('#book_charge').val();
    // }
    var donate = 0;
    if ($('#donate').val() != '') {
        donate = $('#donate').val();
    }
    bookcharge = 0;
    admsn = 0;
    schfee = $("#sfee").val();


    var initialtotal = parseFloat(pendcharge) + parseFloat(dresscharge) + parseFloat(transportcharge);
    var ab = initialtotal + parseFloat(othercharge);
    var abc = ab - parseFloat(discountcharge);
    var abcd = abc + parseFloat(bookcharge);
    var addsc = parseFloat(admsn) + parseFloat(schfee);
    var abcde = abcd + parseFloat(addsc);
    var abdef = abcde + parseFloat(donate);
    var finaltotalamount = parseFloat(abdef);
    //console.log(abcd);
    $('#subtotalamount').val(finaltotalamount);
    $('#finalsubtotalamount').val(finaltotalamount);

}

//enter only digits from keyboard
function onlyDigitsMother(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if (charCode > 47 && charCode < 58) {

            return true;
        }
        else {
            $('.mother_contact_number,.father_contact_number').html('Please Enter Digits only');
            return false;
        }

    }
    catch (err) {

        alert(err.Description);
    }
}

function check_empty() {
    var classid = $("#class").val();
    var stdid = $("#student_list").val();
    if (classid != '') {
        confirmationmodal();
        return false;
    }
    else {
        return true;
    }
    return false;
}

function printData() {
    var divToPrint = document.getElementById("studendetailconfirmationmodal");
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}

function form_submit() {
    //
    //alert('ow');
    // window.print();
    $("#feesubmission").submit();

}

function confirmationmodal() {
    var student_name = $('#student_list option:selected').text();
    //alert(studentfname);

    var classes = $('#class option:selected').text();
    var newsection = $('#newsection option:selected').text();


    var txtdresscharge = $('.txtdresscharge').val();
    var transportcharge = $('.tranportchargegetdata option:selected').text();
    var txtothercharge = $('#txtothercharge').val();
    var txtdiscountcharge = $('.txtdiscountcharge').val();
    var pending_m = $("input[name=pendingcharge]").val();
        var total_fees = $("input[name=total_fees]").val();
                var amount = $("input[name=amount]").val();



    var schfee = $('.monthlycharge').val();
    var donate = $('#donate').val();
    var subtotalamount = $('#subtotalamount').val();
    //alert(subtotalamount);


    $('#student_full').text(student_name);
    $('#student_class').text(classes);
    $('#student_section').text(newsection);
    $('#studentdresscharge').text(txtdresscharge);
    $('#pending_m').text(pending_m);
    $('#studenttxtdiscountcharge').text(txtdiscountcharge);
    $('#studenttransport').text(transportcharge);
    $('#studentother').text(txtothercharge);
    /*$('#studentbook').text(book_charge);
    $('#studentadmision').text(adfee);*/
    $('#studentschoolfee').text(schfee);
    /*$('#studentdonation').text(donate);*/
    $('#studentsubtotalamount').text(subtotalamount);


    $('#studendetailconfirmationmodal').modal('show');
}

/*function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}*/
