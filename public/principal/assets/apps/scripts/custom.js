$(document).ready(function () {
    
// $('#addbox').click(function () {
//     $('#newtextboxadd').append('<input type="text" id="current Name" value="" />');
    
// });
//change mark
$( '[id^=addbox-]' ).on( 'click', function() {
    $this = $(this);
    var test_id = $this.data( 'test_id' );
    $('#newtextboxadd-'+test_id).empty();
    $('#newtextboxadd-'+test_id).append('<input type="text" id="current-'+test_id+'" value="" />');
    $('#removetd-'+test_id).hide();
    $('#addtd-'+test_id).show();
            // $this.find( '.modal-body' ).html( 'Loading...' );
            // $.ajax( {
            //     url: '/staffupdate/fetchdata/' + staff_member_id,
            //     method: 'GET',
            //     success: function( response ) {
            //         $this.find( '.modal-body' ).html( response );
            //     },
            // } );
            // console.log( staff_member_id );
    });
//cancel mark
$( '[id^=canceltestdetail-]' ).on( 'click', function() {
    $this = $(this);
    var cancel_id = $this.data( 'cancel_id' );
    //alert(cancel_id);
    $('#removetd-'+cancel_id).show();
    $('#notavailable-'+cancel_id).show();
    $('#addtd-'+cancel_id).hide();
    $('#newtextboxadd-'+cancel_id).empty();
    $('#newtextboxadd-'+cancel_id).append('<h4 class="notavailable">N/A</h4>');
});
//save mark
$( '[id^=savebox-]' ).on( 'click', function() {
    $this = $(this);
    var save_id = $this.data( 'save_id' );
    var userdata = $('#current-'+save_id).val();
    //alert(userdata);
    $.ajax( {
        url: '/testdetail/testmark/',
        method: 'GET',
        data : {'studentid':save_id,'userdata':userdata},
        success: function( response ) {
                    console.log(response['obtain_mark']);
                    $( '#current-'+save_id ).remove();
                    $('#newtextboxadd-'+save_id).html(response['obtain_mark']);
                },
    });
});
    
//classwise subject
     $('#mynewclass').on('change',function()
     {
        var classid = $(this).val();
        var url1 = 'gethighclasslist?classid='+classid;
        var url2 = 'getsubjectlist?classid='+classid;
        //alert(classid);
        if(classid == '14' || classid=='15')
        {
         $('.highclassidcl').show();
         $('.newsubjectididcl').hide();
         $(".highclassidcl").removeAttr("style");
            $.ajax({
                type:"GET",
                url:url1,
                dataType: "json",
                success:function(res)
                {
                    if(res)
                    {
                        $("#highclassid").empty();
                        $("#highclassid").append('<option>--Select Stream--</option>');
                        $.each(res,function(key,value){
                            console.log(key);
                            $("#highclassid").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else
                    {
                        $("#highclassid").empty();
                    }
                }
            });
        }else{
            $('.highclassidcl').hide();
            $('.newsubjectididcl').show();
            $(".stremtestid").css('display','none');
            $.ajax({
                type:"GET",
                url:url2,
                dataType: "json",
                success:function(res)
                {
                    if(res)
                    {
                        $("#newsubjectid").empty();
                        $("#newsubjectid").append('<option>--Select Subject--</option>');
                        $.each(res,function(key,value){
                            console.log(key);
                            $("#newsubjectid").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else
                    {
                        $("#newsubjectid").empty();
                    }
                }
            });
        }
        //highsubjectlist
        $('.newhighclassidcl').on('change',function()
        {
            $('.newstreamsubjectid').removeAttr('style');
            var streamid = $(this).val();
            var url3 = 'gethighSubjectList?stream='+streamid;
            $.ajax({
                type:"GET",
                url:url3,
                dataType: "json",
                success:function(res)
                {
                    if(res)
                    {
                        $("#streamsubjectid").empty();
                        $("#streamsubjectid").append('<option>--Select Subject--</option>');
                        $.each(res,function(key,value){
                            console.log(key);
                            $("#streamsubjectid").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else
                    {
                        $("#streamsubjectid").empty();
                    }
                }
            });
        });
    });
    // cherckboxes for sections and classes
    {
        $( '.sectioncheckedfirst' ).on( 'change', function( e ){
            //console.log('hello');
            e.preventDefault();
            var $this = $(this);
            
            var parent_row = $this.closest( '.margin-bottom' );
            if( $this.is( ':checked' ) ) {
                parent_row.find( '.class_check' ).prop( 'checked', true );
                return;
            }
            var any_checked = false;
            parent_row.find( '.sectioncheckedfirst' ).each( function() {
                if( $(this).is( ':checked' ) ) {
                    any_checked = true;
                }
            } );
            if( !any_checked ) {
                parent_row.find( '.class_check' ).prop( 'checked', false );
            }
        } );
    }
    //add section
     $('.classsectionclose').on('click',function()
     {
        var customsection = $('#customnewsectionadd').val();
        // save the previously checked states
        {
            // var checked_sections = {};
            // $( '.sectioncheckedfirst' ).each( function() {
            //     if( $(this).is( ':checked' ) ) {
            //         var id = $(this).attr( 'id' );
            //         id = id.replace( 'inputChecked', '' ).split( '_' );
            //         if( !( checked_sections[ id[ 0 ] ] ) ) checked_sections[ id[ 0 ] ] = [];
            //         checked_sections[ id[ 0 ] ].push( id[ 1 ] );
            //     }
            // } );
            // $( '.sectioncheckedfirst' ).each( function() {
            //     if( 1 == $( this ).data( 'custom' ) ) {
            //         $( this ).closest( '.checkbox-custom' ).remove();
            //     }
            // } );
            // $( '.margin-bottom' ).each( function() {
            //     var sectioncheck = $( this ).find( '.class_only_sections' );
            //     if( sectioncheck.length ) {
            //         var sectioncheckedfirst = sectioncheck.find( '.sectioncheckedfirst' ).eq( 0 );
            //         var class_id = sectioncheckedfirst.data( 'class_id' );
            //         sectioncheck.append('<div class="checkbox-custom checkbox-primary change-display-property "><input type="checkbox" data-class_id="'+ class_id +'" data-custom="1" class="sectioncheckedfirst" id="inputChecked'+ class_id +'_6" name="sectionclass['+ class_id+'][]" data-section="" value="6"><label for="inputChecked' + class_id + '_6">Test value to add</label></div>');
            //     }
            // } );
            //return;
        
        $.ajax({
                url: '/addnewsction/',
                method: 'get',
                data:{'customsection':customsection},
                dataType: "json",
                success: function( res ) {
                    console.log(res);
                    // console.log(res['id']);
                    $('.datanewsection').remove();
                    $( '.sectioncheckedfirst' ).each( function() {
                        if( 1 == $( this ).data( 'custom' ) ) {
                            $( this ).closest( '.checkbox-custom' ).remove();
                        }
                    } );
                    $( '.sectioncheckedfirst' ).each( function() {
                        if( 1 == $( this ).data( 'custom' ) ) {
                            $( this ).closest( '.checkbox-custom' ).remove();
                        }
                    } );
                    $.each(res,function(key,value){
                    $( '.margin-bottom' ).each( function() {
                        
                        var sectioncheck = $( this ).find( '.class_only_sections' );
                        if( sectioncheck.length ) {
                            var sectioncheckedfirst = sectioncheck.find( '.sectioncheckedfirst' ).eq( 0 );
                            var class_id = sectioncheckedfirst.data( 'class_id' );
                            
                            sectioncheck.append('<div class="checkbox-custom checkbox-primary change-display-property datanewsection"><input type="checkbox" data-class_id="'+ class_id +'" data-custom="0" class="sectioncheckedfirst mysectionnchcking" id="inputChecked'+ class_id +'_'+key+'" name="sectionclass['+ class_id+'][]" data-section="" value="'+key+'" ><label for="inputChecked' + class_id + '_'+key+'">'+value+'</label></div>');
                        }
                    });
                    });
                    //console.log(res['newquery']);
                    /*if(res)
                    {
                        $(".sectioncheck").empty();
                        $.each(res,function(key,value){
                            //$.each(res['newquery'],function(newkey,newvalue){
                                // if(newvalue == 1){
                                // var checkvalue = 'checked';
                                // }
                            $('.sectioncheck').append("<div class='checkbox-custom checkbox-primary change-display-property'><input type='checkbox' id='inputChecked"+key+"' name='sectionclass"+key+"'  value="+value+"/><label for='inputChecked"+key+"'>"+value+"</label></div>");
                                    
                            });
                       // });
                    }else
                    {
                        $("#sectioncheck").empty();
                    }*/
                    // console.log(response['dob']);
                    // $( '.changedob' ).val(response['dob']).html( response['dob'] );
                    // $('.newdob').html(response['dob']);
                },
        });
        
        }
  });

$('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Matching').css('color', '#66cc66');
    $(".wizard-buttons a:nth-child(2)").css('display','block');
    $(".similarpass").css('display','block');
  } else 
    $('#message').html('Not Matching').css('color', '#ff6666');
    })
    //check password
    $('#password').on('keyup', function () {
      if ($('#password').val().length >= 7) {
        
        //$('#messagelength').html('correct').css('color', '#66cc66');
        
      } else 
        $('#messagelength').html('you have to enter at least 8 digit!').css('color', '#ff6666');
        $(".wizard-buttons a:nth-child(2)").css('display','none');
        $(".similarpass").css('display','none');
        });
    //alert($('.cropped').val());
});




//Delete
$('tbody').delegate('.btn-delete','click',function(){

        var value=$(this).data('id');
        var e = $('.btn-delete').attr('title');

        //alert($e);

        var url = e+"/destroy/"+value;

        // $.ajax({

        //   type: 'get',

        //   url: url,

        //   data: {'id':value},

        //   success : function(data){

        //     $('#employee'+value).remove()

        //     //console.log(data);

        //   }

        // })

      });

      $('body').on('click','.btn-delete',function(){
       that = $(this);
       var value=$(this).data('id');
        var e = $('.btn-delete').attr('title');

        //alert($e);

        var url = e+"/destroy/"+value;
        
        swal({

          title: "Are you sure?",

          text: "Do you want to delete!",

          type: "warning",

          showCancelButton: true,

          confirmButtonColor: "#DD6B55",

          confirmButtonText: "Yes, delete it!",

          cancelButtonText: "No, cancel plx",

          closeOnConfirm: 1,

          closeOnCancel: 1

        },

        function(isConfirm){

          if (isConfirm) {

            swal("Deleted!", e+ "deleted.", "success");

            $.ajax({

              type: 'get',

              url: e+"/destroy/"+value,

              data: {'id':value},

            })
             
             $(that).parents('tr').hide();
            //$(tbody).parents('tr').hide();
             $('#class'+value).remove();



          } else {

           swal("Cancelled", "Your imaginary file is safe :)", "error");

          }

        });

      })
      //change status
      function changestatus( id, e ) {
    var el = $( e );
    var td = el.closest( "td" );
    var tr = el.closest( "tr" );
    var titles = el.attr('title');
    
    
    var statuson = td.find( '.statuson' ).prop( 'disabled', true );
    var statusoff = td.find( '.statusoff' ).prop( 'disabled', true );
    
    var status = +el.is( ':checked' );
    
    $.ajax( {
        url: "/"+titles+"/changestatus/" + id,
        method: "POST",
        data: {
            status: status
        },
        success: function() {
            if( 1 == status ) {
                tr.css( 'color', '' );
            } else {
                tr.css( 'color', 'red' );
            }
        },
        complete: function(){
            statuson.prop( 'disabled', false );
            statusoff.prop( 'disabled', false );
        }
    } );
    return;
}
function myFunction() {
    location.reload();
}
//validation
 $("form#staff").validate({
       
        showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
 $("form#announce-add").validate({
       
        showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
  //moadl open checkbox click
//   if($('#inlineCheckbox1-classincharge').checked){
//       alert('hello');
//      $('#myModal').modal();
//   }
$(document).ready(function () {
$(".classesselect").hide();
$("#inlineCheckbox1-classincharge").click(function() {
    if($(this).is(":checked")) {
        $(".classesselect").show();
    } else {
        $(".classesselect").hide();
    }
});
});


$(function () {
        $("#class").change(function () {
            if ($(this).val() == '14' || $(this).val() == '15') {
                $("#stream").show();
                $("#subject").show();
                $("#section").hide();
                
            }else{
                $("#stream").hide();
                $("#subject").hide();
                $("#section").show();
                
            }
            //alert($(this).val());
            if ($(this).val() == '') {
                $("#addsn").hide();
                $("#schfee").hide();
                $("#book").hide();
                $("#subtotal").hide();
                $("#dontaion").hide();
            } else {
                $("#addsn").show();
                $("#schfee").show();
                $("#book").show();
                $("#subtotal").show();
                $("#dontaion").show();
               
            }
            var classid = $(this).val();
            var authenticateuserid = $('#authenticateuserid').val();
            $.ajax({
                url: '/getcharge?classid=' + classid + '&authenticateuserid='+authenticateuserid ,
                method: 'GET',
                success: function( response ) {
                    //console.log(response[0]['dresscharge']);
                    $('#txtdresscharge').val(response[0]['dresscharge']);
                    $('#txttransportcharge').val(response[0]['transportcharge']);
                    $('#book_charge').val(response[0]['bookcharge']);
                    $('#adfee').val(response[0]['admissionfee']);
                    $('#sfee').val(response[0]['schoolfee']);
                    var bookcharge = $('#book_charge').val();
                    var admsn = $('#adfee').val();
                    var schfee = $("#sfee").val();
                    var finalamount = parseFloat(bookcharge) + parseFloat(admsn) + parseFloat(schfee); 
                    $('#subtotalamount').val(finalamount);
                    
                    //$this.find( '.modal-body' ).html( response );
                },
            });
            console.log(authenticateuserid+'-----'+classid);
        });
        //fee status class
        $("#newclass").change(function () {
             if ($(this).val() == '14' || $(this).val() == '15') {
                $("#stream").show();
                $("#section").hide();
                
            }else{
                $("#stream").hide();
                $("#section").show();
                
            }
        });
    });
$(function () {
        $(".dressc").click(function () {
                $(".dvPassport").toggle(500);
        });
        $(".transc").click(function () {
                $(".transcharge").toggle(500);
        });
        $(".oth").click(function () {
                $(".ocharge").toggle(500);
        });
        $(".discountamount").click(function () {
           $(".discountamounts").toggle(500);
        });
    });
    

$(document).ready(function($){
   
    /*page load totle*/
     var readytotle = addTotleAmount();
    $('#subtotalamount').val(readytotle);
    /*page load totle ends*/
    
    /*upper input change val*/
    $('.add').blur(function(){       
      checkeachcheckboxVal();
    });
    
   /*upper input change val ends*/
   
    /*check and change the dresscharge */
    function calc_charges() {
        
        var total = 0;
        
        // subtotalamount
        var subtotalamount = $( '#subtotalamount' );
        
        // book charge
        var book_charge = +$('#book_charge').val();
        
        // adfee
        var adfee = +$('#adfee').val();
        
        // sfee
        var sfee = +$('#sfee').val();
        
        total = book_charge + adfee + sfee;
        
        
        // check if the dress charge is checked
        var inputChecked_dress = $( '#inputChecked_dress' );
        if( inputChecked_dress.is( ':checked' ) ) {
            var dress_val = +$('#txtdresscharge').val();
            total = total + dress_val;
        }
        
        // check if the transport charge is checked
        var inputChecked_trans = $( '#inputChecked_trans' );
        var transcheck = $('input[name=inputRadios]').val();
        var fare =0;
        var txttransportcharge;
        if( inputChecked_trans.is( ':checked' ) ) {
            fare = +$('#inputRadioscharge').val();
            total = total + fare;
        
        }
        
        // check if the other charge is checked
        var inputChecked_other = $( '#inputChecked_other' );
        if( inputChecked_other.is( ':checked' ) ) {
            var txtothercharge = +$( '#txtothercharge' ).val();
            total = total + txtothercharge;
        }
        
        // check if the discount is checked
        var inputChecked_discount = $( '#inputChecked_discount' );
        if( inputChecked_discount.is( ':checked' ) ) {
            var txtdiscountcharge = +$( '#txtdiscountcharge' ).val();
            total = total - txtdiscountcharge;
        }
        
        // donate
        var donate = +$( '#donate' ).val();
        if( donate ) total = total + donate;
        
        subtotalamount.val( total );
        $('#subfinaltotalnew').val( total );
        $('#finalsubtotalamountcard').val( total );
    }
    $( document ).on( 'change', '.checkboxamount_d, #txtothercharge, #txtdiscountcharge, #donate, #transport,#inputRadioscharge ', calc_charges );
    $( document ).on( 'keyup', '#txtothercharge, #txtdiscountcharge, #donate', calc_charges );
    
    //paid button hide show
    
});
function checkeachcheckboxValother(){
    var total = 0;
    var upperinputTotal =  addTotleAmount();
    $('.checkboxamount').each(function(){
         $(this).val(this.checked);
        if(this.checked) { 
              var showidinput =  $(this).attr('show');
              var id = '.'+showidinput;
              $(document).find(id).css('display','block');
              var idamount = $(id).find('input').val();  
             total = parseInt(total) + parseInt(idamount);            
        }
    });
    var othercharge = $('#txtothercharge').val();
    var final = parseFloat(upperinputTotal)+parseFloat(othercharge);
   // console.log(parseInt(total));
    $('#subtotalamount').val(final);
   
}
function checkeachcheckboxValdis(){
    var total = 0;
    var upperinputTotal =  addTotleAmount();
    $('.checkboxamount').each(function(){
         $(this).val(this.checked);
        if(this.checked) { 
              var showidinput =  $(this).attr('show');
              var id = '.'+showidinput;
              $(document).find(id).css('display','block');
              var idamount = $(id).find('input').val();  
             total = parseInt(total) + parseInt(idamount);            
        }
    });
    var discount = $('#txtdiscountcharge').val();
    var final = parseFloat(upperinputTotal)-parseFloat(discount);
   // console.log(parseInt(total));
    $('#subtotalamount').val(final);
   
}


/*all inputs amounts*/
function addTotleAmount(){
    var total= 0;
    var bookcharge  = $('#book_charge').val();
    var admissionfee  = $('#adfee').val();
    var schfee  = $('#sfee').val();
    
    total = parseInt(bookcharge) + parseInt(admissionfee) + parseInt(schfee);
    //console.log(total);
    //$('#subtotalamount').val(total);
    return total;
}
/*all inputs amounts ends*/


///feestotal
function mainfee(){
    return;
    var dresscharge = 0;
    var bookcharge = ''; 
    var admsn = '';
    var schfee = '';
    var discountcharge = 0;
    $('#txtdresscharge').change(function() {
        if($(this).val()!=''){
        dresscharge = $(this).val();
        
        }
    })
    
    var transportcharge = 0;
    $('#txttransportcharge').change(function() {
        if($(this).val()!=''){
            transportcharge = $(this).val();
        }
    })
    
    var othercharge = 0;
    if($('#txtothercharge').val()!=''){
        othercharge = $('#txtothercharge').val();
    }
    
    if($('#txtdiscountcharge').val()!=''){
        discountcharge = $('#txtdiscountcharge').val();
    }
    // var bookcharge = 0;
    // if($('#book_charge').val()!=''){
    //     bookcharge = $('#book_charge').val();
    // }
    var donate = 0;
    if($('#donate').val()!=''){
        donate = $('#donate').val();
    }
    bookcharge = $('#book_charge').val();
    admsn = $('#adfee').val();
    schfee = $("#sfee").val();
    
    
    var initialtotal = parseFloat(dresscharge) + parseFloat(transportcharge);
    var ab = initialtotal + parseFloat(othercharge);
    var abc = ab - parseFloat(discountcharge);
    var abcd = abc + parseFloat(bookcharge);
    var addsc = parseFloat(admsn) + parseFloat(schfee);
    var abcde = abcd + parseFloat(addsc);
    var abdef = abcde + parseFloat(donate);
    var finaltotalamount = parseFloat(abdef);
    //console.log(abcd);
    $('#subtotalamount').val(finaltotalamount);
    $('#finalsubtotalamount').val(finaltotalamount);
    
}
/*all check box amounts totle ends*/
// $(function (){
//     $("#class").change(function () {
//      var admsn = $('#adfee').val();
//      var schfee = $("#sfee").val();
//     var dresscharge = $('#txtdresscharge').val();
//     var transportcharge = $('#txttransportcharge').val();
//     var total = parseInt(admsn)+parseInt(schfee)+parseInt(dresscharge)+parseInt(transportcharge);
//      $('#subtotalamount').val(total);
//      $('#finalsubtotalamount').val(total);
// });
// //  console.log(total);
// });
 $("#radio-btn-1").click(function() {
    $("#div1").removeAttr("style");
    $("#div1").fadeIn();
    $("#div2").fadeOut();
});
$("#radio-btn-2").click(function() {
    $("#div2").fadeIn();
    $("#div1").fadeOut();
});


function caltotal(){
    //first amount
   var first = $('#first').text();
   var afirst = $('#afirst').val();
   var totalfirst = afirst*first;
   //second amount
   var three = $('#three').text();
   var athree = $('#athree').val();
   var totalthree = three*athree;
   //four amount
   var four = $('#four').text();
   var afour = $('#afour').val();
   var totalfour = four*afour;
   //five amount
   var five = $('#five').text();
   var afive = $('#afive').val();
   var totalfive = five*afive;
   //six amount
   var six = $('#six').text();
   var asix = $('#asix').val();
   var totalsix = six*asix;
   //seven amount
   var seven = $('#seven').text();
   var aseven = $('#aseven').val();
   var totalseven = seven*aseven;
   //eight amount
   var eight = $('#eight').text();
   var aeight = $('#aeight').val();
   var totaleight = eight*aeight;
   //nine amount
   var nine = $('#nine').text();
   var anine = $('#anine').val();
   var totalnine = nine*anine;
   //ten amount
   var ten = $('#ten').text();
   var aten = $('#aten').val();
   var totalten = ten*aten;
   //eleven amount
   var eleven = $('#eleven').text();
   var aeleven = $('#aeleven').val();
   var totaleleven = eleven*aeleven;
   
   //finaltotal
   var finaltotal = totalfirst+totalthree+totalfour+totalfive+totalsix+totalseven+totaleight+totalnine+totalten+totaleleven;
   $('#finaltotal').val(finaltotal);
   var subtotalnew = $('#subfinaltotalnew').val();
   var finalsubtotalnew =  parseFloat(subtotalnew) - parseFloat(finaltotal);
   console.log('final ='+finaltotal+',subtotalnew'+subtotalnew);
   if(subtotalnew <= finaltotal){
        $('#messagefield').toggleClass('hidden');
        $('.returnamount').toggleClass('hidden');
   }else{
       console.log('bye');
       $('#returnamount').val(finalsubtotalnew);
   }
   
}



// $(document).ready(function() {
//     // $('#afirst').keyup(function() {
//     //     //first amount
//     //     var afirst = $(this).val();
//     //     var first = $('#first').text();
//     //     var totalfirst = afirst*first;
//     // });
//     console.log(totalfirst);
// });
$(function () {
        // $(".rolescheck").click(function () {
        //     if ($(this).val() == '3' || $(this).val() == '5') {
        //     }
        //     else{
        //         swal({
        //               title: "Are you sure?",
        //               text: "Your will not be able to recover this imaginary file!",
        //               type: "warning",
        //               showCancelButton: true,
        //               confirmButtonClass: "btn-danger",
        //               confirmButtonText: "Yes, delete it!",
        //               closeOnConfirm: false
        //             },
        //             function(){
        //               swal("Deleted!", "Your imaginary file has been deleted.", "success");
        //             });
        //         $(".rolescheck").prop("checked", false);
        //     }
        // })
        $(".rolescheck").click(function () {
            if ($(this).val() == '5') {
                $("#classcheck").toggle();
                $(".classcheck").toggle();
            }
        })
        $(".classnew").click(function () {
            var id = $(this).val();
            if ($(this).val() == '14' || $(this).val() == '15') {
                
                $(".streamcheck"+id).toggle();
            }
        })
    });
function confirmationmodal(){
    var studentfname = $('#studentfname').val();
    var studentmname = $('#studentmname').val();
    var studentlname = $('#studentlname').val();
    var username = $('#username').val();
    var genderselect = $('#gender').val();
    var gender;
    if(genderselect == 'M'){
        gender = 'Male';
    }else{
        gender = 'Male';
    }
    var classes = $('#class option:selected').text();
    var newsection = $('#newsection option:selected').text();
    var subjectdata = $('#subjectdata option:selected').text();
    var classwisesnewtreamdata = $('#classwisesnewtream option:selected').text();
    var classwisesnewtream;
    if(classwisesnewtreamdata == ''){
        classwisesnewtream = 'N/A';
    }else{
        classwisesnewtream = $('#classwisesnewtream option:selected').text();
    }
    
    var txtdresscharge = $('.txtdresscharge').val();
    var transportcharge = $('.tranportchargegetdata option:selected').text();
    var txtothercharge = $('#txtothercharge').val();
    var txtdiscountcharge = $('.txtdiscountcharge').val();
    var book_charge = $('#book_charge').val();
    var adfee = $('#adfee').val();
    var schfee = $('.monthlycharge').val();
    var donate = $('#donate').val();
    var subtotalamount = $('#subtotalamount').val();
    var father_name = $('#father_name').val();
    var mother_name = $('#mother_name').val();
    var father_contact = $('#father_contact').val();
    var mother_contact = $('#mother_contact').val();
    var home_tel = $('#home_tel').val();
    var useremail = $('#useremail').val();
    var countryname = $('#country_list option:selected').text();
    var statename = $('.state_list option:selected').text();
    var cityname = $('.city_list option:selected').text();
    var pin = $('#pin').val();
    var address = $('#address').val();
    var dob = $('#dob').val();
    var testfinal = $('#finaltotal').val();
    var finalamountdata = $('#subfinaltotalnew').val();
    var finalamountdatapayble = $('#finalsubtotalamountpayable').val();
    //card detail
    if($('#cardNumber').val() == ''){
        $('#oldcardowner').css('display','none');
        $('#oldcardnumber').css('display','none');
        $('#oldcardexpiration').css('display','none');
        $('#oldcardpayment').css('display','none');
    }else{
        var cardnumber = $('#cardNumber').val();
        var ownername = $('#owner').val();
        var expiration_month = $('#expiration_month option:selected').text();
        var expiration_year = $('#expiration_year option:selected').text();
    }
    
    var visacard = $('#visa').attr('alt');
    var mastercard = $('#mastercarddata').attr('alt');
    var amexcard = $('#amexcardata').attr('alt');
    var visacardvalue = $('#visacard').val();
    var mastercardvalue = $('#mastercardvalue').val();
    var amexcardvalue = $('#amexcardvalue').val();
    
    if(finalamountdatapayble == ''){
        $('#studentfinalsubtotalamountpayable').css('display','none');
    }
    var returnamountdata = $('#returnamount').val();
    var paytype = $('#paytype').val();
    //console.log(paytype);
    var paymentmode;
    if(finalamountdatapayble == ''){
        paymentmode = 'Cash';
    }else if(testfinal == '' || testfinal == 0){
        paymentmode = 'Card';
    }else{
        paymentmode = 'Cash + Card';
    }
    
    var FullName = studentfname+' '+studentmname+' '+studentlname;
    $('#studentcompletename').text(FullName);
    $('#studentusername').text(username);
    $('#studentgender').text(gender);
    $('#studentclass').text(classes);
    $('#studentsection').text(newsection);
    $('#studentsubject').text(subjectdata);
    $('#studentclasswisesnewtream').text(classwisesnewtream);
    $('#studentdresscharge').text(txtdresscharge);
    $('#studenttxtdiscountcharge').text(txtdiscountcharge);
    $('#studenttransport').text(transportcharge);
    $('#studentother').text(txtothercharge);
    $('#studentbook').text(book_charge);
    $('#studentadmision').text(adfee);
    $('#studentschoolfee').text(schfee);
    $('#studentdonation').text(donate);
    $('#studentsubtotalamount').text(subtotalamount);
    $('#studentfather_name').text(father_name);
    $('#studentmother_name').text(mother_name);
    $('#studentfather_contact').text(father_contact);
    $('#studentmother_contact').text(mother_contact);
    $('#studenthome_tel').text(home_tel);
    $('#studentuseremail').text(useremail);
    $('#studentcountryname').text(countryname);
    $('#studentstatename').text(statename);
    $('#studentcityname').text(cityname);
    $('#studentpin').text(pin);
    $('#studentaddress').text(address);
    $('#studentpaymentmode').text(paymentmode);
    $('#studentdob').text(dob);
    $('#studentfinalamountdata').text(finalamountdata);
    $('#studentreturnamountdata').text(returnamountdata);
    $('#studentcardnumber').text(cardnumber);
    $('#studentownername').text(ownername);
    $('#studentcardexpirationmonth').text(expiration_month);
    $('#studentcardexpirationyear').text(expiration_year);
    $('#studentfinalsubtotalamountpayable').text(finalamountdatapayble);
    $('#studendetailconfirmationmodal').modal('show');
}
function validateformdata(){
    
    var studentname = $('#studentfname').val();
    if(studentname === ''){
        $('.studentfname').text('Student name not empty');
        return false;
    }
    var fname = $('#fathername').val();
    if(fname === ''){
        $('.fathername').text('fathername name not empty');
        return false;
    }else{
        //$('.newdoc').removeClass('hidden');
        //$('.doc').removeAttr('disabled');
        //$('.mynext').removeAttr('disabled');
    }
}
$(function () {
        $(".rolescheckedit").click(function () {
            if ($(this).val() == '5') {
                $("#classchecknew").toggle();
            }
        })
        $(".classchecknew").click(function () {
           var id = $(this).val();
            if ($(this).val() == '14' || $(this).val() == '15') {
                
                $("#streamchecknew"+id).toggle();
            }
        });
        $('#inputCheckedteacher').change(function(){
            if( !this.checked && $('#inputCheckedclassincharge').prop( 'checked' ) ) {
                $('#inputCheckedclassincharge').click();
            }
        });
        
        $('#inputCheckedclassincharge').change(function(){
            if( this.checked && !$('#inputCheckedteacher').prop('checked') ) {
                $('#inputCheckedteacher').click();
            }
            if( !this.checked ) {
                $('.classnew').prop('checked', false);
            }
        });
        // $(".rolescheck").click(function () {
        //     var chk3 = $("input[type='checkbox'][value='3']");
        //     if ($(this).val() == '5') {
        //         swal({
        //               title: "Please Checked first teacher",
        //               text: "",
        //               type: "warning",
        //               showCancelButton: false ,
        //               confirmButtonClass: "btn-Success",
        //               confirmButtonText: "",
        //               closeOnConfirm: true
        //             }, function(){});
        //         $(".rolescheck").prop("checked", false);
        //         $("#classcheck").hide();
        //     }
        // })
        
        $(".rolescheckedit").click(function () {
            var chk3 = $("input[type='checkbox'][value='3']");
            if ($(this).val() == '5') {
                swal({
                      title: "Please Checked first teacher",
                      text: "",
                      type: "warning",
                      showCancelButton: false ,
                      confirmButtonClass: "btn-Success",
                      confirmButtonText: "",
                      closeOnConfirm: true
                    },
                    function(){
                      
                });
                $(".rolescheckedit").prop("checked", false);
                $("#classchecknew").hide();
            }
            
            if($('.rolescheckedit3').is(':checked') ){
                $("input[type='checkbox'][value='5']").prop("checked", true);
                $("#classchecknew").show();
            }
        })
    });
$(document).ready(function() {
   $(function() {

      $(".newstaff").validate({
        rules: {
          name: {
            required: true,
          },
           username: {
            required: true,
          },
           password: {
            required: true,
          },
           salary: {
            required: true,
          },
           totaexp: {
            required: true,
          },
          action: "required"
        },
        messages: {
          name: {
            required: "Please enter name",
            minlength: "Your data must be at least 8 characters"
          },
          username: {
            required: "Please enter username",
            minlength: "Your data must be at least 8 characters"
          },
          password: {
            required: "Please enter password",
            minlength: "Your data must be at least 8 characters"
          },
          salary: {
            required: "Please enter nsalaryame",
            minlength: "Your data must be at least 8 characters"
          },
          totaexp: {
            required: "Please enter totaexp",
            minlength: "Your data must be at least 8 characters"
          },
          action: "Please provide some data"
        }
      });
    });
    
    //numeric validation
    //called when key is pressed in textbox
  $(".numbervalid").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
   $(function () {

            var unsaved = false;
            $(":input").change(function () {
                unsaved = true;
            });

            $('.btnCancel').click(function () {
                //alert('hello');
                if (unsaved) {
                    //var flag = confirm("Job Function Not Saved. Are you Sure you want to leave with out saving the data?");
                    if (flag)
                        $('.mymodal').modal('hide');

                }
                else
                    $('.mymodal').modal('hide');

            });

        });
        
        $(document).ready(function(){
         $('#classselect').on('change',function(){
          
          //alert($(this).val());
          $('.newclasselect').val($(this).val());
          });
        
        $('#sectionselectid').on('change',function(){
          
          //$('.hfvalue').val();
          var classid=$('.newclasselect').val();
          var sectionid=$(this).val();
          var previousdate = $('.date-pair-select').val();
          var dateinsert = $('.newpreviousdate').val(previousdate);
          var d = new Date();
          var dd = d.getDate();
          var mm = d.getMonth() + 1; //Months are zero based
          var curr_year = d.getFullYear();
          if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
          var newdate = mm + "/" + dd + "/" + curr_year
        //   alert(newdate);
          if(newdate === previousdate){
              $('.savehide').css({'display':'block','float':'right'});
              $.ajax( {
                url: '/selfattendence/fetchattendence/',
                method: 'get',
                data:{'classid':classid,'sectionid':sectionid,'dd':dd,'mm':mm,'curr_year':curr_year,'previousdate':previousdate},
                dataType: "json",
                
                success: function(t1t) {
                 
                   //  var rrr = $.parseJSON(t1t);
                    console.log(t1t);
                    $('.classid').val(classid);
                    $('.sectionid').val(sectionid);
                    $('.studentattendence').remove();
                    $('.studentdata').remove();
                    //$('.studentattendencedata').css('display','block');
                    for(var i=0;i< t1t.length;i++)
                    {
                        //console.log(t1t[i]['sid'])
                      var userid =t1t[i]['uid'];
                      var studentid =t1t[i]['sid'];
                      var studentname =t1t[i]['uname'];
                      
                      $('.aprad').append('<tr class="odd"><td class="studentdata">'+t1t[i]['uname']+'</td><td class="studentdata"><input type="hidden" value="'+studentid+'" name="studentid[]" class="studentattendence"><input type="hidden" value="'+userid+'" name="userid[]" class="studentattendence"><div class="radio-custom radio-primary attedenceradio"><span class="spacingradio radio-success"><input type="radio" id="inputRadiosUnchecked-'+i+'" value="present" name="studentattendence-'+i+'" class="studentattendence"><label for="inputRadiosUnchecked-'+i+'">Present</label></span><span class="spacingradio radio-danger"><input type="radio" value="absent" id="inputRadiosUnchecked-'+i+'" name="studentattendence-'+i+'" class="studentattendence"><label for="inputRadiosUnchecked-'+i+'">Absent</label></span><span class="radio-warning"><input type="radio" value="leave" id="inputRadiosUnchecked-'+i+'" name="studentattendence-'+i+'" class="studentattendence"><label for="inputRadiosUnchecked-'+i+'">Leave</label></span></div></td></tr>');
                    //   $('.aprad').append('"'+t1t[i]['uname']+'"<input type="radio" value="present" class="studentattendence">');
                     }
                 
                    console.log(t1t.length);
                    //$this.find( '.modal-body' ).html( response );
                },
            } );
          }else{
              $('.savehide').css('display','none');
               $.ajax( {
                    url: '/selfattendence/fetchattendence/',
                    method: 'get',
                    
                    dataType: "json",
                    data:{'classid':classid,'sectionid':sectionid,'dd':dd,'mm':mm,'curr_year':curr_year,'previousdate':previousdate},
                    success: function(t1t) {
                   //  var rrr = $.parseJSON(t1t);
                    console.log(t1t);
                    $('.studentattendence').remove();
                    $('.studentdata').remove();
                    
                    $('.studentattendencedata').css('display','block;')
                    for(var i=0;i< t1t.length;i++)
                    {
                        //console.log(t1t[i]['sid'])
                      var userid =t1t[i]['uid'];
                      var studentid =t1t[i]['sid'];
                      var studentname =t1t[i]['uname'];
                      var status = t1t[i]['status'];
                      var attndstatus;
                      if(status === 'absent'){
                          attndstatus = '<span id="redabsent">Absent</span>';
                      }else if(status === 'present'){
                          attndstatus = '<span id="greenpresent">Present</span>';
                      }else if(status === 'leave'){
                          attndstatus = '<span id="yellowleave">Leave</span>';
                      }
                      
                      console.log(attndstatus);
                      $('.aprad').append('<tr class="odd"><td class="studentdata">'+studentname+'</td><td class="studentdata">'+attndstatus+'</td></tr>');
                    //   $('.aprad').append('"'+t1t[i]['uname']+'"<input type="radio" value="present" class="studentattendence">');
                     }
                 
                    console.log(t1t.length);
                    //$this.find( '.modal-body' ).html( response );
                },
            });
          }
           
         
          }); 
                
            $('.sectionselectid').on('change',function() {
                  var previousdate = $('.date-pair-select').val();
                  var classid=$('.newclassid').val();
                  var sectionid=$(this).val();
                 
                  console.log(previousdate);
                  console.log(classid);
                  console.log(sectionid);
            });
         });
        
        //homework
        $( '[id^=myhomeworkdetail-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var homework_id = $this.data( 'homework_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/homework/fetchhomework/' + homework_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( homework_id );
        } );
        //attendence
        $( '[id^=myattendence-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var attendence_id = $this.data( 'attendence_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/attendence/fetchattendence/' + attendence_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( attendence_id );
        } );
        
        //expense
        $( '[id^=myeditexpensess-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var expense_id = $this.data( 'expense_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/expense/fetchexpense/' + expense_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( expense_id );
        } );
        
        //exam
        $( '[id^=myexamdetail-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var exam_id = $this.data( 'exam_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/exam/examupdate/' + exam_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( exam_id );
        } );
        //busroute
        $( '[id^=myroute-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var busid = $this.data( 'busroute_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/busroute/busrouteupdate/' + busid,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( busid );
        } );
        //studentclasssection
        $( '[id^=examplePositionSectionClass-]' ).on( 'show.bs.modal', function() {
            $this = $( this );
            var studentsectionclass_id = $this.data( 'studentsectionclass_id' );
            var classsecid = $this.data( 'classsecid' );
            var schoolid = $this.data( 'schoolid' );
            console.log(studentsectionclass_id + classsecid + schoolid);
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/studentsecdeatail/stusecdetail/' + studentsectionclass_id +'/'+classsecid+'/'+schoolid,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( studentsectionclass_id );
        } );
        //student detail
        $( '[id^=examplePositionClass-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var studentclass_id = $this.data( 'studentclass_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/studentdeatail/stuget/' + studentclass_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( studentclass_id );
        } );
        
        
        //holiday
        $( '[id^=myeditholiday-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var holiday_id = $this.data( 'holiday_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/holiday/holidayupdate/' + holiday_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( holiday_id );
        } );
        
        //announce
        $( '[id^=myAnnounce-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var announce_id = $this.data( 'announce_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/announceupdate/fetchdata/' + announce_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( announce_id );
        } );
        
        //slider
        $( '[id^=mySlider-]' ).on( 'show.bs.modal', function() {
        
            $this = $( this );
            var slider_id = $this.data( 'slider_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/sliderupdate/fetchdata/' + slider_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( slider_id );
        } );
        
        //staffmember
        $( '[id^=mydetailedit-]' ).on( 'show.bs.modal', function() {
            $this = $( this );
            var staff_member_id = $this.data( 'staff_member_id' );
            $this.find( '.modal-body' ).html( 'Loading...' );
            $.ajax( {
                url: '/staffupdate/fetchdata/' + staff_member_id,
                method: 'GET',
                success: function( response ) {
                    $this.find( '.modal-body' ).html( response );
                },
            } );
            console.log( staff_member_id );
        } );
        
        $('.toggle').click(function () {
            //check if checkbox is checked
            if ($(this).is(':checked')) {
                
                $('#sendNewSms').removeAttr('disabled'); //enable input
                
            } else {
                $('#sendNewSms').attr('disabled', true); //disable input
            }
        });
    
    
        
    // $('.update').click(function(){
    //   var value=$(this).data('id');
    //     var e = $('.update').attr('title');
    //   // alert(value);

    //     var url = e+"/fetchdata/"+value;
        
    //     $.ajax({

    //           type: 'get',

    //           url: e+"/fetchdata/",

    //           data: {'id':value},

    //         })
    // });
    

});

$(document).ready(function(){
    //father
    $(".edit-icon").click(function(){
        $(".edit-data").toggleClass("hide-father");
        $(".profile-content-data").toggleClass("hide-father");
        $(".check-icon").toggleClass("hide-father");
        $(".checkdata-icon").toggleClass("hide-father");
    });
    $(".check-icon").click(function(){
        $(".edit-data").toggleClass("hide-father");
        $(".profile-content-data").toggleClass("hide-father");
        $(".check-icon").toggleClass("hide-father");
    });
    //mother
    $(".edit-icon-mother").click(function(){
        $(".edit-data-mother").toggleClass("hide-mother");
        $(".profile-content-data-mother").toggleClass("hide-mother");
        $(".checkmother-icon").toggleClass("hide-mother");
        $(".checkdata-icon").toggleClass("hide-mother");
    });
    $(".checkmother-icon").click(function(){
        $(".edit-data-mother").toggleClass("hide-mother");
        $(".profile-content-data-mother").toggleClass("hide-mother");
        $(".checkmother-icon").toggleClass("hide-mother");
    });
    //dob
    $(".edit-icon-dob").click(function(){
        $(".edit-data-dob").toggleClass("hide-dob");
        $(".profile-content-data-dob").toggleClass("hide-dob");
        $(".checkdob-icon").toggleClass("hide-dob");
        $(".checkdata-icon").toggleClass("hide-dob");
    });
    $(".checkdob-icon").click(function(){
        $(".edit-data-dob").toggleClass("hide-dob");
        $(".profile-content-data-dob").toggleClass("hide-dob");
        $(".checkdob-icon").toggleClass("hide-dob");
    });
    //contact
    $(".edit-icon-contact").click(function(){
        $(".edit-data-contact").toggleClass("hide-contact");
        $(".profile-content-data-contact").toggleClass("hide-contact");
        $(".checkcontact-icon").toggleClass("hide-contact");
        $(".checkdata-icon").toggleClass("hide-contact");
    });
    $(".checkcontact-icon").click(function(){
        $(".edit-data-contact").toggleClass("hide-contact");
        $(".profile-content-data-contact").toggleClass("hide-contact");
        $(".checkcontact-icon").toggleClass("hide-contact");
    });
    //religion
    $(".edit-icon-religion").click(function(){
        $(".edit-data-religion").toggleClass("hide-religion");
        $(".profile-content-data-religion").toggleClass("hide-religion");
        $(".checkreligion-icon").toggleClass("hide-religion");
        $(".checkdata-icon").toggleClass("hide-religion");
    });
    $(".checkreligion-icon").click(function(){
        $(".edit-data-religion").toggleClass("hide-religion");
        $(".profile-content-data-religion").toggleClass("hide-religion");
        $(".checkreligion-icon").toggleClass("hide-religion");
    });
    //aadhar
    $(".edit-icon-aadhar").click(function(){
        $(".edit-data-aadhar").toggleClass("hide-aadhar");
        $(".profile-content-data-aadhar").toggleClass("hide-aadhar");
        $(".checkaadhar-icon").toggleClass("hide-aadhar");
        $(".checkdata-icon").toggleClass("hide-aadhar");
    });
    $(".checkaadhar-icon").click(function(){
        $(".edit-data-aadhar").toggleClass("hide-aadhar");
        $(".profile-content-data-aadhar").toggleClass("hide-aadhar");
        $(".checkaadhar-icon").toggleClass("hide-aadhar");
    });
    //graduation college name
    $(".edit-icon-gcn").click(function(){
        $(".edit-data-gcn").toggleClass("hide-gcn");
        $(".profile-content-data-gcn").toggleClass("hide-gcn");
        $(".checkgcn-icon").toggleClass("hide-gcn");
        $(".checkdata-icon").toggleClass("hide-gcn");
    });
    $(".checkgcn-icon").click(function(){
        $(".edit-data-gcn").toggleClass("hide-gcn");
        $(".profile-content-data-gcn").toggleClass("hide-gcn");
        $(".checkgcn-icon").toggleClass("hide-gcn");
    });
    //last job
    $(".edit-icon-lastjob").click(function(){
        $(".edit-data-lastjob").toggleClass("hide-lastjob");
        $(".profile-content-data-lastjob").toggleClass("hide-lastjob");
        $(".checklastjob-icon").toggleClass("hide-lastjob");
        $(".checkdata-icon").toggleClass("hide-lastjob");
    });
    $(".checklastjob-icon").click(function(){
        $(".edit-data-lastjob").toggleClass("hide-lastjob");
        $(".profile-content-data-lastjob").toggleClass("hide-lastjob");
        $(".checklastjob-icon").toggleClass("hide-lastjob");
    });
    //last job designation
    $(".edit-icon-lastjobdesignation").click(function(){
        $(".edit-data-lastjobdesignation").toggleClass("hide-lastjobdesignation");
        $(".profile-content-data-lastjobdesignation").toggleClass("hide-lastjobdesignation");
        $(".checklastjobdesignation-icon").toggleClass("hide-lastjobdesignation");
        $(".checkdata-icon").toggleClass("hide-lastjobdesignation");
    });
    $(".checklastjobdesignation-icon").click(function(){
        $(".edit-data-lastjobdesignation").toggleClass("hide-lastjobdesignation");
        $(".profile-content-data-lastjobdesignation").toggleClass("hide-lastjobdesignation");
        $(".checklastjobdesignation-icon").toggleClass("hide-lastjobdesignation");
    });
    //last school leave date
    $(".edit-icon-lastschooling").click(function(){
        $(".edit-data-lastschooling").toggleClass("hide-lastschooling");
        $(".profile-content-data-lastschooling").toggleClass("hide-lastschooling");
        $(".checklastschooling-icon").toggleClass("hide-lastschooling");
        $(".checkdata-icon").toggleClass("hide-lastschooling");
    });
    $(".checklastschooling-icon").click(function(){
        $(".edit-data-lastschooling").toggleClass("hide-lastschooling");
        $(".profile-content-data-lastschooling").toggleClass("hide-lastschooling");
        $(".checklastschooling-icon").toggleClass("hide-lastschooling");
    });
    //last salary
    $(".edit-icon-salary").click(function(){
        $(".edit-data-salary").toggleClass("hide-salary");
        $(".profile-content-data-salary").toggleClass("hide-salary");
        $(".checksalary-icon").toggleClass("hide-salary");
        $(".checkdata-icon").toggleClass("hide-salary");
    });
    $(".checksalary-icon").click(function(){
        $(".edit-data-salary").toggleClass("hide-salary");
        $(".profile-content-data-salary").toggleClass("hide-salary");
        $(".checksalary-icon").toggleClass("hide-salary");
    });
    //last increment date
    $(".edit-icon-lastincrementdate").click(function(){
        $(".edit-data-lastincrementdate").toggleClass("hide-lastincrementdate");
        $(".profile-content-data-lastincrementdate").toggleClass("hide-lastincrementdate");
        $(".checklastincrementdate-icon").toggleClass("hide-lastincrementdate");
        $(".checkdata-icon").toggleClass("hide-lastincrementdate");
    });
    $(".checklastincrementdate-icon").click(function(){
        $(".edit-data-lastincrementdate").toggleClass("hide-lastincrementdate");
        $(".profile-content-data-lastincrementdate").toggleClass("hide-lastincrementdate");
        $(".checklastincrementdate-icon").toggleClass("hide-lastincrementdate");
    });
    //last increment amount
    $(".edit-icon-lastamount").click(function(){
        $(".edit-data-lastamount").toggleClass("hide-lastamount");
        $(".profile-content-data-lastamount").toggleClass("hide-lastamount");
        $(".checklastamount-icon").toggleClass("hide-lastamount");
        $(".checkdata-icon").toggleClass("hide-lastamount");
    });
    $(".checklastamount-icon").click(function(){
        $(".edit-data-lastamount").toggleClass("hide-lastamount");
        $(".profile-content-data-lastamount").toggleClass("hide-lastamount");
        $(".checklastamount-icon").toggleClass("hide-lastamount");
    });
    //facebook
    $(".edit-icon-facebook").click(function(){
        $(".edit-data-facebook").toggleClass("hide-facebook");
        $(".profile-content-data-facebook").toggleClass("hide-facebook");
        $(".checkfacebook-icon").toggleClass("hide-facebook");
       // $(".checkdata-icon").toggleClass("hide-facebook");
    });
    $(".checkfacebook-icon").click(function(){
        $(".edit-data-facebook").toggleClass("hide-facebook");
        $(".profile-content-data-facebook").toggleClass("hide-facebook");
        $(".checkfacebook-icon").toggleClass("hide-facebook");
    });
    //Instagram
    $(".edit-icon-Instagram").click(function(){
        $(".edit-data-Instagram").toggleClass("hide-Instagram");
        $(".profile-content-data-Instagram").toggleClass("hide-Instagram");
        $(".checkInstagram-icon").toggleClass("hide-Instagram");
        //$(".checkdata-icon").toggleClass("hide-Instagram");
    });
    $(".checkInstagram-icon").click(function(){
        $(".edit-data-Instagram").toggleClass("hide-Instagram");
        $(".profile-content-data-Instagram").toggleClass("hide-Instagram");
        $(".checkInstagram-icon").toggleClass("hide-Instagram");
    });
    //twitter
    $(".edit-icon-tweet").click(function(){
        $(".edit-data-tweet").toggleClass("hide-tweet");
        $(".profile-content-data-tweet").toggleClass("hide-tweet");
        $(".checktweet-icon").toggleClass("hide-tweet");
        //$(".checkdata-icon").toggleClass("hide-Instagram");
    });
    $(".checktweet-icon").click(function(){
        $(".edit-data-tweet").toggleClass("hide-tweet");
        $(".profile-content-data-tweet").toggleClass("hide-tweet");
        $(".checktweet-icon").toggleClass("hide-tweet");
    });
    //name
    $(".edit-icon-name").click(function(){
        $(".edit-data-name").toggleClass("hide-name");
        $(".profile-content-data-name").toggleClass("hide-name");
        $(".checkname-icon").toggleClass("hide-name");
        $(".checkdata-icon").toggleClass("hide-name");
    });
    $(".checkname-icon").click(function(){
        $(".edit-data-name").toggleClass("hide-name");
        $(".profile-content-data-name").toggleClass("hide-name");
        $(".checkname-icon").toggleClass("hide-name");
    });
    //username
    $(".edit-icon-username").click(function(){
        $(".edit-data-username").toggleClass("hide-username");
        $(".profile-content-data-username").toggleClass("hide-username");
        $(".checkusername-icon").toggleClass("hide-username");
        $(".checkdata-icon").toggleClass("hide-username");
    });
    $(".checkusername-icon").click(function(){
        $(".edit-data-username").toggleClass("hide-username");
        $(".profile-content-data-username").toggleClass("hide-username");
        $(".checkusername-icon").toggleClass("hide-username");
    });
    //email
    $(".edit-icon-email").click(function(){
        $(".edit-data-email").toggleClass("hide-email");
        $(".profile-content-data-email").toggleClass("hide-email");
        $(".checkemail-icon").toggleClass("hide-email");
        $(".checkdata-icon").toggleClass("hide-email");
    });
    $(".checkemail-icon").click(function(){
        $(".edit-data-email").toggleClass("hide-email");
        $(".profile-content-data-email").toggleClass("hide-email");
        $(".checkemail-icon").toggleClass("hide-email");
    });
    //password
    $(".edit-icon-password").click(function(){
        $('.changeconfirmpassword').css('display','block');
        $(".edit-data-password").toggleClass("hide-password");
        $(".profile-content-data-password").toggleClass("hide-password");
        $(".checkpassword-icon").toggleClass("hide-password");
        $(".checkdata-icon").toggleClass("hide-password");
    });
    $(".checkpassword-icon").click(function(){
        $(".edit-data-password").toggleClass("hide-password");
        $(".profile-content-data-password").toggleClass("hide-password");
        $(".checkpassword-icon").toggleClass("hide-password");
    });
    //confirm password
    $(".edit-icon-newconfirmpassword").click(function(){
        $(".edit-data-newconfirmpassword").toggleClass("hide-newconfirmpassword");
        $(".profile-content-data-newconfirmpassword").toggleClass("hide-newconfirmpassword");
        $(".checknewconfirmpassword-icon").toggleClass("hide-newconfirmpassword");
        $(".checkdata-icon").toggleClass("hide-newconfirmpassword");
    });
    $(".checknewconfirmpassword-icon").click(function(){
        $(".edit-data-password").toggleClass("hide-newconfirmpassword");
        $(".profile-content-data-newconfirmpassword").toggleClass("hide-newconfirmpassword");
        $(".checknewconfirmpassword-icon").toggleClass("hide-newconfirmpassword");
    });
    //old password
    $(".edit-icon-oldpassword").click(function(){
        $(".edit-data-oldpassword").toggleClass("hide-oldpassword");
        $(".profile-content-data-oldpassword").toggleClass("hide-oldpassword");
        $(".checkoldpassword-icon").toggleClass("hide-oldpassword");
        $(".checkdata-icon").toggleClass("hide-oldpassword");
    });
    $(".checkoldpassword-icon").click(function(){
        $(".edit-data-oldpassword").toggleClass("hide-oldpassword");
        $(".profile-content-data-oldpassword").toggleClass("hide-oldpassword");
        $(".checkoldpassword-icon").toggleClass("hide-oldpassword");
    });
    
    
   
    //father_name change
    $('.newchangedata').click(function() {
      var data = $('.changefather').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'father_name':data},
                success: function( response ) {
                    console.log(response['father_name']);
                    $( '.changefather' ).val(response['father_name']).html( response['father_name'] );
                    $('.newfather').html(response['father_name']);
                },
        } );
    });
    //Mother_name change
    $('.newchangedatamother').click(function() {
      var data = $('.changemother').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'mother_name':data},
                success: function( response ) {
                    console.log(response['mother_name']);
                    $( '.changemother' ).val(response['mother_name']).html( response['mother_name'] );
                    $('.newmother').html(response['mother_name']);
                },
        } );
    });
    //contact number
    $('.newchangedatacontact').click(function() {
      var data = $('.changecontact').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'contact_number':data},
                success: function( response ) {
                    console.log(response['contact_number']);
                    $( '.changecontact' ).val(response['contact_number']).html( response['contact_number'] );
                    $('.newcontact').html(response['contact_number']);
                },
        } );
    });
    //dob
    $('.newchangedatadob').click(function() {
      var data = $('.changedob').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'dob':data},
                success: function( response ) {
                    console.log(response['dob']);
                    $( '.changedob' ).val(response['dob']).html( response['dob'] );
                    $('.newdob').html(response['dob']);
                },
        } );
    });
    //aadhar card
    $('.newchangedataaadhar').click(function() {
      var data = $('.changeaadhar').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'adhar_card_number':data},
                success: function( response ) {
                    console.log(response['adhar_card_number']);
                    $( '.changeaadhar' ).val(response['adhar_card_number']).html( response['adhar_card_number'] );
                    $('.newaadhar').html(response['adhar_card_number']);
                },
        } );
    });
    //religion
    $('.newchangedatareligion').click(function() {
      var data = $('.changereligion').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'religion':data},
                success: function( response ) {
                    console.log(response['religion']);
                    $( '.changereligion' ).val(response['religion']).html( response['religion'] );
                    $('.newreligion').html(response['religion']);
                },
        } );
    });
    //graduation college name
    $('.newchangedatagcn').click(function() {
      var data = $('.changegcn').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'graduation_college_name':data},
                success: function( response ) {
                    console.log(response['graduation_college_name']);
                    $( '.changegcn' ).val(response['graduation_college_name']).html( response['graduation_college_name'] );
                    $('.newgcn').html(response['graduation_college_name']);
                },
        } );
    });
    //last job
    $('.newchangedatalastjob').click(function() {
      var data = $('.changelastjob').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'last_job':data},
                success: function( response ) {
                    console.log(response['last_job']);
                    $( '.changelastjob' ).val(response['last_job']).html( response['last_job'] );
                    $('.newlastjob').html(response['last_job']);
                },
        } );
    });
    //last job designation
    $('.newchangedatalastjobdesignation').click(function() {
      var data = $('.changelastjobdesignation').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'last_job_designation':data},
                success: function( response ) {
                    console.log(response['last_job_designation']);
                    $( '.changelastjobdesignation' ).val(response['last_job_designation']).html( response['last_job_designation'] );
                    $('.newlastjobdesignation').html(response['last_job_designation']);
                },
        } );
    });
    //last school leave date
    $('.newchangedatalastschooling').click(function() {
      var data = $('.changelastschooling').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'school_Leaving_date':data},
                success: function( response ) {
                    console.log(response['school_Leaving_date']);
                    $( '.changelastschooling' ).val(response['school_Leaving_date']).html( response['school_Leaving_date'] );
                    $('.newlastschooling').html(response['school_Leaving_date']);
                },
        } );
    });
    //last salary
    $('.newchangedatasalary').click(function() {
      var data = $('.changesalary').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'salary':data},
                success: function( response ) {
                    console.log(response['salary']);
                    $( '.changesalary' ).val(response['salary']).html( response['salary'] );
                    $('.newsalary').html(response['salary']);
                },
        } );
    });
    //last increment date
    $('.newchangedatalastincrementdate').click(function() {
      var data = $('.changelastincrementdate').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'last_increment_date':data},
                success: function( response ) {
                    console.log(response['last_increment_date']);
                    $( '.changelastincrementdate' ).val(response['last_increment_date']).html( response['last_increment_date'] );
                    $('.newlastincrementdate').html(response['last_increment_date']);
                },
        } );
    });
    //last increment amount
    $('.newchangedatalastamount').click(function() {
      var data = $('.changelastamount').val();
      var teacher = $('.teacherdata').val();
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'last_increment_amount':data},
                success: function( response ) {
                    console.log(response['last_increment_amount']);
                    $( '.changelastamount' ).val(response['last_increment_amount']).html( response['last_increment_amount'] );
                    $('.newlastamount').html(response['last_increment_amount']);
                },
        } );
    });
    //facebook
    $('.newchangedatafacebook').click(function() {
      var data = $('.changefacebook').val();
      var teacher = $('#teachernewid').val();
      //alert(teacher);
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'facebook':data},
                success: function( response ) {
                    console.log(response['facebook']);
                    $( '.changefacebook' ).val(response['facebook']).html( response['facebook'] );
                    $('.newfacebook').html(response['facebook']);
                },
        } );
    });
    //instagram
    $('.newchangedataInstagram').click(function() {
      var data = $('.changeInstagram').val();
      var teacher = $('#teachernewid').val();
      //alert(teacher);
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'instagram':data},
                success: function( response ) {
                    console.log(response['instagram']);
                    $( '.changeInstagram' ).val(response['instagram']).html( response['instagram'] );
                    $('.newInstagram').html(response['instagram']);
                },
        } );
    });
     //twitter
    $('.newchangedatatweet').click(function() {
      var data = $('.changetweet').val();
      var teacher = $('#teachernewid').val();
      //alert(teacher);
      $.ajax( {
                url: '/profileupdate/updatedata/',
                method: 'get',
                data:{'teacherid':teacher,'twitter':data},
                success: function( response ) {
                    console.log(response['twitter']);
                    $( '.changetweet' ).val(response['twitter']).html( response['twitter'] );
                    $('.newtweet').html(response['twitter']);
                },
        } );
    });
    //name
    $('.newchangedataname').click(function() {
      var data = $('.changefirstname').val();
      var userid = $('#userid').val();
      $.ajax( {
                url: '/userprofileupdate/updatedata/',
                method: 'get',
                data:{'userid':userid,'name':data},
                success: function( response ) {
                    console.log(response['name']);
                    $( '.changefirstname' ).val(response['name']).html( response['name'] );
                    $('.newname').html(response['name']);
                },
        } );
    });
    //username
    $('.newchangedatausername').click(function() {
      var data = $('.changeusername').val();
      var userid = $('#userid').val();
      $.ajax( {
                url: '/userprofileupdate/updatedata/',
                method: 'get',
                data:{'userid':userid,'username':data},
                success: function( response ) {
                    console.log(response['username']);
                    $( '.changeusername' ).val(response['username']).html( response['username'] );
                    $('.newusername').html(response['username']);
                },
        } );
    });
    //email
    $('.newchangedataemail').click(function() {
      var data = $('.changeemail').val();
      var userid = $('#userid').val();
      $.ajax( {
                url: '/userprofileupdate/updatedata/',
                method: 'get',
                data:{'userid':userid,'email':data},
                success: function( response ) {
                    console.log(response['email']);
                    $( '.changeemail' ).val(response['email']).html( response['email'] );
                    $('.newemail').html(response['email']);
                },
        } );
    });
    //change password
    $('.newchangedatanewconfirmpassword').click(function() {
      var datamainpass = $('.changepassword').val();
      var datachangepassword = $('.changenewconfirmpassword').val();
      console.log('mainpass'+datamainpass+'changepass'+datachangepassword)
      var userid = $('#userid').val();
      $.ajax( {
                url: '/userprofileupdate/updatedata/',
                method: 'get',
                data:{'userid':userid,'changepassnew':datamainpass},
                success: function( response ) {
                    console.log(response);
                    $('.changepasswordsuccess').css('display','none');
                    $('.passtextbox').css('display','none');
                    $('.newpasswordchangesucces').html('Password has been changed');
                },
        } );
    });
     //old password
    $('.newchangedataoldpassword').click(function() {
      var dataoldpass = $('.changeoldpassword').val();
      var userid = $('#userid').val();
      $.ajax( {
                url: '/userprofileupdate/passwordcheck/',
                method: 'get',
                data:{'userid':userid,'oldpass':dataoldpass},
                success: function( response ) {
                    console.log(response);
                    if(response == 1){
                        $('.authchangepassword').css('display','block');
                        $('.authchangepassword').css('display','block');
                        $('.checkpassold').css('display','none');
                    }else if(response == 0){
                        $('.newoldpassword').html('Your Password Not match');
                    }
                    //$( '.changeemail' ).val(response['dataoldpass']).html( response['dataoldpass'] );
                    
                },
        } );
    });
    
    $(function() {
        $('.selectpicker').selectpicker();
    });
    
});
$('.mysectionnchcking').change(function(){
    console.log('hello');
    //e.preventDefault();
    var $this = $(this);
    
    var parent_row = $this.closest( '.margin-bottom' );
    if( $this.is( ':checked' ) ) {
        parent_row.find( '.class_check' ).prop( 'checked', true );
        return;
    }
    var any_checked = false;
    parent_row.find( '.sectioncheckedfirst' ).each( function() {
        if( $(this).is( ':checked' ) ) {
            any_checked = true;
        }
    } );
    if( !any_checked ) {
        parent_row.find( '.class_check' ).prop( 'checked', false );
    }
});
$('.admissionclass').change(function(){
    var id = $(this).val();
        if(id == '14' || id == '15'){
        $.ajax({
            
            url: '/classstream',
            method: 'get',
            data:{'classid':id},
            success: function( response ) {
                console.log(response);
                // console.log(response[0]['sid']);
                $('.classwisestream').empty();
                $('.classwisestream').append('<option value="0">--Select--</option>');
                for(var i=0;i< response.length;i++)
                {
                    $('.classwisestream').append('<option value="'+response[i]['hid']+'" data-newstream="'+response[i]['highsubname']+'">'+response[i]['highsubname']+'</option>');    
                }
            },
        });
        
    }else{
    $.ajax({
            
            url: '/sectionclass',
            method: 'get',
            data:{'classid':id},
            success: function( response ) {
                console.log(response);
                // console.log(response[0]['sid']);
                $('.classwidesection').empty();
                $('.classwidesection').append('<option value="0">--Select--</option>');
                for(var i=0;i< response.length;i++)
                {
                    $('.classwidesection').append('<option data-secname="'+response[i]['section_display_name']+'" value="'+response[i]['sid']+'">'+response[i]['section_display_name']+'</option>');    
                }
            },
        });
    }
});
$('.studentclass').change(function(){
    var classid = $(this).val();
    //alert(classid);
    if(classid == '14' || classid == '15'){
        $('.studentsubjectnew').hide();
        $('.stream').show();
        
    }else{
        $('.studentsubjectnew').show();
        $('.stream').hide();
    }
});

$('.sectionnewclasschecked').change(function(){
    var classid = $(this).val();
    var $this = $(this);
    var newclassid = $('.classnewcheck-'+classid);
    var parent_row = $this.closest( '.margin-bottom' );
    var any_checked = false;
    $('.class_check').each( function() {
                if( $(this).is( ':checked' ) ) {
                    $(newclassid).prop( 'checked', true );
                }
            } );
    //$(newclassid).parent_row.find( '.class_check' ).prop( 'checked', true );
    console.log(newclassid);
});
$(document).ajaxError(function(event, jqxhr, settings, exception) {

    if (exception == 'Unauthorized') {

        // Prompt user if they'd like to be redirected to the login page
        bootbox.confirm("Your session has expired. Would you like to be redirected to the login page?", function(result) {
            if (result) {
                window.location = '/mainpage';
            }
        });

    }
});
//first name
function onlyAlphabetsFname(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)){
        
            return true;
        }
        else{
            $('.studentfname').html('Please Enter Charecter only');
            return false;
        }
            
    }
    catch (err) {
        
        alert(err.Description);
    }
}
//Middle name
function onlyAlphabetsMname(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)){
        
            return true;
        }
        else{
            $('.studentmname').html('Please Enter Charecter only');
            return false;
        }
            
    }
    catch (err) {
        
        alert(err.Description);
    }
}
//last name
function onlyAlphabetslname(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)){
        
            return true;
        }
        else{
            $('.studentlname').html('Please Enter Charecter only');
            return false;
        }
            
    }
    catch (err) {
        
        alert(err.Description);
    }
}
//map address search
$( document ).ready(function() {
 var input = document.getElementById('address');
 var autocomplete = new google.maps.places.Autocomplete(input);
});
