@extends('layouts.principaltest.newapp')

@section('content')
 <?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;

?>
<style>
    #__lpform_fname{
    display:none !important;
}
</style>
<div class="">
    <div class="page-header">
        <h1 class="page-title">{{$firstparam}}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
        </ol>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <!-- Panel Wizard Form Container -->
                <div class="panel" id="exampleWizardForm">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$secondparam}}</h3>
                    </div>
                    <div class="panel-body">
                        <!-- Steps -->
                        <div class="pearls row">
                            <div class="pearl current col-2">
                                <div class="pearl-icon">
                                    <i class="fa fa-key" aria-hidden="true"></i>
                                </div>
                                <span class="pearl-title">Set Password</span>
                            </div>
                            <div class="pearl col-2">
                                <div class="pearl-icon">
                                    <i class="icon md-card" aria-hidden="true"></i>
                                </div>
                                <span class="pearl-title">Personal Information</span>
                            </div>
                            <div class="pearl col-2">
                                <div class="pearl-icon">
                                    <i class="icon md-check" aria-hidden="true"></i>
                                </div>
                                <span class="pearl-title">Academic Information</span>
                            </div>
                            <div class="pearl col-2">
                                <div class="pearl-icon">
                                    <i class="icon md-check" aria-hidden="true"></i>
                                </div>
                                <span class="pearl-title">Resdential Information</span>
                            </div>
                            <div class="pearl col-2">
                                <div class="pearl-icon">
                                    <i class="icon md-check" aria-hidden="true"></i>
                                </div>
                                <span class="pearl-title">Job Experience</span>
                            </div>
                        </div>
                        <!--End Steps-->
                        <!-- Wizard Content -->
                        <form id="exampleFormContainer" action="{{url('/teacherprofile')}}" method="POST" enctype="multipart/form-data">
                            <div class="wizard-content">
                                <div class="wizard-pane active" role="tabpanel">
                                    <div class="form-group form-material">
                                    <label class="form-control-label" for="inputUserNameOne">Password</label>
                                        <input type="hidden" class="form-control" name="staffid" value="{{$staff->id}}" required="required">
                                        <input type="hidden" class="form-control" name="userid" value="{{$userid->id}}" required="required">
                                        <input type="password" class="form-control" name="password" id="password" value="{{ old('password')}}" onkeyup="checkPass(); return true;" required="required">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @else
                                            <span id="messagelength"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                    <label class="form-control-label" for="inputUserNameOne">Confirm Password</label>
                                        <input type="password" class="form-control" name="confirmpassword" id="confirm_password" value="{{ old('confirmpassword')}}"   required="required">
                                        <span id="message"></span>
                                    </div>
                                </div>
                                <div class="wizard-pane" id="exampleGettingOne" role="tabpanel">
                                <h3 class="block">Provide your Personal details</h3>
                                    <div class="form-group form-material">
                                    <label class="form-control-label" for="inputUserNameOne">Username</label>
                                        <input type="text" class="form-control" name="username" value="{{ old('username') }}" required="required">
                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"> Provide your User Name </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                    <label class="form-control-label" for="inputUserNameOne">Email</label>
                                        <input type="text" class="form-control" name="email" value="{{ old('email') }}" required="required">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"> Provide your Email </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                    <label class="form-control-label" for="inputUserNameOne">Religion</label>
                                        <input type="text" class="form-control" name="religion"  required="required">
                                        @if ($errors->has('religion'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('religion') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"> Provide your religion </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="form-control-label" for="inputUserNameOne">Gender</label>
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="inputRadiosUnchecked" name="gender" value="M" />
                                            <label for="inputRadiosUnchecked">Male</label>
                                        </div>
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="inputRadiosUnchecked" name="gender" value="F" />
                                            <label for="inputRadiosUnchecked">Female</label>
                                        </div>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('gender') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"> Provide your Gender </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="form-control-label" for="inputUserNameOne">Martial Status</label>
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="inputRadiosUnchecked" name="martial_status" value="married" />
                                            <label for="inputRadiosUnchecked">Married</label>
                                        </div>
                                        <div class="radio-custom radio-primary">
                                            <input type="radio" id="inputRadiosUnchecked" name="martial_status" value="unmarried" />
                                            <label for="inputRadiosUnchecked">Unmarried</label>
                                        </div>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('gender') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"> Provide your Gender </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                    <label class="form-control-label" for="inputUserNameOne">Aadhar Number</label>
                                        <input type="text" class="form-control" name="aadhar_num" required="required">
                                        @if ($errors->has('aadhar_num'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('aadhar_num') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"> Provide your Adhar Number </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Upload Aadhar Card
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="file" class="form-control" name="aadharfirst" required/>
                                        <input type="file" class="form-control" name="aadharsecond" required/>
                                        @if ($errors->has('aadharfirst') || $errors->has('aadharsecond'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('aadharfirst') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your aadhar card detail</span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Father Name
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="father_name" />
                                        @if ($errors->has('father_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('father_name') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your father Name</span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Mother Name
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="mother_name" />
                                         @if ($errors->has('mother_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('mother_name') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your Mother Name</span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Date of Birth
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="datepair-wrap" data-plugin="datepair">
                                            <div class="input-daterange-wrap">
                                                <div class="input-daterange">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                                        </span>
                                                        <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="dob">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('dob'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dob') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your date of birth </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Contact Number
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="contact" />
                                        @if ($errors->has('contact'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('contact') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your contact number </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Alternate Number
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="alternate_number" />
                                        @if ($errors->has('alternate_number'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('alternate_number') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your Alternate Number </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="wizard-pane" id="exampleBillingtwo" role="tabpanel">
                                    <h3 class="block">Academic Information</h3>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Graduation College Name
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="college_name" />
                                        @if ($errors->has('college_name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('college_name') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your Graduation College Name </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Graduation Percentage
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="graduation_percentage" />
                                        @if ($errors->has('graduation_percentage'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('graduation_percentage') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your Graduation Percentage </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="wizard-pane" id="exampleBillingtwo" role="tabpanel">
                                    <h3 class="block">Residential Information</h3>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Address 1
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="address1" />
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Address 2
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="address2" />
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">State
                                            <span class="required"> * </span>
                                        </label>
                                        <select class="form-control" name="state">
                                            @foreach($state as $states)
                                                <option value="{{$states->id}}">{{$states->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">City
                                            <span class="required"> * </span>
                                        </label>
                                        <select class="form-control" name="state">
                                            @foreach($city as $cities)
                                                <option value="{{$cities->id}}">{{$cities->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Pin code
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="pin_code" />
                                    </div>
                                </div>
                                <div class="wizard-pane" id="exampleBillingtwo" role="tabpanel">
                                    <h3 class="block">Job Experience</h3>
                                    <div class="form-group form-material">
                                        <label class="form-control-label">Last School
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="last_school" required/>
                                        @if ($errors->has('last_school'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_school') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your last school </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="form-control-label">Last School Designation
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="last_school_designation" required/>
                                        @if ($errors->has('last_school_designation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_school_designation') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your last school designation</span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="form-control-label">Last Salary
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="last_salary" required/>
                                        @if ($errors->has('last_salary'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_salary') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your last school designation</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Last Job Reliaving Certificate
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="file" class="form-control" name="exp_certificate" required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Two Month Salary Slip
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="file" class="form-control" name="salary1" required/>
                                        <input type="file" class="form-control" name="salary2" required/>
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">School Leave Date
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="datepair-wrap" data-plugin="datepair">
                                            <div class="input-daterange-wrap">
                                                <div class="input-daterange">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                                        </span>
                                                        <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="school_Leaving_date">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('school_Leaving_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('school_Leaving_date') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your School Leave Date </span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                        <label class="control-label col-md-3">Last Increment Date
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="datepair-wrap" data-plugin="datepair">
                                            <div class="input-daterange-wrap">
                                                <div class="input-daterange">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                                        </span>
                                                        <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="last_increment_date">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($errors->has('dob'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dob') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"> Provide your Last Increment Date </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Last Increment Amount
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="last_increment" required/>
                                    </div>
                                    <div class="form-group form-material">
                                        <div class="checkbox-custom checkbox-primary">
                                          <input type="checkbox" id="inputUnchecked" class="toggle">
                                          <label for="inputUnchecked">Please Check all information is Correct</label>
                                        </div>
                                    </div>
                                    <button type="submit" id="sendNewSms" class="btn btn-success btn-outline float-right" data-wizard="finish">Submit</button>
                                </div>
                            </div>
                            <!---->
                        </form>
                        <!--End Wizard Content-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection