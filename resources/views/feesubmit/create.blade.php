@extends('layouts.principaltest.student')

@section('content')
  <?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;

?>
<style>
    #__lpform_fname{
    display:none !important;
}
</style>
<!-- Page -->
<div class="page">
    <div class="page-header">
         <div class="page-header">
        <h1 class="page-title">{{$firstparam}}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
        </ol>
    </div>
    </div>

    <div class="page-content container-fluid">
         @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif
        <!-- Add Feesubmission Form -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="panel-desc"></span>
                </h3>
            </div>
            <div class="panel-body">

                <form id="feesubmission" autocomplete="off" method ="post" action="{{ action('FeeSubmissionController@store') }}" enctype="multipart/form-data">
                    <div class="row row-lg">
                        <div class="form-group form-material col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="select2" name="clas" id="class" class="form-control" data-live-search="true" required="">
                                            <option value="">--Select Class---</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                      <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="section">
                                    <label class="control-label" for="inputUserNameOne">Section</label>
                                        <select data-plugin="select2" name="section" class="form-control" id="newsection">
                                            <option>--Select Section--</option>
                                               
                                        </select>
                                        @if ($errors->has('section'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('section') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                    
                                    <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="student_list_">
                                    <label class="control-label" for="inputUserNameOne">Student</label>
                                    <input type="hidden" name="fee_id" value="" id="fe_id">
                                        <select data-plugin="select2" name="student_lst" class="form-control" id="student_list">
                                            <option value="">--Select Student--</option>
                                               
                                        </select>
                                        @if ($errors->has('student_list'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('student_list') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material submission_months col-lg-4 col-md-4 col-sm-4 col-xs-12" id="submission_months" style="display: none">
                                    <label class="form-control-label" for="inputUserNameOne">Submission Months</label>
                                        <select data-plugin="select2" multiple name="mondata[]" id="submis_mon" class="form-control mondata" data-live-search="true">
                                            <option value="">--Select Subject--</option>
                                                @foreach($month as $months)
                                                    <option value="{{$months->monthname}}" data-monthname="{{$months->monthname}}" data-id="{{$months->id}}">{{$months->monthname}}</option>
                                                    <?php $months = $months->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                  <div class=" col-xl-12 col-md-9" id="fee_months" style="display:none;">
                                  <label for="inputChecked_pend">Pending Months
                                        <!--input type="text" id="pending_months" class="form-control disabled" value="" readonly-->
                                                                                 <!--label for="inputChecked_sub">Submission Months-->
                                 <input type="hidden" class="form-control" name="count_pend" id="count_pend1" value="">
                                 <input type="hidden" class="form-control" name="pen_data" id="count_pend1" value="">
                                 <input type="hidden" class="form-control" name="sub_data" id="count_pend1" value="">
                                 <input type="hidden" class="form-control" name="paid_m" id="count_pend1" value="">
                                 <input type="hidden" class="form-control" name="pend_data" id="count_pend1" value="">
                                 <input type="hidden" class="form-control" id="" name="total_fees" value="" readonly/>
                                 <input type="hidden" class="form-control" id="" name="amount" value="" readonly/>



                                 <select data-plugin="select2" multiple name="" id="pending_months" class="form-control mondata" data-live-search="true">
                                                
                                        </select>
                                 </div>
                                    </div>
                                    <div class="form-group form-material submission_months col-lg-4 col-md-4 col-sm-4 col-xs-12" id="sub_mont" style="display: none">
                                    <label class="form-control-label" for="inputUserNameOne">Submission Months</label>
                                        <select data-plugin="select2" multiple name="mondata[]" id="submis_mon1" class="form-control mondata" data-live-search="true">
                                            <option value="">--Select Month--</option>
                                               
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-md-checkboxes col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                        <label class="control-label">Charges</label>
                                        <div class="col-md-12 md-checkbox-inline">
                                            <div class="row">
                                                <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                  <input type="checkbox" id="inputChecked_pending" class="pendingc checkboxamount_d" show="amount1" name="pending_c" value="1" data-title="Pending Month Charge apply."/>
                                                  <label for="inputChecked_pendin">Pending Month
                                                  <div class="dvPassport1 amount1">
                                                            <div class="input-group admscharges ">
                                                                <span class="input-group-addon">
                                                                    &#8377;
                                                                </span>
                                                                <input type="text" class="form-control txtpendcharge" id="txtpendcharge" name="pendingcharge" value="" readonly/>

                                                            </div>
                                                        </div>
                                                  </label>
                                                </div>
                                                <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                  <input type="checkbox" id="inputChecked_dress" class="dressc checkboxamount_d" show="amount1" name="dress_charge" value="1" data-title="Dress Charge apply."/>
                                                  <label for="inputChecked_dress">Dress Charge
                                                  <div class="dvPassport amount1">
                                                            <div class="input-group admscharges ">
                                                                <span class="input-group-addon">
                                                                    &#8377;
                                                                </span>
                                                                <input type="text" class="form-control txtdresscharge" id="txtdresscharge" name="dresscharge" value="" readonly/>
                                                            </div>
                                                        </div>
                                                  </label>
                                                </div>
                                                <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                  <input type="checkbox" id="inputChecked_trans" class="transc checkboxamount_d" show="amount2" name="©_charge" onclick="" value="1" data-title="Transport Charge apply."/>
                                                  <label for="inputChecked_trans">Transport Charge
                                                  <div class="transcharge amount2">
                                                            <div class="input-group admscharges ">
                                                                <!--<span class="input-group-addon">-->
                                                                <!--    &#8377;-->
                                                                <!--</span>-->
                                                                <select data-plugin="selectpicker" name="inputRadioscharge" id="inputRadioscharge" class="form-control tranportchargegetdata" data-live-search="true">
                                                                <option value="0">--Select Route--</option>
                                                                @foreach($busroute as $busroutes)
                                                                    <option value="{{$busroutes->fare}}">{{$busroutes->routename}}(&#8377; {{$busroutes->fare}})</option>
                                                                @endforeach
                                                                </select>
                                                               
                                                            </div>
                                                        </div>
                                                  </label>
                                                </div>
                                                <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                  <input type="checkbox" id="inputChecked_other" class="oth checkboxamount_d" show="amount3" name="other_charge" value="1" data-title="Other Charge apply." onclick=""/>
                                                  <label for="inputChecked_other">Other Charge</label>
                                                  <div class="ocharge">
                                                            <div class="input-group admscharges ">
                                                                <span class="input-group-addon">
                                                                    &#8377;
                                                                </span>
                                                                <input type="number" min="1" class="form-control" id="txtothercharge" name="othercharge" value="1" />
                                                            </div>
                                                            <div class="input-group admscharges">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-envelope"></i>
                                                                </span>
                                                                <input type="text" class="form-control" id="txtothercharge" name="remark" placeholder="Remark"/>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <input type="checkbox" id="inputChecked_discount" show="amount4" class="discountamount checkboxamount_d" name="discount" value="1" data-title="Discount Charge apply." id="discount" onclick="">
                                                <label for="inputChecked_discount">Discount Charge</label>
                                                <div class="discountamounts">
                                                        <div class="input-group admscharges">
                                                            <span class="input-group-addon">
                                                                &#8377;
                                                            </span>
                                                            <input type="number" min="1" class="form-control txtdiscountcharge" id="txtdiscountcharge" name="discountcharge" value="1"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="schfee" style="display: none">
                                                    <label class="control-label">Monthly Charges</label>
                                                    <input type="text" class="form-control add monthlycharge" id="sfee" name="schfee" value="" readonly/>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="subtotal" style="display: none">
                                                        <label class="control-label">Subtotal Amount
                                                        <span class="required"> * </span>
                                                    </label>
                                                        <input type="text" class="form-control" name="subtotal" id="subtotalamount" value="" readonly/>
                                                        <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                    
                                                </div>
                                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="radio-custom radio-primary col-lg-3">
                                            <input type="radio" id="" name="pay" value="Y">
                                            <label for="inputYes">Cash</label>
                                            <span class="required"> * </span>
                                                <div id="cash" style="display:none;">  
                                               Cash Amount:<input type="text" name="cash_amount" required>
                                            </div>
                                        </div>
                                        <div class="radio-custom radio-primary col-lg-3">
                                            <input type="radio" id="" name="pay" value="N">
                                            <label for="">Card</label>
                                    <span class="required"> * </span>
                                       <div id="card" style="display:none;">  
                                      Card Amount: <input type="text" name="card_amount" required>
 Last 4 digits: <input type="text" name="last_digits" >
 Card Holder: <input type="text" name="card_holder" >
 Approval Code: <input type="text" name="approve_code" >
</div>

                                        </div>
                            </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div id="studendetailconfirmationmodal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Student Complete Detail</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table>
                                                    <th style="text-align:left;">Student Detail</th>
                                                    <tr><td>Student Full Name:</td><td id="student_full"></td></tr>
                                                    <tr><td>Class:</td><td id="student_class"></td></tr>
                                                    <tr><td>Section:</td><td id="student_section"></td></tr>
                                                    <tr><td>Pending Months:</td><td id="pending_m"></td></tr>
                                                    <!--tr><td>Stream:</td><td id="studentclasswisesnewtream"></td></tr-->
                                                    <th style="text-align:left;padding:15px 0 10px 0 !important;">Charges</th>
                                                    <!--tr><td>Admission Fee:</td><td id="studentadmision">&#x20B9;</td></tr-->
                                                    <tr><td>Monthly Charges:</td><td id="studentschoolfee">&#x20B9;</td></tr>
                                                    <!--tr><td>Book Charge:</td><td id="studentbook">&#x20B9;</td></tr-->
                                                    <tr><td>Dress Charge:</td><td id="studentdresscharge">&#x20B9;</td></tr>
                                                    <tr><td>Other Charge:</td><td id="studentother">&#x20B9;</td></tr>
                                                    <tr><td>Transport Charge:</td><td id="studenttransport">&#x20B9;</td></tr>
                                                    <tr><td>Discount Charge:</td><td id="studenttxtdiscountcharge">&#x20B9;</td></tr>
                                                    <!--tr><td>Donation:</td><td id="studentdonation">&#x20B9;</td></tr-->
                                                     <tr><td>Total Amount: </td><td id="studentsubtotalamount">&#x20B9;</td></tr>
                                                    <!--tr><td>Payment Mode:</td><td id="studentpaymentmode"></td></tr-->
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                      <button type="button" class="btn btn-default" id="print" onclick="printData();">Print</button>
<button type="Submit" class="btn btn-default" id="confrm" onclick="javascript:form.submit();">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                      
                            
                        <div class="form-group form-material col-xl-12 text-right padding-top-m">
                            <button type="submit" class="btn btn-primary sbm" id="validateButton_feesubmit">Submit</button>
                        </div>
</div>                            
                </form>
            </div>
            </div>
        <!-- End Panel Full Example -->

        
    </div>
</div>
<!-- End Page -->



 @endsection