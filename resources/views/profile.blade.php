@extends('layouts.principaltest.app')

@section('content')
<!-- Page -->
  <div class="page">
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-lg-3">
          <!-- Page Widget -->
          <div class="card card-shadow text-center">
            <div class="card-block">
              <a class="avatar avatar-lg profile_pic" href="javascript:void(0)">
                @if(!empty($profile->profile_pic))
                  <img src="{{$profile->profile_pic}}" alt="{{Auth()->user()->username}}">
                  <i></i>
                @else
                  <img src="{{asset('images/person-dummy.jpg')}}" alt="Dummy Pic">
                  <i></i>
                @endif
              </a>
              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Choose Image</button>
              
                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                  <div class="modal-dialog">
                  
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-body">
                        <form action="{{url('imagesave')}}" method="post" id="imageform">
                          <main class="pagecrop">
                          	<div class="boxcrop">
                          		<input type="file" id="file-input">
                          	</div>
                          	<!-- leftbox -->
                          	<div class="box-2 crop">
                          		<div class="result"></div>
                          	</div>
                          	<!--rightbox-->
                          	<div class="box-2 img-result hide crop">
                          		<!-- result of crop -->
                          		<input type="hidden" name="teacherid" id="teacherid" value="{{$teacher->id}}"/>
                          		<input type="hidden" class="userimage" name="userimage" id="imagevalue" value="">
                          		<img class="cropped" id="imagedata" src="" alt="">
                          	</div>
                          	<!-- input file -->
                          	<div class="box">
                          		<div class="options hide">
                          			<label> Width</label>
                          			<input type="number" class="img-w" value="300" min="100" max="1200" />
                          		</div>
                          		<!-- save btn -->
                          		
                          		<!-- download btn -->
                          		<a href="" class="btn download hide">Download</a>
                          	</div>
                          </main>
                          <div class="modal-footer">
                            <button class="btn save hide" id="myimage">Save</button>
                            <!--<button type="submit" class="btn btn-default" onclick="myimageForm()">Submit</button>-->
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </form>
                      </div>
                      
                    </div>
                    
                  </div>
                </div>
              <h4 class="profile-user">{{$userid->name}}</h4>
              <p class="profile-job">{{$userid->username}}</p>
              <p class="profile-job">{{$userid->email}}</p>
              <p>Hi! I'm Adrian the Senior UI Designer at AmazingSurge. We hope you
                enjoy the design and quality of Social.</p>
              <div class="profile-social">
                <a class="icon bd-twitter"  href="{{$teacher->twitter}}" target="_blank"></a>
                <a class="icon bd-facebook" href="{{$teacher->facebook}}" target="_blank"></a>
                <a class="icon bd-dribbble" href="{{$teacher->instagram}}" target="_blank"></a>
              </div>
              <button type="button" class="btn btn-primary">Follow</button>
            </div>
            <div class="card-footer">
              <div class="row no-space">
                <div class="col-4">
                  <strong class="profile-stat-count">260</strong>
                  <span>Follower</span>
                </div>
                <div class="col-4">
                  <strong class="profile-stat-count">180</strong>
                  <span>Following</span>
                </div>
                <div class="col-4">
                  <strong class="profile-stat-count">2000</strong>
                  <span>Tweets</span>
                </div>
              </div>
            </div>
          </div>
          <!-- End Page Widget -->
        </div>

        <div class="col-lg-9">
          <!-- Panel -->
          <div class="panel">
            <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
              <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                <li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#activities"
                    aria-controls="activities" role="tab">Activities <span class="badge badge-pill badge-danger">5</span></a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#profile" aria-controls="profile"
                    role="tab">Profile</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#messages" aria-controls="messages"
                    role="tab">Messages</a></li>
                <!--<li class="nav-item dropdown">-->
                <!--  <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>-->
                <!--  <div class="dropdown-menu" role="menu">-->
                <!--    <a class="active dropdown-item" data-toggle="tab" href="#activities" aria-controls="activities"-->
                <!--      role="tab">Activities <span class="badge badge-pill badge-danger">5</span></a>-->
                <!--    <a class="dropdown-item" data-toggle="tab" href="#profile" aria-controls="profile"-->
                <!--      role="tab">Profile</a>-->
                <!--    <a class="dropdown-item" data-toggle="tab" href="#messages" aria-controls="messages"-->
                <!--      role="tab">Messages</a>-->
                <!--  </div>-->
                <!--</li>-->
              </ul>

              <div class="tab-content">
                <div class="tab-pane active animation-slide-left" id="activities" role="tabpanel">
                  <ul class="list-group">
                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/2.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Ida Fleming
                            <small>posted an updated</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">“Check if it can be corrected with overflow : hidden”</div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/3.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Julius
                            <small>uploaded 4 photos</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief clearfix">
                            <img class="profile-uploaded" src="../../global/photos/animal-2-240x160.jpg" alt="...">
                            <img class="profile-uploaded" src="../../global/photos/animal-3-240x160.jpg" alt="...">
                            <img class="profile-uploaded" src="../../global/photos/animal-4-240x160.jpg" alt="...">
                            <img class="profile-uploaded" src="../../global/photos/animal-5-240x160.jpg" alt="...">
                          </div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/4.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Owen Hunt
                            <small>posted a new note</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Integer nec odio. Praesent libero. Sed cursus ante
                            dapibus diam. Sed nisi. Nulla quis sem at nibh elementum
                            imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce
                            nec tellus sed augue semper porta. Mauris massa.</div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/5.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Terrance Arnold
                            <small>posted a new blog</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">
                            <div class="media">
                              <a class="pr-20">
                                <img class="w-160" src="../../global/photos/animal-1-240x160.jpg" alt="...">
                              </a>
                              <div class="media-body pl-20">
                                <h4 class="mt-0 mb-5">Getting Started</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                  elit. Integer nec odio. Praesent libero. Sed
                                  cursus ante dapibus diam. Sed nisi. Nulla quis
                                  sem at nibh elementum imperdiet. Duis sagittis
                                  ipsum. Praesent mauris.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/2.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Ida Fleming
                            <small>posted an new activity comment</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">Cras sit amet nibh libero, in gravida nulla. Nulla vel
                            metus.</div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/3.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Julius
                            <small>posted an updated</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Integer nec odio. Praesent libero. Sed cursus ante
                            dapibus diam.</div>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <a class="btn btn-block btn-default profile-readMore" href="javascript:void(0)"
                    role="button">Show more</a>
                </div>

                <div class="tab-pane animation-slide-left" id="profile" role="tabpanel">
                  <div class="row">
                    <div class="col-md-12 profilesec">
                      <div class="row">
                        <div class="col-md-6">
                          <input type="hidden" name="teacherid" class="form-control edit-data hide-father teacherdata" value="{{$teacher->id}}">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Father Name</label>
                            <input type="text" name="pagedataset" class="form-control edit-data hide-father changefather" placeholder="Father Name" value="{{$teacher->father_name}}">
                            <a href="#" class="fa fa-check check-icon hide-father newchangefather newchangedata"></a>
                            <a href="#" class="fa fa-close check-icon hide-father"></a>
                            <div class="profile-content-data">
                              <a href="#" class="fa fa-pencil edit-icon">&nbsp;Edit</a>
                            <span class="newfather">{{$teacher->father_name}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Mother Name</label>
                            <input type="text" name="mother_name" id="mother_name" class="form-control edit-data-mother hide-mother changemother" placeholder="Mother Name" value="{{$teacher->mother_name}}">
                            <a href="#" class="fa fa-check checkmother-icon hide-mother newchangemother newchangedatamother"></a>
                            <a href="#" class="fa fa-close checkmother-icon hide-mother"></a>
                            <div class="profile-content-data-mother">
                              <a href="#" class="fa fa-pencil edit-icon-mother">&nbsp;Edit</a>
                            <span class="newmother">{{$teacher->mother_name}}</<span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Contact Number</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-contact hide-contact changecontact" placeholder="Contact Number" value="{{$teacher->contact_number}}">
                            <a href="#" class="fa fa-check checkcontact-icon hide-contact newchangecontact newchangedatacontact"></a>
                            <a href="#" class="fa fa-close checkcontact-icon hide-contact"></a>
                            <div class="profile-content-data-contact">
                              <a href="#" class="fa fa-pencil edit-icon-contact">&nbsp;Edit</a>
                            <span class="newcontact">{{$teacher->contact_number}}</span>
                            </div>

                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">DOB</label>
                            <div class="datepair-wrap" data-plugin="datepair">
                              <div class="input-daterange-wrap">
                                  <div class="input-daterange">
                                      <div class="input-group">
                                          <input type="text" class="form-control datepair-date datepair-start cal-set edit-data-dob hide-dob changedob" data-plugin="datepicker" name="approved_date" id="approved_date" placeholder="Approved Date" value="{{$teacher->dob}}">
                                          <a href="#" class="fa fa-check checkdob-icon hide-dob newchangedob newchangedatadob"></a>
                                          <a href="#" class="fa fa-close checkdob-icon hide-dob"></a>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            <div class="profile-content-data-dob">
                              <a href="#" class="fa fa-pencil edit-icon-dob">&nbsp;Edit</a>
                            <span class="newdob">{{$teacher->dob}}</span>
                            </div>
                             
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Aadhar Card</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-aadhar hide-aadhar changeaadhar" placeholder="Aadhar Number" value="{{$teacher->adhar_card_number}}">
                            <a href="#" class="fa fa-check checkaadhar-icon hide-aadhar newchangeaadhar newchangedataaadhar"></a>
                            <a href="#" class="fa fa-close checkaadhar-icon hide-aadhar"></a>
                            <div class="profile-content-data-aadhar">
                              <a href="#" class="fa fa-pencil edit-icon-aadhar">&nbsp;Edit</a>
                            <span class="newaadhar">{{$teacher->adhar_card_number}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Religion</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-religion hide-religion changereligion" placeholder="Religion" value="{{$teacher->religion}}">
                            <a href="#" class="fa fa-check checkreligion-icon hide-religion newchangereligion newchangedatareligion"></a>
                            <a href="#" class="fa fa-close checkreligion-icon hide-religion"></a>
                            <div class="profile-content-data-religion">
                              <a href="#" class="fa fa-pencil edit-icon-religion">&nbsp;Edit</a>
                            <span class="newreligion">{{$teacher->religion}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Graduate College Name</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-gcn hide-gcn changegcn" placeholder="College Name" value="{{$teacher->graduation_college_name}}">
                            <a href="#" class="fa fa-check checkgcn-icon hide-gcn newchangegcn newchangedatagcn"></a>
                            <a href="#" class="fa fa-close checkgcn-icon hide-gcn"></a>
                            <div class="profile-content-data-gcn">
                               <a href="#" class="fa fa-pencil edit-icon-gcn">&nbsp;Edit</a>
                            <span class="newgcn">{{$teacher->graduation_college_name}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Last Job</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-lastjob hide-lastjob changelastjob" placeholder="Last Job" value="{{$teacher->last_job}}">
                            <a href="#" class="fa fa-check checklastjob-icon hide-lastjob newchangelastjob newchangedatalastjob"></a>
                            <a href="#" class="fa fa-close checklastjob-icon hide-lastjob"></a>
                            <div class="profile-content-data-lastjob">
                              <a href="#" class="fa fa-pencil edit-icon-lastjob">&nbsp;Edit</a>
                            <span class="newlastjob">{{$teacher->last_job}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Last Designation</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-lastjobdesignation hide-lastjobdesignation changelastjobdesignation" placeholder="Last Job Designation" value="{{$teacher->last_job_designation}}" readonly="true">
                            <a href="#" class="fa fa-check checklastjobdesignation-icon hide-lastjobdesignation newchangelastjobdesignation newchangedatalastjobdesignation"></a>
                            <a href="#" class="fa fa-close checklastjobdesignation-icon hide-lastjobdesignation"></a>
                            <div class="profile-content-data-lastjobdesignation">
                              <!--<a href="#" class="fa fa-pencil edit-icon-lastjobdesignation">&nbsp;Edit</a>-->
                            <span class="newlastjobdesignation">{{$teacher->last_job_designation}}</span>
                            </div>

                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Last School Leaving Date</label>
                            <div class="datepair-wrap" data-plugin="datepair">
                              <div class="input-daterange-wrap">
                                  <div class="input-daterange">
                                      <div class="input-group">
                                          <input type="text" class="form-control datepair-date datepair-start cal-set edit-data-lastschooling hide-lastschooling changelastschooling" data-plugin="datepicker" name="approved_date" id="approved_date" placeholder="School Leaving Date" value="{{$teacher->school_Leaving_date}}" >
                                          <a href="#" class="fa fa-check checklastschooling-icon hide-lastschooling newchangelastschooling newchangedatalastschooling"></a>
                                          <a href="#" class="fa fa-close checklastschooling-icon hide-lastschooling"></a>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            <div class="profile-content-data-lastschooling">
                            <a href="#" class="fa fa-pencil edit-icon-lastschooling">&nbsp;Edit</a>
                            <span class="newlastschooling">{{$teacher->school_Leaving_date}}</<span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Last Salary</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-salary hide-salary changesalary" placeholder="Salary" value="{{$teacher->salary}}" readonly="true">
                            <a href="#" class="fa fa-check checksalary-icon hide-salary newchangesalary newchangedatasalary"></a>
                            <a href="#" class="fa fa-close checksalary-icon hide-salary"></a>
                            <div class="profile-content-data-salary">
                              <!--<a href="#" class="fa fa-pencil edit-icon-salary">&nbsp;Edit</a>-->
                            &#8377;<span class="newsalary"> {{$teacher->salary}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Last Increment Date</label>
                            <div class="datepair-wrap" data-plugin="datepair">
                              <div class="input-daterange-wrap">
                                  <div class="input-daterange">
                                      <div class="input-group">
                                          <input type="text" class="form-control datepair-date datepair-start cal-set edit-data-lastincrementdate hide-lastincrementdate changelastincrementdate" data-plugin="datepicker" name="approved_date" id="approved_date" placeholder="Last Increment Date" value="{{$teacher->last_increment_date}}" readonly="true">
                                          <a href="#" class="fa fa-check checklastincrementdate-icon hide-lastincrementdate newchangelastincrementdate newchangedatalastincrementdate"></a>
                                          <a href="#" class="fa fa-close checklastincrementdate-icon hide-lastincrementdate"></a>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            <div class="profile-content-data-lastincrementdate">
                              <!--<a href="#" class="fa fa-pencil edit-icon-lastincrementdate">&nbsp;Edit</a>-->
                            <span class="newlastincrementdate">{{$teacher->last_increment_date}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group form-material profilelabel">
                            <label class="form-control-label" for="inputBasicFirstName">Last Increment Amount</label>
                            <input type="text" name="pagedataset" class="form-control edit-data-lastamount hide-lastamount changelastamount" placeholder="Amount" value="{{$teacher->last_increment_amount}}" readonly="true">
                            <a href="#" class="fa fa-check checklastamount-icon hide-lastamount newchangelastamount newchangedatalastamount"></a>
                            <a href="#" class="fa fa-close checklastamount-icon hide-lastamount"></a>
                            <div class="profile-content-data-lastamount">
                              <!--<a href="#" class="fa fa-pencil edit-icon-lastamount">&nbsp;Edit</a>-->
                            &#8377; <span class="newlastamount"> {{$teacher->last_increment_amount}}</span>
                            </div>
                              
                              <div id="editor_error"> </div>
                              @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="tab-pane animation-slide-left" id="messages" role="tabpanel">
                  <ul class="list-group">
                    <li class="list-group-item">
                      <div class="media"> 
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/2.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Ida Fleming
                            <small>posted an updated</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">“Check if it can be corrected with overflow : hidden”</div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/5.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Terrance Arnold
                            <small>posted a new blog</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">
                            <div class="media">
                              <a class="pr-20">
                                <img class="w-160" src="../../global/photos/animal-1-240x160.jpg" alt="...">
                              </a>
                              <div class="media-body pl-20">
                                <h4 class="mt-0 mb-5">Getting Started</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                  elit. Integer nec odio. Praesent libero. Sed
                                  cursus ante dapibus diam. Sed nisi. Nulla quis
                                  sem at nibh elementum imperdiet. Duis sagittis
                                  ipsum. Praesent mauris.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/4.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Owen Hunt
                            <small>posted a new note</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Integer nec odio. Praesent libero. Sed cursus ante
                            dapibus diam. Sed nisi. Nulla quis sem at nibh elementum
                            imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce
                            nec tellus sed augue semper porta. Mauris massa.</div>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="media">
                        <div class="pr-20">
                          <a class="avatar" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/3.jpg"
                              alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-0 mb-5">Julius
                            <small>posted an updated</small>
                          </h5>
                          <small>active 14 minutes ago</small>
                          <div class="profile-brief">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Integer nec odio. Praesent libero. Sed cursus ante
                            dapibus diam.</div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- End Panel -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
@endsection