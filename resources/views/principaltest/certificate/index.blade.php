@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Add Certificate</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full table-responsive" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>certificate</th>
                            <th>Approved By</th>
                            <th>Approved Date</th>
                            <th>Issued By</th>
                            <th>Issued Date</th>
                            <th>Recieved Date</th>
                            @if(Request::is('principal'))
                            <th>Action</th>
                            @endif
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($user as $users)
                            <tr id="class">
                                <td>{{$i++}}</td>
                                <td>{{$users->name}}</td>
                                <td>{{$users->username}}</td>
                                <td><img src="principal/certificate/<?php echo $users->certificate ?>" width="50" height="50"></td>
                                <td>{{$users->approved_by}}</td>
                                <td>{{$users->approved_date}}</td>
                                <td>{{$users->issued_by}}</td>
                                <td>{{$users->issued_date}}</td>
                                <td>{{$users->received_date}}</td>
                                @if(Request::is('principal'))
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel " title="certificate" data-toggle="modal" data-target="#mycertificateetail-{{$users->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button title="certificate" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete"  data-id="{{$users->id}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <div class="modal fade" id="mycertificateetail-{{$users->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">{{$users->name}}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <!-- BEGIN FORM-->
                                                    <form action="{{url('certificate/update',$users->id)}}" class="horizontal-form" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    {{method_field('PATCH')}}
                                                        <div class="form-group form-material">
                                                            <input type="text" name="uname" id="uname" class="form-control"  
                                                            data-msg-required="The Name field is required." 
                                                            data-rule-required="true" value="{{$users->name}}" readonly>
                                                            @if ($errors->has('uname'))<p class="help-block">{{ $errors->first('uname')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" name="email" id="email" class="form-control"  
                                                            data-msg-required="The Email field is required." 
                                                            data-rule-required="true" value="{{$users->email}}" readonly>
                                                            @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" name="approved_by" id="approved_by" class="form-control"  
                                                            data-msg-required="The approve field is required." 
                                                            data-rule-required="true" value="{{$users->approved_by}}">
                                                            @if ($errors->has('approved_by'))<p class="help-block">{{ $errors->first('approved_by')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" name="approved_date" id="approved_date" class="form-control"  
                                                            data-msg-required="The approved date field is required." 
                                                            data-rule-required="true" value="{{$users->approved_date}}">
                                                            @if ($errors->has('approved_date'))<p class="help-block">{{ $errors->first('approved_date')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" name="issued_by" id="issued_by" class="form-control"  
                                                            data-msg-required="The issued field is required." 
                                                            data-rule-required="true" value="{{$users->issued_by}}">
                                                            @if ($errors->has('issued_by'))<p class="help-block">{{ $errors->first('issued_by')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" name="issued_date" id="issued_date" class="form-control"  
                                                            data-msg-required="The Issued Date field is required." 
                                                            data-rule-required="true" value="{{$users->issued_date}}">
                                                            @if ($errors->has('issued_date'))<p class="help-block">{{ $errors->first('issued_date')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" name="received_by" id="received_by" class="form-control"  
                                                            data-msg-required="The received by field is required." 
                                                            data-rule-required="true" value="{{$users->received_by}}">
                                                            @if ($errors->has('received_by'))<p class="help-block">{{ $errors->first('received_by')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" name="received_date" id="received_date" class="form-control"  
                                                            data-msg-required="The received date field is required." 
                                                            data-rule-required="true" value="{{$users->received_date}}">
                                                            @if ($errors->has('received_date'))<p class="help-block">{{ $errors->first('received_date')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <img src="principal/certificate/{{$users->certificate}}" width="100" height="100">
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="file" name="certificate" id="input-file-now-custom-1" data-plugin="dropify" data-default-file=""  
                                                            data-msg-required="The Certificate field is required." 
                                                            data-rule-required="true" placeholder="Certificate">
                                                            @if ($errors->has('certificate'))<p class="help-block">{{ $errors->first('certificate')}}</p>@endif
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-info">Update</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Holiday</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('certificate/store')}}" class="horizontal-form" method="post" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                    <div class="form-group form-material">
                                                            <select  name="uname" id="uname" class="form-control" 
                                                            data-msg-required="The Name field is required." 
                                                            data-rule-required="true">
                                                                <option>--Select--</option>
                                                                @foreach($studentprofile as $studentprofiles)
                                                                    <option value="{{$studentprofiles->id}}">{{$studentprofiles->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('uname'))<p class="help-block">{{ $errors->first('uname')}}</p>@endif
                                                        </div>
                                                    <div class="row">
                                                            <div class="form-group form-material col-md-6">
                                                                <input type="hidden" name="schoolid" value="{{$schoolid->id}}">
                                                                <input type="text" name="approved_by" id="approved_by" class="form-control"  
                                                                data-msg-required="The approve field is required." 
                                                                data-rule-required="true" placeholder="Approved By">
                                                                @if ($errors->has('approved_by'))<p class="help-block">{{ $errors->first('approved_by')}}</p>@endif
                                                            </div>
                                                            <div class="form-group form-material col-md-6">
                                                                <div class="datepair-wrap" data-plugin="datepair">
                                                                    <div class="input-daterange-wrap">
                                                                        <div class="input-daterange">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="approved_date" id="approved_date" placeholder="Approved Date">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @if ($errors->has('approved_date'))<p class="help-block">{{ $errors->first('approved_date')}}</p>@endif
                                                    <div class="row">
                                                            <div class="form-group form-material col-md-6">
                                                                <input type="text" name="issued_by" id="issued_by" class="form-control"  
                                                                data-msg-required="The issued field is required." 
                                                                data-rule-required="true" placeholder="Issued By">
                                                                @if ($errors->has('issued_by'))<p class="help-block">{{ $errors->first('issued_by')}}</p>@endif
                                                            </div>
                                                            <div class="form-group form-material col-md-6">
                                                                <div class="datepair-wrap" data-plugin="datepair">
                                                                    <div class="input-daterange-wrap">
                                                                        <div class="input-daterange">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="issued_date" id="issued_date" placeholder="Issued Date">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @if ($errors->has('issued_date'))<p class="help-block">{{ $errors->first('issued_date')}}</p>@endif
                                                    <div class="row">
                                                            <div class="form-group form-material col-md-6">
                                                                <input type="text" name="received_by" id="received_by" class="form-control"  
                                                                data-msg-required="The received by field is required." 
                                                                data-rule-required="true" placeholder="Received By">
                                                                @if ($errors->has('received_by'))<p class="help-block">{{ $errors->first('received_by')}}</p>@endif
                                                            </div>
                                                            <div class="form-group form-material col-md-6">
                                                                <div class="datepair-wrap" data-plugin="datepair">
                                                                    <div class="input-daterange-wrap">
                                                                        <div class="input-daterange">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="received_date" id="received_date" placeholder="Received Date">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @if ($errors->has('received_date'))<p class="help-block">{{ $errors->first('received_date')}}</p>@endif
                                                    <div class="form-group form-material">
                                                            <input type="file" name="certificate" id="input-file-now-custom-1" data-plugin="dropify" data-default-file=""  
                                                            data-msg-required="The Certificate field is required." 
                                                            data-rule-required="true" placeholder="Certificate">
                                                            @if ($errors->has('certificate'))<p class="help-block">{{ $errors->first('certificate')}}</p>@endif
                                                        </div>
                                                    <div class="modal-footer">
                                                            <button type="submit" class="btn btn-info">Save</button
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                                </form>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                           
                </div>
            </div>
        </div>
    </div>
@endsection