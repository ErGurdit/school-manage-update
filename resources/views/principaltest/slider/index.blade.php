@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
<div class="page">
    <div class="page-header">
        <h1 class="page-title">{{$firstparam}}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
        </ol>
    </div>
    <div class="page-content container-fluid">
        <!-- Panel Modals Styles -->
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-xl-12">
                        <!-- Example Tabs -->
                        <div class="example-wrap">
                            <!--tabs-->
                            <div class="nav-tabs-horizontal" data-plugin="tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne"
                                        aria-controls="exampleTabsOne" role="tab">All Slider</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" data-toggle="tab" href="#exampleTabsTwo"
                                            aria-controls="exampleTabsTwo" role="tab">Add Slider
                                        </a>
                                    </li>
                                    <!--<li class="nav-item" role="presentation">-->
                                    <!--    <a class="nav-link" data-toggle="tab" href="#exampleTabsThree"-->
                                    <!--        aria-controls="exampleTabsThree" role="tab" onclick="myFunction()">Active Slider-->
                                    <!--    </a>-->
                                    <!--</li>-->
                                    <!--<li class="nav-item" role="presentation">-->
                                    <!--    <a class="nav-link" data-toggle="tab" href="#exampleTabsFour"-->
                                    <!--        aria-controls="exampleTabsFour" role="tab" onclick="myFunction()">inactive Slider-->
                                    <!--    </a>-->
                                    <!--</li>-->
                                </ul>
                                <div class="tab-content pt-20">
                                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                        <table class="table table-hover dataTable table-striped w-full table-responsive" data-plugin="dataTable">
                                            <thead>
                                                <tr>
                                                    <th>SNo.</th>
                                                    <th>Username</th>
                                                    <th>Image</th>
                                                    <th>Heading</th>
                                                    <th>Description</th>
                                                    <th>Slider Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1;?>
                                                @foreach($slider as $sliders)
                                                <tr style="color:{{$sliders->status ? '' : 'red' }}">
                                                    <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>
                                         
                                                    <td class="statusset-{{$sliders->id}}">{!!ucwords($sliders->uname)!!}</td>
                                         
                                                    <td class="statusset-{{$sliders->id}}"><img src="{!!$sliders->slider!!}" width="100" height="100"></td>
                                         
                                                    <td class="statusset-{{$sliders->id}}">{{ucwords($sliders->topcontent)}}</td>
                                         
                                                    <td class="statusset-{{$sliders->id}}">{{substr($sliders->bottomcontent,0,40)}}</td>
                                                    
                                                    <td>@if($sliders->status == 1)<input type='checkbox' class="editstatus js-switch-small" data-plugin="switchery" data-size="small" data-on-text="Active" data-off-text="Inactive" title ="slider" name='checkbox2' rel="{{$sliders->status}}" checked  onchange="changestatus({{$sliders->id}},this)"/>@elseif($sliders->status == 0)<input type='checkbox' class="editstatus js-switch-small" data-plugin="switchery" data-size="small"  data-on-text="Active" data-off-text="Inactive" title ="slider"  name='checkbox2'  rel="{{$sliders->status}}" onchange="changestatus({{$sliders->id}},this)"/>@endif</td>
                                                
                                                    <td>
                                                        <button type="button" class="btn btn-floating btn-success btn-sm waves-effect waves-classic editdel" data-toggle="modal" title="sliderupdate" data-id="{{$sliders->id}}" data-target="#mySlider-{{$sliders->id}}"><i class="icon md-edit" aria-hidden="true"></i></button><button type="button" class="btn btn-delete btn-danger btn-floating btn-sm waves-effect waves-classic" title="slider"  data-id="{{$sliders->id}}"><i class="icon md-delete" aria-hidden="true"></i></button>
                                                         <div class="modal fade" id="mySlider-{{$sliders->id}}" role="dialog" data-slider_id="{{$sliders->id}}">
                                                            <div class="modal-dialog">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="addModalLabel">{{ucwords($sliders->topcontent)}} [edit]</h5>
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    </div>
                                                                    <div class="modal-body"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                        <div class="col-md-12">
                                          <!-- Example Basic Form Without Label -->
                                            <div class="example-wrap">
                                                <div class="example">
                                                    <form action="{{url('slider/store')}}" method="post" role="form" id="school-add" enctype="multipart/form-data">
                                                    <div class="form-group form-material">
                                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                            <input type="text" class="form-control" name="topcontent" data-msg-date="The Content field is required. must be a date." data-msg-required="The Name field is required." 
                                                            data-rule-date="true" data-rule-required="true" placeholder="topcontent" required/>
                                                            @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="text" class="form-control" name="bottomcontent"  data-msg-date="The Content Name field is required. must be a date." data-msg-required="The Principal Name field is required." 
                                                            data-rule-date="true" data-rule-required="true" placeholder="bottomcontent" required/>
                                                            @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <input type="file" name="filename" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" />
                                                            @if ($errors->has('filename'))<p class="help-block">{{ $errors->first('filename')}}</p>@endif
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn-primary" onclick="$('#school-add').submit()">Save</button>
                                                        </div>
                                                  </form>
                                                </div>
                                            </div>
                                          <!-- End Example Basic Form Without Label -->
                                        </div>
                                    </div>
                                    <!--<div class="tab-pane" id="exampleTabsThree" role="tabpanel">-->
                                    <!--    <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">-->
                                    <!--        <thead>-->
                                    <!--            <tr>-->
                                    <!--                <th>SNo.</th>-->
                                    <!--                <th>Username</th>-->
                                    <!--                <th>Image</th>-->
                                    <!--                <th>Heading</th>-->
                                    <!--                <th>Description</th>-->
                                    <!--            </tr>-->
                                    <!--        </thead>-->
                                    <!--        <tbody>-->
                                    <!--            <?php $i = 1;?>-->
                                    <!--            @foreach($slideractive as $sliders)-->
                                    <!--            <tr style="color:{{$sliders->status ? '' : 'red' }}">-->
                                    <!--                <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>-->
                                             
                                    <!--                <td class="statusset-{{$sliders->id}}">{!!$sliders->uname!!}</td>-->
                                             
                                    <!--                <td class="statusset-{{$sliders->id}}"><img src="/theme/mainimage/{!!$sliders->slider!!}" width="100" height="100"></td>-->
                                             
                                    <!--                <td class="statusset-{{$sliders->id}}">{{$sliders->topcontent}}</td>-->
                                             
                                    <!--                <td class="statusset-{{$sliders->id}}">{{substr($sliders->bottomcontent,0,60)}}</td>-->
                                                        
                                    <!--            </tr>-->
                                    <!--            @endforeach-->
                                    <!--        </tbody>-->
                                    <!--    </table>-->
                                    <!--</div>-->
                                    <!--<div class="tab-pane" id="exampleTabsFour" role="tabpanel">-->
                                    <!--    <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">-->
                                    <!--        <thead>-->
                                    <!--            <tr>-->
                                    <!--                <th>SNo.</th>-->
                                    <!--                <th>Username</th>-->
                                    <!--                <th>Image</th>-->
                                    <!--                <th>Heading</th>-->
                                    <!--                <th>Description</th>-->
                                    <!--            </tr>-->
                                    <!--        </thead>-->
                                    <!--        <tbody>-->
                                    <!--            <?php $i = 1;?>-->
                                    <!--            @foreach($sliderinactive as $sliders)-->
                                    <!--            <tr style="color:{{$sliders->status ? '' : 'red' }}">-->
                                    <!--                <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>-->
                                         
                                    <!--                <td class="statusset-{{$sliders->id}}">{!!$sliders->uname!!}</td>-->
                                         
                                    <!--                <td class="statusset-{{$sliders->id}}"><img src="/theme/mainimage/{!!$sliders->slider!!}" width="100" height="100"></td>-->
                                         
                                    <!--                <td class="statusset-{{$sliders->id}}">{{$sliders->topcontent}}</td>-->
                                         
                                    <!--                <td class="statusset-{{$sliders->id}}">{{substr($sliders->bottomcontent,0,60)}}</td>-->
                                                    
                                    <!--            </tr>-->
                                    <!--            @endforeach-->
                                    <!--        </tbody>-->
                                    <!--    </table>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection