@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                </div>
                <img src="{{ Session::get('image') }}" width="200px">
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#mygallery" data-toggle="modal" type="button">Add Gallery</button>
            <div class="panel-body container-fluid">
                <table class="table table-hover datatable table-striped w-full table-responsive" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Image</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($gallery as $galleries)
                            <tr>
                                <td>{{$i++}}</td>
                                <td><img src="{{$galleries->secure_url}}" width="100px" height="100px"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                    <!-- Modal -->
                                    <div class="modal fade" id="mygallery" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Gallery</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- BEGIN FORM-->
                                                    <form action="{{route('upload-file')}}" method="post" enctype="multipart/form-data">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="file" id="galleryimage" name="image_file" multiple class="form-control">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <button type="submit" class="btn btn-success">Upload</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection