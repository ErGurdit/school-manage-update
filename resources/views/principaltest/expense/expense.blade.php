<!--Markdown  editor-->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.min.css?v4.0.1')}}">
<!--Markdown editor-->
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/marked/marked.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/to-markdown/to-markdown.js?v4.0.1')}}"></script>
<!-- BEGIN FORM-->
<form action="{{url('expenses/update',$expenses->eid)}}" class="horizontal-form" method="post">
{{ csrf_field() }}
{{method_field('PATCH')}}
    <div class="form-group form-material">
        <input type="text" name="name" id="name" class="form-control" placholder="name" value="{{$expenses->exname}}">
        <span class="help-block"> @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif </span>
    </div>
    <div class="form-group form-material">
        <input type="text" name="amount" id="amount" class="form-control" value="{{$expenses->examount}}">
        <span class="help-block"> @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif </span>
    </div>
    <div class="form-group form-material">
        <select name="user_select" id="user_select" class="form-control">
            @foreach($user as $users)
                <?php $con = '';?>
                    <?php 
                        if ($users->uid == $expenses->userid) 
                        {
                            $con = ' selected="selected"';
                        }
                    ?>
                    <option <?php echo $con ;?>value="{{$users->uid}}">{{$users->uname}}-{{$users->rname}}</option>
            @endforeach
        </select>
        <span class="help-block"> @if ($errors->has('user'))<p class="help-block">{{ $errors->first('user')}}</p>@endif </span>
    </div>
    <div class="form-group form-material">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="icon md-calendar" aria-hidden="true"></i>
            </span>
            <input type="text" class="form-control" name="expense_date" value="{{$expenses->exdate}}" data-plugin="datepicker">
        </div>
        <span class="help-block"> @if ($errors->has('expense_date'))<p class="help-block">{{ $errors->first('expense_date')}}</p>@endif </span>
    </div>
    <div class="form-group form-material">
        <textarea name="pagedataset" data-provide="markdown" rows="10" cols="30" data-error-container="#editor_error" autofocus data-msg-date="The description field is required. must be a date." data-msg-required="The end date field is required." 
        data-rule-date="true" data-rule-required="true" required>{{$expenses->exdescribtion}}</textarea>
        <div id="editor_error"> </div>
        @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
    </div>
    @if(Request::is('accountant') || Request::is('feesdepartment'))
    <div class="modal-footer">
        <button type="submit" class="btn btn-info" value="Update">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
    @endif
</form>
<!-- END FORM-->