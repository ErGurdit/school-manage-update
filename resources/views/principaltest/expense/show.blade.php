@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
               <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                    <p>Staff Role:{{$expense->name}}</p>
                                    <p>Rs.{{$expense->expense_amount}}</p>
                                    <p>{{$expense->payment_display_name}}</p>
                                    <p>{{$expense->date}}</p>
                                    <p>{{$expense->description}}</p>
                                    <p>
                                        <form action="{{url('expenses/approve',$expense->id)}}" method="post">
                                            {{ csrf_field() }}
                                            {{method_field('PATCH')}}
                                            <input type="hidden" name="approve" value="{{$userdata}}">
                                            <input type="hidden" name="name" value="{{$expense->name}}">
                                            <input type="hidden" name="amount" value="{{$expense->expense_amount}}">
                                            <input type="hidden" name="pagedataset" value="{{$expense->description}}">
                                            <button type="submit" class="btn btn-binary">Approve</button>
                                        </form>
                                    </p>
                                </div>    
                            </div>                  
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>
@endsection