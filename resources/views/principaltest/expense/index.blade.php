@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            @if(Entrust::hasRole('accountant'))
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Add Expense</button>
            @endif
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>Name</th>
                            <th>Expense Amount</th>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($expense as $expenses)
                            <tr id="class" <?php if($expenses->askstatus ==1){?> style="color:#ffc107;"<?php } ?>>
                                <td>{{$i++}}</td>
                                <td>{{$expenses->username}}</td>
                                <td>{{$expenses->exname}}</td>
                                <td>{{$expenses->examount}}</td>
                                <td>{{$expenses->exdate}}</td>
                                <td>{{$expenses->exdescribtion}}</td>
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="expense" data-toggle="modal" data-target="#myeditexpensess-{{$expenses->eid}}" data-id="{{$expenses->eid}}">
                                        <i class="icon md-account-box-mail"></i>
                                    </button>
                                    <div class="modal fade" id="myeditexpensess-{{$expenses->eid}}" role="dialog" data-expense_id="{{$expenses->eid}}">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">{{$expenses->exname}}</h4>
                                                    </div>
                                                    <div class="modal-body"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Add Expense</h4>
                                                    
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{url('expenses/store')}}" class="horizontal-form" method="post">
                                                    {{ csrf_field() }}
                                                        <!--<div class="form-group form-material">-->
                                                        <!--    <input type="text" name="name" id="name" class="form-control" placeholder="Name">-->
                                                        <!--    <span class="help-block"> @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif </span>-->
                                                        <!--</div>-->
                                                        <div class="form-group form-material">
                                                            <input type="text" name="amount" id="amount" class="form-control" placeholder="Amount">
                                                            <span class="help-block"> @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif </span>
                                                        </div>
                                                        @foreach($paymentmethod as $paymentmethods)
                                                        <div class="radio-custom radio-primary">
                                                          <input type="radio" id="inputRadiosUnchecked" name="paymenttype"  value={{$paymentmethods->id}}/>
                                                          <label for="inputRadiosUnchecked">{{$paymentmethods->payment_display_name}}</label>
                                                        </div>
                                                        @endforeach
                                                        <!--<div class="form-group form-material">-->
                                                        <!--    <select name="user_select" id="user_select" class="form-control" required>-->
                                                        <!--        <option>Select</option>-->
                                                        <!--        @foreach($user as $users)-->
                                                        <!--            <option value="{{$users->uid}}">{{$users->uname}}-{{$users->rname}}</option>-->
                                                        <!--        @endforeach-->
                                                        <!--    </select>-->
                                                        <!--    <span class="help-block"> @if ($errors->has('user'))<p class="help-block">{{ $errors->first('user')}}</p>@endif </span>-->
                                                        <!--</div>-->
                                                        <!--<div class="form-group form-material">-->
                                                        <!--    <div class="input-group">-->
                                                        <!--        <span class="input-group-addon">-->
                                                        <!--            <i class="icon md-calendar" aria-hidden="true"></i>-->
                                                        <!--        </span>-->
                                                        <!--        <input type="text" class="form-control" name="expense_date" data-plugin="datepicker">-->
                                                        <!--    </div>-->
                                                        <!--    <span class="help-block"> @if ($errors->has('expense_date'))<p class="help-block">{{ $errors->first('expense_date')}}</p>@endif </span>-->
                                                        <!--</div>-->
                                                        <div class="form-group form-material">
                                                            <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                            data-rule-required="true" data-iconlibrary="fa" required></textarea>
                                                            <div id="editor_error"> </div>
                                                            @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material">
                                                          <div class="checkbox-custom checkbox-default">
                                                            <input type="checkbox" id="inputBasicRemember" name="askapprove" value="1"/>
                                                            <label for="inputBasicRemember">Ask Approval</label>
                                                          </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-success ">Save Changes</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>
@endsection