<link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.min.css?v4.0.1')}}">
<link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css?v4.0.1')}}">
<script src="{{asset('principal/assetstest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/marked/marked.min.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/to-markdown/to-markdown.js?v4.0.1')}}"></script>
  
<!-- BEGIN FORM-->
<form action="{{url('holiday/update',$holidays->id)}}" class="horizontal-form" method="post">
{{ csrf_field() }}
{{method_field('PATCH')}}
    <div class="row">
        <input type="hidden" name="schoolid" value="{{$school->id}}">
        <div class="form-group form-material col-md-6">
            <div class="input-daterange" data-plugin="datepicker">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="icon md-calendar" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control cal-set" name="from" value="{{$holidays->startdate}}">
                </div>
            </div>
        </div>
        <div class="form-group form-material col-md-6">
            <div class="input-daterange" data-plugin="datepicker">
                <div class="input-group">
                    <span class="input-group-addon">to</span>
                    <input type="text" class="form-control cal-set" name="to" value="{{$holidays->enddate}}"/>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-material">
        <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
        data-rule-required="true" data-iconlibrary="fa" required>{{$holidays->description}}</textarea>
        <div id="editor_error"> </div>
        @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update Changes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>