@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myholiday" data-toggle="modal"
            type="button">Add Holiday</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Description</th>
                            <th>Update</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($holiday as $holidays)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$holidays->startdate}}</td>
                                <td>{{$holidays->enddate}}</td>
                                <td>{{$holidays->description}}</td>
                                <td>{{$holidays->updated_at}}</td>
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic editdel" title="holiday" data-toggle="modal" data-target="#myeditholiday-{{$holidays->id}}" data-id="{{$holidays->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button title="holiday" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic btn-delete" id="holiday" data-id="{{$holidays->id}}" >
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <div class="modal fade" id="myeditholiday-{{$holidays->id}}" role="dialog" data-holiday_id="{{$holidays->id}}">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$holidays->date}}</h4>
                                                </div>
                                                <div class="modal-body"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myholiday" aria-hidden="true" aria-labelledby="examplePositionCenter"
                                      role="dialog" tabindex="-1">
                                        <div class="modal-dialog modal-simple modal-center">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title">Holiday</h4>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="modal-body">
                                                    <form action="{{url('holiday/store')}}" class="horizontal-form" method="post">
                                                    {{ csrf_field() }}
                                                        <div class="row">
                                                            <div class="form-group form-material col-md-6">
                                                                <input type="hidden" name="schoolid" value="{{$school->id}}">
                                                                <div class="input-daterange" data-plugin="datepicker">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control" name="from">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-material col-md-6">
                                                                <div class="input-daterange" data-plugin="datepicker">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">to</span>
                                                                        <input type="text" class="form-control" name="to" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                            data-rule-required="true" data-iconlibrary="fa" required></textarea>
                                                            <div id="editor_error"> </div>
                                                            @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Save Changes</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>
@endsection