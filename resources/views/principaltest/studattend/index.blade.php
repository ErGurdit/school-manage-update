@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<style>
  #__lpform_fname{
    display:none !important;
}
    
    

</style>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif

                        <!-- Panel Table Add Row -->
      <div class="panel">
        <header class="panel-heading">
          <!--h3 class="panel-title">Add Row</h3-->
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              
            </div>
          </div>
            <div class="example-wrap">
                <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <!--ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne"
                        aria-controls="exampleTabsOne" role="tab">Student</a></li>
                    <!--li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo"
                        aria-controls="exampleTabsTwo" role="tab">Increment</a></li-->
                    
                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                        <form action="{{ action('StudentAttendanceController@store') }}" class="horizontal-form" method="post" id="k">

                      <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
                               <div class="form-group form-material col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="class" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="section">
                                    <label class="control-label" for="inputUserNameOne">Section</label>
                                        <select data-plugin="select2" name="section" class="form-control" id="newsection">
                                            <option value="">--Select Section--</option>
                                               
                                        </select>
                                        @if ($errors->has('section'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('section') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                     <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="stream_" style="display:none">
                                    <label class="control-label" for="inputUserNameOne">Stream</label>
                                        <select data-plugin="select2" class="form-control" name="stream[]" id="stream" data-live-search="true">
                                        </select>
                                        @if ($errors->has('stream'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('stream') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>

            <thead>
              <tr>
                <th>Student</th>
                                <th>Username</th>

                <th>Father</th>
                <th>Mother</th>
                <th style="width: 150px">Present</th>
                <th style="width: 150px">Absent</th>
                <th style="width: 150px">Leaves</th>
                <th style="width: 150px">Holidays</th>

              </tr>
            </thead>
<input type="hidden" name="nme" id="nme">
            <tbody id="student_lst1">
             </tbody>
             <td style="display:none;" id="submt"><button type="submit" class="btn btn-primary" value="Save">Submit</button></td>
          </table>
                      </form>

                    </div>
                  
                  </div>
                </div>
              </div>
          
        </div>
      </div>
      <!-- End Panel Table Add Row -->

</div><!---End page-->
</div>