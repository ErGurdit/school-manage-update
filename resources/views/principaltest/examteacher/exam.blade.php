<!-- BEGIN FORM-->
<form action="{{url('exam/update',$exams->eid)}}" class="horizontal-form" method="post">
{{ csrf_field() }}
{{method_field('PATCH')}}
    <div class="form-group form-material col-md-12">
        <select name="examtype" id="examtype" class="form-control">
            @foreach($examtype as $examtypes)
            <?php $con = '';?>
                <?php 
                    if ($examtypes->id == $exams->examid) 
                    {
                       $con = ' selected="selected"';
                    }
                ?>
                <option value="{{$examtypes->id}}">{{$examtypes->name}}</option>
            @endforeach
        </select>
        <span class="help-block"> @if ($errors->has('student_name'))<p class="help-block">{{ $errors->first('student_name')}}</p>@endif </span>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <select name="student_name" id="student_name" class="form-control">
                @foreach($schoolstudent as $schoolstudents)
                    <?php $con = '';?>
                        <?php 
                            if ($schoolstudents->userid == $exams->userid) 
                            {
                                $con = ' selected="selected"';
                            }
                        ?>
                        <option <?php echo $con ;?>value="{{$schoolstudents->userid}}">{{$schoolstudents->name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('student_name'))<p class="help-block">{{ $errors->first('student_name')}}</p>@endif </span>
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <select name="class_select" id="class_select" class="form-control">
                @foreach($class as $classes)
                    <?php $con = '';?>
                        <?php 
                            if ($classes->id == $exams->classid) 
                            {
                                $con = ' selected="selected"';
                            }
                        ?>
                        <option <?php echo $con ;?>value="{{$classes->id}}">{{$classes->class_name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('class_select'))<p class="help-block">{{ $errors->first('class_select')}}</p>@endif </span>
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="text" name="ename" id="name" class="form-control"  
            data-msg-required="The Exam Name field is required." 
            data-rule-required="true" value="{{$exams->ename}}" placeholder="Exam Name">
            @if ($errors->has('ename'))<p class="help-block">{{ $errors->first('ename')}}</p>@endif
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="text" name="tomarks" id="cname" class="form-control"  
            data-msg-required="The total Marks field is required." 
            data-rule-required="true" value="{{$exams->total_marks}}" placeholder="Tomarks">
            @if ($errors->has('tomarks'))<p class="help-block">{{ $errors->first('tomarks')}}</p>@endif
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="text" name="percentage" id="percentage" class="form-control"  
            data-msg-required="The Percentage field is required." 
            data-rule-required="true" value="{{$exams->percentage}}" placeholder="Percentage">
            @if ($errors->has('percentage'))<p class="help-block">{{ $errors->first('percentage')}}</p>@endif
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="text" name="position" id="position" class="form-control"  
            data-msg-required="The Position field is required." 
            data-rule-required="true" value="{{$exams->position}}" placeholder="Position">
            @if ($errors->has('position'))<p class="help-block">{{ $errors->first('position')}}</p>@endif
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="text" name="result" id="result" class="form-control"  
            data-msg-required="The result field is required." 
            data-rule-required="true" value="{{$exams->result}}" placeholder="Result">
            @if ($errors->has('result'))<p class="help-block">{{ $errors->first('result')}}</p>@endif
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-info" value="Update">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>
<!-- END FORM-->