@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Check Exam</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Exam Name</th>
                            <th>Student name</th>
                            <th>Username</th>
                            <th>class</th>
                            <th>Total Marks</th>
                            <th>Obtain Marks</th>
                            <th>Percentage</th>
                            <th>Status</th>
                            <th>Passing Marks</th>
                            <th>Remarks</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($exam as $exams)
                            <tr id="class">
                                <td>{{$i++}}</td>
                                <td>{{$exams->id}}</td>
                                <td><a href="/studentdetails">{{$exams->uname}}</a></td>
                                <td>{{$exams->username}}</td>
                                <td>{{$exams->class_name}}</td>
                                <td>{{$exams->total_marks}}</td>
                                <td>{{$exams->obtain_marks}}</td>
                                <td>{{$exams->percentage}}%</td>
                                @if($exams->status == 1 )
                                <td>Pass</td>
                                @else
                                <td>Fail</td>
                                @endif
                                <td>{{$exams->passing_marks}}</td>
                                <td>{{$exams->remarks}}</td>
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="exam" data-toggle="modal" data-target="#myexamdetail-{{$exams->eid}}" data-id="{{$exams->eid}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button title="exam" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete"  data-id="{{$exams->id}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                <div class="modal fade" id="myexamdetail-{{$exams->eid}}" role="dialog" data-exam_id="{{$exams->eid}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$exams->id}}{{$exams->uname}}</h4>
                                                </div>
                                                <div class="modal-body1">
                                                <form action="{{url('examteacher/update',$exams->id)}}" class="horizontal-form" method="post" id="checkexamedit">
                                                    {{ csrf_field() }}
                                                       <div class="form-group form-material">
        <div class="col-md-12">
            <input type="hidden" name="schoolid" value="{{$school->id}}">
            <select name="classsid" id="class" class="form-control">
                <option>--Select Classes--</option>
                @foreach($class as $clss)
                    <?php 
                      $connew='';
                      if ($clss->id == $exams->classid) {
                          $connew = ' selected="selected"';
                          } 
                ?>
                    <option <?php echo $connew;?> value="{{$clss->id}}">{{$clss->class_display_name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
        </div>
    </div>
    <div class="form-group form-material">
    <div class="col-md-12">
            <select name="sectionid" id="sectionid" class="form-control">
                <option>--Select Section--</option>
                @foreach($section as $sections)
                <?php 
                      $connew='';
                      //dd($sections->sectionid);
                      if ($sections->id == $exams->sectionid) {
                          $connew = 'selected="selected"';
                          }
                ?>
                    <option <?php echo $connew;?> value="{{$sections->id}}">{{$sections->section_display_name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('sectionname'))<p class="help-block">{{ $errors->first('sectionname')}}</p>@endif </span>
        </div>
    </div>
    
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="tomarks" id="cname" class="form-control"  
                                                                 value="{{$exams->total_marks}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="obtain_marks" id="obtain_marks1" class="form-control"  
                                                                data-msg-required="The Obtain Marks field is required." 
                                                                data-rule-required="true" value="{{$exams->obtain_marks}}" placeholder="Obtain Marks">
                                                                @if ($errors->has('obtain_marks'))<p class="help-block">{{ $errors->first('obtain_marks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="passing_marks" id="passing_marks" class="form-control"  
                                                                data-msg-required="The Passing Marks field is required." 
                                                                data-rule-required="true" value="{{$exams->passing_marks}}" placeholder="passing_marks">
                                                                @if ($errors->has('passing_marks'))<p class="help-block">{{ $errors->first('passing_marks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="percentage" id="percentage1" class="form-control"  
                                                                data-msg-required="The Percentage field is required." 
                                                                data-rule-required="true" value="{{$exams->percentage}}" placeholder="Percentage" readonly>
                                                                @if ($errors->has('percentage'))
                                                                <p class="help-block">{{ $errors->first('percentage')}}</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="remarks" id="remarks" class="form-control"  
                                                                data-msg-required="The Position field is required." 
                                                                data-rule-required="true" value="{{$exams->remarks}}" placeholder="Remarks">
                                                                @if ($errors->has('remarks'))<p class="help-block">{{ $errors->first('remarks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                      <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="status1">
                                    <label class="control-label" for="inputUserNameOne">Status</label>
                                        <select data-plugin="select2" name="status" value="{{$exams->status}}" class="form-control" id="st">
                                            <option value="">--Select Status--</option>
                                          <?php 
                      $connew='';
                      if ($exams->status == 1) {
                          $connew = ' selected="selected"';
                           
                ?>
                    <option <?php echo $connew;?> value="{{$exams->status}}">Pass</option>
               <?php } else {
                                             $connew = ' selected="selected"';

                   ?>
                   
                                       <option <?php echo $connew;?> value="{{$exams->status}}">Fail</option>

                   <?php
               }
               ?>
                                        </select>
                                        @if ($errors->has('student_list'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('student_list') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save" id="checkexam_button_edit">Save Changes</button>
                                                        </div>
                                                    </form>
                                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Check Exam</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('examteacher/store')}}" class="horizontal-form" method="post" id="checkexam">
                                                    {{ csrf_field() }}
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="schoolid" value="{{$school->id}}">
                                                                 <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="classid" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                         </div> </div>
                                                        </div>
                                                         <div class="form-group form-material" id="section">

                                        <label class="control-label" for="inputUserNameOne">Section</label>
                                        <select data-plugin="select2" name="section" class="form-control" id="newsection">
                                            <option value="">--Select Section--</option>
                                        </select>
                                        @if ($errors->has('section'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('section') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                            
                                                       <div class="form-group form-material" id="stream_" style="display:none">

                                        <label class="control-label" for="inputUserNameOne">Stream</label>
                                        <select data-plugin="select2" class="form-control" name="stream[]" id="stream" data-live-search="true">
                                        </select>
                                        @if ($errors->has('stream'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('stream') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                     <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="student_list_">
                                    <label class="control-label" for="inputUserNameOne">Student</label>
                                    <input type="hidden" name="fee_id" value="" id="fe_id">
                                        <select data-plugin="select2" name="student_lst" class="form-control" id="student_list">
                                            <option value="">--Select Student--</option>
                                               
                                        </select>
                                        @if ($errors->has('student_list'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('student_list') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="tomarks" id="cname" class="form-control"  
                                                                data-msg-required="The total Marks field is required." 
                                                                data-rule-required="true" placeholder="Total marks">
                                                                @if ($errors->has('tomarks'))<p class="help-block">{{ $errors->first('tomarks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="obtain_marks" id="obtain_marks" class="form-control"  
                                                                data-msg-required="The Obtain Marks field is required." 
                                                                data-rule-required="true" placeholder="Obtain Marks">
                                                                @if ($errors->has('obtain_marks'))<p class="help-block">{{ $errors->first('obtain_marks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="passing_marks" id="passing_marks" class="form-control"  
                                                                data-msg-required="The Passing Marks field is required." 
                                                                data-rule-required="true" placeholder="passing_marks">
                                                                @if ($errors->has('passing_marks'))<p class="help-block">{{ $errors->first('passing_marks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="percentage" id="percentage" class="form-control"  
                                                                data-msg-required="The Percentage field is required." 
                                                                data-rule-required="true" placeholder="Percentage" readonly>
                                                                @if ($errors->has('percentage'))
                                                                <p class="help-block">{{ $errors->first('percentage')}}</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="remarks" id="remarks" class="form-control"  
                                                                data-msg-required="The Position field is required." 
                                                                data-rule-required="true" placeholder="Remarks">
                                                                @if ($errors->has('remarks'))<p class="help-block">{{ $errors->first('remarks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                      <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="status1">
                                    <label class="control-label" for="inputUserNameOne">Status</label>
                                        <select data-plugin="select2" name="status" class="form-control" id="st">
                                            <option value="">--Select Status--</option>
                                            <option value="1">Pass</option>
                                            <option value="0">Fail</option>
   
                                        </select>
                                        @if ($errors->has('student_list'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('student_list') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save" id="checkexam_button">Save Changes</button>
                                                        </div>
                                                    </form>
                                                <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>

@endsection