@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
    		    <div class="alert alert-success">
    		        {{ session()->get('message') }}
    		    </div>
    		@endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myTest" data-toggle="modal"
            type="button">Add Test</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <td>Test Date</td>
                            <td>Class</td>
                            <td>Section</td>
                            <td>Subject</td>
                            <th>Total Mark</th>
                            <th>Passing Mark</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($testmark as $testmark)
                            <tr>
                                <td>{{$i++}}</td>
                                <td><a href="#" data-target="#mytestdata-{{$testmark->tid}}" data-toggle="modal">{{$testmark->test_date}}</a></td>
                                <td><a href="{{url('studenttestdetail',['classid'=>$testmark->cid,'secid'=>$testmark->secid,'subid'=>$testmark->subid])}}">{{$testmark->class_display_name}}</a></td>
                                <td>{{$testmark->section_display_name}}</td>
                                <td>{{$testmark->subjectname}}</td>
                                <td>{{$testmark->totalmarks}}</td>
                                <td>{{$testmark->passingmarks}}</td>
                                <td>{{$testmark->description}}</td>
                                <td></td>
                            </tr>
                            <div class="modal fade" id="mytestdata-{{$testmark->tid}}" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Test Review</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group form-material">
                                                        <select data-plugin="selectpicker" name="classid" id="" class="form-control" data-live-search="true">
                                                            <option>--Select Classes--</option>
                                                            @foreach($classes as $class)
                                                            <?php $con =''; 
                                                                if($testmark->cid == $class->id){
                                                                $con = ' selected="selected"';
                                                            
                                                            }
                                                            ?>
                                                            
                                                                <option <?php echo $con; ?> value="{{$class->cid}}">{{$class->class_display_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group form-material">
                                                        <select name="sectionid" id="sectionid" class="form-control" data-plugin="selectpicker" data-live-search="true">
                                                            <option>--Select Section--</option>
                                                            @foreach($section as $sections)
                                                            <?php $con ='';
                                                                if($testmark->secid == $sections->id){
                                                                $con = ' selected="selected"';
                                                                }
                                                            ?>
                                                                <option <?php echo $con;?> value="{{$sections->sid}}">{{$sections->section_display_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block"> @if ($errors->has('sectionname'))<p class="help-block">{{ $errors->first('sectionname')}}</p>@endif </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group form-material">
                                                        <select name="subjectid" id="newsubjectid" class="form-control newsubjectididcl" data-plugin="selectpicker" data-live-search="true">
                                                        <option>--Select Subject--</option>
                                                            @foreach($subject as $subjects)
                                                            <?php $connew ='';
                                                                if($testmark->subid == $subjects->id){
                                                                $connew = ' selected="selected"';
                                                                }
                                                            ?>
                                                                <option <?php echo $connew ?> value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block"> @if ($errors->has('subjectname'))<p class="help-block">{{ $errors->first('subjectname')}}</p>@endif </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material">
                                                <div class="col-md-12">
                                                    <textarea name="description" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                    data-rule-required="true" data-iconlibrary="fa" placeholder="Description">{{$testmark->description}}</textarea>
                                                    @if ($errors->has('description'))<p class="help-block">{{ $errors->first('description')}}</p>@endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group form-material">
                                                        <div class="datepair-wrap" data-plugin="datepair">
                                                            <div class="input-daterange-wrap">
                                                                <div class="input-daterange">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="selectdate" id="received_date" placeholder="Received Date" value="{{$testmark->test_date}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <span class="help-block"> @if ($errors->has('selectdate'))<p class="help-block">{{ $errors->first('selectdate')}}</p>@endif </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-material">
                                                        <input type="text" name="totalmark" class="form-control" placeholder ="Total Mark" value="{{$testmark->totalmarks}}">
                                                        <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-material">
                                                        <input type="text" name="passingmark" class="form-control" placeholder ="Passing Mark" value="{{$testmark->passingmarks}}">
                                                        <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myTest" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Add Test</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('testmark/store')}}" class="horizontal-form" method="post">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group form-material">
                                                                <select data-plugin="selectpicker" name="classid" id="mynewclass" class="form-control" data-live-search="true">
                                                                    <option>--Select Classes--</option>
                                                                    @foreach($classes as $class)
                                                                        <option value="{{$class->cid}}">{{$class->class_display_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group form-material">
                                                                <select name="sectionid" id="sectionid" class="form-control" data-plugin="selectpicker" data-live-search="true">
                                                                    <option>--Select Section--</option>
                                                                    @foreach($section as $sections)
                                                                        <option value="{{$sections->sid}}">{{$sections->section_display_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('sectionname'))<p class="help-block">{{ $errors->first('sectionname')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group form-material">
                                                                <select name="subjectid" id="newsubjectid" class="form-control newsubjectididcl" data-plugin="selectpicker" data-live-search="true">
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('subjectname'))<p class="help-block">{{ $errors->first('subjectname')}}</p>@endif </span>
                                                                <select name="highclassid" id="highclassid" class="form-control highclassidcl newhighclassidcl" data-plugin="selectpicker" data-live-search="true" style="display:none">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group form-material newstreamsubjectid stremtestid" style="display:none;">
                                                                <select name="streamsubjectid" id="streamsubjectid" class="form-control streamsubjectid" data-plugin="selectpicker" data-live-search="true">
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('sectionid'))<p class="help-block">{{ $errors->first('sectionid')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-material">
                                                        <div class="col-md-12">
                                                            <textarea name="description" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                            data-rule-required="true" data-iconlibrary="fa" placeholder="Description" required></textarea>
                                                            @if ($errors->has('description'))<p class="help-block">{{ $errors->first('description')}}</p>@endif
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group form-material">
                                                                <div class="datepair-wrap" data-plugin="datepair">
                                                                    <div class="input-daterange-wrap">
                                                                        <div class="input-daterange">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="selectdate" id="received_date" placeholder="Received Date">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <span class="help-block"> @if ($errors->has('selectdate'))<p class="help-block">{{ $errors->first('selectdate')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group form-material">
                                                                <input type="text" name="totalmark" class="form-control" placeholder ="Total Mark">
                                                                <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group form-material">
                                                                <input type="text" name="passingmark" class="form-control" placeholder ="Passing Mark">
                                                                <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary" value="Save">Save Changes</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>

@endsection