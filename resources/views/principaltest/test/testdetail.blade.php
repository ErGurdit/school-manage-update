@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$classdetail->class_display_name}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Section : {{$sectiondetail->section_display_name}}</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Subject: {{$subdetail->subjectname}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
    		    <div class="alert alert-success">
    		        {{ session()->get('message') }}
    		    </div>
    		@endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <!--<button class="btn btn-primary" data-target="#myTest" data-toggle="modal"
            type="button">Add Test</button>-->
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <td>Student Name</td>
                            <th>Total Mark</th>
                            <th>Passing Mark</th>
                            <th>Obtain Mark</th>
                            <th>Description</th>
                            <th style="display:none;"></th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($studnetsubdetail as $studnetsubdetails)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$studnetsubdetails->name}}</td>
                                <td>{{$studnetsubdetails->totalmarks}}</td>
                                <td>{{$studnetsubdetails->passingmarks}}</td>
                                @if(!empty($studnetsubdetails->obtain_mark))
                                <td>{{$studnetsubdetails->obtain_mark}}</td>
                                @else
                                <td id="newtextboxadd-{{$studnetsubdetails->aid}}"><h4 class="notavailable">N/A</h4></td>
                                @endif
                                <td>{{$studnetsubdetails->description}}</td>
                                <td id="removetd-{{$studnetsubdetails->aid}}">
                                    <button type="button" id="addbox-{{$studnetsubdetails->aid}}" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" data-toggle="modal"  data-target="#mytestdeatil-{{$studnetsubdetails->aid}}" title="markupdate"  data-test_id="{{$studnetsubdetails->aid}}"><i class="icon md-edit"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete" title="markupdate"  data-id="{{$studnetsubdetails->aid}}"><i class="icon md-delete" aria-hidden="true"></i></button>
                                </td>
                                <td id="addtd-{{$studnetsubdetails->aid}}" style="display:none;">
                                    <button type="button" id="savebox-{{$studnetsubdetails->aid}}" class="btn btn-floating btn-success btn-sm waves-effect waves-classic waves-effect waves-classic" data-toggle="modal"  data-target="#mytestdeatil-{{$studnetsubdetails->aid}}" title="markupdate"  data-save_id="{{$studnetsubdetails->aid}}"><i class="fa fa-check" aria-hidden="true"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic" title="markupdate" id="canceltestdetail-{{$studnetsubdetails->aid}}"  data-cancel_id="{{$studnetsubdetails->aid}}"><i class="fa fa-times" aria-hidden="true"></i></button>
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>                           
            </div>
        </div>
    </div>

@endsection