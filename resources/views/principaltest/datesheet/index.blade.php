@extends('layouts.principaltest.student')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Add DateSheet</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Class</th>
                            <th>Section</th>
                            <th>Date </th>
                            <th>Subject </th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Action</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($datesheet as $dt)
                            <tr id="class">
                                <td>{{$i++}}</td>
                                 <td>{{$dt->classname}}</td>
                                 <td>{{$dt->sectionname}}</td>
                                 <td>{{date('d-m-Y', strtotime($dt->ct))}}</td>
                                 <td>{{$dt->subjectname}}</td>
                                 <td>{{$dt->examtime}}</td>
                                                                  <td>{{$dt->examtime}}</td>
            <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="exam" data-toggle="modal" data-target="#myexamdetail-{{$dt->dit}}" data-id="{{$dt->dit}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button title="exam" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete" data-toggle="modal" data-target="#delete-{{$dt->dit}}"  data-id="{{$dt->id}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                         <div class="modal fade" id="delete-{{$dt->dit}}" role="dialog" data-id="{{$dt->dit}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$dt->dit}}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{url('datesheet/destroy',$dt->dit)}}" class="horizontal-form" method="post" id="datesheetform">
                                                    {{ csrf_field() }}
<div class="row">
    <h1>Are you sure you want to delete</h1>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save" id="datesheet_button_delet">Yes</button>
                                                        </div>
                                                        </div><!-- row-->
                                                    </form>
                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-----------Delete-------->
                                <div class="modal fade" id="myexamdetail-{{$dt->dit}}" role="dialog" data-id="{{$dt->dit}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$dt->dit}}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{url('datesheet/update',$dt->dit)}}" class="horizontal-form" method="post" id="datesheetform">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                         <div class="form-group form-material">
        <div class="col-md-12">
            <input type="hidden" name="schoolid" value="{{$school->id}}">
            <select name="classid" id="class" class="form-control">
                <option>--Select Classes--</option>
                @foreach($class as $clss)
                    <?php 
                      $connew='';
                      if ($clss->id == $dt->classid) {
                          $connew = ' selected="selected"';
                          } 
                ?>
                    <option <?php echo $connew;?> value="{{$clss->id}}">{{$clss->class_display_name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
        </div>
    </div>
                                         
                                         <div class="form-group form-material">
    <div class="col-md-12">
            <select name="sectionid" id="sectionid" class="form-control">
                <option>--Select Section--</option>
                @foreach($section as $sections)
                <?php 
                      $connew='';
                      //dd($sections->sectionid);
                      if ($sections->id == $dt->sectionid) {
                          $connew = 'selected="selected"';
                          }
                ?>
                    <option <?php echo $connew;?> value="{{$sections->id}}">{{$sections->section_display_name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('sectionname'))<p class="help-block">{{ $errors->first('sectionname')}}</p>@endif </span>
        </div>
    </div>
                                     <div class="form-group form-material">
    <div class="col-md-12">
            <select name="subjectid" id="subjectid" class="form-control">
                <option>--Select Section--</option>
                @foreach($subject as $subjects)
                <?php 
                      $connew='';
                      //dd($sections->sectionid);
                      if ($subjects->id == $dt->subjectid) {
                          $connew = 'selected="selected"';
                          }
                ?>
                    <option <?php echo $connew;?> value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('subjectname'))<p class="help-block">{{ $errors->first('subjectname')}}</p>@endif </span>
        </div>
    </div>
                                    </div><!-- row-->
                                    <div class="row">
                                
                                         <div class="col-md-3">
                                                    <div class="form-group form-material">
<!-- Example Auto Colse -->
              <div class="example-wrap">
               <div class="example">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <span class="md-time"></span>
                    </span>
                    <input type="text" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true" name="time" value="{{$dt->examtime}}">
                  </div>
                </div>
              </div>
                                                        </div>
              </div>
              <!-- End Example Auto Colse -->
                                                        </div><!-- row-->
<div class="row">
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save" id="datesheet_button">Save Changes</button>
                                                        </div>
                                                        </div><!-- row-->
                                                    </form>
                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Add New Datesheet</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('datesheet/store')}}" class="horizontal-form" method="post" id="datesheetform">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                        <div class="form-group form-material">
                                                            <div class="col-md-3">
                                                                <input type="hidden" name="schoolid" value="{{$school->id}}">
                                                                </div>
                                                        </div>
                                          <div class="col-md-3">
                                                    <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="classid" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                         </div>
                                         </div>
                                          <div class="col-md-3">
                                                    <div class="form-group form-material">

                                        <label class="control-label" for="inputUserNameOne">Section</label>
                                        <select data-plugin="select2" name="section" class="form-control" id="newsection">
                                            <option value="">--Select Section--</option>
                                        </select>
                                        @if ($errors->has('section'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('section') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                    </div>
                                      <div class="col-md-3">
                                                    <div class="form-group form-material">

                                        <label class="control-label" for="inputUserNameOne">Stream</label>
                                        <select data-plugin="select2" class="form-control" name="stream[]" id="stream" data-live-search="true">
                                        </select>
                                        @if ($errors->has('stream'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('stream') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                    </div>
                                    </div><!-- row-->
                                    <div class="row">
                                <div class="col-md-3">
                                                    <div class="form-group form-material">
                             <input type="hidden" name="schoolid" value="{{$school->id}}">
                                                                <div class="input-daterange" data-plugin="datepicker">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control" name="from">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             </div>
                                                               <div class="col-md-3">
                                                          <div class="form-group form-material col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-control-label" for="inputUserNameOne">Subject</label>
                                                <select data-plugin="select2" multiple name="subjectdata[]" id="subjectdata" class="form-control subjectdata" required="required">
                                                   
                                                </select>
                                                    @if ($errors->has('class'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('class') }}</strong>
                                                        </span>
                                                    @else
                                                    <span class="help-block"></span>
                                                    @endif
                                            </div>
                                            <div class="form-group form-material col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-control-label" for="inputUserNameOne">Other Subject</label>
                                                <select data-plugin="select2" multiple name="leftsubject[]" id="leftsubject" class="form-control leftsubject">
                                                   
                                                </select>
                                                    @if ($errors->has('class'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('class') }}</strong>
                                                        </span>
                                                    @else
                                                    <span class="help-block"></span>
                                                    @endif
                                            </div>
                                       </div>
                                         <div class="col-md-3">
                                                    <div class="form-group form-material">
<!-- Example Auto Colse -->
              <div class="example-wrap">
               <div class="example">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <span class="md-time"></span>
                    </span>
                    <input type="text" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true" name="time">
                  </div>
                </div>
              </div>
                                                        </div>
              </div>
              <!-- End Example Auto Colse -->
                                                        </div><!-- row-->
<div class="row">
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save" id="datesheet_button">Save Changes</button>
                                                        </div>
                                                        </div><!-- row-->
                                                    </form>
                                                <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>

@endsection