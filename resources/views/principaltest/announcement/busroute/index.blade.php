@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Add Route</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Destination</th>
                            <th>Fare</th>
                            <th>Last Update</th>
                            <td>Status</td>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($busroute as $busroutes)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$busroutes->routename}}</td>
                                <td>{{$busroutes->fare}}</td>
                                <td>{{$busroutes->updated_at}}</td>
                                <td>@if($busroutes->status == 1)<input type='checkbox' class="editstatus js-switch-small" data-plugin="switchery" data-size="small" data-on-text="Active" data-off-text="Inactive" title ="busroute" name='checkbox2' rel="{{$busroutes->status}}" checked  onchange="changestatus({{$busroutes->id}},this)"/>@elseif($busroutes->status == 0)<input type='checkbox' class="editstatus js-switch-small" data-plugin="switchery" data-size="small"  data-on-text="Active" data-off-text="Inactive" title ="busroute"  name='checkbox2'  rel="{{$busroutes->status}}" onchange="changestatus({{$busroutes->id}},this)"/>@endif</td>
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="exam" data-toggle="modal" data-target="#myroute-{{$busroutes->id}}" data-id="{{$busroutes->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button title="busroute" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete"  data-id="{{$busroutes->id}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <div class="modal fade" id="myroute-{{$busroutes->id}}" role="dialog" data-busroute_id="{{$busroutes->id}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$busroutes->routename}}</h4>
                                                </div>
                                                <div class="modal-body"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Add Route</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('busroute/store')}}" class="horizontal-form" method="post">
                                                    {{ csrf_field() }}
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="schoolid" id="schoolid" value="{{$school->id}}"> 
                                                                <input type="text" name="destination" id="destination" class="form-control"  
                                                                data-msg-required="The Destination field is required." 
                                                                data-rule-required="true" placeholder="Destination">
                                                                @if ($errors->has('destination'))<p class="help-block">{{ $errors->first('destination')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="fare" id="fare" class="form-control"  
                                                                data-msg-required="The Fare field is required." 
                                                                data-rule-required="true" placeholder="Fare">
                                                                @if ($errors->has('fare'))<p class="help-block">{{ $errors->first('fare')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save">Save Changes</button>
                                                        </div>
                                                    </form>
                                                <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>

@endsection