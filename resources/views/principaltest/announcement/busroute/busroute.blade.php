<!-- BEGIN FORM-->
<form action="{{url('busroute/update',$busroute->id)}}" class="horizontal-form" method="post">
{{ csrf_field() }}
{{method_field('PATCH')}}
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="text" name="destination" id="destination" class="form-control"  
            data-msg-required="The Destination field is required." 
            data-rule-required="true" value="{{$busroute->routename}}" placeholder="Destination">
            @if ($errors->has('destination'))<p class="help-block">{{ $errors->first('destination')}}</p>@endif
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="text" name="fare" id="fare" class="form-control"  
            data-msg-required="The Fare field is requiredd." 
            data-rule-required="true" value="{{$busroute->fare}}" placeholder="Fare">
            @if ($errors->has('tomarks'))<p class="help-block">{{ $errors->first('tomarks')}}</p>@endif
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-info" value="Update">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>
<!-- END FORM-->