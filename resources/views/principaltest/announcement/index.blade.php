@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
<div class="page">
    <div class="page-header">
        <h1 class="page-title">{{$firstparam}}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
        </ol>
    </div>
    <div class="page-content container-fluid">
        <!-- Panel Modals Styles -->
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row row-lg">
                    <div class="col-xl-12">
                        <!-- Example Tabs -->
                        <div class="example-wrap">
                            <!--tabs-->
                            <div class="nav-tabs-horizontal" data-plugin="tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne"
                                        aria-controls="exampleTabsOne" role="tab">All Announcement</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" data-toggle="tab" href="#exampleTabsTwo"
                                            aria-controls="exampleTabsTwo" role="tab">Add Announcement
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content pt-20">
                                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                        <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                                            <thead>
                                                <tr>
                                                    <th>SNo.</th>
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th>Days Left</th>
                                                     <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1;?>
                                                @foreach($announce as $announces)
                                                <tr>
                                                    <td class="statusset-{{$announces->id}}">{!!$i++!!}</td>
                                                    <td class="statusset-{{$announces->id}}">{{$announces->title}}</td>
                                                    <td class="statusset-{{$announces->id}}">{{substr($announces->description,0,50)}}</td>
                                                    <td class="statusset-{{$announces->id}}">@if(!empty($announces->startdate)){{date($announces->startdate)}}@else {{date("Y-m-d")}}@endif</td>
                                                    <td class="statusset-{{$announces->id}}">{!!$announces->enddate!!}</td>
                                                    <?php 
                                                        $startdate = strtotime($announces->startdate);
                                                        $enddate = strtotime($announces->enddate);
                                                            if(!empty($startdate))
                                                            {
                                                                $now = $startdate;
                                                            }
                                                            else
                                                            {
                                                            $now = time();
                                                            }
                                                   
                                                        $datediff = $enddate-$now;
                                                   
                                                        $diff = round($datediff / (60 * 60 * 24));?>
                                                    @if($diff>0) 
                                                    <td><?php echo $diff ?> days</td>
                                                    @else
                                                    <td><?php echo '<span id="taskdone">Done</span>';?></td>
                                                    @endif
                                                    <td>
                                                        <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" data-toggle="modal"  data-target="#myAnnounce-{{$announces->id}}" title="announceupdate"  data-id="{{$announces->id}}"><i class="icon md-edit"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete" title="announce"  data-id="{{$announces->id}}"><i class="icon md-delete" aria-hidden="true"></i></button>
                                                        <div class="modal fade" id="myAnnounce-{{$announces->id}}" role="dialog" data-announce_id="{{$announces->id}}">
                                                        <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="addModalLabel">Edit-{{$announces->title}}</h5>
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                </div>
                                                                <div class="modal-body"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                        <!-- Example Basic Form Without Label -->
                                        <div class="example-wrap">
                                            <div class="example">
                                                <form action="{{url('announce/store')}}" method="post" role="form" id="announce-add" class="announce-add"  enctype="multipart/form-data">
                                                    <div class="form-group form-material">
                                                            <input type="hidden" name="schoolid" value="{{$school->id}}"/>
                                                            <input type="text" name="title" id="title" class="form-control"  
                                                            data-msg-required="The title field is required." 
                                                            data-rule-required="true" placeholder="Title" required>
                                                            @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
                                                        </div>
                                                    <div class="form-group form-material col-md-12">
                                                            <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                            data-rule-required="true" data-iconlibrary="fa" placeholder="Description" required></textarea>
                                                            <div id="editor_error"> </div>
                                                            @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                                        </div>
                                                    <div class="row">
                                                            <div class="form-group form-material col-md-6">
                                                                <div class="datepair-wrap" data-plugin="datepair">
                                                                    <div class="input-daterange-wrap">
                                                                        <div class="input-daterange" data-date="<?php echo date("m/d/Y");?>">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control datepair-date datepair-start" data-plugin="datepicker" name="datestart" >
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-material col-md-6">
                                                                <div class="datepair-wrap" data-plugin="datepair">
                                                                    <div class="input-daterange-wrap">
                                                                        <div class="input-daterange">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">
                                                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control datepair-date datepair-start" data-plugin="datepicker" name="dateend">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary">Save</button>
                                                    </div>
                                                </form>
                                            </div>
                                          <!-- End Example Basic Form Without Label -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection