<link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.min.css?v4.0.1')}}">
<script src="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/marked/marked.min.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/to-markdown/to-markdown.js?v4.0.1')}}"></script>
<div class="example">
    <form action="{{url('announce/update',$announces->id)}}" method="post" role="form" id="school-edit-{{$announces->id}}" class="school-update-{{$announces->id}}" enctype="multipart/form-data">
    {{method_field('PATCH')}}
    <!--Announce title-->
        <div class="form-group form-material">
            <input type="text" name="title" id="title" class="form-control"  
            data-msg-date="The Title field is required. must be a date." data-msg-required="The title field is required." 
            data-rule-date="true" data-rule-required="true" value="{{$announces->title}}">
            @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
        </div>
        <!--Announcement Decsription-->
        <div class="form-group form-material">
            <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-date="The description field is required. must be a date." data-msg-required="The end date field is required." 
            data-rule-date="true" data-rule-required="true" data-iconlibrary="fa" required>{{$announces->description}}</textarea>
            <div id="editor_error"> </div>
            @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
        </div>
        <div class="row">
            <div class="form-group form-material col-md-6">
                <div class="datepair-wrap" data-plugin="datepair">
                    <div class="input-daterange-wrap">
                        <div class="input-daterange">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="datestart" value="{{$announces->startdate}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-material col-md-6">
                <div class="datepair-wrap" data-plugin="datepair">
                    <div class="input-daterange-wrap">
                        <div class="input-daterange">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="dateend" value="{{$announces->endtdate}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$announces->id}}').submit()">Update changes</button>
    </div>
</div>