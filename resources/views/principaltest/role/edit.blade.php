@extends('layouts.principaltest.app')

@section('content')
  <!-- BEGIN CONTENT -->
    <?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Roles</a></li>
                
                <li class="breadcrumb-item active">{{$currentURL}}</li>
            </ol>
        </div>
        <div class="page-content">
            <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row row-lg">
                        <div class="col-md-12 col-lg-6">
                            <div class="example-wrap">
                                <div class="example">
                                    <form action="{{route('role.store')}}" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group form-material">
                                            <input type="hidden" name="role_create" value="1">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Role name" value="{{$role->name}}" />
                                            <span class="help-block">@if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif </span>
                                        </div>
                                        <div class="form-group form-material">
                                            <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Role display name" value="{{$role->display_name}}" />
                                            <span class="help-block">@if ($errors->has('display_name'))<p class="help-block">{{ $errors->first('display_name')}}</p>@endif </span>
                                        </div>
                                        <div class="form-group form-material">
                                            <textarea class="form-control" name="description" placeholder="Role Description">{{$role->description}}</textarea>
                                        </div>
                                        <div class="form-group form-material">
                                            @foreach($permission as $permissions)
                                            <?php 
                                                $con = '';
                                                if($permissions->name == 'read'){
                                                $con = ' checked="checked"';
                                            ?>
                                            <div class="checkbox-custom checkbox-default" style="display:none">
                                                <input type="checkbox" id="inputCheckboxAgree" name="permission[]" value="{{$permissions->id}}"  <?php echo $con ;?> />
                                                <label for="inputCheckboxAgree">Agree Policy</label>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                            @endforeach
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary">Update</button>
                                        </div>
                                  </form>
                                </div>
                            </div>
                          <!-- End Example Basic Form Without Label -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection