@extends('layouts.principaltest.app')

@section('content')
    <?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
          <!-- Panel Basic -->
          <div class="panel">
            <!--<a href="{{route('role.create')}}" class="btn btn-primary">Create Role</a>-->
            <header class="panel-heading">
              <div class="panel-actions"></div>
              <h3 class="panel-title">{{$firstparam}}</h3>
            </header>
            
            <div class="panel-body">
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Display Name</th>
                    <th>Description</th>
                    <!--<th>Action</th>-->
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php $i=1;?>
                        @forelse($role as $roles)
                            <td>{{$i++}}</td>
                            <td> {{$roles->display_name}} </td>
                            <td> {{$roles->description}} </td>
                            <!--<td>-->
                            <!--    <a href="{{route('role.edit',$roles->id)}}" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel"><i class="icon md-edit"></i></a>-->
                            <!--    <a href="{{route('role.destroy',$roles->id)}}" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic"><i class="icon md-delete"></i></a>-->
                                <!--<form action="{{route('role.destroy',$roles->id)}}" method="POST">-->
                                <!--    {{csrf_field()}}-->
                                <!--    {{method_field('DELETE')}}-->
                                <!--    <input type="submit" value="Delete"/>-->
                                <!--</form> -->
                            <!--</td>-->
                    </tr>
                    @empty
                    <tr>
                        <td>No Role</td>
                    </tr>
                    @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
@endsection