@extends('layouts.principaltest.app')
@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<!-- BEGIN CONTENT -->
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Users</a></li>
                
                <li class="breadcrumb-item active">{{$currentURL}}</li>
            </ol>
        </div>
        <div class="page-content">
          <!-- Panel Basic -->
          <div class="panel">
            <header class="panel-heading">
              <div class="panel-actions"></div>
              <h3 class="panel-title">{{$firstparam}}</h3>
            </header>
            
            <div class="panel-body">
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php $i=1;?>
                        @forelse($user as $users)
                            <td>{{$i++}}</td>
                            <td>{{$users->name}}</td>
                            <td>
                                @foreach($users->roles as $role)
                                    {{$role->name}}
                                @endforeach
                            </td>
                            <td>
                                <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" data-toggle="modal" data-target="#exampleModal-{{$users->id}}">
                                   <i class="icon md-edit"></i>
                                </button>
                                <button type="button" class="btn btn-delete btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic" title="userdel" data-toggle="modal" data-target="#exampleModal-{{$users->id}}">
                                   <i class="icon md-delete"></i>
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal-{{$users->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{$users->name}} Role Edit</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                     <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                    <form action="{{route('user.update',$users->id)}}" method="post" role="form" id="role-form-{{$users->id}}">
                                                        {{csrf_field()}}
                                                        {{method_field('PATCH')}}
                                                        <div class="form-group">
                                                            <select name="roles[]" multiple>
                                                                @foreach($allrole as $role)
                                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                                @endforeach
                                                            </select>  
                                                        </div>
                                                                
                                                        <!--<input class="btn btn-primary" type="submit" value="Submit"/>-->
                                                    </form>
                                                <div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary" onclick="$('#role-form-{{$users->id}}').submit()">Save changes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td>No Role</td>
                        </tr>
                        @endforelse
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
@endsection