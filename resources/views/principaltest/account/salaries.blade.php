@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
             @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
           @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Add Salary</button>
<div class="row row-lg">
             <div class="panel-body container-fluid">
                   <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name </th>
                            <th>Salary</th>
                            <th>Last Increment month</th>
                            <th>Last increment amount</th>
                            <th>Paid amount</th>
                            <th>Last month Salary (Paid/Pending)</th>
                            <th>Current Month Salary (Paid/Pending)</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?>
                            @foreach($salary as $sal)
                            <div class="modal fade" id="pay-{{$sal->id}}" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Pay Salary</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{ route('salary.update',$sal->id)}}" class="horizontal-form" method="post" id="k">
                                                                                                   {{ csrf_field() }}

                                                <input type="hidden" value="{{$sal->id}}" name="salid">
                                                <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save">Pay Now</button>
                                                        </div>    
                                                </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                            <tr>
                                <td>{{$i++}}</td>
                            <td>
                                {{$sal->name}}
                            </td>
                            <td>
                                {{$sal->salary}}
                            </td>
                            <td>
                                {{$sal->last_increment_month}}
                            </td>
                            <td>
                                {{$sal->last_increment_amount}}
                            </td>
                            <td>
                                {{$sal->deposit}}
                            </td>
                            <td>
                                @if($sal->status == 1)
                                <span>Paid</span>
                                @else
                                <span>Pending</span>
                                @endif
                            </td>
                            <td>
                                @if($sal->status == 1)
                                <span>Paid</span>
                                @else
                                <span>Pending</span>
                                @endif
                            </td>
                            <td>
                                @if($sal->status == 0)
                                <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#pay-{{$sal->id}}">
<i class="fas fa-user-edit"></i>                                    </button>
@else
<span style="color:green">Paid</span>
@endif
                            </td>
                            </tr>
                            @endforeach
                            </tbody>
                            </table>
                            
                            
                            <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Add Salary</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('salaries/store')}}" class="horizontal-form" method="post" id="add_salary">
                                                    {{ csrf_field() }}
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                 <label class="control-label" for="inputClass">Staffmember</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}" name="authid">
                                        <select data-plugin="selectpicker" name="stafmid" id="staffm" class="form-control" data-live-search="true">
                                            <option value="">--Select Staff--</option>
                                                @foreach($staff as $staf)
                                                    <option value="{{$staf->id}}" data-name ="{{$staf->userid}}" data-id="{{$staf->userid}}">{{$staf->uname}}</option>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                                               </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <label>Salary</label>
<select data-plugin="select2" name="staffms" class="form-control" id="staffmsalary" required="">
                                            <option>--Select Section--</option>
                                               
                                        </select>                                                                 </div>
                                                                 <div class="col-md-6">
                                                                <lable>Deposit</lable><input type="text" name="depo" id="inc" class="form-control"  
                                                                placeholder="" >
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                <lable>Deductions</lable><input type="text" name="dec" id="inc" class="form-control"  
                                                                placeholder="" >
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                <lable>Increment</lable><input type="text" name="inc" id="inc" class="form-control"  
                                                                placeholder="" >
                                                                 </div>
                                                                 <!--div class="col-md-6">
                                                                <lable>Decrement</lable><input type="text" name="dec" id="dec" class="form-control"  
                                                                placeholder="" >
                                                                 </div-->
                                                                 <div class="col-md-6">
                                                                <lable>Bonus</lable><input type="text" name="bon" id="bon" class="form-control"  
                                                                placeholder="" >
                                                                 </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save" id="submit_changes">Save Changes</button>
                                                        </div>
                                                    </form>
                                                <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div><!--panel-body-->
            </div><!-- End row  -->
        </div><!--panel-->
    </div>
</div>
<!--Main Page-->

@endsection