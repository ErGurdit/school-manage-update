@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#expenses" data-toggle="modal"
            type="button">Add Expense</button>
            <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Amount</th>
                            <th>Payment Method</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($expense as $expenses)
                            <tr id="class">
                                <td>{{$i++}}</td>
                                <td>{{$expenses->date}}</td>
                                <td>{{$expenses->description}}</td>
                                <td>{{$expenses->expense_amount}}</td>
                                <td>{{$expenses->payment_display_name}}</td>
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="class" data-toggle="modal" data-target="#myexpense-{{$expenses->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-floating btn-danger btn-delete btn-sm waves-effect waves-classic waves-effect waves-classic" title="expense" data-toggle="modal">
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <button type="button" class="btn btn-floating btn-success btn-sm waves-effect waves-classic waves-effect waves-classic" title="expense" data-toggle="modal" data-target="#myexpense-{{$expenses->id}}">
                                        <i class="icon md-users"></i>
                                    </button>
                                    <div class="modal fade" id="myexpense-{{$expenses->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Expense</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <!-- BEGIN FORM-->
                                                <form action="{{url('expenseupdate',$expenses->id)}}" class="horizontal-form" method="post">
                                                {{ csrf_field() }}
                                                    {{method_field('PATCH')}}
                                                    <div class="form-group form-material col-md-12">
                                                        <textarea name="description" data-provide="markdown" data-iconlibrary="fa" rows="11" 
                                                        data-rule-required="true" placeholder="Description">{{$expenses->description}}</textarea>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group form-material col-md-6">
                                                            <input name="amount" data-msg-required="The Expense field is required." 
                                                            data-rule-required="true" class="form-control" placeholder="Expense Amount" value="{{$expenses->expense_amount}}">
                                                            @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif
                                                        </div>
                                                        <div class="form-group form-material col-md-6">
                                                            @foreach($paymentmethod as $paymentmethods)
                                                            <?php $con ='';?>
                                                                <?php if($paymentmethods->id == $expenses->payment_type){
                                                                    $con = ' checked="checked"';
                                                                }?>
                                                                <div class="radio-custom radio-primary">
                                                                  <input type="radio" id="inputRadiosChecked" name="payment_type" <?php echo $con; ?> value="{{$paymentmethods->id}}"/>
                                                                  <label for="inputRadiosChecked">{{$paymentmethods->payment_display_name}}</label>
                                                                </div>
                                                                @if ($errors->has('payment_type'))<p class="help-block">{{ $errors->first('payment_type')}}</p>@endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Update</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        <div class="modal fade" id="expenses" role="dialog">
                            <div class="modal-dialog">
                            <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Expense</h4>
                                    </div>
                                    <div class="modal-body">
                                    <!-- BEGIN FORM-->
                                    <form action="{{url('expensestore')}}" class="horizontal-form" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group form-material col-md-12">
                                        <textarea name="description" data-provide="markdown" data-iconlibrary="fa" rows="11" 
                                        data-rule-required="true" placeholder="Description" required></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="form-group form-material col-md-6">
                                            <input name="amount" data-msg-required="The Expense field is required." 
                                            data-rule-required="true" class="form-control" placeholder="Expense Amount" required>
                                            @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            @foreach($paymentmethod as $paymentmethods)
                                                <div class="radio-custom radio-primary">
                                                    <input type="radio" id="inputRadiosChecked" name="payment_type" value="{{$paymentmethods->id}}" required/>
                                                    <label for="inputRadiosChecked">{{$paymentmethods->payment_display_name}}</label>
                                                </div>
                                                @if ($errors->has('payment_type'))<p class="help-block">{{ $errors->first('payment_type')}}</p>@endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-info">Save</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection