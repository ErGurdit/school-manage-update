@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#" data-toggle="modal"
            type="button">Maintain Fee Charge</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Classes</th>
                            <th>Admission Fee</th>
                            <th>Monthly Fee</th>
                            <th>Tution Fee</th>
                            <th>Book Fee</th>
                            <th>Dress Fee</th>
                            <!--th>Transport Fee</th-->
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @forelse($allclasses as $allclass)
                            <tr id="class">
                                <td>{{$i++}}</td>
                                <td>{{$allclass->class_display_name}}</td>
                                <td>
                                    @foreach($allmaintain as $allmaintains)
                                        @if($allmaintains->classid == $allclass->cid)
                                        <?php
                                        if(!empty($allmaintains->admissionfee)){
                                            $number = $allmaintains->admissionfee;
                                        }elseif($allmaintains->admissionfee == ''){
                                            $number = 0;
                                        }else{
                                            $number = 0;
                                        }
                                        
                                        setlocale(LC_MONETARY, 'en_IN');
                                        $admissionfee = money_format('%!i', $number);
                                        ?>
                                            &#8377;&nbsp;<?php if($admissionfee == 0){ echo '<span style="color:red;">0.00</span>';} else { echo $admissionfee; } ?>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($allmaintain as $allmaintains)
                                        @if($allmaintains->classid == $allclass->cid)
                                        <?php
                                        if(!empty($allmaintains->schoolfee)){
                                            $number = $allmaintains->schoolfee;
                                        }elseif($allmaintains->schoolfee == ''){
                                            $number = 0;
                                        }else{
                                            $number = 0;
                                        }
                                        
                                        setlocale(LC_MONETARY, 'en_IN');
                                        $schoolfee = money_format('%!i', $number);
                                        ?>
                                            &#8377;&nbsp;<?php if($schoolfee == 0){ echo '<span style="color:red;">0.00</span>';} else { echo $schoolfee; } ?>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($allmaintain as $allmaintains)
                                        @if($allmaintains->classid == $allclass->cid)
                                        <?php
                                        if(!empty($allmaintains->classcharge)){
                                            $number = $allmaintains->classcharge;
                                        }elseif($allmaintains->classcharge == ''){
                                            $number = 0;
                                        }else{
                                            $number = 0;
                                        }
                                        setlocale(LC_MONETARY, 'en_IN');
                                        $classcharge = money_format('%!i', $number);
                                        ?>
                                        
                                            &#8377;&nbsp;<?php if($classcharge == 0){ echo '<span style="color:red;">0.00</span>';} else { echo $classcharge; } ?>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($allmaintain as $allmaintains)
                                        @if($allmaintains->classid == $allclass->cid)
                                        <?php
                                        if(!empty($allmaintains->bookcharge)){
                                            $number = $allmaintains->bookcharge;
                                        }elseif($allmaintains->bookcharge == ''){
                                            $number = 0;
                                        }else{
                                            $number = 0;
                                        }
                                        setlocale(LC_MONETARY, 'en_IN');
                                        $bookcharge = money_format('%!i', $number);
                                        ?>
                                            &#8377;&nbsp;<?php if($bookcharge == 0){ echo '<span style="color:red;">0.00</span>';} else { echo $bookcharge; } ?>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($allmaintain as $allmaintains)
                                        @if($allmaintains->classid == $allclass->cid)
                                        <?php
                                        if(!empty($allmaintains->dresscharge)){
                                            $number = $allmaintains->dresscharge;
                                        }elseif($allmaintains->dresscharge == ''){
                                            $number = 0;
                                        }else{
                                            $number = 0;
                                        }
                                        
                                        setlocale(LC_MONETARY, 'en_IN');
                                        $dresscharge = money_format('%!i', $number);
                                        ?>
                                            &#8377;&nbsp;<?php if($dresscharge == 0){ echo '<span style="color:red;">0.00</span>';} else { echo $dresscharge; } ?>
                                        @endif
                                    @endforeach
                                </td>
                                <!--td>
                                    <?php
                                    /*@foreach($allmaintain as $allmaintains)
                                        @if($allmaintains->classid == $allclass->cid)
                                        <?php
                                        if(!empty($allmaintains->transportcharge)){
                                            $number = $allmaintains->transportcharge;
                                        }elseif($allmaintains->transportcharge == ''){
                                            $number = 0;
                                        }else{
                                            $number = 0;
                                        }
                                        
                                        setlocale(LC_MONETARY, 'en_IN');
                                        $transportcharge = money_format('%!i', $number);
                                        ?>
                                            &#8377;&nbsp;<?php if($transportcharge == 0){ echo '<span style="color:red;">0.00</span>';} else { echo $transportcharge; } ?>
                                        @endif
                                    @endforeach*/
                                    ?>
                                </td-->
                                
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#myfeedetail-{{$allclass->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <div class="modal fade" id="myfeedetail-{{$allclass->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">{{$allclass->class_display_name}}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <!-- BEGIN FORM-->
                                                <form action="{{url('classfeeupdate')}}" class="horizontal-form" method="post">
                                                {{ csrf_field() }}
                                                    {{method_field('PATCH')}}
                                                    <div class="row">
                                                        <input type="hidden" name="classid" value="{{$allclass->cid}}"/>
                                                        <input type="hidden" name="schoolid" value="{{$staffmember->schoolid}}"/>
                                                                <!--<input type="text" name="name" id="name" class="form-control"  -->
                                                                <!--data-msg-required="The Exam Name field is required." -->
                                                                <!--data-rule-required="true" value="{{$allclass->class_display_name}}" placeholder="Exam Name">-->
                                                        <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">Admission Fee
                                                                <?php $admissionfee = '';?>
                                                                @foreach($allmaintain as $allmaintains)
                                                                    @if($allmaintains->classid == $allclass->id)
                                                                    <?php
                                                                        if(!empty($allmaintains->admissionfee)){
                                                                            $number = $allmaintains->admissionfee;
                                                                        }else{
                                                                            $number = 0;
                                                                        }
                                                                        
                                                                        $admissionfee = $number;
                                                                    ?>
                                                                    @endif
                                                                @endforeach
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-inr"></i>
                                                                    </span>
                                                                <input type="text" name="admissionfee" id="name" class="form-control"  
                                                                data-msg-required="The Admission Fee field is required." 
                                                                data-rule-required="true" placeholder="0.00" value="@if($admissionfee == 0)  @else {{ $admissionfee}} @endif">
                                                                @if ($errors->has('admissionfee'))<p class="help-block">{{ $errors->first('admissionfee')}}</p>@endif
                                                            </div>
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">Monthly Fee
                                                                <?php $schoolfee = '';?>
                                                                @foreach($allmaintain as $allmaintains)
                                                                    @if($allmaintains->classid == $allclass->id)
                                                                    <?php
                                                                        if(!empty($allmaintains->schoolfee)){
                                                                            $number = $allmaintains->schoolfee;
                                                                        }else{
                                                                            $number = 0;
                                                                        }
                                                                        
                                                                        $schoolfee = $number;
                                                                    ?>
                                                                    @endif
                                                                @endforeach
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-inr"></i>
                                                                    </span>
                                                                <input type="text" name="schoolfee" id="name" class="form-control"  
                                                                data-msg-required="The School Fee field is required." 
                                                                data-rule-required="true" placeholder="0.00" value="@if($schoolfee == 0)  @else {{ $schoolfee}} @endif">
                                                                @if ($errors->has('schoolfee'))<p class="help-block">{{ $errors->first('schoolfee')}}</p>@endif
                                                            </div>
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">Tution Fee
                                                                <?php $classcharge = '';?>
                                                                @foreach($allmaintain as $allmaintains)
                                                                    @if($allmaintains->classid == $allclass->id)
                                                                    <?php
                                                                    if(!empty($allmaintains->classcharge)){
                                                                            $number = $allmaintains->classcharge;
                                                                        }else{
                                                                            $number = 0;
                                                                        }
                                                                        
                                                                        $classcharge = $number;
                                                                    ?>
                                                                    @endif
                                                                @endforeach
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-inr"></i>
                                                                    </span>
                                                                <input type="text" name="classcharge" id="name" class="form-control"  
                                                                data-msg-required="The Class Charge field is required." 
                                                                data-rule-required="true" placeholder="0.00" value="@if($classcharge == 0)  @else {{ $classcharge}} @endif">
                                                                @if ($errors->has('classcharge'))<p class="help-block">{{ $errors->first('classcharge')}}</p>@endif
                                                            </div>
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">Book Fee
                                                                <?php $bookcharge = '';?>
                                                                @foreach($allmaintain as $allmaintains)
                                                                    @if($allmaintains->classid == $allclass->id)
                                                                     <?php
                                                                        if(!empty($allmaintains->bookcharge)){
                                                                            $number = $allmaintains->bookcharge;
                                                                        }else{
                                                                            $number = 0;
                                                                        }
                                                                        
                                                                        $bookcharge = $number;
                                                                    ?>
                                                                    @endif
                                                                @endforeach
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-inr"></i>
                                                                    </span>
                                                                <input type="text" name="bookcharge" id="name" class="form-control"  
                                                                data-msg-required="The Book Charge field is required." 
                                                                data-rule-required="true" placeholder="0.00" value="@if($bookcharge == 0)  @else {{ $bookcharge}} @endif">
                                                                @if ($errors->has('bookcharge'))<p class="help-block">{{ $errors->first('bookcharge')}}</p>@endif
                                                            </div>
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">Dress Fee
                                                                <?php $dresscharge = '';?>
                                                                @foreach($allmaintain as $allmaintains)
                                                                    @if($allmaintains->classid == $allclass->id)
                                                                    <?php
                                                                        if(!empty($allmaintains->dresscharge)){
                                                                            $number = $allmaintains->dresscharge;
                                                                        }else{
                                                                            $number = 0;
                                                                        }
                                                                        
                                                                        $dresscharge = $number;
                                                                    ?>
                                                                    @endif
                                                                @endforeach
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-inr"></i>
                                                                    </span>
                                                                <input type="text" name="dresscharge" id="name" class="form-control"  
                                                                data-msg-required="The Class Charge field is required." 
                                                                data-rule-required="true" placeholder="0.00" value="@if($dresscharge == 0)  @else {{ $dresscharge}} @endif">
                                                                @if ($errors->has('dresscharge'))<p class="help-block">{{ $errors->first('dresscharge')}}</p>@endif
                                                            </div>
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">Transport Fee
                                                                <?php $transportcharge = '';?>
                                                                @foreach($allmaintain as $allmaintains)
                                                                    @if($allmaintains->classid == $allclass->id)
                                                                    <?php
                                                                        if(!empty($allmaintains->transportcharge)){
                                                                            $number = $allmaintains->transportcharge;
                                                                        }else{
                                                                            $number = 0;
                                                                        }
                                                                        
                                                                        $transportcharge = $number;
                                                                    ?>
                                                                    @endif
                                                                @endforeach
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-inr"></i>
                                                                    </span>
                                                                <input type="text" name="transportcharge" id="name" class="form-control"  
                                                                data-msg-required="The Transport Charge field is required." 
                                                                data-rule-required="true" placeholder="0.00" value="@if($transportcharge == 0)  @else {{ $transportcharge}} @endif">
                                                                @if ($errors->has('transportcharge'))<p class="help-block">{{ $errors->first('transportcharge')}}</p>@endif
                                                            </div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Update</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <td>No Fee</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection