@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                           <th>Vehicle Number</th>
                            <th>Driver Name</th>
                            <th>Driver Number</th>
                            <th>Helper</th>
                            <th>Helper Number</th>
                            <th>Charges</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($route_data as $routes)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$routes->vehicle_number}}</td>
                        <td>{{$routes->driver_name}}</td>
                        <td>{{$routes->driver_contact}}</td>
                        <td>{{$routes->helper}}</td>
                        <td>{{$routes->helper_contact}}</td>
                                <td>Rs.{{$routes->charges}}</td>
                                <td>
                                    <div class="modal fade" id="myroute-{{$routes->id}}" role="dialog" data-id="{{$routes->id}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$routes->vehicle_number}}</h4>
                                                    </div>
                                                    <div style="padding-left:3%;padding-bottom:2%;">
                                                    <form method="post" action="{{ route('othercharges.approvestore',$routes->id) }}" enctype="multipart/form-data">
                                                     <h4>Do you want to Approve -{{$routes->vehicle_number}}-{{$routes->driver_name}} </h4>
                                                     <input type="hidden" name="approvedid" value="{{$routes->id}}">
                                                     <input type="hidden" name="status" value="1">
                             <button type="submit" class="btn btn-primary" id="">Yes</button>
                          <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                                </form>
                                                </div>
                                                </div>
                                                <!--div class="modal-content"-->
                                
                                                <!--/div-->
                                            </div>
                                        </div>
                                    </div><!-- modal fade-->
                                    <div class="modal fade" id="rejected-{{$routes->id}}" role="dialog" data-id="{{$routes->id}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$routes->vehicle_number}}</h4>
                                                    </div>
                                                    <div style="padding-left:3%;padding-bottom:2%;">
                                                    <form method="post" action="{{ route('othercharges.approvestore',$routes->id) }}" enctype="multipart/form-data">
                                                     <h4>Do you want to Reject {{$routes->vehicle_number}}-{{$routes->driver_name}} </h4>
                                                     <input type="hidden" name="approvedid" value="{{$routes->id}}">
                                                     <input type="hidden" name="status" value="2">
                             <button type="submit" class="btn btn-primary" id="">Yes</button>
                          <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                                </form>
                                                </div>
                                                </div>
                                                <!--div class="modal-content"-->
                                
                                                <!--/div-->
                                            </div>
                                        </div>
                                    </div><!-- modal fade-->
                                    @if($routes->status == '0' || $routes->status == NULL)
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="approved" data-toggle="modal" data-target="#myroute-{{$routes->id}}" data-id="{{$routes->id}}">
<i class="fas fa-thumbs-up"></i> </button>
<button type="button" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="rejected" data-toggle="modal" data-target="#rejected-{{$routes->id}}" data-id="{{$routes->id}}">
<i class="fas fa-close"></i> </button>
@elseif($routes->status == '1')
<button type="button" class="btn btn-floating btn-success btn-sm waves-effect waves-classic waves-effect waves-classic" title="approved" data-toggle="modal" data-target="" data-id="" disabled>
<i class="fas fa-check-circle"></i> </button> 
@elseif($routes->status == '2')
<button type="button" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic" title="rejected" data-toggle="modal" data-target="" data-id="" disabled>
<i class="fas fa-close"></i> </button> 
@endif
                                    </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>                           
            </div>
        </div>
    </div>

@endsection