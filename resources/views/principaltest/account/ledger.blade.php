@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div id="msg" class="alert alert-success">
                    <p class="msg1">{{ session()->get('message') }}</p>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
<div class="row row-lg">
               <div class="page-content container-fluid">
      <!-- Panel Tabs -->
        <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Cash amount</th>
                            <th>Card amount</th>
                            <th>Cheque amount</th>
                            <th>Total Fees</th>
                            <th>Expenses</th>
                            <th>Total cash in hand</th>

                <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php $i=1;?>
                        @foreach($ledger_detail as $ld)
                            <tr id="class">
                                <td>{{$i++}}</td>
                              <td>{{date('d-m-Y', strtotime($ld->updated_at))}}</td>
                              <td>{{$ld->cash_amount}}</td>
                              <td>{{$ld->card_amount}}</td>
                                <td>0</td>
                               <td>{{$ld->tm}}</td>
                               <td>{{rtrim(ltrim($expense_amount,'"['),']"')}}</td>
                               <td>{{rtrim(ltrim($total_hand_cash,'['),']')}}</td>
                              <td><a href="/ledger/view"><i class="fas fa-eye"></i></a></td>
    
                              

                              </tr>
                              @endforeach

                            </tbody>
                            </table>
      
</div>
</div>
</div><!--Panel-->


@endsection