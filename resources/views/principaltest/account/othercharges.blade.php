@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif
            
                    
            

            <!-- Panel Modals Styles -->
            <div class="panel">
<div class="row row-lg">
                                            <!--Add Route form view-->
                                            <div class="modal fade" id="other_charg" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-simple modal-center">
                                                <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title"></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12">
                                                                <div class="example-wrap">
                                                                    <div class="example">
                                                                    <!-- BEGIN FORM-->
                                <form id="otherchargesform" autocomplete="off" method ="post" action="{{ url('othercharges/store') }}" enctype="multipart/form-data">
                                               {{csrf_field()}}
                                               <div class="form-group form-material">
                                                            <div class="form-group form-material">
                                <label class="control-label">Vehicle Number
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="vehcle" placeholder=""
                                           value="">
                                </div>
                            </div>
                               
                            </div>
                    
                            <div class="form-group form-material">
                                <label class="control-label">Driver Name
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="dr_name" placeholder=""
                                           required="">
                                </div>
                            </div>
                            <div class="form-group form-material">
                                <label class="control-label">Driver Contact Number
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="dr_contact" placeholder=""
                                           required="" maxlength="10">
                                </div>
                            </div>
                            <div class="form-group form-material">
                                <label class="control-label">Helper 
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="helper" placeholder=""
                                           required="">
                                </div>
                            </div>
                            <div class="form-group form-material">
                                <label class="control-label">Helper contact number
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="helper_contact" placeholder=""
                                           required="" maxlength="10">
                                </div>
                            </div>
                           
                            <div class="form-group form-material">
                                <label class="control-label">Charges (P/M)
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="charges" placeholder=""
                                           required="">
                                </div>
                            </div>
                            <div class="modal-footer">
                                    <div class="form-group">
                             <button type="submit" class="btn btn-primary" id="validateButton_other">Submit</button>

                                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                            </div>
                                                </div>
                                                                        </form>
                                                      </div>
                                                             </div>
                                                          </div>
                                                              </div><!-- Modal Body-->
                                                            </div>
                                                              </div>
                                                                    </div><!-- Main Modal-->
                                    <!--Book Modal-->
                                     <div class="modal fade" id="other_book_charg" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-simple modal-center">
                                                <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title"></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12">
                                                                <div class="example-wrap">
                                                                    <div class="example">
                                                                    <!-- BEGIN FORM-->
                                                                        <form id="otherbookchargesform" autocomplete="off" method ="post" action="{{ url('othercharges/bookstore') }}" enctype="multipart/form-data">
                                               {{csrf_field()}}
 <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="class" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material">
                                <label class="control-label">Book Name
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="book_name" placeholder="" required="">
                                </div>
                                @if ($errors->has('dress_charges'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('book_name') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                            </div>
                        <div class="form-group form-material">
                                <label class="control-label">Book Charges
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="book_charges" placeholder="" required="">
                                </div>
                                @if ($errors->has('book_charges'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('book_charges') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                            </div>
                                            <div class="modal-footer">
                                    <div class="form-group">
                             <button type="submit" class="btn btn-primary" id="validateButton_other">Submit</button>

                                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                            </div>
                                                </div>
                                                                        </form>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div><!-- Modal Body-->
                                                                    </div>
                                                                    </div>
                                                                    </div><!-- Main Modal-->
                                    <!--Book Modal End-->
                                    
                                    <div class="modal fade" id="other_dress_charg" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-simple modal-center">
                                                <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title"></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12">
                                                                <div class="example-wrap">
                                                                    <div class="example">
                                                                    <!-- BEGIN FORM-->
                                                                        <form id="otherdresschargesform" autocomplete="off" method ="post" action="{{ url('othercharges/dressstore') }}" enctype="multipart/form-data">
                                               {{csrf_field()}}
 <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="class" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                        <div class="form-group form-material">
                                <label class="control-label">Dress Charges
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="dress_charges" placeholder="" required="">
                                </div>
                                @if ($errors->has('dress_charges'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dress_charges') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                            </div>
                                            <div class="modal-footer">
                                    <div class="form-group">
                             <button type="submit" class="btn btn-primary" id="validateButton_other">Submit</button>

                                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                            </div>
                                                </div>
                                                                        </form>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div><!-- Modal Body-->
                                                                    </div>
                                                                    </div>
                                                                    </div><!-- Main Modal-->
                                    <!--Books Modal-->
               <div class="page-content container-fluid">
      <!-- Panel Tabs -->
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title"></h3>

        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">
              
            <div class="col-xl-12">
              <!-- Example Tabs -->
              <div class="example-wrap">
                <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne"
                        aria-controls="exampleTabsOne" role="tab">Transport Charges</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo"
                        aria-controls="exampleTabsTwo" role="tab">Dress Charges</a></li>
                         <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsThree"
                        aria-controls="exampleTabsTwo" role="tab">Book Charges</a></li>
                    
                    
                    
                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#other_charg">Add Route</button>

                       @if(count($route_data) > 0)
                        <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                            <tr>

                            </tr>
                          <tr>
                            <th>#</th>
                            <th>Vehicle Number</th>
                            <th>Driver Name</th>
                            <th>Driver Number</th>
                            <th>Helper</th>
                            <th>Helper Number</th>
                            <th>Count Student</th>
                            <th>Charges</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>                        <?php $i=1;?>
                    @foreach($route_data as $routes)
                            <tr id="class">
                                <td>{{$i++}}</td>
                        <td>{{$routes->vehicle_number}}</td>
                        <td>{{$routes->driver_name}}</td>
                        <td>{{$routes->driver_contact}}</td>
                        <td>{{$routes->helper}}</td>
                        <td>{{$routes->helper_contact}}</td>
                        <td>{{$routes->count_student}}</td>
                        <td>{{$routes->charges}}</td>
 @if($routes->status == '0' || $routes->status == NULL)
                                   <td><span style="color:blue">Pending</span></td>
@elseif($routes->status == '1')
<td><!--button type="button" class="btn btn-floating btn-success btn-sm waves-effect waves-classic waves-effect waves-classic" title="approved" data-toggle="modal" data-target="#myroute-{{$routes->id}}" data-id="" disabled>
<i class="fas fa-check-circle"></i> </button--> <span style="color:green">Approved</span></td>  
@elseif($routes->status == '2')
<td><!--button type="button" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic" title="decline" data-toggle="modal" data-target="" data-id="" disabled>
<i class="fas fa-close"></i> </button--> <span style="color:red">Rejected</span></td>  
@endif

                    <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#myfeedetail-{{$routes->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#mydelete-{{$routes->id}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <div class="modal fade" id="mydelete-{{$routes->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Are you sure?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                <form action="{{ route('othercharges.destroy',$routes->id) }}" class="horizontal-form" method="post">

                                                        <h3>Do you want to delete Vehicle - {{$routes->vehicle_number}}-{{$routes->driver_name}}</h3>
                                                         <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Yes</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                    </form>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                    <div class="modal fade" id="myfeedetail-{{$routes->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <!-- BEGIN FORM-->
                                                <form action="{{ route('othercharges.update',$routes->id) }}" class="horizontal-form" method="post">
                                                {{ csrf_field() }}
                                                  
                                                    <div class="row">
                                                        <input type="hidden" name="classid" value=""/>
                                                        <input type="hidden" name="schoolid" value="{{$routes->schoolid}}"/>
                                                               <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">{{$routes->id}} </label>
                                                           </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="form-group form-material">
                                <label class="control-label">Vehicle Number
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="vehcle" placeholder=""
                                           value="{{$routes->vehicle_number}}">
                                </div>
                            </div>
                               
                            </div>
                            <div class="form-group form-material">
                                <label class="control-label">Driver Name
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="dr_name" placeholder=""
                                           value="{{$routes->driver_name}}">
                                </div>
                            </div>
                            <div class="form-group form-material">
                                <label class="control-label">Driver Contact Number
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="dr_contact" placeholder=""
                                           value="{{$routes->driver_contact}}">
                                </div>
                            </div>
                            <div class="form-group form-material">
                                <label class="control-label">Helper 
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="helper" placeholder=""
                                           value="{{$routes->helper}}">
                                </div>
                            </div>
                            <div class="form-group form-material">
                                <label class="control-label">Helper contact number
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="helper_contact" placeholder=""
                                           value="{{$routes->helper_contact}}">
                                </div>
                            </div>
                            
                            <div class="form-group form-material">
                                <label class="control-label">Charges (P/M)
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="charges" placeholder=""
                                           value="{{$routes->charges}}">
                                            <input type="hidden" class="form-control" name="status" placeholder=""
                                           value="{{$routes->status}}">
                                </div>
                            </div>
                           
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Update</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div><!--Moday-Body-->
                                        </div><!--Modal Content-->
                                    </div>
                                </div><!-- Main Modal-->
                                                                </td>
                                                                
                            </tr>
                            @endforeach
                    </tbody>
                </table> 
                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                       <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                            <tr>
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#other_dress_charg">Add Dress Charge</button>

                            </tr>
                          <tr>
                            <th>#</th>
                            <th>Class</th>
                            <th>Charges(Whole Dress)</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>                        <?php $i=1;?>

                    @foreach($dress_charges as $d_c)
                            <tr id="class">
                                <td>{{$i++}}</td>
                        <td>{{$d_c->classname}}</td>
                        <td>{{$d_c->dress_charges}}</td>
                        


                    <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#mydressdetail-{{$d_c->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#mydress-{{$d_c->id}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <div class="modal fade" id="mydress-{{$d_c->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Are you sure?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                <form action="{{ route('dress.destroy',$d_c->id) }}" class="horizontal-form" method="post">

                                                        <h3>Do you want to delete</h3> 
                                                         <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Yes</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                    </form>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                    <div class="modal fade" id="mydressdetail-{{$d_c->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <!-- BEGIN FORM-->
                                                <form action="{{ route('dress.update',$d_c->id) }}" class="horizontal-form" method="post">
                                                {{ csrf_field() }}
                                                  
                                                    <div class="row">
                                                        <input type="hidden" name="classid" value=""/>
                                                        <input type="hidden" name="schoolid" value="{{$d_c->schoolid}}"/>
                                                               <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">{{$d_c->id}} </label>
                                                           </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="class" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                                        
                                <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Dress Charges</label>
                                    <input type="text" class="form-control" name="dress_charges" placeholder="" value="{{$d_c->dress_charges}}">
                                </div>
                            </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Update</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div><!--Moday-Body-->
                                        </div><!--Modal Content-->
                                    </div>
                                </div><!-- Main Modal-->
                                                                </td>
                                                                
                            </tr>
                            @endforeach
                    </tbody>
                </table> 
                @endif
                    </div>
                    <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                     <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                            <tr>
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#other_book_charg">Add Book Charge</button>

                            </tr>
                          <tr>
                            <th>#</th>
                            <th>Class</th>
                            <th>Book Name</th>
                            <th>Book Charges</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>                        <?php $i=1;?>

                    @foreach($book_charges as $b_c)
                            <tr id="class">
                                <td>{{$i++}}</td>
                        <td>{{$b_c->classname}}</td>
                        <td>{{$b_c->book_name}}</td>
                        <td>{{$b_c->book_charges}}</td>
                        


                    <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#mybookdetail-{{$b_c->id}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic" title="class" data-toggle="modal" data-target="#mybook-{{$b_c->id}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <div class="modal fade" id="mybook-{{$b_c->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Are you sure?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                <form action="{{ route('book.destroy',$b_c->id) }}" class="horizontal-form" method="post">

                                                        <h3>Do you want to delete</h3> 
                                                         <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Yes</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                    </form>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                    <div class="modal fade" id="mybookdetail-{{$b_c->id}}" role="dialog">
                                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <!-- BEGIN FORM-->
                                                <form action="{{ route('book.update',$b_c->id) }}" class="horizontal-form" method="post">
                                                {{ csrf_field() }}
                                                  
                                                    <div class="row">
                                                        <input type="hidden" name="classid" value=""/>
                                                        <input type="hidden" name="schoolid" value="{{$b_c->schoolid}}"/>
                                                               <div class="form-group form-material col-md-6">
                                                            <label for="inputChecked">{{$b_c->id}} </label>
                                                           </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="class" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                     <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Book Name</label>
                                    <input type="text" class="form-control" name="book_name" placeholder="" value="{{$b_c->book_name}}">
                                </div>
                                                        
                                <div class="form-group form-material">
                                    <label class="control-label" for="inputClass">Book Charges</label>
                                    <input type="text" class="form-control" name="book_charges" placeholder="" value="{{$b_c->book_charges}}">
                                </div>
                            </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-info">Update</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div><!--Moday-Body-->
                                        </div><!--Modal Content-->
                                    </div>
                                </div><!-- Main Modal-->
                                                                </td>
                                                                <!-- toaster-->
                                                                

                                                                <!-- -->
                                                                
                            </tr>
                            @endforeach
                    </tbody>
                </table> 
                
                    </div>
                   
                    
                  </div>
                </div>
              </div>
              
              <!-- End Exmaple Close Button Notifacations -->
              
            <!-- toaster-->
            </div>
        </div>
    </div>
</div>


@endsection