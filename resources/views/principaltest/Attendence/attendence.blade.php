<div class="example">
    <form action="{{url('slider/update',$sliders->id)}}" method="post" role="form" id="school-edit-{{$sliders->id}}" class="school-update-{{$sliders->id}}" enctype="multipart/form-data">
    {{method_field('PATCH')}}
        <div class="form-group form-material">
            <input type="text" name="topcontent" id="name" class="form-control"  
            data-msg-date="The Name field is required. must be a date." data-msg-required="The Content field is required." 
            data-rule-date="true" data-rule-required="true" value="{{ucwords($sliders->topcontent)}}" required>
            @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
        </div>
        <div class="form-group form-material">
            <input type="text" name="bottomcontent" id="principalname" class="form-control" 
            data-msg-date="The Content field is required. must be a date." data-msg-required="The Content field is required." 
            data-rule-date="true" data-rule-required="true" value="{{$sliders->bottomcontent}}" required>
            @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
        </div>
        <div class="form-group form-material">
            <label for="slider">slider: </label><br>
            <img src="/theme/mainimage/{{$sliders->slider}}" width="200" height="100"><br>
            <!--<input type="file" name="filename" id="email" class="form-control" required>-->
            @if ($errors->has('file'))<p class="help-block">{{ $errors->first('file')}}</p>@endif
        </div>
        <div class="form-group form-material">
            <input type="file" name="filename" id="input-file-now-custom-1" data-plugin="dropify" data-default-file=""  
            data-msg-required="The image is required." 
            data-rule-required="true" placeholder="image">
            @if ($errors->has('filename'))<p class="help-block">{{ $errors->first('filename')}}</p>@endif
        </div>
        <div class="form-group form-material">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$sliders->id}}').submit()">Save changes</button>
            </div>
        </div>
    </form>
</div>