@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
    		    <div class="alert alert-success">
    		        {{ session()->get('message') }}
    		    </div>
    		@endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <!--<button class="btn btn-primary" data-target="#studentattendence" data-toggle="modal"-->
            <!--    type="button">Add Student Attendence</button>-->
            <div class="row">
                <div class="col-md-4">
                    <div class="datepair-wrap" data-plugin="datepair">
                        <div class="input-daterange-wrap">
                            <div class="input-daterange" data-date="<?php echo date('d/m/Y');?>">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                    </span>
                                    <input type="text" class="form-control datepair-date datepair-start date-pair-select" data-plugin="datepicker" name="datestart" value="<?php echo date('m/d/Y');?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <select name="classsid" id="classselect" class="form-control">
                        <option>--Select Classes--</option>
                        @foreach($classes as $class)
                            <option value="{{$class->id}}">{{$class->class_display_name}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" value="" class="newclasselect newclassid">
                </div>
                <div class="col-md-4">
                    <select name="sectionid" id="sectionselectid" class="form-control sectionselectid">
                        <option>--Select Section--</option>
                        @foreach($section as $sections)
                            <option value="{{$sections->id}}">{{$sections->section_display_name}}</option>
                        @endforeach
                    </select>
                </div>
                
            </div>
            <div class="panel-body container-fluid">
                    @if(Entrust::hasRole('principal'))
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Section</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($attendence as $attendences)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$attendences->name}}</td>
                                <td>{{$attendences->class_display_name}}</td>
                                <td>{{$attendences->section_display_name}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @elseif(Entrust::hasRole('teacher'))
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- BEGIN FORM-->
                                <form action="{{url('attendence/store')}}" class="horizontal-form" method="post">
                                {{ csrf_field() }}
                                    <div class="form-group form-material">
                                        <div class="col-md-6">
                                            <input type="hidden" name="teacherid" value="{{$teacher->id}}"/>
                                            <input type="hidden" name="schoolid" value="{{$teacher->schoolid}}"/>
                                            <input type="hidden" class="classid" name="classid" value=""/>
                                            <input type="hidden" class="sectionid" name="sectionid" value=""/>
                                            <input type="hidden" class="newpreviousdate" name="previousdate" value=""/>
                                        </div>
                                    </div>
                                    <div class="form-group form-material">
                                        <table class="table table-hover datatable table-striped w-full studentattendencedata">
                                            <thead>
                                              <tr>
                                                <th>Student Name</th>
                                                <th>Action</th>
                                              </tr>
                                            </thead>
                                            <tbody class="aprad">
                                            </tbody>
                                        </table>
                                        <!--<div class="col-md-12 aprad">-->
                                        <!--</div>-->
                                    </div>
                                    <div class="modal-footer savehide">
                                    <button type="submit" class="btn btn-primary" value="Save">Save Changes</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
            </div>
        </div>
    </div>

@endsection