 @if(!empty($newdata))
    <form action="{{url('class/classupdate')}}" class="horizontal-form" method="post">
        @else
            <form action="{{url('class/classstore')}}" class="horizontal-form" method="post">
        @endif
        {{ csrf_field() }}
        <div class="form-group form-matrial maintain-height">
            <input type="hidden" name="schoolname" id="schoolname" class="form-control" value="{{$user->schoolname}}" readonly>
        <div class="classes">
            <h4>Classes</h4>
        </div>
        <div class="section">
            <h4>Sections</h4>
        </div>
        @foreach($myclass as $myclasses)
            @if(!empty($newdata))
            <div class="margin-bottom">
                <div class="checkbox-custom checkbox-primary classes">
                    <input type="checkbox" id="inputChecked{{$myclasses->id}}"  <?php if(in_array($myclasses->id,$newdata)){echo 'checked';}?> name="classname[]"  value="{{$myclasses->id}}"/>
                    <label for="inputChecked{{$myclasses->id}}" id="{{$myclasses->class_name}}">{{$myclasses->class_display_name}}</label>
                </div>
                <div class="section">
                    <?php $i=0;?>
                    @foreach($sectionclass as $sectionclasses)
                        @if(!empty($tempsec))
                            @if(0 == $myclasses->has_stream)
                            <?php $secid = $sectionclasses->id.$i++.$myclasses->id;?>
                                <div class="checkbox-custom checkbox-primary change-display-property">
                                    <input type="checkbox" id="inputChecked<?php echo $secid;?>" name="sectionclass[{{$myclasses->id}}][]" <?php if(!empty($classsection_map[$myclasses->id]) && in_array($sectionclasses->id,$classsection_map[$myclasses->id])){echo 'checked';}?> value="{{$sectionclasses->id}}"/>
                                    <label for="inputChecked<?php echo $secid;?>">{{$sectionclasses->section_display_name}}</label>
                                </div>
                            @endif
                        @endif
                    @endforeach
                    @if(1 == $myclasses->has_stream)
                        <?php $i=0;?>
                        @foreach($stream as $streams)
                        <?php $streamid = $streams->id.$i++.$myclasses->id;?>
                            <div class="checkbox-custom checkbox-primary">
                                <input type="checkbox" id="inputChecked<?php echo $streamid;?>" name="sectionclass[{{$myclasses->id}}][]" <?php if(!empty($classsection_map[$myclasses->id]) && in_array($streams->id,$classsection_map[$myclasses->id])){echo 'checked';}?> value="{{$streams->id}}"/>
                                <label for="inputChecked<?php echo $streamid;?>">{{$streams->highsubname}}</label>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
            @else
            <div class="checkbox-custom checkbox-primary">
                <input type="checkbox" id="inputChecked{{$myclasses->class_name}}" name="classname[]" value="{{$myclasses->id}}"/>
                <label for="inputChecked{{$myclasses->class_name}}">{{$myclasses->class_display_name}}</label>
            </div>
            @endif
        @endforeach
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>