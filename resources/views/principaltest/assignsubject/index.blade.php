@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
                <button class="btn btn-primary" data-target="#examplePositionCenter" data-toggle="modal"
                type="button">Assign Subject</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover dataTable table-striped w-full table-responsive" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th> # </th>
                            <th>Classes</th>
                            <th>Subjects</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                            @foreach($class as $classes)
                            @if(0 == $classes->hasstream)
                              <tr id="class">
                                    <td>{{$i++}}</td>
                                    <td>
                                        {{$classes->cdisplay}}
                                    </td>
                                    <td>
                                        <!--display sections-->
                                        <?php $subs = []; ?>
                                       @foreach($subject as $subjects)
			                        		 @if(0 == $classes->hasstream)
                                                @if(!empty($classsection_map[$classes->cid]) && in_array($subjects->id,$classsection_map[$classes->cid]))
                                                    <?php $subs[] = $subjects->subjectname;?>
                                                @endif
                                            @endif
                                        @endforeach
                                        @if(1 == $classes->hasstream)
                                            @foreach($subject as $subjects)
                                                @if(!empty($classsection_map[$classes->cid]) && in_array($subjects->id,$classsection_map[$classes->cid]))
                                                    <?php $subs[] = $subjects->subjectname;?>
                                                @endif
                                            @endforeach
                                        @endif
                                            @if( empty( $subs ) )
                                                <span class="text-danger">No Subjects</span>
                                            @else
                                                @foreach( $subs as $sub )
                                                <span class="badge badge-primary badge-lg">{{ $sub }}</span>
                                                @endforeach
                                            @endif
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            @foreach($class as $classes)
                             @foreach($streamfirst as $streamfirsts)
                              @if(14 == $classes->cid)
                              <tr id="class">
                                    <td>{{$i++}}</td>
                                    <td>
                                    @if(1 == $classes->hasstream)
                                     {{$classes->cdisplay}}
                                     ({{$streamfirsts->highsubname}})
                                    @endif
                                    </td>
                                    <td>
                                        <!--display sections-->
                                       @if(1 == $classes->hasstream)
                                           <?php
                                           $subs = array();
                                           ?>
                                            @foreach($subject as $subjects)
                                                @if(!empty($classsection_map[$classes->cid][$streamfirsts->sectionid]) && in_array($subjects->id,$classsection_map[$classes->cid][$streamfirsts->sectionid]))
                                                    <?php
                                                    $subs[] = $subjects->subjectname;
                                                    ?>
                                                @endif
                                            @endforeach
                                            @if( empty( $subs ) )
                                                <span class="text-danger">No Subjects</span>
                                            @else
                                                @foreach( $subs as $sub )
                                                <span class="badge badge-primary badge-lg">{{ $sub }}</span>
                                                @endforeach
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @endif
                             @endforeach
                            @endforeach
                            @foreach($class as $classes)
                             @foreach($streamsecond as $streamseconds)
                              @if(15 == $classes->cid)
                              <tr id="class">
                                    <td>{{$i++}}</td>
                                    <td>
                                    @if(1 == $classes->hasstream)
                                     {{$classes->cdisplay}}
                                     ({{$streamseconds->highsubname}})
                                    @endif
                                    </td>
                                    <td>
                                        <!--display sections-->
                                       @if(1 == $classes->hasstream)
                                           <?php
                                           $subs = array();
                                           ?>
                                            @foreach($subject as $subjects)
                                                @if(!empty($classsection_map[$classes->cid][$streamseconds->sectionid]) && in_array($subjects->id,$classsection_map[$classes->cid][$streamseconds->sectionid]))
                                                    <?php
                                                    $subs[] = $subjects->subjectname;
                                                    ?>
                                                @endif
                                            @endforeach
                                            @if( empty( $subs ) )
                                                <span class="text-danger">No Subjects</span>
                                            @else
                                                @foreach( $subs as $sub )
                                                <span class="badge badge-primary badge-lg">{{ $sub }}</span>
                                                @endforeach
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @endif
                             @endforeach
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="examplePositionCenter" aria-hidden="true" aria-labelledby="examplePositionCenter"
                                      role="dialog" tabindex="-1">
                                        <div class="modal-dialog modal-simple modal-center">
                                            <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title">Add/Remove Classes</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="modal-body ">
                                                        @if(!empty($newdata))
                                                            <form action="{{url('assignsubject/assignsubjectupdate')}}" class="horizontal-form" method="post">
                                                        @else
                                                            <form action="{{url('assignsubject/assignsubjectstore')}}" class="horizontal-form" method="post">
                                                        @endif
                                                        {{ csrf_field() }}
                                                        <div class="form-group form-matrial maintain-height">
                                                            <input type="hidden" name="schoolname" id="schoolname" class="form-control" value="{{$user->schoolname}}" readonly>
                                                            <input type="hidden" name="schoolid" id="schoolid" class="form-control" value="{{$user->schoolid}}" readonly>
                                                        <div class="classes">
                                                            <h4>Classes</h4>
                                                        </div>
                                                        <div class="section">
                                                            <h4>Sections</h4>
                                                        </div>
                                                        @foreach($myclass as $myclasses)
                                                            @if(!empty($newdata))
                                                            <div class="margin-bottom">
                                                                <div class="checkbox-custom checkbox-primary classes">
                                                                    <?php if(in_array($myclasses->cid,$newdata)){?>
                                                                    <input type="hidden" name="classes[]" value="{{$myclasses->cid}}">
                                                                    @if(0 == $myclasses->has_stream)
                                                                    {{$myclasses->class_display_name}}
                                                                    @endif
                                                                    <?php $i=0;?>
                                                                    
                                                                    <?php } ?>
                                                                </div>
                                                                <?php $i=0;?>
                                                                    <div class="select2-primary section">
                                                                       @if(0 == $myclasses->has_stream)
                                                                        <select class="form-control" name="subjects[{{$myclasses->cid}}][]" multiple="multiple" data-plugin="select2">
                                                                            @foreach($subject as $subjects)
                                                                                <?php $subid = $subjects->id.$i++.$myclasses->cid;?>
                                                                                <option <?php if(!empty($classsection_map[$myclasses->cid]) && in_array($subjects->id,$classsection_map[$myclasses->cid])){echo 'selected';}?> value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @endif
                                                                    </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            @endif
                                                        @endforeach
                                                        @foreach($myclass as $myclasses)
                                                          @foreach($streamfirst as $streamfirsts)
                                                            @if(!empty($newdata))
                                                            <div class="margin-bottom">
                                                                <div class="checkbox-custom checkbox-primary classes">
                                                                    <?php if(in_array($myclasses->cid,$newdata)){?>
                                                                    <input type="hidden" name="classes[]" value="{{$myclasses->cid}}">
                                                                    @if(1 == $myclasses->has_stream)
                                                                    @if(14 == $myclasses->cid)
                                                                       {{$myclasses->class_display_name}}
                                                                       {{$streamfirsts->highsubname}}
                                                                    @endif
                                                                    @endif
                                                                    
                                                                    <?php } ?>
                                                                </div>
                                                                <?php $i=0;?>
                                                                @if(14 == $myclasses->cid)
                                                                    <div class="select2-primary section">
                                                                       @if(1 == $myclasses->has_stream)
                                                                        <select class="form-control" name="subjects_s[{{$myclasses->cid}}][{{$streamfirsts->hid}}][]" multiple="multiple" data-plugin="select2">
                                                                            @foreach($subject as $subjects)
                                                                                <?php $subid = $subjects->id.$i++.$myclasses->cid;?>
                                                                                <option <?php if(!empty($classsection_map[$myclasses->cid][$streamfirsts->hid]) && in_array($subjects->id,$classsection_map[$myclasses->cid][$streamfirsts->hid])){echo 'selected';}?> value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @endif
                                                                    </div>
                                                                @endif
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            @endif
                                                        @endforeach
                                                        @endforeach
                                                        @foreach($myclass as $myclasses)
                                                          @foreach($streamsecond as $streamseconds)
                                                            @if(!empty($newdata))
                                                            <div class="margin-bottom">
                                                                <div class="checkbox-custom checkbox-primary classes">
                                                                    <?php if(in_array($myclasses->cid,$newdata)){?>
                                                                    <input type="hidden" name="classes[]" value="{{$myclasses->cid}}">
                                                                    @if(1 == $myclasses->has_stream)
                                                                    @if(15 == $myclasses->cid)
                                                                       {{$myclasses->class_display_name}}
                                                                       {{$streamseconds->highsubname}}
                                                                    @endif
                                                                    @endif
                                                                    <?php } ?>
                                                                </div>
                                                                <?php $i=0;?>
                                                                @if(15 == $myclasses->cid)
                                                                    <div class="select2-primary section">
                                                                       @if(1 == $myclasses->has_stream)
                                                                        <select class="form-control" name="subjects_s[{{$myclasses->cid}}][{{$streamseconds->hid}}][]" multiple="multiple" data-plugin="select2">
                                                                            @foreach($subject as $subjects)
                                                                                <?php $subid = $subjects->id.$i++.$myclasses->cid;?>
                                                                                <option <?php if(!empty($classsection_map[$myclasses->cid][$streamseconds->hid]) && in_array($subjects->id,$classsection_map[$myclasses->cid][$streamseconds->hid])){echo 'selected';}?> value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @endif
                                                                    </div>
                                                                @endif
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            @endif
                                                        @endforeach
                                                        @endforeach
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                    </form>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                <!-- End Modal -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection