<div class="tableoverflow">
    <table class="table table-hover dataTable table-striped w-full table-responsive" data-plugin="dataTable">
        <thead>
            <tr>
                <th>RollNumber</th>
                <th>Student Name</th>
                <th>DOB</th>
                <th>Gender</th>
                <th>Father Name</th>
                <th>Mother Name</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
        <?php $i=1;?>
        @foreach($admission as $admissions)
            
                <tr id="class">
                    <td>{{$admissions->rollnumber}}</td>
                    <td>{{$admissions->uname}}</td>
                    <td>{{$admissions->DOB}}</td>
                    <td>{{$admissions->gender}}</td>
                    <td>{{$admissions->father_name}}/{{$admissions->father_contact_number}}</td>
                    <td>{{$admissions->mother_name}}/{{$admissions->mother_contact_number}}</td>
                    <td>{{$admissions->address}},{{$admissions->cityname}},{{$admissions->sname}},{{$admissions->cname}},{{$admissions->pincode}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>