@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
                <button class="btn btn-primary" data-target="#examplePositionCenter" data-toggle="modal"
                type="button">Add/Edit class</button>
                <div class="panel-body container-fluid">
                    <div class="tableoverflow">
                        <table class="table table-hover dataTable table-striped w-full table-responsive" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th> # </th>
                            <th>Classes</th>
                            <th>Sections</th>
                            <th>Male Students</th>
                            <th>Female Students</th>
                            <th>Total Students</th>
                            <th>View Students</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @if(!empty($class))
                            @foreach($class as $classes)
                            <?php
                            $male_students = $female_students = $total_students = 0;
                            foreach( $gender_data as $g_data ) {
                                if( $classes->cid == $g_data->classid ) {
                                    $male_students = $g_data->total_males;
                                    $female_students = $g_data->total_females;
                                    $total_students += $male_students;
                                    $total_students += $female_students;
                                }
                            }
                            ?>
                                <tr id="class">
                                    <td>{{$i++}}</td>
                                    <td><!--a href="#" data-target="#examplePositionClass-{{$classes->cid}}" data-toggle="modal"-->{{$classes->cdisplay}}<!--/a--></td>
                                    <td>
                                        <!--display sections-->
                                        @foreach($sectionclass as $sectionclasses)
                                       <?php //dd($sectionclasses->id);?> 

                                            @if(!empty($tempsec))
                                                @if(0 == $classes->hasstream)
                                                    @if(!empty($classsection_map[$classes->cid]) && in_array($sectionclasses->id,$classsection_map[$classes->cid]))
                                                        <!--a href="#" data-target="#examplePositionSectionClass-{{$sectionclasses->id}}" data-toggle="modal"-->{{$sectionclasses->section_display_name}}<!--/a-->
                                                    @endif
                                                @endif
                                            @endif
                                             <!--Section Modal-->
                                            <div class="modal fade" id="examplePositionSectionClass-{{$sectionclasses->id}}" aria-hidden="true" aria-labelledby="examplePositionCenter"
                                              role="dialog" tabindex="-1" data-classsecid="{{$classes->cid}}" data-schoolid="{{$schoolid->id}}" data-studentsectionclass_id="{{$sectionclasses->id}}">
                                                <div class="modal-dialog modal-simple modal-center">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title">All Section Detail</h4>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="modal-body ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @if(1 == $classes->hasstream)
                                            @foreach($stream as $streams)
                                                @if(!empty($classsection_map[$classes->cid]) && in_array($streams->id,$classsection_map[$classes->cid]))
                                                    <?php echo $streams->highsubname.',';?>
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{$male_students}}</td>
                                    <td>{{$female_students}}</td>
                                    <td>{{$total_students}}</td>
                                     <td><button type="button" id="student_det" value="{{$classes->cid}}" class="btn btn-floating btn-success btn-sm waves-effect waves-classic waves-effect waves-classic" title="detail" data-toggle="modal" data-target="#mydetail-{{$classes->cid}}"  onClick="javascript:getdatastudent(this.value);">
                                            <i class="icon md-account"></i>
                                        </button>
                                        </td>
                                                     <!--a href="/class/view"><i class="fas fa-eye"></i></a></td-->
<!--Details Student-->
         <div class="modal fade" id="mydetail-{{$classes->cid}}" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-simple modal-center">
                                                <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">Student Names</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-lg-12">

                                                                    <!-- BEGIN FORM-->
                                                                       <div class="tableoverflow">
                                                                                  <div id="re">
                                                                               
                                                                                  </div>
                                                                                  </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
<!--Student Detials Modal-->
                                    
                                    <!--Class Modal -->
                                    <div class="modal fade" id="examplePositionClass-{{$classes->cid}}" aria-hidden="true" aria-labelledby="examplePositionCenter"
                                      role="dialog" tabindex="-1" data-studentclass_id="{{$classes->cid}}">
                                        <div class="modal-dialog modal-simple modal-center">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title">All Detail</h4>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="modal-body ">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td>No Class</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    </div>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="examplePositionCenter" aria-hidden="true" aria-labelledby="examplePositionCenter"
                                      role="dialog" tabindex="-1">
                                        <div class="modal-dialog modal-simple modal-center">
                                            <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title">Add/Remove Classes</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="modal-body ">
                                                        @if(!empty($newdata))
                                                            <form action="{{url('class/classupdate')}}" class="horizontal-form" method="post">
                                                        @else
                                                            <form action="{{url('class/classupdate')}}" class="horizontal-form" method="post">
                                                        @endif
                                                        {{ csrf_field() }}
                                                        <div class="form-group form-matrial maintain-height">
                                                            <input type="hidden" name="schoolname" id="schoolname" class="form-control" value="{{$user->schoolname}}" readonly>
                                                        <div class="classes">
                                                            <h4>Classes</h4>
                                                        </div>
                                                        
                                                        <div class="section">
                                                            <h4>Sections <button class="btn btn-primary customsection">Custom</button>
                                                            <div class="section">
                                                                    <input type="text" id="customnewsectionadd" value="{{ implode( ',', $custom_sections ) }}" name="customsection[]" data-plugin="tokenfield">
                                                                    <a href="#" class="fa fa-check classsectionclose"></a>
                                                                        <!--<a href="#" class="fa fa-close classsectionclose"></a>-->
                                                                </div>
                                                            </h4>
                                                        </div>
                                                        <?php //echo $newdata;?>
                                                            
                                                        @foreach($myclass as $myclasses)
                                                         @if(!empty($newdata))
                                                            <div class="margin-bottom">
                                                                <div class="checkbox-custom checkbox-primary classes">
                                                                    <input class="class_check" type="checkbox" id="inputChecked{{$myclasses->id}}" <?php if(in_array($myclasses->id,$newdata)){echo 'checked';}?> disabled />
                                                                    <label>{{$myclasses->class_display_name}}</label>
                                                                </div>
                                                                <div class="section sectioncheck {{ !$myclasses->has_stream ? 'class_only_sections' : '' }}">
                                                                    <?php $i=0;?>
                                                                    @foreach($sectionclass as $sectionclasses)
                                                                        @if(!empty($tempsec))
                                                                            @if(0 == $myclasses->has_stream)
                                                                            <?php $secid = $myclasses->id. '_' . $sectionclasses->id;?>
                                                                                  <div class="checkbox-custom checkbox-primary change-display-property ">
                                                                                    <input type="checkbox" data-class_id="{{$myclasses->id}}" data-custom="{{ $sectionclasses->schoolid ? 1 : 0 }}" class="sectioncheckedfirst" id="inputChecked<?php echo $secid;?>" name="sectionclass[{{$myclasses->id}}][]" data-section="<?php if(!empty($classsection_map[$myclasses->id]) && in_array($sectionclasses->id,$classsection_map[$myclasses->id])){echo 'checked';}?>" <?php if(!empty($classsection_map[$myclasses->id]) && in_array($sectionclasses->id,$classsection_map[$myclasses->id])){echo 'checked';}?> value="{{$sectionclasses->id}}"/>
                                                                                    <label for="inputChecked<?php echo $secid;?>">{{$sectionclasses->section_display_name}}</label>
                                                                                </div>
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                    @if(1 == $myclasses->has_stream)
                                                                        <?php $i=0;?>
                                                                        @foreach($stream as $streams)
                                                                        <?php $streamid = $streams->id.$i++.$myclasses->id;?>
                                                                            <div class="checkbox-custom checkbox-primary">
                                                                                <input type="checkbox" class="sectionclasschecked" id="inputChecked<?php echo $streamid;?>" name="sectionclass[{{$myclasses->id}}][]" <?php if(!empty($classsection_map[$myclasses->id]) && in_array($streams->id,$classsection_map[$myclasses->id])){echo 'checked';}?> value="{{$streams->id}}"/>
                                                                                <label for="inputChecked<?php echo $streamid;?>">{{$streams->highsubname}}</label>
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            @else
                                                            <div class="margin-bottom">
                                                            <div class="checkbox-custom checkbox-primary classes">
                                                                    <input class="class_check classnewcheck-{{$myclasses->id}}" type="checkbox" data-newclass_id="sectionnewclasschecked-{{$myclasses->id}}" id="inputChecked{{$myclasses->id}}" disabled />
                                                                    <label>{{$myclasses->class_display_name}}</label>
                                                            </div>
                                                            <div class="section sectioncheck {{ !$myclasses->has_stream ? 'class_only_sections' : '' }}">
                                                                    <?php $i=0;?>
                                                                    @foreach($sectionclass as $sectionclasses)
                                                                        @if(0 == $myclasses->has_stream)
                                                                            <?php $secid = $myclasses->id. '_' . $sectionclasses->id;?>
                                                                            <div class="checkbox-custom checkbox-primary change-display-property">
                                                                                <input type="checkbox"   data-class_id="{{$myclasses->id}}" data-custom="{{ $sectionclasses->schoolid ? 1 : 0 }}" class="sectionclasschecked sectionnewclasschecked" id="inputChecked<?php echo $secid;?>" name="sectionclass[{{$myclasses->id}}][]" value="{{$sectionclasses->id}}"/>
                                                                                <label for="inputChecked<?php echo $secid;?>">{{$sectionclasses->section_display_name}}</label>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach
                                                                    @if(0 == $myclasses->has_stream)
                                                                    <!--<button class="btn btn-primary">Custom Section</button>-->
                                                                    @endif
                                                                    @if(1 == $myclasses->has_stream)
                                                                        <?php $i=0;?>
                                                                        @foreach($stream as $streams)
                                                                        <?php $streamid = $streams->id.$i++.$myclasses->id;?>
                                                                            <div class="checkbox-custom checkbox-primary">
                                                                                <input type="checkbox" id="inputChecked<?php echo $streamid;?>" name="sectionclass[{{$myclasses->id}}][]" value="{{$streams->id}}"/>
                                                                                <label for="inputChecked<?php echo $streamid;?>">{{$streams->highsubname}}</label>
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            @endif
                                                        @endforeach
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                    </form>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                <!-- End Modal -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection