<div class="col-md-12">
    <div class="example-wrap">
        <div class="example">
            <!-- BEGIN FORM-->
            <form role="form" action="{{url('staff/update',$staffmembers->uid)}}" id="staffdata" method="post">
            {{csrf_field()}}
            {{method_field('PATCH')}}
                <div class="form-group form-material">
                    <input type="hidden" name="userid" value="{{$staffmembers->uid}}"/>
                    <input type="hidden" name="schoolid" value="{{$school->id}}"/>
                    <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Full Name" data-msg-date="The Content field is required." data-msg-required="The Name field is required." 
                    data-rule-date="true" data-rule-required="true" value="{{$staffmembers->uname}}">
                </div>
                <div class="form-group form-material">
                    <input type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="username" data-msg-date="The Username field is required." data-msg-required="The Username field is required." 
                    data-rule-date="true" data-rule-required="true" value="{{$staffmembers->username}}" >
                </div>
                <div class="form-group form-material">
                   <div class="input-group input-icon right">
                        <input id="email" class="input-error form-control" type="text" name="email" value="{{$staffmembers->uemail}}" data-msg-date="The mail is required." data-msg-required="The mail field is required." 
                        data-rule-date="true" data-rule-required="true" > 
                    </div>
                </div>
                <div class="row">
                    <div class="form-group form-material col-md-4">
                        <div class="datepair-wrap" data-plugin="datepair">
                            <div class="input-daterange-wrap">
                                <div class="input-daterange">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="doj" value="{{$staffmembers->doj}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-material col-md-4">
                        <input type="text" name="salary" class="form-control" id="exampleInputPassword1" placeholder="Salary" data-msg-date="The Salary field is required." data-msg-required="The Salary field is required." 
                        data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->salary}}">
                    </div>
                    <div class="form-group form-material col-md-4">
                        <input type="text" class="form-control" name="totaexp" id="exampleInputPassword1" placeholder="Total Experirnce" data-msg-date="The Experience field is required." data-msg-required="The Experience field is required." 
                        data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->total_experience}}" >
                    </div>
                </div>
                <div class="form-group form-material">
                    <?php $i=1;
                    $data = json_decode($staffmembers->roleid);?>
                    @foreach($role as $roles)
                    @foreach($data as $dataroles)
                        @if('student' != $roles->name)
                            <?php
                             $con='';
                              if (in_array($roles->id,$dataroles)) 
                                {
                                    $con = ' checked="checked"';
                                } 
                            ?>
                            <div class="checkbox-custom checkbox-primary">
                                <input type="checkbox" id="inputChecked{{$roles->name}}" name="roles[]" <?php echo $con; ?>  class="rolescheckedit rolescheckedit{{$roles->id}}" value="{{$roles->id}}"/ class="rolescheck">
                                <label for="inputChecked{{$roles->name}}">{{$roles->display_name}}</label>
                            </div>
                        @endif
                        @endforeach
                        <?php if( 'classincharge' === $roles->name ) { ?>
                        <div id="classchecknew">
                            <div class="form-group form-material" id="class-incharge-classes">
                                <?php $i=1;?>
                                @foreach($class as $classes)
                                    <?php 
                                        $con='';
                                          if (in_array($classes->id,$servicedataclassid)) 
                                            {
                                                $con = ' checked="checked"';
                                            } 
                                    ?>
                                    <div class="checkbox-custom checkbox-primary">
                                        <input type="checkbox" id="inputChecked{{$classes->class_name}}" class="classchecknew" name="classes[]" <?php echo $con; ?>  value="{{$classes->id}}"/>
                                        <label for="inputChecked{{$classes->class_name}}">{{$classes->class_display_name}}</label>
                                    </div>
                                    <?php if( 1 == $classes->has_stream ) { ?>
                                    
                                    <div id="streamchecknew" class="streamcheck{{$classes->id}}">
                                            <div class="form-group form-material" id="class-incharge-stream">
                                            <?php $i=1;?>
                                            @foreach($highclass as $highclasses)
                                                <?php 
                                                  $con ='';            
                                                  if (in_array($highclasses->id,$servicedatastreamid)) 
                                                    {
                                                        $con = ' checked="checked"';
                                                    } 
                                                ?>
                                                <div class="checkbox-custom checkbox-primary">
                                                    <input type="checkbox" id="inputChecked{{$highclasses->highsubname}}" name="stream[]" <?php echo $con; ?>  value="{{$highclasses->id}}"/>
                                                    <label for="inputChecked{{$highclasses->highsubname}}">{{$highclasses->highsubname}}</label>
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>
                                    <?php } ?>
                                @endforeach
                                </div>
                            </div>
                        <?php } ?>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update Changes</button>
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>