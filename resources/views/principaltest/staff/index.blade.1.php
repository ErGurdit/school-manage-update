@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
    ?>
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            <!-- Panel Modals Styles -->
            <div class="panel">
               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myClass">Add Staff Memeber</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover dataTable table-striped w-full table-responsive" data-plugin="dataTable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>DOJ</th>
                                <th>Role</th>
                                <th>Class Incharge</th>
                                <th>Leaves</th>
                                <th>Salary</th>
                                <th>Status</th>
                                <th>Action Buttons</th>
                            </tr>
                        </thead>
                            
                        <tbody>
                        <?php $i=1;?>
                            @foreach($staffmember as $staffmembers)
                                <tr id="class">
                                    <td>{{$staffmembers->uname}}</td>
                                    <td>{{$staffmembers->uemail}}</td>
                                    <td>{{$staffmembers->doj}}</td>
                                    <td>
                                        @foreach($servicerole as $v)
                                          {{str_replace('-',' ',$v->display_name).','}}
                                        @endforeach
                                    </td>
                                    <td>
                                         @foreach($serviceclass as $v)
                                          {{str_replace('-',' ',$v['class_display_name']).','}}
                                        @endforeach
                                    </td>
                                    <td></td>
                                    <td>{{$staffmembers->salary}}</td>
                                    <?php  
                                    $join = strtotime($staffmembers->doj);
                                    $now = strtotime(date('y-m-d'));
                                    $datediff = $join-$now;
                                    $diff = round($datediff / (60 * 60 * 24));
                                    ?>
                                    @if($diff >= 1)
                                        <td>Pending</td>
                                    @elseif($diff == 1)
                                        <td>Joining</td>
                                    @elseif($diff < 1)
                                        <td>Working</td>
                                    @endif
                                    <td>
                                        <button class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="edit" data-toggle="modal" data-target="#mydetailedit-{{$staffmembers->uid}}">
                                            <i class="icon md-edit"></i>
                                        </button>
                                        
                                        <button class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete editdel" title="staff"  data-id="{{$staffmembers->uid}}">
                                            <i class="icon md-delete"></i>
                                        </button>
                                        <button type="button" class="btn btn-floating btn-success btn-sm waves-effect waves-classic waves-effect waves-classic" title="detail" data-toggle="modal" data-target="#mydetail-{{$staffmembers->uid}}">
                                            <i class="icon md-account"></i>
                                        </button>
                                        <?php echo implode(',',$servicedataroleid)?>
                                        <div class="row row-lg">
                                            <!--Details view-->
                                            <div class="modal fade" id="mydetail-{{$staffmembers->uid}}" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-simple modal-center">
                                                <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">{{$staffmembers->uname}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12">
                                                                <div class="example-wrap">
                                                                    <div class="example">
                                                                    <!-- BEGIN FORM-->
                                                                        <form role="form" action="{{url('staff/update')}}" id="staffdata" method="post">
                                                                        {{csrf_field()}}
                                                                            <div class="form-group form-material">
                                                                                <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Full Name" data-msg-date="The Content field is required." data-msg-required="The Name field is required." 
                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->uname}}" readonly>
                                                                            </div>
                                                                            <div class="form-group form-material">
                                                                                <input type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="username" data-msg-date="The Username field is required." data-msg-required="The Username field is required." 
                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->username}}" readonly>
                                                                            </div>
                                                                            <div class="form-group form-material">
                                                                                <div class="input-group input-icon right">
                                                                                    <input id="email" class="input-error form-control" type="text" name="email" value="{{$staffmembers->uemail}}" data-msg-date="The mail is required." data-msg-required="The mail field is required." 
                                                                                    data-rule-date="true" data-rule-required="true" readonly> 
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="form-group form-material col-md-4">
                                                                                    <input type="text" class="form-control" placeholder="DOJ" name="doj" value="{{$staffmembers->doj}}" readonly> 
                                                                                </div>
                                                                                <div class="form-group form-material col-md-4">
                                                                                    <input type="text" name="salary" class="form-control" id="exampleInputPassword1" placeholder="Salary" data-msg-date="The Salary field is required." data-msg-required="The Salary field is required." 
                                                                                    data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->salary}}" readonly>
                                                                                </div>
                                                                                <div class="form-group form-material col-md-4">
                                                                                    <input type="text" class="form-control" name="totaexp" id="exampleInputPassword1" placeholder="Total Experirnce" data-msg-date="The Experience field is required." data-msg-required="The Experience field is required." 
                                                                                    data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->total_experience}}" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-material">
                                                                                
                                                                                @foreach($role as $roles)
                                                                                <?php 
                                                                                  $con='';
                                                                                  if (in_array($roles->id,$servicedataroleid)) 
                                                                                    {
                                                                                        $con = ' checked="checked"';
                                                                                    } 
                                                                                ?>
                                                                                <div class="checkbox-custom checkbox-default">
                                                                                    <input type="checkbox" id="inputCheckboxAgree" name="roles[]" <?php echo $con; ?>  value="{{$roles->id}}"/>
                                                                                    <label for="inputCheckboxAgree">{{$roles->display_name}}</label>
                                                                                </div>
                                                                                @endforeach
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <div class="form-group">
                                                                              <button type="button" class="btn btn-primary">Cancel</button>
                                                                            </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Detailsedit-->
                                            <div class="modal fade" id="mydetailedit-{{$staffmembers->uid}}" role="dialog">
                                                <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">{{$staffmembers->uname}}</h4>
                                                    </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12">
                                                                <div class="example-wrap">
                                                                    <div class="example">
                                                                        <!-- BEGIN FORM-->
                                                                        <form role="form" action="{{url('staff/update',$staffmembers->uid)}}" id="staffdata" method="post">
                                                                        {{csrf_field()}}
                                                                        {{method_field('PATCH')}}
                                                                            <div class="form-group form-material">
                                                                                <input type="hidden" name="userid" value="{{$staffmembers->uid}}"/>
                                                                                <input type="hidden" name="schoolid" value="{{$school->id}}"/>
                                                                                <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Full Name" data-msg-date="The Content field is required." data-msg-required="The Name field is required." 
                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->uname}}">
                                                                            </div>
                                                                            <div class="form-group form-material">
                                                                                <input type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="username" data-msg-date="The Username field is required." data-msg-required="The Username field is required." 
                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->username}}" >
                                                                            </div>
                                                                            <div class="form-group form-material">
                                                                               <div class="input-group input-icon right">
                                                                                    <input id="email" class="input-error form-control" type="text" name="email" value="{{$staffmembers->uemail}}" data-msg-date="The mail is required." data-msg-required="The mail field is required." 
                                                                                    data-rule-date="true" data-rule-required="true" > 
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="form-group form-material col-md-4">
                                                                                    <div class="datepair-wrap" data-plugin="datepair">
                                                                                        <div class="input-daterange-wrap">
                                                                                            <div class="input-daterange">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon">
                                                                                                      <i class="icon md-calendar" aria-hidden="true"></i>
                                                                                                    </span>
                                                                                                    <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="doj" value="{{$staffmembers->doj}}">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group form-material col-md-4">
                                                                                    <input type="text" name="salary" class="form-control" id="exampleInputPassword1" placeholder="Salary" data-msg-date="The Salary field is required." data-msg-required="The Salary field is required." 
                                                                                    data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->salary}}">
                                                                                </div>
                                                                                <div class="form-group form-material col-md-4">
                                                                                    <input type="text" class="form-control" name="totaexp" id="exampleInputPassword1" placeholder="Total Experirnce" data-msg-date="The Experience field is required." data-msg-required="The Experience field is required." 
                                                                                    data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->total_experience}}" >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-material">
                                                                                <?php $i=1;?>
                                                                                @foreach($role as $roles)
                                                                                    @if('student' != $roles->name)
                                                                                        <?php
                                                                                         $con='';
                                                                                          if (in_array($roles->id,$servicedataroleid)) 
                                                                                            {
                                                                                                $con = ' checked="checked"';
                                                                                            } 
                                                                                        ?>
                                                                                        <div class="checkbox-custom checkbox-primary">
                                                                                            <input type="checkbox" id="inputChecked{{$roles->name}}" name="roles[]" <?php echo $con; ?>  class="rolescheckedit rolescheckedit{{$roles->id}}" value="{{$roles->id}}"/ class="rolescheck">
                                                                                            <label for="inputChecked{{$roles->name}}">{{$roles->display_name}}</label>
                                                                                        </div>
                                                                                    @endif
                                                                                    <?php if( 'classincharge' === $roles->name ) { ?>
                                                                                    <div id="classchecknew">
                                                                                        <div class="form-group form-material" id="class-incharge-classes">
                                                                                            <?php $i=1;?>
                                                                                            @foreach($class as $classes)
                                                                                                <?php 
                                                                                                    $con='';
                                                                                                      if (in_array($classes->id,$servicedataclassid)) 
                                                                                                        {
                                                                                                            $con = ' checked="checked"';
                                                                                                        } 
                                                                                                ?>
                                                                                                <div class="checkbox-custom checkbox-primary">
                                                                                                    <input type="checkbox" id="inputChecked{{$classes->class_name}}" class="classchecknew" name="classes[]" <?php echo $con; ?>  value="{{$classes->id}}"/>
                                                                                                    <label for="inputChecked{{$classes->class_name}}">{{$classes->class_display_name}}</label>
                                                                                                </div>
                                                                                                <?php if( 1 == $classes->has_stream ) { ?>
                                                                                                
                                                                                                <div id="streamchecknew" class="streamcheck{{$classes->id}}">
                                                                                                        <div class="form-group form-material" id="class-incharge-stream">
                                                                                                        <?php $i=1;?>
                                                                                                        @foreach($highclass as $highclasses)
                                                                                                            <?php 
                                                                                                              $con ='';            
                                                                                                              if (in_array($highclasses->id,$servicedatastreamid)) 
                                                                                                                {
                                                                                                                    $con = ' checked="checked"';
                                                                                                                } 
                                                                                                            ?>
                                                                                                            <div class="checkbox-custom checkbox-primary">
                                                                                                                <input type="checkbox" id="inputChecked{{$highclasses->highsubname}}" name="stream[]" <?php echo $con; ?>  value="{{$highclasses->id}}"/>
                                                                                                                <label for="inputChecked{{$highclasses->highsubname}}">{{$highclasses->highsubname}}</label>
                                                                                                            </div>
                                                                                                        @endforeach
                                                                                                        </div>
                                                                                                    </div>
                                                                                                <?php } ?>
                                                                                            @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                @endforeach
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <div class="form-group">
                                                                                    <button type="submit" class="btn btn-primary">Update Changes</button>
                                                                                    <button type="button" class="btn default">Cancel</button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="modal hide fade" id="myClass" role="dialog" tabindex="-1" data-focus-on="input:first">
                            <div class="modal-dialog">
                            <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Staff Member</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" action="{{url('staff/store')}}" class="newstaff" method="post">
                                        {{csrf_field()}}
                                            <div class="form-group form-material">
                                                <input type="hidden" name="schoolid" value="{{$school->id}}"/>
                                                <input type="text" name="name" class="form-control" id="" placeholder="Full Name" data-msg-required="The Name field is required." 
                                                data-rule-required="true" required="true">
                                            </div>
                                            <div class="form-group form-material">
                                                <input type="text" name="username" class="form-control" id="" placeholder="username" data-msg-required="The Username field is required." 
                                                data-rule-required="true" required="true">
                                            </div>
                                            <div class="form-group form-material">
                                                <div class="input-group input-icon right">
                                                    <input id="email" class="input-error form-control" type="text" name="email" placeholder="email" value="" data-msg-required="The mail field is required." 
                                                    data-rule-required="true" required="true">
                                                </div>
                                            </div>
                                            <div class="form-group form-material">
                                                <input type="password" name="password" class="form-control" id="" placeholder="Password" data-msg-required="The password field is required." 
                                                data-rule-required="true" required="true">
                                            </div>
                                            <div class="row">
                                                <div class="form-group form-material col-md-4">
                                                    <div class="datepair-wrap" data-plugin="datepair">
                                                        <div class="input-daterange-wrap">
                                                            <div class="input-daterange">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="doj">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material col-md-4">
                                                    <input type="text" name="salary" class="form-control numbervalid" id="" placeholder="Salary" data-msg-required="The Salary field is required." 
                                                    data-rule-required="true"  required>
                                                </div>
                                                <div class="form-group form-material col-md-4">
                                                    <input type="text" class="form-control numbervalid" name="totaexp" id="" placeholder="Total Experirnce" data-msg-required="The Experience field is required." 
                                                    data-rule-required="true"  required>
                                                </div>
                                            </div>
                                            <div class="form-group form-material">
                                                <?php $i=1;?>
                                                @forelse($role as $roles)
                                                @if('student'!= $roles->name)
                                                    <div class="checkbox-custom checkbox-primary">
                                                        <input type="checkbox" class="cehck rolescheck rolescheck{{$roles->id}}" id="inputChecked{{$roles->name}}" name="roles[]" value="{{$roles->id}}"/>
                                                        <label for="inputChecked{{$roles->name}}">{{$roles->display_name}}</label>
                                                    </div>
                                                @endif   
                                                    <?php if( 'classincharge' === $roles->name ) { ?>
                                                    <div id="classcheck">
                                                        <div class="form-group form-material" id="class-incharge-classes">
                                                            <?php $i=1;?>
                                                            @foreach($class as $classes)
                                                                <div class="checkbox-custom checkbox-primary">
                                                                    <input type="checkbox" id="inputChecked{{$classes->class_name}}" name="classes[]" class="classnew"  value="{{$classes->id}}"/>
                                                                    <label for="inputChecked{{$classes->class_name}}">{{$classes->class_display_name}}</label>
                                                                </div>
                                                                <?php if( 1 == $classes->has_stream ) { ?>
                                                                <div id="streamcheck" class="streamcheck{{$classes->id}} streamcnew">
                                                                    <div class="form-group form-material" id="class-incharge-stream">
                                                                    <?php $i=1;?>
                                                                    @foreach($highclass as $highclasses)
                                                                    <div class="checkbox-custom checkbox-primary">
                                                                        <input type="checkbox" id="inputChecked{{$highclasses->highsubname}}" name="stream[]"  value="{{$highclasses->id}}"/>
                                                                        <label for="inputChecked{{$highclasses->highsubname}}">{{$highclasses->highsubname}}</label>
                                                                    </div>
                                                                    @endforeach
                                                                    </div>
                                                                </div>
                                                                <?php } ?>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    
                                                    @empty
                                                @endforelse
                                            </div>
                                            
                                            <div class="modal-footer">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection