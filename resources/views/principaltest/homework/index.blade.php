@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
    		    <div class="alert alert-success">
    		        {{ session()->get('message') }}
    		    </div>
    		@endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Add HomeWork</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($homework as $homeworks)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$homeworks->title}}</td>
                                <td>{{$homeworks->description}}</td>
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="homework" data-toggle="modal" data-target="#myhomeworkdetail-{{$homeworks->hid}}" data-id="{{$homeworks->hid}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button title="homework" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete"  data-id="{{$homeworks->hid}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                    <div class="modal fade" id="myhomeworkdetail-{{$homeworks->hid}}" role="dialog" data-homework_id="{{$homeworks->hid}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$homeworks->title}}</h4>
                                                </div>
                                                <div class="modal-body"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Add Homework</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('homework/store')}}" class="horizontal-form" method="post">
                                                    {{ csrf_field() }}
                                                        <div class="form-group form-material">
                                                            <div class="col-md-12">
                                                                <input type="text" name="title" id="obtain_marks" class="form-control"  
                                                                data-msg-required="The Title field is required." 
                                                                data-rule-required="true" placeholder="Title">
                                                                @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-12">
                                                                <select name="classsid" id="class" class="form-control">
                                                                    <option>--Select Classes--</option>
                                                                    @foreach($classes as $class)
                                                                        <option value="{{$class->id}}">{{$class->class_display_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-12">
                                                                <select name="sectionid" id="sectionid" class="form-control">
                                                                    <option>--Select Section--</option>
                                                                    @foreach($section as $sections)
                                                                        <option value="{{$sections->id}}">{{$sections->section_display_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('sectionname'))<p class="help-block">{{ $errors->first('sectionname')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-12">
                                                                <select name="subjectid" id="subjectid" class="form-control">
                                                                    <option>--Select Subject--</option>
                                                                    @foreach($subject as $subjects)
                                                                        <option value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('subjectname'))<p class="help-block">{{ $errors->first('subjectname')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-12">
                                                                <textarea name="description" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                                data-rule-required="true" data-iconlibrary="fa" placeholder="Description" required></textarea>
                                                                @if ($errors->has('description'))<p class="help-block">{{ $errors->first('description')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save">Save Changes</button>
                                                        </div>
                                                    </form>
                                                <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>

@endsection