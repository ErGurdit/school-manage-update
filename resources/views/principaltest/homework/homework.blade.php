<link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.min.css?v4.0.1')}}">
<script src="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/marked/marked.min.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/to-markdown/to-markdown.js?v4.0.1')}}"></script>
<div class="example">
    <form action="{{url('homework/update',$homeworks->hid)}}" method="post" role="form" id="homework-edit-{{$homeworks->hid}}" class="homework-update-{{$homeworks->hid}}" enctype="multipart/form-data">
    {{method_field('PATCH')}}
    <div class="form-group form-material">
        <div class="col-md-12">
            <input type="hidden" name="homeworkid" id="homeworkid" value="{{$homeworks->hid}}">  
            <input type="text" name="title" id="title" class="form-control"  
            data-msg-required="The Title field is required." 
            data-rule-required="true" placeholder="Title" value="{{$homeworks->title}}">
            @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <select name="classsid" id="class" class="form-control">
                <option>--Select Classes--</option>
                @foreach($classes as $class)
                    <?php 
                      $connew='';
                      if ($class->id == $homeworks->classid) {
                          $connew = ' selected="selected"';
                          } 
                ?>
                    <option <?php echo $connew;?> value="{{$class->id}}">{{$class->class_display_name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('classsid'))<p class="help-block">{{ $errors->first('classsid')}}</p>@endif </span>
        </div>
    </div>
    <div class="form-group form-material">
    <div class="col-md-12">
            <select name="sectionid" id="sectionid" class="form-control">
                <option>--Select Section--</option>
                @foreach($section as $sections)
                <?php 
                      $connew='';
                      if ($sections->id == $homeworks->sectionid) {
                          $connew = ' selected="selected"';
                          } 
                ?>
                    <option <?php echo $connew;?> value="{{$sections->id}}">{{$sections->section_display_name}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('sectionname'))<p class="help-block">{{ $errors->first('sectionname')}}</p>@endif </span>
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <select name="subjectid" id="subjectid" class="form-control">
                <option>--Select Subject--</option>
                @foreach($subject as $subjects)
                <?php 
                      $connew='';
                      if ($subjects->id == $homeworks->subjectid) {
                          $connew = ' selected="selected"';
                          } 
                ?>
                    <option <?php echo $connew;?> value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                @endforeach
            </select>
            <span class="help-block"> @if ($errors->has('subjectname'))<p class="help-block">{{ $errors->first('subjectname')}}</p>@endif </span>
        </div>
    </div>
    <div class="form-group form-material">
        <div class="col-md-12">
            <textarea name="description" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
            data-rule-required="true" data-iconlibrary="fa" placeholder="Description" required>{{$homeworks->description}}</textarea>
            @if ($errors->has('description'))<p class="help-block">{{ $errors->first('description')}}</p>@endif
        </div>
    </div>
    </form>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="$('#homework-edit-{{$homeworks->hid}}').submit()">Update changes</button>
    </div>
</div>