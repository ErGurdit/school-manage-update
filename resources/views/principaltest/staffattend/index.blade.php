
@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<style>
  #__lpform_fname{
    display:none !important;
}
    
    
}
</style>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif

                        <!-- Panel Table Add Row -->
      <div class="panel">
        <header class="panel-heading">
          <!--h3 class="panel-title">Add Row</h3-->
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <!--div class="mb-15">
                <button id="addToTable" class="btn btn-primary" type="button">
                  <i class="icon md-plus" aria-hidden="true"></i> Add row
                </button>
              </div-->
            </div>
          </div>
            <div class="example-wrap">
                <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne"
                        aria-controls="exampleTabsOne" role="tab">Staff</a></li>
                    <!--li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo"
                        aria-controls="exampleTabsTwo" role="tab">Increment</a></li-->
                    
                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                      <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
            <thead>
              <tr>
                <th>Date</th>
                <th>Staff Member</th>
                <th>Designation</th>
                <th>Present</th>
                <th>Absent</th>
                <th>Leaves</th>
                <th>Holidays</th>

              </tr>
            </thead>
            <tbody>
                                                                                                    {{ csrf_field() }}
                                              <form action="{{ action('StaffAttendanceController@store') }}" class="horizontal-form" method="post" id="k">

                                                     @foreach($staff as $staf)

                 <tr>
                 <td>{{$current_date}}</td>
                 <td>{{$staf->name}}</td>   
                 <td>{{$staf->dn}}</td> 
                 <td>
                                          <input type="hidden" name="nme" value="{{$staf->name}}">
                                                               <input type="hidden" name="cur_date" value="{{$current_date}}">


                     <input type="hidden" name="schoolid" value="{{$staf->schoolid}}">
                     
                                   <li class="list-inline-item">
                    <div class="radio-custom radio-success">
          <input type="radio" id="" show="present" checked="true" name="attendance[{{$staf->uid}}]" value="present" data-title="Discount Charge apply." id="discount" onclick="">
                      <label></label>
                    </div>
                  </li>
                 </td>   
                 <td>
                                   <li class="list-inline-item">
                    <div class="radio-custom radio-danger">
          <input type="radio" id="" show="absent" class="" name="attendance[{{$staf->uid}}]" value="absent" data-title="Discount Charge apply." id="discount" onclick="">
                      <label></label>
                    </div>
                  </li></td>   
                 <td><li class="list-inline-item">
                    <div class="radio-custom radio-info">
          <input type="radio" id="" show="leave" class="" name="attendance[{{$staf->uid}}]" value="leave" data-title="Discount Charge apply." id="discount" onclick="">
                      <label></label>
                    </div>
                  </li></td> 
                                   <td><li class="list-inline-item">
                    <div class="radio-custom radio-warning">
          <input type="radio" id="" show="holidays" class="" name="attendance[{{$staf->uid}}]" value="holiday" data-title="Discount Charge apply." id="discount" onclick="">
                      <label></label>
                    </div>
                  </li>
                                   </td>

                 </tr>
                         
                                @endforeach
            <tr><td><button type="submit" class="btn btn-primary" value="Save">Submit</button></td></tr>

</form>

            </tbody>
          </table>
                    </div>
                  
                  </div>
                </div>
              </div>
          
        </div>
      </div>
      <!-- End Panel Table Add Row -->

</div><!---End page-->
</div>