@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <button class="btn btn-primary" data-target="#myClass" data-toggle="modal"
            type="button">Add Exam</button>
                <div class="panel-body container-fluid">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Exam Name</th>
                            <th>Student name</th>
                            <th>Username</th>
                            <th>class</th>
                            <th>Total Marks</th>
                            <th>Obtain Marks</th>
                            <th>Percentage</th>
                            <th>Position</th>
                            <th>Result</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($exam as $exams)
                            <tr id="class">
                                <td>{{$i++}}</td>
                                <td>{{$exams->ename}}</td>
                                <td>{{$exams->uname}}</td>
                                <td>{{$exams->username}}</td>
                                <td>{{$exams->class_name}}</td>
                                <td>{{$exams->total_marks}}</td>
                                <td>{{$exams->obtain_marks}}</td>
                                <td>{{$exams->percentage}}%</td>
                                <td>{{$exams->position}}</td>
                                <td>{{$exams->result}}</td>
                                <td>
                                    <button type="button" class="btn btn-floating btn-info btn-sm waves-effect waves-classic waves-effect waves-classic editdel" title="exam" data-toggle="modal" data-target="#myexamdetail-{{$exams->eid}}" data-id="{{$exams->eid}}">
                                        <i class="icon md-edit"></i>
                                    </button>
                                    <button title="exam" class="btn btn-floating btn-danger btn-sm waves-effect waves-classic waves-effect waves-classic btn-delete"  data-id="{{$exams->eid}}">
                                        <i class="icon md-delete"></i>
                                    </button>
                                <div class="modal fade" id="myexamdetail-{{$exams->eid}}" role="dialog" data-exam_id="{{$exams->eid}}">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$exams->ename}}</h4>
                                                </div>
                                                <div class="modal-body"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row row-lg">
                        <div class="col-xl-12">
                            <div class="example-wrap">
                                <div class="example">
                                <!-- Modal -->
                                    <div class="modal fade" id="myClass" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Add Exam</h4>
                                                </div>
                                                <div class="modal-body">
                                                <!-- BEGIN FORM-->
                                                <form action="{{url('exam/store')}}" class="horizontal-form" method="post">
                                                    {{ csrf_field() }}
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="schoolid" value="{{$school->id}}">
                                                                <select name="examtype" id="examtype" class="form-control">
                                                                    <option>--Select--</option>
                                                                    @foreach($examtype as $examtypes)
                                                                        <option value="{{$examtypes->id}}">{{$examtypes->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('student_name'))<p class="help-block">{{ $errors->first('student_name')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <select name="student_name" id="student_name" class="form-control">
                                                                    <option>--Select--</option>
                                                                    @foreach($allstudent as $allstudents)
                                                                        <option value="{{$allstudents->id}}">{{$allstudents->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('student_name'))<p class="help-block">{{ $errors->first('student_name')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <select name="classselect" id="class_select" class="form-control">
                                                                    <option>--Select--</option>
                                                                    @foreach($class as $classes)
                                                                        <option value="{{$classes->id}}">{{$classes->class_display_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block"> @if ($errors->has('class_select'))<p class="help-block">{{ $errors->first('class_select')}}</p>@endif </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="tomarks" id="cname" class="form-control"  
                                                                data-msg-required="The total Marks field is required." 
                                                                data-rule-required="true" placeholder="Total marks">
                                                                @if ($errors->has('tomarks'))<p class="help-block">{{ $errors->first('tomarks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="obtain_marks" id="obtain_marks" class="form-control"  
                                                                data-msg-required="The Obtain Marks field is required." 
                                                                data-rule-required="true" placeholder="Obtain Marks">
                                                                @if ($errors->has('obtain_marks'))<p class="help-block">{{ $errors->first('obtain_marks')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="percentage" id="percentage" class="form-control"  
                                                                data-msg-required="The Percentage field is required." 
                                                                data-rule-required="true" placeholder="Percentage">
                                                                @if ($errors->has('percentage'))<p class="help-block">{{ $errors->first('percentage')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="position" id="position" class="form-control"  
                                                                data-msg-required="The Position field is required." 
                                                                data-rule-required="true" placeholder="Position">
                                                                @if ($errors->has('position'))<p class="help-block">{{ $errors->first('position')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-material">
                                                            <div class="col-md-6">
                                                                <input type="text" name="result" id="result" class="form-control"  
                                                                data-msg-required="The result field is required." 
                                                                data-rule-required="true" placeholder="Result">
                                                                @if ($errors->has('result'))<p class="help-block">{{ $errors->first('result')}}</p>@endif
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary" value="Save">Save Changes</button>
                                                        </div>
                                                    </form>
                                                <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>
    </div>

@endsection