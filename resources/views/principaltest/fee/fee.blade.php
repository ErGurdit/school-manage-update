@extends('layouts.principaltest.app')

@section('content')
 <?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;

?>
<style>
    #__lpform_fname{
    display:none !important;
}
</style>
<div class="page">
    <div class="page-header">
         <div class="page-header">
        <h1 class="page-title">{{$firstparam}}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
        </ol>
    </div>
    </div>
    <div class="page-content container-fluid">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
       @if(\Session::has("success"))
            <div id="msg" class="alert alert-success">
                {{ \Session::get("success") }}
            </div>
             
        @endif
        @if(\Session::has("danger"))
            <div id="msg" class="alert alert-danger">
                {{ \Session::get("danger") }}
            </div>
             
        @endif
            <div class="panel">
                <div class="panel-body container-fluid">
                    <form method="post" action="{{url('feestore')}}" id="signup-form" class="signup-form" name="form_nme" enctype="multipart/form-data">
                        <h5 style="display:none">
                                Student
                        </h5>
                        <fieldset>
                                <div class="form-row">
                                    <div class="form-group-flex row">
                                        <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="form-control-label" for="inputUserNameOne">First Name</label>
                                            <input type="hidden" name="admissionroom" value="{{$rollno}}">
                                                <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('fname')}}" required="required">
                                                <span class="help-block studentfname"></span>
                                                @if ($errors->has('fname'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('fname') }}</strong>
                                                    </span>
                                                @else
                                                    <span class="help-block"></span>
                                                @endif
                                        </div>                                        
                                        <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="form-control-label" for="inputUserNameOne">Middle Name</label>
                                            <input type="text" class="form-control" id="inputmname"  name="mname" value="{{ old('mname')}}">
                                            <span class="help-block studentmname"></span>
                                            @if ($errors->has('mname'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('mname') }}</strong>
                                                </span>
                                            @else
                                                <span class="help-block"></span>
                                            @endif
                                        </div>
                                        <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="form-control-label" for="inputUserNameOne">Last Name</label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" required="required" onkeypress="return onlyAlphabetslname(event,this);">
                                            <span class="help-block studentlname"></span>
                                            @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @else
                                                <span class="help-block"></span>
                                            @endif                            
                                        </div>
                                        <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <label class="form-control-label" for="inputUserNameOne">Username</label>
                                            <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" required="required">
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @else
                                                <span class="help-block"></span>
                                            @endif                            
                                        </div>
                                        <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <label class="form-control-label" for="inputUserNameOne">Gender</label>
                                            <div class="container row">
                                                <div class="radio-custom radio-primary col-lg-3">
                                                    <input type="radio" id="inputRadiosUncheckedMale" id="gender" name="gender" value="M" checked/>
                                                    <label for="inputRadiosUncheckedMale">Male</label>
                                                </div>
                                                <div class="radio-custom radio-primary col-lg-3">
                                                    <input type="radio" id="inputRadiosUncheckedFemale" id="gender" name="gender" value="F"  required="required"/>
                                                    <label for="inputRadiosUncheckedFemale">Female</label>
                                                </div>
                                            </div>
                                            @if ($errors->has('gender'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                            @else
                                                <span class="help-block"></span>
                                            @endif                   
                                        </div>
                                        <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <label class="form-control-label" for="inputUserNameOne">Class</label>
                                            <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                            <select data-plugin="selectpicker" name="class" id="class" required="required" class="form-control admissionclass studentclass" data-live-search="true">
                                                <option value="">--Select Class--</option>
                                                    @foreach($myclass as $classes)
                                                        <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                        <?php $classid = $classes->id;?>
                                                    @endforeach
                                            </select>
                                            @if ($errors->has('class'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('class') }}</strong>
                                                </span>
                                            @else
                                            <span class="help-block"></span>
                                            @endif
                                        </div>
                                        <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group form-material" id="section" style="display: none">
                                                <label class="form-control-label" for="inputUserNameOne">Section</label>
                                                    <select  name="section" class="form-control classwidesection" id="newsection">
                                                    </select>
                                                    @if ($errors->has('section'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('section') }}</strong>
                                                        </span>
                                                    @else
                                                        <span class="help-block"></span>
                                                    @endif
                                            </div>
                                            <div class="form-group form-material" id="stream_" style="display:none">
                                               <label class="control-label" for="inputUserNameOne">Stream</label>
                                        <select data-plugin="select2" class="form-control" name="stream[]" id="stream" data-live-search="true">
                                        </select>
                                        @if ($errors->has('stream'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('stream') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                            </div>
                                            </div>
                                        <div class="row form-group form-material col-lg-12 col-md-6 col-sm-6 col-xs-12">
                                      <div class="col-lg-6">
                                           <div class="form-group form-material">
                                                  <input type="hidden" name="sd[]" value="">
                                                <label class="form-control-label" for="inputUserNameOne">Subject</label>
                                                <select data-plugin="select2" multiple name="subjectdata[]" id="subjectdata" class="form-control subjectdata">
                                                   </select>
                                                    @if ($errors->has('subjectdata'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('subjectdata') }}</strong>
                                                        </span>
                                                    @else
                                                    <span class="help-block"></span>
                                                    @endif
                                            </div> 
                                      </div>
                                        <div class="col-lg-6"><div class="form-group form-material">
                                                <label class="form-control-label" for="inputUserNameOne">Optional Subject</label>
                                                <select data-plugin="select2" multiple name="leftsubject[]" id="leftsubject" class="form-control leftsubject">
                                                   
                                                </select>
                                                    @if ($errors->has('leftsubject'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('leftsubject') }}</strong>
                                                        </span>
                                                    @else
                                                    <span class="help-block"></span>
                                                    @endif
                                            </div>
                                            </div>

                                            
                                           </div>
                                                 <div class="form-group form-material col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="control-label">Charges</label>
                                                <div class="col-md-12 md-checkbox-inline">
                                                    <div class="row">
                                                        <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                          <input type="checkbox" id="inputChecked_dress" class="dressc checkboxamount_d" show="amount1" name="dress_charge" value="1" data-title="Dress Charge apply."/>
                                                          <label for="inputChecked_dress">Dress Charge
                                                          <div class="dvPassport amount1">
                                                                    <div class="input-group admscharges ">
                                                                        <span class="input-group-addon">
                                                                            &#8377;
                                                                        </span>
                                                                        <input type="text" class="form-control txtdresscharge" id="txtdresscharge" name="dresscharge" value="" readonly/>
                                                                    </div>
                                                                </div>
                                                          </label>
                                                        </div>
                                                        <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                          <input type="checkbox" id="inputChecked_trans" class="transc checkboxamount_d" show="amount2" name="©_charge" onclick="" value="1" data-title="Transport Charge apply."/>
                                                          <label for="inputChecked_trans">Transport Charge
                                                          <div class="transcharge amount2">
                                                                    <div class="input-group admscharges ">
                                                                        <!--<span class="input-group-addon">-->
                                                                        <!--    &#8377;-->
                                                                        <!--</span>-->
                                                                        <select data-plugin="selectpicker" name="inputRadioscharge" id="inputRadioscharge" class="form-control tranportchargegetdata" data-live-search="true">
                                                                        <option value="">--Select Route--</option>
                                                                        @foreach($busroute as $busroutes)
                                                                            <option value="{{$busroutes->fare}}">{{$busroutes->routename}}(&#8377; {{$busroutes->fare}})</option>
                                                                        @endforeach
                                                                        </select>
                                                                        <!--<div class="radio-custom radio-primary">-->
                                                                        <!--  <input type="radio" id="{{$busroutes->id}}" data-id="{{$busroutes->id}}" class="inputRadiosUncheckedtrans-{{$busroutes->id}}" value="{{$busroutes->fare}}" name="inputRadioscharge" onclick=""/>-->
                                                                        <!--  <label for="inputRadiosUnchecked">{{$busroutes->routename}}({{$busroutes->fare}})</label>-->
                                                                        <!--</div>-->
                                                                        <!--<input type="text" class="form-control" id="txttransportcharge" name="transportcharge" value="" readonly />-->
                                                                    </div>
                                                                </div>
                                                          </label>
                                                        </div>
                                                        <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                          <input type="checkbox" id="inputChecked_other" class="oth checkboxamount_d" show="amount3" name="other_charge" value="1" data-title="Other Charge apply." onclick=""/>
                                                          <label for="inputChecked_other">Other Charge</label>
                                                          <div class="ocharge">
                                                                    <div class="input-group admscharges ">
                                                                        <span class="input-group-addon">
                                                                            &#8377;
                                                                        </span>
                                                                        <input type="number" min="1" class="form-control" id="txtothercharge" name="othercharge" value="1" />
                                                                    </div>
                                                                    <div class="input-group admscharges">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control" id="txtothercharge" name="remark" placeholder="Remark"/>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="checkbox-custom checkbox-primary charges col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                            <input type="checkbox" id="inputChecked_discount" show="amount4" class="discountamount checkboxamount_d" name="discount" value="1" data-title="Discount Charge apply." id="discount" onclick="">
                                                            <label for="inputChecked_discount">Discount Charge</label>
                                                            <div class="discountamounts">
                                                                <div class="input-group admscharges">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="number" min="1" class="form-control txtdiscountcharge" id="txtdiscountcharge" name="discountcharge" value="1"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row charges-position" id="book" style="display: none">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                            <label class="control-label">Book Charge
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <input type="text" class="form-control" id="book_charge" name="book_charge" value="" readonly/>
                                                            <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                            @if ($errors->has('book_charge'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('book_charge') }}</strong>
                                                                </span>
                                                            @else
                                                            <span class="help-block">  </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="addsn" style="display: none">
                                                            <label class="control-label">Admission Fee</label>
                                                            <input type="text" class="form-control add" id="adfee" name="admsnfee" value="" readonly/>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="schfee" style="display: none">
                                                            <label class="control-label">Monthly Charges</label>
                                                            <input type="text" class="form-control add monthlycharge" id="sfee" name="schfee" value="" readonly/>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="dontaion" style="display: none">
                                                                <label class="control-label">Donation</label>
                                                                <input type="number" min="0" step="10" class="form-control" id="donate" name="dontaion" value="0"/>
                                                            </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="subtotal" style="display: none">
                                                                                <label class="control-label">Subtotal Amount
                                                                                <span class="required"> * </span>
                                                                            </label>
                                                                                <input type="text" class="form-control" name="subtotal" id="subtotalamount" value="" readonly/>
                                                                                <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                                            
                                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                    </fieldset>
                        <h5 style="display:none">
                            Documentation
                        </h5>
                        <fieldset>
                            <div class="row">
                                <div class="form-group form-material col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                   <label class="form-control-label">Last School
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="text" class="form-control" name="last_school" required/>
                                    @if ($errors->has('last_school'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_school') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                  <!--label class="form-control-label" for="inputCardNumber">Card Number</label>
                                  <input type="text" class="form-control" id="inputCardNumber" name="number" placeholder="Card number"-->
                            </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                   <label class="form-control-label">Upload School Leaving Certificate
                                                        <span class="required"> * </span>
                                                    </label>
                                    <input type="file" id="file-input" name="leavecertificate" required/>
                                    @if ($errors->has('leavecertificate'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('leavecertificate') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif  
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                   <label class="form-control-label">Upload Last Class Certificate
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="file" id="file-input" name="lastclass" required/>
                                    @if ($errors->has('lastclass'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('lastclass') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif 
                            </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Aadhar Card
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <input type="file" id="file-input" name="aadharfront" placeholder="Front" required/>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <input type="file" id="file-input" name="aadharback" placeholder="Back" required/>
                                        </div>
                                            
                                            @if ($errors->has('aadharfirst') || $errors->has('aadharsecond'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('aadharfirst') }}</strong>
                                                </span>
                                            @else
                                    <span class="help-block"></span>
                                    @endif
                                    </div> 
                            </div>
                            </div>
                    </fieldset>
                        <h5 style="display:none">
                            Getting
                        </h5>
                        <fieldset>
                            <div class="row">
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Father Name
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="text" class="form-control" id="fathername" name="father_name" required/>
                                    @if ($errors->has('father_name'))
                                        <span class="help-block fathername">
                                            <strong>{{ $errors->first('father_name') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Father Contact Number
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="text" class="form-control" id="father_contact" minlength="10" maxlength="10" name="father_contact" onkeypress="return onlyDigitsMother(event,this);" required="required" />
                                    @if ($errors->has('father_contact'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('father_contact') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="form-control-label">Mother Name
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="text" class="form-control" id="mother_name"  name="mother_name" required/>
                                     @if ($errors->has('mother_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mother_name') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Mother Contact Number
                                        <span class="required"> * </span>
                                        </label>
                                        <input type="text" class="form-control" id="mother_contact" minlength="10"  maxlength="10" name="mother_contact" onkeypress="return onlyDigitsMother(event,this);" required="required" />
                                        @if ($errors->has('mother_contact'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('mother_contact') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Home Tel
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="text" class="form-control" id="home_tel" name="home_tel" minlength="10"  maxlength="20" onkeypress="return onlyDigitsMother(event,this);" required="required"/>
                                    @if ($errors->has('home_tel'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('home_tel') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Email
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="email" class="form-control" id="useremail" name="email" required="required" />
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="form-control-label">Date of Birth
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="datepair-wrap" data-plugin="datepair">
                                        <div class="input-daterange-wrap">
                                            <div class="input-daterange">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                                    </span>
                                                    <input type="text" class="form-control datepair-date datepair-start cal-set" data-plugin="datepicker" name="dob" id="dob" required="required">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('dob'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('dob') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                          </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Country</label>
                                        <select name="country" id="country_list" class="form-control select2" required="required">
                                                <option value="0">--Select Country--</option>
                                                @foreach($country as $countries)
                                                <option value="{{$countries->id}}" data-countrname="{{$countries->name}}">{{$countries->name}}</option>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">State</label>
                                    <select name="state" id="state_list" class="form-control state_list select2">
                                            <!--@foreach($state as $states)-->
                                            <!--<option value="{{$states->id}}">{{$states->name}}</option>-->
                                            <!--@endforeach-->
                                    </select>
                                    @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">City</label>
                                    <select name="city" id="city_list" class="form-control city_list select2">
                                            <!--@foreach($city as $cities)-->
                                            <!--<option value="{{$cities->id}}">{{$cities->name}}</option>-->
                                            <!--@endforeach-->
                                    </select>
                                    @if ($errors->has('city'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">Pin
                                        <span class="required"> * </span>
                                    </label>
                                    <input type="text" class="form-control" id="pin" name="pin" required="required" />
                                    @if ($errors->has('pin'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('pin') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div>
                                <div class="form-group form-material col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label">Address</label>
                                    <textarea class="form-control" rows="3" id="address" name="address" required="required"></textarea>
                                    @if ($errors->has('address'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @else
                                    <span class="help-block"></span>
                                    @endif
                                </div> 
                            </div>
                    </fieldset>
                        <h5 style="display:none">Payment</h5>
                        <fieldset>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="nav-tabs-horizontal" data-plugin="tabs">
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach($payment as $payments)
                                            @if($payments->status == '1')
                                                <li class="nav-item" role="presentation"><a class="nav-link <?php if($payments->id == 2){echo 'active';}?>" data-toggle="tab" href="#exampleTabs_{{$payments->id}}"
                                                aria-controls="exampleTabs_{{$payments->id}}" role="tab">{{$payments->payment_display_name}}</a></li>
                                                <input type="hidden" name="paytype" id="paytype" value="{{$payments->id}}">
                                                <input type="hidden" name="payname" value="{{$payments->payment_name}}">
                                            @endif
                                        @endforeach
                                    </ul>
                                    <div class="tab-content pt-20">
                                        <div class="tab-pane" id="exampleTabs_1" role="tabpanel">
                                            <div class="row">
                                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <label class="control-label">Final Subtotal Amount
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control" name="finalsubtotal" id="finalsubtotalamountcard" value=""/>
                                                    <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                </div>
                                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <label class="control-label">Payable Amount
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control" name="finalsubtotalamountpayable" id="finalsubtotalamountpayable" value=""/>
                                                    <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                </div>
                                                <!---<div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <label class="control-label">Card Type</label>
                                                    <select name="card_type" id="card_type" class="form-control select2">
                                                            <option>--Select Card Type--</option>
                                                            <option value="master">Mater</option>
                                                            <option value="visa">Visa</option>
                                                            <option value="rupay">Rupay</option>
                                                            <option value="dinner-club">Dinner Club</option>
                                                    </select>
                                                </div>-->
                                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <label class="control-label">Card Holder Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control" name="card_name" id="owner" />
                                                    <span class="help-block"> </span>
                                                </div>
                                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12" id="card-number-field">
                                                    <label class="control-label">Card Number
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control" name="card_number" id="cardNumber"/>
                                                    <span class="help-block"> </span>
                                                </div>
                                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <label class="control-label">Approval Code
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control" name="card_approval_code" id="cvv"/>
                                                    <span class="help-block"> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane active" id="exampleTabs_2" role="tabpanel">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="first" data-value="2000">2000</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" name="first" class="form-control" id="afirst" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="three" data-value="500">500</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" name="three" class="form-control" id="athree" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="four" data-value="200">200</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="four" id="afour" value=""  onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="five" data-value="100">100</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="five" id="afive" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="six" data-value="50">50</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="six" id="asix" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="seven" data-value="20">20</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="seven" id="aseven" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="eight" data-value="10">10</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="eight" id="aeight" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="nine" data-value="5">5</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="nine" id="anine" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="ten" data-value="2">2</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="ten" id="aten" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                    <span id="eleven" data-value="1">1</span> 
                                                                    <span>X</span>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" name="eleven" id="aeleven" value="" onkeyup="caltotal()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                   <label>Subtotal Amount</label>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" readonly="true" name="finaltotal" id="finaltotal" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                   <label>Final Amount</label>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" readonly="true" name="subfinaltotalnew" id="subfinaltotalnew" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 set-align">
                                                                <p>
                                                                   <label>Return Amount</label>
                                                                </p>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 returnamount">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" readonly="true" name="returnamount" id="returnamount" value="">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 hidden" id="messagefield">
                                                                <div class="input-group admschargesnew">
                                                                    <span class="input-group-addon">
                                                                        &#8377;
                                                                    </span>
                                                                    <input type="text" class="form-control" readonly="true" name="messagefield" id="" value="Price Is No Valid">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                   <!-- </div>-->
                    <!--button type="button" class="btn btn-success btn-outline float-right btn-paid " onclick="confirmationmodal()">Paid</button-->
                    <!--button type="submit" class="btn btn-success btn-outline float-right btn-paid " data-wizard="finish">Paid</button-->
                    <div id="studendetailconfirmationmodal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Student Complete Detail</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table>
                                                        <th style="text-align:left;">Student Detail</th>
                                                        <tr><td>Student Full Name:</td><td id="studentcompletename"></td></tr>
                                                        <tr><td>UserName:</td><td id="studentusername"></td></tr>
                                                        <tr><td>Gender:</td><td id="studentgender"></td></tr>
                                                        <tr><td>Class:</td><td id="studentclass"></td></tr>
                                                        <tr><td>Section:</td><td id="studentsection"></td></tr>
                                                        <tr><td>Subject:</td><td id="studentsubject"></td></tr>
                                                        <tr><td>Stream:</td><td id="studentclasswisesnewtream"></td></tr>
                                                        <th style="text-align:left;padding:15px 0 10px 0 !important;">Personal Detail</th>
                                                        <tr><td>Father Name:</td><td id="studentfather_name"></td></tr>
                                                        <tr><td>Mother Name:</td><td id="studentmother_name"></td></tr>
                                                        <tr><td>DOB:</td><td id="studentdob"></td></tr>
                                                        <th style="text-align:left;padding:15px 0 10px 0 !important;">Contact Detail</th>
                                                        <tr><td>Father Contact:</td><td id="studentfather_contact"></td></tr>
                                                        <tr><td>Mother Contact:</td><td id="studentmother_contact"></td></tr>
                                                        <tr><td>Home Tel:</td><td id="studenthome_tel"></td></tr>
                                                        <tr><td>Email:</td><td id="studentuseremail"></td></tr>
                                                        <th style="text-align:left;padding:15px 0 10px 0 !important;">Residence</th>
                                                        <tr><td>Country:</td><td id="studentcountryname"></td></tr>
                                                        <tr><td>State:</td><td id="studentstatename"></td></tr>
                                                        <tr><td>City:</td><td id="studentcityname"></td></tr>
                                                        <tr><td>Pin:</td><td id="studentpin"></td></tr>
                                                        <tr><td>Address:</td><td id="studentaddress"></td></tr>
                                                        <th style="text-align:left;padding:15px 0 10px 0 !important;">Charges</th>
                                                        <tr><td>Admission Fee:</td><td id="studentadmision">&#x20B9;</td></tr>
                                                        <tr><td>Monthly Charges:</td><td id="studentschoolfee">&#x20B9;</td></tr>
                                                        <tr><td>Book Charge:</td><td id="studentbook">&#x20B9;</td></tr>
                                                        <tr><td>Dress Charge:</td><td id="studentdresscharge">&#x20B9;</td></tr>
                                                        <tr><td>Other Charge:</td><td id="studentother">&#x20B9;</td></tr>
                                                        <tr><td>Transport Charge:</td><td id="studenttransport">&#x20B9;</td></tr>
                                                        <tr><td>Discount Charge:</td><td id="studenttxtdiscountcharge">&#x20B9;</td></tr>
                                                        <tr><td>Donation:</td><td id="studentdonation">&#x20B9;</td></tr>
                                                        <!--<tr><td>Card Type:</td><td id="studentdonation">&#x20B9;</td></tr>-->
                                                        <tr id="oldcardowner"><td>Card Owner Name:</td><td id="studentownername"></td></tr>
                                                        <tr id="oldcardnumber"><td>Card Number:</td><td id="studentcardnumber"></td></tr>
                                                        <!--<tr id="oldcardexpiration"><td>Card Expiration:</td><td><span id="studentcardexpirationmonth"></span>/<span id="studentcardexpirationyear"></span></td></tr>-->
                                                        <tr id="oldcardpayment"><td>Card Payment:</td><td id="studentfinalsubtotalamountpayable">&#x20B9;</td></tr>
                                                        <tr><td>Total Amount: </td><td id="studentfinalamountdata">&#x20B9;</td></tr>
                                                        <!--<tr><td>Return Amount:</td><td id="studentreturnamountdata">&#x20B9;</td></tr>-->
                                                        <tr><td>Payment Mode:</td><td id="studentpaymentmode"></td></tr>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="Submit" class="btn btn-default">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                <!--</div>-->
                </fieldset>
                    </form>
                </div>
            </div>
        </div><!--page-->
    </div>
@endsection