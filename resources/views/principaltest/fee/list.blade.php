@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <!--<button class="btn btn-primary" data-target="#myClass" data-toggle="modal"-->
            <!--type="button">Add Certificate</button>-->
                <div class="panel-body container-fluid admission">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Invoice No</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>DOB</th>
                            <th>Subject</th>
                            <th>Class</th>
                            <th>Father Name</th>
                            <th>Mother Name</th>
                            <th>Home Telephone</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Address</th>
                            <th>Pin</th>
                            <th>Payment Method</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($alladmission as $alladmissions)
                        <?php
                            $admissioninvoice = explode('|',$alladmissions->Invoice_fees);
                            $newfinal = explode('-',$admissioninvoice[1]);
                            $finalinvoice = $admissioninvoice[0].'-'.$newfinal[1];
                        ?>
                        <tr id="class">
                            <td>{{$i++}}</td>
                            <td>{{$finalinvoice}}</td>
                            <td>{{$alladmissions->name}}</td>
                            <td>{{$alladmissions->username}}</td>
                            <td>{{$alladmissions->email}}</td>
                            <td>{{$alladmissions->gender}}</td>
                            <td>{{$alladmissions->DOB}}</td>
                            <td>
                                @foreach($sub as $v)
                                    {{str_replace('-',' ',$v['subjectname']).','}}
                                @endforeach
                            </td>
                            <td>{{$alladmissions->classdisplay}}</td>
                            <td>{{$alladmissions->father_name}}/
                                {{$alladmissions->father_contact_number}}</td>
                            <td>{{$alladmissions->mother_name}}/
                                {{$alladmissions->mother_contact_number}}</td>
                            <td>{{$alladmissions->home_tel}}</td>
                            <td>{{$alladmissions->cname}}</td>
                            <td>{{$alladmissions->statename}}</td>
                            <td>{{$alladmissions->cityname}}</td>
                            <td>{{substr($alladmissions->address,0,20)}}</td>
                            <td>{{$alladmissions->pincode}}</td>
                            <td>{{$alladmissions->payment_display_name}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection