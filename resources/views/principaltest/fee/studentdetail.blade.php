<table border="1" style="display:none;">
    <th style="text-align:left;">Student Detail</th>
    <tr><td>Student Full Name:</td><td><?php $studentname = $StudentDetail['name']; ?></td></tr>
    <tr><td>Username:</td><td><?php $username = $StudentDetail['username']; ?></td></tr>
    <tr><td>Gender:</td><td><<?php if($StudentDetail['gender'] == 'M'){ $male = 'Male'; }else{ $male = 'Female'; }?></td></tr>
    <tr><td>Class:</td><td><?php $class = $StudentDetail['class']; ?></td></tr>
    <tr><td>Section:</td><td><?php $section = $StudentDetail['section']; ?></td></tr>
    <tr><td>Subjects:</td><td><?php $subject = $StudentDetail['subject']; ?></td></tr>
    <th style="text-align:left;padding:15px 0 0 0 !important;">Personal Detail</th>
    <tr><td>Father Name:</td><td><?php $father = $StudentDetail['father']; ?></td></tr>
    <tr><td>Mother Name:</td><td><?php $mother = $StudentDetail['mother']; ?></td></tr>
    <tr><td>DOB:</td><td><?php $dob = $StudentDetail['dob'];?></td></tr>
    <th style="text-align:left;padding:15px 0 0 0 !important;">Contact Detail</th>
    <tr><td>Father Contact No:</td><td><?php $fathercontact = $StudentDetail['father_contact']; ?></td></tr>
    <tr><td>Mother Contact No:</td><td><?php $mothercontact = $StudentDetail['mother_contact']; ?></td></tr>
    <tr><td>Home Tel:</td><td><?php $hometel = $StudentDetail['home_tel']; ?></td></tr>
    <tr><td>Email:</td><td><?php $email = $StudentDetail['email']; ?></td></tr>
    <th style="text-align:left;padding:15px 0 0 0 !important;">Residence</th>
    <tr><td>Country:</td><td><?php $country =  $StudentDetail['country']; ?></td></tr>
    <tr><td>State:</td><td><?php $state =  $StudentDetail['state']; ?></td></tr>
    <tr><td>City:</td><td><?php $city = $StudentDetail['city']; ?></td></tr>
    <tr><td>Pin:</td><td><?php $pincode = $StudentDetail['pincode']; ?></td></tr>
    <tr><td>Address:</td><td><?php $address = $StudentDetail['address']; ?></td></tr>
    <th style="text-align:left; padding:15px 0 0 0 !important;">Charges</th>
    <tr><td>Admission Fee:</td><td>&#x20B9; <?php $admsnfee = $StudentDetail['admission_fee']; ?></td></tr>
    <tr><td>Monthly Charges:</td><td>&#x20B9; <?php $monthlychrages = $StudentDetail['monthly_charges']; ?></td></tr>
    <tr><td>Books Charges:</td><td>&#x20B9; <?php $bookcharge = $StudentDetail['book_charge']; ?></td></tr>
    <tr><td>Dress Charges:</td><td>&#x20B9; <?php $dresscharge = $StudentDetail['dress_charge']; ?></td></tr>
    <tr><td>Transport Charges:</td><td>&#x20B9; <?php $transport = $StudentDetail['transport']; ?></td></tr>
    <?php $remark = $StudentDetail['remark']; ?>
    <tr><td>Other Charges:</td><td>&#x20B9; <?php $othercharge = $StudentDetail['other_charge'].'('.$remark.')';?></td></tr>
    <tr><td>Discount Charges:</td><td>&#x20B9; <?php $discount = $StudentDetail['discount']; ?></td></tr>
    <tr><td>Donation:</td><td>&#x20B9; <?php $donation = $StudentDetail['donation']; ?></td></tr>
    <tr><td>Total:</td><td>&#x20B9; <?php $finaltotal = $StudentDetail['finaltotal']; ?></td></tr>
    <tr><td>Payment Mode:</td><td><?php $paymentmode = $StudentDetail['payment_mode']; ?></td></tr>
</table>
<script LANGUAGE="JavaScript"> 
if (window.print) {
document.write('<form><table border="0">'+
    '<th style="text-align:left;">Student Detail</th>'+
    '<tr><td>Student Full Name:</td><td>{{$studentname}}</td></tr>'+
    '<tr><td>Username:</td><td>{{$username}}</td></tr>'+
    '<tr><td>Gender:</td><td>{{$male}}</td></tr>'+
    '<tr><td>Class:</td><td>{{$class}}</td></tr>'+
    '<tr><td>Section:</td><td>{{$section}}</td></tr>'+
    '<tr><td>Subjects:</td><td>{{$subject}}</td></tr>'+
    '<th style="text-align:left;padding:15px 0 0 0 !important;">Personal Detail</th>'+
    '<tr><td>Father Name:</td><td>{{$father}}</td></tr>'+
    '<tr><td>Mother Name:</td><td>{{$mother}}</td></tr>'+
    '<tr><td>DOB:</td><td>{{$dob}}</td></tr>'+
    '<th style="text-align:left;padding:15px 0 0 0 !important;">Contact Detail</th>'+
    '<tr><td>Father Contact No:</td><td>{{$fathercontact}}</td></tr>'+
    '<tr><td>Mother Contact No:</td><td>{{$mothercontact}}</td></tr>'+
    '<tr><td>Home Tel:</td><td>{{$hometel}}</td></tr>'+
    '<tr><td>Email:</td><td>{{$email}}</td></tr>'+
    '<th style="text-align:left;padding:15px 0 0 0 !important;">Residence</th>'+
    '<tr><td>Country:</td><td>{{$country}}</td></tr>'+
    '<tr><td>State:</td><td>{{$state}}</td></tr>'+
    '<tr><td>City:</td><td>{{$city}}</td></tr>'+
    '<tr><td>Pin:</td><td>{{$pincode}}</td></tr>'+
    '<tr><td>Address:</td><td>{{$address}}</td></tr>'+
    '<th style="text-align:left;padding:15px 0 0 0 !important;">Charges</th>'+
    '<tr><td>Admission Fee:</td><td>&#x20B9; {{$admsnfee}}</td></tr>'+
    '<tr><td>Monthly Charges:</td><td>&#x20B9; {{$monthlychrages}}</td></tr>'+
    '<tr><td>Books Charges:</td><td>&#x20B9; {{$bookcharge}}</td></tr>'+
    '<tr><td>Dress Charges:</td><td>&#x20B9; {{$dresscharge}}</td></tr>'+
    '<tr><td>Transport Charges:</td><td>&#x20B9; {{$transport}}</td></tr>'+
    '<tr><td>Other Charges:</td><td>&#x20B9; {{$othercharge}}</td></tr>'+
    '<tr><td>Discount:</td><td>&#x20B9; {{$discount}}</td></tr>'+
    '<tr><td>Donation:</td><td>&#x20B9; {{$donation}}</td></tr>'+
    '<tr><td>Total:</td><td>&#x20B9; {{$finaltotal}}</td></tr>'+
    '<tr><td>Payment Mode:</td><td>{{$paymentmode}}</td></tr>'+
    '<tr style="float:right;padding:15px 0 0 0 !important;"><td><input type=button name=print value="Print" onClick="window.print()"></td></tr>'+
    '</table></form>');
}
</script>