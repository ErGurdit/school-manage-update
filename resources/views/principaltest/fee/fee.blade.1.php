@extends('layouts.principal.newapp')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;"> Option 1</a>
                                                    </li>
                                                    <li class="divider"> </li>
                                                    <li>
                                                        <a href="javascript:;">Option 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 4</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable tabbable-tabdrop">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab1" data-toggle="tab">All Announcement</a>
                                                </li>
                                                <li>
                                                    <a href="#tab2" data-toggle="tab">Add Announcement</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab1">
                                                <div class="col-md-4">
                                                    <form action="<?php $_SERVER['PHP_SELF'];?>" method="get">
                                                        <select id="newclass" name="class" class="form-control select2" onchange="this.form.submit()">
                                                            <option value =''>--Select Class--</option>
                                                            @foreach($allclass as $myclasses)
                                                            
                                                                <?php 
                                                                if(isset($_GET['class'])){
                                                                $con = '';
                                                                    
                                                                if ($myclasses->id == $_GET['class']) {
                                                                    
                                                                    	$con = ' selected="selected"';
                                                                    
                                                                    }
                                                                } 
                                                                ?>
                                                                <option <?php echo $con ?> value="{{$myclasses->id}}">{{$myclasses->class_display_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </form>
                                                </div>
                                                <div class="col-md-4" id="section">
                                                     
                                                    <select class="form-control select2">
                                                        <?php if(isset($_GET['class'])){
                                                             if($_GET['class'] < 13){
                                                        ?>
                                                        <option>--Select Section--</option>
                                                        <?php }elseif($_GET['class'] == 14 || $_GET['class'] == 15){?>
                                                        <option>--Select Stream--</option>
                                                        <?php }} ?>
                                                        <?php if(isset($_GET['class'])){
                                                             if($_GET['class'] < 13){
                                                        ?>
                                                        @foreach($allsection as $mysections)
                                                            <option value="{{$mysections->id}}">{{$mysections->section_display_name}}</option>
                                                        @endforeach
                                                        <?php }elseif($_GET['class'] == 14 || $_GET['class'] == 15){?>
                                                         @foreach($allstream as $mystreams)
                                                            <option value="{{$mystreams->id}}">{{$mystreams->highsubname}}</option>
                                                        @endforeach
                                                        
                                                        <?php }} ?>
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Title</th>
                                              <th>Description</th>
                                              <th>Start Date</th>
                                              <th>End Date</th>
                                              <th>Days Left</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                  



                                        <tr>
                                        
                                         <td class="statusset-">{!!$i++!!}</td>
                                         
                                         <td class="statusset-"></td>
                                         
                                         <td class="statusset-"></td>

                                         <td class="statusset-"></td>

                                         <td class="statusset-"></td>
                                         
                                         <td></td>

                                        <td><button type="button" class="btn btn-icon-only blue" data-toggle="modal"  data-target="#myModal-"><i class="fa fa-edit"></i></button><button class="btn btn-icon-only red btn-delete" title="announce"  data-id=""><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="myModal-" role="dialog">
                                            <div class="modal-dialog">
    
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('announce/update')}}" method="post" role="form" id="school-edit-" class="school-update-" enctype="multipart/form-data">
                                       
                                       {{method_field('PATCH')}}
                                        <!--Announcement Title-->
                                        
                                        <div class="form-group col-md-12">
                                          <label for="inputName">Title</label><br>
                
                                            <input type="text" name="title" id="title" class="form-control"  
                                                data-msg-date="The Title field is required. must be a date." data-msg-required="The title field is required." 
                                                 data-rule-date="true" data-rule-required="true" value="">
                                                @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
                                        </div>
                                        <!--Announcement Decsription-->
                                        <!--Announcement Decsription-->
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-date="The description field is required. must be a date." data-msg-required="The end date field is required." 
                                                 data-rule-date="true" data-rule-required="true" required></textarea>
                                                    <div id="editor_error"> </div>
                                                @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="col-md-6 editor">
                                                <div class="col-md-3">
                                                    <div class="input-group input-medium date date-picker" data-date="<?php echo date('Y/m/d');?>" data-date-format="yyyy/mm/dd" data-date-viewmode="years" data-date-minviewmode="months">
                                                        <input type="text" class="form-control" placeholder="startdate" name="datestart" value="" readonly>
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!--enddate-->
                                            <div class="form-group">
                                                <div class="col-md-6 editor">
                                                        <div class="col-md-3">
                                                            <div class="input-group input-medium date date-picker" data-date="<?php echo date('Y/m/d');?>" data-date-format="yyyy/mm/dd" data-date-viewmode="years" data-date-minviewmode="months">
                                                                <input type="text" class="form-control" placeholder="enddate" name="dateend" value="" readonly>
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                       
            
                                         </form>
                                        <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-').submit()">Update changes</button>
                                        </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                        </td>

                                         </tr>

                                    

                                              </tbody>
                                            </table>
                                    </div>
                                                </div>  
                                                </div>
                                                <div class="tab-pane" id="tab2">
                                                    <form action="{{url('announce/store')}}" method="post" role="form" id="announce-add" class="announce-add"  enctype="multipart/form-data">
                                        
                                        
                                        <div class="form-group col-md-12">
                                          <label for="inputName">Title</label><br>
                                            <input type="hidden" name="schoolid" value=""/>
                                            <input type="text" name="title" id="title" class="form-control"  
                                                data-msg-required="The title field is required." 
                                                 data-rule-required="true" required>
                                                @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
                                        </div>
                                        <!--Announcement Decsription-->
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                 data-rule-required="true" required></textarea>
                                                    <div id="editor_error"> </div>
                                                @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 editor">
                                                    <div class="col-md-3">
                                                        <div class="input-group input-medium date date-picker" data-date="<?php echo date('Y/m/d');?>" data-date-format="yyyy/mm/dd" data-date-viewmode="years" data-date-minviewmode="months">
                                                            <input type="text" class="form-control" placeholder="startdate" name="datestart" readonly>
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!--enddate-->
                                            <div class="form-group">
                                                <div class="col-md-6 editor">
                                                        <div class="col-md-3">
                                                            <div class="input-group input-medium date date-picker" data-date="<?php echo date('Y/m/d');?>" data-date-format="yyyy/mm/dd" data-date-viewmode="years" data-date-minviewmode="months">
                                                                <input type="text" class="form-control" placeholder="enddate" name="dateend"
                                                                data-msg-required="The Date field is required." 
                                                                data-rule-required="true"  required readonly>
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                             <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#announce-add').submit()">Save</button>
                                             </div>
                                        </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                    </div>
                </div>
            </div>
@endsection