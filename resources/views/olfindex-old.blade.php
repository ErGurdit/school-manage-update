@extends('layouts.homepage.header')
@section('content')

   <body>
      <div class="wrapper">
         <header>
            <div class="top_header">
               <!--=======================================carousel========================================================================================================-->
               <div id="demo" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ul class="carousel-indicators">
                     <li data-target="#demo" data-slide-to="0" class="active"></li>
                     <li data-target="#demo" data-slide-to="1"></li>
                     <li data-target="#demo" data-slide-to="2"></li>
                     <li data-target="#demo" data-slide-to="3"></li>
                  </ul>
                  <!--=======================================Wrapper==for==slides========================================================================================================-->
                  <div class="carousel-inner">
                     <div class="carousel-item active">
                        <img src="{{asset('theme/assets/images/banner.png')}}" class="img-fluid sld_img" alt="slide1">
                        <div class="carousel-caption">
                           <div class="left_caption">
                           <h3 class="wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">Dedicated to Excellence</h3>
                         <p class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">There are many variations of passages of Lorem Ipsum available.</p>
                           
                           </div>
                           
                        </div>
                     </div>
                     <div class="carousel-item">
                        <img src="{{asset('theme/assets/images/banner.png')}}" class="img-fluid sld_img" alt="slide2">
                         <div class="carousel-caption">
                           <div class="left_caption">
                           <h3 class="wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">Dedicated to Excellence</h3>
                           <p class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">There are many variations of passages of Lorem Ipsum available.</p>
                           </div>
                          
                        </div>
                     </div>
                     <div class="carousel-item">
                        <img src="{{asset('theme/assets/images/banner.png')}}" class="img-fluid sld_img" alt="slide3">
                         <div class="carousel-caption">
                           <div class="left_caption">
                           <h3 class="wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">Dedicated to Excellence</h3>
                           <p class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">There are many variations of passages of Lorem Ipsum available.</p>
                           
                           </div>
                          
                        </div>
                     </div>
                     <div class="carousel-item">
                        <img src="{{asset('theme/assets/images/banner.png')}}" class="img-fluid sld_img" alt="slide4">
                        <div class="carousel-caption">
                           <div class="left_caption">
                           <h3 class="wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">Dedicated to Excellence</h3>
                           <p class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">There are many variations of passages of Lorem Ipsum available.</p>
                          
                           </div>
                          
                        </div>
                     </div>
                      <div class="right_caption">
                           <h3 class="wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">Announcement</h3>
                          <p class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">There are many variations of passages of Lorem Ipsum available, but the majority have suffered</p>
                           <p class="wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">Pricipal of Primary School - 12 years of working experiece in education industry.</p>
                           
                           </div>
                     <!--=======================================Contact==Number========================================================================================================-->
                     <div class="captn">
                        <div class="container">
                           <div class="row">
                              <div class="col-sm-3 col-xs-6 col-lg-3 col-xl-3 col-md-3">
                                 <a class="contact_top"href="callto:+18002684686"><i class="fa fa-phone"></i>+91172-5020309 </a>
                              </div>
                              <!--=======================================Mail==Social==icons========================================================================================================-->
                              <div class="col-sm-9 col-xs-12 col-lg-9 col-xl-9 col-md-9">
                                 <ul class="form-inline mail pull-right hidden-xs">
                                    <li><a href="mailto:abcd@mail.com"><i class="fa fa-envelope-o"></i><span>abcd@mail.com</span></a></li>
                                    <li class="login"><a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-user"></i>Log In </a></li>
                                    <li class="hidden-xs"><a href=""><i class="fa fa-facebook"></i></a></li>
                                    <li class="hidden-xs"><a href=""><i class="fa fa-twitter"></i></a></li>
                                    <li class="hidden-xs"><a href=""><i class="fa fa-instagram"></i></a></li>
                                    <li class="hidden-xs"><a href=""><i class="fa fa-google-plus"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        
                        <!--=======================================NAV========================================================================================================-->
                      <div class="bootsnipp-search">
                      <div class="container">
                        <form action="https://bootsnipp.com/search" method="GET" role="search">
                          <div class="input-group">
                            <input class="form-control" name="q" placeholder="Search for snippets and hit enter" type="text">
                            <span class="input-group-btn">
                              <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
                            </span>
                          </div>
                        </form>
                      </div>
                      </div>
                        <nav class="navbar main-nav navbar-expand-lg navbar-dark ">
                           <div class="container">
                              <a id="brand" class="navbar-brand " href="index.html"><img alt="Logo" src="{{asset('theme/assets/images/logo.png')}}"></a>
                              
                              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="fa fa-bars"></span>
                              </button>
                              <a class="tgl"><span class="fa fa-search"></span></a>
                              
                              
                              
                                   
                              <div class="collapse navbar-collapse" id="navbarsExample07">
                                 <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active">
                                       <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" href="About.html">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link " href="hire_us.html">Why you hire us</a>
                                    </li>
                                    <li class="nav-item ">
                                       <a class="nav-link" href="join_us.html" >Join us</a>
                                    </li>
                                    <li class="nav-item ">
                                       <a class="nav-link" href="contactus.html" >Contact</a>
                                    </li>
                                    <li class="visible-xs nav-item"><a href="mailto:abcd@mail.com"><i class="fa fa-envelope-o"  class="nav-link"></i><span>abcd@mail.com</span></a></li>
                                    <li class="visible-xs nav-item"><a href="callto:+18002684686"><i class="fa fa-phone"  class="nav-link"></i><span>+91172-5020309</span></a></li>
                                    <li class="visible-xs nav-item"><a href="" data-toggle="modal" data-target="#myModal"  class="nav-link"><i class="fa fa-user"></i>Log In </a></li>
                                   <ul class="form-inline">
                                    <li class="visible-xs nav-item"><a href="" class="nav-link"><i class="fa fa-facebook"></i></a></li>
                                    <li class="visible-xs nav-item"><a href="" class="nav-link"><i class="fa fa-twitter"></i></a></li>
                                    <li class="visible-xs nav-item"><a href="" class="nav-link"><i class="fa fa-instagram"></i></a></li>
                                    <li class="visible-xs nav-item"><a href="" class="nav-link"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                 </ul>
                                 <form class="form-inline hidden-xs " id="custom-search-form">
                                    <div class="input-append span12">
                                       <input type="text" class="search-query mac-style" placeholder="Search">
                                       <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </header>
         <!-- Carrousel Strips blue-->
         <div class="beleving_strip wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h5><span class="left_rh"></span>Seeing is Believing Watch The Websites<span class="right_rh"></span></h5>
         </div>
         <!--=======================================about--section========================================================================================================-->
         <div class="container">
            <div class="about">
               <div class="row">
                  <div class="col-sm-7 col-lg-7 col-xl-7 col-md-7 pad-0 wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
                     <img src="{{asset('theme/assets/images/about.png')}}" class="img-fluid" alt="about-us">
                  </div>
                  <div class="col-sm-5 col-lg-5 col-xl-5 col-md-5 pad-0 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                     <div class="about_txt">
                        <h2>About School</h2>
                        <p>I have learnt a lot from frriends, teachers and people here. And it’s my honor to become the representative pupil of Primary to enter the national writing contest. I will try my best to not let you down. Thansk a lot!</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--=======================================static==straight==section========================================================================================================-->
         <section class="static wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms" style="background:url(images/bg.png)no-repeat">
            <div class="container">
               <div class="row">
                  <div class="col-sm-6 col-lg-6 col-xl-6 col-xs-12 visible-xs">
                     <h3 class="text-center">School </h3>
                     <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel" >
                        <!-- slides -->
                        <div class="carousel-inner"  style="background:url(images/fun_phn.png)no-repeat">
                           <div class="carousel-item active">
                              <img src="{{asset('theme/assets/images/fun_in.png')}}" class="img-fluid" alt="enjoying">
                           </div>
                           <div class="carousel-item">
                              <img src="{{asset('theme/assets/images/fun_in.png')}}" class="img-fluid" alt="enjoying">
                           </div>
                           <div class="carousel-item">
                              <img src="{{asset('theme/assets/images/fun_in.png')}}" class="img-fluid" alt="enjoying">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-lg-6 col-xl-6 col-xs-12">
                     <h3>Straight </h3>
                     <ul class="straights_list">
                        <li><span class="img_class"><img src="{{asset('theme/assets/images/school_icon.png')}}" alt="icn1" class="img-fluid"></span><span class="wht">Total Schools</span><br/><span class="blk"><i>500 </i></span></li>
                        <li><span class="img_class"><img src="{{asset('theme/assets/images/people_icon.png')}}" alt="icn2" class="img-fluid"></span><span class="wht">Total staff</span><br/><span class="blk"><i>500 </i></span></li>
                        <li><span class="img_class"><img src="{{asset('theme/assets/images/student_icon.png')}}" alt="icn3" class="img-fluid"></span><span class="wht">Total Students</span><br/><span class="blk"><i>5000 </i></span></li>
                     </ul>
                  </div>
                  <div class="col-sm-6 col-lg-6 col-xl-6 col-6 hidden-xs">
                     <h3 class="text-center">School </h3>
                     <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel" >
                        <!-- slides -->
                        <div class="carousel-inner"  style="background:url(images/fun_phn.png)no-repeat">
                           <div class="carousel-item active">
                              <img src="{{asset('theme/assets/images/fun_in.png')}}" class="img-fluid" alt="enjoying">
                           </div>
                           <div class="carousel-item">
                              <img src="{{asset('theme/assets/images/fun_in.png')}}" class="img-fluid" alt="enjoying">
                           </div>
                           <div class="carousel-item">
                              <img src="{{asset('theme/assets/images/fun_in.png')}}" class="img-fluid" alt="enjoying">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--=======================================Gallery=section========================================================================================================-->
         <section class="galry">
            <div class="container">
               <h2>Gallery</h2>
               
                  <ul class="gallry_img wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
                     <li>
                        <div class="figure">
                           <a href="">
                              <img src="{{asset('theme/assets/images/glry1.png')}}" alt="gallery1" class="img-fluid gal-img">
                              <div class="img_des">
                                 <h4>Active Learning</h4>
                                
                              </div>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div class="figure">
                           <a href="">
                              <img src="{{asset('theme/assets/images/glry2.png')}}" alt="gallery2" class="img-fluid gal-img">
                              <div class="img_des">
                                 <h4>Proffessional Teacher</h4>
                                 
                              </div>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div class="figure">
                           <a href="">
                              <img src="{{asset('theme/assets/images/glry3.png')}}" alt="gallery3" class="img-fluid gal-img">
                              <div class="img_des">
                                 <h4>Parents Day</h4>
                                 
                              </div>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div class="figure">
                           <a href="">
                              <img src="{{asset('theme/assets/images/glry4.png')}}" alt="gallery4" class="img-fluid gal-img">
                              <div class="img_des">
                                 <h4>Music Leasson</h4>
                                
                              </div>
                           </a>
                        </div>
                     </li>
                  </ul>
              
            </div>
         </section>
         <!--=======================================call_consultr========================================================================================================-->	
         <section class="call_consult wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
            <div class="container">
               <div class="row">
                  <div class="col-sm-3 col-md-3 col-lg-3 col-xs-6 col-xl-3 hidden-xs">
                     <img src="{{asset('theme/assets/images/girl.png')}}" class="img-fluid grl wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms" alt="Call Girl">
                  </div>
                  <div class="col-sm-12 col-md-9 col-lg-9 col-xs-12 col-xl-9">
                     <div class="consltion wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <form>
                           <h2>CALL TO CONSULTATION</h2>
                           <div class="row">
                              <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="name" placeholder="Name:">
                                 </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="email" placeholder="Email">
                                 </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="phone" placeholder="Phone:">
                                 </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="time" placeholder="Preferred time to contact you:">
                                 </div>
                              </div>
                              <div class="col-sm-12 col-xl-12 col-md-12 col-lg-12">
                                 <div class="form-group">
                                    <textarea class="form-control" id="msg" placeholder="Messages:"></textarea>
                                 </div>
                              </div>
                           </div>
                           <button class="submit">Submit Now</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <a href="javascript:" id="return-to-top" style="display: block;"><i class="fa fa-angle-up"></i></a>
      </div>
      <!-- Wrapper-->
      <!--=======================================footer========================================================================================================-->	
      <footer>
         <div class="in-footer">
            <div class="container">
               <div class="top-footer">
                  <div class="row ">
                     <div class="col-sm-2 col-xs-6 col-lg-2 col-xl-2 col-md-2">
                        <div class="logo_ftr">
                           <a href="index.html"><img src="{{asset('theme/assets/images/logo.png')}}" alt="logo" class="img-fluid"></a>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <h4>ABOUT</h4>
                        <p>These men promptly escaped from a maximum security 
                           stock ade to the Los Angeles underground these Happy 
                           Days are yours and mine Happy Days as long wrong with 
                           that the Brady Bunch these men promptly escaped from a
                           maximum security.
                        </p>
                     </div>
                     <div class="col-sm-2 col-xs-6 col-lg-2 col-xl-2 col-md-2">
                        <h4>Our Links</h4>
                        <ul class="links">
                           <li ><a href="index.html">Home</a></li>
                           <li><a href="About.html">About Us  </a></li>
                           <li><a href="hire_us.html">Why you hire us</a></li>
                           <li><a href="join_us.html"> Join us</a></li>
                           <li><a href="contactus.html"> Contact</a></li>
                        </ul>
                     </div>
                     <div class="col-sm-2 col-xs-6 col-lg-2 col-xl-2 col-md-2">
                        <h4>Other links</h4>
                        <ul class="links">
                           <li ><a href="login.html">Student Login</a></li>
                           <li><a href="fee.html">Courses Fee</a></li>
                           <li><a href="Infrastructure.html">Infrastructure</a></li>
                        </ul>
                     </div>
                     <div class="col-sm-3 col-lg-3 col-xl-3 col-md-3 col-xs-6">
                        <h4>Contact us</h4>
                        <ul class="links">
                           <li >Acd , industrial area</li>
                           <li>Mohali- 160 059, Punjab (India) </li>
                           <li><i class="fa fa-phone"></i>+(91)-(172)-2236607 / 2236822</li>
                           <li><i class="fa fa-envelope-o"></i>info@bhasin-packard.com </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <p class="btm_footer">© Copyright 2017. All Right Reserved.</p>
         </div>
      </footer>
      <!-- jquery JS --> <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <!-- proper JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
      <!-- bootstrap JS -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
      <!-- custom JS -->
      <script src="{{asset('theme/assets/js/custom.js')}}"></script>
      <script src="{{asset('theme/assets/js/wow.min.js')}}"></script>
   </body>
</html>
@endsection