@extends('layouts.principaltest.app')

@section('content')
<!-- Page -->
<div class="page">
	<div class="page-content container-fluid">
	  	<div class="row">
			<div class="col-lg-12">
			  	<!-- Panel -->
			  	<div class="panel">
					<div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
						<ul class="nav nav-tabs nav-tabs-line" role="tablist">
							<li class="nav-item" role="presentation">
								<a class="active nav-link" data-toggle="tab" href="#general"
								aria-controls="activities" role="tab">General</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" data-toggle="tab" href="#security" aria-controls="profile"
								role="tab">Security</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" data-toggle="tab" href="#social" aria-controls="messages"
								role="tab">Social</a>
							</li>
					  	</ul>
						<div class="tab-content">
							<div class="tab-pane animation-slide-left active" id="general" role="tabpanel">
							  	<div class="row">
									<div class="col-md-12 profilesec">
								  		<div class="row">
										<div class="col-md-6">
											<div class="form-group form-material profilelabel">
												<input type="hidden" name="userid" id="userid" placeholder="userid" value="{{$userid->id}}">
												<label class="form-control-label" for="inputBasicName">Name</label>
												<input type="text" name="name" id="name" class="form-control edit-data-name hide-name changefirstname" placeholder="Name" value="{{$userid->name}}">
												<a href="#" class="fa fa-check checkname-icon hide-name newchangename newchangedataname"></a>
												<a href="#" class="fa fa-close checkname-icon hide-name"></a>
												<div class="profile-content-data-name">
													<a href="#" class="fa fa-pencil edit-icon-name">&nbsp;Edit</a>
													<span class="newname">{{$userid->name}}</span>
												</div>
												<div id="editor_error"> </div>
												@if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif
									  		</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-material profilelabel">
												<label class="form-control-label" for="inputUsername">Username</label>
												<input type="text" name="username" class="form-control edit-data-username hide-username changeusername" placeholder="Username" value="{{$userid->username}}">
												<a href="#" class="fa fa-check checkusername-icon hide-username newchangeusername newchangedatausername"></a>
												<a href="#" class="fa fa-close checkusername-icon hide-username"></a>
												<div class="profile-content-data-username">
													@if($userid->usernamecount > 0)
														<a href="#" class="fa fa-pencil edit-icon-username">&nbsp;Edit</a>
													@else
													<span class="newusername">{{$userid->username}}</span>
													<div id="editor_error"> </div>
														<p class="help-block">Sorry limit is exceed</p>
													@endif
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-material profilelabel">
												<label class="form-control-label" for="inputEmail">Email</label>
												<input type="text" name="email" class="form-control edit-data-email hide-email changeemail" placeholder="Email" value="{{$userid->email}}">
												<a href="#" class="fa fa-check checkemail-icon hide-email newchangeemail newchangedataemail"></a>
												<a href="#" class="fa fa-close checkemail-icon hide-email"></a>
												<div class="profile-content-data-email">
													<a href="#" class="fa fa-pencil edit-icon-email">&nbsp;Edit</a>
													<span class="newemail">{{$userid->email}}</span>
												</div>
												<div id="editor_error"> </div>
												@if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
										  	</div>
										</div>
									</div>
									</div>
					  			</div>
							</div>
							<div class="tab-pane animation-slide-left" id="security" role="tabpanel">
						  		<div class="row">
									<div class="col-md-12 profilesec">
									  	<div class="row">
									  		<div class="col-md-6 checkpassold">
												<div class="form-group form-material passwordlabel">
													<label class="form-control-label" for="inputoldpassword">Change Password</label><a href="#" class="fa fa-pencil edit-icon-oldpassword">&nbsp;Change</a>
													<input type="password" name="oldpassword" class="form-control edit-data-oldpassword hide-oldpassword changeoldpassword" placeholder="Oldpassword" value="">
													<a href="#" class="fa fa-check checkoldpassword-icon hide-oldpassword newchangeoldpassword newchangedataoldpassword"></a>
													<a href="#" class="fa fa-close checkoldpassword-icon hide-oldpassword"></a>
													<div class="profile-content-data-oldpassword">
													
														<span class="newoldpassword"></span>
													</div>
													<div id="editor_error"> </div>
													@if ($errors->has('oldpassword'))<p class="help-block">{{ $errors->first('oldpassword')}}</p>@endif
											  	</div>
											</div>
											<!--<div class="col-md-6">
												<div class="form-group form-material profilelabel">
													<label class="form-control-label" for="inputoldpassword">Old Password</label>
													<input type="text" name="oldpassword" class="form-control edit-data-oldpassword hide-oldpassword changeoldpassword" placeholder="Oldpassword" value="">
													<a href="#" class="fa fa-check checkoldpassword-icon hide-oldpassword newchangeoldpassword newchangedataoldpassword"></a>
													<a href="#" class="fa fa-close checkoldpassword-icon hide-oldpassword"></a>
													<div class="profile-content-data-oldpassword">
														<a href="#" class="fa fa-pencil edit-icon-oldpassword">&nbsp;Edit</a>
														<span class="newoldpassword"></span>
													</div>
													<div id="editor_error"> </div>
													@if ($errors->has('oldpassword'))<p class="help-block">{{ $errors->first('oldpassword')}}</p>@endif
											  	</div>
											</div>-->
											<div class="col-md-6 authchangepassword" style="display:none;">
												<div class="form-group form-material passwordlabel">
													<label class="form-control-label" for="inputPassword">Password </label>
												</div>
												<div class="form-group form-material">
											  	<input type="password" name="password" class="form-control changepassword textpass passtextbox" id="password" placeholder="Password">
													<!--<a href="#" class="fa fa-check checkpassword-icon hide-password newchangepassword newchangedatapassword"></a>-->
													<!--<a href="#" class="fa fa-close checkpassword-icon hide-password"></a>-->
													<div class="profile-content-data-password">
														
														<span class="newpasswordchangesucces"></span>
														<!--<span id="message"></span>-->
													</div>
													<div id="editor_error"> </div>
													@if ($errors->has('password'))<p class="message">{{ $errors->first('password')}}</p>@endif
												</div>
											</div>
											<div class="col-md-6 changepasswordsuccess changeconfirmpassword confirmpasswordsuccess authchangepassword" style="display:none;">
												<div class="form-group form-material profilelabel">
													<label class="form-control-label" for="inputnewconfirmpassword">Confirm Password</label>
												</div>
												<div class="form-group form-material profilelabel">
													<input type="password" name="newconfirmpassword" class="form-control edit-data-newconfirmpassword hide-newconfirmpassword changenewconfirmpassword" id="confirm_password" placeholder="Change Password" value="">
													<a href="#" class="fa fa-check checknewconfirmpassword-icon hide-newconfirmpassword newchangenewconfirmpassword newchangedatanewconfirmpassword similarpass"></a>
													<a href="#" class="fa fa-close checknewconfirmpassword-icon hide-newconfirmpassword"></a>
													<div class="profile-content-data-newconfirmpassword">
														<!--<a href="#" class="fa fa-pencil edit-icon-newconfirmpassword">&nbsp;Edit</a>-->
														<span class="newnewconfirmpassword"></span>
														<span id="messagelength"></span>
													</div>
													<div id="editor_error"> </div>
													@if ($errors->has('newconfirmpassword'))<p class="help-block">{{ $errors->first('newconfirmpassword')}}</p>@endif
											  	</div>
											</div>
										</div>
									</div>
						  		</div>
							</div>
							<div class="tab-pane animation-slide-left" id="social" role="tabpanel">
							  	<div class="row">
									<div class="col-md-12 profilesec">
								  		<div class="row">
								  			<input type="hidden" name="teachernewid" id="teachernewid" value="{{$teacher->id}}">
											<div class="col-md-6">
											  	<div class="form-group form-material profilelabel profilefb">
													<label class="form-control-label" for="inputBasicFacebook">Facebook</label><a href="#" class="fa fa-pencil edit-icon-facebook">&nbsp;Edit</a>
												</div>
												<div class="form-group form-material profilelabel">
													<input type="text" name="facebook" id="facebook" class="form-control edit-data-facebook hide-facebook changefacebook" placeholder="Facebook Link" value="{{$teacher->facebook}}">
													<a href="#" class="fa fa-check checkfacebook-icon hide-facebook newchangefacebook newchangedatafacebook"></a>
													<a href="#" class="fa fa-close checkfacebook-icon hide-facebook"></a>
													<div class="profile-content-data-facebook">
														<!--<a href="#" class="fa fa-pencil edit-icon-facebook">&nbsp;Edit</a>-->
														<span class="newfacebook">{{$teacher->facebook}}</span>
													</div>
													<div id="editor_error"> </div>
													 @if ($errors->has('facebook'))<p class="help-block">{{ $errors->first('facebook')}}</p>@endif
											  	</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-material profilelabel profileinsta">
													<label class="form-control-label" for="inputBasicInstagram">Instagram</label><a href="#" class="fa fa-pencil edit-icon-Instagram">&nbsp;Edit</a>
												</div>
												<div class="form-group form-material profilelabel">
													<input type="text" name="Instagram" id="Instagram" class="form-control edit-data-Instagram hide-Instagram changeInstagram" placeholder="Instagram" value="{{$teacher->instagram}}">
													<a href="#" class="fa fa-check checkInstagram-icon hide-Instagram newchangeInstagram newchangedataInstagram"></a>
													<a href="#" class="fa fa-close checkInstagram-icon hide-Instagram"></a>
													<div class="profile-content-data-mother">
														<!--<a href="#" class="fa fa-pencil edit-icon-Instagram">&nbsp;Edit</a>-->
														<span class="newInstagram">{{$teacher->instagram}}</span>
													</div>
													<div id="editor_error"> </div>
													@if ($errors->has('Instagram'))<p class="help-block">{{ $errors->first('Instagram')}}</p>@endif
											  	</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-material profilelabel profiletweet">
													<label class="form-control-label" for="inputBasictweet">Twitter</label><a href="#" class="fa fa-pencil edit-icon-tweet">&nbsp;Edit</a>
												</div>
												<div class="form-group form-material profilelabel">
													<input type="text" name="tweet" class="form-control edit-data-tweet hide-tweet changetweet" placeholder="Twitter" value="{{$teacher->twitter}}">
													<a href="#" class="fa fa-check checktweet-icon hide-tweet newchangetweet newchangedatatweet"></a>
													<a href="#" class="fa fa-close checktweet-icon hide-tweet"></a>
													<div class="profile-content-data-tweet">
														<!--<a href="#" class="fa fa-pencil edit-icon-tweet">&nbsp;Edit</a>-->
														<span class="newtweet">{{$teacher->twitter}}</span>
													</div>
													<div id="editor_error"> </div>
													@if ($errors->has('tweet'))<p class="help-block">{{ $errors->first('tweet')}}</p>@endif
											  	</div>
											</div>
										</div>
									</div>
				  				</div>
							</div>
			  			</div>
					</div>
			  	</div>
				<!-- End Panel -->
			</div>
		</div>
	</div>
</div>
  <!-- End Page -->
@endsection