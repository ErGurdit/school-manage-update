@extends('layouts.admin.app')

@section('content')

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
<section id="content">

<div class="page page-dashboard">

<h3>Roles</h3>
<a href="{{route('role.create')}}" class="btn btn-success">Create Role</a>
<table class="table">
    <tr>
        <th>Name</th>
        <th>Display Name</th>
        <th>Description</th>
        <th>Action</th>
    </tr>
    @forelse($role as $roles)
    <tr>
        <td>{{$roles->name}}</td>
        <td>{{$roles->display_name}}</td>
        <td>{{$roles->description}}</td>
        
        <td>
            <a href="{{url('permission')}}" title="permission" class="myIcon icon-success icon-ef-3 icon-ef-3a hover-color">
                <i class="fa fa-user"></i>
            </a>
            <a href="{{route('role.edit',$roles->id)}}"  title="edit" class="myIcon icon-success icon-ef-3 icon-ef-3a hover-color">
                <i class="fa fa-edit"></i>
            </a>
            @role('Superadmin')
            <form action="{{route('role.destroy',$roles->id)}}" method="POST">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <input type="submit" value="Delete"/>
            </form>
            @endrole
        </td>
    </tr>
    @empty
    <tr>
        <td>No Role</td>
    </tr>
    @endforelse
</table>
                   
</div>
</section>
            <!--/ CONTENT -->

@endsection


