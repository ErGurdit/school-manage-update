@extends('layouts.admin.app2')
@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
<div id="wrap" class="animsition">
    <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
    <section id="content">
        <div class="page page-tables-datatables">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-12">
                    <div class="school-filter-btn">
                        <button type="button" class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10" data-toggle="modal" data-target="#addModal" >
                           <i class="fa fa-plus"></i><span> Add School</span>
                         </button>
                        <a href="{{url('school')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('school')){{'activeclass'}}@endif">
                            <i class="fa fa-tasks"></i><span>All School</span>
                        </a>
                        <a href="{{url('school/show')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('school/show')){{'activeclass'}}@endif">
                            <i class="fa fa-star"></i><span>Active School</span>
                        </a>
                        <a href="{{url('school/showin')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('school/showin')){{'activeclass'}}@endif">
                            <i class="fa fa-minus"></i><span>Inactive School</span>
                        </a>
                
                    </div>
                       <!-- Modal -->
            
                        <section class="tile">


                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <tr>
                                                <th>Name</th>
                                                <th>Permission</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>
                                         $tempdata=array();
                                         <?php foreach($role as $roles){
                                          $tempdata[$roles->display_name]=$roles;
                                            }?>
                                         @foreach($tempdata as $roles)



                                        <tr style="color:{{$roles->status ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$roles->id}}">{{$roles->display_name}}</td>
                                         
                                         <td class="statusset-{{$roles->id}}">
                                             @foreach($permission as $permissions)
                                             <?php $per = '';?>
                                             <?php /*if ($permissions->id == $roles->roleid) {
                        	                    $per = ' checked="checked"';
                                                }*/
                                            ?>
                                             <label class="checkbox-inline checkbox-custom">
                                                    <input type="checkbox" <?php //echo $per; ?> name="fbstatus" id="inlineCheckbox4" class="chkfb form-control" value="1"><i></i>{{$permissions->display_name}}
                                                </label>
                                            @endforeach
                                        </td>

                                         <td class="statusset-{{$roles->id}}">{{$roles->description}}</td>

                                        <td>@if($roles->status == 1)<input type='checkbox' class="editstatus" title ="school" name='checkbox2' rel="{{$roles->status}}" checked  onchange="changestatus({{$roles->id}},this)"/>@elseif($roles->status == 0)<input type='checkbox' class="editstatus" title ="school"  name='checkbox2'  rel="{{$roles->status}}" onchange="changestatus({{$roles->id}},this)"/>@endif</td>
                                        </td>

                                         </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                </div>
                                <!-- /tile body -->

                            </section>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ CONTENT -->
</div>
@endsection