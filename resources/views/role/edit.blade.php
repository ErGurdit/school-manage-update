@extends('layouts.admin.appform')

@section('content')

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
<section id="content">

<div class="page page-dashboard">

<!-- tile -->
                            <section class="tile">
                                    <h2>Edit Role</h2>
                                <!-- tile body -->
                                <div class="tile-body">

                                    <form name="form2" role="form" id="form2" data-parsley-validate action="{{route('role.update',$role)}}" method="POST">
                                        {{method_field('PATCH')}}
                                        {{ csrf_field() }}
                                        
                                        <div class="row">

                                            <div class="form-group col-md-6">
                                                <label for="name">Role Name: </label>
                                                
                                                <input type="text" name="name" id="name" class="form-control" value="{{$role->name}}" required>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="displayname">Role Display Name: </label>
                                                <input type="text" name="display_name" id="display_name" class="form-control" value="{{$role->display_name}}" required>
                                            </div>
                                            
                                            <div class="form-group col-md-6">
                                                <label for="description">Role Display Name: </label>
                                                <input type="text" name="description" id="description" class="form-control" value="{{$role->description}}" required>
                                            </div>
                                            
                                            
                                            <div class="form-group col-md-12">
                                                <label for="description">Permissions: </label><br>
                                                @foreach($permission as $permissions)
                                                <input type="checkbox" {{in_array($permissions->id,$role_permission)?"checked":""}} name="permission[]" value="{{$permissions->id}}"/>{{$permissions->name}}<br>
                                                @endforeach
                                            </div>

                                        </div>
   
                                        <div class="tile-footer text-right bg-tr-black lter dvd dvd-top">
                                    <!-- SUBMIT BUTTON -->
                                    <button type="submit" class="btn btn-lightred" id="form2Submit">Submit</button>
                                </div>

                            </form>

                                </div>
                                <!-- /tile body -->

                        </section>
                            <!-- /tile -->
                   
</div>
</section>
            <!--/ CONTENT -->

@endsection


