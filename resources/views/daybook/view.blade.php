@extends('layouts.principaltest.app')

@section('content')
<?php 
    //dd($student);
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <!--<button class="btn btn-primary" data-target="#myClass" data-toggle="modal"-->
            <!--type="button">Add Certificate</button>-->
                <div class="panel-body container-fluid admission">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                            <tr>
                               <th>#</th>
                <th>Date</th>
                <th>Student Name(s)</th>
              </tr>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                @foreach($student as $students)
              <tr>
                <td>{{$i++}}</td>
                <td>{{date('d-m-Y', strtotime($students->updated_at))}}</td>
                     <td>{{$students->name}}</td>
   </tr>
                                         @endforeach
                        </tbody>
                    </table>
                </div>
                
                </div><!--panl-->
            
        </div>
    </div>
@endsection