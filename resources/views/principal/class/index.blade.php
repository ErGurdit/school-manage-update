@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <a class=" btn green btn-outline sbold" data-target="#stack1" data-toggle="modal">Add/Remove Classes</a>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Classes</div>
                                       </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th>Classes</th>
                                                        <th>Sections</th>
                                                        <th>Male Students</th>
                                                        <th>Female Students</th>
                                                        <th>Total Students</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($class as $classes)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$classes->cdisplay}}</td>
                                                        <td>
                                                        <!--display sections-->
                                                        @foreach($sectionclass as $sectionclasses)
                                                        @if(!empty($tempsec))
                                                        @if(0 == $classes->hasstream)
                                                        @if(!empty($classsection_map[$classes->cid]) && in_array($sectionclasses->id,$classsection_map[$classes->cid]))
                                                            <?php echo $sectionclasses->section_display_name .',';?>
                                                        @endif
                                                        @endif
                                                        @endif
                                                        @endforeach
                                                        @if(1 == $classes->hasstream)
                                                        @foreach($stream as $streams)
                                                        @if(!empty($classsection_map[$classes->cid]) && in_array($streams->id,$classsection_map[$classes->cid]))
                                                            <?php echo $streams->highsubname.',';?>
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                        </td>
                                                        <td>15</td>
                                                        <td>15</td>
                                                        <td>30</td>
                                                    </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Class</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <!-- Modal -->
                             <div id="stack1" class="modal fade" tabindex="-1" data-width="400">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">Add/Remove Classes</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                               <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    @if(!empty($newdata))
                                                    <form action="{{url('class/classupdate')}}" class="horizontal-form" method="post">
                                                    @else
                                                    <form action="{{url('class/classstore')}}" class="horizontal-form" method="post">
                                                    @endif
                                                        {{ csrf_field() }}
                                                        <div class="form-body">
                                                            <div class="row">
                                                               <div class="form-group">
                                                                <div class="mt-checkbox-inline">
                                                            <!--<input type="hidden" name="schoolid" value="{{$user->schoolid}}">-->
                                                            <input type="hidden" name="schoolname" id="schoolname" class="form-control" value="{{$user->schoolname}}" readonly>
                                                            <div class="classes">
                                                                <h4>Classes</h4>
                                                            </div>
                                                            <div class="section">
                                                                <h4>Sections</h4>
                                                            </div>
                                                            @foreach($myclass as $myclasses)
                                                            @if(!empty($newdata))
                                                            <div class="margin-bottom">
                                                                <div class="md-checkbox classes">
                                                                    <input type="checkbox" id="checkbox{{$myclasses->id}}" class="md-check"  <?php if(in_array($myclasses->id,$newdata)){echo 'checked';}?> name="classname[]"  value="{{$myclasses->id}}">
                                                                <label for="checkbox{{$myclasses->id}}" class="block" id="{{$myclasses->class_name}}">
                                                                   <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> {{$myclasses->class_display_name}}
                                                                </label>
                                                                </div>
                                                                <div class="section">
                                                                @if($myclasses->class_name != 'nursery' || $myclasses->class_name != 'lkg' || $myclasses->class_name != 'ukg')
                                                                    <!--<label>Section</label>-->
                                                                @endif
                                                                <div class="col-md-12 md-checkbox-inline md-checkbox-inline-stream">
                                                                <?php $i=0;?>
                                                                @foreach($sectionclass as $sectionclasses)
                                                                @if(!empty($tempsec))
                                                                @if(0 == $myclasses->has_stream)
                                                                <div class="col-md-2 md-checkbox charges">
                                                                        <?php $secid = $sectionclasses->id.$i++.$myclasses->id;?>
                                                                        <input type="checkbox" id="checkbox<?php echo $secid;?>" name="sectionclass[{{$myclasses->id}}][]" <?php if(!empty($classsection_map[$myclasses->id]) && in_array($sectionclasses->id,$classsection_map[$myclasses->id])){echo 'checked';}?> value="{{$sectionclasses->id}}">
                                                                        <label for="checkbox<?php echo $secid;?>">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span>{{$sectionclasses->section_display_name}}
                                                                        </label>
                                                                    </div>
                                                                @endif
                                                                @endif
                                                                @endforeach
                                                                </div>
                                                                @if(1 == $myclasses->has_stream)
                                                                <div class="col-md-12 md-checkbox-inline md-checkbox-inline-stream ">
                                                                    <?php $i=0;?>
                                                                    @foreach($stream as $streams)
                                                                    <div class="col-md-6 md-checkbox charges sectionmargintop">
                                                                    <?php $streamid = $streams->id.$i++.$myclasses->id;?>
                                                                    <input type="checkbox" id="checkbox<?php echo $streamid;?>" name="sectionclass[{{$myclasses->id}}][]" <?php if(!empty($classsection_map[$myclasses->id]) && in_array($streams->id,$classsection_map[$myclasses->id])){echo 'checked';}?> value="{{$streams->id}}">
                                                                        <label for="checkbox<?php echo $streamid;?>">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span>{{$streams->highsubname}}
                                                                        </label>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                                @endif
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            @else
                                                            <label class="mt-checkbox classes" id="{{$myclasses->class_name}}">
                                                                <input type="checkbox"  <?php //if(in_array($myclasses->id,$newdata)){echo 'checked';}?> name="classname[]" id="inlineCheckbox2" value="{{$myclasses->id}}">{{$myclasses->class_display_name}}
                                                                <span></span>
                                                            </label>
                                                            @endif
                                                           @endforeach
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div> 
                                                    <div class="modal-footer">
                                                            <input type="submit" class="btn btn-info" value="Add Classes">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                        </div>
                                    </div>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection