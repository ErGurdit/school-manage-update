@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;"> Option 1</a>
                                                    </li>
                                                    <li class="divider"> </li>
                                                    <li>
                                                        <a href="javascript:;">Option 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 4</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable tabbable-tabdrop">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab1" data-toggle="tab">Staff Attendence</a>
                                                </li>
                                                <li>
                                                    <a href="#tab2" data-toggle="tab">School Attendence</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab1">
                                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Name</th>
                                              <th>Designation</th>
                                              <th>Total Present Days</th></th>
                                              <th>Total Absent</th>
                                              <th>Total Leaves</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                   @foreach($slider as $sliders)



                                        <tr style="color:{{$sliders->status ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>
                                         
                                          <td class="statusset-{{$sliders->id}}"><a href="#" data-toggle="modal"  data-target="#staffname-{{$sliders->id}}">Ankit</a></td>
                                         
                                         <td class="statusset-{{$sliders->id}}">Teacher</td>
                                         
                                         <td class="statusset-{{$sliders->id}}">20</td>
                                         
                                         <td class="statusset-{{$sliders->id}}">10</td>

                                         <td>10</td>
                                         <td><button type="button" class="btn btn-icon-only blue" data-toggle="modal"  data-target="#myModal-{{$sliders->id}}"><i class="fa fa-edit"></i></button><button class="btn btn-icon-only red btn-delete" title="slider"  data-id="{{$sliders->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="myModal-{{$sliders->id}}" role="dialog">
                                            <div class="modal-dialog">
    
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$sliders->topcontent}}</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('slider/update',$sliders->id)}}" method="post" role="form" id="school-edit-{{$sliders->id}}" class="school-update-{{$sliders->id}}" enctype="multipart/form-data">
                                       
                                       {{method_field('PATCH')}}
                                        <!--Top Content-->
                                        
                                        <div class="form-group col-md-6">
                                          <label for="top-content">Top Content</label><br>
                
                                            <input type="text" name="topcontent" id="name" class="form-control"  
                                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Content field is required." 
                                                 data-rule-date="true" data-rule-required="true" value="{{$sliders->topcontent}}" required>
                                                @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                        </div>
                                        <!--Bottom Content-->
                                        <div class="form-group col-md-6">
                                          <label for="bottomcontent">Bottom Content</label><br>
                                             <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                data-msg-date="The Content field is required. must be a date." data-msg-required="The Content field is required." 
                                                data-rule-date="true" data-rule-required="true" value="{{$sliders->bottomcontent}}" required>
                                                @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                        </div>
                                         <!--Slider-->
                                            <div class="form-group col-md-10">
                                                <label for="slider">slider: </label><br>
                                                <img src="/theme/mainimage/{{$sliders->slider}}" width="200" height="100"><br>
                                                <input type="file" name="filename" id="email" class="form-control" required>
                                                @if ($errors->has('file'))<p class="help-block">{{ $errors->first('file')}}</p>@endif
                                            </div>
                                         </form>
                                         <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$sliders->id}}').submit()">Save changes</button>
                                     </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                        <div class="modal fade" id="staffname-{{$sliders->id}}" role="dialog">
                                            <div class="modal-dialog">
    
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$sliders->topcontent}}</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('slider/update',$sliders->id)}}" method="post" role="form" id="school-edit-{{$sliders->id}}" class="school-update-{{$sliders->id}}" enctype="multipart/form-data">
                                       
                                       {{method_field('PATCH')}}
                                        <!--Top Content-->
                                        
                                        <div class="form-group col-md-6">
                                          <label for="top-content">Top Content</label><br>
                
                                            <input type="text" name="topcontent" id="name" class="form-control"  
                                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Content field is required." 
                                                 data-rule-date="true" data-rule-required="true" value="{{$sliders->topcontent}}" required>
                                                @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                        </div>
                                        <!--Bottom Content-->
                                        <div class="form-group col-md-6">
                                          <label for="bottomcontent">Bottom Content</label><br>
                                             <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                data-msg-date="The Content field is required. must be a date." data-msg-required="The Content field is required." 
                                                data-rule-date="true" data-rule-required="true" value="{{$sliders->bottomcontent}}" required>
                                                @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                        </div>
                                         <!--Slider-->
                                            <div class="form-group col-md-10">
                                                <label for="slider">slider: </label><br>
                                                <img src="/theme/mainimage/{{$sliders->slider}}" width="200" height="100"><br>
                                                <input type="file" name="filename" id="email" class="form-control" required>
                                                @if ($errors->has('file'))<p class="help-block">{{ $errors->first('file')}}</p>@endif
                                            </div>
                                         </form>
                                         <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$sliders->id}}').submit()">Save changes</button>
                                     </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                        </td>

                                         </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                                    
                                                </div>
                                                <div class="tab-pane" id="tab2">
                                                   <div class="table-responsive">
                                  
                                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                              <th>SNo.</th>
                                                              <th>Name</th>
                                                              <th>Father Name</th>
                                                              <th>Class</th>
                                                              <th>Total Present Days</th></th>
                                                              <th>Total Absent</th>
                                                              <th>Total Leaves</th>
                                                              <th>Action</th>
                                                            </tr>
                                                          </thead>
                                                          <tbody>
                                                              <?php $i = 1;?>
                
                                                   @foreach($slider as $sliders)
                
                
                
                                                        <tr style="color:{{$sliders->status ? '' : 'red' }}">
                                                        
                                                         <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>
                                         
                                                         <td class="statusset-{{$sliders->id}}"><a href="#" data-toggle="modal"  data-target="#schname-{{$sliders->id}}">Ankit</a></td>
                                                         
                                                         <td class="statusset-{{$sliders->id}}">Rajendra Gupta</td>
                                                         
                                                         <td class="statusset-{{$sliders->id}}">First Class</td>
                                                         
                                                         <td class="statusset-{{$sliders->id}}">20</td>
                                                         
                                                         <td class="statusset-{{$sliders->id}}">10</td>
                
                                                         <td>10</td>
                                                         <td><button type="button" class="btn btn-icon-only blue" data-toggle="modal"  data-target="#myModal-{{$sliders->id}}"><i class="fa fa-edit"></i></button><button class="btn btn-icon-only red btn-delete" title="slider"  data-id="{{$sliders->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                        <div class="modal fade" id="myModal-{{$sliders->id}}" role="dialog">
                                                            <div class="modal-dialog">
                    
                                                          <!-- Modal content-->
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$sliders->topcontent}}</h5>
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            </div>
                                                         <div class="modal-body">
                                                     <div class="row">
                                                    <div class="col-md-12">
                                                     <form action="{{url('slider/update',$sliders->id)}}" method="post" role="form" id="school-edit-{{$sliders->id}}" class="school-update-{{$sliders->id}}" enctype="multipart/form-data">
                                                       
                                                       {{method_field('PATCH')}}
                                                        <!--Top Content-->
                                                        
                                                        <div class="form-group col-md-6">
                                                          <label for="top-content">Top Content</label><br>
                                
                                                            <input type="text" name="topcontent" id="name" class="form-control"  
                                                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Content field is required." 
                                                                 data-rule-date="true" data-rule-required="true" value="{{$sliders->topcontent}}" required>
                                                                @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                                        </div>
                                                        <!--Bottom Content-->
                                                        <div class="form-group col-md-6">
                                                          <label for="bottomcontent">Bottom Content</label><br>
                                                             <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                                data-msg-date="The Content field is required. must be a date." data-msg-required="The Content field is required." 
                                                                data-rule-date="true" data-rule-required="true" value="{{$sliders->bottomcontent}}" required>
                                                                @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                                        </div>
                                                         <!--Slider-->
                                                            <div class="form-group col-md-10">
                                                                <label for="slider">slider: </label><br>
                                                                <img src="/theme/mainimage/{{$sliders->slider}}" width="200" height="100"><br>
                                                                <input type="file" name="filename" id="email" class="form-control" required>
                                                                @if ($errors->has('file'))<p class="help-block">{{ $errors->first('file')}}</p>@endif
                                                            </div>
                                                         </form>
                                                         <div class="modal-footer col-md-12">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$sliders->id}}').submit()">Save changes</button>
                                                     </div>
                                                     </div>
                                                     </div>
                                                </div>
                                            </div>
                                                    
                                                    </div>
                                                  </div>
                                                        <div class="modal fade" id="schname-{{$sliders->id}}" role="dialog">
                                                                <div class="modal-dialog">
                        
                                                              <!-- Modal content-->
                                                              <div class="modal-content">
                                                                <div class="modal-header">
                                                                  <h5 class="modal-title" id="addModalLabel">Edit-{{$sliders->topcontent}}</h5>
                                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                </div>
                                                             <div class="modal-body">
                                                         <div class="row">
                                                        <div class="col-md-12">
                                                         <form action="{{url('slider/update',$sliders->id)}}" method="post" role="form" id="school-edit-{{$sliders->id}}" class="school-update-{{$sliders->id}}" enctype="multipart/form-data">
                                                           
                                                           {{method_field('PATCH')}}
                                                            <!--Top Content-->
                                                            
                                                            <div class="form-group col-md-6">
                                                              <label for="top-content">Top Content</label><br>
                                    
                                                                <input type="text" name="topcontent" id="name" class="form-control"  
                                                                    data-msg-date="The Name field is required. must be a date." data-msg-required="The Content field is required." 
                                                                     data-rule-date="true" data-rule-required="true" value="{{$sliders->topcontent}}" required>
                                                                    @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                                            </div>
                                                            <!--Bottom Content-->
                                                            <div class="form-group col-md-6">
                                                              <label for="bottomcontent">Bottom Content</label><br>
                                                                 <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                                    data-msg-date="The Content field is required. must be a date." data-msg-required="The Content field is required." 
                                                                    data-rule-date="true" data-rule-required="true" value="{{$sliders->bottomcontent}}" required>
                                                                    @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                                            </div>
                                                             <!--Slider-->
                                                                <div class="form-group col-md-10">
                                                                    <label for="slider">slider: </label><br>
                                                                    <img src="/theme/mainimage/{{$sliders->slider}}" width="200" height="100"><br>
                                                                    <input type="file" name="filename" id="email" class="form-control" required>
                                                                    @if ($errors->has('file'))<p class="help-block">{{ $errors->first('file')}}</p>@endif
                                                                </div>
                                                             </form>
                                                             <div class="modal-footer col-md-12">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$sliders->id}}').submit()">Save changes</button>
                                                         </div>
                                                         </div>
                                                         </div>
                                                    </div>
                                                </div>
                                                        
                                                        </div>
                                                      </div>
                                                        </td>
                
                                                         </tr>
                
                                                    @endforeach
                
                                                              </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                    </div>
                </div>
            </div>
@endsection