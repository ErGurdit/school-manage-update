@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Assign Role</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Role</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($user as $users)
                                                   
                                                    <tr>
                                                        <td>{{$users->name}}</td>
                                                        <td>
                                                            @foreach($users->roles as $role)
                                                                {{$role->name}}
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal-{{$users->id}}">
                                                                  Edit
                                                            </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal-{{$users->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">{{$users->name}} Role Edit</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                     <div class="modal-body">
                                                        <form action="{{route('user.update',$users->id)}}" method="post" role="form" id="role-form-{{$users->id}}">
                                                        {{csrf_field()}}
                                                        {{method_field('PATCH')}}
                                                        <div class="form-group">
                                                          <select name="roles[]" multiple>
                                                              @foreach($allrole as $role)
                                                              <option value="{{$role->id}}">{{$role->name}}</option>
                                                              @endforeach
                                                          </select>  
                                                        </div>
                                                        
                                                        <!--<input class="btn btn-primary" type="submit" value="Submit"/>-->
                                                    </form>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" onclick="$('#role-form-{{$users->id}}').submit()">Save changes</button>
                                                  </div>
                                                </div>
                                                </div>
                                                </div>
                                                    </td>
                                                </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Role</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
                </div>
@endsection