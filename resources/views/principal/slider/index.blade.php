@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;"> Option 1</a>
                                                    </li>
                                                    <li class="divider"> </li>
                                                    <li>
                                                        <a href="javascript:;">Option 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 4</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable tabbable-tabdrop">
                                            <ul class="nav nav-tabs">
                                                <li class="">
                                                    <a href="#tab1" data-toggle="tab">All Slider</a>
                                                </li>
                                                <li>
                                                    <a href="#tab2" data-toggle="tab">Add Slider</a>
                                                </li>
                                                <li>
                                                    <a href="#tab3" data-toggle="tab" onclick="myFunction()">Active Slider</a>
                                                </li>
                                                <li>
                                                    <a href="#tab4" data-toggle="tab" onclick="myFunction()">Inactive Slider</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab1">
                                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Username</th>
                                              <th>Image</th>
                                              <th>Heading</th></th>
                                              <th>Description</th>
                                              <th>Slider Status</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                   @foreach($slider as $sliders)



                                        <tr style="color:{{$sliders->status ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>
                                         
                                          <td class="statusset-{{$sliders->id}}">{!!$sliders->uname!!}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}"><img src="/theme/mainimage/{!!$sliders->slider!!}" width="100" height="100"></td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{$sliders->topcontent}}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{substr($sliders->bottomcontent,0,60)}}</td>

                                         <td>@if($sliders->status == 1)<input type='checkbox' class="editstatus make-switch" data-on-text="Active" data-off-text="Inactive" title ="slider" name='checkbox2' rel="{{$sliders->status}}" checked  onchange="changestatus({{$sliders->id}},this)"/>@elseif($sliders->status == 0)<input type='checkbox' class="editstatus make-switch" data-on-text="Active" data-off-text="Inactive" title ="slider"  name='checkbox2'  rel="{{$sliders->status}}" onchange="changestatus({{$sliders->id}},this)"/>@endif</td>
                                         <td><button type="button" class="btn btn-icon-only blue" data-toggle="modal"  data-target="#myModal-{{$sliders->id}}"><i class="fa fa-edit"></i></button><button class="btn btn-icon-only red btn-delete" title="slider"  data-id="{{$sliders->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="myModal-{{$sliders->id}}" role="dialog">
                                            <div class="modal-dialog">
    
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$sliders->topcontent}}</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('slider/update',$sliders->id)}}" method="post" role="form" id="school-edit-{{$sliders->id}}" class="school-update-{{$sliders->id}}" enctype="multipart/form-data">
                                       
                                       {{method_field('PATCH')}}
                                        <!--Top Content-->
                                        
                                        <div class="form-group col-md-6">
                                          <label for="top-content">Top Content</label><br>
                
                                            <input type="text" name="topcontent" id="name" class="form-control"  
                                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Content field is required." 
                                                 data-rule-date="true" data-rule-required="true" value="{{$sliders->topcontent}}" required>
                                                @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                        </div>
                                        <!--Bottom Content-->
                                        <div class="form-group col-md-6">
                                          <label for="bottomcontent">Bottom Content</label><br>
                                             <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                data-msg-date="The Content field is required. must be a date." data-msg-required="The Content field is required." 
                                                data-rule-date="true" data-rule-required="true" value="{{$sliders->bottomcontent}}" required>
                                                @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                        </div>
                                         <!--Slider-->
                                            <div class="form-group col-md-10">
                                                <label for="slider">slider: </label><br>
                                                <img src="/theme/mainimage/{{$sliders->slider}}" width="200" height="100"><br>
                                                <input type="file" name="filename" id="email" class="form-control" required>
                                                @if ($errors->has('file'))<p class="help-block">{{ $errors->first('file')}}</p>@endif
                                            </div>
                                         </form>
                                         <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$sliders->id}}').submit()">Save changes</button>
                                     </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                        </td>

                                         </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                                    
                                                </div>
                                                <div class="tab-pane" id="tab2">
                                                    <form action="{{url('slider/store')}}" method="post" role="form" id="school-add" enctype="multipart/form-data">
                                        <!--Top Content-->
                                        <div class="form-group col-md-6">
                                        <label for="top-content">Top Content: </label>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="text" name="topcontent" id="name" class="form-control"  
                                            data-msg-date="The Content field is required. must be a date." data-msg-required="The Name field is required." 
                                            data-rule-date="true" data-rule-required="true"  required>
                                                @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                            </div>
                                            <!--Principal Name-->
                                            <div class="form-group col-md-6">
                                                <label for="bottom-content">Bottom Content: </label>
                                                <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                data-msg-date="The Content Name field is required. must be a date." data-msg-required="The Principal Name field is required." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                            </div>
                                            <div class="form-group col-md-4" id="wrapper">
                                                <label for="email">Slider: </label>
                                                <input type="file" name="filename" id="fileupload" class="form-control" accept="image/*" onchange="preview_image(event)" required>
                                                <br><br>
                                                <img id="output_image"/>
                                                @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                            </div>
                                           <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#school-add').submit()">Save</button>
                                             </div>
                                        </form>
                                                </div>
                                                <div class="tab-pane" id="tab3">
                                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Username</th>
                                              <th>Image</th>
                                              <th>Heading</th></th>
                                              <th>Description</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                   @foreach($slideractive as $sliders)



                                        <tr style="color:{{$sliders->status ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>
                                         
                                          <td class="statusset-{{$sliders->id}}">{!!$sliders->uname!!}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}"><img src="/theme/mainimage/{!!$sliders->slider!!}" width="100" height="100"></td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{$sliders->topcontent}}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{$sliders->bottomcontent}}</td>
                                        
                                        </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab4">
                                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Username</th>
                                              <th>Image</th>
                                              <th>Heading</th></th>
                                              <th>Description</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                   @foreach($sliderinactive as $sliders)



                                        <tr style="color:{{$sliders->status ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>
                                         
                                          <td class="statusset-{{$sliders->id}}">{!!$sliders->uname!!}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}"><img src="/theme/mainimage/{!!$sliders->slider!!}" width="100" height="100"></td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{$sliders->topcontent}}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{$sliders->bottomcontent}}</td>

                                        </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                    </div>
                </div>
            </div>
@endsection