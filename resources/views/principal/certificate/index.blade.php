@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <button type="button" class="btn blue btn-block" data-toggle="modal" data-target="#myholiday">Add Exams</button>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Exams</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th>Exam Name</th>
                                                        <th>Student name</th>
                                                        <th>Username</th>
                                                        <th>class</th>
                                                        <th>class incharge</th>
                                                        <th>Total Marks</th>
                                                        <th>Obtain Marks</th>
                                                        <th>Percentage</th>
                                                        <th>Position</th>
                                                        <th>Result</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($user as $users)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$users->uname}}</td>
                                                        <td>{{$users->username}}</td>
                                                        <td>{{$users->uemail}}</td>
                                                        <td>{{$users->certificate}}</td>
                                                        <td>{{$users->approved_by}}</td>
                                                        <td>{{$users->approved_date}}%</td>
                                                        <td>{{$users->issued_by}}</td>
                                                        <td>{{$users->issued_date}}</td>
                                                        <td>{{$users->uname}}</td>
                                                        <td>{{$users->received_date}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon-only blue" title="class" data-toggle="modal" data-target="#mycertificateetail-{{$users->uid}}">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <div class="modal fade" id="mycertificateetail-{{$users->uid}}" role="dialog">
                                                            <div class="modal-dialog">
                                
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">{{$users->uname}}</h4>
                                                                    </div>
                                                                <div class="modal-body">
                                                                    <div class="portlet light bg-inverse">
                                                                            <div class="portlet-title">
                                                                                <div class="tools">
                                                                                    <a href="" class="collapse"> </a>
                                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                                    <a href="" class="reload"> </a>
                                                                                    <a href="" class="remove"> </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="portlet-body form">
                                                                                <!-- BEGIN FORM-->
                                                                                <form action="{{url('holiday/update',$users->uid)}}" class="horizontal-form" method="post">
                                                                                    {{ csrf_field() }}
                                                                                    {{method_field('PATCH')}}
                                                                                    <div class="form-body">
                                                                                        <div class="row">
                                                                                             <div class="form-group col-md-6">
                                                                                              <label for="top-content">Student Name</label><br>
                                                                    
                                                                                                <input type="text" name="uname" id="uname" class="form-control"  
                                                                                                    data-msg-required="The Name field is required." 
                                                                                                     data-rule-required="true" value="{{$users->uname}}" readonly>
                                                                                                    @if ($errors->has('uname'))<p class="help-block">{{ $errors->first('uname')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Email</label><br>
                                                                    
                                                                                                <input type="text" name="email" id="email" class="form-control"  
                                                                                                    data-msg-required="The Email field is required." 
                                                                                                     data-rule-required="true" value="{{$users->uemail}}" readonly>
                                                                                                    @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Certificate</label><br>
                                                                    
                                                                                                <input type="text" name="certificate" id="certificate" class="form-control"  
                                                                                                    data-msg-required="The Certificate field is required." 
                                                                                                     data-rule-required="true" value="{{$users->certificate}}" readonly>
                                                                                                    @if ($errors->has('certificate'))<p class="help-block">{{ $errors->first('certificate')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Approved By</label><br>
                                                                    
                                                                                                <input type="text" name="approved_by" id="approved_by" class="form-control"  
                                                                                                    data-msg-required="The approve field is required." 
                                                                                                     data-rule-required="true" value="{{$users->approved_by}}" readonly>
                                                                                                    @if ($errors->has('obtainmarks'))<p class="help-block">{{ $errors->first('obtainmarks')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Approved Date</label><br>
                                                                    
                                                                                                <input type="text" name="approved_date" id="approved_date" class="form-control"  
                                                                                                    data-msg-required="The approved date field is required." 
                                                                                                     data-rule-required="true" value="{{$users->approved_date}}" readonly>
                                                                                                    @if ($errors->has('approved_date'))<p class="help-block">{{ $errors->first('approved_date')}}</p>@endif
                                                                                            </div>
                                                                                             <div class="form-group col-md-6">
                                                                                              <label for="top-content">Issued By</label><br>
                                                                    
                                                                                                <input type="text" name="issued_by" id="issued_by" class="form-control"  
                                                                                                    data-msg-required="The issued field is required." 
                                                                                                     data-rule-required="true" value="{{$users->issued_by}}" readonly>
                                                                                                    @if ($errors->has('issued_by'))<p class="help-block">{{ $errors->first('issued_by')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Issued Date</label><br>
                                                                    
                                                                                                <input type="text" name="issued_date" id="issued_date" class="form-control"  
                                                                                                    data-msg-required="The Issued Date field is required." 
                                                                                                     data-rule-required="true" value="{{$users->issued_date}}" readonly>
                                                                                                    @if ($errors->has('issued_date'))<p class="help-block">{{ $errors->first('issued_date')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Received By</label><br>
                                                                    
                                                                                                <input type="text" name="received_by" id="received_by" class="form-control"  
                                                                                                    data-msg-required="The received by field is required." 
                                                                                                     data-rule-required="true" value="{{$users->received_by}}" readonly>
                                                                                                    @if ($errors->has('received_by'))<p class="help-block">{{ $errors->first('received_by')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Received Date</label><br>
                                                                    
                                                                                                <input type="text" name="received_date" id="received_date" class="form-control"  
                                                                                                    data-msg-required="The received date field is required." 
                                                                                                     data-rule-required="true" value="{{$users->uname}}" readonly>
                                                                                                    @if ($errors->has('received_date'))<p class="help-block">{{ $errors->first('received_date')}}</p>@endif
                                                                                            </div>
                                                                                        </div>
                                                                                      </div> 
                                                                                      <div class="modal-footer">
                                                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                </form>
                                                                                <!-- END FORM-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                    </div>
                                                    </td>
                                                    </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Exam</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <!-- Modal -->
                              <div class="modal fade" id="myholiday" role="dialog">
                                <div class="modal-dialog">
    
                                <!-- Modal content-->
                                <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Holiday</h4>
                                        </div>
                                    <div class="modal-body">
                                        <div class="portlet light bg-inverse">
                                                <div class="portlet-title">
                                                    <div class="tools">
                                                        <a href="" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="" class="reload"> </a>
                                                        <a href="" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="{{url('holiday/store')}}" class="horizontal-form" method="post">
                                                        {{ csrf_field() }}
<div class="form-body">
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Student Name</label><br>
                                                                    <input type="text" name="uname" id="uname" class="form-control"  
                                                                    data-msg-required="The Name field is required." 
                                                                    data-rule-required="true">
                                                                    @if ($errors->has('uname'))<p class="help-block">{{ $errors->first('uname')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                             <label for="top-content">Email</label><br>
                                                                <input type="text" name="email" id="email" class="form-control"  
                                                                data-msg-required="The Email field is required." 
                                                                data-rule-required="true">
                                                                @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Certificate</label><br>
                                                                    <input type="text" name="certificate" id="certificate" class="form-control"  
                                                                    data-msg-required="The Certificate field is required." 
                                                                    data-rule-required="true">
                                                                    @if ($errors->has('certificate'))<p class="help-block">{{ $errors->first('certificate')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Approved By</label><br>
                                                                <input type="text" name="approved_by" id="approved_by" class="form-control"  
                                                                data-msg-required="The approve field is required." 
                                                                data-rule-required="true">
                                                                @if ($errors->has('obtainmarks'))<p class="help-block">{{ $errors->first('obtainmarks')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Approved Date</label><br>
                                                                <input type="text" name="approved_date" id="approved_date" class="form-control"  
                                                                data-msg-required="The approved date field is required." 
                                                                data-rule-required="true">
                                                                @if ($errors->has('approved_date'))<p class="help-block">{{ $errors->first('approved_date')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Issued By</label><br>
                                                                <input type="text" name="issued_by" id="issued_by" class="form-control"  
                                                                data-msg-required="The issued field is required." 
                                                                data-rule-required="true">
                                                                @if ($errors->has('issued_by'))<p class="help-block">{{ $errors->first('issued_by')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Issued Date</label><br>
                                                                <input type="text" name="issued_date" id="issued_date" class="form-control"  
                                                                data-msg-required="The Issued Date field is required." 
                                                                data-rule-required="true">
                                                                @if ($errors->has('issued_date'))<p class="help-block">{{ $errors->first('issued_date')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Received By</label><br>
                                                                <input type="text" name="received_by" id="received_by" class="form-control"  
                                                                data-msg-required="The received by field is required." 
                                                                data-rule-required="true">
                                                                @if ($errors->has('received_by'))<p class="help-block">{{ $errors->first('received_by')}}</p>@endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="top-content">Received Date</label><br>
                                                                <input type="text" name="received_date" id="received_date" class="form-control"  
                                                                data-msg-required="The received date field is required." 
                                                                data-rule-required="true" value="{{$users->uname}}" readonly>
                                                                @if ($errors->has('received_date'))<p class="help-block">{{ $errors->first('received_date')}}</p>@endif
                                                            </div>
                                                        </div>
                                                    </div>  
                                                          <div class="modal-footer">
                                                              <input type="submit" class="btn btn-info">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection