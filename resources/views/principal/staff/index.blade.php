@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <button type="button" class="btn blue btn-block" data-toggle="modal" data-target="#myClass">Add Staff Memeber</button>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Staff Member</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Date of Joining</th>
                                                        <th>Role checkboxes</th>
                                                        <th>Class Incharge</th>
                                                        <th>Leaves</th>
                                                        <th>Salary</th>
                                                        <th>Status</th>
                                                        <th>Action Buttons</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($staffmember as $staffmembers)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$staffmembers->uname}}</td>
                                                        <td>{{$staffmembers->doj}}</td>
                                                        <td>{{$staffmembers->rdisplay}}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>{{$staffmembers->salary}}</td>
                                                        <?php  
                                                            $join = strtotime($staffmembers->doj);
                                                            $now = strtotime(date('y-m-d'));
                                                            $datediff = $join-$now;
                                                            $diff = round($datediff / (60 * 60 * 24));
                                                        ?>
                                                        @if($diff >= 1)
                                                        <td>Pending</td>
                                                        @elseif($diff == 1)
                                                        <td>Joining</td>
                                                        @elseif($diff < 1)
                                                        <td>Working</td>
                                                        @endif
                                                        <td>
                                                            <button class="btn btn-icon-only blue" title="edit" data-toggle="modal" data-target="#mydetailedit-{{$staffmembers->uid}}">
                                                            <i class="fa fa-pencil"></i>
                                                            </button>
                                                           <button class="btn btn-icon-only red btn-delete" title="staff"  data-id="{{$staffmembers->uid}}">
                                                               <i class="fa fa-times"></i>
                                                           </button>
                                                           <button type="button" class="btn btn-icon-only blue" title="detail" data-toggle="modal" data-target="#mydetail-{{$staffmembers->uid}}">
                                                                <i class="fa fa-user"></i>
                                                            </button>
                                                            <!--Details view-->
                                                            <div class="modal fade" id="mydetail-{{$staffmembers->uid}}" role="dialog">
                                                            <div class="modal-dialog">
                                
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">{{$staffmembers->uname}}</h4>
                                                                    </div>
                                                                <div class="modal-body">
                                                                    <div class="portlet light bg-inverse">
                                                                            <div class="portlet-title">
                                                                                <div class="tools">
                                                                                    <a href="" class="collapse"> </a>
                                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                                    <a href="" class="reload"> </a>
                                                                                    <a href="" class="remove"> </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="portlet-body form">
                                                                                <!-- BEGIN FORM-->
                                                                               <form role="form" action="{{url('staff/update')}}" id="staffdata" method="post">
                                                                                            {{csrf_field()}}
                                                                                        <div class="form-body">
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputnName">Name</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Full Name" data-msg-date="The Content field is required." data-msg-required="The Name field is required." 
                                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->uname}}" readonly>
                                                                                                <span class="input-group-addon">
                                                                                                    <i class="fa fa-user font-blue"></i>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputPassword1">Username</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="username" data-msg-date="The Username field is required." data-msg-required="The Username field is required." 
                                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->username}}" readonly>
                                                                                                <span class="input-group-addon">
                                                                                                    <i class="fa fa-user font-blue"></i>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="form-group col-md-6">
                                                                                            <label>Email</label>
                                                                                            <div class="input-group input-icon right">
                                                                                                <span class="input-group-addon">
                                                                                                    <i class="fa fa-envelope font-purple"></i>
                                                                                                </span>
                                                                                                <i class="fa fa-exclamation tooltips" data-original-title="Invalid email." data-container="body"></i>
                                                                                                <input id="email" class="input-error form-control" type="text" name="email" value="{{$staffmembers->uemail}}" data-msg-date="The mail is required." data-msg-required="The mail field is required." 
                                                                                                data-rule-date="true" data-rule-required="true" readonly> </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleDateofJoining">Date Of Joining</label>
                                                                                            <input type="text" class="form-control" placeholder="DOJ" name="doj" value="{{$staffmembers->doj}}" readonly>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleSalary">Salary</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="salary" class="form-control" id="exampleInputPassword1" placeholder="Salary" data-msg-date="The Salary field is required." data-msg-required="The Salary field is required." 
                                                                                                data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->salary}}" readonly>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleSalary">Total Experince</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control" name="totaexp" id="exampleInputPassword1" placeholder="Total Experirnce" data-msg-date="The Experience field is required." data-msg-required="The Experience field is required." 
                                                                                                data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->total_experience}}" readonly>
                                                                                            </div>
                                                                                        </div>
                                                                               
                                                                                       <div class="form-group col-md-12">
                                                                                            <label>Assign Roles</label>
                                                                                            <div class="mt-checkbox-inline">
                                                                                                @forelse($role as $roles)
                                                                                                <?php $con = '';?>
                                                                                                <?php if ($roles->name == $staffmembers->rname) {
                                                                                                    $con = ' checked="checked"';
                                                                                                    }
                                                                                                ?>
                                                                                                <label class="mt-checkbox">
                                                                                                    <input type="checkbox" id="inlineCheckbox1" name="roles[]" <?php echo $con ?>  value="{{$roles->id}}">{{$roles->display_name}}
                                                                                                    <span></span>
                                                                                                </label>
                                                                                                
                                                                                                @empty
                                                                                                @endforelse
                                                                                                
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-actions">
                                                                                        <!--<button type="submit" class="btn blue">Submit</button>-->
                                                                                        <button type="button" class="btn default">Cancel</button>
                                                                                    </div>
                                                                                </form>
                                                                                <!-- END FORM-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                    </div>
                                                            <!--Detailsedit-->
                                                            <div class="modal fade" id="mydetailedit-{{$staffmembers->uid}}" role="dialog">
                                                            <div class="modal-dialog">
                                
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">{{$staffmembers->uname}}</h4>
                                                                    </div>
                                                                <div class="modal-body">
                                                                    <div class="portlet light bg-inverse">
                                                                            <div class="portlet-title">
                                                                                <div class="tools">
                                                                                    <a href="" class="collapse"> </a>
                                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                                    <a href="" class="reload"> </a>
                                                                                    <a href="" class="remove"> </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="portlet-body form">
                                                                                <!-- BEGIN FORM-->
                                                                               <form role="form" action="{{url('staff/update',$staffmembers->uid)}}" id="staffdata" method="post">
                                                                                            {{csrf_field()}}
                                                                                            {{method_field('PATCH')}}
                                                                                        <div class="form-body">
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputnName">Name</label>
                                                                                            <div class="input-group">
                                                                                                <input type="hidden" name="userid" value="{{$staffmembers->uid}}"/>
                                                                                                <input type="hidden" name="schoolid" value="{{$school->id}}"/>
                                                                                                <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Full Name" data-msg-date="The Content field is required." data-msg-required="The Name field is required." 
                                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->uname}}" readonly>
                                                                                                <span class="input-group-addon">
                                                                                                    <i class="fa fa-user font-blue"></i>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputPassword1">Username</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="username" data-msg-date="The Username field is required." data-msg-required="The Username field is required." 
                                                                                                data-rule-date="true" data-rule-required="true" value="{{$staffmembers->username}}" readonly>
                                                                                                <span class="input-group-addon">
                                                                                                    <i class="fa fa-user font-blue"></i>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                         <div class="form-group col-md-6">
                                                                                            <label>Email</label>
                                                                                            <div class="input-group input-icon right">
                                                                                                <span class="input-group-addon">
                                                                                                    <i class="fa fa-envelope font-purple"></i>
                                                                                                </span>
                                                                                                <i class="fa fa-exclamation tooltips" data-original-title="Invalid email." data-container="body"></i>
                                                                                                <input id="email" class="input-error form-control" type="text" name="email" value="{{$staffmembers->uemail}}" data-msg-date="The mail is required." data-msg-required="The mail field is required." 
                                                                                                data-rule-date="true" data-rule-required="true" readonly> </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleDateofJoining">Date Of Joining</label>
                                                                                            <input type="text" class="form-control" placeholder="DOJ" name="doj" value="{{$staffmembers->doj}}" readonly>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleSalary">Salary</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="salary" class="form-control" id="exampleInputPassword1" placeholder="Salary" data-msg-date="The Salary field is required." data-msg-required="The Salary field is required." 
                                                                                                data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->salary}}" readonly>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleSalary">Total Experince</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control" name="totaexp" id="exampleInputPassword1" placeholder="Total Experirnce" data-msg-date="The Experience field is required." data-msg-required="The Experience field is required." 
                                                                                                data-rule-date="true" data-rule-required="true"  value="{{$staffmembers->total_experience}}" readonly>
                                                                                            </div>
                                                                                        </div>
                                                                               
                                                                                       <div class="form-group col-md-12">
                                                                                            <label>Assign Roles</label>
                                                                                            <div class="mt-checkbox-inline">
                                                                                                @forelse($role as $roles)
                                                                                                <?php $con = '';?>
                                                                                                <?php if ($roles->name == $staffmembers->rname) {
                                                                                                    $con = ' checked="checked"';
                                                                                                    }
                                                                                                ?>
                                                                                                <label class="mt-checkbox">
                                                                                                    <input type="checkbox" id="inlineCheckbox1-{{$roles->name}}" name="roles[]" <?php echo $con ?>  value="{{$roles->id}}">{{$roles->display_name}}
                                                                                                    <span></span>
                                                                                                </label>
                                                                                                
                                                                                                @empty
                                                                                                @endforelse
                                                                                                
                                                                                            </div>
                                                                                        </div>
                                                                                        <!--class-->
                                                                                        <div class="form-group col-md-12 classesselect">
                                                                                            <label>Assign Class</label>
                                                                                            <div class="mt-checkbox-inline">
                                                                                                @forelse($class as $classes)
                                                                                                <?php 
                                                                                                $connew='';
                                                                                                if ($classes->class_name == $staffmembers->classid) {
                                                                                                    $connew = ' checked="checked"';
                                                                                                    } 
                                                                                                ?>
                                                                                                <label class="mt-checkbox">
                                                                                                    <input type="checkbox" <?php echo $connew ?> id="inlineCheckbox1" name="classes[]" value="{{$classes->id}}">{{$classes->class_name}}
                                                                                                    <span></span>
                                                                                                </label>
                                                                                                @empty
                                                                                                @endforelse
                                                                                            </div>
                                                                                         </div>
                                                                                    </div>
                                                                                    <div class="form-actions">
                                                                                        <button type="submit" class="btn blue">Submit</button>
                                                                                        <button type="button" class="btn default">Cancel</button>
                                                                                    </div>
                                                                                </form>
                                                                                <!-- END FORM-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                    </div>
                                                            </td>
                                                    </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Staff</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <!-- Modal -->
                              <div class="modal fade" id="myClass" role="dialog">
                                <div class="modal-dialog">
    
                                <!-- Modal content-->
                                <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Staff Member</h4>
                                        </div>
                                    <div class="modal-body">
                                        <div class="portlet light bg-inverse">
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                <form role="form" action="{{url('staff/store')}}" id="staff" method="post">
                                                    {{csrf_field()}}
                                                <div class="form-body">
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputnName">Name</label>
                                                    <div class="input-group">
                                                        <input type="hidden" name="schoolid" value="{{$school->id}}"/>
                                                        <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Full Name" data-msg-required="The Name field is required." 
                                                        data-rule-required="true"  required>
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-user font-blue"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputPassword1">Username</label>
                                                    <div class="input-group">
                                                        <input type="text" name="username" class="form-control" id="exampleInputPassword1" placeholder="username" data-msg-required="The Username field is required." 
                                                        data-rule-required="true"  required>
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-user font-blue"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                 <div class="form-group col-md-6">
                                                    <label>Email</label>
                                                    <div class="input-group input-icon right">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-envelope font-purple"></i>
                                                        </span>
                                                        <i class="fa fa-exclamation tooltips" data-original-title="Invalid email." data-container="body"></i>
                                                        <input id="email" class="input-error form-control" type="text" name="email" value="" data-msg-required="The mail field is required." 
                                                        data-rule-required="true"  required> </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputPassword1">Password</label>
                                                    <div class="input-group">
                                                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" data-msg-required="The password field is required." 
                                                        data-rule-required="true"  required>
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-user font-red"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleDateofJoining">Date Of Joining</label>
                                                    <div class="input-group date date-picker" data-date="<?php echo date('Y/m/d');?>" data-date-format="yyyy/mm/dd" data-date-viewmode="years" data-date-minviewmode="months" data-msg-date="The DOJ field is required. must be a date." data-msg-required="The DOJ field is required." 
                                                        data-rule-date="true" data-rule-required="true"  required>
                                                        <input type="text" class="form-control" placeholder="DOJ" name="doj">
                                                            <span class="input-group-btn">
                                                                <button class="btn default cal" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleSalary">Salary</label>
                                                    <div class="input-group">
                                                        <input type="text" name="salary" class="form-control" id="exampleInputPassword1" placeholder="Salary" data-msg-required="The Salary field is required." 
                                                        data-rule-required="true"  required>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleSalary">Total Experince</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="totaexp" id="exampleInputPassword1" placeholder="Total Experirnce" data-msg-required="The Experience field is required." 
                                                        data-rule-required="true"  required>
                                                    </div>
                                                </div>
                                                
                                                  <div class="form-group col-md-12">
                                                    <label>Assign Roles</label>
                                                    <div class="mt-checkbox-inline">
                                                        @forelse($role as $roles)
                                                        <label class="mt-checkbox">
                                                            <input type="checkbox" id="inlineCheckbox1-{{$roles->name}}" name="roles[]" value="{{$roles->id}}">{{$roles->display_name}}
                                                            <span></span>
                                                        </label>
                                                        @empty
                                                        @endforelse
                                                    </div>
                                                 </div>
                                       
                                               <div class="form-group col-md-12 classesselect">
                                                    <label>Assign Class</label>
                                                    <div class="mt-checkbox-inline">
                                                        @forelse($class as $classes)
                                                        <label class="mt-checkbox">
                                                            <input type="checkbox" id="inlineCheckbox1" name="classes[]" value="{{$classes->id}}">{{$classes->class_name}}
                                                            <span></span>
                                                        </label>
                                                        @empty
                                                        @endforelse
                                                    </div>
                                                 </div>
                                             </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn blue">Submit</button>
                                                <button type="button" class="btn default">Cancel</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection