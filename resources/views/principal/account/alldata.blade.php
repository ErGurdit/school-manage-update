@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <!--<button type="button" class="btn blue btn-block" data-toggle="modal" data-target="#myholiday">Add Exams</button>-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Maintain Fee Charge</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th>Classes</th>
                                                        <th>Charges</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($allclasses as $allclass)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$allclass->class_display_name}}</td>
                                                        <td>
                                                             @foreach($allmaintain as $allmaintains)
                                                                @if($allmaintains->classid == $allclass->id)
                                                                    &#8377;&nbsp;{{str_replace('.00','',$allmaintains->classcharge)}}.00
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon-only blue" title="class" data-toggle="modal" data-target="#myexamdetail-{{$allclass->id}}">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <div class="modal fade" tabindex="-1" aria-hidden="true" id="myexamdetail-{{$allclass->id}}" role="dialog">
                                                            <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                    <h4 class="modal-title">Class Fee</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="portlet-body form">
                                                                        <!-- BEGIN FORM-->
                                                                        
                                                                        <form action="{{url('classfeeupdate')}}" class="horizontal-form" method="post">
                                                                        {{ csrf_field() }}
                                                                        {{method_field('PATCH')}}
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="top-content">Class Name</label>
                                                                                    <input type="hidden" name="classid" value="{{$allclass->id}}"/>
                                                                                    <input type="text" name="name" id="name" class="form-control"  
                                                                                    data-msg-required="The Exam Name field is required." 
                                                                                    data-rule-required="true" value="{{$allclass->class_display_name}}" readonly>
                                                                                    @if ($errors->has('ename'))<p class="help-block">{{ $errors->first('ename')}}</p>@endif
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="top-content">Class Charge</label>
                                                                                    <?php $classcharge='';?>
                                                                                    @foreach($allmaintain as $allmaintains)
                                                                                    @if($allmaintains->classid == $allclass->id)
                                                                                        <?php $classcharge = $allmaintains->classcharge; ?>
                                                                                        @endif
                                                                                    @endforeach
                                                                                    <div class="input-group">
                                                                                            <span class="input-group-addon">
                                                                                                <i class="fa fa-inr"></i>
                                                                                            </span>
                                                                                        <input type="text" name="classcharge" id="name" class="form-control"  
                                                                                        data-msg-required="The Class Charge field is required." 
                                                                                        data-rule-required="true" value="{{$classcharge}}">
                                                                                        @if ($errors->has('classcharge'))<p class="help-block">{{ $errors->first('classcharge')}}</p>@endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                        </form>
                                                                        <!-- END FORM-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                        <tr>
                                            <td>No Exam</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
                    <!-- Modal -->
                              <div class="modal fade" id="myholiday" role="dialog">
                                <div class="modal-dialog">
    
                                <!-- Modal content-->
                                <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Holiday</h4>
                                        </div>
                                    <div class="modal-body">
                                        <div class="portlet light bg-inverse">
                                            <div class="portlet-title">
                                                <div class="tools">
                                                    <a href="" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="" class="reload"> </a>
                                                    <a href="" class="remove"> </a>
                                                </div>
                                            </div>
                                    <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                        <form action="{{url('holiday/store')}}" class="horizontal-form" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="top-content">Exam Name</label>
                                                    <input type="text" name="ename" id="name" class="form-control"  
                                                    data-msg-required="The Exam Name field is required." 
                                                    data-rule-required="true" >
                                                    @if ($errors->has('ename'))<p class="help-block">{{ $errors->first('ename')}}</p>@endif
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-info">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection