@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <button type="button" class="btn blue btn-block" data-toggle="modal" data-target="#myClass">Add Expense</button>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Expenses</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th> Username </th>
                                                        <th> Name </th>
                                                        <th> Expense Amount </th>
                                                        <th> Date </th>
                                                        <th> Description </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($expense as $expenses)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$expenses->username}}</td>
                                                        <td>{{$expenses->exname}}</td>
                                                        <td>{{$expenses->examount}}</td>
                                                        <td>{{$expenses->exdate}}</td>
                                                        <td>{{$expenses->exdescribtion}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon-only blue" title="class" data-toggle="modal" data-target="#myeditClass-{{$expenses->eid}}">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button title="expenses" class="btn btn-icon-only red btn-delete"  data-id="{{$expenses->eid}}">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                            <div class="modal fade" id="myeditClass-{{$expenses->eid}}" role="dialog">
                                                            <div class="modal-dialog">
                                
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">{{$expenses->exname}}</h4>
                                                                    </div>
                                                                <div class="modal-body">
                                                                    <div class="portlet light bg-inverse">
                                                                            <div class="portlet-title">
                                                                                <div class="tools">
                                                                                    <a href="" class="collapse"> </a>
                                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                                    <a href="" class="reload"> </a>
                                                                                    <a href="" class="remove"> </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="portlet-body form">
                                                                                <!-- BEGIN FORM-->
                                                                                   <form action="{{url('expenses/update',$expenses->eid)}}" class="horizontal-form" method="post">
                                                                                    {{ csrf_field() }}
                                                                                    {!! method_field('patch') !!}
                                                                                    <div class="form-body">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label">Expense Name:</label>
                                                                                                    <input type="text" name="name" id="name" class="form-control" value="{{$expenses->username}}">
                                                                                                    <span class="help-block"> @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif </span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label">Expense Amount:</label>
                                                                                                    <input type="text" name="amount" id="amount" class="form-control" value="{{$expenses->examount}}">
                                                                                                    <span class="help-block"> @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif </span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label">Please set expense amount to user:</label>
                                                                                                    <select name="user_select" id="user_select" class="form-control">
                                                                                                        @foreach($user as $users)
                                                                                                        <?php $con = '';?>
                                                                                                        <?php if ($users->uid == $expenses->userid) {
                                	                                                                    $con = ' selected="selected"';
                                                                                                        }
                                                                                                        ?>
                                                                                                        <option <?php echo $con ;?>value="{{$users->uid}}">{{$users->uname}}-{{$users->rname}}</option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                    <span class="help-block"> @if ($errors->has('user'))<p class="help-block">{{ $errors->first('user')}}</p>@endif </span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-5">
                                                                                                <div class="form-group">
                                                                                                <label class="control-label">Expense Date:</label>
                                                                                                    <div class="input-group date date-picker" data-date="<?php echo date('Y/m/d');?>" data-date-format="yyyy/mm/dd" data-date-viewmode="years" data-date-minviewmode="months">
                                                                                                        <input type="text" class="form-control" placeholder="Expense Date" name="expense_date" value="{{$expenses->exdate}}" readonly>
                                                                                                        <span class="input-group-btn">
                                                                                                        <button class="btn default cal-1" type="button">
                                                                                                            <i class="fa fa-calendar"></i>
                                                                                                        </button>
                                                                                                        </span>
                                                                                                        <span class="help-block"> @if ($errors->has('expense_date'))<p class="help-block">{{ $errors->first('expense_date')}}</p>@endif </span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="form-group">
                                                                                                <label class="control-label">Expense Description:</label>
                                                                                                    <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-date="The description field is required. must be a date." data-msg-required="The end date field is required." 
                                                                                                    data-rule-date="true" data-rule-required="true" required>{{$expenses->exdescribtion}}</textarea>
                                                                                                    <div id="editor_error"> </div>
                                                                                                    @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div> 
                                                                                        <div class="modal-footer">
                                                                                            <input type="submit" class="btn btn-info" value="Update">
                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                    </form>
                                                                                   <!-- END FORM-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                    </div>
                                                    </td>
                                                    </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Class</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <!-- Modal -->
                              <div class="modal fade" id="myClass" role="dialog">
                                <div class="modal-dialog">
    
                                <!-- Modal content-->
                                <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Class</h4>
                                        </div>
                                    <div class="modal-body">
                                        <div class="portlet light bg-inverse">
                                                <div class="portlet-title">
                                                    <div class="tools">
                                                        <a href="" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="" class="reload"> </a>
                                                        <a href="" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                   <!-- BEGIN FORM-->
                                                   <form action="{{url('expenses/store')}}" class="horizontal-form" method="post">
                                                    {{ csrf_field() }}
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Expense Name:</label>
                                                                    <input type="text" name="name" id="name" class="form-control">
                                                                    <span class="help-block"> @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Expense Amount:</label>
                                                                    <input type="text" name="amount" id="amount" class="form-control">
                                                                    <span class="help-block"> @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Please set expense amount to user:</label>
                                                                    <select name="user_select" id="user_select" class="form-control">
                                                                        @foreach($user as $users)
                                                                        <option value="{{$users->uid}}">{{$users->uname}}-{{$users->rname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <span class="help-block"> @if ($errors->has('user'))<p class="help-block">{{ $errors->first('user')}}</p>@endif </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                <label class="control-label">Expense Date:</label>
                                                                    <div class="input-group date date-picker" data-date="<?php echo date('Y/m/d');?>" data-date-format="yyyy/mm/dd" data-date-viewmode="years" data-date-minviewmode="months">
                                                                        <input type="text" class="form-control" placeholder="Expense Date" name="expense_date" readonly>
                                                                        <span class="input-group-btn">
                                                                        <button class="btn default cal-1" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                        </span>
                                                                        <span class="help-block"> @if ($errors->has('expense_date'))<p class="help-block">{{ $errors->first('expense_date')}}</p>@endif </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                <label class="control-label">Expense Description:</label>
                                                                    <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-date="The description field is required. must be a date." data-msg-required="The end date field is required." 
                                                                    data-rule-date="true" data-rule-required="true" required></textarea>
                                                                    <div id="editor_error"> </div>
                                                                    @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                        <div class="modal-footer">
                                                            <input type="submit" class="btn btn-info" value="Save Changes">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                   <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection