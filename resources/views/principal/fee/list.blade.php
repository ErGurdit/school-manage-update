@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <button type="button" class="btn blue btn-block" data-toggle="modal" data-target="#myClass">Add Expense</button>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Expenses</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover table-heading-width">
                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th>Name</th>
                                                        <th>Username</th>
                                                        <th>Email</th>
                                                        <th>Gender</th>
                                                        <th>DOB</th>
                                                        <th>Subject</th>
                                                        <th>Class</th>
                                                        <th>Father Name</th>
                                                        <th>Mother Name</th>
                                                        <th>Home Telephone</th>
                                                        <th>Country</th>
                                                        <th>State</th>
                                                        <th>City</th>
                                                        <th>Address</th>
                                                        <th>Pin</th>
                                                        <th>Payment Method</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($alladmission as $alladmissions)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$alladmissions->name}}</td>
                                                        <td>{{$alladmissions->username}}</td>
                                                        <td>{{$alladmissions->email}}</td>
                                                        <td>{{$alladmissions->gender}}</td>
                                                        <td>{{$alladmissions->DOB}}</td>
                                                        <td>
                                                            @foreach($sub as $v)
                                                                {{str_replace('-',' ',$v['subjectname']).','}}
                                                            @endforeach
                                                        </td>
                                                        <td>{{$alladmissions->classdisplay}}</td>
                                                        <td>{{$alladmissions->father_name}}/
                                                        {{$alladmissions->father_contact_number}}</td>
                                                        <td>{{$alladmissions->mother_name}}/
                                                        {{$alladmissions->mother_contact_number}}</td>
                                                        <td>{{$alladmissions->home_tel}}</td>
                                                        <td>{{$alladmissions->cname}}</td>
                                                        <td>{{$alladmissions->statename}}</td>
                                                        <td>{{$alladmissions->cityname}}</td>
                                                        <td>{{substr($alladmissions->address,0,20)}}</td>
                                                        <td>{{$alladmissions->pincode}}</td>
                                                        <td>{{$alladmissions->payment_display_name}}</td>
                                                    </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Admission</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                    </div>
                </div>
            </div>
@endsection