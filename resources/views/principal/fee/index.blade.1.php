@extends('layouts.principal.newapp')

@section('content')
<div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <div class="theme-panel hidden-xs hidden-sm">
                            <div class="toggler"> </div>
                            <div class="toggler-close"> </div>
                            <div class="theme-options">
                                <div class="theme-option theme-colors clearfix">
                                    <span> THEME COLOR </span>
                                    <ul>
                                        <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"> </li>
                                        <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"> </li>
                                        <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"> </li>
                                        <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"> </li>
                                        <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"> </li>
                                        <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"> </li>
                                    </ul>
                                </div>
                                <div class="theme-option">
                                    <span> Theme Style </span>
                                    <select class="layout-style-option form-control input-sm">
                                        <option value="square" selected="selected">Square corners</option>
                                        <option value="rounded">Rounded corners</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Layout </span>
                                    <select class="layout-option form-control input-sm">
                                        <option value="fluid" selected="selected">Fluid</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Header </span>
                                    <select class="page-header-option form-control input-sm">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Top Menu Dropdown</span>
                                    <select class="page-header-top-dropdown-style-option form-control input-sm">
                                        <option value="light" selected="selected">Light</option>
                                        <option value="dark">Dark</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Mode</span>
                                    <select class="sidebar-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Menu </span>
                                    <select class="sidebar-menu-option form-control input-sm">
                                        <option value="accordion" selected="selected">Accordion</option>
                                        <option value="hover">Hover</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Style </span>
                                    <select class="sidebar-style-option form-control input-sm">
                                        <option value="default" selected="selected">Default</option>
                                        <option value="light">Light</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Position </span>
                                    <select class="sidebar-pos-option form-control input-sm">
                                        <option value="left" selected="selected">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Footer </span>
                                    <select class="page-footer-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Account</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">
                                                <i class="icon-bell"></i> Action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-shield"></i> Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-user"></i> Something else here</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-bag"></i> Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="form-horizontal" action="{{url('/feestore')}}" id="submit_form" method="POST" enctype="multipart/form-data">
                                            <div class="form-wizard">
                                                <div class="form-body">
                                                    <ul class="nav nav-pills nav-justified steps">
                                                        <li>
                                                            <a href="#tab1" data-toggle="tab" class="step">
                                                                <span class="number"> 1 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Account Registration </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab2" data-toggle="tab" class="step">
                                                                <span class="number"> 2 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Profile Setup </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab3" data-toggle="tab" class="step active">
                                                                <span class="number"> 3 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Personal Detail </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab4" data-toggle="tab" class="step">
                                                                <span class="number"> 4 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Billing Setup </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab5" data-toggle="tab" class="step">
                                                                <span class="number"> 5 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Confirm </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                                        <div class="progress-bar progress-bar-success"> </div>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="alert alert-danger display-none">
                                                            <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                                        <div class="alert alert-success display-none">
                                                            <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                                        <div class="tab-pane active" id="tab1">
                                                            <h3 class="block">Provide your account details</h3>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="fname" value="{{ old('fname')}}" required/>
                                                                    @if ($errors->has('fname'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('fname') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your First Name </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Middle Name
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="mname" value="{{ old('mname') }}"/>
                                                                    @if ($errors->has('fname'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('mname') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your Middle Name. </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="lname" value="{{ old('lname') }}" required/>
                                                                    @if ($errors->has('lname'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('lname') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your Last name </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Username
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="username" value="{{ old('username') }}" required/>
                                                                    @if ($errors->has('username'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('username') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your username </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                             <div class="form-group form-md-radios">
                                                                <label class="control-label col-md-3">Gender</label>
                                                                <div class="col-md-4 md-radio-inline">
                                                                    <div class="md-radio">
                                                                        <input type="radio" id="radio6" name="gender" value="M" class="md-radiobtn" data-title="Male">
                                                                        <label for="radio6">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span> Male </label>
                                                                    </div>
                                                                    <div class="md-radio">
                                                                        <input type="radio" id="radio7" name="gender" value="F" class="md-radiobtn" data-title="Female">
                                                                        <label for="radio7">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span> Female </label>
                                                                    </div>
                                                                    @if ($errors->has('gender'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('gender') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your gender </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Class</label>
                                                                <div class="col-md-4">
                                                                    <select name="class" id="class" class="form-control select2">
                                                                        <option value="">--Select Class--</option>
                                                                        @foreach($myclass as $classes)
                                                                        <option value="{{$classes->id}}" data-id="{{$classes->id}}">{{$classes->class_display_name}}</option>
                                                                        <?php $classid = $classes->id;?>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('class'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('class') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your Class </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group" id="section" style="display: none">
                                                                <label class="control-label col-md-3">Section</label>
                                                                <div class="col-md-4">
                                                                    <select name="section" id="class" class="form-control select2">
                                                                        <option value="">--Select Section--</option>
                                                                        @foreach($section as $sections)
                                                                        <option value="{{$sections->id}}">{{$sections->section_display_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                     @if ($errors->has('section'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('section') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your Section </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="stream" style="display: none">
                                                                <label class="control-label col-md-3">Stream</label>
                                                                <div class="col-md-4">
                                                                    <select name="stream" id="class" class="form-control select2">
                                                                        <option>--Select Stream--</option>
                                                                        @foreach($stream as $streams)
                                                                        <option value="{{$streams->id}}">{{$streams->highsubname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('stream'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('stream') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your Stream </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="subject" style="display: none">
                                                                <label class="control-label col-md-3">Subject</label>
                                                                <div class="col-md-4">
                                                                    <select name="subject[]" id="class" class="form-control select2-multiple select2-hidden-accessible" multiple>
                                                                        <option>--Select Subject--</option>
                                                                        @foreach($subject as $subjects)
                                                                        <option value="{{$subjects->id}}">{{$subjects->subjectname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('subject'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('subject') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your subject </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-md-checkboxes" >
                                                                <label class="control-label col-md-3">Charges</label>
                                                                <div class="col-md-9 md-checkbox-inline">
                                                                    <div class="col-md-3 md-checkbox charges">
                                                                        <input type="checkbox" id="checkbox6" class="md-check dressc checkboxamount" show="amount1" name="dress_charge" value="1" data-title="Dress Charge apply.">
                                                                        <label for="checkbox6">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span> Dress Charge
                                                                                <div class="dvPassport amount1">
                                                                                    <div class="input-group admscharges ">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-inr"></i>
                                                                                        </span>
                                                                                        <input type="text" class="form-control" id="txtdresscharge"   name="dresscharge" value="3000" readonly/>
                                                                                    </div>
                                                                                </div>
                                                                            </label>
                                                                    </div>
                                                                    <div class="col-md-3 md-checkbox charges" >
                                                                        <input type="checkbox" id="checkbox7" class="md-check transc checkboxamount" show="amount2" class="md-check" name="dress_charge" onclick="ShowTransportCharge(this)" value="1" data-title="Dress Charge apply.">
                                                                        <label for="checkbox7">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span> Transport Charge
                                                                                 <div class="transcharge amount2">
                                                                                    <div class="input-group admscharges">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-inr"></i>
                                                                                        </span>
                                                                                        <input type="text" class="form-control" id="txttransportcharge" name="transportcharge" value="5000" readonly />
                                                                                    </div>
                                                                                </div>
                                                                            </label>
                                                                    </div>
                                                                    <div class="col-md-3 md-checkbox charges">
                                                                        <input type="checkbox" id="checkbox8" class="md-check oth" name="other_charge" value="1" data-title="Other Charge apply." onclick="ShowOtherCharge(this)">
                                                                        <label for="checkbox8">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span> Other Charge
                                                                                <div class="ocharge">
                                                                                    <div class="input-group admscharges">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-inr"></i>
                                                                                        </span>
                                                                                        <input type="text" class="form-control" id="txtothercharge" name="othercharge" onkeyup="mainfee()"/>
                                                                                    </div>
                                                                                    <div class="input-group admscharges">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-envelope"></i>
                                                                                        </span>
                                                                                        <input type="text" class="form-control" id="txtothercharge" name="remark" placeholder="Remark"/>
                                                                                    </div>
                                                                                </div>
                                                                            </label>
                                                                    </div>
                                                                    <div class="col-md-3 md-checkbox charges">
                                                                        <input type="checkbox" id="checkbox9" class="md-check discountamount" name="discount" value="1" data-title="Discount Charge apply." id="discount" onclick="ShowDiscountCharge(this)">
                                                                        <label for="checkbox9">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span> Discount Charge
                                                                                <div class="discountamounts">
                                                                                    <div class="input-group admscharges">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-inr"></i>
                                                                                        </span>
                                                                                        <input type="text" class="form-control" id="txtdiscountcharge" name="discountcharge" onkeyup="mainfee()"/>
                                                                                    </div>
                                                                                </div>
                                                                            </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="book" style="display: none">
                                                                <label class="control-label col-md-3">Book Charge
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" id="book_charge" name="book_charge" value="" onkeyup="mainfee()"/>
                                                                    <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                                    @if ($errors->has('book_charge'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('book_charge') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your Book Charge </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="addsn" style="display: none">
                                                                <label class="control-label col-md-3">Admission Fee</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control add" id="adfee" name="admsnfee" value="10000" readonly/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="schfee" style="display: none">
                                                                <label class="control-label col-md-3">School Fee</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control add" id="sfee" name="schfee" value="20000" readonly/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="dontaion" style="display: none">
                                                                <label class="control-label col-md-3">Donation</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" id="donate" name="dontaion" value="" onkeyup="mainfee()"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="subtotal" style="display: none">
                                                                <label class="control-label col-md-3">Subtotal Amount
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="subtotal" id="subtotalamount" value=""/>
                                                                    <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab2">
                                                            <h3 class="block">Provide your School Detail</h3>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last School
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="last_school" required/>
                                                                    @if ($errors->has('last_school'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('last_school') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your last school </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Upload School Leaving Certificate
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="file" class="form-control" name="leavecertificate" required/>
                                                                    @if ($errors->has('leavecertificate'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('leavecertificate') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your school leavecertificate </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Upload Last Class Certificate
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="file" class="form-control" name="lastclass" required/>
                                                                    @if ($errors->has('lastclass'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('lastclass') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your school lastclass </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Upload Aadhar Card
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="file" class="form-control" name="aadharfirst" required/>
                                                                    <input type="file" class="form-control" name="aadharsecond" required/>
                                                                    @if ($errors->has('aadharfirst') || $errors->has('aadharsecond'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('aadharfirst') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your aadhar card detail</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab3">
                                                            <h3 class="block">Provide your Personal details</h3>
                                                            
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Father Name
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="father_name" />
                                                                    @if ($errors->has('father_name'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('father_name') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your father Name</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Mother Name
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="mother_name" />
                                                                     @if ($errors->has('mother_name'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('mother_name') }}</strong>
                                                                        </span>
                                                                    @else
                                                                    <span class="help-block"> Provide your Mother Name</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Date of Birth
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                   <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="dob" value="" />
                                                                   <span class="help-block"> Provide your date of birth </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Father Contact Number
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="father_contact" />
                                                                    <span class="help-block"> Provide your father contact number </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Mother Contact Number
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="mother_contact" />
                                                                    <span class="help-block"> Provide your contact number </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Home Tel
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="home_tel" />
                                                                    <span class="help-block"> Provide your home telephone </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="email" class="form-control" name="email" />
                                                                    <span class="help-block"> Provide your email </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Country</label>
                                                                <div class="col-md-4">
                                                                    <select name="country" id="state_list" class="form-control select2">
                                                                        @foreach($country as $countries)
                                                                        <option value="{{$countries->id}}">{{$countries->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State</label>
                                                                <div class="col-md-4">
                                                                    <select name="state" id="state_list" class="form-control select2">
                                                                        @foreach($state as $states)
                                                                        <option value="{{$states->id}}">{{$states->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City</label>
                                                                <div class="col-md-4">
                                                                    <select name="city" id="state_list" class="form-control select2">
                                                                        @foreach($city as $cities)
                                                                        <option value="{{$cities->id}}">{{$cities->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Address</label>
                                                                <div class="col-md-4">
                                                                    <textarea class="form-control" rows="3" name="address"></textarea>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Pin
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="pin" />
                                                                    <span class="help-block"> </span>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="tab-pane" id="tab4">
                                                            <h3 class="block">Confirm your account</h3>
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                  <div class="portlet-body">
                                                                    <div class="tabbable-custom nav-justified">
                                                                        <ul class="nav nav-tabs nav-justified">
                                                                            @foreach($payment as $payments)
                                                                            @if($payments->status == '1')
                                                                            <li class="<?php if($payments->id == 1){echo 'active';}?>">
                                                                                <a href="#tab_1_1_{{$payments->id}}" data-toggle="tab">{{$payments->payment_display_name}}</a>
                                                                                <input type="hidden" name="paytype" value="{{$payments->id}}">
                                                                                <input type="hidden" name="payname" value="{{$payments->payment_name}}">
                                                                            </li>
                                                                            @endif
                                                                            @endforeach
                                                                            <!--<li>-->
                                                                            <!--    <a href="#tab_1_1_2" data-toggle="tab">Card</a>-->
                                                                            <!--    <input type="hidden" name="card" value="card">-->
                                                                            <!--</li>-->
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                </div>
                                                            </div>
                                                            <div class="tab-content">
                                                                <div class="tab-pane active" id="tab_1_1_1">
                                                                       <div class="form-group">
                                                                        
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="first" data-value="2000">2000</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" name="first" class="form-control" id="afirst" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="second" data-value="1000">1000</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" name="second" class="form-control" id="asecond" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="three" data-value="500">500</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" name="three" class="form-control" id="athree" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="four" data-value="200">200</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="four" id="afour" value=""  onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="five" data-value="100">100</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="five" id="afive" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="six" data-value="50">50</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="six" id="asix" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="seven" data-value="20">20</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="seven" id="aseven" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="eight" data-value="10">10</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="eight" id="aeight" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="nine" data-value="5">5</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="nine" id="anine" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="ten" data-value="2">2</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="ten" id="aten" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                                <span id="eleven" data-value="1">1</span> 
                                                                                <span>X</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="eleven" id="aeleven" value="" onkeyup="caltotal()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-md-2 col-xs-4 set-align">
                                                                            <p>
                                                                               <label>Subtotal Amount</label>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-8">
                                                                            <div class="input-group admschargesnew">
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-money"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control" name="finaltotal" id="finaltotal" value="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        
                                                                       
                                                                        
                                                                      <!--  <div class="col-md-4">
                                                                            <span id="first" data-value="2000">2000</span>  x  <input type="text" name="first" id="afirst" value="" onkeyup="caltotal()"><br>
                                                                            <span id="second" data-value="500">500</span>    x  <input type="text" name="second" id="asecond" value="" onkeyup="caltotal()"><br>
                                                                            <span id="three" data-value="200">200</span>     x  <input type="text" name="three" id="athree" value=""  onkeyup="caltotal()"><br>
                                                                            <span id="four" data-value="100">100</span>     x  <input type="text" name="four" id="afour" value="" onkeyup="caltotal()"><br>
                                                                            <span id="five" data-value="50">50</span>        x  <input type="text" name="five" id="afive" value="" onkeyup="caltotal()"><br>
                                                                            <span id="six" data-value="20">20</span>      x  <input type="text" name="six" id="asix" value="" onkeyup="caltotal()"><br>
                                                                            <span id="seven" data-value="10">10</span>      x  <input type="text" name="seven" id="aseven" value="" onkeyup="caltotal()"><br>
                                                                            <span id="eight" data-value="5">5</span>         x  <input type="text" name="eight" id="aeight" value="" onkeyup="caltotal()"><br>
                                                                            <span id="nine" data-value="2">2</span>          x  <input type="text" name="nine" id="anine" value="" onkeyup="caltotal()"><br>
                                                                            <span id="ten" data-value="1">1</span>       x  <input type="text" name="ten" id="aten" value="" onkeyup="caltotal()"><br>
                                                                            <label>Subtotal Amount</label><input type="text" name="finaltotal" id="finaltotal" value="">
                                                                        </div>-->
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="tab_1_1_2">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Card Type</label>
                                                                        <div class="col-md-4">
                                                                            <select name="card_type" id="card_type" class="form-control select2">
                                                                                <option>--Select Card Type--</option>
                                                                                <option value="master">Mater</option>
                                                                                <option value="visa">Visa</option>
                                                                                <option value="rupay">Rupay</option>
                                                                                <option value="dinner-club">Dinner Club</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Card Holder Name
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control" name="card_name" />
                                                                            <span class="help-block"> </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Card Number
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control" name="card_number" />
                                                                            <span class="help-block"> </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Expiration(MM/YYYY)
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" placeholder="MM/YYYY" maxlength="7" class="form-control" name="card_expiry_date" />
                                                                            <span class="help-block"> e.g 11/2020 </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Approval Code
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control" name="card_approval_code" />
                                                                            <span class="help-block"> </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" id="subtotal" style="display: none">
                                                                        <label class="control-label col-md-3">Final Subtotal Amount
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control" name="finalsubtotal" id="finalsubtotalamount" value=""/>
                                                                            <!--<span class="help-block"> Provide your Book Charge </span>-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab5">
                                                            <h3 class="block">Confirm your account</h3>
                                                            <h4 class="form-section">Account</h4>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="fname"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Middle Name:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="mname"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="lname"> </p>
                                                                </div>
                                                            </div>
                                                          <div class="form-group">
                                                                <label class="control-label col-md-3">Username:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="username"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Gender:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="gender"> </p>
                                                                </div>
                                                            </div>
                                                             <div class="form-group">
                                                                <label class="control-label col-md-3">Class:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="class"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Section:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="section"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Stream:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="stream"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Dresscharge:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="dresscharge"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Transportcharge:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="transportcharge"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Othercharge:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="othercharge"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Remark:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="remark"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Discountcharge:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="discountcharge"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Book Charge:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="book_charge"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Admission Fee:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="admsnfee"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">School Fee:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="schfee"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Donation:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="dontaion"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Subtotal:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="subtotal"> </p>
                                                                </div>
                                                            </div>
                                                            <h4 class="form-section">Student Detail</h4>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Lastschool:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="last_school"> </p>
                                                                </div>
                                                            </div>
                                                            <h4 class="form-section">Personal Detail</h4>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Father Name:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="father_name"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Father Contact:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="father_contact"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Mother Name:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="mother_name"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Mother Contact:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="mother_contact"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Date of Birth:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="dob"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="email"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="home_tel"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Country:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="country"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="state"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="city"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Address:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="address"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Pin:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="pin"> </p>
                                                                </div>
                                                            </div>
                                                            <h4 class="form-section">Billing</h4>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Payment Method:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="payname"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">2000:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="first"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">1000:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="second"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">500:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="three"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">200:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="four"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">100:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="five"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">50:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="six"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">20:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="seven"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">10:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="eight"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">5:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="nine"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">2:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="ten"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">1:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="eleven"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Subtotal:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="finaltotal"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Card Typee:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="card_type"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Card Holder Name:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="card_name"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Card Number:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="card_number"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Card Expiration:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="card_expiry_date"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Card Approval Code:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="card_approval_code"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Final Subtotal:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="finalsubtotal"> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <a href="javascript:;" class="btn default button-previous">
                                                                <i class="fa fa-angle-left"></i> Back </a>
                                                            <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                                <i class="fa fa-angle-right"></i>
                                                            </a>
                                                            <button type="submit" class="btn green button-submit"> Submit
                                                                <i class="fa fa-check"></i>
                                                            </buttom>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
@endsection