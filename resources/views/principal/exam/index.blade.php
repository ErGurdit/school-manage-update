@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <button type="button" class="btn blue btn-block" data-toggle="modal" data-target="#myholiday">Add Exams</button>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Exams</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th>Exam Name</th>
                                                        <th>Student name</th>
                                                        <th>Username</th>
                                                        <th>class</th>
                                                        <th>class incharge</th>
                                                        <th>Total Marks</th>
                                                        <th>Obtain Marks</th>
                                                        <th>Percentage</th>
                                                        <th>Position</th>
                                                        <th>Result</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($exam as $exams)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$exams->ename}}</td>
                                                        <td>{{$exams->uname}}</td>
                                                        <td>{{$exams->username}}</td>
                                                        <td>{{$exams->class_name}}</td>
                                                        <td></td>
                                                        <td>{{$exams->total_marks}}</td>
                                                        <td>{{$exams->obtain_marks}}</td>
                                                        <td>{{$exams->percentage}}%</td>
                                                        <td>{{$exams->position}}</td>
                                                        <td>{{$exams->result}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon-only blue" title="class" data-toggle="modal" data-target="#myexamdetail-{{$exams->eid}}">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <div class="modal fade" id="myexamdetail-{{$exams->eid}}" role="dialog">
                                                            <div class="modal-dialog">
                                
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">{{$exams->ename}}</h4>
                                                                    </div>
                                                                <div class="modal-body">
                                                                    <div class="portlet light bg-inverse">
                                                                            <div class="portlet-title">
                                                                                <div class="tools">
                                                                                    <a href="" class="collapse"> </a>
                                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                                    <a href="" class="reload"> </a>
                                                                                    <a href="" class="remove"> </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="portlet-body form">
                                                                                <!-- BEGIN FORM-->
                                                                                <form action="{{url('holiday/update',$exams->eid)}}" class="horizontal-form" method="post">
                                                                                    {{ csrf_field() }}
                                                                                    {{method_field('PATCH')}}
                                                                                    <div class="form-body">
                                                                                        <div class="row">
                                                                                             <div class="form-group col-md-6">
                                                                                              <label for="top-content">Exam Name</label><br>
                                                                    
                                                                                                <input type="text" name="ename" id="name" class="form-control"  
                                                                                                    data-msg-required="The Exam Name field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->ename}}" readonly>
                                                                                                    @if ($errors->has('ename'))<p class="help-block">{{ $errors->first('ename')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Student Name</label><br>
                                                                    
                                                                                                <input type="text" name="sname" id="name" class="form-control"  
                                                                                                    data-msg-required="The Student Name field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->uname}}" readonly>
                                                                                                    @if ($errors->has('sname'))<p class="help-block">{{ $errors->first('sname')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Class Name</label><br>
                                                                    
                                                                                                <input type="text" name="cname" id="cname" class="form-control"  
                                                                                                    data-msg-required="The Class Name field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->class_name}}" readonly>
                                                                                                    @if ($errors->has('cname'))<p class="help-block">{{ $errors->first('cname')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Total Marks</label><br>
                                                                    
                                                                                                <input type="text" name="tomarks" id="cname" class="form-control"  
                                                                                                    data-msg-required="The total Marks field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->total_marks}}" readonly>
                                                                                                    @if ($errors->has('tomarks'))<p class="help-block">{{ $errors->first('tomarks')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Obtain Marks</label><br>
                                                                    
                                                                                                <input type="text" name="obtainmarks" id="obtainmarks" class="form-control"  
                                                                                                    data-msg-required="The Obtain Marks field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->obtain_marks}}" readonly>
                                                                                                    @if ($errors->has('obtainmarks'))<p class="help-block">{{ $errors->first('obtainmarks')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Percentage</label><br>
                                                                    
                                                                                                <input type="text" name="percentage" id="percentage" class="form-control"  
                                                                                                    data-msg-required="The Percentage field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->percentage}}" readonly>
                                                                                                    @if ($errors->has('percentage'))<p class="help-block">{{ $errors->first('percentage')}}</p>@endif
                                                                                            </div>
                                                                                             <div class="form-group col-md-6">
                                                                                              <label for="top-content">Position</label><br>
                                                                    
                                                                                                <input type="text" name="position" id="position" class="form-control"  
                                                                                                    data-msg-required="The Position field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->position}}" readonly>
                                                                                                    @if ($errors->has('position'))<p class="help-block">{{ $errors->first('position')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Result</label><br>
                                                                    
                                                                                                <input type="text" name="result" id="result" class="form-control"  
                                                                                                    data-msg-required="The result field is required." 
                                                                                                     data-rule-required="true" value="{{$exams->result}}" readonly>
                                                                                                    @if ($errors->has('result'))<p class="help-block">{{ $errors->first('result')}}</p>@endif
                                                                                            </div>
                                                                                        </div>
                                                                                      </div> 
                                                                                      <div class="modal-footer">
                                                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                </form>
                                                                                <!-- END FORM-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                    </div>
                                                    </td>
                                                    </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Exam</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <!-- Modal -->
                              <div class="modal fade" id="myholiday" role="dialog">
                                <div class="modal-dialog">
    
                                <!-- Modal content-->
                                <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Holiday</h4>
                                        </div>
                                    <div class="modal-body">
                                        <div class="portlet light bg-inverse">
                                                <div class="portlet-title">
                                                    <div class="tools">
                                                        <a href="" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="" class="reload"> </a>
                                                        <a href="" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="{{url('holiday/store')}}" class="horizontal-form" method="post">
                                                        {{ csrf_field() }}
                                                        <div class="form-body">
                                                                                        <div class="row">
                                                                                             <div class="form-group col-md-6">
                                                                                              <label for="top-content">Exam Name</label><br>
                                                                    
                                                                                                <input type="text" name="ename" id="name" class="form-control"  
                                                                                                    data-msg-required="The Exam Name field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('ename'))<p class="help-block">{{ $errors->first('ename')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Student Name</label><br>
                                                                    
                                                                                                <input type="text" name="sname" id="name" class="form-control"  
                                                                                                    data-msg-required="The Student Name field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('sname'))<p class="help-block">{{ $errors->first('sname')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Class Name</label><br>
                                                                    
                                                                                                <input type="text" name="cname" id="cname" class="form-control"  
                                                                                                    data-msg-required="The Class Name field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('cname'))<p class="help-block">{{ $errors->first('cname')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Total Marks</label><br>
                                                                    
                                                                                                <input type="text" name="tomarks" id="cname" class="form-control"  
                                                                                                    data-msg-required="The total Marks field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('tomarks'))<p class="help-block">{{ $errors->first('tomarks')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Obtain Marks</label><br>
                                                                    
                                                                                                <input type="text" name="obtainmarks" id="obtainmarks" class="form-control"  
                                                                                                    data-msg-required="The Obtain Marks field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('obtainmarks'))<p class="help-block">{{ $errors->first('obtainmarks')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Percentage</label><br>
                                                                    
                                                                                                <input type="text" name="percentage" id="percentage" class="form-control"  
                                                                                                    data-msg-required="The Percentage field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('percentage'))<p class="help-block">{{ $errors->first('percentage')}}</p>@endif
                                                                                            </div>
                                                                                             <div class="form-group col-md-6">
                                                                                              <label for="top-content">Position</label><br>
                                                                    
                                                                                                <input type="text" name="position" id="position" class="form-control"  
                                                                                                    data-msg-required="The Position field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('position'))<p class="help-block">{{ $errors->first('position')}}</p>@endif
                                                                                            </div>
                                                                                            <div class="form-group col-md-6">
                                                                                              <label for="top-content">Result</label><br>
                                                                    
                                                                                                <input type="text" name="result" id="result" class="form-control"  
                                                                                                    data-msg-required="The result field is required." 
                                                                                                     data-rule-required="true" >
                                                                                                    @if ($errors->has('result'))<p class="help-block">{{ $errors->first('result')}}</p>@endif
                                                                                            </div>
                                                                                        </div>
                                                                                      </div>  
                                                          <div class="modal-footer">
                                                              <input type="submit" class="btn btn-info">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection