@extends('layouts.principal.app')

@section('content')
 <!-- BEGIN CONTENT -->
 
                <div class="page-content-wrapper">
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="row">
                            
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <button type="button" class="btn blue btn-block" data-toggle="modal" data-target="#myholiday">Add Holidays</button>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Holidays</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>
                                                        <th>Description</th>
                                                        <th>Update</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1;?>
                                                    @forelse($holiday as $holidays)
                                                   
                                                    <tr id="class">
                                                        <td>{{$i++}}</td>
                                                        <td>{{$holidays->startdate}}</td>
                                                        <td>{{$holidays->enddate}}</td>
                                                        <td>{{$holidays->description}}</td>
                                                        <td>{{$holidays->updated_at}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon-only blue" title="class" data-toggle="modal" data-target="#myeditholiday-{{$holidays->id}}">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button title="holiday" class="btn btn-icon-only red btn-delete" id="holiday" data-id="{{$holidays->id}}">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                            <div class="modal fade" id="myeditholiday-{{$holidays->id}}" role="dialog">
                                                            <div class="modal-dialog">
                                
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">{{$holidays->date}}</h4>
                                                                    </div>
                                                                <div class="modal-body">
                                                                    <div class="portlet light bg-inverse">
                                                                            <div class="portlet-title">
                                                                                <div class="tools">
                                                                                    <a href="" class="collapse"> </a>
                                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                                    <a href="" class="reload"> </a>
                                                                                    <a href="" class="remove"> </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="portlet-body form">
                                                                                <!-- BEGIN FORM-->
                                                                                <form action="{{url('holiday/update',$holidays->id)}}" class="horizontal-form" method="post">
                                                                                    {{ csrf_field() }}
                                                                                    {{method_field('PATCH')}}
                                                                                    <div class="form-body">
                                                                                        <div class="row">
                                                                                            <div class="form-group col-md-4">
                                                                                                <label for="exampleDateofJoining">Holiday Date</label>
                                                                                                <input type="hidden" name="schoolid" value="{{$school->id}}">
                                                                                                <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                                                                    <input type="text" class="form-control" placeholder="Start Holidays" name="from" value="{{$holidays->startdate}}">
                                                                                                    <span class="input-group-addon"> to </span>
                                                                                                    <input type="text" class="form-control" placeholder="End Holidays" name="to" value="{{$holidays->enddate}}"> 
                                                                                                </div>
                                                                                            </div>
                                                                                             <div class="form-group">
                                                                                                <div class="col-md-12">
                                                                                                    <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                                                                     data-rule-required="true" required>{{$holidays->description}}</textarea>
                                                                                                        <div id="editor_error"> </div>
                                                                                                    @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                      </div> 
                                                                                      <div class="modal-footer">
                                                                                          <input type="submit" class="btn btn-info" value="Update">
                                                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                </form>
                                                                                <!-- END FORM-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                    </div>
                                                    </td>
                                                    </tr>
                                                    
                                                    @empty
                                                        <tr>
                                                            <td>No Holidays</td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <!-- Modal -->
                              <div class="modal fade" id="myholiday" role="dialog">
                                <div class="modal-dialog">
    
                                <!-- Modal content-->
                                <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Holiday</h4>
                                        </div>
                                    <div class="modal-body">
                                        <div class="portlet light bg-inverse">
                                                <div class="portlet-title">
                                                    <div class="tools">
                                                        <a href="" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="" class="reload"> </a>
                                                        <a href="" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="{{url('holiday/store')}}" class="horizontal-form" method="post">
                                                        {{ csrf_field() }}
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleDateofJoining">Holiday Date</label>
                                                                    <input type="hidden" name="schoolid" value="{{$school->id}}">
                                                                        <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                                            <input type="text" class="form-control" name="from">
                                                                            <span class="input-group-addon"> to </span>
                                                                            <input type="text" class="form-control" name="to"> 
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <textarea name="pagedataset" data-provide="markdown" rows="10" data-error-container="#editor_error" autofocus data-msg-required="The text announce field is required." 
                                                                         data-rule-required="true" required></textarea>
                                                                        <div id="editor_error"> </div>
                                                                        @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                          <div class="modal-footer">
                                                              <input type="submit" class="btn btn-info">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection