@extends('layouts.admin.app')
@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
<div id="wrap" class="animsition">
    <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
    <section id="content">
        <div class="page page-tables-datatables">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-12">
                    <!--<a href="{{route('school.create')}}" class="btn btn-primary">Add School</a>-->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add School</button>
                       <!-- Modal -->
            
                        <section class="tile">


                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="table-responsive">
                                   <input type="hidden" name="usertoken" id="usertoken" value="{{csrf_token()}}">
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>School Name</th>
                                              <th>Principal Name</th>
                                              <th>Email</th>
                                              <th>Phone number</th>
                                              <th>Mobile number</th>
                                              <th>State</th>
                                              <th>City</th>
                                              <th>Pincode</th>
                                              <th>Address</th>
                                              <th>Trial Period</th>
                                              <th>Payment Status</th>
                                              <th>School Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i=1; ?>
                                              @foreach($school as $schools)
                                             
                                            <tr>
                                              <td>{{$i++}}</td>
                                              <td class="tabledit-view-mode" id="editname-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->name}}</span><input class="tabledit-input form-control input-sm" type="text" id="editname" name="editname" value="{{$schools->name}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editprincipal_name-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->principal_name}}</span><input class="tabledit-input form-control input-sm" id="editprincipal_name" type="text" name="editprincipal_name" value="{{$schools->principal_name}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editemail-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->email}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editemail" name="editemail" value="{{$schools->email}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editphone-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->landline}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editphone"name="editphone" value="{{$schools->landline}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editmobile-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->mobile}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editmobile"name="editmobile" value="{{$schools->mobile}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editstatename-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->statename}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editstatename" name="editstatename" value="{{$schools->statename}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editcityname-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->cityname}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editcityname" name="editcityname" value="{{$schools->cityname}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editpincode-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->pincode}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editpincode" name="editpincode" value="{{$schools->pincode}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editaddress-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->address}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editaddress" name="editaddress" value="{{$schools->address}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editdaterange-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->timeperiod}}</span><input class="tabledit-input" type="text"  id="daterange" name="daterange" value="{{$schools->timeperiod}}" style="display: none;" disabled=""></td>
                                              <td class="tabledit-view-mode" id="editpayment-{{$schools->id}}"><span class="tabledit-span" style="display: inline;">{{$schools->payment_method}}</span><input class="tabledit-input form-control input-sm" type="text"  id="editpayment" name="editpayment" value="{{$schools->payment_method}}" style="display: none;" disabled=""></td>
                                              <td id="editstatus-{{$schools->id}}">@if($schools->status == 0)<input type='checkbox' class="editstatus" name='checkbox2' rel="{{$schools->status}}" onchange="changestatus({{$schools->id}},this)"/>@else<input type='checkbox' class="editstatus" checked  name='checkbox2'  rel="{{$schools->status}}" onchange="changestatus({{$schools->id}},this)"/>@endif</td>
                                            </tr>
                                                      @endforeach
                                              </tbody>
                                            </table>
                                    </div>
                                </div>
                                <!-- /tile body -->

                            </section>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ CONTENT -->
</div>
 <!--/ Application Content -->
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalLabel">School Add</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                         </div>
                          <div class="modal-body">
                              <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{route('school.store')}}" method="post" role="form" id="school-add">
                                        
                                        
                                        <!--School Name-->
                                        <div class="form-group col-md-6">
                                        <label for="name">School Name: </label>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" name="name" id="name" class="form-control"  
                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Name field is required." 
                                data-rule-date="true" data-rule-required="true"  required>
                                                @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif
                                            </div>
                                            <!--Principal Name-->
                                            <div class="form-group col-md-6">
                                                <label for="name">Principal Name: </label>
                                                <input type="text" name="principalname" id="principalname" class="form-control" 
                                                data-msg-date="The Principal Name field is required. must be a date." data-msg-required="The Principal Name field is required." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('principalname'))<p class="help-block">{{ $errors->first('principalname')}}</p>@endif
                                            </div>
                                            <!--Logo-->
                                            <!--<div class="form-group col-md-4">-->
                                            <!--    <label for="name">Logo: </label>-->
                                            <!--    <input type="file" name="filename" id="filename" class="form-control" required>-->
                                            <!--    @if ($errors->has('filename'))<p class="help-block">{{ $errors->first('filename')}}</p>@endif-->
                                                
                                            <!--</div>-->
                                              <!--Email-->
                                            <div class="form-group col-md-4">
                                                <label for="email">Email: </label>
                                                <input type="email" name="email" id="email" class="form-control"
                                                data-msg-date="The Email field is required. must be a date."
                                                data-msg-required="The Email field is required." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                            </div>
                                            <!--Set School Time-->
                                            <!--<div class="form-group col-md-4">-->
                                            <!--    <label>Time:</label>-->
                                            <!--    <input type="text" name="timeset" class="form-control">-->
                                            <!--    @if ($errors->has('timeset'))<p class="help-block">{{ $errors->first('timeset')}}</p>@endif-->
                                            <!--</div>-->
                                             
                                            <!--School Contact Number-->
                                            <div class="form-group col-md-4">
                                                <label>Telephone number:</label>
                                                <input type="text" name="telephone" id="telephone" class="form-control number-only"
                                                data-msg-date="The Number field is required. must be a date." data-msg-required="The Number field is required." 
                                                data-rule-date="true" data-rule-required="true" data-rule-maxlength="11" 
                                                data-rule-minlength="10">
                                                @if ($errors->has('telephone'))<p class="help-block">{{ $errors->first('telephone')}}</p>@endif
                                            </div>
                                            <!--School Mobile Number-->
                                            <div class="form-group col-md-4">
                                                <label>Mobile number:</label>
                                                <input type="text" name="mobile" id="mobile" class="form-control number-only"
                                                data-msg-date="The Mobile field is required. must be a date." data-msg-required="The Mobile field is required." 
                                                data-rule-date="true" data-rule-required="true" data-rule-maxlength="11" 
                                                data-rule-minlength="10">
                                                @if ($errors->has('mobile'))<p class="help-block">{{ $errors->first('mobile')}}</p>@endif
                                            </div>
                                             <!--Country-->
                                            <div class="form-group col-md-4">
                                                <label for="country">Country: </label>
                                                <select name="country" class="form-control custom-select" id="country"
                                                data-msg-required="The Country field is required." 
                                                data-rule-required="true" >
                                                    <option value="0">--Select--</option>
                                                    @foreach($country as $countries)
                                                    <option value="{{$countries->id}}">{{$countries->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('country'))<p class="help-block">{{ $errors->first('country')}}</p>@endif
                                            </div>
                                            <!--State-->
                                            <div class="form-group col-md-4">
                                                <label for="state">State: </label>
                                                <select name="state" class="form-control" id="state"
                                                 data-msg-required="The State field is required." 
                                                data-rule-required="true" >
                                                    
                                                        <option value="7"></option>
                                                    </select>
                                            </div>
                                            <!--City-->
                                            <div class="form-group col-md-4">
                                                <label for="city">City: </label>
                                                <select name="city" id="city" class="form-control" 
                                                 data-msg-required="The City field is required." 
                                                data-rule-required="true" >
                                                    <option value="122"></option>
                                                </select>
                                            </div>
                                            <!--Pin Code-->
                                            <div class="form-group col-md-6">
                                                <label for="pin">Pin Code: </label>
                                                <input type="text" name="pin" id="pin" class="form-control number-only">
                                                @if ($errors->has('pin'))<p class="help-block">{{ $errors->first('pin')}}</p>@endif
                                            </div>
                                            
                                             <!--Payment Method-->
                                            <div class="form-group col-md-6">
                                                <label for="pm">Payment Menthod: </label><br>
                                                <input type="radio" name="pm" id="pm" value="online">Online
                                                <input type="radio" name="pm" id="pm" value="check">Check
                                                @if ($errors->has('pm'))<p class="help-block">{{ $errors->first('pm')}}</p>@endif
                                            </div>
                                            
                                            
                                         
                                             <!--Address-->
                                            <div class="form-group col-md-12">
                                                <label for="address">Address: </label>
                                                <textarea name="address" id="address" class="form-control" 
                                                 required></textarea>
                                                @if ($errors->has('address'))<p class="help-block">{{ $errors->first('address')}}</p>@endif
                                            </div>
                                            
                                            <!--Date Range-->
                                            <div class="form-group col-md-12">
                                                <input type="checkbox" class="trialperiod" name="trialperiod">Trial Period
                                                <div class="range form-group">
                                                <label for="date">Date</label>: </label>
                                                <input type="text" name="daterange" value="01/01/2015 - 01/01/2015" />
                                                @if ($errors->has('daterange'))<p class="help-block">{{ $errors->first('daterange')}}</p>@endif
                                                </div>
                                            </div>
                                             <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#school-add').submit()">Save</button>
                                             </div>
                                        </form>
                                  </div>
                                </div>
                            </div>
                        </div>
     
    </div>
  </div>
</div>
@endsection