@extends('layouts.admin.appform')

@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
<section id="content">
<div class="page page-forms-validate">
    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-12">


<!-- tile -->
                            <section class="tile">
                                    <h2>Edit {{$school->name}}</h2>
                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                    <form name="form2" role="form" id="form2" data-parsley-validate action="{{route('school.update',$school->slug)}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{method_field('PATCH')}}
                                        
                                        <!--Name-->
                                            <div class="form-group col-md-6">
                                                <label for="name">Name: </label>
                                                <input type="text" name="name" id="name" class="form-control" value="{{$school->name}}" required>
                                            </div>
                                            <!--Principal Name-->
                                            <div class="form-group col-md-6">
                                                <label for="name">Principal Name: </label>
                                                <input type="text" name="name" id="name" class="form-control" value="{{$school->name}}" required>
                                            </div>
                                            <!--Email-->
                                            <div class="form-group col-md-4">
                                                <label for="email">Email: </label>
                                                <input type="email" name="email" id="email" class="form-control" value="{{$school->email}}" required>
                                            </div>
                                              <!--School Contact Number-->
                                            <div class="form-group col-md-4">
                                                <label>Telephone number:</label>
                                                <input type="text" name="telephone" class="form-control" value="{{$school->landline}}">
                                            </div>
                                            <!--School Mobile Number-->
                                            <div class="form-group col-md-4">
                                                <label>Mobile number:</label>
                                                <input type="text" name="mobile" class="form-control" value="{{$school->mobile}}">
                                            </div>
                                             {{-- country --}}

                                            <div class="form-group col-md-4">

                                                <label for="country">Select Country:</label>

                                                    <select name="country" id="country" class="form-control" >
                                                    <option value="0">--Select--</option>
                                                    @foreach($country as $countries)
                                                    <?php $con = '';?>

                                                    <?php if ($countries->id == $school->countryid) {

	                                                    $con = ' selected="selected"';

                                                    }

                                                    ?>

                                                    <option <?php echo $con ?> value="{{$countries->id}}">{{$countries->name}}</option>

                                                        @endforeach

                                                </select>

                                            @if ($errors->has('country'))<p class="help-block">{{ $errors->first('country')}}</p>@endif

                                        </div>
                                        <!--State-->

                                         <div class="form-group col-md-4">

                                            <label for="title">Select State:</label>

                                                <select name="state" id="state" class="form-control">
                                                    @foreach($state as $states)
                                                    <?php $state = '';?>

                                                    <?php if ($states->id == $states->stateid) {

	                                                    $state = ' selected="selected"';

                                                    }

                                                    ?>
                                                    <option <?php echo $state ?> value="{{$states->id}}">{{$states->name}}</option>

                                                        @endforeach

                                                </select>
                                        @if ($errors->has('state'))<p class="help-block">{{ $errors->first('state')}}</p>@endif
                                        </div>

                                        <!--City-->

                                        <div class="form-group col-md-4">

                                            <label for="title">Select City:</label>

                                                <select name="city" id="city" class="form-control">
                                                   
                                                    @foreach($city as $cities)
                                                    <?php $city = '';?>

                                                    <?php if ($cities->id == $cities->cityid) {

	                                                    $city = ' selected="selected"';

                                                    }

                                                    ?>
                                                    <option <?php echo $city ?> value="{{$cities->id}}">{{$cities->name}}</option>

                                                        @endforeach

                                                </select>

                                        </div>

                                             <!--Logo-->
                                            <!--<div class="form-group">-->
                                            <!--    <label for="name">Logo: </label>-->
                                            <!--    <img src="{{asset('images/'.$school->image)}}" width="100" height="100">-->
                                            <!--    <input type="file" name="filename" id="filename" class="form-control" required>-->
                                            <!--</div>-->
                                             <!--Address-->
                                            <div class="form-group col-md-12">
                                                <label for="address">Address: </label>
                                                <textarea name="address" id="address" class="form-control" required>{{$school->address}}</textarea>
                                            </div>
                                           

                                            
                                            <!--Set School Time-->
                                            <!--<div class="form-group">-->
                                            <!--    <label>Time:</label>-->
                                            <!--    <input type="text" name="timeset" class="form-control" value="{{$school->time}}">-->
                                            <!--</div>-->
                                           
                                             
                                        <div class="tile-footer text-right bg-tr-black lter dvd dvd-top col-md-12">
                                    <!-- SUBMIT BUTTON -->
                                    <button type="submit" class="btn btn-lightred" id="form2Submit">Update</button>
                                      </div>
                             </form>
                            </div>
                        </div>
                    </div>
                                <!-- /tile body -->

                        </section>
                            <!-- /tile -->
                 </div>                 
            </div>
        </div>
</section>
            <!--/ CONTENT -->
</div>
@endsection


