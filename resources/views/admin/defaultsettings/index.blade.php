@extends('layouts.admin.app2')

@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
<section id="content">
<div class="page page-forms">
    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-12">


<!-- tile -->
                            <section class="tile">
                                    <h2>Default Settings</h2>
                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                        @if(!empty($defaultsetting->userid))
                                        <form name="form2" role="form" id="default-setting" data-parsley-validate action="{{url('defaultsetting/update',$defaultsetting->userid)}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{method_field('PATCH')}}
                                        @else
                                        <form name="form2" role="form" id="default-setting" data-parsley-validate action="{{url('defaultsetting/store')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        @endif
                                      <!--Website Mobile Number-->
                                            <div class="form-group col-md-4">
                                                @if(!empty('$defaultsetting->mobile'))
                                               <label class="checkbox-inline checkbox-custom">
                                                    <input type="checkbox" id="inlineCheckbox4" class="chkmobile form-control" value="1"
                                                    name="mobstatus" required="true" checked><i></i>Mobile
                                                </label>
                                                  <input type="text" id="txtmobileNumber" class="form-control" name="mobile"  value="{{$defaultsetting->mobile}}" />
                                                  @else
                                                  <input type="text" id="txtmobileNumber" class="form-control" name="mobile" />
                                                  @endif
                                                @if ($errors->has('mobile'))<p class="help-block">{{ $errors->first('mobile')}}</p>@endif
                                            </div>
                                            <!--Website Email-->
                                            <div class="form-group col-md-4">
                                                @if(!empty('$defaultsetting->email'))
                                                <label class="checkbox-inline checkbox-custom">
                                                    <input type="checkbox"  name="empstatus" id="inlineCheckbox4" class="chkemail form-control" value="1" 
                                                     required="true" checked><i></i>Email
                                                </label><br><br>
                                                <input type="text" id="txtemail" class="form-control" name="email" value="{{$defaultsetting->email}}" />
                                                 @else
                                                  <input type="text" id="txtemail" class="form-control" name="email" />
                                                  @endif
                                                @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                            </div>
                                            <!--Facebook Link-->
                                            <div class="form-group col-md-4">
                                                @if(!empty('$defaultsetting->fb'))
                                                <label class="checkbox-inline checkbox-custom">
                                                    <input type="checkbox" name="fbstatus" id="inlineCheckbox4" class="chkfb form-control" value="1" <?php if($defaultsetting->fbstatus == 1){ echo 'checked';}?>><i></i>Facebook Link
                                                </label>
                                                <input type="text" id="txtfb" class="form-control" name="fb_link" value="{{$defaultsetting->fb}}" readonly="readonly" />
                                                 @else
                                                 <input type="text" id="txtfb" class="form-control" name="fb_link"  readonly="readonly" />
                                                 <input type="checkbox" class="form-control" name="fbstatus" value="0">
                                                  @endif
                                                @if ($errors->has('fb_link'))<p class="help-block">{{ $errors->first('fb_link')}}</p>@endif
                                            </div>
                                            <!--twitter Link-->
                                            <div class="form-group col-md-4">
                                                @if(!empty('$defaultsetting->twitter'))
                                                <label class="checkbox-inline checkbox-custom">
                                                    <input type="checkbox" id="inlineCheckbox4" name="tweetstatus" class="chktwitter form-control" value="1" <?php if($defaultsetting->tweetstatus == 1){ echo 'checked';}?>><i></i>Twiter Link
                                                </label>
                                                <input type="text" id="txttwitter" name="twitter_link" class="form-control"  value="{{$defaultsetting->twitter}}"  readonly="readonly" />
                                                @else
                                                <input type="text" id="txttwitter" class="form-control" name="twitter_link" readonly="readonly" />
                                                <input type="checkbox" class="form-control" name="tweetstatus" value="0">
                                                @endif
                                                @if ($errors->has('twitter_link'))<p class="help-block">{{ $errors->first('twitter_link')}}</p>@endif
                                            </div>
                                            <!--Intstagram Link-->
                                            <div class="form-group col-md-4">
                                                @if(!empty('$defaultsetting->insta'))
                                                <label class="checkbox-inline checkbox-custom">
                                                    <input type="checkbox" id="inlineCheckbox4" class="chkinsta form-control" value="1" <?php if($defaultsetting->instastatus	== 1){ echo 'checked';}?>><i></i>Instagram Link
                                                </label>
                                                <input type="text" id="txtinsta" class="form-control" name="instagram_link" value="{{$defaultsetting->insta}}" readonly="readonly" />
                                                @else
                                                  <input type="text" id="txtinsta" class="form-control" name="instagram_link"  readonly="readonly" />
                                                  <input type="checkbox"  class="form-control" name="instastatus" value="0">
                                                @endif
                                                  
                                                @if ($errors->has('instagram_link'))<p class="help-block">{{ $errors->first('instagram_link')}}</p>@endif
                                            </div>
                                            <!--Google Plus Link-->
                                            <div class="form-group col-md-4">
                                                @if(!empty('$defaultsetting->gp'))
                                                <label class="checkbox-inline checkbox-custom">
                                                    <input type="checkbox" id="inlineCheckbox4" class="chkgp form-control" value="1" <?php if($defaultsetting->gpstatus	== 1){ echo 'checked';}?>><i></i>Google Plus Link
                                                </label>
                                                <input type="text" id="txtgp" class="form-control" name="gp_link" value="{{$defaultsetting->gp}}" readonly="readonly" />
                                                 @else
                                                  <input type="text" id="txtgp" name="gp_link"  readonly="readonly" />
                                                  <input type="checkbox" class="form-control" name="gpstatus" value="0">
                                                @endif
                                                @if ($errors->has('gp_link'))<p class="help-block">{{ $errors->first('gp_link')}}</p>@endif
                                            </div>
                                            
                                             
                                             
                                        <div class="tile-footer text-right bg-tr-black lter dvd dvd-top col-md-12">
                                    <!-- SUBMIT BUTTON -->
                                    <button type="submit" class="btn btn-lightred" id="form2Submit" onClick="ValidateForm(this.form)">Submit</button>
                                      </div>
                             </form>
                            </div>
                        </div>
                    </div>
                                <!-- /tile body -->

                        </section>
                            <!-- /tile -->
                 </div>                 
            </div>
        </div>
</section>
            <!--/ CONTENT -->
</div>
@endsection


