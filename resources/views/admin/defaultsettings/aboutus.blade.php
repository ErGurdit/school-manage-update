@extends('layouts.admin.app2')

@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
<section id="content">
<div class="page page-forms">
    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-12">


<!-- tile -->
                            <section class="tile">
                                    <h2>Change About us</h2>
                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                    <form name="form2" role="form" id="school-add" data-parsley-validate action="{{url('defaultsetting/aboutusupdate',$defaultsetting->userid)}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{method_field('PATCH')}}
                                      <!--About us-->
                                            <div class="form-group col-md-12">
                                                <label>About us:</label>
                                                
                                                @if(!empty($defaultsetting->aboutus))
                                                <input name="image" type="file" id="upload" class="hidden" onchange="" style="display:none">
                                                <textarea name="aboutus" class="form-control" id="editor" autofocus>{{$defaultsetting->aboutus}}</textarea>
                                                @else
                                                <textarea name="aboutus" class="form-control" id="editor" autofocus></textarea>
                                                @endif
                                                @if ($errors->has('aboutus'))<p class="help-block">{{ $errors->first('aboutus')}}</p>@endif
                                            </div>
                                        <div class="tile-footer text-right bg-tr-black lter dvd dvd-top col-md-12">
                                    <!-- SUBMIT BUTTON -->
                                    <button type="submit" class="btn btn-lightred" id="form2Submit">Submit</button>
                                      </div>
                             </form>
                            </div>
                        </div>
                    </div>
                                <!-- /tile body -->

                        </section>
                            <!-- /tile -->
                 </div>                 
            </div>
        </div>
</section>
            <!--/ CONTENT -->
</div>
@endsection


