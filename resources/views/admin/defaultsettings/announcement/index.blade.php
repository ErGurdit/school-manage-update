@extends('layouts.admin.app2')
@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
<div id="wrap" class="animsition">
    <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
    <section id="content">
        <div class="page page-tables-datatables">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-12">
                    <div class="school-filter-btn">
                        <button type="button" class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10" data-toggle="modal" data-target="#addModal" >
                           <i class="fa fa-plus"></i><span> Add Announcement</span>
                         </button>
                        <a href="{{url('announcement')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('announcement')){{'activeclass'}}@endif">
                            <i class="fa fa-tasks"></i><span>All Announcement</span>
                        </a>
                        <!--<a href="{{url('school/show')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('announcement/show')){{'activeclass'}}@endif">-->
                        <!--    <i class="fa fa-star"></i><span>Active Announcement</span>-->
                        <!--</a>-->
                        <!--<a href="{{url('school/showin')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('announcement/showin')){{'activeclass'}}@endif">-->
                        <!--    <i class="fa fa-minus"></i><span>Inactive Announcement</span>-->
                        <!--</a>-->
                
                    </div>
                       <!-- Modal -->
            
                        <section class="tile">


                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Title</th>
                                              <th>Description</th>
                                              <th>Start Date</th>
                                              <th>End Date</th>
                                              <th>Days Left</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                   @foreach($announce as $announces)



                                        <tr>
                                        
                                         <td class="statusset-{{$announces->id}}">{!!$i++!!}</td>
                                         
                                         <td class="statusset-{{$announces->id}}">{{$announces->title}}</td>
                                         
                                         <td class="statusset-{{$announces->id}}">{{substr($announces->description,0,50)}}</td>

                                         <td class="statusset-{{$announces->id}}">@if(!empty($announces->startdate)){{date($announces->startdate)}}@else {{date("Y-m-d")}}@endif</td>

                                         <td class="statusset-{{$announces->id}}">{!!$announces->enddate!!}</td>
                                         <?php $startdate = strtotime($announces->startdate);
                                               $enddate = strtotime($announces->enddate);
                                               if(!empty($startdate)){
                                                 $now = $startdate;
                                               }
                                               else{
                                                 $now = time();
                                               }
                                               
                                               $datediff = $enddate-$now;
                                               
                                               $diff = round($datediff / (60 * 60 * 24));?>
                                               
                                         <td><?php echo $diff ?> days</td>

                                        <td><button type="button" class="btn btn-rounded-40 btn-ef btn-ef-2 btn-ef-2-cyan btn-ef-2a mb-10 ml-10" data-toggle="modal"  data-target="#myModal-{{$announces->id}}"><i class="fa fa-pencil"></i></button><button class="btn btn-rounded-10 btn-ef btn-ef-2 btn-ef-2-amethyst btn-ef-2a mb-10 btn-delete" title="announce"  data-id="{{$announces->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="myModal-{{$announces->id}}" role="dialog">
                                            <div class="modal-dialog">
    
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$announces->title}}</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('announce/update',$announces->id)}}" method="post" role="form" id="school-edit-{{$announces->id}}" class="school-update-{{$announces->id}}" enctype="multipart/form-data">
                                       
                                       {{method_field('PATCH')}}
                                        <!--Announcement Title-->
                                        
                                        <div class="form-group col-md-12">
                                          <label for="inputName">Title</label><br>
                
                                            <input type="text" name="title" id="title" class="form-control"  
                                                data-msg-date="The Title field is required. must be a date." data-msg-required="The title field is required." 
                                                 data-rule-date="true" data-rule-required="true" value="{{$announces->title}}">
                                                @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
                                        </div>
                                        <!--Announcement Decsription-->
                                        <div class="form-group col-md-12">
                                          <label for="inputName">Description</label><br>
                                                <textarea name="pagedataset" class="form-control" id="editor" autofocus>{{$announces->description}}</textarea>
                                                @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('pagedataset')}}</p>@endif
                                        </div>
                                         <!--startdate-->
                                            <div class='form-group input-group datepicker col-md-6' data-format="L">
                                                <input type='text' class="form-control" placeholder="startdate" name="datestart" value="{{$announces->startdate}}"/>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            
                                            <!--enddate-->
                                            
                                            <div class='form-group input-group datepicker col-md-6' data-format="L">
                                                
                                                    <input type='text' class="form-control" placeholder="enddate" name="dateend" value="{{$announces->startdate}}"/>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
            
                                         </form>
                                        <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$announces->id}}').submit()">Save changes</button>
                                        </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                        </td>

                                         </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                </div>
                                <!-- /tile body -->

                            </section>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ CONTENT -->
</div>
 <!--/ Application Content -->
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalLabel">Announcement Add</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                         </div>
                          <div class="modal-body">
                              <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{url('announce/store')}}" method="post" role="form" id="school-add" class="announce-add" enctype="multipart/form-data">
                                        
                                        
                                        <div class="form-group col-md-12">
                                          <label for="inputName">Title</label><br>
                
                                            <input type="text" name="title" id="title" class="form-control"  
                                                data-msg-date="The Title field is required. must be a date." data-msg-required="The title field is required." 
                                                 data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('title'))<p class="help-block">{{ $errors->first('title')}}</p>@endif
                                        </div>
                                        <!--Announcement Decsription-->
                                        <div class="form-group col-md-12">
                                          <label for="inputName">Description</label><br>
                                             <input name="image" type="file" id="upload" class="hidden" onchange="" style="display:none">
                                                <textarea  name="pagedataset" id="addeditor" autofocus data-msg-date="The description field is required. must be a date." data-msg-required="The end date field is required." 
                                                 data-rule-date="true" data-rule-required="true" required></textarea>
                                                @if ($errors->has('pagedataset'))<p class="help-block">{{ $errors->first('principalname')}}</p>@endif
                                        </div>
                                        
                                             <!--startdate-->
                                            <div class='form-group input-group datepicker col-md-6' data-format="L">
                                                <input type='text' class="form-control" placeholder="startdate" name="datestart"  data-msg-date="The start date field is required. must be a date." data-msg-required="The start date field is required." 
                                                 data-rule-date="true" data-rule-required="true"/>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            
                                            <!--enddate-->
                            
                                            <div class='form-group input-group datepicker col-md-6' data-format="L">
                                                <input type='text' class="form-control" placeholder="enddate" name="dateend"  data-msg-date="The end date  field is required. must be a date." data-msg-required="The end date field is required." 
                                                 data-rule-date="true" data-rule-required="true" required/>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            
                                             <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#school-add').submit()">Save</button>
                                             </div>
                                        </form>
                                  </div>
                                </div>
                            </div>
                        </div>
     
    </div>
  </div>
</div>
@endsection