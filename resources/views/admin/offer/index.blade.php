@extends('layouts.admin.app2')
@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
<div id="wrap" class="animsition">
    <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
    <section id="content">
        <div class="page page-tables-datatables">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-12">
        
                <button type="button" class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10" data-toggle="modal" data-target="#addModal">
                   <i class="fa fa-plus"></i><span> Add offer</span>
                </button>
                <a href="{{url('offer')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('offer')){{'activeclass'}}@endif">
                    <i class="fa fa-tasks"></i><span>All offer</span>
                </a>
                <a href="{{url('offer/show')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('offer/show')){{'activeclass'}}@endif">
                    <i class="fa fa-star"></i><span>Active offer</span>
                </a>
                <a href="{{url('offer/showin')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('offer/showin')){{'activeclass'}}@endif">
                    <i class="fa fa-minus"></i><span>Inactive offer</span>
                </a>
                       <!-- Modal -->
            
                        <section class="tile">


                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>School Name</th>
                                              <th>Offer Name</th>
                                              <th>offer Description</th>
                                              <th>Start Date</th>
                                              <th>End Date</th>
                                              <th>City</th>
                                              <th>State</th>
                                              <th>Country</th>
                                              <th>amount</th>
                                              <th>offer Status</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                   @foreach($myoffer as $myoffers)



                                        <tr style="color:{{$myoffers->status ? '' : 'red' }}">

                                         <td class="statusset-{{$myoffers->id}}">{!!$i++!!}</td>
                                        
                                         <td class="statusset-{{$myoffers->id}}">{{$schooldata}}</td>

                                         <td class="statusset-{{$myoffers->id}}">{{$myoffers->offer_name}}</td>

                                         <td class="statusset-{{$myoffers->id}}">{!!$myoffers->offer_desc!!}</td>

                                        <td class="statusset-{{$myoffers->id}}">{!!$myoffers->first_date!!}</td>
                                        <td class="statusset-{{$myoffers->id}}">{!!$myoffers->last_date!!}</td>
                                        <td class="statusset-{{$myoffers->id}}">{{$countrydata}}</td>
                                        <td class="statusset-{{$myoffers->id}}">{{$statedata}}</td>
                                        <td class="statusset-{{$myoffers->id}}">{{$citydata}}</td>
                                        <td class="statusset-{{$myoffers->id}}">{!!$myoffers->amount!!}</td>
                                        <td>@if($myoffers->status == 1)<input type='checkbox' class="editstatus" title ="offer" name='checkbox2' rel="{{$myoffers->status}}" checked onchange="changestatus({{$myoffers->id}},this)"/>@elseif($myoffers->status == 0)<input type='checkbox' class="editstatus" title ="offer" name='checkbox2'  rel="{{$myoffers->status}}" onchange="changestatus({{$myoffers->id}},this)"/>@endif</td>
                                        <td><button type="button" class="btn btn-rounded-40 btn-ef btn-ef-2 btn-ef-2-cyan btn-ef-2a mb-10 ml-10" data-toggle="modal" data-target="#myModal-{{$myoffers->id}}"><i class="fa fa-pencil"></i></button><button class="btn btn-rounded-10 btn-ef btn-ef-2 btn-ef-2-amethyst btn-ef-2a mb-10 btn-delete" title="offer" data-id="{{$myoffers->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="myModal-{{$myoffers->id}}" role="dialog">
                                            <div class="modal-dialog">
    
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$myoffers->offer_name}}</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('offer/update',$myoffers->id)}}" method="post" role="form" id="school-edit-{{$myoffers->id}}" class="offer-update-{{$myoffers->slug}}">
                                       
                                       {{method_field('PATCH')}}
                                       
                                        <!--offer Name-->
                                        <div class="form-group col-md-12">
                                          <label for="offername">Offer Name</label><br>
                                             <input type="text" name="name" id="offername" class="form-control" 
                                                data-msg-date="The offer Name field is required. must be a date." data-msg-required="The offer Name field is required." 
                                                data-rule-date="true" data-rule-required="true" value="{{$myoffers->offer_name}}" required>
                                                @if ($errors->has('offername'))<p class="help-block">{{ $errors->first('offername')}}</p>@endif
                                        </div>
                                         <!--offer Desc-->
                                             <div class="form-group col-md-12">
                                                <label for="offerdesc">Offer Desc: </label><br>
                                                <textarea name="offdesc" id="offerdesc" class="form-control" 
                                                 required>{{$myoffers->offer_desc}}</textarea>
                                                @if ($errors->has('offerdesc'))<p class="help-block">{{ $errors->first('offerdesc')}}</p>@endif
                                            </div>
                                        
                                        <!--Country-->
                                            <div class="col-md-4">
                                                <label for="country">Country: </label><br>
                                                <select name="country[]" class="form-control chosen-select" multiple id="country1"
                                                data-msg-required="The Country field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a Country...">
                                                    
                                                    @foreach($country as $countries)
                                                    <?php $con = '';?>

                                                    <?php if (in_array($countries->name,$cnamearray)) {
                                                        $con = ' selected="selected"';}?>
                                                    <option <?php echo $con ?> value="{{$countries->id}}">{{$countries->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('country'))<p class="help-block">{{ $errors->first('country')}}</p>@endif
                                            </div>
                                            <!--State-->
                                            <div class="form-group col-md-4">
                                                <label for="state">State: </label><br>
                                                <select name="state[]" class="form-control chosen-select" multiple id="state1"
                                                 data-msg-required="The State field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a State...">
                                                   @foreach($state as $states)
                                                    <?php $con = '';?>

                                                    <?php if (in_array($states->name,$snamearray)) {
                                                        $con = ' selected="selected"';}?>
                                                    <option <?php echo $con ?> value="{{$states->id}}">{{$states->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!--City-->
                                            <div class="form-group col-md-4">
                                                <label for="city">City: </label><br>
                                                <select name="city[]" id="city1" multiple class="form-control chosen-select" 
                                                 data-msg-required="The City field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a City...">
                                                     @foreach($city as $cities)
                                                    <?php $con = '';?>

                                                    <?php if (in_array($cities->name,$citynamearray)) {
                                                        $con = ' selected="selected"';}?>
                                                    <option <?php echo $con ?> value="{{$cities->id}}">{{$cities->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <!--shool name-->
                                            <div class="form-group col-md-12">
                                                <label>School Name:</label><br>
                                                 <select  name="schoolname[]" class="form-control chosen-select" multiple id="schoolname"
                                                data-msg-required="The School Name field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a School..." >
                                                    <option value="0">--Select--</option>
                                                    @foreach($school as $schools)
                                                     <?php $con = '';?>
                                                    <?php if (in_array($schools->name,$schoolnamearray)) {
                                                        $con = ' selected="selected"';}?>
                                                    <option <?php echo $con ?> value="{{$schools->id}}">{{$schools->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('schoolname'))<p class="help-block">{{ $errors->first('schoolname')}}</p>@endif
                                            </div>
                                        
                                        <!--offer start-->
                                            <div class="form-group col-md-4">
                                                <label for="startdate">Start Date: </label><br>
                                                <input type="text" name="datestart" id="datepickerstart1" class="form-control datepickerstart" 
                                                data-msg-date="Please Select Date." data-msg-required="Please Select Date." 
                                                data-rule-date="true" data-rule-required="true" required value="{{$myoffers->first_date}}">
                                                @if ($errors->has('datestart'))<p class="help-block">{{ $errors->first('datestart')}}</p>@endif
                                            </div>
                                            <!--offer end-->
                                            <div class="form-group col-md-4">
                                                <label for="enddate">End Date: </label><br>
                                                 <input type="text" name="dateend" id="datepickerend1" class="form-control datepickerend" 
                                                data-msg-date="Please Select Date." data-msg-required="Please Select Date." 
                                                data-rule-date="true" data-rule-required="true" required value="{{$myoffers->first_date}}">
                                                @if ($errors->has('dateend'))<p class="help-block">{{ $errors->first('dateend')}}</p>@endif
                                            </div>
                                            <!--offer Amount-->
                                            <div class="form-group col-md-4">
                                                <label for="pin">offer Amount: </label><br>
                                                <input type="text" name="amount" id="amount" class="form-control number-only" 
                                                data-msg-date="Please set the offer amount." data-msg-required="Please set the offer amount." 
                                                data-rule-date="true" data-rule-required="true" value="{{$myoffers->amount}}">
                                                @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif
                                            </div>
                                         </form>
                                         <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$myoffers->id}}').submit()">Save changes</button>
                                     </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                        </td>

                                         </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                </div>
                                <!-- /tile body -->

                            </section>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ CONTENT -->
</div>
 <!--/ Application Content -->
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalLabel">offer Add</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                         </div>
                          <div class="modal-body">
                              <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{url('offer/store')}}" method="post" role="form" id="offer-add">
                                        
                                        
                                        <!--offer Name-->
                                        <div class="form-group col-md-12">
                                        <label for="name">offer Name: </label>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" name="name" id="name" class="form-control" data-msg-required="The Name field is required." 
                                data-rule-date="true" data-rule-required="true"  required>
                                                @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif
                                            </div>
                                             <!--offer Desc-->
                                            <div class="form-group col-md-12">
                                                <label>offer Description:</label>
                                                 <textarea name="offdesc" id="offdesc" class="form-control"  data-msg-required="Please give description." 
                                                 required></textarea>
                                                @if ($errors->has('offdesc'))<p class="help-block">{{ $errors->first('offdesc')}}</p>@endif
                                            </div>
                                            
                                          <!--Country-->
                                            <div class="form-group col-md-4">
                                                <label for="country">Country: </label>
                                                <select name="country[]" class="form-control chosen-select" multiple id="country"
                                                data-msg-required="The Country field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a Country...">
                                                    <option value="0">--Select--</option>
                                                    @foreach($country as $countries)
                                                    <option value="{{$countries->id}}">{{$countries->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('country'))<p class="help-block">{{ $errors->first('country')}}</p>@endif
                                            </div>
                                            <!--State-->
                                            <div class="form-group col-md-4 style-sub-1" style="display:none;">
                                                <label for="state">State: </label>
                                                <select name="state[]" class="form-control chosen-select " multiple id="state"
                                                 data-msg-required="The State field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a State...">
                                                    @foreach($state as $states)
                                                    <option value="{{$states->id}}">{{$states->name}}</option>
                                                    @endforeach
                                                  </select>
                                            </div>
                                            <!--City-->
                                            <div class="form-group col-md-4  style-sub-2" style="display:none;">
                                                <label for="city">City: </label>
                                                <select name="city[]" id="city" class="form-control chosen-select" multiple 
                                                 data-msg-required="The City field is required." 
                                                data-rule-required="true" data-placeholder="Choose a City...">
                                                   @foreach($city as $cities)
                                                      <option value="{{$cities->id}}">{{$cities->name}}</option>
                                                   @endforeach
                                                </select>
                                            </div>
                                            <!--shool name-->
                                            <div class="form-group col-md-12">
                                                <label>School Name:</label>
                                                 <select  name="schoolname[]" class="form-control chosen-select" multiple id="schoolname"
                                                data-msg-required="The School Name field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a School..." >
                                                    <option value="0">--Select--</option>
                                                    @foreach($school as $schools)
                                                    <option value="{{$schools->id}}">{{$schools->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('schoolname'))<p class="help-block">{{ $errors->first('schoolname')}}</p>@endif
                                            </div>
                                              <!--offer start-->
                                            <div class="form-group col-md-4">
                                                <label for="startdate">Start Date: </label>
                                                <input type="text" name="datestart" id="datepickerstart" class="form-control datepickerstart" 
                                                data-msg-date="Please Select Date." data-msg-required="Please Select Date." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('datestart'))<p class="help-block">{{ $errors->first('datestart')}}</p>@endif
                                            </div>
                                            <!--offer end-->
                                            <div class="form-group col-md-4">
                                                <label for="enddate">End Date: </label>
                                                 <input type="text" name="dateend" id="datepickerend" class="form-control datepickerend" 
                                                data-msg-date="Please Select Date." data-msg-required="Please Select Date." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('dateend'))<p class="help-block">{{ $errors->first('dateend')}}</p>@endif
                                            </div>
                                            <!--offer Amount-->
                                            <div class="form-group col-md-4">
                                                <label for="pin">offer Amount: </label>
                                                <input type="text" name="amount" id="amount" class="form-control number-only" 
                                                data-msg-date="Please set the offer amount." data-msg-required="Please set the offer amount." 
                                                data-rule-date="true" data-rule-required="true">
                                                @if ($errors->has('amount'))<p class="help-block">{{ $errors->first('amount')}}</p>@endif
                                            </div>
                                           
                                             
                                            
                                            <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#offer-add').submit()">Save</button>
                                             </div>
                                        </form>
                                  </div>
                                </div>
                            </div>
                        </div>
     
    </div>
  </div>
</div>
@endsection