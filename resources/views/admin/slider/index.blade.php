@extends('layouts.admin.app2')
@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
<div id="wrap" class="animsition">
    <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
    <section id="content">
        <div class="page page-tables-datatables">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-12">
                    <div class="school-filter-btn">
                        <button type="button" class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10" data-toggle="modal" data-target="#addModal" >
                           <i class="fa fa-plus"></i><span> Add Slides</span>
                         </button>
                        <a href="{{url('slider')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('slider')){{'activeclass'}}@endif">
                            <i class="fa fa-tasks"></i><span>All Slides</span>
                        </a>
                        <a href="{{url('slider/show')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('slider/show')){{'activeclass'}}@endif">
                            <i class="fa fa-star"></i><span>Active Slides</span>
                        </a>
                        <a href="{{url('slider/showin')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('slider/showin')){{'activeclass'}}@endif">
                            <i class="fa fa-minus"></i><span>Inactive Slides</span>
                        </a>
                
                    </div>
                       <!-- Modal -->
            
                        <section class="tile">


                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Username</th>
                                              <th>Image</th>
                                              <th>Heading</th></th>
                                              <th>Description</th>
                                              <th>Slider Status</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>

                                   @foreach($slider as $sliders)



                                        <tr style="color:{{$sliders->status ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$sliders->id}}">{!!$i++!!}</td>
                                         
                                          <td class="statusset-{{$sliders->id}}">{!!$sliders->uname!!}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}"><img src="/theme/mainimage/{!!$sliders->slider!!}" width="100" height="100"></td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{$sliders->topcontent}}</td>
                                         
                                         <td class="statusset-{{$sliders->id}}">{{$sliders->bottomcontent}}</td>

                                         <td>@if($sliders->status == 1)<input type='checkbox' class="editstatus" title ="slider" name='checkbox2' rel="{{$sliders->status}}" checked  onchange="changestatus({{$sliders->id}},this)"/>@elseif($sliders->status == 0)<input type='checkbox' class="editstatus" title ="slider"  name='checkbox2'  rel="{{$sliders->status}}" onchange="changestatus({{$sliders->id}},this)"/>@endif</td>
                                         <td><button type="button" class="btn btn-rounded-40 btn-ef btn-ef-2 btn-ef-2-cyan btn-ef-2a mb-10 ml-10" data-toggle="modal"  data-target="#myModal-{{$sliders->id}}"><i class="fa fa-pencil"></i></button><button class="btn btn-rounded-10 btn-ef btn-ef-2 btn-ef-2-amethyst btn-ef-2a mb-10 btn-delete" title="slider"  data-id="{{$sliders->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="myModal-{{$sliders->id}}" role="dialog">
                                            <div class="modal-dialog">
    
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$sliders->topcontent}}</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('slider/update',$sliders->id)}}" method="post" role="form" id="school-edit-{{$sliders->id}}" class="school-update-{{$sliders->id}}" enctype="multipart/form-data">
                                       
                                       {{method_field('PATCH')}}
                                        <!--Top Content-->
                                        
                                        <div class="form-group col-md-6">
                                          <label for="top-content">Top Content</label><br>
                
                                            <input type="text" name="topcontent" id="name" class="form-control"  
                                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Content field is required." 
                                                 data-rule-date="true" data-rule-required="true" value="{{$sliders->topcontent}}" required>
                                                @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                        </div>
                                        <!--Bottom Content-->
                                        <div class="form-group col-md-6">
                                          <label for="bottomcontent">Bottom Content</label><br>
                                             <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                data-msg-date="The Content field is required. must be a date." data-msg-required="The Content field is required." 
                                                data-rule-date="true" data-rule-required="true" value="{{$sliders->bottomcontent}}" required>
                                                @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                        </div>
                                         <!--Slider-->
                                            <div class="form-group col-md-10">
                                                <label for="slider">slider: </label><br>
                                                <img src="/theme/mainimage/{{$sliders->slider}}" width="200" height="100"><br>
                                                <input type="file" name="filename" id="email" class="form-control" required>
                                                @if ($errors->has('file'))<p class="help-block">{{ $errors->first('file')}}</p>@endif
                                            </div>
                                         </form>
                                         <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#school-edit-{{$sliders->id}}').submit()">Save changes</button>
                                     </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                        </td>

                                         </tr>

                                    @endforeach

                                              </tbody>
                                            </table>
                                    </div>
                                </div>
                                <!-- /tile body -->

                            </section>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ CONTENT -->
</div>
 <!--/ Application Content -->
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalLabel">Slider Add</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                         </div>
                          <div class="modal-body">
                              <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{url('slider/store')}}" method="post" role="form" id="school-add" enctype="multipart/form-data">
                                        <!--Top Content-->
                                        <div class="form-group col-md-6">
                                        <label for="top-content">Top Content: </label>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" name="topcontent" id="name" class="form-control"  
                                data-msg-date="The Content field is required. must be a date." data-msg-required="The Name field is required." 
                                data-rule-date="true" data-rule-required="true"  required>
                                                @if ($errors->has('topcontent'))<p class="help-block">{{ $errors->first('topcontent')}}</p>@endif
                                            </div>
                                            <!--Principal Name-->
                                            <div class="form-group col-md-6">
                                                <label for="bottom-content">Bottom Content: </label>
                                                <input type="text" name="bottomcontent" id="principalname" class="form-control" 
                                                data-msg-date="The Content Name field is required. must be a date." data-msg-required="The Principal Name field is required." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('bottomcontent'))<p class="help-block">{{ $errors->first('bottomcontent')}}</p>@endif
                                            </div>
                                            <div class="form-group col-md-4" id="wrapper">
                                                <label for="email">Slider: </label>
                                                <input type="file" name="filename" id="fileupload" class="form-control" accept="image/*" onchange="preview_image(event)" required>
                                                <br><br>
                                                <img id="output_image"/>
                                                @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                            </div>
                                           <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#school-add').submit()">Save</button>
                                             </div>
                                        </form>
                                  </div>
                                </div>
                            </div>
                        </div>
     
    </div>
  </div>
</div>
@endsection