@extends('layouts.admin.appform')

@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
<section id="content">
<div class="page page-forms-validate">
    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-12">


<!-- tile -->
                            <section class="tile">
                                    <h2>Create New School</h2>
                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                    <form name="form2" role="form" id="form2" data-parsley-validate action="{{route('school.store')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        
                                        <!--School Name-->
                                            <div class="form-group col-md-6">
                                                <label for="name">School Name: </label>
                                                <input type="text" name="name" id="name" class="form-control" required>
                                                @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif
                                            </div>
                                            <!--Principal Name-->
                                            <div class="form-group col-md-6">
                                                <label for="name">Principal Name: </label>
                                                <input type="text" name="principalname" id="principalname" class="form-control" required>
                                                @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif
                                            </div>
                                            <!--Logo-->
                                            <!--<div class="form-group col-md-4">-->
                                            <!--    <label for="name">Logo: </label>-->
                                            <!--    <input type="file" name="filename" id="filename" class="form-control" required>-->
                                            <!--    @if ($errors->has('filename'))<p class="help-block">{{ $errors->first('filename')}}</p>@endif-->
                                                
                                            <!--</div>-->
                                              <!--Email-->
                                            <div class="form-group col-md-4">
                                                <label for="email">Email: </label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                                @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                            </div>
                                            <!--Set School Time-->
                                            <!--<div class="form-group col-md-4">-->
                                            <!--    <label>Time:</label>-->
                                            <!--    <input type="text" name="timeset" class="form-control">-->
                                            <!--    @if ($errors->has('timeset'))<p class="help-block">{{ $errors->first('timeset')}}</p>@endif-->
                                            <!--</div>-->
                                             
                                            <!--School Contact Number-->
                                            <div class="form-group col-md-4">
                                                <label>Telephone number:</label>
                                                <input type="text" name="telephone" class="form-control">
                                                @if ($errors->has('telephone'))<p class="help-block">{{ $errors->first('telephone')}}</p>@endif
                                            </div>
                                            <!--School Mobile Number-->
                                            <div class="form-group col-md-4">
                                                <label>Mobile number:</label>
                                                <input type="text" name="mobile" class="form-control">
                                                @if ($errors->has('mobile'))<p class="help-block">{{ $errors->first('mobile')}}</p>@endif
                                            </div>
                                             <!--Country-->
                                            <div class="form-group col-md-4">
                                                <label for="country">Country: </label>
                                                <select name="country" class="form-control" id="country">
                                                    <option value="0">--Select--</option>
                                                    @foreach($country as $countries)
                                                    <option value="{{$countries->id}}">{{$countries->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('country'))<p class="help-block">{{ $errors->first('country')}}</p>@endif
                                            </div>
                                            <!--State-->
                                            <div class="form-group col-md-4">
                                                <label for="state">State: </label>
                                                <select name="state" class="form-control" id="state">
                                                    <select name="state" id="state" class="form-control">
                                                        <option value="7"></option>
                                                    </select>
                                            </div>
                                            <!--City-->
                                            <div class="form-group col-md-4">
                                                <label for="city">City: </label>
                                                <select name="city" id="city" class="form-control">
                                                    <option value="122"></option>
                                                </select>
                                            </div>
                                           
                                             <!--Address-->
                                            <div class="form-group col-md-12">
                                                <label for="address">Address: </label>
                                                <textarea name="address" id="address" class="form-control" required></textarea>
                                                @if ($errors->has('address'))<p class="help-block">{{ $errors->first('address')}}</p>@endif
                                            </div>
                                            
                                             
                                             
                                        <div class="tile-footer text-right bg-tr-black lter dvd dvd-top col-md-12">
                                    <!-- SUBMIT BUTTON -->
                                    <button type="submit" class="btn btn-lightred" id="form2Submit">Submit</button>
                                      </div>
                             </form>
                            </div>
                        </div>
                    </div>
                                <!-- /tile body -->

                        </section>
                            <!-- /tile -->
                 </div>                 
            </div>
        </div>
</section>
            <!--/ CONTENT -->
</div>
@endsection


