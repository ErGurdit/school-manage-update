@extends('layouts.admin.app2')
@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
<div id="wrap" class="animsition">
    <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
    <section id="content">
        
        <div class="page page-tables-datatables">
            <!-- row -->
            <div class="row">
                 @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
                <!-- col --->
                <div class="col-md-12">
                    <div class="school-filter-btn">
                        <button type="button" class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10" data-toggle="modal" data-target="#addModal" >
                           <i class="fa fa-plus"></i><span> Add Subject</span>
                         </button>
                       
                
                    </div>
                       <!-- Modal -->
            
                        <section class="tile">
                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="table-responsive">
                          
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              <th>Subject Name</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>
                                   @foreach($subject as $subjects)
                                        <tr>
                            <td>{{$i++}}</td>
                                                        <td>{{$subjects->subjectname}}</td>

                                      <td><button type="button" class="btn btn-rounded-40 btn-ef btn-ef-2 btn-ef-2-cyan btn-ef-2a mb-10 ml-10" data-toggle="modal"  data-target="#myModal-{{$subjects->id}}"><i class="fa fa-pencil"></i></button><button class="btn btn-rounded-10 btn-ef btn-ef-2 btn-ef-2-amethyst btn-ef-2a mb-10" title="school" data-toggle="modal" data-target="#delModal-{{$subjects->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="myModal-{{$subjects->id}}" role="dialog">
                                            <div class="modal-dialog">
                                              <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="addModalLabel">Edit-{{$subjects->subjectname}}</h5>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                         <div class="modal-body">
                                     <div class="row">
                                    <div class="col-md-12">
                                     <form action="{{url('subject/update',$subjects->id)}}" method="post" role="form" id="subject-edit-{{$subjects->id}}" class="" enctype="multipart/form-data">
                                           {{method_field('PATCH')}}
                                        <!--School Name-->
                                     <div class="form-group col-md-6">
                                          <label for="inputName">Name</label><br>
                                                <input type="text" name="name" id="name" class="form-control"  
                                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Name field is required." 
                                                 data-rule-date="true" data-rule-required="true" value="{{$subjects->subjectname}}" required>
                                                                                     <span class="required">*</span>

                                                @if ($errors->has('name'))<p class="help-block name">{{ $errors->first('name')}}</p>@endif
                                        </div>
                                         </form>
                                         <div class="modal-footer col-md-12">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="$('#subject-edit-{{$subjects->id}}').submit()">Save changes</button>
                                     </div>
                                     </div>
                                     </div>
                                </div>
                            </div>
                                    
                                    </div>
                                  </div>
                                  <div class="modal fade" id="delModal-{{$subjects->id}}" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="delModalLabel">Subject Remove</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                         </div>
                          <div class="modal-body">
                              <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{url('subject/delete',$subjects->id)}}" method="get" role="form" id="subject-delete" enctype="multipart/form-data">
                                                    <!--School Name-->
                                            <div class="form-group col-md-6">
<p>Do you want to delete - {{$subjects->subjectname}}?</p> 
<input type="hidden" name="subj_id" value="{{$subjects->id}}">
<input type="hidden" name="subjectname" value="{{$subjects->subjectname}}"></div>
                                            
                                             <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Yes</button>
                                             </div>
                                        </form>
                                  </div>
                                </div>
                            </div>
                        </div>
     
    </div>
  </div>
</div>
                                        </td>
                                         </tr>
                                    @endforeach
                                              </tbody>
                                            </table>
                                    </div>
                                </div>
                                <!-- /tile body -->

                            </section>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ CONTENT -->
</div>
 <!--/ Application Content -->
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalLabel">Subject Add</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                         </div>
                          <div class="modal-body">
                              <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{url('subject/store')}}" method="post" role="form" id="subject-add" enctype="multipart/form-data">
                                            <!--School Name-->
                                            <div class="form-group col-md-6">
                                                <label for="name">Subject Name: </label>
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="text" name="name" id="name" class="form-control"  
                                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Name field is required." 
                                                data-rule-date="true" data-rule-required="true"  required>
                                                @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif
                                            </div>
                                            
                                             <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#subject-add').submit()" id="validate_subj_admin">Save</button>
                                             </div>
                                        </form>
                                  </div>
                                </div>
                            </div>
                        </div>
     
    </div>
  </div>
</div>

@endsection