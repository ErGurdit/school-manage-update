@extends('layouts.admin.app')

@section('content')

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
<section id="content">

<div class="page page-dashboard">

<h3>Assign Role</h3>
<table class="table">
    <tr>
        <th>Name</th>
        <th>Role</th>
        <th>Action</th>
    </tr>
    @forelse($user as $users)
    <tr>
        <td>{{$users->name}}</td>
        <td>
            @foreach($users->roles as $role)
            {{$role->name}}
            @endforeach
        </td>
        <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal-{{$users->id}}">
              Edit
            </button>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal-{{$users->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$users->name}} Role Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('user.update',$users->id)}}" method="post" role="form" id="role-form-{{$users->id}}">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <div class="form-group">
              <select name="roles[]" multiple>
                  @foreach($allrole as $role)
                  <option value="{{$role->id}}">{{$role->name}}</option>
                  @endforeach
              </select>  
            </div>
            
            <!--<input class="btn btn-primary" type="submit" value="Submit"/>-->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="$('#role-form-{{$users->id}}').submit()">Save changes</button>
      </div>
    </div>
  </div>
</div>
        </td>
    </tr>
    @empty
    <tr>
        <td>No Role</td>
    </tr>
    @endforelse
</table>
                   
</div>
</section>
            <!--/ CONTENT -->

@endsection


