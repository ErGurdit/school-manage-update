@extends('layouts.admin.app2')
@section('content')
<!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
<div id="wrap" class="animsition">
    <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
    <section id="content">
        <div class="page page-tables-datatables">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-12">
                    <div class="school-filter-btn">
                        <!--<button type="button" class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10" data-toggle="modal" data-target="#addModal" >-->
                        <!--   <i class="fa fa-plus"></i><span> Add School</span>-->
                        <!-- </button>-->
                        <a href="{{url('payment/school')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('payment/school')){{'activeclass'}}@endif">
                            <i class="fa fa-tasks"></i><span>School Payment</span>
                        </a>
                        <a href="{{url('payment/student')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('payment/student')){{'activeclass'}}@endif">
                            <i class="fa fa-star"></i><span>Student Payment</span>
                        </a>
                        <a href="{{url('payment/onlinefee')}}" class="btn btn-warning btn-rounded btn-ef btn-ef-5 btn-ef-5b mb-10" id="@if(Request::is('payment/onlinefee')){{'activeclass'}}@endif">
                            <i class="fa fa-minus"></i><span>Online Fee</span>
                        </a>
                
                    </div>
                       <!-- Modal -->
            
                        <section class="tile">


                                <!-- tile body -->
                                <div class="tile-body">
                                    <div class="table-responsive">
                                  
                                        <table class="table table-striped table-bordered display nowrap" id="example-2" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>SNo.</th>
                                              @if(Request::is('payment/student'))
                                              <th>Student Name</th>
                                              <th>Student Username</th>
                                              <th>Student Contact Number</th>
                                              @endif
                                              @if(Request::is('payment/student') || Request::is('payment/school') || Request::is('payment/onlinefee'))
                                              <th>School Name</th>
                                              <th>School Username</th>
                                              <th>School Email</th>
                                              <th>School Contact Number</th>
                                              @endif
                                              @if(Request::is('payment/school') || Request::is('payment/onlinefee'))
                                              <th>Country</th>
                                              <th>State</th>
                                              <th>City</th>
                                              <th>Pincode</th>
                                              <th>Address</th>
                                              @endif
                                              @if(Request::is('payment/student') || Request::is('payment/school') || Request::is('payment/onlinefee'))
                                              <th>Last Payment Date</th>
                                              <th>Payment Method</th>
                                              @endif
                                              @if(Request::is('payment/school') || Request::is('payment/onlinefee'))
                                              <th>Last payment amount</th>
                                              @elseif(Request::is('payment/student'))
                                              <th>Payment amount</th>
                                              <th>Payment Detail</th>
                                              <th>Payment transfer to school</th>
                                              <th>Transfer Date</th>
                                              @endif
                                              @if(Request::is('payment/school') || Request::is('payment/onlinefee'))
                                              <th>Next payment date</th>
                                              <th>Extended period</th>
                                              @elseif(Request::is('payment/onlinefee'))
                                              <th>Total Fees Taken</th>
                                              <th>Total amount taken</th>
                                              <th>Total pending amount</th>
                                              <th>Total paid amount</th>
                                              <th>Payment Status</th>
                                              @endif
                                              <th>View History</th>
                                              @if(Request::is('payment/onlinefee'))
                                              <th>Pay</th>
                                              @endif
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i = 1;?>
                                        @if(Request::is('payment/school'))
                                        @foreach($school as $schools)
                                        
                                        <tr rel="{{$schools->slug}}" style="color:{{$schools->status ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$schools->id}}">{!!$i++!!}</td>
                                         
                                         <td class="statusset-{{$schools->id}}">{{$schools->name}}</td>
                                         
                                         <td class="statusset-{{$schools->id}}">{{$schools->username}}</td>

                                         <td class="statusset-{{$schools->id}}">{!!$schools->email!!}</td>

                                         <td class="statusset-{{$schools->id}}">{!!$schools->mobile!!}</td>
                                           
                                         <td class="statusset-{{$schools->id}}">{!!$schools->cname!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->statename!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->cityname!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->pincode!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->address!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->last_payment_date!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->payment_method!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->last_payment_amount!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->next_payment_date!!}</td>
                                         <td class="statusset-{{$schools->id}}">{!!$schools->extended_period!!}</td>
                                        <!--@if(!empty($schools->timeperiod))-->
                                        <!--<td class="statusset-{{$schools->id}}">{!!$schools->timeperiod!!}</td>-->
                                        <!--@else-->
                                        <!--<td class="statusset-{{$schools->id}}">Not Applicable</td>-->
                                        <!--@endif-->
                                        <!--<td class="statusset-{{$schools->id}}">{!!$schools->payment_method!!}</td>-->
                                        <!--<td>@if($schools->status == 1)<input type='checkbox' class="editstatus" title ="school" name='checkbox2' rel="{{$schools->status}}" checked  onchange="changestatus({{$schools->id}},this)"/>@elseif($schools->status == 0)<input type='checkbox' class="editstatus" title ="school"  name='checkbox2'  rel="{{$schools->status}}" onchange="changestatus({{$schools->id}},this)"/>@endif</td>-->
                           
                                        <td>
                                        <button class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10">
                                            <span>View History</span>
                                        </button></td>
                                         </tr>

                                        @endforeach
                                        @elseif(Request::is('payment/student'))
                                         @foreach($student as $students)
                                        
                                        <tr style="color:{{$students->id ? '' : 'red' }}">
                                        
                                         <td class="statusset-{{$students->id}}">{!!$i++!!}</td>
                                         
                                         <td class="statusset-{{$students->id}}">{{$students->uname}}</td>
                                         
                                         <td class="statusset-{{$students->id}}">{{$students->username}}</td>

                                         <td class="statusset-{{$students->id}}">{!!$students->father_contact_number!!}/{!!$students->mother_contact_number!!}</td>

                                         <td class="statusset-{{$students->id}}">{!!$students->schname!!}</td>
                                         
                                         <td class="statusset-{{$students->id}}">{!!$students->username!!}</td>
                                         
                                         <td class="statusset-{{$students->id}}">{!!$students->schmail!!}</td>
                                         
                                         <td class="statusset-{{$students->id}}">{!!$students->schmobile!!}/{!!$students->schlandline!!}</td>
                                         
                                         <td class="statusset-{{$students->id}}">{!!$students->last_payment_date!!}</td>
                                         
                                         <td class="statusset-{{$students->id}}">{!!$students->payment_method!!}</td>
                                           
                                         <td class="statusset-{{$students->id}}">{!!$students->payment_amount!!}</td>
                                         <td class="statusset-{{$students->id}}">{!!$students->payment_transfer_school!!}</td>
                                         <td class="statusset-{{$students->id}}">{!!$students->payment_detail!!}</td>
                                         <td class="statusset-{{$students->id}}">{!!$students->transfer_date!!}</td>
                                        <td>
                                        <button class="btn btn-success btn-rounded btn-ef btn-ef-6 btn-ef-6c mb-10">
                                            <span>View History</span>
                                        </button></td>
                                         </tr>

                                        @endforeach
                                        @endif

                                              </tbody>
                                            </table>
                                    </div>
                                </div>
                                <!-- /tile body -->

                            </section>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ CONTENT -->
</div>
 <!--/ Application Content -->
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalLabel">School Add</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                         </div>
                          <div class="modal-body">
                              <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{url('payment/store')}}" method="post" role="form" id="school-add">
                                        
                                        
                                        <!--School Name-->
                                        <div class="form-group col-md-6">
                                        <label for="name">School Name: </label>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        
                                <input type="text" name="name" id="name" class="form-control"  
                                data-msg-date="The Name field is required. must be a date." data-msg-required="The Name field is required." 
                                data-rule-date="true" data-rule-required="true"  required>
                                                @if ($errors->has('name'))<p class="help-block">{{ $errors->first('name')}}</p>@endif
                                            </div>
                                            <!--Principal Name-->
                                            <div class="form-group col-md-6">
                                                <label for="name">Principal Name: </label>
                                                <input type="text" name="principalname" id="principalname" class="form-control" 
                                                data-msg-date="The Principal Name field is required. must be a date." data-msg-required="The Principal Name field is required." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('principalname'))<p class="help-block">{{ $errors->first('principalname')}}</p>@endif
                                            </div>
                                            <!--Logo-->
                                            <!--<div class="form-group col-md-4">-->
                                            <!--    <label for="name">Logo: </label>-->
                                            <!--    <input type="file" name="filename" id="filename" class="form-control" required>-->
                                            <!--    @if ($errors->has('filename'))<p class="help-block">{{ $errors->first('filename')}}</p>@endif-->
                                                
                                            <!--</div>-->
                                              <!--Email-->
                                            <div class="form-group col-md-4">
                                                <label for="email">Email: </label>
                                                <input type="email" name="email" id="email" class="form-control"
                                                data-msg-date="The Email field is required. must be a date."
                                                data-msg-required="The Email field is required." 
                                                data-rule-date="true" data-rule-required="true" required>
                                                @if ($errors->has('email'))<p class="help-block">{{ $errors->first('email')}}</p>@endif
                                            </div>
                                            <!--Set School Time-->
                                            <!--<div class="form-group col-md-4">-->
                                            <!--    <label>Time:</label>-->
                                            <!--    <input type="text" name="timeset" class="form-control">-->
                                            <!--    @if ($errors->has('timeset'))<p class="help-block">{{ $errors->first('timeset')}}</p>@endif-->
                                            <!--</div>-->
                                             
                                            <!--School Contact Number-->
                                            <div class="form-group col-md-4">
                                                <label>Telephone number:</label>
                                                <input type="text" name="telephone" id="telephone" class="form-control number-only"
                                                data-msg-date="The Number field is required. must be a date." data-msg-required="The Number field is required." 
                                                data-rule-date="true" data-rule-required="true" data-rule-maxlength="11" 
                                                data-rule-minlength="10">
                                                @if ($errors->has('telephone'))<p class="help-block">{{ $errors->first('telephone')}}</p>@endif
                                            </div>
                                            <!--School Mobile Number-->
                                            <div class="form-group col-md-4">
                                                <label>Mobile number:</label>
                                                <input type="text" name="mobile" id="mobile" class="form-control number-only"
                                                data-msg-date="The Mobile field is required. must be a date." data-msg-required="The Mobile field is required." 
                                                data-rule-date="true" data-rule-required="true" data-rule-maxlength="11" 
                                                data-rule-minlength="10">
                                                @if ($errors->has('mobile'))<p class="help-block">{{ $errors->first('mobile')}}</p>@endif
                                            </div>
                                             <!--Country-->
                                            <div class="form-group col-md-4">
                                                <label for="country">Country: </label>
                                                <select name="country" class="form-control chosen-select" id="country"
                                                data-msg-required="The Country field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a Country...">
                                                    <option value="0">--Select--</option>
                                                    @foreach($country as $countries)
                                                    <option value="{{$countries->id}}">{{$countries->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('country'))<p class="help-block">{{ $errors->first('country')}}</p>@endif
                                            </div>
                                            <!--State-->
                                            <div class="form-group col-md-4 style-sub-1" style="display:none;">
                                                <label for="state">State: </label>
                                                <select name="state" class="form-control chosen-select" id="state"
                                                 data-placeholder="Choose a State..." tabindex="3">
                                                   @foreach($state as $states)
                                                   
                                                  <option value="{{$states->id}}">{{$states->name}}</option>
                                                  
                                                   @endforeach
                                                  </select>
                                            </div>
                                            <!--City-->
                                            <div class="form-group col-md-4  style-sub-2" style="display:none;">
                                                <label for="city">City: </label>
                                                <select name="city" id="city" class="form-control chosen-select" 
                                                 data-msg-required="The City field is required." 
                                                data-rule-required="true" tabindex="4" data-placeholder="Choose a City...">
                                                   @foreach($city as $cities)
                                                
                                                  <option value="{{$cities->id}}">{{$cities->name}}</option>
                                                  
                                                   @endforeach
                                        
                                                </select>
                                            </div>
                                            <!--Pin Code-->
                                            <div class="form-group col-md-6">
                                                <label for="pin">Pin Code: </label>
                                                <input type="text" name="pin" id="pin" class="form-control number-only">
                                                @if ($errors->has('pin'))<p class="help-block">{{ $errors->first('pin')}}</p>@endif
                                            </div>
                                            
                                             <!--Payment Method-->
                                            <!--<div class="form-group col-md-6">-->
                                            <!--    <label for="pm">Payment Menthod: </label><br>-->
                                            <!--    <input type="radio" name="pm" id="pm" value="online">Online-->
                                            <!--    <input type="radio" name="pm" id="pm" value="check">Check-->
                                            <!--    @if ($errors->has('pm'))<p class="help-block">{{ $errors->first('pm')}}</p>@endif-->
                                            <!--</div>-->
                                            
                                            
                                         
                                             <!--Address-->
                                            <div class="form-group col-md-12">
                                                <label for="address">Address: </label>
                                                <textarea name="address" id="address" class="form-control" 
                                                 required></textarea>
                                                @if ($errors->has('address'))<p class="help-block">{{ $errors->first('address')}}</p>@endif
                                            </div>
                                            
                                            <!--Date Range-->
                                            <div class="form-group col-md-12">
                                                <input type="checkbox" class="trialperiod" name="trialperiod">Trial Period
                                                <div class="range form-group">
                                                <label for="date">Date</label>: </label>
                                                <input type="text" name="daterange" id="daterange" value="" />
                                                @if ($errors->has('daterange'))<p class="help-block">{{ $errors->first('daterange')}}</p>@endif
                                                </div>
                                            </div>
                                             <div class="modal-footer col-md-12 ">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="$('#school-add').submit()">Save</button>
                                             </div>
                                        </form>
                                  </div>
                                </div>
                            </div>
                        </div>
     
    </div>
  </div>
</div>
@endsection