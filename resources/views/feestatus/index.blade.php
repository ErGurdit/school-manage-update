@extends('layouts.principaltest.app')

@section('content')
<?php 
    //dd($student);
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
 <div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif
            <!-- Panel Modals Styles -->
            <div class="panel">
            <!--<button class="btn btn-primary" data-target="#myClass" data-toggle="modal"-->
            <!--type="button">Add Certificate</button>-->
                <div class="panel-body container-fluid admission">
                    <table class="table table-hover datatable table-striped w-full" data-plugin="dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Class</th>
                    <th>Student Name</th>
                    <th>User Name</th>
                                        <th>Email</th>

                    <th>Father Name</th>
                    <th>Father Contact Nu</th>
                    <th>Mother Name</th>
                    <th>Mother Contact No.</th>
                    <th>Resi Contact</th>
                    <th>Pending Months</th>
                    <th>Paid Months</th>
                    <th>Last Amount Paid</th>
                     <th>Last Paid Date</th>
                    <th>Last Submitted by</th>
                    <th>Last Paid by</th>
                    <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                @foreach($student as $students)
                  <tr id="class">
                    <td>{{$i++}}</td>
                    <!--td>{{$students->id}}</td-->
                    <td>{{$students->classname}}</td>
                    <td>{{$students->name}}</td>
                    <td>{{$students->username}}</td>
                                        <td><a href="mailto:{{$students->em}}">{{$students->em}}</a></td>

                    <td>{{$students->father_name}}</td>
                    <td>{{$students->father_contact_number}}</td>
                    <td>{{$students->mother_name}}</td>
                    <td>{{$students->mother_contact_number}}</td>
                    <td>{{$students->home_tel}}</td>
                    <td>{{$students->cp}}</td>
                    <td>{{$students->sm}}</td>
                    <td>{{$students->tm}}</td>
                    <td>{{date('d-m-Y', strtotime($students->up))}}</td>
                    <td>{{$students->name}}</td>
                    <td>{{$students->name}}</td>
                    <td><!--button type="button" class="btn btn-floating btn-success btn-sm waves-effect waves-classic waves-effect waves-classic" title="detail" data-toggle="modal" data-target="#mydetail-{{$students->id}}">
                                            <i class="icon md-account"></i>
                                        </button-->
                                        <button type="button" class="btn btn-floating btn-warning btn-sm waves-effect waves-classic waves-effect waves-classic" title="detail" data-toggle="modal" data-target="#mydetail-{{$students->id}}">
                                            <i class="icon md-account"></i>
                                        </button>
                                        
                                        <div class="row row-lg">
                        <div class="modal hide fade" id="mydetail-{{$students->id}}" role="dialog" tabindex="-1" data-focus-on="input:first">
                            <div class="modal-dialog">
                            <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Send Notification</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form name="f_s" method="post" action="{{ route('feestatus.store')}}" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                            <div class="form-group form-material">
                                                <label>Student</label>
                                                <input type="checkbox" name="student" value="1">
                                                <label>Accounts</label>
    <select name="account">

  @foreach($acct_id_m as $acct)
    <option value="{{$acct->accnt_id}}">{{$acct->uname}}</option>
  @endforeach
  </select>
                                                <label>Principal</label>
                                                <input type="checkbox" name="principal" value="1"><br>
                                                <label>Custom Message</label><br>
                                                <textarea name="custom_msg" rows="7" cols="24">
                                                    </textarea>
                                                <input type="hidden" name="schoolid" value="{{$students->schoolid}}"/>
                                                <input type="hidden" name="studentid" value="{{$students->userid}}"/>
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Send Notification</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                     </div>
                                     </div>
                                     </div>
                                     </div><!-- row-lg-->
                                        </td>
                  </tr>
                 
                 @endforeach
                        </tbody>
                    </table>
                </div>
                
                </div><!--panl-->
            
        </div>
    </div>
@endsection