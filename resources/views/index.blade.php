@extends('layouts.homepage.header')
@section('content')
@include('layouts.homepage.banner')
<!-- MAIN CONTENT -->
    <section class="clearfix linkSection hidden-xs">
      <div class="sectionLinkArea hidden-xs scrolling">
        <div class="container">
          <div class="row">
            <div class="col-sm-3">
              <a href="#ourCourses" class="sectionLink bg-color-1" id="coursesLink">
                <i class="fa fa-file-text-o linkIcon border-color-1" aria-hidden="true"></i>
                <span class="linkText">Courses</span>
                <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
              </a>
            </div>
            <div class="col-sm-3">
              <a href="#ourTeam" class="sectionLink bg-color-2" id="teamLink">
                <i class="fa fa-calendar-o linkIcon border-color-2" aria-hidden="true"></i>
                <span class="linkText">Teachers</span>
                <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
              </a>
            </div>
            <div class="col-sm-3">
              <a href="#ourGallery" class="sectionLink bg-color-3" id="galleryLink">
                <i class="fa fa-picture-o linkIcon border-color-3" aria-hidden="true"></i>
                <span class="linkText">Gallery</span>
                <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
              </a>
            </div>
            <div class="col-sm-3">
              <a href="#latestNews" class="sectionLink bg-color-4" id="newsLink">
                <i class="fa fa-files-o linkIcon border-color-4" aria-hidden="true"></i>
                <span class="linkText">News</span>
                <i class="fa fa-chevron-down locateArrow" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    @include('layouts.homepage.feature')
    @include('layouts.homepage.promote')
    <!-- WHITE SECTION -->
    <section class="whiteSection full-width clearfix coursesSection" id="ourCourses">
      <div class="container">
        <div class="sectionTitle text-center">
          <h2>
            <span class="shape shape-left bg-color-4"></span>
            <span>Our Courses</span>
            <span class="shape shape-right bg-color-4"></span>
          </h2>
        </div>

        <div class="row">
          <div class="col-sm-3 col-xs-12 block">
            <div class="thumbnail thumbnailContent">
              <a href="course-single-left-sidebar.html"><img src="{{asset('theme/assets/img/home/courses/course-1.jpg')}}" alt="image" class="img-responsive"></a>
              <div class="sticker bg-color-1">$50</div>
              <div class="caption border-color-1">
                <h3><a href="course-single-left-sidebar.html" class="color-1">Morbi scelerisque nibh.</a></h3>
                <ul class="list-unstyled">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                <ul class="list-inline btn-yellow">
                  <li><a href="cart-page.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Add to Cart</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> More</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12 block">
            <div class="thumbnail thumbnailContent">
              <a href="course-single-left-sidebar.html"><img src="{{asset('theme/assets/img/home/courses/course-2.jpg')}}" alt="image" class="img-responsive"></a>
              <div class="sticker bg-color-2">$50</div>
              <div class="caption border-color-2">
                <h3><a href="course-single-left-sidebar.html" class="color-2">Phasellus convallis eros.</a></h3>
                <ul class="list-unstyled">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                <ul class="list-inline btn-green">
                  <li><a href="cart-page.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Add to Cart</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> More</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12 block">
            <div class="thumbnail thumbnailContent">
              <a href="course-single-left-sidebar.html"><img src="{{asset('theme/assets/img/home/courses/course-3.jpg')}}" alt="image" class="img-responsive"></a>
              <div class="sticker bg-color-3">$50</div>
              <div class="caption border-color-3">
                <h3><a href="course-single-left-sidebar.html" class="color-3">Suspendisse a libero da.</a></h3>
                <ul class="list-unstyled">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                <ul class="list-inline btn-red">
                  <li><a href="cart-page.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Add to Cart</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> More</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12 block">
            <div class="thumbnail thumbnailContent">
              <a href="course-single-left-sidebar.html"><img src="{{asset('theme/assets/img/home/courses/course-4.jpg')}}" alt="image" class="img-responsive"></a>
              <div class="sticker bg-color-4">$50</div>
              <div class="caption border-color-4">
                <h3><a href="course-single-left-sidebar.html" class="color-4">Aenean cursus urna nec.</a></h3>
                <ul class="list-unstyled">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                <ul class="list-inline btn-sky">
                  <li><a href="cart-page.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Add to Cart</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> More</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    @include('layouts.homepage.color')
    <!-- WHITE SECTION -->
    <section class="whiteSection full-width clearfix homeGallerySection" id="ourGallery">
      <div class="container">
        <div class="sectionTitle text-center">
          <h2>
            <span class="shape shape-left bg-color-4"></span>
            <span>Our Gallery</span>
            <span class="shape shape-right bg-color-4"></span>
          </h2>
        </div>

        <div class="row">
          <div class="col-xs-12">
            <div class="filter-container isotopeFilters">
              <ul class="list-inline filter">
                <li class="active"><a href="#" data-filter="*">View All</a></li>
                <li><a href="#" data-filter=".charity">Charity</a></li>
                <li><a href="#" data-filter=".nature">nature</a></li>
                <li><a href="#" data-filter=".children">children</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row isotopeContainer" id="container">
          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector charity">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_1.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_1.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector nature">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_2.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_2.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector nature">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_3.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_3.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector charity">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_4.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_4.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector nature">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_5.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_5.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector children">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_6.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_6.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector children">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_7.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_7.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector children">
            <article class="">
              <figure>
                <img src="{{asset('theme/assets/img/home/home_gallery/gallery_sm_8.jpg')}}" alt="image" class="img-rounded">
                <div class="overlay-background">
                  <div class="inner"></div>
                </div>
                <div class="overlay">
                  <a class="fancybox-pop" rel="portfolio-1" href="{{asset('theme/assets/img/home/home_gallery/gallery_lg_8.jpg')}}">
                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                  </a>
                </div>
              </figure>
            </article>
          </div>
        </div>

        <div class="btnArea">
          <a href="photo-gallery.html" class="btn btn-primary">View more</a>
        </div>

      </div>
    </section>

    <!-- COUNT UP SECTION-->
    <section class="countUpSection">
      <div class="container">
        <div class="sectionTitleSmall">
          <h2>Some Fun Facts</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod</p>
        </div>

        <div class="row">
          <div class="col-sm-3 col-xs-12">
            <div class="text-center">
              <div class="counter">179</div>
              <div class="counterInfo bg-color-1">Events Held</div>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12">
            <div class="text-center">
              <div class="counter">579</div>
              <div class="counterInfo bg-color-2">Happy Funs</div>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12">
            <div class="text-center">
              <div class="counter">279</div>
              <div class="counterInfo bg-color-3">happy clients</div>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12">
            <div class="text-center">
              <div class="counter">479</div>
              <div class="counterInfo bg-color-4">hours support</div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- WHITE SECTION -->
    <section class="whiteSection full-width clearfix newsSection" id="latestNews">
      <div class="container">
        <div class="sectionTitle text-center">
          <h2>
            <span class="shape shape-left bg-color-4"></span>
            <span>Latest News</span>
            <span class="shape shape-right bg-color-4"></span>
          </h2>
        </div>

        <div class="row">
          <div class="col-sm-4 col-xs-12 block">
            <div class="thumbnail thumbnailContent">
              <a href="single-blog-left-sidebar.html"><img src="{{asset('theme/assets/img/home/news/news-1.jpg')}}" alt="image" class="img-responsive"></a>
              <div class="sticker-round bg-color-1">10 <br>July</div>
              <div class="caption border-color-1">
                <h3><a href="single-blog-left-sidebar.html" class="color-1">The standard chunk of Lorem.</a></h3>
                <ul class="list-inline">
                  <li><a href="single-blog-left-sidebar.html"><i class="fa fa-user" aria-hidden="true"></i>Jone Doe</a></li>
                  <li><a href="single-blog-left-sidebar.html"><i class="fa fa-comments-o" aria-hidden="true"></i>4 Comments</a></li>
                </ul>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature. </p>
                <ul class="list-inline btn-yellow">
                  <li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-4 col-xs-12 block">
            <div class="thumbnail thumbnailContent">
              <a href="single-blog-left-sidebar.html"><img src="{{asset('theme/assets/img/home/news/news-2.jpg')}}" alt="image" class="img-responsive"></a>
              <div class="sticker-round bg-color-2">10 <br>July</div>
              <div class="caption border-color-2">
                <h3><a href="single-blog-left-sidebar.html" class="color-2">Vestibulum congue massa vitae.</a></h3>
                <ul class="list-inline">
                  <li><a href="single-blog-left-sidebar.html"><i class="fa fa-user" aria-hidden="true"></i>Jone Doe</a></li>
                  <li><a href="single-blog-left-sidebar.html"><i class="fa fa-comments-o" aria-hidden="true"></i>4 Comments</a></li>
                </ul>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature. </p>
                <ul class="list-inline btn-green">
                  <li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-4 col-xs-12 block">
            <div class="thumbnail thumbnailContent">
              <a href="single-blog-left-sidebar.html"><img src="{{asset('theme/assets/img/home/news/news-3.jpg')}}" alt="image" class="img-responsive"></a>
              <div class="sticker-round bg-color-3">10 <br>July</div>
              <div class="caption border-color-3">
                <h3><a href="single-blog-left-sidebar.html" class="color-3">Curabitur ac nulla sodales risus.</a></h3>
                <ul class="list-inline">
                  <li><a href="single-blog-left-sidebar.html"><i class="fa fa-user" aria-hidden="true"></i>Jone Doe</a></li>
                  <li><a href="single-blog-left-sidebar.html"><i class="fa fa-comments-o" aria-hidden="true"></i>4 Comments</a></li>
                </ul>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classi cal Latin literature. </p>
                <ul class="list-inline btn-red">
                  <li><a href="single-blog-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="btnArea">
          <a href="blog-grid.html" class="btn btn-primary">View more</a>
        </div>

      </div>
    </section>

    <!-- LIGHT SECTION -->
    <section class="lightSection full-width clearfix homeContactSection">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-xs-12">
            <div class="homeContactContent">
              <h2>Our Address</h2>
              <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              <address>
                <p><i class="fa fa-map-marker bg-color-1" aria-hidden="true"></i>9/4/C Ring Road,Garden Street Dhaka,Bangladesh-1200</p>
                <p><i class="fa fa-envelope bg-color-2" aria-hidden="true"></i><a href="mailto:hello@example.com">hello@example.com</a></p>
                <p><i class="fa fa-phone bg-color-4" aria-hidden="true"></i>3333 222 1111</p>
              </address>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12">
            <div class="homeContactContent">
              <form action="#" method="POST" role="form">
                <div class="form-group">
                  <i class="fa fa-user"></i>
                  <input type="text" class="form-control border-color-1" id="exampleInputEmail1" placeholder="First name">
                </div>
                <div class="form-group">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                  <input type="text" class="form-control border-color-2" id="exampleInputEmail2" placeholder="Email address">
                </div>
                <div class="form-group">
                  <i class="fa fa-comments" aria-hidden="true"></i>
                  <textarea class="form-control border-color-4" placeholder="Write message"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Send Message</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    @include('layouts.homepage.footer')
@endsection