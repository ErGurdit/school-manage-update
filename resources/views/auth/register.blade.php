@extends('layouts.admin.app')

@section('content')
  <div id="wrap" class="animsition" style="opacity:1 !important;">




            <div class="page page-core page-login">

                <div class="text-center"><h3 class="text-light text-white"><span class="text-lightred">School</span>Management</h3></div>

                <div class="container w-420 p-15 bg-white mt-40 text-center">

                    <h2 class="text-light text-greensea">Sign Up</h2>
                    <form class="form-validation mt-20" role="form" method="POST" action="{{ url('/register') }}" name="form" >
                        {{ csrf_field() }}
                        <p class="help-block text-left">
                            Enter your personal details below:
                        </p>
                        
                        <!--Full Name-->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            
                                <input id="name" type="text" class="form-control underline-input" placeholder="Full Name" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                           
                        </div>
                        
                        <!--Email-->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           
                                <input id="email" type="email" class="form-control underline-input" placeholder="Email" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div>
                        
                        <!--Password-->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           
                                <input id="password" type="password" class="form-control underline-input" name="password" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                        </div>

                        <!--Password Confirm-->
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                
                                <input id="password-confirm" type="password" class="form-control underline-input" name="password_confirmation" placeholder="Password Confirm">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                        
                        </div>
                        
                        <div class="form-group text-left">
                            <label class="checkbox checkbox-custom-alt checkbox-custom-sm inline-block">
                                <input type="checkbox"><i></i> I agree to the <a href="javascript:;">Terms of Service</a> &amp; <a href="javascript:;">Privacy Policy</a>
                            </label>
                        </div>
                        
                    <div class="bg-slategray lt wrap-reset mt-20 text-left">
                        <p class="m-0">
                             <button type="submit" class="btn btn-greensea b-0 text-uppercase pull-right">
                                    <i class="fa fa-btn fa-user"></i> Register
                            </button>
                            <a href="{{ url('/login') }}" class="btn btn-lightred b-0 text-uppercase">Back</a>
                        </p>
                    </div>

                    </form>
                </div>
            </div>
        </div>
@endsection
