@extends('layouts.admin.app')

@section('content')
<!--Main Div-->
 <div id="wrap" class="animsition" style="opacity:1;">




            <div class="page page-core page-login">

                <div class="text-center"><h3 class="text-light text-white"><span class="text-lightred">School</span>Management</h3></div>

                <div class="container w-420 p-15 bg-white mt-40 text-center">


                    <h2 class="text-light text-greensea">Log In</h2>
                    <!--Start Form Tag-->
                    <form class="form-validation mt-20" role="form" method="POST" action="{{ url('/login') }}" novalidate="" name="form">
                        {{ csrf_field() }}
                        
                        <!--Email-->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           
                                <input id="email" type="email" class="form-control underline-input" name="email" value="{{ old('email') }}" placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        
                        <!--Password-->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           
                            <input id="password" type="password" class="form-control underline-input" name="password" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                        </div>

                        <div class="form-group text-left mt-20">
                            <button type="submit" class="btn btn-greensea b-0 br-2 mr-5">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                            </button>
                            <label class="checkbox checkbox-custom-alt checkbox-custom-sm inline-block">
                                        <input type="checkbox" name="remember"><i></i> Remember Me
                            </label>
                             <a class="pull-right mt-10" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                        </div>
                    </form>
                    <!--End Form Tag-->
                        <hr class="b-3x">
                    
                    <!--Social Logins-->
                    <div class="social-login text-left">

                        <ul class="pull-right list-unstyled list-inline">
                            <li class="p-0">
                                <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li class="p-0">
                                <a class="btn btn-sm btn-info b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li class="p-0">
                                <a class="btn btn-sm btn-lightred b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li class="p-0">
                                <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>

                        <h5>Or login with</h5>

                    </div>
                    
                    <!--Register User-->
                    <div class="bg-slategray lt wrap-reset mt-40">
                        <p class="m-0">
                            <a href="{{ url('/register') }}" class="text-uppercase">Create an account</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Main Div-->
@endsection
