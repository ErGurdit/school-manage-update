@extends('layouts.principaltest.messageapp')

@section('content')
<style>

</style>
<div class="page">
    <!-- Message Sidebar -->
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner">
        <div class="input-search">
          <button class="input-search-btn" type="submit">
            <i class="icon md-search" aria-hidden="true"></i>
          </button>
          <form>
            <input class="form-control" type="text" placeholder="Search Keyword" name="">
          </form>
        </div>

        <div class="app-message-list page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <ul class="list-group">
                <li class="list-group-item " v-for="privsteMsg in privsteMsgs">
                	<!--ifstart-->
                  <div class="media " v-if="privsteMsg.status==1"  @click="messages(privsteMsg.id)">
                    <div class="pr-20 active">
                      <a class="avatar avatar-online" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/1.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">@{{privsteMsg.name}}</h5>
                      <span class="media-time">@{{privsteMsg.gender}}</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger">3</span>
                    </div>
                  </div>
                  <!--elsestart-->
                  <div class="media" v-else  @click="messages(privsteMsg.id)">
                    <div class="pr-20">
                      <a class="avatar avatar-online" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/1.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">@{{privsteMsg.name}}</h5>
                      <span class="media-time">@{{privsteMsg.gender}}</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger">3</span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-online" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/2.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Eric hoffman</h5>
                      <span class="media-time">1 minutes ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-online" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/3.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Eddie Lobanovskiy</h5>
                      <span class="media-time">5 minutes ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-online" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/4.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Bill S Kenney</h5>
                      <span class="media-time">15 minutes ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger">5</span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item active">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-away" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/5.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Derek Bradley</h5>
                      <span class="media-time">40 minutes ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-away" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/6.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Mariusz Ciesla</h5>
                      <span class="media-time">2 hours ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-online" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/7.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Jesse Dodds</h5>
                      <span class="media-time">3 hours ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-off" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/8.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Gerren Lamson</h5>
                      <span class="media-time">3 hours ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-off" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/9.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Daniel Waldron</h5>
                      <span class="media-time">5 hours ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="pr-20">
                      <a class="avatar avatar-off" href="javascript:void(0)">
                        <img class="img-fluid" src="{{asset('principal/assetstest/global/portraits/10.jpg')}}"
                          alt="..."><i></i></a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-0 mb-5">Celikovic</h5>
                      <span class="media-time">7 hours ago</span>
                    </div>
                    <div class="pl-20">
                      <span class="badge badge-pill badge-danger"></span>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Message Sidebar -->
    <div class="page-main">
      <!-- Chat Box -->
      <div class="app-message-chats">
        <button type="button" id="historyBtn" class="btn btn-round btn-default btn-flat primary-500">History Messages</button>
        <div class="chats" v-for="singleMsg in singleMsgs">
          <div class="chat" v-if="singleMsg.user_from == <?php echo Auth::user()->id; ?>">
            <div class="chat-avatar">
              <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="">
                <img src="{{asset('principal/assetstest/global/portraits/4.jpg')}}" alt="June Lane">
              </a>
            </div>
            <div class="chat-body">
              <div class="chat-content">
                <p>
                  @{{singleMsg.msg}}
                </p>

              </div>
            </div>
          </div>
          <div class="chat chat-left" v-else>
            <div class="chat-avatar">
              <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                <img src="{{asset('principal/assetstest/global/portraits/5.jpg')}}" alt="Edward Fletcher">
              </a>
            </div>
            <div class="chat-body">
              <div class="chat-content">
                <p>
                  @{{singleMsg.msg}}
                </p>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- End Chat Box -->

      <!-- Message Input-->
      <form class="app-message-input">
        <div class="input-group form-material">
          <span class="input-group-btn">
            <a href="javascript: void(0)" class="btn btn-pure btn-default icon md-camera"></a>
          </span>
          <input type="hidden" v-model="conID">
	         <textarea class="col-md-12 form-control" v-model="msgFrom" @keydown="inputHandler"></textarea>
          <span class="input-group-btn">
            <button type="button" class="btn btn-pure btn-default icon md-mail-send"></button>
          </span>
        </div>
      </form>
      <!-- End Message Input-->

    </div>
  </div>
@endsection