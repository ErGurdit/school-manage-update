
@extends('layouts.principaltest.app')

@section('content')
<?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;
?>
<style>
  #__lpform_fname{
    display:none !important;
}
    
    
}
</style>
<div class="page">
        <div class="page-header">
            <h1 class="page-title">{{$firstparam}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
            </ol>
        </div>
        <div class="page-content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif

                        <!-- Panel Table Add Row -->
      <div class="panel">
        <header class="panel-heading">
          <!--h3 class="panel-title">Add Row</h3-->
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <!--div class="mb-15">
                <button id="addToTable" class="btn btn-primary" type="button">
                  <i class="icon md-plus" aria-hidden="true"></i> Add row
                </button>
              </div-->
            </div>
          </div>
            <div class="example-wrap">
                <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne"
                        aria-controls="exampleTabsOne" role="tab">Received</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo"
                        aria-controls="exampleTabsTwo" role="tab">Increment</a></li>
                    
                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                      <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
            <thead>
              <tr>
                <th>Date</th>
                <th>Deductions</th>
                <th>Salary Amount(s)</th>
                <th>Accountant name</th>
                <th>Deposit Amount</th>
                <th>Leaves</th>
                <th>Absent</th>
                <th>Present</th>


               
              </tr>
            </thead>
            <tbody>
                @foreach($student as $std)
                <tr class="gradeA">
                <td>{{$std->created_at}}</td>
                <td>{{$std->last_decrement_amount}}</td>
                <td>{{$std->salary}}</td>
                 <td>{{$uname}}</td>
                 <td>{{$std->deposit}}</td>
                 @if($std->pr != NULL)
                                  <td>{{$std->count_pr}}</td>
                                <td>{{$std->count_ab}}</td>
                                                                <td>{{$std->count_lv}}</td>

                                @endif

                                  

                <!--td class="actions">
                  <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row"
                    data-toggle="tooltip" data-original-title="Save" hidden><i class="icon md-wrench" aria-hidden="true"></i></a>
                  <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                    data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
                   </td-->
              </tr>
                          @endforeach 
            </tbody>
          </table>
                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                      <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
            <thead>
              <tr>
                <th>Date</th>
                <th>Increment(s)</th>
                 <th>Bonus(s)</th>
              </tr>
            </thead>
            <tbody>
                @foreach($student as $std)
                            <tr class="gradeA">
                <td>{{$std->created_at}}</td>
                <td>{{$std->last_increment_amount}}</td>
                <td>{{$std->bonus}}</td>

                <!--td class="actions">
                  <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row"
                    data-toggle="tooltip" data-original-title="Save" hidden><i class="icon md-wrench" aria-hidden="true"></i></a>
                  <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                    data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
                   </td-->
              </tr>
                          @endforeach 
            </tbody>
          </table>
                         </div>
                    
                  </div>
                </div>
              </div>
          
        </div>
      </div>
      <!-- End Panel Table Add Row -->

</div><!---End page-->
</div>