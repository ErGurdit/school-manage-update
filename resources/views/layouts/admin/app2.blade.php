<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>School Management - Admin Dashboard</title>
        <link rel="icon" type="image/ico" href="{{asset('assets/images/favicon.ico')}}" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/animsition/css/animsition.min.css')}}">
        <link href="//www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">-->
      
        
       
          
         
         <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/datatables.bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/ColVis/css/dataTables.colVis.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.min.css')}}">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="{{asset('assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
         
      <!-- project main css files -->
      <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}"> 
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="{{asset('assets/css/switch/on-off-switch.css')}}">
        <link href="{{asset('assets/js/jquery-customselect.css')}}" rel='stylesheet' />
        <!--<link rel="stylesheet" href="{{asset('assets/css/newstyle.css')}}">-->
        <link rel="stylesheet" href="{{asset('assets/css/prism.css')}}">
         <link rel="stylesheet" href="{{asset('assets/css/chosen.css')}}">
         <link rel="stylesheet" type="text/css" href="{{asset('theme/simeditor/asset/css/simditor.css')}}" />
        <!--/ stylesheets -->

        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script src="{{asset('assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
        <!--/ modernizr -->




</head>
<body id="minovate" class="appWrapper">
    
  
    @if(Auth::check())
    @include('layouts.admin.header')
    @include('layouts.admin.sidebar')
    @endif
    @yield('content')
   
 <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
    <!-- JavaScripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('assets/js/jquery-customselect.js')}}"></script>
    <script src="{{asset('assets/js/switch/on-off-switch.js')}}"></script>
    <script src="{{asset('assets/js/switch/on-off-switch-onload.js')}}"></script>
    <script src="{{asset('assets/js/switch/bootstrap-switch.js')}}"></script>
    
   
   

        <script src="{{asset('assets/js/vendor/bootstrap/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/jRespond/jRespond.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/sparkline/jquery.sparkline.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/slimscroll/jquery.slimscroll.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/animsition/js/jquery.animsition.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/screenfull/screenfull.min.js')}}"></script>
     
       
       
       
       
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.js"></script>
        <script src="{{asset('assets/js/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/ColVis/js/dataTables.colVis.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/dataTables.bootstrap.js')}}"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script src="{{asset('assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <script type="text/javascript" src="{{asset('assets/js/jquery.tabledit.min.js')}}"></script>
        <script src="{{asset('assets/js/chosen.jquery.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/prism.js')}}" type="text/javascript" charset="utf-8"></script>
        <script src="{{asset('assets/js/init.js')}}" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="{{asset('theme/simeditor/asset/script/module.js')}}"></script>
        <script type="text/javascript" src="{{asset('theme/simeditor/asset/script/hotkeys.js')}}"></script>
        <script type="text/javascript" src="{{asset('theme/simeditor/asset/script/uploader.js')}}"></script>
        <script type="text/javascript" src="{{asset('theme/simeditor/asset/script/simditor.js')}}"></script>
             <script src="{{asset('principal/assetstest/global/vendor/formvalidation/formValidation.min.js?v4.0.1')}}"></script>

        
        
            <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
<script>
	
 $(function () {
        $(".chkmobile").click(function () {
            if ($(this).is(":checked")) {
                $("#txtmobileNumber").removeAttr("readonly");
                $("#txtmobileNumber").focus();
                
            } else {
                $("#txtmobileNumber").attr("readonly", "readonly");
                 
            }
        });
        $(".chkemail").click(function () {
            if ($(this).is(":checked")) {
                $("#txtemail").removeAttr("readonly");
                $("#txtemail").focus();
            } else {
                $("#txtemail").attr("readonly", "readonly");
            }
        });
        $(".chkfb").click(function () {
            if ($(this).is(":checked")) {
                $("#txtfb").removeAttr("readonly");
                $("#txtfb").focus();
            } else {
                $("#txtfb").attr("readonly", "readonly");
            }
        });
        $(".chktwitter").click(function () {
            if ($(this).is(":checked")) {
                $("#txttwitter").removeAttr("readonly");
                $("#txttwitter").focus();
            } else {
                $("#txttwitter").attr("readonly", "readonly");
            }
        });
        $(".chkinsta").click(function () {
            if ($(this).is(":checked")) {
                $("#txtinsta").removeAttr("readonly");
                $("#txtinsta").focus();
            } else {
                $("#txtinsta").attr("readonly", "readonly");
            }
        });
        $(".chkgp").click(function () {
            if ($(this).is(":checked")) {
                $("#txtgp").removeAttr("readonly");
                $("#txtgp").focus();
            } else {
                $("#txtgp").attr("readonly", "readonly");
            }
        });
    });

 
//image preview
function preview_image(event) 
{
 var reader = new FileReader();
 reader.onload = function()
 {
  var output = document.getElementById('output_image');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}
$(".editstatus").bootstrapSwitch({
    onText:"Active"
});

function changestatus( id, e ) {
    var el = $( e );
    var td = el.closest( "td" );
    var tr = el.closest( "tr" );
    var titles = el.attr('title');
    
    
    var statuson = td.find( '.statuson' ).prop( 'disabled', true );
    var statusoff = td.find( '.statusoff' ).prop( 'disabled', true );
    
    var status = +el.is( ':checked' );
    
    $.ajax( {
        url: "/"+titles+"/changestatus/" + id,
        method: "POST",
        data: {
            status: status
        },
        success: function() {
            if( 1 == status ) {
                tr.css( 'color', '' );
            } else {
                tr.css( 'color', 'red' );
            }
        },
        complete: function(){
            statuson.prop( 'disabled', false );
            statusoff.prop( 'disabled', false );
        }
    } );
    return;
}
// $(".editstatus").change(function(rel) {
//     alert(rel);
//         // $.ajax({
//         //       type: "get",
//         //       url: "school/status"+id,
//         //       success: function(data) {
//         //             var obj = $.parseJSON(data); 
//         //             console.log(obj)
//         //             var result = "<ul>"
//         //             $.each(obj, function() {
//         //                 result = result + "<li>First Name : " + this['firstname'] + " , Last Name : " + this['lastname'] + "</li>";
//         //             });
//         //             result = result + "</ul>"
//         //             $("#result").html(result);
//         //       }
//         // }); 
//     });
$(document).ready(function(){
 //datepicker
   $(".alert-success").delay(5000).fadeOut("slow");
    //alert($(".alert-danger").html());
        
$(function() {
    $('#datepickerstart').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        format: 'DD/MM/YYYY',
    });
    $('#datepickerstart1').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        format: 'DD/MM/YYYY'
    });
});
$(function() {
    $('#datepickerend').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        format: 'DD/MM/YYYY',
    });
    $('#datepickerend1').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        format: 'DD/MM/YYYY',
    });
     $('#datepickerstartannounce1').daterangepicker({
        autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      format: 'DD/MM/YYYY',
    });
    $('#datepickerstartannounce2').daterangepicker({
         autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      format: 'DD/MM/YYYY',
    });
    $('#datepickerendannounce1').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      format: 'DD/MM/YYYY',
    });
     $('#datepickerendannounce2').daterangepicker({
        autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      },
    });
});   


$('input.trialperiod').change(function(){
		if ($(this).is(':checked')) $('div.range').show();
		else $('div.range').hide();
	}).change();
	
//defaultsetting
$('input.text-mobile').change(function(){
		if ($(this).is(':checked')) $('input#mobile').show();
		else $('input#mobile').hide();
	}).change();
$('input.text-email').change(function(){
		if ($(this).is(':checked')) $('input#email').show();
		else $('input#email').hide();
	}).change();
$('input.text-fb').change(function(){
		if ($(this).is(':checked')) $('input#fb').show();
		else $('input#fb').hide();
	}).change();
$('input.text-tweet').change(function(){
		if ($(this).is(':checked')) $('input#tweet').show();
		else $('input#tweet').hide();
	}).change();
$('input.text-insta').change(function(){
		if ($(this).is(':checked')) $('input#insta').show();
		else $('input#insta').hide();
	}).change();
$('input.text-google').change(function(){
		if ($(this).is(':checked')) $('input#google').show();
		else $('input#google').hide();
	}).change();
//range
$(function() {
    $('input[name="daterange"]').daterangepicker({
  locale: { cancelLabel: 'Clear',
  format: 'DD/MMM/YYYY' } ,
     defaultDate: '',
     
  });
  $('#daterange').val('');
});
$('#daterange').on('cancel.daterangepicker', function(ev, picker) {
  //do something, like clearing an input
  $(this).val(picker.startDate.format('DD/MMM/YYYY') + ' to ' + picker.endDate.format('DD/MMM/YYYY'));
  $(this).val('');
});

$( ".savebutton" ).bind( "click", function() {
  var str = $(this).parent().parent().prev().attr('id');
  var res = str.split("-");
  saveAsNewName(res[1]);
});
// $( ".btn-delete" ).bind( "click", function() {
//   var str = $(this).parent().parent().prev().attr('id');
//   var res = str.split("-");
//   saveAsNewName(res[1]);
// });
    //number validation
    $('.number-only').keypress(function(e) {
	if(isNaN(this.value+""+String.fromCharCode(e.charCode))) return false;
  })
  .on("cut copy paste",function(e){
	e.preventDefault();
  });
  //email validate
  var sEmail = $('#email').val();
  function validateEmail(sEmail) {
  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
  if (filter.test(sEmail)) {
   return true;
  }
  else {
  return false;
  }
  };
  //select with searchh
    $(function() {
        $("#country").customselect();
        $("#country1").customselect();
    });
     //dataupdate
    function saveAsNewName(id) {
         
        
    var values = {
            'str': $('td#editname-'+id +' input').val(),
            'str1': $('td#editprincipal_name-'+id +' input').val(),
            'str2': $('td#editemail-'+id +' input').val(),
            'str3': $('td#editmobile-'+id +' input').val(),
            'str4': $('td#editdaterange-'+id +' input').val(),
            'str5': $('td#editphone-'+id +' input').val(),
            'str6': $('td#editpincode-'+id +' input').val(),
            'str7': $('td#editaddress-'+id +' input').val(),
            'str8': $('td#editpayment-'+id +' input').val(),
            
            
    };
    $.ajax({
        url: "/school/update/"+id,
        type: "POST",
        data: values,
    });


};

   $('#country').change(function(){

    var countryID = $(this).val();

    if(countryID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-state-list')}}?country_id="+countryID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#state").empty();

                $("#state").append('<option>Select</option>');

                $.each(res,function(key,value){

                    $("#state").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#state").empty();

            }

           }

        });

    }else{

        $("#state").empty();

        $("#city").empty();

    }

   });
   $('#state').on('change',function(){

    var stateID = $(this).val();

    if(stateID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-city-list')}}?state_id="+stateID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#city").empty();

                $.each(res,function(key,value){

                    $("#city").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#city").empty();

            }

           }

        });

    }else{

        $("#city").empty();

    }



   });
   
   $('#country1').change(function(){

    var countryID = $(this).val();

    if(countryID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-state-list')}}?country_id="+countryID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#state1").empty();

                //$("#state1").append('<option>Select</option>');

                $.each(res,function(key,value){

                    $("#state1").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#state1").empty();

            }

           }

        });

    }else{

        $("#state1").empty();

        $("#city1").empty();

    }

   });
   $('#state1').on('change',function(){

    var stateID = $(this).val();

    if(stateID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-city-list')}}?state_id="+stateID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#city1").empty();

                $.each(res,function(key,value){

                    $("#city1").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#city1").empty();

            }

           }

        });

    }else{

        $("#city1").empty();

    }



   });
   
   //validatie data
    $("form#school-add").validate({
          rules: {

        telephone: {
            number: true, // as space is not a number it will return an error
            minlength: 10, // will count space 
            maxlength: 12
        },
        mobile: {
            number: true, // as space is not a number it will return an error
            minlength: 10, // will count space 
            maxlength: 12
        },
        pin: {
            number: true, // as space is not a number it will return an error
            minlength: 6, // will count space 
            maxlength: 7
        }
     },
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
     
  });
    $("form#school-edit").validate({
        
        rules: {

        telephone: {
            number: true, // as space is not a number it will return an error
            minlength: 10, // will count space 
            maxlength: 12
        },
        mobile: {
            number: true, // as space is not a number it will return an error
            minlength: 10, // will count space 
            maxlength: 12
        },
        pin: {
            number: true, // as space is not a number it will return an error
            minlength: 6, // will count space 
            maxlength: 7
        }
     },
          
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
  $("form#subject-add").validate({
        
        rules: {

       
        name: {
            required: true, // as space is not a number it will return an error
           // minlength: 6, // will count space 
            //maxlength: 7
        }
     },
          
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
   $("form#offer-add").validate({
       
        showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
  $("form.announce-add").validate({
       
        showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
   
  
  //change select
 $('#country').change(function(){
     $('.style-sub-1').removeAttr("style");
 });
 $('#state').change(function(){
     $('.style-sub-2').removeAttr("style");
 });
 
 //Datatable
 $("#example-2").DataTable( {
        "scrollX": true,
    });
    
$('.tabledit-edit-button').click(function() {
        $('.tabledit-edit-button').toggle();
        $('.tabledit-delete-button').toggle();
    });
    $('.tabledit-save-button').click(function() {
        $('.tabledit-save-button').toggle();
        $('.tabledit-edit-button').toggle();
        $('.tabledit-delete-button').toggle();
        
    });
//Delete
$('tbody').delegate('.btn-delete','click',function(){

        var value=$(this).data('id');
        var e = $('.btn-delete').attr('title');

        //alert($e);

        var url = e+"/destroy/"+value;

        // $.ajax({

        //   type: 'get',

        //   url: url,

        //   data: {'id':value},

        //   success : function(data){

        //     $('#employee'+value).remove()

        //     //console.log(data);

        //   }

        // })

      });

      $('body').on('click','.btn-delete',function(){

       var value=$(this).data('id');
        var e = $('.btn-delete').attr('title');

        //alert($e);

        var url = e+"/destroy/"+value;

        swal({

          title: "Are you sure?",

          text: "Do you want to delete!",

          type: "warning",

          showCancelButton: true,

          confirmButtonColor: "#DD6B55",

          confirmButtonText: "Yes, delete it!",

          cancelButtonText: "No",

          closeOnConfirm: false,

          closeOnCancel: false

        },

        function(isConfirm){

          if (isConfirm) {

            swal("Deleted!", e+ "deleted.", "success");

            $.ajax({

              type: 'get',

              url: e+"/destroy/"+value,

              data: {'id':value},

            })

            //$(tbody).parents('tr').hide();
             $('#employee'+value).remove();



          } else {

           swal("Cancelled", "Your imaginary file is safe :)", "error");

          }

        });

      })
});
var editor = new Simditor({
  textarea: $('#editor')
 });
 var editor = new Simditor({
  textarea: $('#addeditor')
 });

</script>
        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="{{asset('assets/js/main.js')}}"></script>
        <!--/ custom javascripts -->






        
</body>
</html>
