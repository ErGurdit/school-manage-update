<!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                        <ul id="navigation">
                                            <li class=""><a href="{{url('/home')}}" title="Home"  id="@if(Request::is('home')){{'activesidetab'}}@endif"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                                            <!--<li>
                                                <a role="button" tabindex="0" title="Roles" href="{{route('role.index')}}" id="@if(Request::is('role')){{'activesidetab'}}@endif"><i class="fa fa-user"></i> <span>Roles</span> <span class="badge bg-lightred"></a>
                                            </li>-->
                                            <li>
                                                <a role="button" tabindex="0" title="Users" href="{{route('user.index')}}" id="@if(Request::is('user')){{'activesidetab'}}@endif"><i class="fa fa-user"></i> <span>User</span> <span class="badge bg-lightred"></a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" title="Add Subject" href="{{url('/subject')}}" id="@if(Request::is('subject')){{'activesidetab'}}@endif"><i class="fa fa-user"></i> <span>Subject</span> <span class="badge bg-lightred"></a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" title="Schools" id="@if(Request::is('school') || Request::is('school/show') || Request::is('school/showin')){{'activesidetab'}}@endif"><i class="fa fa-list"></i> <span>School</span></a>
                                                <ul>
                                                    <li><a href="{{url('school')}}" title="View School"><i class="fa fa-caret-right"></i> View School</a></li>
                                            <!--li><a href="{{url('subject')}}" title="Add Subject"><i class="fa fa-caret-right"></i> Add Subject</a></li-->

                                                    <!--<li><a href=""><i class="fa fa-caret-right"></i> Add School</a></li>-->
                                                </ul>
                                            </li>
                                             <li>
                                                <a role="button" tabindex="0" title="Offers" id="@if(Request::is('offer') || Request::is('offer/show') || Request::is('offer/showin')){{'activesidetab'}}@endif"><i class="fa fa-list"></i> <span>offer</span></a>
                                                <ul>
                                                    <li><a href="{{url('offer')}}" title="View Offer"><i class="fa fa-caret-right"></i> View offer</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" title="Offers" id="@if(Request::is('payment/school') || Request::is('payment/student') || Request::is('payment/onlinefee')){{'activesidetab'}}@endif"><i class="fa fa-list"></i> <span>Payment</span></a>
                                                <ul>
                                                    <li><a href="{{url('payment/school')}}" title="View Payment"><i class="fa fa-caret-right"></i> View Payment</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" title="Offers" id="@if(Request::is('defaultsetting') || Request::is('announcement') ){{'activesidetab'}}@endif"><i class="fa fa-list"></i> <span>Default Settings</span></a>
                                                <ul>
                                                    <li><a href="{{url('defaultsetting')}}" title="View Payment"><i class="fa fa-caret-right"></i> Defaultsetting</a></li>
                                                    <li><a href="{{url('announcement')}}" title="View Payment"><i class="fa fa-caret-right"></i> Announcement</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="{{url('slider')}}" role="button" tabindex="0" title="Offers" id="@if(Request::is('slider')){{'activesidetab'}}@endif"><i class="fa fa-list"></i> <span>Change Slider</span></a>
                                            </li>
                                            <li>
                                                <a href="{{url('aboutus')}}" role="button" tabindex="0" title="Offers" id="@if(Request::is('aboutus')){{'activesidetab'}}@endif"><i class="fa fa-list"></i> <span>Change About us</span></a>
                                            </li>
                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-pencil"></i> <span>UI Kit</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li><a href="ui-general.html"><i class="fa fa-caret-right"></i> General Elements</a></li>-->
                                            <!--        <li><a href="ui-buttons-icons.html"><i class="fa fa-caret-right"></i> Buttons & Icons</a></li>-->
                                            <!--        <li><a href="ui-typography.html"><i class="fa fa-caret-right"></i> Typography</a></li>-->
                                            <!--        <li><a href="ui-navs.html"><i class="fa fa-caret-right"></i> Navigation & Accordions</a></li>-->
                                            <!--        <li><a href="ui-modals.html"><i class="fa fa-caret-right"></i> Modals</a></li>-->
                                            <!--        <li><a href="ui-tiles.html"><i class="fa fa-caret-right"></i> Tiles</a></li>-->
                                            <!--        <li><a href="ui-portlets.html"><i class="fa fa-caret-right"></i> Portlets</a></li>-->
                                            <!--        <li><a href="ui-grid.html"><i class="fa fa-caret-right"></i> Grid</a></li>-->
                                            <!--        <li><a href="ui-widgets.html"><i class="fa fa-caret-right"></i> Widgets</a></li>-->
                                            <!--        <li><a href="ui-tree.html"><i class="fa fa-caret-right"></i> Tree </a></li>-->
                                            <!--        <li><a href="ui-lists.html"><i class="fa fa-caret-right"></i> Lists</a></li>-->
                                            <!--        <li><a href="ui-alerts.html"><i class="fa fa-caret-right"></i> Alerts & Notifications</a></li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->
                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-shopping-cart"></i> <span>Shop</span> <span class="label label-success">new</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li><a href="shop-orders.html"><i class="fa fa-caret-right"></i> Orders</a></li>-->
                                            <!--        <li><a href="shop-single-order.html"><i class="fa fa-caret-right"></i> Single Order</a></li>-->
                                            <!--        <li><a href="shop-products.html"><i class="fa fa-caret-right"></i> Products</a></li>-->
                                            <!--        <li><a href="shop-single-product.html"><i class="fa fa-caret-right"></i> Single Product</a></li>-->
                                            <!--        <li><a href="shop-invoices.html"><i class="fa fa-caret-right"></i> Invoices</a></li>-->
                                            <!--        <li><a href="shop-single-invoice.html"><i class="fa fa-caret-right"></i> Single Invoice</a></li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->
                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-table"></i> <span>Tables</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li><a href="tables-bootstrap.html"><i class="fa fa-caret-right"></i> Bootstrap Tables</a></li>-->
                                            <!--        <li><a href="tables-datatables.html"><i class="fa fa-caret-right"></i> DataTables</a></li>-->
                                            <!--        <li><a href="tables-footable.html"><i class="fa fa-caret-right"></i> FooTable</a></li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->
                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-desktop"></i> <span>Extra Pages</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li><a href="login.html"><i class="fa fa-caret-right"></i> Login Page</a></li>-->
                                            <!--        <li><a href="signup.html"><i class="fa fa-caret-right"></i> Signup Page</a></li>-->
                                            <!--        <li><a href="forgotpass.html"><i class="fa fa-caret-right"></i> Forgot Password Page</a></li>-->
                                            <!--        <li><a href="page404.html"><i class="fa fa-caret-right"></i> Page 404</a></li>-->
                                            <!--        <li><a href="page500.html"><i class="fa fa-caret-right"></i> Page 500</a></li>-->
                                            <!--        <li><a href="page-offline.html"><i class="fa fa-caret-right"></i> Page Offline</a></li>-->
                                            <!--        <li><a href="locked.html"><i class="fa fa-caret-right"></i> Locked Screen</a></li>-->
                                            <!--        <li><a href="gallery.html"><i class="fa fa-caret-right"></i> Gallery</a></li>-->
                                            <!--        <li><a href="timeline.html"><i class="fa fa-caret-right"></i> Timeline</a></li>-->
                                            <!--        <li><a href="chat.html"><i class="fa fa-caret-right"></i> Chat</a></li>-->
                                            <!--        <li><a href="search-results.html"><i class="fa fa-caret-right"></i> Search Results</a></li>-->
                                            <!--        <li><a href="profile.html"><i class="fa fa-caret-right"></i> Profile Page</a></li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->
                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-delicious"></i> <span>Layouts</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li><a href="layout-boxed.html"><i class="fa fa-caret-right"></i> Boxed layout</a></li>-->
                                            <!--        <li><a href="layout-fullwidth.html"><i class="fa fa-caret-right"></i> Full-width layout</a></li>-->
                                            <!--        <li><a href="layout-sidebar-sm.html"><i class="fa fa-caret-right"></i> Small sidebar</a></li>-->
                                            <!--        <li><a href="layout-sidebar-xs.html"><i class="fa fa-caret-right"></i> Extra-small sidebar</a></li>-->
                                            <!--        <li><a href="layout-offcanvas.html"><i class="fa fa-caret-right"></i> Off-canvas sidebar  <span class="label label-success">new</span></a></li>-->
                                            <!--        <li><a href="layout-hz-menu.html"><i class="fa fa-caret-right"></i> Horizontal menu</a></li>-->
                                            <!--        <li><a href="layout-rtl.html"><i class="fa fa-caret-right"></i> RTL layout</a></li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->
                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-file-o"></i> <span>Front Themes</span> <span class="label label-success">new</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li><a href="http://www.tattek.sk/minovate-corp" target="_blank"><i class="fa fa-caret-right"></i> Corporate</a></li>-->
                                            <!--        <li><a href="http://www.tattek.sk/minovate-commerce" target="_blank"><i class="fa fa-caret-right"></i> Commerce</a></li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->
                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-map-marker"></i> <span>Maps</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li><a href="maps-vector.html"><i class="fa fa-caret-right"></i> Vector Maps</a></li>-->
                                            <!--        <li><a href="maps-google.html"><i class="fa fa-caret-right"></i> Google Maps</a></li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->
                                            <!--<li><a href="calendar.html"><i class="fa fa-calendar-o"></i> <span>Calendar</span> <span class="label label-success">new events</span></a></li>-->
                                            <!--<li><a href="charts.html"><i class="fa fa-bar-chart-o"></i> <span>Charts & Graphs</span></a></li>-->

                                            <!--<li>-->
                                            <!--    <a role="button" tabindex="0"><i class="fa fa-magic"></i> <span>Menu Levels</span></a>-->
                                            <!--    <ul>-->
                                            <!--        <li>-->
                                            <!--            <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 1.1</a>-->
                                            <!--            <ul>-->
                                            <!--                <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.1</a></li>-->
                                            <!--                <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.2</a></li>-->
                                            <!--                <li>-->
                                            <!--                    <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.3</a>-->
                                            <!--                    <ul>-->
                                            <!--                        <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.1</a></li>-->
                                            <!--                        <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.2</a></li>-->
                                            <!--                    </ul>-->
                                            <!--                </li>-->
                                            <!--            </ul>-->
                                            <!--        </li>-->
                                            <!--        <li>-->
                                            <!--            <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 1.2</a>-->
                                            <!--            <ul>-->
                                            <!--                <li>-->
                                            <!--                    <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.1</a>-->
                                            <!--                    <ul>-->
                                            <!--                        <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.1</a></li>-->
                                            <!--                        <li>-->
                                            <!--                            <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.2</a>-->
                                            <!--                            <ul>-->
                                            <!--                                <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 4.1</a></li>-->
                                            <!--                                <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 4.2</a></li>-->
                                            <!--                            </ul>-->
                                            <!--                        </li>-->
                                            <!--                        <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.3</a></li>-->
                                            <!--                    </ul>-->
                                            <!--                </li>-->
                                            <!--                <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.2</a></li>-->
                                            <!--            </ul>-->
                                            <!--        </li>-->
                                            <!--    </ul>-->
                                            <!--</li>-->


                                        </ul>
                                        <!--/ NAVIGATION Content -->


                                    </div>
                                </div>
                            </div>
                            <!--<div class="panel charts panel-default">-->
                            <!--    <div class="panel-heading" role="tab">-->
                            <!--        <h4 class="panel-title">-->
                            <!--            <a data-toggle="collapse" href="#sidebarCharts">-->
                            <!--                Orders Summary <i class="fa fa-angle-up"></i>-->
                            <!--            </a>-->
                            <!--        </h4>-->
                            <!--    </div>-->
                            <!--    <div id="sidebarCharts" class="panel-collapse collapse in" role="tabpanel">-->
                            <!--        <div class="panel-body">-->
                            <!--            <div class="summary">-->

                            <!--                <div class="media">-->
                            <!--                    <a class="pull-right" role="button" tabindex="0">-->
                            <!--                        <span class="sparklineChart"-->
                            <!--                              values="5, 8, 3, 4, 6, 2, 1, 9, 7"-->
                            <!--                              sparkType="bar"-->
                            <!--                              sparkBarColor="#92424e"-->
                            <!--                              sparkBarWidth="6px"-->
                            <!--                              sparkHeight="36px">-->
                            <!--                        Loading...</span>-->
                            <!--                    </a>-->
                            <!--                    <div class="media-body">-->
                            <!--                        This week sales-->
                            <!--                        <h4 class="media-heading">26, 149</h4>-->
                            <!--                    </div>-->
                            <!--                </div>-->

                            <!--                <div class="media">-->
                            <!--                    <a class="pull-right" role="button" tabindex="0">-->
                            <!--                        <span class="sparklineChart"-->
                            <!--                              values="2, 4, 5, 3, 8, 9, 7, 3, 5"-->
                            <!--                              sparkType="bar"-->
                            <!--                              sparkBarColor="#397193"-->
                            <!--                              sparkBarWidth="6px"-->
                            <!--                              sparkHeight="36px">-->
                            <!--                        Loading...</span>-->
                            <!--                    </a>-->
                            <!--                    <div class="media-body">-->
                            <!--                        This week balance-->
                            <!--                        <h4 class="media-heading">318, 651</h4>-->
                            <!--                    </div>-->
                            <!--                </div>-->

                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="panel settings panel-default">-->
                            <!--    <div class="panel-heading" role="tab">-->
                            <!--        <h4 class="panel-title">-->
                            <!--            <a data-toggle="collapse" href="#sidebarControls">-->
                            <!--                General Settings <i class="fa fa-angle-up"></i>-->
                            <!--            </a>-->
                            <!--        </h4>-->
                            <!--    </div>-->
                            <!--    <div id="sidebarControls" class="panel-collapse collapse in" role="tabpanel">-->
                            <!--        <div class="panel-body">-->
                            <!--            <div class="form-group">-->
                            <!--                <div class="row">-->
                            <!--                  <label class="col-xs-8 control-label">Switch ON</label>-->
                            <!--                  <div class="col-xs-4 control-label">-->
                            <!--                    <div class="onoffswitch greensea">-->
                            <!--                      <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch-on" checked="">-->
                            <!--                      <label class="onoffswitch-label" for="switch-on">-->
                            <!--                        <span class="onoffswitch-inner"></span>-->
                            <!--                        <span class="onoffswitch-switch"></span>-->
                            <!--                      </label>-->
                            <!--                    </div>-->
                            <!--                  </div>-->
                            <!--                </div>-->
                            <!--              </div>-->

                            <!--              <div class="form-group">-->
                            <!--                <div class="row">-->
                            <!--                  <label class="col-xs-8 control-label">Switch OFF</label>-->
                            <!--                  <div class="col-xs-4 control-label">-->
                            <!--                    <div class="onoffswitch greensea">-->
                            <!--                      <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch-off">-->
                            <!--                      <label class="onoffswitch-label" for="switch-off">-->
                            <!--                        <span class="onoffswitch-inner"></span>-->
                            <!--                        <span class="onoffswitch-switch"></span>-->
                            <!--                      </label>-->
                            <!--                    </div>-->
                            <!--                  </div>-->
                            <!--                </div>-->
                            <!--              </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->





                <!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->
                <aside id="rightbar">

                    <div role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#users" aria-controls="users" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a></li>
                            <li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab"><i class="fa fa-clock-o"></i></a></li>
                            <li role="presentation"><a href="#friends" aria-controls="friends" role="tab" data-toggle="tab"><i class="fa fa-heart"></i></a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-cog"></i></a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="users">
                                <h6><strong>Online</strong> Users</h6>

                                <ul>

                                    <li class="online">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/ici-avatar.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Ulaanbaatar, Mongolia</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="online">
                                        <div class="media">

                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/arnold-avatar.jpg" alt>
                                            </a>
                                            <span class="badge bg-lightred unread">3</span>

                                            <div class="media-body">
                                                <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Bratislava, Slovakia</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>

                                        </div>
                                    </li>

                                    <li class="online">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/peter-avatar.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Peter <strong>Kay</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Kosice, Slovakia</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="online">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/george-avatar.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">George <strong>McCain</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Prague, Czech Republic</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="busy">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar1.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Lucius <strong>Cashmere</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Wien, Austria</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="busy">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar2.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Jesse <strong>Phoenix</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Berlin, Germany</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                </ul>

                                <h6><strong>Offline</strong> Users</h6>

                                <ul>

                                    <li class="offline">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar4.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Dell <strong>MacApple</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Paris, France</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="offline">
                                        <div class="media">

                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar5.jpg" alt>
                                            </a>

                                            <div class="media-body">
                                                <span class="media-heading">Roger <strong>Flopple</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Rome, Italia</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>

                                        </div>
                                    </li>

                                    <li class="offline">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar6.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Nico <strong>Vulture</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Kyjev, Ukraine</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="offline">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar7.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Bobby <strong>Socks</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Moscow, Russia</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="offline">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar8.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Anna <strong>Opichia</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Budapest, Hungary</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="history">
                                <h6><strong>Chat</strong> History</h6>

                                <ul>

                                    <li class="online">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/ici-avatar.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="busy">
                                        <div class="media">

                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/arnold-avatar.jpg" alt>
                                            </a>
                                            <span class="badge bg-lightred unread">3</span>

                                            <div class="media-body">
                                                <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                                <small>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>

                                        </div>
                                    </li>

                                    <li class="offline">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/peter-avatar.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Peter <strong>Kay</strong></span>
                                                <small>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit </small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="friends">
                                <h6><strong>Friends</strong> List</h6>
                                <ul>

                                    <li class="online">
                                        <div class="media">

                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/arnold-avatar.jpg" alt>
                                            </a>
                                            <span class="badge bg-lightred unread">3</span>

                                            <div class="media-body">
                                                <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Bratislava, Slovakia</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>

                                        </div>
                                    </li>

                                    <li class="offline">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar8.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Anna <strong>Opichia</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Budapest, Hungary</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="busy">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/random-avatar1.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Lucius <strong>Cashmere</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Wien, Austria</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="online">
                                        <div class="media">
                                            <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                                <img class="media-object img-circle" src="assets/images/ici-avatar.jpg" alt>
                                            </a>
                                            <div class="media-body">
                                                <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                                <small><i class="fa fa-map-marker"></i> Ulaanbaatar, Mongolia</small>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="settings">
                                <h6><strong>Chat</strong> Settings</h6>

                                <ul class="settings">

                                    <li>
                                        <div class="form-group">
                                            <label class="col-xs-8 control-label">Show Offline Users</label>
                                            <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="show-offline" checked="">
                                                    <label class="onoffswitch-label" for="show-offline">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="form-group">
                                            <label class="col-xs-8 control-label">Show Fullname</label>
                                            <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="show-fullname">
                                                    <label class="onoffswitch-label" for="show-fullname">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="form-group">
                                            <label class="col-xs-8 control-label">History Enable</label>
                                            <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="show-history" checked="">
                                                    <label class="onoffswitch-label" for="show-history">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="form-group">
                                            <label class="col-xs-8 control-label">Show Locations</label>
                                            <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="show-location" checked="">
                                                    <label class="onoffswitch-label" for="show-location">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="form-group">
                                            <label class="col-xs-8 control-label">Notifications</label>
                                            <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="show-notifications">
                                                    <label class="onoffswitch-label" for="show-notifications">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="form-group">
                                            <label class="col-xs-8 control-label">Show Undread Count</label>
                                            <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="show-unread" checked="">
                                                    <label class="onoffswitch-label" for="show-unread">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>

                    </div>

                </aside>
                <!--/ RIGHTBAR Content -->




            </div>
            <!--/ CONTROLS Content -->
