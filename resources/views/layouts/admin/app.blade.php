<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>School Management - Admin Dashboard</title>
        <link rel="icon" type="image/ico" href="{{asset('assets/images/favicon.ico')}}" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/animsition/css/animsition.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/animsition/css/animsition.min.css')}}">
        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
      
         @if(Request::is('home'))
        <link rel="stylesheet" href="{{asset('assets/js/vendor/daterangepicker/daterangepicker-bs3.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/morris/morris.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/owl-carousel/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/owl-carousel/owl.theme.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/rickshaw/rickshaw.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/datatables.bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/chosen/chosen.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/summernote/summernote.css')}}">
         @endif
         
         @if(Request::is('role') || Request::is('user'))
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/datatables.bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/colorpicker/css/bootstrap-colorpicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/touchspin/jquery.bootstrap-touchspin.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/chosen/chosen.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/summernote/summernote.css')}}">
        
          @endif
          
          @if(Request::is('school'))
         <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/datatables.bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/ColVis/css/dataTables.colVis.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.min.css')}}">
        <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
          @endif
      <!-- project main css files -->
      <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="{{asset('assets/css/switch/on-off-switch.css')}}">
        <link href="{{asset('assets/js/jquery-customselect.css')}}" rel='stylesheet' />
        <!--/ stylesheets -->

        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script src="{{asset('assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
        <!--/ modernizr -->




</head>
<body id="minovate" class="appWrapper">
    
  
    @if(Auth::check())
    @include('layouts.admin.header')
    @include('layouts.admin.sidebar')
    @endif
    @yield('content')
   
 <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
    <!-- JavaScripts -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('assets/js/jquery-customselect.js')}}"></script>
    <script src="{{asset('assets/js/switch/on-off-switch.js')}}"></script>
    <script src="{{asset('assets/js/switch/on-off-switch-onload.js')}}"></script>
    <script src="{{asset('assets/js/switch/bootstrap-switch.js')}}"></script>
    
    @if(Request::is('login'))
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @endif
    @if(Request::is('register') || Request::is('home') || Request::is('role') || Request::is('user') || Request::is('school'))
    <script>window.jQuery || document.write('<script src="{{asset("assets/js/vendor/jquery/jquery-1.11.2.min.js")}}"><\/script>')</script>

        <script src="{{asset('assets/js/vendor/bootstrap/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/jRespond/jRespond.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/sparkline/jquery.sparkline.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/slimscroll/jquery.slimscroll.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/animsition/js/jquery.animsition.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/screenfull/screenfull.min.js')}}"></script>
     @endif
     
        @if(Request::is('register'))
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>
        @endif
       
       @if(Request::is('home'))
        
        <script src="{{asset('assets/js/vendor/d3/d3.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/d3/d3.layout.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/rickshaw/rickshaw.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/daterangepicker/moment.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/daterangepicker/daterangepicker.js')}}"></script>

        <script src="{{asset('assets/js/vendor/screenfull/screenfull.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/flot/jquery.flot.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/flot-spline/jquery.flot.spline.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/easypiechart/jquery.easypiechart.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/raphael/raphael-min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/morris/morris.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/owl-carousel/owl.carousel.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/dataTables.bootstrap.js')}}"></script>

        <script src="{{asset('assets/js/vendor/chosen/chosen.jquery.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/summernote/summernote.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/coolclock/coolclock.js')}}"></script>
        <script src="{{asset('assets/js/vendor/coolclock/excanvas.js')}}"></script>
        
        <!--/ vendor javascripts -->
        <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
      <script>
            $(window).load(function(){
                // Initialize Statistics chart
                var data = [{
                    data: [[1,15],[2,40],[3,35],[4,39],[5,42],[6,50],[7,46],[8,49],[9,59],[10,60],[11,58],[12,74]],
                    label: 'Unique Visits',
                    points: {
                        show: true,
                        radius: 4
                    },
                    splines: {
                        show: true,
                        tension: 0.45,
                        lineWidth: 4,
                        fill: 0
                    }
                }, {
                    data: [[1,50],[2,80],[3,90],[4,85],[5,99],[6,125],[7,114],[8,96],[9,130],[10,145],[11,139],[12,160]],
                    label: 'Page Views',
                    bars: {
                        show: true,
                        barWidth: 0.6,
                        lineWidth: 0,
                        fillColor: { colors: [{ opacity: 0.3 }, { opacity: 0.8}] }
                    }
                }];

                var options = {
                    colors: ['#e05d6f','#61c8b8'],
                    series: {
                        shadowSize: 0
                    },
                    legend: {
                        backgroundOpacity: 0,
                        margin: -7,
                        position: 'ne',
                        noColumns: 2
                    },
                    xaxis: {
                        tickLength: 0,
                        font: {
                            color: '#fff'
                        },
                        position: 'bottom',
                        ticks: [
                            [ 1, 'JAN' ], [ 2, 'FEB' ], [ 3, 'MAR' ], [ 4, 'APR' ], [ 5, 'MAY' ], [ 6, 'JUN' ], [ 7, 'JUL' ], [ 8, 'AUG' ], [ 9, 'SEP' ], [ 10, 'OCT' ], [ 11, 'NOV' ], [ 12, 'DEC' ]
                        ]
                    },
                    yaxis: {
                        tickLength: 0,
                        font: {
                            color: '#fff'
                        }
                    },
                    grid: {
                        borderWidth: {
                            top: 0,
                            right: 0,
                            bottom: 1,
                            left: 1
                        },
                        borderColor: 'rgba(255,255,255,.3)',
                        margin:0,
                        minBorderMargin:0,
                        labelMargin:20,
                        hoverable: true,
                        clickable: true,
                        mouseActiveRadius:6
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%s: %y',
                        defaultTheme: false,
                        shifts: {
                            x: 0,
                            y: 20
                        }
                    }
                };

                var plot = $.plot($("#statistics-chart"), data, options);

                $(window).resize(function() {
                    // redraw the graph in the correctly sized div
                    plot.resize();
                    plot.setupGrid();
                    plot.draw();
                });
                // * Initialize Statistics chart

                //Initialize morris chart
                Morris.Donut({
                    element: 'browser-usage',
                    data: [
                        {label: 'Chrome', value: 25, color: '#00a3d8'},
                        {label: 'Safari', value: 20, color: '#2fbbe8'},
                        {label: 'Firefox', value: 15, color: '#72cae7'},
                        {label: 'Opera', value: 5, color: '#d9544f'},
                        {label: 'Internet Explorer', value: 10, color: '#ffc100'},
                        {label: 'Other', value: 25, color: '#1693A5'}
                    ],
                    resize: true
                });
                //*Initialize morris chart


                // Initialize owl carousels
                $('#todo-carousel, #feed-carousel, #notes-carousel').owlCarousel({
                    autoPlay: 5000,
                    stopOnHover: true,
                    slideSpeed : 300,
                    paginationSpeed : 400,
                    singleItem : true,
                    responsive: true
                });

                $('#appointments-carousel').owlCarousel({
                    autoPlay: 5000,
                    stopOnHover: true,
                    slideSpeed : 300,
                    paginationSpeed : 400,
                    navigation: true,
                    navigationText : ['<i class=\'fa fa-chevron-left\'></i>','<i class=\'fa fa-chevron-right\'></i>'],
                    singleItem : true
                });
                //* Initialize owl carousels


                // Initialize rickshaw chart
                var graph;

                var seriesData = [ [], []];
                var random = new Rickshaw.Fixtures.RandomData(50);

                for (var i = 0; i < 50; i++) {
                    random.addData(seriesData);
                }

                graph = new Rickshaw.Graph( {
                    element: document.querySelector("#realtime-rickshaw"),
                    renderer: 'area',
                    height: 133,
                    series: [{
                        name: 'Series 1',
                        color: 'steelblue',
                        data: seriesData[0]
                    }, {
                        name: 'Series 2',
                        color: 'lightblue',
                        data: seriesData[1]
                    }]
                });

                var hoverDetail = new Rickshaw.Graph.HoverDetail( {
                    graph: graph,
                });

                graph.render();

                setInterval( function() {
                    random.removeData(seriesData);
                    random.addData(seriesData);
                    graph.update();

                },1000);
                //* Initialize rickshaw chart

                //Initialize mini calendar datepicker
                $('#mini-calendar').datetimepicker({
                    inline: true
                });
                //*Initialize mini calendar datepicker


                //todo's
                $('.widget-todo .todo-list li .checkbox').on('change', function() {
                    var todo = $(this).parents('li');

                    if (todo.hasClass('completed')) {
                        todo.removeClass('completed');
                    } else {
                        todo.addClass('completed');
                    }
                });
                //* todo's


                //initialize datatable
                $('#project-progress').DataTable({
                    "aoColumnDefs": [
                      { 'bSortable': false, 'aTargets': [ "no-sort" ] }
                    ],
                });
                //*initialize datatable

                //load wysiwyg editor
                $('#summernote').summernote({
                    toolbar: [
                        //['style', ['style']], // no style button
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        //['insert', ['picture', 'link']], // no insert buttons
                        //['table', ['table']], // no table button
                        //['help', ['help']] //no help button
                    ],
                    height: 143   //set editable area's height
                });
                //*load wysiwyg editor
            });
        </script>
        <!--/ Page Specific Scripts -->
        @endif
        <?php $url = url()->current();
         ?>
        @if(Request::is('role') || Request::is('user'))
        <script src="{{asset('assets/js/vendor/slider/bootstrap-slider.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/daterangepicker/moment.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/chosen/chosen.jquery.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/filestyle/bootstrap-filestyle.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/summernote/summernote.min.js')}}"></script>
        
        <script src="{{asset('assets/js/vendor/parsley/parsley.min.js')}}"></script>
        
        
                <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
        <script>
            $(window).load(function(){
                $('#ex1').slider({
                    formatter: function(value) {
                        return 'Current value: ' + value;
                    }
                });
                $("#ex1").on("slide", function(slideEvt) {
                    $("#ex1SliderVal").text(slideEvt.value);
                });

                $("#ex2, #ex3, #ex4, #ex5").slider();

                //load wysiwyg editor
                $('#summernote').summernote({
                    height: 200   //set editable area's height
                });
                //*load wysiwyg editor
            });
            
            $('#form2Submit').on('click', function(){
                    $('#form2').submit();
                });
                // $('#exampleModal').close()
        </script>
        <!--/ Page Specific Scripts -->
        @endif
        
        @if(Request::is('school'))
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.js"></script>
        <script src="{{asset('assets/js/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/ColVis/js/dataTables.colVis.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/dataTables.bootstrap.js')}}"></script>
        <script type="text/javascript" src="http://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="http://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <script type="text/javascript" src="{{asset('assets/js/jquery.tabledit.min.js')}}"></script>
        
        
        
        
        
            <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
<script>

  $('#example-2').Tabledit({
                columns: {
                    identifier:[0,'name'],
                    editable: [[1, 'principal_name'],[2, 'email'],[4,'mobile'],[5,'timeperiod']]
                },
                // activate focus on first input of a row when click in save button
autoFocus: false,

// hide the column that has the identifier
hideIdentifier: false,

// activate edit button instead of spreadsheet style
editButton: true,

// activate delete button
deleteButton: true,

// activate save button when click on edit button
saveButton: true,
// activate save button when click on edit button
cancelButton: true,

// activate restore button to undo delete action
restoreButton: false,
// trigger to change for edit mode.
// e.g. 'dblclick'
eventType: 'click',
// // class for toolbar
toolbarClass: 'btn-toolbar',

// class for buttons group
groupClass: 'btn-group btn-group-sm',
dangerClass: 'danger',

buttons: {
  edit: {
    class: 'btn btn-sm btn-defaul editable',
    html: '<span class="glyphicon glyphicon-pencil"></span>',
    action: 'edit'
  },
  delete: {
    class: 'btn btn-sm btn-default del',
    html: '<span class="glyphicon glyphicon-trash"></span>',
    action: 'delete'
  },
  save: {
    class: 'btn btn-sm btn-success savebutton',
    html: '<i class="fa fa-check" style="font-size:10px;"></i>'
  },
  restore: {
    class: 'btn btn-sm btn-warning',
    html: 'Restore',
    action: 'false'
  },
  confirm: {
    class: 'btn btn-sm btn-danger btn-delete example-2',
    html: 'confirm'
  }
},
});
$( ".savebutton" ).bind( "click", function() {
  var str = $(this).parent().parent().prev().attr('id');
  var res = str.split("-");
  saveAsNewName(res[1]);
});
$( ".btn-delete" ).bind( "click", function() {
  var str = $(this).parent().parent().prev().attr('id');
  var res = str.split("-");
  saveAsNewName(res[1]);
});
    //number validation
    $('.number-only').keypress(function(e) {
	if(isNaN(this.value+""+String.fromCharCode(e.charCode))) return false;
  })
  .on("cut copy paste",function(e){
	e.preventDefault();
  });
  //email validate
  var sEmail = $('#email').val();
  function validateEmail(sEmail) {
  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
  if (filter.test(sEmail)) {
   return true;
  }
  else {
  return false;
  }
  }
  //select with searchh
    $(function() {
        $("#country").customselect();
    });
      
    //dataupdate
    function saveAsNewName(id) {
         
        
    var values = {
            'str': $('td#editname-'+id +' input').val(),
            'str1': $('td#editprincipal_name-'+id +' input').val(),
            'str2': $('td#editemail-'+id +' input').val(),
            'str3': $('td#editmobile-'+id +' input').val(),
            'str4': $('td#editdaterange-'+id +' input').val(),
            'str5': $('td#editphone-'+id +' input').val(),
            'str6': $('td#editpincode-'+id +' input').val(),
            'str7': $('td#editaddress-'+id +' input').val(),
            'str8': $('td#editpayment-'+id +' input').val(),
            
            
    };
    $.ajax({
        url: "/school/update/"+id,
        type: "POST",
        data: values,
    });


}
//delete
$(".btn-delete").click(function() {
         var str = $(this).parent().parent().prev().attr('id');
        var res = str.split("-");
        var value = res[1];
       
   var url = "/school/destroy/"+value;
        
        $.ajax({

          type: 'GET',

          url: url,
          
          data: value,

          success : function(data){

            $('.example-2'+value).remove()

            console.log(data);

          }

        })


});
$(".editstatus").bootstrapSwitch();
function changestatus(id,e){
        var values = $(e).attr('rel');
        if(values == 0){
        $(e).attr('rel','1');
        }else{
        $(e).attr('rel','0');
        }
       
            
       
  $.ajax({
        url: "/school/status/"+id,
        type: "POST",
        data: {'status':values},
         success : function(data){
             console.log(data);
         }
    });
}
</script>
        <!--/ Page Specific Scripts -->
        <!--World Data-->
        <script type="text/javascript">

    $('#country').change(function(){

    var countryID = $(this).val();

    if(countryID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-state-list')}}?country_id="+countryID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#state").empty();

                $("#state").append('<option>Select</option>');

                $.each(res,function(key,value){

                    $("#state").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#state").empty();

            }

           }

        });

    }else{

        $("#state").empty();

        $("#city").empty();

    }

   });

    $('#state').on('change',function(){

    var stateID = $(this).val();

    if(stateID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-city-list')}}?state_id="+stateID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#city").empty();

                $.each(res,function(key,value){

                    $("#city").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#city").empty();

            }

           }

        });

    }else{

        $("#city").empty();

    }



   });
//validatie data
 $("form").validate({
          
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      },
      submitHandler: function(form) {
          form.submit();
      }
  });
  
  //Delete
        $(document).ready(function(){

//       $('tbody').delegate('.btn-delete','click',function(){
// var str = $(this).parent().parent().prev().attr('id');
//   var res = str.split("-");
//         var value=res[1];
//         console.log(value);
        
        
        
//         var url = "/school/destroy/"+value;
        
//         $.ajax({

//           type: 'GET',

//           url: url,
          
//           data: value,

//           success : function(data){

//             $('#example-2'+value).remove()

//             console.log(data);

//           }

//         })

//       });

//       $('body').on('click','.btn-delete',function(){
       
        

//         that = $(this);
        
//         value = $(this).data('id');

//         swal({

//           title: "Are you sure?",

//           text: "Do you want to delete!",

//           type: "warning",

//           showCancelButton: true,

//           confirmButtonColor: "#DD6B55",

//           confirmButtonText: "Yes, delete it!",

//           cancelButtonText: "No",

//           closeOnConfirm: false,

//           closeOnCancel: false

//         },

//         function(isConfirm){

//           if (isConfirm) {

//             swal("Deleted!", "School deleted.", "success");
            
            

//             $.ajax({

//               type: 'GET',

//               url: url,

//               data: value,

//             })

//             $(that).parents('tr').hide();



//           } else {

//           swal("Cancelled", "Your imaginary file is safe :)", "error");

//           }

//         });

//       })
      
     
$('input.trialperiod').change(function(){
		if ($(this).is(':checked')) $('div.range').show();
		else $('div.range').hide();
	}).change();
    });
    
</script>
<script type="text/javascript">
$(document).ready(function() {
$(function() {
    $('input[name="daterange"]').daterangepicker();
});
});
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
    // $("#example-2").DataTable( {
    //     "scrollX": true,
    // } );
    
    $('.tabledit-edit-button').click(function() {
        $('.tabledit-edit-button').toggle();
        $('.tabledit-delete-button').toggle();
    });
    $('.tabledit-save-button').click(function() {
        $('.tabledit-save-button').toggle();
        $('.tabledit-edit-button').toggle();
        $('.tabledit-delete-button').toggle();
        
    });
//     $('.savebutton').click(function(){
//         $('.btn-toolbar').append('<button class="cross" onclick="cancel()"><i class="fa fa-close" style="font-size: 10px;"></i></button>');

// $(function cancel(){
//         alert('hello');
//     });
//     });
    
    
    
    
    //     // $('.tabledit-edit-button').toggle('button');
    //     $('.tabledit-delete-button').toggle('button');
    //     $('.savebutton').toggle('button');
    // $('.savebutton').click(function() {
    //     $('.tabledit-edit-button').toggle('button');
    //     $('.tabledit-delete-button').toggle('button');
    // })
    
   // $('.btn-toolbar').append('<button class="cross"><i class="fa fa-close" style="font-size: 10px;"></i></button>');

});

</script>

        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="{{asset('assets/js/main.js')}}"></script>
        <!--/ custom javascripts -->






        
</body>
</html>
