<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>School Management - Admin Dashboard</title>
        <link rel="icon" type="image/ico" href="{{asset('assets/images/favicon.ico')}}" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">




        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/animsition/css/animsition.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

        <!-- project main css files -->
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <!--/ stylesheets -->



        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script src="{{asset('assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
        <!--/ modernizr -->




    </head>
<body id="minovate" class="appWrapper">
    
    <!--<nav class="navbar navbar-default navbar-static-top">-->
    <!--    <div class="container">-->
    <!--        <div class="navbar-header">-->

                <!-- Collapsed Hamburger -->
                <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">-->
                <!--    <span class="sr-only">Toggle Navigation</span>-->
                <!--    <span class="icon-bar"></span>-->
                <!--    <span class="icon-bar"></span>-->
                <!--    <span class="icon-bar"></span>-->
                <!--</button>-->

                <!-- Branding Image -->
    <!--            <a class="navbar-brand" href="{{ url('/') }}">-->
    <!--                School Management-->
    <!--            </a>-->
    <!--        </div>-->

    <!--        <div class="collapse navbar-collapse" id="app-navbar-collapse">-->
                <!-- Left Side Of Navbar -->
                <!--<ul class="nav navbar-nav">-->
                <!--    <li><a href="{{ url('/home') }}">Home</a></li>-->
                <!--</ul>-->

                <!-- Right Side Of Navbar -->
    <!--            <ul class="nav navbar-nav navbar-right">-->
                    <!-- Authentication Links -->
    <!--                @if (Auth::guest())-->
    <!--                    <li><a href="{{ url('/login') }}">Login</a></li>-->
    <!--                    <li><a href="{{ url('/register') }}">Register</a></li>-->
    <!--                @else-->
    <!--                    <li class="dropdown">-->
    <!--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">-->
    <!--                            {{ Auth::user()->name }} <span class="caret"></span>-->
    <!--                        </a>-->

    <!--                        <ul class="dropdown-menu" role="menu">-->
    <!--                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>-->
    <!--                        </ul>-->
    <!--                    </li>-->
    <!--                @endif-->
    <!--            </ul>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</nav>-->
    @include('layouts.admin.header')
    @include('layouts.admin.sidebar')
    @yield('content')
   
   <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{asset("assets/js/vendor/jquery/jquery-1.11.2.min.js")}}"><\/script>')</script>

        <script src="{{asset('assets/js/vendor/bootstrap/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/jRespond/jRespond.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/sparkline/jquery.sparkline.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/slimscroll/jquery.slimscroll.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/animsition/js/jquery.animsition.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/screenfull/screenfull.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/parsley/parsley.min.js')}}"></script>
        <!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="{{asset('assets/js/main.js')}}"></script>
        <!--/ custom javascripts -->






        <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
        <script>
            $(window).load(function(){
                $('#form1Submit').on('click', function(){
                    $('#form1').submit();
                });

                $('#form2Submit').on('click', function(){
                    $('#form2').submit();
                });

                $('#form3Submit').on('click', function(){
                    $('#form3').submit();
                });

                $('#form4Submit').on('click', function(){
                    $('#form4').submit();
                });
            });
        </script>
        <!--/ Page Specific Scripts -->
        <script type="text/javascript">

    $('#country').change(function(){

    var countryID = $(this).val();

    if(countryID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-state-list')}}?country_id="+countryID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#state").empty();

                $("#state").append('<option>Select</option>');

                $.each(res,function(key,value){

                    $("#state").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#state").empty();

            }

           }

        });

    }else{

        $("#state").empty();

        $("#city").empty();

    }

   });

    $('#state').on('change',function(){

    var stateID = $(this).val();

    if(stateID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-city-list')}}?state_id="+stateID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#city").empty();

                $.each(res,function(key,value){

                    $("#city").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#city").empty();

            }

           }

        });

    }else{

        $("#city").empty();

    }



   });

</script>
        
</body>
</html>
