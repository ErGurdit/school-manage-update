<a class="list-group-item dropdown-item" href="{{url('thread/show',$notification->data['thread']['id'])}}" role="menuitem" id="markasread" onclick="markNotificationAsRead({{count(auth()->user()->unreadNotifications)}})">
    <div class="media">
        <div class="pr-10">
            <i class="icon md-receipt bg-red-600 white icon-circle" aria-hidden="true"></i>
        </div>
        <div class="media-body">
            <h6 class="media-heading">{{$notification->data['user']['name']}} comment on {{$notification->data['thread']['amount']}}</h6>
            <time class="media-meta" datetime="2017-06-12T20:50:48+08:00">5 hours ago</time>
        </div>
    </div>
</a>