<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>School Management System</title>
      <!-- bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"  type="text/css">
      <!-- font-awesome CSS -->
      <link href="http://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link href="/theme/assets/css/font-awesome-new.min.css" rel="stylesheet">
      <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css" type="text/css">
      <!-- style CSS -->
      <link href="/theme/assets/css/style.css"  rel="stylesheet" type="text/css">
      <link href="/theme/assets/css/responsive.css"  rel="stylesheet" type="text/css">
   </head>
  @yield('content')