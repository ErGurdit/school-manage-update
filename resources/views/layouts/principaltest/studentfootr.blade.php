<!-- Footer -->
  <footer class="site-footer')}}">
    <div class="site-footer-legal')}}">© <?php echo date('Y');?></div>
    <div class="site-footer-right')}}">
     School Management
    </div>
  </footer>
  <!-- Core  -->
<script src="{{asset('principal/assetstest/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/animsition/animsition.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/mousewheel/jquery.mousewheel.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/asscrollbar/jquery-asScrollbar.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/asscrollable/jquery-asScrollable.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/waves/waves.min.js')}}"></script>

<!--Datepicker-->
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js?v4.0.1')}}"></script>


<!-- Plugins -->
  <script src="{{asset('principal/assetstest/global/vendor/select2/select2.full.min.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-select/bootstrap-select.min.js?v4.0.1')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/intro-js/intro.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/screenfull/screenfull.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/slidepanel/jquery-slidePanel.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/formvalidation/formValidation.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/formvalidation/framework/bootstrap4.min.js')}}"></script>

<!-- Data tables-->
<script src="{{asset('principal/assetstest/global/vendor/datatables.net/jquery.dataTables.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-scroller/dataTables.scroller.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-responsive/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/dataTables.buttons.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.html5.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.flash.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.print.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.colVis.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asrange/jquery-asRange.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootbox/bootbox.min.js?v4.0.1')}}"></script>
<!--- Datatable-->
<!-- Scripts -->
<script src="{{asset('principal/assetstest/global/js/Component.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/js/Plugin.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/js/Base.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/js/Config.min.js')}}"></script>

<script src="{{asset('principal/assetstest/assets/js/Section/Menubar.min.js')}}"></script>
<script src="{{asset('principal/assetstest/assets/js/Section/Sidebar.min.js')}}"></script>
<script src="{{asset('principal/assetstest/assets/js/Section/PageAside.min.js')}}"></script>
<script src="{{asset('principal/assetstest/assets/js/Plugin/menu.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('principal/assets/apps/scripts/newcustom.js')}}"></script>


<!-- Config -->
<script src="{{asset('principal/assetstest/global/js/config/colors.min.js')}}"></script>
<script src="{{asset('principal/assetstest/assets/js/config/tour.min.js')}}"></script>
<script>Config.set('assets', '../../assets');</script>

<!-- Page -->
<script src="{{asset('principal/assetstest/assets/js/Site.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/js/Plugin/asscrollable.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/js/Plugin/slidepanel.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/js/Plugin/switchery.min.js')}}"></script>

<script src="{{asset('principal/assetstest/assets/examples/js/forms/validation.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/moment/moment.min.js?v4.0.1')}}"></script>
   <script src="{{asset('principal/assetstest/global/vendor/clockpicker/bootstrap-clockpicker.min.js?v4.0.1')}}"></script>
<!-- new wizard js-->
 <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/validation.js')}}"></script>
    <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/additional.js')}}"></script>
    <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/step.js')}}"></script>
       <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/mycustomwizard.js')}}"></script>
<!-- End wizard -->
<script src="{{asset('principal/assetstest/global/js/Plugin/datatables.min.js?v4.0.1')}}"></script>


  <script src="{{asset('principal/assetstest/assets/examples/js/tables/datatable.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/examples/js/uikit/icon.min.js?v4.0.1')}}"></script>

</body>
</html>