<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="apple-touch-icon" href="{{asset('principal/assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('principal/assetstest/assets/images/favicon.ico')}}">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/bootstrap.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/bootstrap-extend.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/css/site.min.css?v4.0.1')}}">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/skintools.min.css?v4.0.1')}}">
  <script src="{{asset('principal/assetstest/assets/js/Plugin/skintools.min.js?v4.0.1')}}"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/animsition/animsition.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/asscrollable/asScrollable.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/switchery/switchery.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/intro-js/introjs.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/slidepanel/slidePanel.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/flag-icon-css/flag-icon.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/waves/waves.min.css?v4.0.1')}}">
  
  <!-- Page -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/examples/css/apps/message.min.css?v4.0.1')}}">

  <!-- Fonts -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/material-design/material-design.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/brand-icons/brand-icons.min.css?v4.0.1')}}">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">
  <link rel="stylesheet" href="{{asset('principal/assetstest/messagecustom.css')}}">


  <!--[if lt IE 9]>
    <script src="{{asset('principal/assetstest/global/vendor/html5shiv/html5shiv.min.js?v4.0.1')}}"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="{{asset('principal/assetstest/global/vendor/media-match/media.match.min.js?v4.0.1')}}"></script>
    <script src="{{asset('principal/assetstest/global/vendor/respond/respond.min.js?v4.0.1')}}"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="{{asset('principal/assetstest/global/vendor/breakpoints/breakpoints.min.js?v4.0.1')}}"></script>
  <script>
    Breakpoints();
  </script>

</head>
<body class="animsition dashboard">
      <div id="app">
            @if(Auth::check())
                @include('layouts.principaltest.messageheader')
                @include('layouts.principaltest.sidebar')
            @endif
            @yield('content')
            @include('layouts.principaltest.messagefooter')