<div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <div>
        <div>
            <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-item active">
              <a href="{{url('home')}}" id="@if(Request::is('home')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                  <span class="site-menu-title">Dashboard</span>
              </a>
            </li>
            @if(Entrust::hasRole('principal'))
            <!--<li class="site-menu-item has-sub">
              <a href="{{route('role.index')}}">
                                <i class="site-menu-icon md-view-compact" aria-hidden="true"></i>
                                <span class="site-menu-title">Roles</span>
                            </a>-->
              <!--<ul class="site-menu-sub">-->
              <!--  <li class="site-menu-item">-->
              <!--    <a href="{{route('role.index')}}" id="@if(Request::is('role')){{'activesidetab'}}@endif">-->
              <!--      <span class="site-menu-title">New Role</span>-->
              <!--    </a>-->
              <!--  </li>-->
                <!--<li class="site-menu-item">-->
                <!--  <a href="{{route('user.index')}}" id="@if(Request::is('user')){{'activesidetab'}}@endif">-->
                <!--    <span class="site-menu-title">Assign Role</span>-->
                <!--  </a>-->
                <!--</li>-->
              <!--</ul>-->
            <!--</li>-->
            <li class="site-menu-item">
              <a href="{{url('class')}}" id="@if(Request::is('class')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-google-pages" aria-hidden="true"></i>
                                <span class="site-menu-title">Class</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('staff')}}" id="@if(Request::is('staff')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                                <span class="site-menu-title">Staff</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('slider')}}" id="@if(Request::is('slider')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-format-color-fill" aria-hidden="true"></i>
                                <span class="site-menu-title">Slider</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('announcement')}}" id="@if(Request::is('announcement')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-puzzle-piece" aria-hidden="true"></i>
                                <span class="site-menu-title">Announcement</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('expenses')}}" id="@if(Request::is('expenses')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-widgets" aria-hidden="true"></i>
                                <span class="site-menu-title">Expenses</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('attendence')}}" id="@if(Request::is('attendence')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-comment-alt-text" aria-hidden="true"></i>
                                <span class="site-menu-title">Attendence</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('holiday')}}" id="@if(Request::is('holiday')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-border-all" aria-hidden="true"></i>
                                <span class="site-menu-title">Holiday</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('exams')}}" id="@if(Request::is('exams')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                                <span class="site-menu-title">Exams</span>
                            </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('certificate')}}" id="@if(Request::is('certificate')){{'activesidetab'}}@endif">
                                <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                                <span class="site-menu-title">Certificate</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('assignsubject')}}" id="@if(Request::is('assignsubject')){{'activesidetab'}}@endif">
                <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                <span class="site-menu-title">Assign Subject</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('addgalleryimage')}}" id="@if(Request::is('addgalleryimage')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Add gallery image</span>
              </a>
            </li>
            @endif
            @if(Entrust::hasRole('feesdepartment'))
            <li class="site-menu-item">
              <a href="{{url('fee')}}" id="@if(Request::is('fee')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">All Admission</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('feestatus')}}" id="@if(Request::is('feestatus')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">New Admission</span>
              </a>
            </li>
             <li class="site-menu-item">
              <a href="{{url('student')}}" id="@if(Request::is('student')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Add Student</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('feesubmit/create')}}" id="@if(Request::is('feesubmit')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Fee Submission</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('feestatus/show')}}" id="@if(Request::is('feestatus/show')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Fee Status</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('daybook/show')}}" id="@if(Request::is('daybook/show')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Day Book</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('salary')}}" id="@if(Request::is('salary')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Salary</span>
              </a>
            </li>
            @endif
            @if(Entrust::hasRole('accountant'))
            <li class="site-menu-item">
              <a href="{{url('ledger')}}" id="@if(Request::is('ledger')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Ledger</span>
              </a>
            </li>
             <li class="site-menu-item">
              <a href="{{url('staffattendance')}}" id="@if(Request::is('staffattendance')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Staff attendance</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('account')}}" id="@if(Request::is('account')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Maintaion Fee</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('expenses')}}" id="@if(Request::is('expenses')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Expenses</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('busroute')}}" id="@if(Request::is('busroute')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Bus Route</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('othercharges')}}" id="@if(Request::is('othercharges')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Other Charges</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('salaries')}}" id="@if(Request::is('salaries')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Salaries</span>
              </a>
            </li>
            @endif
            @if(Entrust::hasRole('teacher'))
            
            <li class="site-menu-item">
              <a href="{{url('homework')}}" id="@if(Request::is('homework')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Homework</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('salary')}}" id="@if(Request::is('salary')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Salary</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('studattendance')}}" id="@if(Request::is('studattendance')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Student attendance</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('datesheet')}}" id="@if(Request::is('datesheet')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Datesheet</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('examteacher')}}" id="@if(Request::is('examteacher')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Exam</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('salary')}}" id="@if(Request::is('salary')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Salary received dates</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a href="{{url('test')}}" id="@if(Request::is('test')){{'activesidetab'}}@endif">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Test</span>
              </a>
            </li>
            @endif
          </ul>
          </div></div></div>
  </div>
  <script type="text/javascript">
    $(".navbar-toggler").click(function(){
      $(".dashboard").toggleClass("site-menubar-unfold");
      /*$('#menuid').toggleClass('site-menubar-hide');
      $("#menuid").toggleClass("site-menubar-unfold");
      $("toggleMenu").toggleClass("unfolded");
      $("toggleMenu").toggleClass("hided");*/
    });
    $(window).load(function(){
      alert("test");
    });
    $(document).ready(function(){
      alert("ready");
    });
  </script>