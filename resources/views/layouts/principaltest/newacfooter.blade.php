<!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© <?php echo date('Y');?></div>
    <div class="site-footer-right">
     School Management
    </div>
  </footer>
  <!-- Core  -->
  <script src="{{asset('principal/assetstest/global/vendor/babel-external-helpers/babel-external-helpers.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jquery/jquery.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/popper-js/umd/popper.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap/bootstrap.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/animsition/animsition.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/mousewheel/jquery.mousewheel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asscrollbar/jquery-asScrollbar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asscrollable/jquery-asScrollable.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/waves/waves.min.js?v4.0.1')}}"></script>

  <!-- Plugins -->
  <script src="{{asset('principal/assetstest/global/vendor/select2/select2.full.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-select/bootstrap-select.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/switchery/switchery.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/intro-js/intro.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/screenfull/screenfull.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/slidepanel/jquery-slidePanel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/multi-select/jquery.multi-select.js?v4.0.1')}}"></script>

  <!-- Plugins For This Page -->
  <script src="{{asset('principal/assetstest/global/js/Plugin/select2.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/bootstrap-select.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('principal/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('principal/assetstest/global/vendor/formvalidation/formValidation.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/formvalidation/framework/bootstrap.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/matchheight/jquery.matchHeight-min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/jquery-wizard.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/multi-select.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/jquery-placeholder.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/typeahead-js/typeahead.jquery.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jquery-placeholder/jquery.placeholder.min.js?v4.0.1')}}"></script>
  
  <!--Datepicker-->
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js?v4.0.1')}}"></script>


  <script src="{{asset('principal/assetstest/assets/examples/js/forms/advanced.min.js?v4.0.1')}}"></script>
  <!-- Scripts -->
  <script src="{{asset('principal/assetstest/global/js/State.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Component.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Base.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Config.min.js?v4.0.1')}}"></script>

  <script src="{{asset('principal/assetstest/assets/js/Section/Menubar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Section/Sidebar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Section/PageAside.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Plugin/menu.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/newcustom.js')}}"></script>
    <script src="{{asset('js/notifications.js')}}"></script>


  <!-- Config -->
  <script src="{{asset('principal/assetstest/global/js/config/colors.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/config/tour.min.js?v4.0.1')}}"></script>
  <script>
    Config.set('assets', '../assets');
  </script>

  <!-- Page -->
  <script src="{{asset('principal/assetstest/assets/js/Site.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/asscrollable.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/slidepanel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/switchery.min.js?v4.0.1')}}"></script>

  <script src="{{asset('principal/assetstest/global/js/Plugin/jquery-wizard.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/matchheight.min.js?v4.0.1')}}"></script>


  <script src="{{asset('principal/assetstest/assets/examples/js/forms/wizard.min.js?v4.0.1')}}"></script>
  <script type="text/javascript" src="{{asset('principal/assets/apps/scripts/custom.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&language=en"></script>
  <script src="{{asset('principal/assetstest/assets/js/jquery.payform.min.js')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/script.js')}}"></script>
  

  

</body>
<script>
  //country state city
$('#country_list').change(function(){
var countryID = $(this).val();

    if(countryID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-state-list')}}?country_id="+countryID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#state_list").empty();

                $("#state_list").append('<option>Select</option>');

                $.each(res,function(key,value){

                    $("#state_list").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#state_list").empty();

            }

           }

        });

    }else{

        $("#state_list").empty();

        $("#city_list").empty();

    }

   });
$('#state_list').on('change',function(){

    var stateID = $(this).val();

    if(stateID){

        $.ajax({

           type:"GET",

           url:"{{url('/get-city-list')}}?state_id="+stateID,

           dataType: "json",

           success:function(res){

            if(res){

                $("#city_list").empty();

                $.each(res,function(key,value){

                    $("#city_list").append('<option value="'+key+'">'+value+'</option>');

                });



            }else{

               $("#city_list").empty();

            }

           }

        });

    }else{

        $("#city_list").empty();

    }



   });
</script>

</html>