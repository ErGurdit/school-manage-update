 <!-- Footer -->
   <footer class="site-footer">
    <div class="site-footer-legal">© <?php echo date('Y');?> School Management</div>
  </footer>
  <!-- Core  -->
  <script src="{{asset('principal/assetstest/global/vendor/babel-external-helpers/babel-external-helpers.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jquery/jquery.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/popper-js/umd/popper.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap/bootstrap.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/animsition/animsition.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/mousewheel/jquery.mousewheel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asscrollbar/jquery-asScrollbar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asscrollable/jquery-asScrollable.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/waves/waves.min.js?v4.0.1')}}"></script>

  <!-- Plugins -->
  <script src="{{asset('principal/assetstest/global/vendor/switchery/switchery.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/intro-js/intro.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/screenfull/screenfull.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/slidepanel/jquery-slidePanel.min.js?v4.0.1')}}"></script>

  <!-- Plugins For This Page -->
  <script src="{{asset('principal/assetstest/global/vendor/slidepanel/jquery-slidePanel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/autosize/autosize.min.js?v4.0.1')}}"></script>

  <!-- Scripts -->
  <script src="{{asset('principal/assetstest/global/js/State.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Component.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Base.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Config.min.js?v4.0.1')}}"></script>

  <script src="{{asset('principal/assetstest/assets/js/Section/Menubar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Section/Sidebar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Section/PageAside.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Plugin/menu.min.js?v4.0.1')}}"></script>

  <!-- Config -->
  <script src="{{asset('principal/assetstest/global/js/config/colors.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/config/tour.min.js?v4.0.1')}}"></script>
  <script>
    Config.set('assets', 'assets');
  </script>

  <!-- Page -->
  <script src="{{asset('principal/assetstest/assets/js/Site.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/asscrollable.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/slidepanel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/switchery.min.js?v4.0.1')}}"></script>

  <script src="{{asset('principal/assetstest/assets/js/Site.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/App/Message.min.js?v4.0.1')}}"></script>

  <script src="{{asset('principal/assetstest/assets/examples/js/apps/message.min.js?v4.0.1')}}"></script>
  <script type="text/javascript" src="{{asset('js/profile.js')}}"></script>
