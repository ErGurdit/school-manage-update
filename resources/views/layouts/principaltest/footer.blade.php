<!-- Footer -->
  <footer class="site-footer footer-position">
    <div class="site-footer-legal">© <?php echo date('Y');?> School Management</div>
  </footer>
  <!-- Core  -->
  @if(Request::is('slider'))
  @endif
  <script src="{{asset('principal/assetstest/global/vendor/babel-external-helpers/babel-external-helpers.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jquery/jquery.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/popper-js/umd/popper.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap/bootstrap.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/animsition/animsition.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/mousewheel/jquery.mousewheel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asscrollbar/jquery-asScrollbar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asscrollable/jquery-asScrollable.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/waves/waves.min.js?v4.0.1')}}"></script>
 
  <!-- Plugins -->
    <script src="{{asset('principal/assetstest/global/vendor/select2/select2.full.min.js?v4.0.1')}}"></script>

   <script src="{{asset('principal/assetstest/global/vendor/formvalidation/formValidation.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/formvalidation/framework/bootstrap.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/matchheight/jquery.matchHeight-min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/switchery/switchery.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/intro-js/intro.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/screenfull/screenfull.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/slidepanel/jquery-slidePanel.min.js?v4.0.1')}}"></script>
  @if(Request::is('homework'))
  <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
  @endif

  <!-- Plugins For This Page -->
  @if(Request::is('slider') || Request::is('certificate'))
  <script src="{{asset('principal/assetstest/global/vendor/matchheight/jquery.matchHeight-min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jquery-ui/jquery-ui.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-tmpl/tmpl.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-load-image/load-image.all.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-file-upload/jquery.fileupload.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-file-upload/jquery.fileupload-process.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-file-upload/jquery.fileupload-image.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/dropify/dropify.min.js?v4.0.1')}}"></script>
  @else
  @if(Request::is('home'))
  <script src="{{asset('principal/assetstest/global/vendor/chartist/chartist.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jvectormap/jquery-jvectormap.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/matchheight/jquery.matchHeight-min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/peity/jquery.peity.min.js?v4.0.1')}}"></script>
  @endif
  <script src="{{asset('principal/assetstest/global/vendor/jquery-placeholder/jquery.placeholder.min.js?v4.0.1')}}"></script>
  @endif
  @if(Request::is('assignsubject'))
  <script src="{{asset('principal/assetstest/global/vendor/select2/select2.full.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-select/bootstrap-select.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/typeahead-js/typeahead.jquery.min.js?v4.0.1')}}"></script>
  @else
  <!--Markdown editor-->
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/marked/marked.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/to-markdown/to-markdown.js?v4.0.1')}}"></script>
  <!--datatable-->
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net/jquery.dataTables.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-scroller/dataTables.scroller.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-responsive/dataTables.responsive.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/dataTables.buttons.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.html5.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.flash.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.print.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons/buttons.colVis.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/asrange/jquery-asRange.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootbox/bootbox.min.js?v4.0.1')}}"></script>
  <!--enddatatable-->

  <!--Datepicker-->
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js?v4.0.1')}}"></script>
  @endif
  
  <!-- Scripts -->
  <script src="{{asset('principal/assetstest/global/js/State.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Component.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Base.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Config.min.js?v4.0.1')}}"></script>

  <script src="{{asset('principal/assetstest/assets/js/Section/Menubar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Section/Sidebar.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Section/PageAside.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/Plugin/menu.min.js?v4.0.1')}}"></script>
 <script src="{{asset('principal/assetstest/global/vendor/toastr/toastr.min.js?v4.0.1')}}"></script>
  <!-- Config -->
  <script src="{{asset('principal/assetstest/global/js/config/colors.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/js/config/tour.min.js?v4.0.1')}}"></script>
  <!-- Page -->

  <script src="{{asset('principal/assetstest/assets/js/Site.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/asscrollable.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/slidepanel.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/switchery.min.js?v4.0.1')}}"></script>

  <script src="{{asset('principal/assetstest/global/js/Plugin/matchheight.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/jvectormap.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/peity.min.js?v4.0.1')}}"></script>
 
  <script src="{{asset('principal/assetstest/global/js/Plugin/toastr.min.js?v4.0.1')}}"></script>

  @if(Request::is('slider') || Request::is('certificate'))
  <script src="{{asset('principal/assetstest/global/js/Plugin/dropify.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/examples/js/forms/uploads.min.js?v4.0.1')}}"></script>
  @else
  <script src="{{asset('principal/assetstest/assets/examples/js/dashboard/v1.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/jquery-placeholder.min.js?v4.0.1')}}"></script>
  @endif
  @if(Request::is('assignsubject'))
    <script src="{{asset('principal/assetstest/global/js/Plugin/select2.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/bootstrap-select.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/multi-select.min.js?v4.0.1')}}"></script>

  <script src="../assets/examples/js/forms/advanced.min.js?v4.0.1"></script>

  @endif
  @if(Request::is('class'))
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/bootstrap-tokenfield.min.js?v4.0.1')}}"></script>
  @endif
  
  <!--Sweetalert-->
  <script src="{{asset('principal/assetstest/global/vendor/bootbox/bootbox.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/vendor/bootstrap-sweetalert/sweetalert.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/toastr.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/bootbox.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/bootstrap-sweetalert.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/global/js/Plugin/toastr.min.js?v4.0.1')}}"></script>
  <script src="{{asset('principal/assetstest/assets/examples/js/advanced/bootbox-sweetalert.min.js?v4.0.1')}}"></script>

  <!--validate-->
  <script src="{{asset('principal/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('principal/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/formvalidation/0.6.1/js/formValidation.min.js"></script>
  <script type="text/javascript" src="{{asset('principal/assets/apps/scripts/custom.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js"></script>
  <script type="text/javascript" src="{{asset('principal/assets/apps/scripts/newcrop.js')}}"></script>
 <script src="{{asset('principal/assetstest/global/vendor/formvalidation/formValidation.min.js')}}"></script>
<script src="{{asset('principal/assetstest/global/vendor/formvalidation/framework/bootstrap4.min.js')}}"></script>
   <script src="{{asset('principal/assetstest/global/vendor/clockpicker/bootstrap-clockpicker.min.js')}}"></script>

     <script type="text/javascript" src="{{asset('principal/assets/apps/scripts/newcustom.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/notifications.js')}}"></script>
<script src="{{asset('principal/assetstest/assets/examples/js/forms/validation.min.js')}}"></script>
  <script src="{{asset('principal/assetstest/assets/assets/examples/js/forms/wizard.min.js?v4.0.1')}}"></script>


    <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/validation.js')}}"></script>
    <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/additional.js')}}"></script>
    <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/step.js')}}"></script>
       <script src="{{asset('principal/assetstest/global/vendor/jquery-wizard/mycustomwizard.js')}}"></script>

    <!--script src="https://colorlib.com/etc/bwiz/colorlib-wizard-14/js/main.js"></script-->
  @if(Request::is('messages'))
  <script type="text/javascript" src="{{asset('js/profile.js')}}"></script>
  @endif
  <!--<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/main.js')}}"></script>-->
  
  <!--@yield('js')-->
  </div>
</body>

</html>