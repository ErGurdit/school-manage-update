<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>School Management</title>

  <link rel="apple-touch-icon" href="{{asset('principal/assetstest/assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('principal/assetstest/assets/images/favicon.ico')}}">
    <!--Datepicker-->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css?')}}">
<link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/material-design/material-design.min.css')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/brand-icons/brand-icons.min.css?v4.0.1')}}">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">
 <!-- new css of wizard form feestatus-->
  <link href="{{asset('principal/assets/apps/css/newcustom.css')}}" rel="stylesheet" type="text/css" />
<!--end -->

    <!-- Stylesheets -->
      <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/clockpicker/clockpicker.min.css')}}">

    <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/assets/css/site.min.css')}}">
    <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/skintools.min.css?v4.0.1')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <script src="{{asset('principal/assetstest/assets/js/Plugin/skintools.min.js?v4.0.1')}}"></script>

    <!-- Plugins -->
        <script src="{{asset('principal/assetstest/global/js/Plugin/clockpicker.min.js')}}"></script>

      <script src="{{asset('principal/assetstest/global/js/Plugin/select2.min.js?v4.0.1')}}"></script>
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/animsition/animsition.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/asscrollable/asScrollable.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/switchery/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/intro-js/introjs.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/slidepanel/slidePanel.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/flag-icon-css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/waves/waves.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/formvalidation/formValidation.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/assets/examples/css/forms/validation.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-bs4/dataTables.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.min.css')}}">
  
  <!--link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/footable/footable.core.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/examples/css/tables/footable.min.css?v4.0.1')}}"-->

    
    <!-- Plugins For This Page -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/select2/select2.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-select/bootstrap-select.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/jquery-wizard/jquery-wizard.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/formvalidation/formValidation.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/multi-select/multi-select.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/typeahead-js/typeahead.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/examples/css/forms/advanced.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/newcustom.css')}}">


    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/material-design/material-design.woff?v=2.2.0')}}">
    <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!--[if lt IE 9]>
    <script src="{{asset('principal/assetstest/global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{asset('principal/assetstest/global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('principal/assetstest/global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Scripts -->
    
    <script src="{{asset('principal/assetstest/global/vendor/breakpoints/breakpoints.min.js')}}"></script>
    <script>
        Breakpoints();
    </script>
  
</head>
    <body class="animsition')}}">
            @if(Auth::check())
                @include('layouts.principaltest.header')
                @if(Auth::user()->firstlogin == 0)
                @elseif(Auth::user()->firstlogin == 1 || Auth::user()->secondlogin == 1)
                  @include('layouts.principaltest.sidebar')
                @endif
            @endif
            @yield('content')
            @include('layouts.principaltest.studentfootr')