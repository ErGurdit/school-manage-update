<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dashboard | Remark Material Admin Template</title>

  <link rel="apple-touch-icon" href="{{asset('principal/assets/images/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{asset('principal/assetstest/assets/images/favicon.ico')}}">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/bootstrap.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/bootstrap-extend.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/css/site.min.css?v4.0.1')}}">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/css/skintools.min.css?v4.0.1')}}">
  <script src="{{asset('principal/assetstest/assets/js/Plugin/skintools.min.js?v4.0.1')}}"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/animsition/animsition.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/asscrollable/asScrollable.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/switchery/switchery.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/intro-js/introjs.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/slidepanel/slidePanel.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/flag-icon-css/flag-icon.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/waves/waves.min.css?v4.0.1')}}">
  
  <!--Sweetalert-->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-sweetalert/sweetalert.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/toastr/toastr.min.css?v4.0.1')}}">

  
  

  
  @if(Request::is('home'))
  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/chartist/chartist.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/jvectormap/jquery-jvectormap.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.css?v4.0.1')}}">
  <!-- Home Page -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/examples/css/dashboard/v1.min.css?v4.0.1')}}">
  @endif
  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-bs4/dataTables.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.min.css?v4.0.1')}}">
  <!--Markdown  editor-->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-markdown/bootstrap-markdown.min.css?v4.0.1')}}">
  <!-- Page -->
  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/toastr/toastr.min.css?v4.0.1')}}">

  <!-- Page -->
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/examples/css/advanced/toastr.min.css?v4.0.1')}}">
  @if(Request::is('profile'))
  <link rel="stylesheet" href="{{asset('principal/assetstest/assets/examples/css/pages/profile.min.css?v4.0.1')}}">
  @endif
  @if(Request::is('slider') || Request::is('certificate'))
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/blueimp-file-upload/jquery.fileupload.min.css?v4.0.1')}}">
  <!--Dropify-->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/dropify/dropify.min.css?v4.0.1')}}">
  @else
  <link rel="stylesheet" href="{{asset('principal/assets/examples/css/tables/datatable.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assets/examples/css/forms/layouts.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assets/examples/css/uikit/modals.min.css?v4.0.1')}}">
  @endif
  
  
  <!--Datepicker-->
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css?v4.0.1')}}">
  <!-- Fonts -->
  <link href="{{asset('principal/assets/apps/css/custom.css')}}" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/material-design/material-design.min.css?v4.0.1')}}">
  <link rel="stylesheet" href="{{asset('principal/assetstest/global/fonts/brand-icons/brand-icons.min.css?v4.0.1')}}">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  @if(Request::is('profile'))
  <link href="{{asset('principal/assets/crop/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('principal/assets/crop/assets/css/main.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('principal/assets/crop/assets/css/croppic.css')}}" rel="stylesheet" type="text/css" />
  <script src="{{asset('principal/assets/crop/assets/js/bootstrap.min.js')}}" type="text/script" ></script>
  <script src="{{asset('principal/assets/crop/assets/js/jquery.mousewheel.min.js')}}" type="text/script" ></script>
  <script src="{{asset('principal/assets/croppic.min.js')}}" type="text/script" ></script>
  <script src="{{asset('principal/assets/crop/assets/js/main.js')}}" type="text/script" ></script>
  <script src="{{asset('principal/assets/apps/scripts/newcrop.js')}}" type="text/script" ></script>
  @endif
  

  <!--[if lt IE 9]>
    <script src="../global/vendor/html5shiv/html5shiv.min.js?v4.0.1"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="../global/vendor/media-match/media.match.min.js?v4.0.1"></script>
    <script src="../global/vendor/respond/respond.min.js?v4.0.1"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="{{asset('principal/assetstest/global/vendor/breakpoints/breakpoints.min.js?v4.0.1')}}"></script>
  <script>
    Breakpoints();
  </script>
</head>

    <body class="animsition @if(Request::is('profile')) page-profile @else dashboard @endif">
      <div id="app">
            @if(Auth::check())
                @include('layouts.principaltest.header')
                @include('layouts.principaltest.sidebar')
            @endif
            @yield('content')
            @include('layouts.principaltest.footer')
    