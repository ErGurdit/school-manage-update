<!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <li class="nav-item start open maintab">
                                <a href="{{url('home')}}" class="nav-link nav-toggle" id="@if(Request::is('home')){{'activesidetab'}}@endif">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                            </li>
                            @if(Entrust::hasRole('principal'))
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Roles</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{route('role.index')}}" class="nav-link " id="@if(Request::is('role')){{'activesidetab'}}@endif">
                                            <span class="title">New Role</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{route('user.index')}}" class="nav-link " id="@if(Request::is('user')){{'activesidetab'}}@endif">
                                            <span class="title">Assign Role</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('class')}}" class="nav-link nav-toggle" id="@if(Request::is('class')){{'activesidetab'}}@endif">
                                    <i class="icon-puzzle"></i>
                                    <span class="title">Class</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('staff')}}" class="nav-link nav-toggle" id="@if(Request::is('staff')){{'activesidetab'}}@endif">
                                    <i class="icon-settings"></i>
                                    <span class="title">Staff</span>
                                    <span class="arrow"></span>
                                </a>
                                
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('slider')}}" class="nav-link nav-toggle" id="@if(Request::is('slider')){{'activesidetab'}}@endif">
                                    <i class="icon-bulb"></i>
                                    <span class="title">Slider</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('announcement')}}" class="nav-link nav-toggle" id="@if(Request::is('announcement')){{'activesidetab'}}@endif">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Announcement</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('expenses')}}" class="nav-link nav-toggle" id="@if(Request::is('expenses')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Expenses</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('attendence')}}" class="nav-link nav-toggle" id="@if(Request::is('attendence')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Attendence</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('holiday')}}" class="nav-link nav-toggle" id="@if(Request::is('holiday')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Holiday</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                             <li class="nav-item  ">
                                <a href="{{url('exams')}}" class="nav-link nav-toggle" id="@if(Request::is('exams')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Exams</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('certificate')}}" class="nav-link nav-toggle" id="@if(Request::is('certificate')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Certificates</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            @endif
                            @if(Entrust::hasRole('feesdepartment'))
                            <li class="nav-item  ">
                                <a href="{{url('fee')}}" class="nav-link nav-toggle" id="@if(Request::is('fee')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Account</span>
                                    <span class="arrow">All Admission List</span>
                                </a>
                            </li>
                            @endif
                            @if(Entrust::hasRole('accountant'))
                            <li class="nav-item  ">
                                <a href="{{url('account')}}" class="nav-link nav-toggle" id="@if(Request::is('account')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Maintaion Fee</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                             <li class="nav-item  ">
                                <a href="{{url('feestatus')}}" class="nav-link nav-toggle" id="@if(Request::is('feestatus')){{'activesidetab'}}@endif">
                                    <i class="icon-wallet"></i>
                                    <span class="arrow">Fee status</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->