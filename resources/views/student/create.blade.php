@extends('layouts.principaltest.student')

@section('content')
  <?php 
    $firstparam = ucfirst(\Request::segment(1));
    $secondparam = ucfirst(\Request::segment(2)); 
    $currentURL = $firstparam.'/'.$secondparam;

?>
<?php //dd($request);?>
<style>
    #__lpform_fname{
    display:none !important;
}
</style>
<!-- Page -->
<div class="page">
    <div class="page-header">
         <div class="page-header">
        <h1 class="page-title">{{$firstparam}}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{$firstparam}}</a></li>
        </ol>
    </div>
    </div>

    <div class="page-content container-fluid">
         @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
           @if(\Session::has("success"))
                <div id="msg" class="alert alert-success">
                    {{ \Session::get("success") }}
                </div>
                 
            @endif
            @if(\Session::has("danger"))
                <div id="msg" class="alert alert-danger">
                    {{ \Session::get("danger") }}
                </div>
                 
            @endif

        <div class="row">
            
        </div>

        <!-- Add Student Form -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="panel-desc"></span>
                </h3>
            </div>
            <div class="panel-body">

                <form id="exampleFullForm" autocomplete="off" method ="post" action="{{ action('StudentController@store') }}" enctype="multipart/form-data">
                    <div class="row row-lg">
                        <!--div class="col-lg-12 form-horizontal"-->
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Name
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="name" placeholder=""
                                           required="">
                                </div>
                            </div>
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Middle Name
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="middlename" placeholder="">
                                </div>
                            </div>
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Last Name
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="lastname" placeholder="" required="">
                                </div>
                            </div>

                            
                            <div class="form-group form-material col-lg-8 col-md-8 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Username
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="username" placeholder=""
                                           required="">
                                </div>
                            </div>
                        
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="control-label">Gender<span class="required">*</span>
                                </label>
                                    <div class="container row">
                                        <div class="radio-custom radio-primary col-lg-3">
                                            <input type="radio" id="inputMale" name="gender" value="M" required="">
                                            <label for="inputMale">Male</label>
                                        </div>
                                        <div class="radio-custom radio-primary col-lg-3">
                                            <input type="radio" id="inputFemale" name="gender" value="F" required="">
                                            <label for="inputFemale">Female</label>
                                        </div>
                               </div>
                            </div>
                            <div class="form-group form-material col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label class="control-label" for="inputClass">Class</label>
                                        <input type="hidden" id="authenticateuserid" value="{{Auth()->user()->id}}">
                                        <select data-plugin="selectpicker" name="class" id="class" class="form-control" data-live-search="true">
                                            <option value="">--Select Class--</option>
                                                @foreach($myclass as $classes)
                                                    <option value="{{$classes->cid}}" data-name ="{{$classes->class_display_name}}" data-id="{{$classes->cid}}">{{$classes->class_display_name}}</option>
                                                    <?php $classid = $classes->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="section">
                                    <label class="control-label" for="inputUserNameOne">Section</label>
                                        <select data-plugin="select2" name="section" class="form-control" id="newsection">
                                            <option value="">--Select Section--</option>
                                               
                                        </select>
                                        @if ($errors->has('section'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('section') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                    
                                    <div class="form-group form-material col-lg-6 col-md-6 col-sm-6 col-xs-12" id="stream_" style="display:none">
                                    <label class="control-label" for="inputUserNameOne">Stream</label>
                                        <select data-plugin="select2" class="form-control" name="stream[]" id="stream" data-live-search="true">
                                        </select>
                                        @if ($errors->has('stream'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('stream') }}</strong>
                                            </span>
                                        @else
                                            <span class="help-block"></span>
                                        @endif
                                    </div>
                                           <div class="form-group form-material col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-control-label" for="inputUserNameOne">Subject</label>
                                                <select data-plugin="select2" multiple name="subjectdata[]" id="subjectdata" class="form-control subjectdata" required="required">
                                                   
                                                </select>
                                                    @if ($errors->has('class'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('class') }}</strong>
                                                        </span>
                                                    @else
                                                    <span class="help-block"></span>
                                                    @endif
                                            </div>
                                            <div class="form-group form-material col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-control-label" for="inputUserNameOne">Optional Subject</label>
                                                <select data-plugin="select2" multiple name="leftsubject[]" id="leftsubject" class="form-control leftsubject">
                                                   
                                                </select>
                                                    @if ($errors->has('class'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('class') }}</strong>
                                                        </span>
                                                    @else
                                                    <span class="help-block"></span>
                                                    @endif
                                            </div>
                            <div class="form-group form-material col-lg-8 col-md-8 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Last School
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="last_school" placeholder=""
                                           required="">
                                </div>
                            </div>
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">DOB
                                    <span class="required">*</span>
                                </label>
                                <div class="datepair-wrap" data-plugin="datepair">
                                            <div class="input-daterange-wrap date-picker-width">
                                                <div class="input-daterange">
                                                    <div class="input-group date-picker-width">
                                                        <span class="input-group-addon">
                                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                                        </span>
                        <input type="text" class="form-control datepair-date datepair-start cal-set" placeholder="" data-plugin="datepicker" name="dob" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                               
                                </div>
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Father Name
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="fname" placeholder=""
                                           required="">
                                </div>
                            </div>  
                            
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Mothername
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="mname" placeholder=""
                                           required="">
                                </div>
                            </div>
                            
                            
                                <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Father Contact Number
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" maxlength="10" name="father_contact" placeholder=""
                                          onkeypress="return onlyDigitsMother(event,this);" >
                                           <span class="father_contact_number"></span>
                                </div>
                            </div>
                             
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Mother Contact Number
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" maxlength="10" name="mother_contact" placeholder="" onkeypress="return onlyDigitsMother(event,this);" >
                                           <span class="mother_contact_number"></span>
                                </div>
                            </div>
                            
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Home Tel
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="hometel" placeholder=""
                                           required="">
                                </div>
                            </div>
                             <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Email
                                    <span class="required">*</span>
                                </label>
                                <div class="col-xl-12 col-md-9">
                                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon md-email" aria-hidden="true"></i>
                        </span>
                                        <input type="email" class="form-control" name="email" placeholder="email@email.com"
                                               required="">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">Country</label>
                                        <select  data-plugin="select2" name="country" id="country_list" class="form-control select2">
                                                <option value="0">--Select Country--</option>
                                                @foreach($country as $countries)
                                                <option value="{{$countries->id}}" data-countrname="{{$countries->name}}">{{$countries->name}}</option>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">State</label>
                                        <select data-plugin="select2" name="state" id="state_list" class="form-control state_list select2">
                                                <!--@foreach($state as $states)-->
                                                <!--<option value="{{$states->id}}">{{$states->name}}</option>-->
                                                <!--@endforeach-->
                                        </select>
                                        @if ($errors->has('state'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                                    <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="control-label">City</label>
                                        <select data-plugin="select2" name="city" id="city_list" class="form-control city_list select2">
                                                <!--@foreach($city as $cities)-->
                                                <!--<option value="{{$cities->id}}">{{$cities->name}}</option>-->
                                                <!--@endforeach-->
                                        </select>
                                        @if ($errors->has('city'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Address
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="address" placeholder=""
                                           required="">
                                </div>
                            </div>
                            
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="col-xl-12 col-md-3 control-label">Pincode
                                    <span class="required">*</span>
                                </label>
                                <div class=" col-xl-12 col-md-9">
                                    <input type="text" class="form-control" name="pincode" placeholder=""
                                           required="">
                                </div>
                            </div>
                            <div class="form-group form-material col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="control-label">Fees Cleared<span class="required">*</span>
                                </label>
                                    <div class="container row">
                                        <div class="radio-custom radio-primary col-lg-3">
                                            <input type="radio" id="feesclear1" name="feesclear" value="Y">
                                            <label for="inputYes">Yes</label>
                                        </div>
                                        <div class="radio-custom radio-primary col-lg-3">
                                            <input type="radio" id="feesclear2" name="feesclear" value="N">
                                            <label for="inputNo">No</label>
                                        </div>
                               </div>
                            </div>
                            <div class="form-group form-material pending_months col-lg-4 col-md-4 col-sm-4 col-xs-12" id="pending_months" style="display: none">
                                    <label class="form-control-label" for="inputUserNameOne">Pending Months</label>
                                        <select data-plugin="select2" multiple name="mondata[]" id="pend_mon" class="form-control monthdata" data-live-search="true">
                                            <option value="">--Select Subject--</option>
                                                @foreach($month as $months)
                                                    <option value="{{$months->id}}" data-monthname="{{$months->monthname}}" data-id="{{$months->id}}">{{$months->monthname}}</option>
                                                    <?php $months = $months->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                        <input type="hidden" name="pend_m">
                                    </div>
                                    <div class="form-group form-material submission_months col-lg-4 col-md-4 col-sm-4 col-xs-12" id="submission_months" style="display: none">
                                    <label class="form-control-label" for="inputUserNameOne">Submission Months</label>
                                        <select data-plugin="select2" multiple name="mondata1[]" id="submis_mon" class="form-control mondata" data-live-search="true">
                                            <option value="">--Select Subject--</option>
                                                @foreach($month as $months)
                                                    <option value="{{$months->id}}" data-monthname="{{$months->monthname}}" data-id="{{$months->id}}">{{$months->monthname}}</option>
                                                    <?php $months = $months->id;?>
                                                @endforeach
                                        </select>
                                        @if ($errors->has('class'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('class') }}</strong>
                                            </span>
                                        @else
                                        <span class="help-block"></span>
                                        @endif
                                    </div>
                            
                        <!--/div-->

                        <div class="form-group form-material col-xl-12 text-right padding-top-m">
                            <button type="submit" class="btn btn-primary" id="validateButton1">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Panel Full Example -->

        
    </div>
</div>
<!-- End Page -->



 @endsection