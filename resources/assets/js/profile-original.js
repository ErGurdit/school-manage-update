
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/ExampleComponent.vue'));
// Vue.component('chat-message', require('./components/ChatMessage.vue'));
// Vue.component('chat-log', require('./components/ChatLog.vue'));
// Vue.component('chat-composer', require('./components/ChatComposer.vue'));
Vue.component('notification', require('./components/Notification.vue'));

const app = new Vue({
    el: '#app',
    data:{
    	msg:'update new post here new',
    	content:'',
        privsteMsgs:[],
        singleMsgs:[],
        msgFrom:'',
        conID:'',
        friend_id: '',
        seen: false,
        newMsgFrom: ''

        },
    ready:function(){
	    this.created();
		},
		created(){
    		axios.get('http://school-manage-npm-complete-gurdit.c9users.io/getMessages').then(response => {
    			console.log(response.data);
                app.privsteMsgs = response.data;
    		})
            .catch(function (error){
                console.log(error);
            });
    	},
    methods:{
        messages:function(id){
            axios.get('http://school-manage-npm-complete-gurdit.c9users.io/getMessages/'+id).then(response => {
                console.log(response.data);
                app.singleMsgs = response.data;
                app.conID = response.data[0].conservation_id
            })
            .catch(function (error){
                console.log(error);
            });
        },

        inputHandler(e){
            if(e.keyCode === 13  && !e.shiftKey){
                e.preventDefault();
                this.sendMsg();
            }
        },
        sendMsg(){
            if(this.msgFrom){
                axios.post('http://school-manage-npm-complete-gurdit.c9users.io/sendMessage', {
                    conID:this.conID,
                    msg:this.msgFrom
                })
                .then(function (response) {
                    console.log(response.data);
                    if(response.status === 200){
                        app.singleMsgs = response.data;
                    }
                })
                .catch(function (error){
                    console.log(error);
                });
            }
        },
         friendID: function(id){
            app.friend_id = id;
        },
       sendNewMsg(){
         axios.post('http://school-manage-npm-complete-gurdit.c9users.io/sendNewMessage', {
                friend_id: this.friend_id,
                msg: this.newMsgFrom,
            })
                .then(function (response) {
                console.log(response.data); // show if success
                if(response.status===200){
                  window.location.replace('http://school-manage-npm-complete-gurdit.c9users.io/messages');
                  app.msg = 'your message has been sent successfully';
                }

              })
              .catch(function (error) {
                console.log(error); // run if we have error
              });
       }
    }
});
