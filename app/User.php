<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Role;

class User extends Authenticatable
{
    use EntrustUserTrait,Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username','email', 'password','role_create'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function staff(){
        return $this->belongsTo('App\staffmember');
    }
    public function studentprofile(){
        return $this->belongsTo('App\studentprofile','userid');
    }
    
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    
    public function feesubmit(){
        return $this->belongsTo('App\feesubmit','userid');
    }
}
