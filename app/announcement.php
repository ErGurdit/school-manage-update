<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class announcement extends Model
{
    protected $fillable = [
        'userid','title', 'description','startdate','enddate','status',
    ];
}
