<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testmark extends Model
{
     protected $fillable = [
        'schoolid', 'staffid','classid','sectionid','streamid','subjectid','totalmarks','passingmarks','description','test_date',
    ];
}
