<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class staffmember extends Model
{
    use EntrustUserTrait;
    public function roles(){
        return $this->belongsToMany('App\Role');
    }
    public function users(){
        return $this->hasMany('App\User');
    }
}
