<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Entrust;
use DB;
use App\school;
use App\staffmember;
use Auth;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if($this->app->environment('local')) {
            \URL::forceScheme('https');
        }
		view()->composer('*', function ($view) 
        {
            if( Auth()->user() ) {
                if(!Entrust::hasRole('superadmin')) {
                    $userid = Auth()->user()->id;
                    
                    $staffmember = staffmember::where('userid',$userid)->first();
                    $school =  school::where('id',$staffmember->schoolid)->first();
                    
                    //getting all accounts
                    $accnt_id = DB::table('staffmembers')
                                ->select('staffmembers.userid as accnt_id','users.name as uname')
                                ->join('users','users.id','=','staffmembers.userid')
                                ->join('role_user','role_user.user_id','=','staffmembers.userid')
                                ->where('role_user.role_id',8)
                                ->where('staffmembers.schoolid',$school->id)
                                ->get();
                                //->pluck('accnt_id');
                                //dd($accnt_id);
                    
                    //get total fees amount of current day
                    
                    $current_total_fees = DB::table('feesubmits')
           ->select('feesubmits.*',DB::raw('sum(feesubmits.amount) as tm'),'feesubmits.*')
           ->where('feesubmits.schoolid',$school->id)
           //->where('approve_moneys.status', 1)
           ->where('feesubmits.updated_at','=', DB::raw('curdate()'))
           ->get();
                    $student = DB::table('studentprofiles')
                             ->select('studentprofiles.*',DB::raw('studentprofiles.student_user_id, COUNT(*) as count'),'users.name as name','users.username as username','feesubmits.pend_m as pm','feesubmits.paid_months as sm','feesubmits.total_amount as tm',DB::raw('sum(feesubmits.amount) AS totalfees'),'feesubmits.updated_at as up')
                    		 ->join('feesubmits', 'feesubmits.studentid', '=', 'studentprofiles.id')
                    		 ->join('users', 'users.id', '=', 'studentprofiles.student_user_id')
                    				->where('feesubmits.updated_at','=', DB::raw('curdate()'))
                    				->where('studentprofiles.userid','=', $staffmember->userid)
                    			    ->get();
                    //dd(json_decode($student[0]->totalfees));
                    $view->with('acct_id_m',$accnt_id);
                    $view->with('sch_i',$staffmember->schoolid);
                    $view->with('fees_amnt',json_decode($current_total_fees[0]->tm));
                    $view->with('current_userid',$userid);

                }
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
