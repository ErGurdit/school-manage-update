<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class approve_money extends Model
{
    //
     public function school() {
		return $this->belongs_to('school','id');
	}
	 public function user(){
        return $this->hasMany('App\User');
    }
}
