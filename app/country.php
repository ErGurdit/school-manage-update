<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{
    protected $fillable = [
        'name', 'iso', 'flag','countrycode',
    ];
    public function offerschool() {
		return $this->hasMany('offer_to_school','countryid');
	}
	public function studentprofile(){
        return $this->belongsTo('App\studentprofile','countryid');
    }
}
