<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class schoolpayment extends Model
{
    protected $fillable = [
        'schoolid', 'studentid','last_payment_due','payment_method','last_payment_amount','next_payment_date','extended_period','last_payment_date','payment_amount','payment_detail','payment_transfer_school','transfer_date'
    ];
    public function school() {
		return $this->belongs_to('school','id');
	}
	public function country() {
		return $this->belongs_to('country','id');
	}
	 public function state() {
		return $this->belongs_to('state','id');
	}
	 public function city() {
		return $this->belongs_to('city','id');
	}
}
