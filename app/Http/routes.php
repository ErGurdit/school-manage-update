// <?php

// /*
// |--------------------------------------------------------------------------
// | Application Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register all of the routes for an application.
// | It's a breeze. Simply tell Laravel the URIs it should respond to
// | and give it the controller to call when that URI is requested.
// |
// */



// // Route::get('/', function () {
// //     return view('welcome');
// // });
// Route::get('/', 'themeController@index');

// Route::get('/logout','HomeController@index');
// Route::auth();
// Route::resource('/role','RoleController');
// Route::resource('/user','UserController');
// Route::get('/permission','RoleController@permission');
// Route::get('principal/role','RoleController@index');


// //school
//Route::get('/school','SchoolController@index');
// Route::get('/schhool/create','SchoolController@create');
// Route::post('/school/store','SchoolController@store');
// Route::patch('/school/update/{id}','SchoolController@update');
// Route::post('/school/changestatus/{id}','SchoolController@ajaxChangeStatus');
// Route::get('/school/destroy/{id}','SchoolController@destroy');
// Route::get('/school/show','SchoolController@show');
// Route::get('/school/newstatus/{id}','SchoolController@statuscheck');
// Route::get('/school/showin','SchoolController@showin');


Route::get('/student/create','StudentController@create');
Route::post('/student/store','StudentController@store');

// //offer
// Route::get('/offer','offercontroller@index');
// Route::get('/offer/create','offercontroller@create');
// Route::post('/offer/store','offercontroller@store');
// Route::patch('/offer/update/{id}','offercontroller@update');
// Route::get('/offer/destroy/{id}','offercontroller@destroy');
// Route::post('/offer/changestatus/{id}','offercontroller@ajaxChangeStatus');
// Route::get('/offer/show','offercontroller@show');
// Route::get('/offer/newstatus/{id}','offercontroller@statuscheck');
// Route::get('/offer/showin','offercontroller@showin');

// //payment
// Route::get('/payment/school','PaymentController@school');
// Route::get('/payment/student','PaymentController@student');
// Route::get('/payment/onlinefee','PaymentController@onlinefee');
// Route::get('/payumoney','PaymentController@paymentuser');
// Route::post('/payumoneysuccess','PaymentController@success');
// Route::post('/payumoneyfailed','PaymentController@failed');
// // Route::post('/offer/store','PaymentController@store');
// // Route::get('/offer/destroy/{id}','PaymentController@destroy');
// // Route::post('/offer/changestatus/{id}','PaymentController@ajaxChangeStatus');
// // Route::get('/offer/show','PaymentController@show');
// // Route::get('/offer/newstatus/{id}','PaymentController@statuscheck');
// // Route::get('/offer/showin','PaymentController@showin');
// //Default Setting
// Route::get('/defaultsetting','defaultsettingController@index');
// Route::post('/defaultsetting/store','defaultsettingController@store');
// Route::patch('/defaultsetting/update/{id}','defaultsettingController@update');
// //slider
// Route::get('/slider','SliderController@index');
// Route::post('/slider/store','SliderController@store');
// Route::patch('/slider/update/{id}','SliderController@update');
// Route::get('/slider/destroy/{id}','SliderController@destroy');
// Route::post('/slider/changestatus/{id}','SliderController@ajaxChangeStatus');
// Route::get('/slider/show','SliderController@show');
// Route::get('/slider/newstatus/{id}','SliderController@statuscheck');
// Route::get('/slider/showin','SliderController@showin');
// //frontend about us change
// Route::get('/aboutus','defaultsettingController@aboutus');
// Route::patch('/defaultsetting/aboutusupdate/{id}','defaultsettingController@aboutusupdate');
// //announcement
// Route::get('/announcement','announcementController@index');
// Route::post('/announce/store','announcementController@store');
// Route::patch('/announce/update/{id}','announcementController@update');
// Route::get('/announce/destroy/{id}','announcementController@destroy');

// //class
// Route::get('/class','ClassController@index');
// Route::post('/class/store','ClassController@store');
// Route::post('/class/classstore','ClassController@classstore');
// Route::post('/class/classupdate','ClassController@classupdate');
// Route::patch('/class/update/{id}','ClassController@update');
// Route::get('/class/destroy/{id}','ClassController@destroy');
// //staff
// Route::get('/staff','StaffController@index');
// Route::post('/staff/store','StaffController@store');
// Route::patch('/staff/update/{id}','StaffController@update');
// Route::get('/staff/destroy/{id}','StaffController@destroy');
// //expenses
// Route::get('/expenses','ExpenseController@index');
// Route::post('/expenses/store','ExpenseController@store');
// Route::patch('/expenses/update/{id}','ExpenseController@update');
// Route::get('/expenses/destroy/{id}','ExpenseController@destroy');
// //attendence
// Route::get('/attendence','AttendenceController@index');
// //holiday
// Route::get('/holiday','StaffController@hindex');
// Route::post('/holiday/store','StaffController@hstore');
// Route::patch('holiday/update/{id}','StaffController@hupdate');
// Route::get('holiday/destroy/{id}','StaffController@hdestroy');
// //exam
// Route::get('/exams','ExamController@index');
// //certificate
// Route::get('/certificate','CerificateController@index');
// //Admission
// Route::get('/fee','FeesController@index');
// Route::post('/feestore','FeesController@accountstore');
// Route::get('/feestatus','FeesController@feestatus');
// //Maintain Fee
// Route::get('/account','AccountController@index');
// Route::patch('/classfeeupdate','AccountController@accountstore');
// //state
// Route::get('/get-state-list', 'worldcontroller@getStateList');
// //city
// Route::get('/get-city-list', 'worldcontroller@getCityList');
// //logout
// Route::get('/logout', 'Auth\LoginController@logout');
// Route::group(['middleware'=>'auth'],function(){
// Route::get('/home', 'HomeController@index');

// });