<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\school;
use App\classes;
use App\User;
use Auth;
use Illuminate\Support\Facades\Input;
use App\class_to_school;
use App\class_to_section;
use App\sectionclass;
use App\highclass;
use App\stream_to_class;
use Redirect;
use Validator;
use App\admission;
use App\maintainfee;
use App\staffmember;
class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
    //   $this->userid = Auth::user()->id;
    //   $this->schoolid = school::where('userid',$this->userid)->first();
    }
    public function index()
    {
        
        //dd();
        // current user
        $user = DB::table('schools')
        ->select('schools.name as schoolname','schools.id as schoolid')
        ->join('users','users.id','=','schools.userid')
        ->where('users.id',Auth::user()->id)
        ->first();
        $schoolid = school::where('userid',AUth::user()->id)->first();
        
        
       
        $class = DB::table('class_to_schools')
        ->select('schools.name as schoolname','classes.class_name','classes.id as cid','classes.class_display_name as cdisplay','class_to_schools.classid as classid','class_to_schools.schoolid as schoolid','classes.has_stream as hasstream')
        ->join('classes','classes.id','=','class_to_schools.classid')
        ->join('schools','schools.id','=','class_to_schools.schoolid')
        ->where('class_to_schools.schoolid',$schoolid->id)
        ->get();
        
        //dd($class[0]->cid);
         $st = DB::table('studentprofiles')
        ->select('studentprofiles.*','users.name as nme')
        ->join('users','users.id','=','studentprofiles.student_user_id')
        ->join('class_to_schools','class_to_schools.classid','=','studentprofiles.classid')
        ->where('studentprofiles.schoolid',$schoolid->id)
        ->get();
        //dd($st);
         
        
        $myclass = classes::where('status',0)->get();
        $sectionclassnew = sectionclass::where('schoolid',$schoolid->id)->get();
        if(!empty($sectionclassnew)){
            $sectionclass = sectionclass::get();
            //dd($sectionclass);
            $studentclass = DB::table('studentprofiles')->select('studentprofiles.*')->get();
                        //dd($studentclass);

        }else{
            $sectionclass = sectionclass::where('schoolid',0)->get(); 
                        $studentclass = DB::table('studentprofiles')
                        ->where('schoolid',0)
                        ->get();

        }
        //dd($sectionclass);
        $classsection = DB::table('class_to_sections')
                        ->join('classes','classes.id','=','class_to_sections.classid')
                        ->join('sectionclasses','sectionclasses.id','=','class_to_sections.sectionid')
                        //->join('studentprofiles','studentprofiles.classid','=','classes.id')
                        ->where('class_to_sections.schoolid',$schoolid->id)
                        ->get();
                      
                        
             $classstudent = DB::table('class_to_schools')
->select('studentprofiles.*','studentprofiles.id','studentprofiles.student_user_id','class_to_schools.*')
//->join('classes','classes.id','=','class_to_sections.classid')
           //->join('sectionclasses','sectionclasses.id','=','class_to_sections.sectionid')
            ->join('studentprofiles','studentprofiles.classid','=','class_to_schools.classid')
            ->join('users','users.id','=','studentprofiles.userid')
            ->where('users.gender','M')
            ->where('studentprofiles.schoolid',$schoolid->id)
                        ->get();
                        
        //dd($classsection1);
        //class_to_school
        $newclass = class_to_school::where('schoolid',$schoolid->id)->get();
        // map all classes and sections
        {
            $classsection_map = [];
            foreach( $classsection as $temp_section ) {
                $clsid = $temp_section->classid;
                $secid = $temp_section->sectionid;
                if( !array_key_exists( $clsid, $classsection_map ) ) {
                    $classsection_map[ $clsid ] = [];
                }
                if( !in_array( $secid, $classsection_map[ $clsid ] ) ) {
                    $classsection_map[ $clsid ][] = $secid;
                }
            }
            $classsection_map = array_filter($classsection_map);
           //dd($classsection_map);
            
            
            $classstudent_map = [];
            foreach( $classstudent as $temp_section ) {
                $clsid = $temp_section->classid;
                $secid = $temp_section->student_user_id;
                //dd(count($clsid));
                
                if( !array_key_exists( $clsid, $classstudent_map ) ) {
                    $classstudent_map[ $clsid ] = [];
                }
                if( !in_array( $secid, $classstudent_map[ $clsid ] ) ) {
                    $classstudent_map[ $clsid ][] = $secid;
                }
            }
            $classstudent_map = array_filter($classstudent_map);
            //dd($classstudent_map);
        }
        //allclass
        $allclass = classes::all();
        //dd($allclass);
        // doing this
        if(!empty($newclass)) {
            $temp = array();
            foreach($newclass as $k => $v){
                $temp['classid'][$k] = $v['classid'];
                $testclass = $temp['classid'];
            }
            if(!empty($testclass)){
                $newdata = $testclass;
            }
        }
        
        
        //section select
        	$tempoffer=array();
			$tempcounter=0;
	    foreach($classsection as $classsection){
		    //$tempoffer[$classsection->classid]['sectionid'][$classsection->sectionid]=$classsection->sectionid;
		    $tempoffer[$classsection->sectionid]['classid'][$classsection->classid]=$classsection->classid;
		    $tempcounter++;
		    $sch['sectionid'] = $classsection->sectionid;
		    
		}
				//dd($sch['sectionid']);

		//student select
        	/*$tempoffer1=array();
			$tempcounter1=0;
	    foreach($classstudent as $classstudent){
		    //$tempoffer[$classsection->classid]['sectionid'][$classsection->sectionid]=$classsection->sectionid;
		    $tempoffer1[$classstudent->sectionid]['classid'][$classstudent->classid]=$classstudent->classid;
		    $tempcounter1++;
		    $sch1['sectionid'] = $classstudent->sectionid;
		    
		}*/
		//dd($sch1['sectionid']);
		
		$finaldata=array();
		foreach($tempoffer as $k=>$v){
		    //$finaldata[$k]['sectionid']=$v['sectionid'];
		    $finaldata[$k]['classid']=$v['classid'];
		}
		if(!empty($finaldata)){
		 $tempsec = $finaldata[$k]['classid'];
		  //print_r($tempsec);
		}
		
		
		/*$finaldata2=array();
		foreach($tempoffer1 as $k=>$v){
		    //$finaldata[$k]['sectionid']=$v['sectionid'];
		    $finaldata2[$k]['classid']=$v['classid'];
		}
		if(!empty($finaldata2)){
		 $tempsec2 = $finaldata2[$k]['classid'];
		  //print_r($tempsec2);
		}*/
		
		//newstream
		
		$stream = highclass::where('status',0)->get();
		//stream select
		$newstream = stream_to_class::where('schoolid',$schoolid->id)->get();
		$tempstream=array();
			$tempcounter=0;
		foreach($newstream as $newstream){
		    //$tempoffer[$classsection->classid]['sectionid'][$classsection->sectionid]=$classsection->sectionid;
		    $tempstream[$newstream->sectionid]['stremid'][$newstream->stremid]=$newstream->stremid;
		    $tempcounter++;
		    $sch['stremid'] = $newstream->stremid;
		    
		}
		
		$finalstream=array();
		foreach($tempstream as $k=>$v){
		    //$finaldata[$k]['sectionid']=$v['sectionid'];
		    $finalstream[$k]['stremid']=$v['stremid'];
		}
		if(!empty($finalstream)){
		 $tempstr = $finalstream[$k]['stremid'];
		 //print_r($tempstr);
		}
		$custom_sections = [];
		foreach( $sectionclass as $sectioncls ) {
		    if( $sectioncls->schoolid ) {
		        $custom_sections[] = $sectioncls->section_display_name;
		    }
		    //dd($sectioncls);
		}
		
		$custom_students = [];
		foreach( $studentclass as $sectioncls ) {
		    if( $sectioncls->schoolid ) {
		        $custom_students[] = $sectioncls->student_user_id;
		    }
		    //dd($sectioncls);
		}
	
        if(!isset($tempstr)){
            $tempstr = array();
        }
        $admission =  DB::table('admissions')->latest('rollnumber')->first();
        $gender_data = DB::select( DB::raw( "SELECT sp.classid, SUM(u.gender='M') as total_males, SUM(u.gender='F') as total_females FROM `studentprofiles` sp JOIN users u ON u.id = sp.student_user_id WHERE sp.classid AND sp.schoolid='$schoolid->id' GROUP BY classid, schoolid ORDER BY schoolid, classid" ));
                        
                        //dd(json_encode($gender_data));
        $genderfemale = DB::table('admissions')
                        ->join('studentprofiles','studentprofiles.id','=','admissions.studentid')
                        ->join('users','users.id','=','studentprofiles.userid')
                        ->join('classes','classes.id','=','admissions.classid')
                        ->where('users.gender','F')
                        ->where('admissions.schoolid',$schoolid->id)
                        ->count();
        $genderTotalcount = DB::table('admissions')
                        ->join('studentprofiles','studentprofiles.id','=','admissions.studentid')
                        ->join('users','users.id','=','studentprofiles.userid')
                        ->join('classes','classes.id','=','admissions.classid')
                        ->where('admissions.schoolid',$schoolid->id)
                        ->count();
        //dd($gendermale);
        $userid = Auth()->user()->id;
                       $schoolid = school::where('userid',$userid)->first();

                       $staffmember = staffmember::where('userid',$userid)->first();
                       $school =  school::where('id',$staffmember->schoolid)->first();
                                        $sch_i = $staffmember->schoolid;

        //dd($admission->rollnumber);
       return view('principaltest.class.index',compact('gender_data','studentclass','sch_i','class','user','school','schoolid','myclass','newclass','newdata','sectionclass','tempsec','tempclass','stream','tempstr','classsection_map', 'custom_sections','admission','genderfemale','gendermale','genderTotalcount'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function classupdate(Request $request)
    {
        //get auth id and schholid
        $userid = Auth::user()->id;
        $schoolid = school::where('userid',$userid)->first();
        //delete old data
         DB::table('class_to_schools')->where('schoolid',$schoolid->id)->delete();
         DB::table('class_to_sections')->where('schoolid',$schoolid->id)->delete();
         DB::table('stream_to_classes')->where('schoolid',$schoolid->id)->delete();
        
            // foreach($request->classname as $classes){
            //     $newclass= strtolower(str_replace(' ','',$classes));
            //     $class = new class_to_school;
            //     $class->classid =  $newclass;
            //     $class->schoolid = Input::get('schoolid');
            //     $class->save();
            // }
            if($request->sectionclass > 0){
                $requestsecclass = $request->sectionclass;
            }else{
                $requestsecclass = 0;
            }
            //dd($requestsecclass);
            $classes = array_keys($requestsecclass);
            // save sections
            foreach( $requestsecclass as $class_id => $sections ) {
                foreach( $sections as $section_id ) {
                    $class_to_section = new class_to_section;
                    $class_to_section->schoolid = $schoolid->id;
                    $class_to_section->classid = $class_id;
                    $class_to_section->sectionid = $section_id;
                    $class_to_section->save();
                }
            }
            
            // save streams
            if($request->sectionsubject) {
                foreach($request->sectionsubject as $class_id => $streams ) {
                    $classes[] = $class_id;
                    foreach( $streams as $stream_id ) {
                        $stream_to_class = new stream_to_class;
                        $stream_to_class->schoolid = $schoolid->id;
                        $stream_to_class->classid = $class_id;
                        $stream_to_class->sectionid = 0;
                        $stream_to_class->stremid = $stream_id;
                        $stream_to_class->save();
                    }
                }
            }
            // save classes
            foreach( $classes as $class_id ) {
                $class_to_school = new class_to_school;
                $class_to_school->schoolid = $schoolid->id;
                $class_to_school->classid = $class_id;
                $class_to_school->save();
            }
            foreach( $classes as $class_id ) {
                $classcheck = maintainfee::where('classid',$class_id)->where('schoolid',$schoolid->id)->first();
                if(!empty($classcheck)){
                    $maintainfee = maintainfee::where('classid',$class_id)->where('schoolid',$schoolid->id)->first();
                    $maintainfee->schoolid = $schoolid->id;
                    $maintainfee->classid = $class_id;
                    $maintainfee->save();
                }else{
                    $maintailfee = new maintainfee;
                    $maintailfee->schoolid = $schoolid->id;
                    $maintailfee->classid = $class_id;
                    $maintailfee->save();
                }
            }
            
            return redirect('class');
            foreach($request->classname as $classes)
            {
                $newclass= strtolower(str_replace(' ','',$classes));
                foreach ($request->sectionclass as $sections) 
                {
                   $newsection= strtolower(str_replace(' ','',$sections));
                   $classsec = new class_to_section;
                   $classsec->schoolid = $schoolid->id;
                   $classsec->classid = $newclass;
                   $classsec->sectionid = $newsection;
                   $classsec->save();
                   if(!empty($request->sectionsubject))
                   {
                     foreach($request->sectionsubject as $subjects)
                     {
                       $newsectionsub= strtolower(str_replace(' ','',$subjects));
                        if($newclass == '14' || $newclass =='15')
                        {
                           $subjectadd = new stream_to_class;
                          $subjectadd->schoolid = Input::get('schoolid');
                          $subjectadd->classid = $newclass;
                          $subjectadd->sectionid = $sections;
                          $subjectadd->stremid = $newsectionsub;
                          $subjectadd->save();
                        }
                    }
                   }
                }
            }
		
        
    }
    /*public function classupdate(Request $request){
        $dataadmin = $request->admissionroom;
        //dd($dataadmin);
        $checkadmin = DB::table('admissions')->latest('rollnumber')->first();
        //dd($checkadmin);
        if($checkadmin->rollnumber == ''){
            echo 'bye';
            $admisiondata = 101;
            
        }elseif($checkadmin->rollnumber == $dataadmin){
            $admisiondata = $checkadmin->rollnumber +1;
            echo 'hello';
        }
        //dd($admisiondata);
        if(!empty($checkadmin->rollnumber)){
            $classid = '';
            $sectionid = '';
            $schoolid = '';
        }else{
            $classid = 1;
            $sectionid = 1;
            $schoolid = 1;
        }
        
        $admission = new admission;
        $admission->classid = Input::get('class');
        $admission->rollnumber = $schoolid.$sectionid.$classid.$admisiondata;
        //dd($admission);
        $admission->save();
        return back();
    }*/
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->classname);
        foreach($request->classname as $classes){
        $newclass= strtolower(str_replace(' ','',$classes));
        $class = new classes;
        $class->class_name =  $newclass;
        $class->class_display_name = $classes;
        $class->save();
        
        }
        return redirect('class');
    }
    
     public function classstore(Request $request)
    {
        //dd($request->classname);
        foreach($request->classname as $classes){
        $newclass= strtolower(str_replace(' ','',$classes));
        $class = new class_to_school;
        $class->classid =  $newclass;
        $class->schoolid = Input::get('schoolid');
        $class->save();
        
        }
        return redirect('class');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class = classes::where('id',$id)->first();
        $class->class_name =  Input::get('classname');
        $class->save();
        return redirect('class');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request,$id)
    {
        
        DB::table('class_to_schools')->where('id',$id)->delete();
        return back()->withMessage("Classes Deleted");
    }
    
    public function customsection(Request $request)
    {
        // current user
        $user = DB::table('schools')
        ->select('schools.name as schoolname','schools.id as schoolid')
        ->join('users','users.id','=','schools.userid')
        ->where('users.id',Auth::user()->id)
        ->first();
        DB::table('sectionclasses')->where('schoolid',$user->schoolid)->delete();
        $data = explode(',',$request->customsection);
        //$newdata =  array($data);
        foreach($data as $newdata){
            $addnewsection = new sectionclass;
            $addnewsection->schoolid = $user->schoolid;
            $addnewsection->section_name = $newdata;
            $addnewsection->section_display_name = $newdata;
            $addnewsection->save();
            //print_r($addnewsection);
        }
        //$lastid = $addnewsection->section_display_name;
        //dd($lastid);
        $query = DB::table('sectionclasses')->where('schoolid',$user->schoolid)->pluck('section_display_name','id');
        $newarray = '';
        foreach($query as $queries){
            $newarray[] = $queries;
        }
        $newclasssection = implode('',$newarray);
        //exit;
        //dd($newarray);
        $newquery = DB::table('class_to_sections')->where('schoolid',$user->schoolid)->pluck('classid','sectionid','id');
        
        return json_encode($query);  
    }
    //class detail
    public function stuget(Request $request){
        $admission = DB::table('admissions')
                ->select('admissions.id as aid','admissions.*','studentprofiles.id as sid','studentprofiles.*','studentprofiles.*','users.id as uid','users.name as uname','users.*','countries.id as cid','countries.name as cname','countries.*','states.id as stateid','states.name as sname','states.*','cities.id as cityid','cities.name as cityname','cities.*')
                ->join('studentprofiles','studentprofiles.id','=','admissions.studentid')
                ->join('users','users.id','=','studentprofiles.userid')
                ->join('countries','countries.id','=','studentprofiles.countryid')
                ->join('states','states.id','=','studentprofiles.stateid')
                ->join('cities','cities.id','=','studentprofiles.cityid')
                ->where('classid',$request->id)
                ->get();
                //dd($admission);
        return view('principaltest.class.classdetail',compact('admission'));
    }
    //section deatil
    public function stusecget(Request $request){
        //dd($request->schoolid);
        $admission = DB::table('admissions')
                ->select('admissions.id as aid','admissions.*','studentprofiles.id as sid','studentprofiles.*','studentprofiles.*','users.id as uid','users.name as uname','users.*','countries.id as cid','countries.name as cname','countries.*','states.id as stateid','states.name as sname','states.*','cities.id as cityid','cities.name as cityname','cities.*')
                ->join('studentprofiles','studentprofiles.id','=','admissions.studentid')
                ->join('users','users.id','=','studentprofiles.userid')
                ->join('countries','countries.id','=','studentprofiles.countryid')
                ->join('states','states.id','=','studentprofiles.stateid')
                ->join('cities','cities.id','=','studentprofiles.cityid')
                ->where('studentprofiles.schoolid',$request->schoolid)
                ->where('classid',$request->classid)
                ->where('sectionid',$request->sectionid)
                ->get();
            //dd($admission);
                
        return view('principaltest.class.classdetail',compact('admission'));
    }
    public function getstudent(Request $request)
    {
       //dd($request);
               $userid = Auth()->user()->id;

               $schoolid = school::where('userid',$userid)->first();

       $student = DB::table('studentprofiles')
			->select('studentprofiles.*','users.name as name','users.*')
	    	->join('users', 'users.id', '=', 'studentprofiles.student_user_id')
            ->where('studentprofiles.classid',$request->classid)
                        ->where('studentprofiles.schoolid',$schoolid->id)

			->get();
			return $student;

    }

    
}
