<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\studentprofile;
use App\country;
use App\state;
use App\city;
use App\school;
use App\classes;
use DB;
use App\User;
use Illuminate\Support\Str;
use App\staffmember;
use Notification;
use App\Notifications\Feestatus;
use Session;



class FeeStatusController extends Controller
{
    //
    public function show(Request $request)
    {
        //dd($request);
                    $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();

        //$student = studentprofile::all();
        $country = country::all();
        $state = state::all();
        $city = city::all();
        $class = classes::all();
        //dd($staffmember->userid);
        
        $accnt_id = DB::table('staffmembers')
                                ->select('staffmembers.userid as accnt_id','users.name as uname')
                                ->join('users','users.id','=','staffmembers.userid')
                                ->join('role_user','role_user.user_id','=','staffmembers.userid')
                                ->where('role_user.role_id',8)
                                ->where('staffmembers.schoolid',$school->id)
                                ->get();

        $student = DB::table('studentprofiles')
        ->select('studentprofiles.*', 'countries.name as countryname', 'states.name as statename', 'cities.name as cityname','classes.class_display_name as classname','users.name as name','users.email as em','users.username as username',/*'sectionclasses.section_display_name as sectionname'*/'feesubmits.pend_m as pm','feesubmits.paid_months as sm','feesubmits.*','feesubmits.sub_total as tm','feesubmits.count_pending as cp','feesubmits.updated_at as up')
			->join('schools', 'schools.id', '=', 'studentprofiles.schoolid')
			->join('countries', 'countries.id', '=', 'studentprofiles.countryid')
			->join('feesubmits', 'feesubmits.studentid', '=', 'studentprofiles.id')
			->join('classes', 'classes.id', '=', 'studentprofiles.classid')
			->join('users', 'users.id', '=', 'studentprofiles.student_user_id')
			//->join('months')
			//->join('sectionclasses', 'sectionclasses.id', '=', 'studentprofiles.sectionid')
			->join('states', 'states.id', '=', 'studentprofiles.stateid')
			->join('cities', 'cities.id', '=', 'studentprofiles.cityid')
			->where('feesubmits.current_user_id',$staffmember->userid)
			->get();
			//DB::enableQueryLog();
			//var_dump($student);
			        //dd($student);

			//$id = '1,2';
              //  $months = DB::select( DB::raw("SELECT * FROM months WHERE id IN ($id)") );
        //$dt = date("d-m-Y", $student->up);
        //  request pen
        
        $pend_m = DB::table('feesubmits')->select('feesubmits.*')->whereNull('pend_m')->where('current_user_id',$staffmember->userid)->get();
        
        return view('feestatus.index',compact('accnt_id','student','country','state','city','class','pend_m'));
    }
    public function store(Request $request)
    {
       
        //dd($request);
                    $principalid =  DB::table('schools')->where('id',$request->schoolid)->get()->pluck('userid');
                    //dd($principalid[0]);
                    
        $data = 'Notification for Fee Status';
        //$id = $route_data->id;
        if($request->student == '1')
        {
        Notification::send(User::find($request->studentid),new Feestatus($data));
        }
        if($request->principal == '1')
        {
        Notification::send(User::find($principalid[0]),new Feestatus($data));
        }
        //return view('feestatus.store');
                return redirect()->back()->with("success","Notification Send");

        //return back()->withMessage("Notification Send");


    }
}
