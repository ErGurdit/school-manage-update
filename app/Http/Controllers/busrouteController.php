<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\school;
use App\busroute;
use App\User;
use App\staffmember;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class busrouteController extends Controller
{
    //redirect page
   public function index(Request $request){
       $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
       $busroute = busroute::where('schoolid',$staffmember->schoolid)->get();
       
       return view('principaltest.busroute.index',compact('school','busroute'));
   }
   
   //store data
   public function store(Request $request){
       $busroute = new busroute;
       $busroute->schoolid =Input::get('schoolid');
       $busroute->routename = Input::get('destination');
       $busroute->fare = Input::get('fare');
       $busroute->save();
       
       $message = "Route saved successfully";
       
       return back()->with($message);
   }
   
   //status change
   public function ajaxChangeStatus( $id, Request $request )
    {
        $new_status = $request->get( 'status' );
        $busroute = busroute::where( 'id', $id )->first();
        $busroute->status = $new_status;
        $busroute->save();
    }
    
    //update data
    public function update(Request $request,$id)
    {
        $busroute = busroute::where('id',$id)->first();
        $busroute->routename = Input::get('destination');
        $busroute->fare = Input::get('fare');
        $busroute->save();
        
        $message = "Route Update successfully";
       
       return back()->with($message);
    }
    
    //fetch data
    public function fetchdata($busid)
    {
        $busroute = busroute::where('id',$busid)->first();
        return view('principaltest.busroute.busroute',compact('busroute'));
    }
    
    public function destroy(Request $request,$id)
    {
        
        DB::table('busroutes')->where('id',$id)->delete();
        return back()->withMessage("Busroute Deleted Deleted");
    }
    
    
}
