<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\defaultsetting;
use slider;
use App\sociallink;
use Auth;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;

class defaultsettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Auth::user()->id);
        //$defaultsetting = defaultsetting::where('userid',Auth::user()->id)->first();
        $defaultsetting = DB::table('defaultsettings')
        ->select('defaultsettings.id as did','defaultsettings.id as userid','defaultsettings.mobile','defaultsettings.email','sociallinks.fb','sociallinks.twitter','sociallinks.insta','sociallinks.gp','sociallinks.fbstatus','sociallinks.tweetstatus','sociallinks.instastatus','sociallinks.gpstatus')
        ->join('sociallinks','sociallinks.deafultid','=','defaultsettings.id')
        ->where('defaultsettings.userid',Auth::user()->id)->first();
       
        // $slider = slider::where('status',1)->get();
        
        return view('admin.defaultsettings.index',compact('defaultsetting','slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutus(Request $request){
        $defaultsetting = defaultsetting::where('userid',Auth::user()->id)->first();
        //dd($defaultsetting->aboutus);
        return view('admin.defaultsettings.aboutus',compact('defaultsetting'));
    }
     //about us update
    public function aboutusupdate(Request $request,$id)
    {
        $defaultsetting = defaultsetting::where('userid',$id)->first();
        $defaultsetting->aboutus = Input::get('aboutus');
        $defaultsetting->save();
        return redirect('aboutus');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(Input::get('mobile'));
        $defaultsetting = new defaultsetting;
        $defaultsetting->userid = Auth::user()->id;
        $defaultsetting->websitetitle = Input::get('webstitle');
        $defaultsetting->mobile = Input::get('mobile');
        $defaultsetting->email = Input::get('email');
        $defaultsetting->save();
        $social = new sociallink;
        $social->deafultid = $defaultsetting->id;
        $social->fb = Input::get('fb_link');
        $social->twitter = Input::get('twitter_link');
        $social->insta =Input::get('instagram_link');
        $social->gp = Input::get('gp_link');
        $social->save();
       
        
    
    return redirect('defaultsetting');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $defaultsetting = defaultsetting::where('userid',$id)->first();
        $defaultsetting->websitetitle = Input::get('webstitle');
        $defaultsetting->mobile = Input::get('mobile');
        $defaultsetting->email = Input::get('email');
        $defaultsetting->save();
        $social = sociallink::where('deafultid',$defaultsetting->id)->first();
        $social->fb = Input::get('fb_link');
        $social->twitter = Input::get('twitter_link');
        $social->insta =Input::get('instagram_link');
        $social->gp = Input::get('gp_link');
        $social->fbstatus = Input::get('fbstatus');
        $social->tweetstatus = Input::get('tweetstatus');
        $social->instastatus = Input::get('instastatus');
        $social->gpstatus= Input::get('gpstatus');
        $social->save();
        return redirect('defaultsetting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
