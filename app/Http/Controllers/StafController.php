<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Role;
use App\Permission;
use DB;
use Entrust;
use App\User;
use App\staffmember;
use Auth;
use Validator;
use App\school;
use App\classes;
use App\staffmemberclass;
use App\staffmemberclassesnew;
use Zizaco\Entrust\EntrustRole;
use Redirect;
use App\holiday;
use App\subject;
use App\highclass;
use App\teacherprofile;

class StafController extends Controller
{
    public function index(Request $request)
    {
          //dd($request);
          if(Auth::guest()){
            return redirect('login');
            }
            else{
            $id = Auth::user()->id;
            $user = User::where('id',$id)->first();
            $school = school::where('userid',$user->id)->first();
            $class = classes::where('status',0)->get();
            $subject = subject::all();
            $highclass = highclass::all();
            $role = role::where('role_create',1)->get();
            }
            
            
            // $staffmember = DB::table('staffmembers')
            // ->select('users.name as uname','users.email as uemail','users.username as username','users.id as uid','staffmembers.doj','staffmembers.salary','staffmembers.total_experience','roles.name as rname','roles.display_name as rdisplay','staffmemberclasses.classid','staffmemberclasses.streamid','highclasses.highsubname','classes.class_display_name')
            // ->join('users','users.id','=','staffmembers.userid')
            // ->join('role_user','role_user.user_id','=','users.id')
            // ->join('roles','roles.id','=','role_user.role_id')
            // ->join('staffmemberclasses','staffmemberclasses.staffid','=','staffmembers.id')
            // ->join('classes','staffmemberclasses.classid','=','classes.id')
            // ->join('highclasses','staffmemberclasses.streamid','=','highclasses.id')
            // ->where('staffmembers.schoolid',$school->id)
            // ->groupBy('staffmemberclasses.staffid')
            // ->get();
            //dd($staffmember);
            $staffmember = DB::table('staffmembers')
            ->select('users.name as uname','users.email as uemail','users.username as username','users.id as uid','staffmembers.doj','staffmembers.salary','staffmembers.total_experience','staffmemberclassesnews.classid','staffmemberclassesnews.streamid','staffmemberclassesnews.roleid')
            ->join('users','users.id','=','staffmembers.userid')
            ->join('staffmemberclassesnews','staffmemberclassesnews.staffid','=','staffmembers.id')
            ->where('staffmembers.schoolid',$school->id)
            ->get();

            
            $myvar = array();
            $myuser= array();
            
            $roleid = array();$classid = '';$streamid = '';$userid=array();$userdata=array();
             foreach($staffmember as $staffmembers){
                $roleid[] = json_decode($staffmembers->roleid);
                $classid = json_decode($staffmembers->classid);
                $streamid = json_decode($staffmembers->streamid);
                $rolearray = json_decode($staffmembers->roleid);
                $userid[] = json_decode($staffmembers->uid);
                //dd($rolearray);
                foreach($rolearray[0] as $staffrole){
                $myvar[] = $staffrole;
               }
            }
            
            
           // $myvar = rtrim($myvar,',');
        //   dd($myvar);
            // $newpassrole = array();$newpassclass = array();$newpassstream = array();
            // if(!empty($roleid)){
            //     foreach($roleid as $k => $value){
            //         $newpassrole[] = $value;
            //     }
            // }
            
            $rolequery = DB::table('roles')
                        ->whereIn('roles.id',$myvar)
                        ->get();
                
            
            $userdata = DB::table('role_user')
                        ->select('roles.*','users.*','roles.id as rid','users.id as uid')
                        ->join('roles','roles.id','=','role_user.role_id')
                        ->join('users','users.id','=','role_user.user_id')
                        ->get();
                        
            
              
            if(!empty($classid)){
                foreach($classid as $k => $value){
                    $newpassclass = $value;
                }
            }
            if(!empty($streamid)){
                foreach($streamid as $k => $value){
                    $newpassstream = $value;
                }
            }
             
            $testrole = array();$testclass = '';$teststream = '';
            $overtestrole = array();$overtestclass = [];$overteststream = [];
            //role
            if(!empty($newpassclass)){
                foreach($newpassclass as $new){
                    $testclass = DB::table('classes')
                            ->select('id','class_display_name')
                            ->where('id',$new)->get();
                    foreach($testclass as $newsub){
                    
                        $overtestclass[] = ['id' => $newsub->id,'class_display_name' => $newsub->class_display_name];
                       
                    }
                }
            }
            if(!empty($newpassstream)){
                foreach($newpassstream as $new){
                    $teststream = DB::table('highclasses')
                            ->select('id','highsubname')
                            ->where('id',$new)
                            ->get();
                    foreach($teststream as $newsub){
                    
                        $overteststream[$newsub->id] = ['id' => $newsub->id,'highsubname' => $newsub->highsubname];
                       
                    }
                }
            }
            
            $servicerole = $rolequery;$serviceclass = $overtestclass;$servicestream = $overteststream;
            
            
            $servicedataroleid = array();
            $servicedatarolename = array();
            $servicedataclassid = array();
            $servicedataclassname = array();
            $servicedatastreamid = array();
            $servicedatastreamname = array();
            foreach($servicerole as $v){
                $servicedataroleid[] = $v->id;
                $servicedatarolename = $v->display_name;
            }
            //dd($servicedataroleid);
            foreach($serviceclass as $v){
                $servicedataclassid[$v['id']] = $v['id'];
                $servicedataclassname[$v['class_display_name']] = $v['class_display_name'];
            }
            foreach($servicestream as $v){
                $servicedatastreamid[$v['id']] = $v['id'];
                $servicedatastreamname[$v['highsubname']] = $v['highsubname'];
            }
            // $staffmemberstream = DB::table('highclasses')
            // ->join('staffmemberclasses','staffmemberclasses.streamid','=','highclasses.id')
            // ->get();
                  $userid = Auth()->user()->id;
                  $schoolid = school::where('userid',$userid)->first();
                  $staffmember = staffmember::where('userid',$userid)->first();
                  $school =  school::where('id',$staffmember->schoolid)->first();
                  $sch_i = $staffmember->schoolid;

        return view('principaltest.staff.index',compact('sch_i','staffmember','role','class','school','subject','highclass','staffmemberstream','servicerole','serviceclass','servicedataroleid','servicedataclassid','servicedatastreamid'));
    }
    
    //store
    
    public function store(Request $request){
        
        if(empty($request->roles)){
            
            $messages = "Please Select atleast one role";
            return Redirect::back()->withInput()->withErrors($messages);
        }
        
        $rules = array(

			'name' => 'required',

			'email' => 'required|email|unique:users',
			
			'password' => 'required',
			
			'username' => 'required',
			
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$messages = $validator->messages();

			return Redirect::back()->withInput()->withErrors($validator);

		} elseif ($validator->passes()) {
        
        
            //date of joining check
            if(!empty(Input::get('doj'))){
            $doj = Input::get('doj');
            $res = explode("/", $doj);
            $doj = $res[0]."-".$res[1]."-".$res[2];
            }else{
               $doj = ''; 
            }
            //useradd
            $user = new user();
            $user->name =Input::get('name');
            $user->email =Input::get('email');
            $user->password = bcrypt(Input::get('password'));
            $user->username =Input::get('username');
            $user->role_create = Auth::user()->id;
            $user->save();
            
            //staff add other information according to userid
            $staff= new staffmember;
            $staff->userid = $user->id;
            $staff->schoolid = Input::get('schoolid');
            $staff->doj = $doj;
            $staff->salary = Input::get('salary');
            $staff->total_experience = Input::get('totaexp');
            $staff->save();
            
            //dd($request->classes);
            //staffmemberassign
            
            // if(!empty($request->classes) ||!empty($request->stream))
            // {
            //     foreach($request->classes as $classes)
            //     {
            //         foreach($request->stream as $streams)
            //         {
            //             foreach($request->roles as $roles)
            //             {
            //                 $classassign = new staffmemberclass;
            //                 $classassign->staffid = $staff->id;
            //                 $classassign->classid = $classes;
            //                 $classassign->streamid = $streams;
            //                 $classassign->roleid = $roles;
            //                 $classassign->save();
            //             }
            //         }
            //     }
            // }
            
            $role = array($request->roles);
            $stream = array($request->stream);
            $classes = array($request->classes);
            //dd(json_encode($data));
            $classassign = new staffmemberclassesnew;
            $classassign->staffid = $staff->id;
            $classassign->classid = json_encode($classes);
            $classassign->streamid = json_encode($stream);
            $classassign->roleid = json_encode($role);
            $classassign->save();
            
            // elseif(!empty($request->roles))
            // {
            //     foreach($request->roles as $roles)
            //       {
            //         $classassign = new staffmemberclass;
            //         $classassign->staffid = $staff->id;
            //         $classassign->roleid = $roles;
            //         $classassign->save();
            //       } 
            // }
            // elseif(!empty($request->roles))
            // {
            //   foreach($request->roles as $roles)
            //   {
            //     $classassign = new staffmemberclass;
            //     $classassign->staffid = $staff->id;
            //     $classassign->roleid = $roles;
            //     $classassign->save();
            //   } 
            // }
            
            
            //roleassign
            $roles = $request->roles;
            if(!empty($roles))
            {
                $user = User::where('id',$user->id)->first();
                foreach($roles as $role){
                    $user->attachRole($role);
                }
            }
		}
		//profile pic data insert
		$profile = new teacherprofile;
		$profile->userid = $user->id;
		$profile->schoolid = Input::get('schoolid');
		$profile->staffid = $staff->id;
		$profile->save();
       return redirect('staff'); 
        
    }
    
    //update
    
    public function update(Request $request,$id){
        
        
       //date of joining check
            if(!empty(Input::get('doj'))){
            $doj = Input::get('doj');
            $res = explode("-", $doj);
            $doj = $res[0]."-".$res[1]."-".$res[2];
            }else{
               $doj = ''; 
            }
            //useradd
            $user = User::where('email',Input::get('email'))->first();
            $user->name =Input::get('name');
            $user->save();
            
            //staff add other information according to userid
            $staff= staffmember::where('userid',$id)->first();
            $staff->schoolid = Input::get('schoolid');
            $staff->doj = $doj;
            $staff->salary = Input::get('salary');
            $staff->total_experience = Input::get('totaexp');
            $staff->save();
            
            //staffmemberassign
            // if(!empty($request->classes))
            // {
            //     foreach($request->classes as $classes)
            //     {
            //     $classassign = staffmemberclass::where('staffid',$staff->id)->first();
            //     $classassign->classid = $classes;
            //     $classassign->save();
            //     }
            // }
            // elseif(!empty($request->roles))
            // {
            //   foreach($request->roles as $roles)
            //   {
            //     $classassign = staffmemberclass::where('staffid',$staff->id)->first();
            //     $classassign->roleid = $roles;
            //     $classassign->save();
            //   } 
            // }
            
            $role = array($request->roles);
            $stream = array($request->stream);
            $classes = array($request->classes);
            //dd(json_encode($data));
            $classassign = staffmemberclassesnew::where('staffid',$staff->id)->first();
            $classassign->staffid = $staff->id;
            $classassign->classid = json_encode($classes);
            $classassign->streamid = json_encode($stream);
            $classassign->roleid = json_encode($role);
            $classassign->save();
            
            //roleassign
            DB::table('role_user')->where('user_id',$user->id)->delete();
            $roles = $request->roles;
            if(!empty($roles))
            {
                $user = User::where('id',$user->id)->first();
                
                foreach($roles as $role){
                    print_r($role);
                    $user->attachRole($role);
                }
            }
       return redirect('staff'); 
        
    }
    
    public function destroy(Request $request,$id){
         DB::table('users')->where('id',$id)->delete();
         return back()->withMessage("Staff Deleted");
    }
    
    //Holiday crud set
    
    public function hindex(Request $request){
        $user = User::where('id',Auth::user()->id)->first();
        $school = school::where('userid',$user->id)->first();
        $holiday = holiday::where('schoolid',$school->id)->get();
        //dd($school);
        return view('principaltest.holiday.index',compact('holiday','school'));
    }
    
    public function hstore(Request $request){
        
        $holiday = new holiday;
        $holiday->schoolid = Input::get('schoolid');
        $holiday->startdate = Input::get('from');
        $holiday->enddate = Input::get('to');
        $holiday->description = Input::get('pagedataset');
        $holiday->save();
        
        return redirect('holiday');
    }
    
    public function hupdate(Request $request,$id){
        $holiday = holiday::where('id',$id)->first();
        $holiday->startdate = Input::get('from');
        $holiday->enddate = Input::get('to');
        $holiday->description = Input::get('pagedataset');
        $holiday->save();
        
        return redirect('holiday');
    }
    
    public function hdestroy(Request $request,$id){
         DB::table('holidays')->where('id',$id)->delete();
         return back()->withMessage("Holidays Deleted");
    }
    
    public function fetchdata( $staff_member_id )
    {
        if(Auth::guest()){
            return redirect('login');
        } else {
            
            
            // get the memeber detail
            $staffmember = DB::table( 'staffmembers' )
            ->select('users.name as uname','users.email as uemail','users.username as username','users.id as uid','staffmembers.doj','staffmembers.salary','staffmembers.total_experience','staffmemberclassesnews.classid','staffmemberclassesnews.streamid','staffmemberclassesnews.roleid')
            ->join('users','users.id','=','staffmembers.userid')
            ->join('staffmemberclassesnews','staffmemberclassesnews.staffid','=','staffmembers.id')
            ->where( 'staffmembers.userid', $staff_member_id )
            ->get();
            
            $staffmembers = $staffmember->first();
            
            $id = Auth::user()->id;
            $user = User::where('id',$id)->first();
            $school = school::where('userid',$user->id)->first();
            
            $highclass = highclass::all();
            
            $role = role::where('role_create',1)->get();
            
            $class = classes::where('status',0)->get();
            
            
            
            
            
            
            
            $classid = '';
            $streamid = '';
            foreach($staffmember as $staffmembers){
                $classid = json_decode($staffmembers->classid);
                $streamid = json_decode($staffmembers->streamid);
            }
            if(!empty($classid)){
                foreach($classid as $k => $value){
                    $newpassclass = $value;
                }
            }
            if(!empty($newpassclass)){
                foreach($newpassclass as $new){
                    $testclass = DB::table('classes')
                            ->select('id','class_display_name')
                            ->where('id',$new)->get();
                    foreach($testclass as $newsub){
                    
                        $overtestclass[] = ['id' => $newsub->id,'class_display_name' => $newsub->class_display_name];
                       
                    }
                }
            }
            $overtestclass = [];
            $serviceclass = $overtestclass;
            $servicedataclassid = array();
            
            foreach($serviceclass as $v){
                $servicedataclassid[$v['id']] = $v['id'];
                $servicedataclassname[$v['class_display_name']] = $v['class_display_name'];
            }
            
            $overteststream = [];
            
            if(!empty($streamid)){
                foreach($streamid as $k => $value){
                    $newpassstream = $value;
                }
            }
            if(!empty($newpassstream)){
                foreach($newpassstream as $new){
                    $teststream = DB::table('highclasses')
                            ->select('id','highsubname')
                            ->where('id',$new)
                            ->get();
                    foreach($teststream as $newsub){
                    
                        $overteststream[$newsub->id] = ['id' => $newsub->id,'highsubname' => $newsub->highsubname];
                       
                    }
                }
            }
            $servicestream = $overteststream;
            
            $servicedatastreamid = array();
            foreach($servicestream as $v){
                $servicedatastreamid[$v['id']] = $v['id'];
                $servicedatastreamname[$v['highsubname']] = $v['highsubname'];
            }
            
            return view( 'principaltest.staff.ajax_memberform', compact( 'staffmembers', 'school', 'role', 'class', 'servicedataclassid', 'highclass', 'servicedatastreamid' ) );
        }
    }
    
    public function fetchdataholiday( $holiday_id )
    {
        if(Auth::guest()){
            return redirect('login');
        } else {
        
        //return $holiday_id;
        $user = User::where('id',Auth::user()->id)->first();
        $school = school::where('userid',$user->id)->first();
        $holidays = holiday::where('id',$holiday_id)->first();
        //dd($school);
        return view('principaltest.holiday.holiday',compact('holidays','school'));        
            
        }
    }
}
