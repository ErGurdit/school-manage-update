<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\slider;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use Entrust;
use App\attendence;
use App\classes;
use App\sectionclass;
use App\staffmember;
use App\school;

class AttendenceController extends Controller
{
    public function index(Request $request)
    {
        
        $classes = classes::all();
        $section = sectionclass::all();
        $schoolid = school::where('userid',Auth::user()->id)->first();
        $teacher = staffmember::where('userid',Auth::user()->id)->first();
        $attendencestudnet = attendence::all();
        if(!empty($attendencestudnet)){
            //echo '11';
            $attendence = DB::table('attendences')
                        ->select('attendences.id as aid','attendences.*','studentprofiles.id as sid','studentprofiles.*','users.id as uid','users.name as uname','users.*','classes.id as classid','classes.*','sectionclasses.*')
                        ->join('studentprofiles','studentprofiles.id','=','attendences.studentid')
                        ->join('users','users.id','=','studentprofiles.userid')
                        ->join('classes','classes.id','=','attendences.classid')
                        ->join('sectionclasses','sectionclasses.id','=','attendences.sectionid')
                        ->where('attendences.schoolid',$schoolid->id)
                        ->get();
        }else{
            $attendence = '';
        
        }
        $school = school::where('userid',Auth::user()->id)->first();

        $sch_i = $school->id;

        return view('principaltest.Attendence.index',compact('sch_i','classes','section','attendence','teacher'));  
    }
    
    public function fetchstudent(Request $request)
    {
        $dd = $request->dd;
        $mm = $request->mm;
        $yy = $request->curr_year;
        $newdate = $dd.'/'.$mm.'/'.$yy;
        $previous = $request->previousdate;
        $previousnewdate = explode('/',$previous);
        $previoussetnewdate = $previousnewdate[1].'/'.$previousnewdate[0].'/'.$previousnewdate[2];
        //dd($previoussetnewdate);
        $currentdate = date('d/m/Y');
        //dd($newdate);
        // return $newdate;
        if($newdate == $previoussetnewdate){
            
            $studentdata = DB::table('admissions')
                            ->select('studentprofiles.id as sid','studentprofiles.*','classes.id as cid','classes.*'
                            ,'sectionclasses.id as sectionid','sectionclasses.*','users.id as uid','users.name as uname','users.*')
                            ->join('studentprofiles','studentprofiles.id','=','admissions.studentid')
                            ->join('classes','classes.id','=','admissions.classid')
                            ->join('sectionclasses','sectionclasses.id','=','admissions.sectionid')
                            ->join('users','users.id','=','studentprofiles.userid')
                            ->where('classid',$request->classid)
                            ->where('sectionid',$request->sectionid)
                            ->get();
          
           return json_encode($studentdata);
        }else{
       
            $studentdata = DB::table('attendences')
                            ->select('attendences.id as aid','attendences.*','studentprofiles.id as sid','studentprofiles.*','users.id as uid','users.name as uname','users.*')
                            ->join('studentprofiles','studentprofiles.id','=','attendences.studentid')
                            ->join('users','users.id','=','studentprofiles.userid')
                            ->where('classid',$request->classid)
                            ->where('sectionid',$request->sectionid)
                            ->where('attendence_date',$currentdate)
                            ->get();
          
           return json_encode($studentdata);
        }
    }
    public function fetchpreviousattendence(Request $request)
    {
        
    }
    
    public function fetchattendence($attendence_id)
    {
        //return $attendence_id;
         $sliders = DB::table('sliders')
                ->select('sliders.*','users.username as uname')
                ->join('users','users.id','=','sliders.userid')
                ->where('sliders.id',$attendence_id)
                ->first();
                
        return view('principaltest.Attendence.attendence',compact('sliders','sliderinactive','slideractive'));  
    }
    
    public function attenddencestore(Request $request)
    {
        $i=0;
        $chardate = $request->previousdate;
        $newchardate = explode('/',$chardate);
        $chardatecheck = $newchardate[1].'/'.$newchardate[0].'/'.$newchardate[2];
        $numdate = date('d/m/Y');
        foreach($request->studentid as $studentid)
            {
                $attendence = new attendence;
                //$attendence->userid = $userid;
                $attendence->schoolid = Input::get('schoolid');
                $attendence->teacherid = Input::get('teacherid');
                $attendence->classid = Input::get('classid');
                $attendence->sectionid = Input::get('sectionid');
                $attendence->studentid = $studentid;
                $attendence->attendence_date = $chardatecheck;
                $attendence->numdate = $numdate;
                $attendence->status = Input::get('studentattendence-'.$i++);
                $attendence->save();
            }
        dd($attendence);
    }
    
}
