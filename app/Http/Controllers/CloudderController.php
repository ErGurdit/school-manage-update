<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Cloudder;
use App\schoolgalleryimage;
use Auth;
use App\User;
use App\school;


class CloudderController extends Controller
{
     public function getFile(){  
        $schoolid = school::where('userid',Auth::user()->id)->first();
        $gallery = schoolgalleryimage::where('schoolid',$schoolid->id)->get(); 
        return view('principaltest.cloudinary.index',compact('gallery'));
    }
    public function uploadFile(Request $request){
        
          if($request->hasFile('image_file')){  
            Cloudder::upload($request->file('image_file'));
            $c=Cloudder::getResult();       
            if($c){
            $schoolid = school::where('userid',Auth::user()->id)->first();
            $gallery = new schoolgalleryimage;
            $gallery->schoolid = $schoolid->id;
            $gallery->satffid = 0;
            $gallery->public_id = $c['public_id'];
            $gallery->version = $c['version'];
            $gallery->signature = $c['signature'];
            $gallery->width = $c['width'];
            $gallery->height = $c['height'];
            $gallery->format = $c['format'];
            $gallery->resource_type = $c['resource_type'];
            $gallery->created_at_image = $c['created_at'];
            $gallery->tags = 0;
            $gallery->bytes = $c['bytes'];
            $gallery->type = $c['type'];
            $gallery->placeholder = $c['placeholder'];
            $gallery->url = $c['url'];
            $gallery->secure_url = $c['secure_url'];
            $gallery->original_filename = $c['original_filename'];
            $gallery->save();
               return back()
                    ->with('success','You have successfully upload images.')
                    ->with('image',$c['url']);
            }
            
            
        }
    }

}
