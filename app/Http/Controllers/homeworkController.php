<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use App\classes;
use App\sectionclass;
use App\User;
use App\teacherprofile;
use App\subject;
use App\homework;
use App\staffmember;
use App\school;
use Notification;
use App\Notifications\RepliedToThread;


class homeworkController extends Controller
{
    public function index(Request $request){
        $classes = classes::all();
        $section = sectionclass::all();
        $subject = subject::all();
        $homework = DB::table('homeworks')
                    ->select('homeworks.id as hid','classes.id as cid','sectionclasses.id as sectionid',
                    'subjects.id as subid','schools.id as schoolid','teacherprofiles.id as teacherid',
                    'users.id as userid','homeworks.*','classes.*','classes.*','sectionclasses.*','subjects.*','schools.*')
                    ->join('users','users.id','=','homeworks.user_id')
                    ->join('teacherprofiles','teacherprofiles.id','=','homeworks.teacherid')
                    ->join('schools','schools.id','=','homeworks.schoolid')
                    ->join('classes','classes.id','=','homeworks.classid')
                    ->join('sectionclasses','sectionclasses.id','=','homeworks.sectionid')
                    ->join('subjects','subjects.id','=','homeworks.subjectid')
                    ->get();
        
        //dd($section);
        $profile = teacherprofile::where('userid',Auth()->user()->id)->first();
        return view('principaltest.homework.index',compact('classes','section','subject','homework','profile'));
    }
    
    public function store(Request $request)
    {
        //dd(Auth::user()->id);
        $staffmember = DB::table('staffmembers')
                        ->select('staffmembers.id as sid','staffmembers.*','staffmemberclassesnews.id as smid','staffmemberclassesnews.*')
                        ->join('staffmemberclassesnews','staffmemberclassesnews.staffid','=','staffmembers.id')
                        //->join('role_user','role_user.user_id','=','staffmembers.userid')
                         //->join('roles','roles.id','=','role_user.role_id') 
                         ->where('userid',Auth::user()->id)
                        ->first();
        //dd($staffmember);
                   
       if(empty($data)){
        
        $data = json_decode($staffmember->roleid);
        
       
        
        $newpass = array();
        foreach($data as $k => $value){
            $newpass = $value;
        }
        
      
        $test = '';
        $overtest = [];
        foreach($newpass as $new){
            $test = DB::table('roles')
                    ->select('id','name')
                    ->where('id',$new)->get();
        foreach($test as $newsub){
        
            $overtest[] = ['id' => $newsub->id];
           
        }
        }
         
        
        $service = $overtest[0]['id'];
        }
        
        
        
        
        $teacher = teacherprofile::where('staffid',$staffmember->sid)->first();
        $school = school::where('id',$staffmember->schoolid)->first();
        $class = classes::where('id',Input::get('classsid'))->first();
        $section = sectionclass::where('id',Input::get('sectionid'))->first();
        $subject = subject::where('id',Input::get('subjectid'))->first();
        $homework = new homework;
        $homework->schoolid = $staffmember->schoolid;
        $homework->teacherid = $teacher->id;
        $homework->user_id = $staffmember->userid;
        $homework->role_id = $service;
        $homework->classid = Input::get('classsid');
        $homework->sectionid = Input::get('sectionid');
        $homework->subjectid = Input::get('subjectid');
        $homework->title = Input::get('title');
        $homework->description = Input::get('description');
        $homework->save();
        
        $note = 'Teacher assign Homwork for Class'.$class.'Section'.$section.'Subject'.$subject;
        Notification::send(User::find($school->userid),new RepliedToThread($note));
        
        $message = 'Homework Added Successfully';
        
        return redirect()->back()->with('message', $message);
    }
    public function fetchdata($homework_id)
    {
        $classes = classes::all();
        $section = sectionclass::all();
        $subject = subject::all();
        $homeworks = DB::table('homeworks')
                    ->select('homeworks.id as hid','classes.id as cid','sectionclasses.id as sectionid',
                    'subjects.id as subid','schools.id as schoolid','teacherprofiles.id as teacherid',
                    'users.id as userid','homeworks.*','classes.*','classes.*','sectionclasses.*','subjects.*','schools.*')
                    ->join('users','users.id','=','homeworks.user_id')
                    ->join('teacherprofiles','teacherprofiles.id','=','homeworks.teacherid')
                    ->join('schools','schools.id','=','homeworks.schoolid')
                    ->join('classes','classes.id','=','homeworks.classid')
                    ->join('sectionclasses','sectionclasses.id','=','homeworks.sectionid')
                    ->join('subjects','subjects.id','=','homeworks.subjectid')
                    ->where('homeworks.id',$homework_id)
                    ->first();
                //dd($homeworks);
        return view('principaltest.homework.homework',compact('classes','section','subject','homeworks'));
    }
    
    public function update(Request $request)
    {
        $staffmember = staffmember::where('userid',Auth::user()->id)->first();
        $teacher = teacherprofile::where('staffid',$staffmember->id)->first();
        $homework = homework::where('id',Input::get('homeworkid'))->first();
        $homework->schoolid = $staffmember->schoolid;
        $homework->teacherid = $teacher->id;
        $homework->user_id = $staffmember->userid;
        $homework->classid = Input::get('classsid');
        $homework->sectionid = Input::get('sectionid');
        $homework->subjectid = Input::get('subjectid');
        $homework->title = Input::get('title');
        $homework->description = Input::get('description');
        $homework->save();
        
        $message = 'Homework Update Successfully';
        
        return redirect()->back()->with('message', $message);
    }
    
    public function destroy(Request $request,$id)
    {
        
        DB::table('homeworks')->where('id',$id)->delete();
        return back()->withMessage("Homwork Deleted Deleted");
    }
}
