<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\country;
use App\state;
use App\city;
use App\staffmember;
use App\school;
use App\classes;
use DB;
use App\User;
class DayBookController extends Controller
{
    //
    public function show()
    {
    $country = country::all();
        $state = state::all();
        $city = city::all();
        $class = classes::all();
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();

        $student = DB::table('studentprofiles')
        ->select('studentprofiles.*',DB::raw('studentprofiles.student_user_id, COUNT(*) as count'),'users.name as name','users.username as username','feesubmits.pend_m as pm','feesubmits.paid_months as sm','feesubmits.sub_total as tm',DB::raw('sum(feesubmits.amount) AS totalfees'),'feesubmits.updated_at as up')
			->join('feesubmits', 'feesubmits.studentid', '=', 'studentprofiles.id')
			->join('users', 'users.id', '=', 'studentprofiles.student_user_id')
				->where('feesubmits.updated_at','=', DB::raw('curdate()'))
				->where('studentprofiles.userid','=', $staffmember->userid)
			->get();
			
			$current_total_fees = DB::table('feesubmits')
           ->select('feesubmits.*',DB::raw('sum(feesubmits.amount) as tm'),'feesubmits.*')
           ->where('feesubmits.schoolid',$school->id)
           //->where('approve_moneys.status', 1)
           ->where('feesubmits.updated_at','=', DB::raw('curdate()'))
           ->get();
			
			$studen = DB::table('studentprofiles')
        ->select('studentprofiles.*','users.name as name','users.username as username','feesubmits.pend_m as pm','feesubmits.paid_months as sm','feesubmits.total_amount as tm','feesubmits.updated_at as up')
			->join('feesubmits', 'feesubmits.studentid', '=', 'studentprofiles.id')
			->join('users', 'users.id', '=', 'studentprofiles.student_user_id')
				->where('feesubmits.updated_at','=', DB::raw('curdate()'))
			->get();
			//$id = '1,2';
			$total = DB::table('feesubmits')
			->select(DB::raw('COUNT(*) as st'))
            ->where('feesubmits.updated_at',DB::raw('curdate()'))
			->get()->pluck('st');
			//dd($total[0]);
              //  $months = DB::select( DB::raw("SELECT * FROM months WHERE id IN ($id)") );

        
        return view('daybook.index',compact('current_total_fees','student','studen','total'));
    }
    public function view(Request $request)
    {
       $student = DB::table('feesubmits')
			->select('feesubmits.*','users.name as name')
	    	->join('users', 'users.id', '=', 'feesubmits.userid')
            ->where('feesubmits.updated_at',DB::raw('curdate()'))
			->get();
			        return view('daybook.view',compact('student'));

    }
}
