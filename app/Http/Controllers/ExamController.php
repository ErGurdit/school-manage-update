<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use Redirect;
use App\school;
use App\classes;
use Auth;
use App\holiday;
use App\exam;
use Illuminate\Support\Facades\Input;
use App\examtype;

class ExamController extends Controller
{
    public function index(Request $request)
    {
        $useridfetch = User::where('id',Auth::user()->id)->first();
        $school = school::where('userid',$useridfetch->id)->first();
        $class = classes::all();
        
        
        $schoolstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->join('classes','classes.id','=','studentprofiles.current_class')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        $allstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        
        $examtype = examtype::all();
        $user = DB::table('role_user')
        ->select('users.name as uname','users.username as username','users.email as uemail')
        ->join('users','role_user.user_id','=','users.id')
        ->join('roles','role_user.role_id','=','roles.id')
        ->where('roles.name','student')
        ->get();
        
        
        $exam = DB::table('exams')
        ->select('users.name as uname','users.username as username','users.email as uemail','users.id as userid','exams.total_marks','examtypes.name as ename','exams.id as eid','exams.obtain_marks','exams.percentage','exams.position','exams.result','classes.class_name','exams.classid','exams.examid')
        ->join('examtypes','examtypes.id','=','exams.examid')
        ->join('users','users.id','=','exams.userid')
        ->join('classes','classes.id','=','exams.classid')
        ->get();
        
        
        return view('principaltest.exam.index',compact('user','school','exam','schoolstudent','class','allstudent','examtype','school'));
    }
    
    public function store(Request $request)
    {
        $exam = new exam;
        $exam->examid = Input::get('examtype');
        $exam->userid = Input::get('student_name');
        $exam->classid = Input::get('classselect');
        $exam->schoolid = Input::get('schoolid');
        $exam->total_marks = Input::get('tomarks');
        $exam->obtain_marks = Input::get('obtain_marks');
        $exam->percentage = Input::get('percentage');
        $exam->position = Input::get('position');
        $exam->result = Input::get('result');
        $exam->save();
        
        $message = "Exam Add Successfully";
        
        return redirect()->back()->with($message);
    }
    
    public function update(Request $request,$id)
    {
        $exam = exam::where('id',$id)->first();
        $exam->examid = Input::get('examtype');
        $exam->userid = Input::get('student_name');
        $exam->classid = Input::get('class_select');
        $exam->total_marks = Input::get('tomarks');
        $exam->obtain_marks = Input::get('obtain_marks');
        $exam->percentage = Input::get('percentage');
        $exam->position = Input::get('position');
        $exam->result = Input::get('result');
        $exam->save();
        
        return redirect('exams');
    }
    
    public function destroy(Request $request,$id)
    {
        
        DB::table('exams')->where('id',$id)->delete();
        return back()->withMessage("Expenses Deleted Deleted");
    }
    
    public function fetchdata($exam_id)
    {
        
        $useridfetch = User::where('id',Auth::user()->id)->first();
        $school = school::where('userid',$useridfetch->id)->first();
        $class = classes::all();
        
        
        $schoolstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->join('classes','classes.id','=','studentprofiles.current_class')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        $allstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        
        $examtype = examtype::all();
        $user = DB::table('role_user')
        ->select('users.name as uname','users.username as username','users.email as uemail')
        ->join('users','role_user.user_id','=','users.id')
        ->join('roles','role_user.role_id','=','roles.id')
        ->where('roles.name','student')
        ->get();
        
        $exams = DB::table('exams')
        ->select('users.name as uname','users.username as username','users.email as uemail','users.id as userid','exams.total_marks','examtypes.name as ename','exams.id as eid','exams.obtain_marks','exams.percentage','exams.position','exams.result','classes.class_name','exams.classid','exams.examid')
        ->join('examtypes','examtypes.id','=','exams.examid')
        ->join('users','users.id','=','exams.userid')
        ->join('classes','classes.id','=','exams.classid')
        ->where('exams.id',$exam_id)
        ->first();
        
        return view('principaltest.exam.exam',compact('user','school','exams','schoolstudent','class','allstudent','examtype'));
    }
    
}
