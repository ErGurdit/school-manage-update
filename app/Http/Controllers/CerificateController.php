<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use App\school;
use App\classes;
use Auth;
use App\holiday;
use App\exam;
use Illuminate\Support\Facades\Input;
use App\certificate;
use Cloudder;

class CerificateController extends Controller
{
    public function index(Request $request)
    {
        $userid = Auth::user()->id;
        $schoolid = school::where('userid',$userid)->first();
        //dd($schoolid);
        $studentprofile = DB::table('studentprofiles')
                        ->join('users','users.id','=','studentprofiles.userid')
                        ->where('studentprofiles.schoolid',$schoolid->id)
                        ->get();
        $user = DB::table('users')
                ->join('certificates','certificates.userid','=','users.id')
                ->get();
                //dd($user);
        return view('principaltest.certificate.index',compact('user','studentprofile','schoolid'));
    }
    
    public function store(Request $request)
    {
        $certificate = new certificate;
        $certificate->userid = Input::get('uname');
        $certificate->studentid = Input::get('uname');
        $certificate->schoolid = Input::get('schoolid');
        if ($request->hasFile('certificate')) {

				$files = Input::file('certificate');

				$name = time() . "_" . $files->getClientOriginalName();

				$image = $files->move(public_path() . '/principal/certificate', $name);

				$certificate->certificate = $name;

        }
        $certificate->approved_by = Input::get('approved_by');
        $certificate->approved_date = Input::get('approved_date');
        $certificate->issued_by = Input::get('issued_by');
        $certificate->issued_date = Input::get('issued_date');
        $certificate->received_by = Input::get('received_by');
        $certificate->received_date = Input::get('received_date');
        $certificate->save();
        
        return redirect('certificate');
    }
    
    public function update(Request $request,$id)
    {
        $certificate = certificate::where('id',$id)->first();
        
        $certificate->approved_by = Input::get('approved_by');
        $certificate->approved_date = Input::get('approved_date');
        $certificate->issued_by = Input::get('issued_by');
        $certificate->issued_date = Input::get('issued_date');
        $certificate->received_by = Input::get('received_by');
        $certificate->received_date = Input::get('received_date');
        
        if ($request->hasFile('certificate')) {
            
                Cloudder::upload($request->file('certificate'));
                $c=Cloudder::getResult();       
                if($c){
                    $certificate->certificate = $c['secure_url'];
                }

				// $files = Input::file('certificate');

				// $name = time() . "_" . $files->getClientOriginalName();

				// $image = $files->move(public_path() . '/principal/certificate', $name);

				// $certificate->certificate = $name;

        }
        $certificate->save();
       
        
        return redirect('certificate');
    }
    
    public function destroy(Request $request,$id)
    {//9340619372
        
        DB::table('certificates')->where('id',$id)->delete();
        return back()->withMessage("Expenses Deleted Deleted");
    }
}
