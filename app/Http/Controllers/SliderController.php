<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\slider;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use Entrust;
use Cloudder;
use App\schoolgalleryimage;
use App\teacherprofile;
use App\school;


class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school = school::where('userid',Auth::user()->id)->first();
        $slider = DB::table('sliders')
                ->select('sliders.*','users.username as uname','users.*','schools.*','schools.id as sid')
                ->join('schools','schools.id','=','sliders.userid')
                ->join('users','users.id','=','schools.userid')
                ->where('schools.id',$school->id)
                ->get();
                //dd($slider);
        if(Entrust::hasRole('superadmin')){
          return view('admin.slider.index',compact('slider'));
        }elseif(Entrust::hasRole('principal')){
            //inactive
            $sliderinactive = DB::table('sliders')
                ->select('sliders.*','users.username as uname','users.*','schools.*','schools.id as sid')
                ->join('schools','schools.id','=','sliders.userid')
                ->join('users','users.id','=','schools.userid')
                ->where('sliders.status',0)
                ->where('schools.id',$school->id)
                ->get();
            //active
            $slideractive = DB::table('sliders')
                ->select('sliders.*','schools.*','schools.id as sid','users.*','users.username as uname')
                ->join('schools','schools.id','=','sliders.userid')
                ->join('users','users.id','=','schools.userid')
                ->where('sliders.status',1)
                ->where('schools.id',$school->id)
                ->get();
                //dd($slideractive);
            $profile = teacherprofile::where('userid',Auth()->user()->id)->first();
            $sch_i = $school->id;

          return view('principaltest.slider.index',compact('sch_i','slider','sliderinactive','slideractive','profile'));  
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $rules = array(

			'filename' => 'required|mimes:jpeg,png,jpg,gif,svg|max:3072',

		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$messages = $validator->messages();

			return Redirect::back()->withInput()->withErrors($validator);

		} elseif ($validator->passes()) 
	    {
                $school = school::where('userid',Auth::user()->id)->first();
                $slider = new slider;
                $slider->userid = $school->id;
                $slider->topcontent = Input::get('topcontent');
                $slider->bottomcontent = Input::get('bottomcontent');
                //$files = Input::file('filename');
                if ($request->hasFile('filename')) 
                    {
        				Cloudder::upload($request->file('filename'));
                        $c=Cloudder::getResult();       
                        if($c){
                            $slider->slider = $c['secure_url'];
                        }
                    }

				// $name = time() . "_" . $files->getClientOriginalName();

				// $image = $files->move(public_path() . '/theme/mainimage', $name);

				// $slider->slider = $name;

				//echo public_path() . '/../public_html/images';exit
				$slider->save();
            }
            return redirect('slider');
		}
        
            

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //active status
    public function show(Request $request)
    {
        $slider = DB::table('sliders')
                ->select('sliders.*','users.username as uname')
                ->join('users','users.id','=','sliders.userid')
                ->where('sliders.status',1)
                ->get();
        return view('admin.slider.index',compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxChangeStatus( $id, Request $request )
    {
        $new_status = $request->get( 'status' );
        $school = slider::where( 'id', $id )->first();
        $school->status = $new_status;
        $school->save();
    }
    
    public function statuscheck(Request $request,$id)
    {
        
        $slider = slider::where('id',$id)->first();
        $status=$slider->status;
        
        return json_encode($status);
    }
    
    //inactive status
    public function showin(Request $request)
    {
         $slider = DB::table('sliders')
                ->select('sliders.*','users.username as uname')
                ->join('users','users.id','=','sliders.userid')
                ->where('sliders.status',0)
                ->get();
        return view('admin.slider.index',compact('slider'));
    }
    
   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $slider = slider::where('id',$id)->first();
        $slider->userid = $school->id;
        $slider->topcontent = Input::get('topcontent');
        $slider->bottomcontent = Input::get('bottomcontent');
                //$files = Input::file('filename');
        if ($request->hasFile('filename')) 
            {
				Cloudder::upload($request->file('filename'));
                $c=Cloudder::getResult();       
                if($c){
                    $slider->slider = $c['secure_url'];
                }
            }

		$slider->save();
	    return redirect('slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('sliders')->where('id',$id)->delete();
        return back()->withMessage("School Deleted");
    }
    
    public function fetchdata( $slider_id )
    {
        if(Auth::guest()){
            return redirect('login');
        } else {
            
            $sliders = DB::table('sliders')
                    ->select('sliders.*','users.username as uname')
                    ->join('users','users.id','=','sliders.userid')
                    ->where('sliders.id',$slider_id)
                    ->first();
                    //dd($slider);
            //return $slider;
            
                return view('principaltest.slider.slider',compact('sliders','sliderinactive','slideractive'));  
            
            
        }
    }
}
