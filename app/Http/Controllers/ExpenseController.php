<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Auth;
use App\expense;
use App\User;
use Illuminate\Support\Facades\Input;
use App\Notifications\RepliedToThread;
use App\paymentmethod;
use Notification;
use App\school;

class ExpenseController extends Controller
{
    public function index(){
        $school = school::where('userid',Auth::user()->id)->first();

        $expense = DB::table('expenses')
        ->select('users.username','expenses.name as exname','expenses.expense_amount as examount','expenses.date as exdate','expenses.description as exdescribtion','expenses.id as eid','expenses.userid as userid','expenses.askstatus')
        ->join('users','users.id','=','expenses.userid')
        ->get();
        
        $paymentmethod = paymentmethod::all();
        
        $user= DB::table('users')
        ->select('users.name as uname','users.id as uid','roles.name as rname')
        ->join('role_user','role_user.user_id','=','users.id')
        ->join('roles','role_user.role_id','=','roles.id')
        ->where('users.role_create',1)
        ->get();
        //dd($user);
                         //$sch_i = $school->id;

        
        return view('principaltest.expense.index',compact('expense','user','paymentmethod'));
    }
    
    public function store(Request $request){
        
        $staffmember = DB::table('staffmembers')->where('userid',Auth::user()->id)->first();
       
        $school = DB::table('schools')->where('id',$staffmember->schoolid)->first();
        
        if(!empty(Input::get('expense_date'))){
            $exdate = Input::get('expense_date');
            $res = explode("/", $exdate);
            $exdate = $res[0]."-".$res[1]."-".$res[2];
        }
        $expense = new expense;
        $expense->userid = Auth::user()->id;
        $expense->name = Auth::user()->name;
        $expense->schoolid = $school->id;
        $expense->expense_amount = Input::get('amount');
        $expense->date = date('d-M-Y');
        $expense->description = Input::get('pagedataset');
        $expense->payment_type = Input::get('paymenttype');
        $expense->askstatus = Input::get('askapprove');
        $expense->save();
        
        $thread = Auth::user()->id;
        $note = array('id'=>$expense->id,'amount'=>Input::get('amount'));
        
        // auth()->user()->notify(new RepliedToThread($note));
        //auth()->user()->notify(new RepliedToThread($note));
        Notification::send(User::find($school->userid),new RepliedToThread($note));

        
        return redirect('expenses');
    }
    
    public function  update(Request $request,$id){
        if(!empty(Input::get('expense_date'))){
            $exdate = Input::get('expense_date');
            $res = explode("/", $exdate);
            $exdate = $res[0]."-".$res[1]."-".$res[2];
        }else{
            $exdate = Input::get('expense_date');
            $res = explode("-", $exdate);
            $exdate = $res;
        }
        
        $expense = expense::where('id',$id)->first();
        $expense->userid = Input::get('user_select');
        $expense->name = Input::get('name');
        $expense->expense_amount = Input::get('amount');
        $expense->date = $exdate;
        $expense->description = Input::get('pagedataset');
        $expense->save();
        
        return redirect('expenses');
    }
    
    public function destroy(Request $request,$id)
    {
        
        DB::table('expenses')->where('id',$id)->delete();
        return back()->withMessage("Expenses Deleted Deleted");
    }
    
    public function  approve(Request $request,$id){
        
        $staffmember = DB::table('staffmembers')->where('userid',Input::get('approve'))->first();
        $school = DB::table('schools')->where('id',$staffmember->schoolid)->first();
        $userdata = User::where('id',$school->userid)->first();
        
        $expense = expense::where('id',$id)->first();
        $expense->askstatus = 0;
        $expense->name = Input::get('name');
        $expense->expense_amount = Input::get('amount');
        $expense->description = Input::get('pagedataset');
        $expense->save();
        
        $note = array('id'=>$userdata->id,'amount'=>Input::get('amount'),'expense'=>'Expense Approved');
        
        
        Notification::send(User::find($staffmember->userid),new RepliedToThread($note));
        
        return redirect('expenses');
    }
    
    public function fetchexpense($expense_id)
    {
        
        $expenses = DB::table('expenses')
        ->select('users.username','expenses.name as exname','expenses.expense_amount as examount','expenses.date as exdate','expenses.description as exdescribtion','expenses.id as eid','expenses.userid as userid','expenses.askstatus')
        ->join('users','users.id','=','expenses.userid')
        ->where('expenses.id',$expense_id)
        ->first();
        
        $paymentmethod = paymentmethod::all();
        
        $user= DB::table('users')
        ->select('users.name as uname','users.id as uid','roles.name as rname')
        ->join('role_user','role_user.user_id','=','users.id')
        ->join('roles','role_user.role_id','=','roles.id')
        ->where('users.role_create',1)
        ->get();
        //dd($user);
        
        return view('principaltest.expense.expense',compact('expenses','user','paymentmethod'));
    }
    
}
