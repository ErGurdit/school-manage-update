<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\staffmember;
use App\school;
use App\User;
use DB;
use App\studentprofile;
use App\busroute;
use App\maintainfee;
use App\feesubmits;
use App\month;
use Session;


class FeeSubmissionController extends Controller
{
    //
    
    public function create(Request $request)
    {
        //dd($request);
        //dd($staffmember->schoolid);
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
        //dd($staffmember->schoolid);
        //dd(Auth()->user()->id);
        $busroute = busroute::where('schoolid',$staffmember->schoolid)->get();
        $month = month::all();
            
            $school =  school::where('id',$staffmember->schoolid)->first();
                //allclasses
            $myclass = DB::table('classes')
                            ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                            ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                            ->where('class_to_schools.schoolid',$school->id)
                            ->get();
                            //dd($myclass);
        return view('feesubmit.create',compact('myclass','busroute','month'));
    }
    
    public function store(Request $request)
    {
        //dd($request);
        //SELECT * FROM  `busroutes` WHERE schoolid =73 AND fare =  '1500'
         $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
         $pay = ($request->cash_amount != 0 ) ? 2 : 1;
         
        
         //dd(Auth()->user()->id);
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();
            $tranport = $request->inputRadioscharge ? $request->inputRadioscharge : 0;
        $busrouteid = DB::table('busroutes')->where('schoolid',$school->id)->where('fare',$tranport)->get()->pluck('id');
        //$bus = json_decode($busrouteid[0]);
        $prev_pend = $request->pen_data;
        $monthdatas = $request->mondata;
        $sub_data = $request->sub_data;
        $pend_data = $request->pend_data;
        
        $sub_monts = $request->pendingcharge;
        $prev_fees = ($request->total_fees) == '' ? 0 : $request->total_fees;
        //$prev_amnt = ($request->amount) == '' ? 0 : $request->amount;

        $datas = ltrim(json_encode($monthdatas),'"[');
        $data_sub = str_replace('","',',',$datas);
        $sub_m = str_replace('"]','',$data_sub);
        
        $paid_months = $request->paid_m;
        //dd($tranport);
        $updated_paid_months = str_replace(" ","",$paid_months);
        //dd($updated_paid_months);
        $months = explode(",",$updated_paid_months);
        foreach ($months as $str) {
        $str = str_replace($updated_paid_months,"", $pend_data);
       }
        
        $remove_updated_pen_m = str_replace(","," ",$str);
        
        $append_with_prev_data = $sub_data.",".$sub_m;
        $update_pend = str_replace($sub_m," ",$prev_pend);
        $remove_pend_in = str_replace(",,",",",$update_pend);
        $remove_pend = str_replace(" ,"," ",$remove_pend_in);
        
        //dd($remove_updated_pen_m);
        
        $current_date = date('Y-m-d H:i:s');
        $only_dt = date("Y-m-d",strtotime($current_date));
         $prev_amnt  = DB::table('feesubmits')
         ->select('amount')
         ->where('current_user_id',Auth()->user()->id)
         ->where('id',$request->fee_id)
         ->where('updated_at','=',$only_dt)
         ->get()->pluck('amount');
        
        $cal = json_decode(rtrim(ltrim($prev_amnt,'["'),'"]'));
        $cal = $cal == "" ? 0 : $cal;
        //dd($cal);
        $pend = $request->count_pend;
        //dd(count($sub_m));
        $total = $pend - $sub_monts;
        //dd($sub_m);
        //$busro = ($bus == "null") ? 0 : $bus ;
         $amount  = $request->card_amount ? $request->card_amount : $request->cash_amount;
          $sub = $request->subtotal;
          
          $total_amt  = $sub + $cal;
          
          //dd($total_amt);

          $tot = ($prev_fees) + ($sub);
        
        if($sub_m!="null")
        {
         //dd($append_with_prev_data);
         $paid_months = $request->paid_m;
            
        DB::table('feesubmits')
            ->where('userid', $request->student_lst)
            ->update(['sectionid' => $request->section,
            'classid' => $request->clas,
                       'dresscharge' => $request->dresscharge,
                       'pending_months' => $remove_pend,
                       'pend_m' => $remove_updated_pen_m,
                       'submission_months' => $append_with_prev_data,
                       //'busrouteid'=> $busro,
                       'paid_months' => $paid_months,
                       'transportcharge' => $request->inputRadioscharge,
                       'school_fee' => $request->schfee,
                       'count_pending' => $total,
                       //'donation' => $request->donation,
                       'sub_total' => $sub,
                      'total_amount' => $tot,
                      'amount' => $total_amt,
                      'card_amount' => $request->card_amount,
                      'cash_amount' => $request->cash_amount,
                      'payment' => $pay,
                      'card_digits' => $request->last_digits,
                      'card_holder' => $request->card_holder,
                      'approved_code' => $request->approve_code,
                      'created_at' => $only_dt,
                      'updated_at' => $only_dt,

            ]);
        }
        else
        {
          DB::table('feesubmits')
            ->where('userid', $request->student_lst)
            ->update(['sectionid' => $request->section,
            'classid' => $request->clas,
                       'dresscharge' => $request->dresscharge,
                       //'submission_months' => $append_with_prev_data,
                       //'busrouteid'=> $busro,
                       'transportcharge' => $request->inputRadioscharge,
                       'school_fee' => $request->schfee,
                       //'count_pending' => $total,
                       //'donation' => $request->donation,
                      'sub_total' => $sub,
                      'total_amount' => $tot,
                      'amount' => $total_amt,
                      'payment' => $pay,
                      'card_amount' => $request->card_amount,
                      'cash_amount' => $request->cash_amount,
                      'card_digits' => $request->last_digits,
                      'card_holder' => $request->card_holder,
                      'approved_code' => $request->approve_code,
                      'created_at' => $current_date,
                      'updated_at' => $current_date,

            ]);  
        }
                            return redirect()->back()->with("success","Student Fees Submitted");

            //return back()->withMessage('Student Fees Submitted');
    }
    
    public function getSudentList(Request $request)
    {
        //dd($request);
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();
                $userid =  studentprofile::where('current_class', $request->classid)->pluck('userid');

        
                        $student = DB::table('users')
                        ->select('users.id as uid','users.*','feesubmits.id as spid','feesubmits.*')
                        ->join('feesubmits','feesubmits.userid','=','users.id')
                        ->where('feesubmits.classid',$request->classid)
                        ->where('feesubmits.sectionid',$request->sectionid)
                        ->where('feesubmits.current_user_id',$staffmember->userid)
                        ->get();
                        
                        return $student;
		//return json_encode($userid);
    }
    //for attendance
    public function getSudentListAttendance(Request $request)
    {
        //dd($request);
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();
                $userid =  studentprofile::where('current_class', $request->classid)->pluck('userid');

        
                        $student = DB::table('users')
                        ->select('users.id as uid','users.*','studentprofiles.id as spid','studentprofiles.*')
                        ->join('studentprofiles','studentprofiles.student_user_id','=','users.id')
                        ->where('studentprofiles.classid',$request->classid)
                        ->where('studentprofiles.sectionid',$request->sectionid)
                        //->where('studentprofiles.current_user_id',$staffmember->userid)
                        ->get();
                        
                        return $student;
		//return json_encode($userid);
    }
    
    //get selected months
    
    public function getselectmonth(Request $request)
    {
       
       //dd($request);
       $id = $request->pen;
       $sub_id = $request->sub;
       //dd($request);
       if($sub_id != NULL && $id != 0)
       {
       $results = DB::select( DB::raw("SELECT * FROM months WHERE id IN ($id) AND id NOT IN ($sub_id)") );
       return $results;
       }
       if($sub_id == 0 && $id != NULL)
       {
        $results = DB::select( DB::raw("SELECT * FROM months WHERE id IN ($id)") );
       return $results;   
       }
       if($id == 0 && $sub_id != NULL)
       {
        $results = DB::select( DB::raw("SELECT * FROM months WHERE id NOT IN ($sub_id)") );
       return $results;   
       }
       if($sub_id == NULL && $id == NULL)
       {
       $results = DB::select( DB::raw("SELECT * FROM months") );
       return $results;
       }
       
    }
    public function getcharge(Request $request)
    {
        
        $staffmember = staffmember::where('userid',$request->authenticateuserid)->first();
        $maintainfee = maintainfee::where('schoolid',$staffmember->schoolid)->where('classid',$request->classid)->get();
        return $maintainfee;
        
    }
    
    public function getStudentdata(Request $request)
    {
        
           $student_data = DB::table('feesubmits')->where('userid',$request->student_id)->get();
        
        return json_decode($student_data);
 
        
    }
}
