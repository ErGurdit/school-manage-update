<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\school;
use App\staffmember;
use Notification;
use App\Notifications\ApproveMoney;
use App\User;
use App\approve_money;
use Session;


class notificationsController extends Controller
{
    
    public function getNotificationsNumberFees() {
        $userid = Auth::user()->id;
        $staffmember = staffmember::where('userid',$userid)->first();
        $school = school::where('id',$staffmember->schoolid)->first();
        
        $accnt_id = DB::table('staffmembers')
    ->select('staffmembers.userid as accnt_id')
    ->join('role_user','role_user.user_id','=','staffmembers.userid')
    ->where('role_user.role_id',7)
    ->where('staffmembers.schoolid',$school->id)
    ->get()->pluck('accnt_id');
    $acid = $accnt_id[0];
        
        
        $notee = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$acid)
					   ->where( 'read_at', '=', null )
					   //->where('type','=','App\Notifications\ApproveMoneyByAccounts')
					   //->where('type','=','App\Notifications\EditNotify')
   					  //->where('type','=','App\Notifications\DeleteNotify')
   					   ->get();
        $count = count($notee);

        return [
            'number' => $count
        ];
    }
    
    public function getNotificationNumber() {
        $userid = Auth::user()->id;
        $staffmember = staffmember::where('userid',$userid)->first();
        $school = school::where('id',$staffmember->schoolid)->first();
        $notee = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where( 'read_at', '=', null )
					   //->where('type','=','App\Notifications\DatabaseNotification')
					   //->where('type','=','App\Notifications\EditNotify')
   					  //->where('type','=','App\Notifications\DeleteNotify')
   					   ->get();
        $count = count($notee);

        return [
            'number' => $count
        ];
    }
    public function getNotificationNumberAccounts() {
        $userid = Auth::user()->id;
        $staffmember = staffmember::where('userid',$userid)->first();
        $school = school::where('id',$staffmember->schoolid)->first();
        
        $accnt_id = DB::table('staffmembers')
    ->select('staffmembers.userid as accnt_id')
    ->join('role_user','role_user.user_id','=','staffmembers.userid')
    ->where('role_user.role_id',8)
    ->where('staffmembers.schoolid',$school->id)
    ->get()->pluck('accnt_id');
    $acid = $accnt_id[0];
        
        
        $notee = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$userid)
					   ->where( 'read_at', '=', null )
					   //->where('type','=','App\Notifications\DatabaseNotification')
					   //->where('type','=','App\Notifications\EditNotify')
   					  //->where('type','=','App\Notifications\DeleteNotify')
   					   ->get();
        $count = count($notee);

        return [
            'number' => $count
        ];
    }
    public function getNotificationVehicle(Request $request)
	{
	  $userid = Auth()->user()->id;
	  $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
             $sch_i = $staffmember->schoolid;
             $accnt_id = DB::table('staffmembers')
    ->select('staffmembers.userid as accnt_id')
    ->join('role_user','role_user.user_id','=','staffmembers.userid')
    ->where('role_user.role_id',8)
    ->where('staffmembers.schoolid',$school->id)
    ->get()->pluck('accnt_id');
    $acid = $accnt_id[0];
             
       $approved_vehicle = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $acid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\ApprovedVehicle')
   					   ->get();  
   	 $count_vehicle  = count($approved_vehicle);
   	 $decline_vehicle = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $acid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\DeclineVehicle')
   					   ->get();  
   	 $count_decline = count($decline_vehicle);
   	 $approve_money = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $acid)
				       ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\ApproveMoney')
   					   ->get();  
   					   $count_app = count($approve_money);
   	 return [	
   	     'approved_vehicle' => $count_vehicle,
   	     'decline_vehicle' => $count_decline,
   	     'appro_money' => $count_app

];

	}
    public function getNotification(Request $request)
	{
	  $userid = Auth()->user()->id;
	  $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
                                               $sch_i = $staffmember->schoolid;


	  $busroute = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
					   ->where('type','=','App\Notifications\DatabaseNotification')
					   ->get();
					   
	  $edit = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type', '=', 'App\Notifications\EditNotify')->get();
   					   
   	   $feest = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type', '=', 'App\Notifications\Feestatus')->get();
   					   
   	  $delete = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
				       ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\DeleteNotify')
   					   ->get();
	   $dress_ch = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\DressChargesNotify')
   					   ->get();
   	  $dress_edit = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\DressEdit')
   					   ->get();
   	   $dress_del = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\DressDel')
   					   ->get();
  $book_ch = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\BookChargesNotify')
   					   ->get();
   	  $book_edit = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\BookEdit')
   					   ->get();
   	   $book_del = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id',$school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\BookDel')
   					   ->get();
   					    $ad = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\NewAdmission')
   					   ->get();  
   	 $count_ad  = count($ad);
   	 
  	  
   					   //return $notifications;
   	   $count = count($busroute);   
   	   $count_ed = count($edit);
   	   $count_del = count($delete);
   	   $count_feest = count($feest);
   	   $count_dre_c = count($dress_ch);
   	   $count_dre_edit = count($dress_edit);
   	   $count_dre_del = count($dress_del);
   	   $count_bok_c = count($book_ch);
   	   $count_bok_edit = count($book_edit);
   	   $count_bok_del = count($book_del);
	
					   return  [ 
					       'sch_i' => $sch_i,
					       'busroute' => $count,
					       'edit' => $count_ed,
					       'delete' => $count_del,
					       'feest' => $count_feest,
					       'dress_charge' => $count_dre_c,
					       'dress_edit' => $count_dre_edit,
					       'dress_delete' => $count_dre_del,
					       'book_add' => $count_bok_c,
					       'book_edit' => $count_bok_edit,
					       'book_delete' => $count_bok_del,
   	                      'newadmission' => $count_ad,
       ];
					     
	}
	public function store(Request $request)
	{
	    //dd($request);
	           $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
	           $school =  school::where('id',$staffmember->schoolid)->first();
	                  $uid = Auth()->user()->id;
	                  $current_date = date('Y-m-d H:i:s');
        $only_dt = date("Y-m-d",strtotime($current_date));
        
        $app_m = DB::table('approve_moneys')->select('id')
                ->where('schoolid',$school->id)
                ->where('amount',$request->fees)
                ->get();
                $d_i = json_encode($app_m);
                //dd($d_i);
                $cnt = count(json_decode($d_i));
                //dd($cnt);


	    if(($request->acc != null) || $cnt == 0)
        {
        //store data in table
        $approves_mon = new approve_money;
        $approves_mon->userid = $request->current_id;
        $approves_mon->schoolid =  $school->id;
        $approves_mon->amount =  $request->fees;
        $approves_mon->current_user_id =  $request->acc;
        $approves_mon->created_at = $only_dt;
        $approves_mon->updated_at = $only_dt;
        $approves_mon->save();
        
        //send
	    $data = 'Notification for approve hand over money from fees department';
        Notification::send(User::find($request->acc),new ApproveMoney($data));
        return redirect()->back()->with("success","Student Fees Submitted");

            //return back()->withMessage("Notification Send");

        }
        else
        {
                return redirect()->back()->with("success","Notification Already Send");
        }

	}
	
	public function getNotificationsMoney(Request $request)
	{
	  $userid = Auth()->user()->id;
	  $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
             $sch_i = $staffmember->schoolid;
             $accnt_id = DB::table('staffmembers')
    ->select('staffmembers.userid as accnt_id')
    ->join('role_user','role_user.user_id','=','staffmembers.userid')
    ->where('role_user.role_id',7)
    ->where('staffmembers.schoolid',$school->id)
    ->get()->pluck('accnt_id');
    $acid = $accnt_id[0];
             
       $money_acc = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $acid)
   					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\ApproveMoneyByAccounts')
   					   ->get();  
   	 $count_money_acc  = count($money_acc);
   	 $money_dec = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $acid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\DeclineMoneyByAccounts')
   					   ->get();  
   	 $count_money_dec  = count($money_dec);
   	 
   	 return [	
   	     
   	     'appro_money_acc' => $count_money_acc,
   	     'dec_money_acc'  => $count_money_dec

];

	}
	public function getNotificationsSalary(Request $request)
	{
	  $userid = Auth()->user()->id;
	  $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
             $sch_i = $staffmember->schoolid;
             
             
       $sal = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\SalaryReceive')
   					   ->get();  
   	 $count_salary  = count($sal);
   	 
   	 
   	 return [	
   	     
   	     'salary' => $count_salary,

];

	}
	
		public function getNotificationsNewAdmission(Request $request)
	{
	  $userid = Auth()->user()->id;
	  $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
             $sch_i = $staffmember->schoolid;
             //dd($school->userid);
             
       $ad = DB::table('notifications')
					   ->select('notifications.data')
					   ->where('notifiable_id', $school->userid)
					   ->where('read_at','=',null)
   					   ->where('type','=','App\Notifications\NewAdmission')
   					   ->get();  
   	 $count_ad = count($ad);
   	 
   	 return ['newadmission' => $count_ad];

	}

}
