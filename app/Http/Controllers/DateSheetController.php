<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Redirect;
use App\school;
use App\classes;
use Auth;
use App\holiday;
use Illuminate\Support\Facades\Input;
use App\staffmember;
use App\subject;
use App\sectionclass;
use App\datesheet;



class DateSheetController extends Controller
{
    //
    public function index(Request $request)
    {
          $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
       $school =  school::where('id',$staffmember->schoolid)->first();
       $class = classes::all();
        $myclass = DB::table('classes')
                            ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                            ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                            ->where('class_to_schools.schoolid',$school->id)
                            ->get();
                            
        $subject = subject::all();
                $section = sectionclass::all();

        

        $stream = DB::table('datesheets')
        ->select('datesheets.*')
        ->join('classes','classes.id','=','datesheets.classid')
        ->join('subjects','subjects.id','=','datesheets.subjectid')
        ->where('datesheets.sectionid',0)
        ->where('datesheets.schoolid',$school->id)
        ->get();
        
        
        $datesheet = DB::table('datesheets')
        ->select('datesheets.*','datesheets.id as dit','datesheets.created_at as ct','classes.id as cid','classes.class_display_name as classname','sectionclasses.section_display_name as sectionname','subjects.*')
                        ->join('users','users.id','=', 'datesheets.userid')
                        ->join('classes','classes.id','=','datesheets.classid')
        			->join('sectionclasses','sectionclasses.id','=','datesheets.sectionid')
        			->join('subjects','subjects.id','=','datesheets.subjectid')
                        ->where('datesheets.schoolid',$school->id)
                        ->get();
        
            return view('principaltest.datesheet.index',compact('section','stream','subject','user','myclass','school','class','allstudent','datesheet'));
    }
    public function store(Request $request)
    {
        //dd($request);
     $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
      $school =  school::where('id',$staffmember->schoolid)->first();

       $uid = Auth()->user()->id;
         if($request->subjectdata !=NULL){
                    //subject
                    //dd($request->subjectdata);
                    
                    foreach($request->subjectdata as $key => $value)
                    {
                     //dd($value);
                     $datesht = new datesheet;
        $datesht->userid =  $uid;
        $datesht->schoolid = $school->id;
        $datesht->classid = Input::get('class');
        $datesht->sectionid = Input::get('section');
        $datesht->examtime = Input::get('time');
        $datesht->subjectid = $value;
      //dd($datesht);
        $datesht->save();
                    }
                return redirect()->back()->with('success','Datesheet Add');

                    }
                    
                    if($request->stream !=NULL){
                    //stream
                   foreach($request->stream as $key => $value)
                    {
                     $datesht->userid =  $uid;
        $datesht->schoolid = $school->id;
        $datesht->classid = Input::get('class');
        $datesht->sectionid = 1;
        $datesht->examtime = Input::get('time');
        $datesht->subjectid = $value;
      //dd($datesht);
        $datesht->save();
                    }
                return redirect()->back()->with('success','Datesheet Add');

        
                    }
       
        
    }
    public function classsubject(Request $request){
            
            $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            $school =  school::where('id',$staffmember->schoolid)->first();
            
            $subject = DB::table('assignsubjects')
                          ->select('assignsubjects.subjectid as sid','assignsubjects.*','subjects.id as csid','subjects.*')
                          ->join('subjects','subjects.id','=','assignsubjects.subjectid')
                          ->where('assignsubjects.schoolid',$school->id)
                          ->where('assignsubjects.classid',$request->classid)
                          ->get();
            return $subject;
            
        }
        public function update(Request $request,$id)
    {
        //dd($request);
        $dt = Datesheet::where('id',$id)->first();
      $uid = Auth()->user()->id;
        $dt->userid = $uid;
        $dt->classid = Input::get('classid');
        $dt->schoolid = Input::get('schoolid');
        $dt->sectionid = Input::get('sectionid');
        $dt->subjectid = Input::get('subjectid');
     $dt->save();
        
        return redirect('datesheet');
    }
    
    public function destroy(Request $request,$id)
    {
        
        DB::table('datesheets')->where('id',$id)->delete();
        return back()->withMessage("Datesheet Deleted");
    }
    
    
}
