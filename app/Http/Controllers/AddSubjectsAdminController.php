<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;
use App\subject;
use Session;

class AddSubjectsAdminController extends Controller
{
    //
    public function index()
    {
    
        $subject =DB::table('subjects')
			->select('subjects.*')
			->get();
        return view('admin.subject.index',compact('subject'));
    }
     public function store(Request $request)
    {
        
        
        $rules = array(

			'name' => 'required',

		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$messages = $validator->messages();

			return Redirect::back()->withInput()->withErrors($validator);

		} elseif ($validator->passes()) {
		    
		    
		//school create
		//dd($request);
		$check_subj = DB::table('subjects')->select('subjectname')
                ->where('subjectname',$request->name)
                ->get();
                $dc = json_encode($check_subj);
                $cnt = count(json_decode($dc));
                
if ($cnt == 0){
        $subject = new subject;
        $subject->subjectname = Input::get('name');
        $subject->save();
       
        
        return back()->withMessage('Subject Add');
        
    }
    else 
                {
         return back()->withMessage("This subject is already exists");


                }
    }
    }
    
    public function update(Request $request, $id)
    {
        
        $rules = array(

			'name' => 'required',



		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$messages = $validator->messages();

			return Redirect::back()->withInput()->withErrors($validator);

		} elseif ($validator->passes()) {
       
		$subject = subject::where('id',$id)->first();
		
        $subject->subjectname = Input::get('name');
        
         $subject->save();
         return redirect('subject')->withMessage('Subject Update');
     }
    }

   
    public function destroy($id,Request $request)
    {
        //dd($request);
        
        DB::table('subjects')
        ->where('subjectname',$request->subjectname)->delete();
        //delete from busroutes 
        
        return back()->withMessage("Subject Deleted");
    }

  
}
