<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\school;
use App\staffmember;


class TopMenuController extends Controller
{
    //
    public function index(Request $request)
    {
         $userid = Auth()->user()->id;
        $schoolid = school::where('userid',$userid)->first();
        $staffmember = staffmember::where('userid',$userid)->first();
                    $school =  school::where('id',$staffmember->schoolid)->first();
    $notify = DB::table('notifications')->select('notifications.*')->where('notifiable_id',$school->user_id)->get();
    return view('principaltest.topmenu',compact('notify'));
    }
}
