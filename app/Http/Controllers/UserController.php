<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Role;
use DB;
use Entrust;
use Auth;
class UserController extends Controller
{
    
    // function __construct(){
    //     $this->middleware(['permission:role-edit','permission:role-delete']);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    
    if(Entrust::hasRole('superadmin')){
        $user = User::where('role_create',2)->get();
        $allrole = Role::where('role_create',2)->get();
        
    return view('admin.user',compact('user','allrole'));
    }elseif(Entrust::hasRole('principal')){
        $user = User::where('role_create',1)->get();
        $allrole = Role::where('role_create',1)->get();
        return view('principaltest.role.user',compact('role','user','allrole'));
    }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
         $user = User::find($id);
         $roles = $request->roles;
         
    DB::table('role_user')->where('user_id',$id)->delete();
        if(!empty($roles)){
        foreach($roles as $role){
            $user->attachRole($role);
        }
    }
        return redirect()->route('user.index')->withMessage('Update');
    }
    
    public function destroy(Request $request,$id)
    {
        DB::table('users')->where('id',$id)->delete();
        return back()->withMessage("School Deleted");
    }
    
    
}
