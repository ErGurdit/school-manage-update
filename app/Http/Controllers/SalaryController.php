<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\studentprofile;
use App\country;
use App\state;
use App\city;
use App\school;
use App\classes;
use DB;
use App\User;
use Illuminate\Support\Str;
use App\staffmember;
use Session;

class SalaryController extends Controller
{
    //
    public function show(Request $request)
    {
        //dd($request);
                    $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();

        $student = DB::table('salaries')
        ->select('salaries.*','users.name as uname',DB::raw('staff_attendances.present as pr, COUNT(*) as count_pr'),DB::raw('staff_attendances.absent as ab,COUNT(*) as count_ab'),DB::raw('staff_attendances.leave as lv,COUNT(*) as count_lv'))
        ->join('users','users.id','=','salaries.userid')
        ->join('staff_attendances','staff_attendances.userid','=','salaries.userid')
        ->where('salaries.userid',Auth()->user()->id)
        ->get();
        $uname = DB::table('users')->select('users.name as uname')->where('id',$student[0]->account_id)->get()->pluck('uname');

        return view('salary.index',compact('student','uname'));
    }
}
