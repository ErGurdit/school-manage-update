<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;
use Auth;
use Entrust;
use App\classes;
use App\highclass;
use App\subject;
use App\staffmember;
use App\school;
use App\sectionclass;
use App\User;
use App\student_profile;
use App\testmark;
use App\attendence;

class testController extends Controller
{
	//main page
    public function index(){
    	$testmark = DB::table('testmarks')
        			->select('testmarks.id as tid','testmarks.*','classes.id as cid','classes.*','sectionclasses.id as secid','sectionclasses.*','subjects.id as subid','subjects.*')
        			->join('classes','classes.id','=','testmarks.classid')
        			->join('sectionclasses','sectionclasses.id','=','testmarks.sectionid')
        			->join('subjects','subjects.id','=','testmarks.subjectid')
        			->get();
    	$classes = DB::table('classes')
                ->select('class_to_sections.id as csid','classes.id as cid','classes.*')
                ->join('class_to_sections','class_to_sections.classid','=','classes.id')
                ->groupBy('class_to_sections.classid')->get();
        $section = DB::table('sectionclasses')
                ->select('class_to_sections.id as csid','sectionclasses.id as sid','sectionclasses.*')
                ->join('class_to_sections','class_to_sections.sectionid','=','sectionclasses.id')->get();
        $subject = subject::all();
        $highclass = highclass::all();
        $school = school::where('userid',Auth::user()->id)->first();
        //print_r($testmark['streamid']);
        return view('principaltest.test.index',compact('testmark','school','classes','section','subject','highclass','testmarknew'));
    }
    
    //studenttestdetail
    public function studenttestdetail(Request $request){
    	//complete detail
    	$studnetsubdetail = DB::table('attendences')
		    				->select('attendences.id as aid','attendences.*','classes.id as cid','classes.*','studentprofiles.id','studentprofiles.*','users.id as uid','users.*','testmarks.id as tid','testmarks.*','sectionclasses.id as secid','sectionclasses.*','subjects.id as subid','subjects.*')
		    				->join('classes','classes.id','=','attendences.classid')
		    				->join('studentprofiles','studentprofiles.id','=','attendences.studentid')
		    				->join('users','users.id','=','studentprofiles.userid')
		    				->join('testmarks','testmarks.classid','=','classes.id')
		    				->join('sectionclasses','sectionclasses.id','=','testmarks.sectionid')
		    				->join('subjects','subjects.id','=','testmarks.subjectid')
		    				->where('attendences.classid',$request->classid)
		    				->get();
		 //dd($studnetsubdetail);
		//class detail
    	$classdetail = DB::table('testmarks')
		    				->select('testmarks.id as tid','testmarks.*','classes.id as cid','classes.*')
		    				->join('classes','classes.id','=','testmarks.classid')
		    				->where('testmarks.classid',$request->classid)
		    				->first();
		//section detail
		$sectiondetail = DB::table('testmarks')
		    				->select('testmarks.id as tid','testmarks.*','sectionclasses.id as cid','sectionclasses.*')
		    				->join('sectionclasses','sectionclasses.id','=','testmarks.sectionid')
		    				->where('testmarks.sectionid',$request->secid)
		    				->first();
		//subject detail
		$subdetail = DB::table('testmarks')
		    				->select('testmarks.id as tid','testmarks.*','subjects.id as sid','subjects.*')
		    				->join('subjects','subjects.id','=','testmarks.subjectid')
		    				->where('testmarks.subjectid',$request->subid)
		    				->first();
		return view('principaltest.test.testdetail',compact('studnetsubdetail','classdetail','sectiondetail','subdetail'));
    }
    
	//subject deatil
    public function getSubjectList(Request $request) {
        
        $userid   = User::where('id',Auth::user()->id)->first();
        $staffid = staffmember::where('userid',$userid->id)->first();
		$subject = DB::table('assignsubjects')
		            ->select('assignsubjects.*','assignsubjects.id as aid','classes.id as cid','subjects.*','subjects.id as sid')
		            ->join('classes','classes.id','=','assignsubjects.classid')
		            ->join('subjects','subjects.id','=','assignsubjects.subjectid')
		            ->where('assignsubjects.schoolid',$staffid->schoolid)
		            ->where('assignsubjects.classid', $request->classid)
		            ->pluck('subjectname', 'id');
	//dd($subject);
		    return json_encode($subject);
    }
    
    //stream detail
    public function getHighclasslist(Request $request) {
        
        $userid   = User::where('id',Auth::user()->id)->first();
        $staffid = staffmember::where('userid',$userid->id)->first();
		$stream = DB::table('assignsubjects')
		            ->select('assignsubjects.*','assignsubjects.id as aid','classes.id as cid','highclasses.id as hid','highclasses.*')
		            ->join('classes','classes.id','=','assignsubjects.classid')
		            ->join('highclasses','highclasses.id','=','assignsubjects.streamtid')
		            ->where('assignsubjects.schoolid',$staffid->schoolid)
		            ->where('assignsubjects.classid', $request->classid)
		            ->pluck('highsubname', 'id');
	//dd($subject);
		    return json_encode($stream);
    }
    
    //stream subject detail
    public function gethighSubjectList(Request $request) {
        
        $userid   = User::where('id',Auth::user()->id)->first();
        $staffid = staffmember::where('userid',$userid->id)->first();
		$subject = DB::table('assignsubjects')
		            ->select('assignsubjects.*','assignsubjects.id as aid','classes.id as cid','subjects.*','subjects.id as sid')
		            ->join('classes','classes.id','=','assignsubjects.classid')
		            ->join('subjects','subjects.id','=','assignsubjects.subjectid')
		            ->where('assignsubjects.schoolid',$staffid->schoolid)
		            ->where('assignsubjects.streamtid', $request->streamid)
		            ->pluck('subjectname', 'id');
	//dd($subject);
		    return json_encode($subject);
    }
	//test mark store
	public function store(Request $request){
	    $staffid = staffmember::where('userid',Auth()->user()->id)->first();
	    $examdate = explode('/',$request->selectdate);
	    $examdatenew = $examdate[1].'-'.$examdate[0].'-'.$examdate[2];
	    if(!empty(Input::get('subjectid'))){
	        $subid = Input::get('subjectid');
	    }else{
	        $subid = 0;
	    }
	    if(!empty(Input::get('streamsubjectid'))){
	        $streamid = Input::get('streamsubjectid');
	    }else{
	        $streamid = 0;
	    }
	    $teststore = new testmark;
	    $teststore->schoolid = $staffid->schoolid;
	    $teststore->staffid = $staffid->id;
	    $teststore->classid = Input::get('classid');
	    $teststore->sectionid = Input::get('sectionid');
	    $teststore->streamid = $streamid;
	    $teststore->subjectid = $subid;
	    $teststore->totalmarks = Input::get('totalmark');
	    $teststore->passingmarks = Input::get('passingmark');
	    $teststore->description = Input::get('description');
	    $teststore->test_date = $examdatenew;
	    $teststore->save();
	    
	    $message = 'Test successfully saved';
	    return back()->with('success',$message);
	}
	
	public function testmarkupdate(Request $request)
	{
		$attendence = attendence::where('id',$request->studentid)->first();
		$attendence->obtain_mark = $request->userdata;
		$attendence->save();
		
		$attendence = attendence::where('id',$request->studentid)->first();
		return $attendence;
	}
	
}
