<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\country;
use App\state;
use App\city;
use Illuminate\Support\Facades\Input;
use App\classes;
use DB;
use App\sectionclass;
use Validator;
use App\User;
use App\school;
use App\studentprofile;
use Illuminate\Support\Str;
use App\staffmember;
use App\month;
use App\feesubmits;
use App\class_to_school;
use Auth;
use Session;



class StudentController extends Controller
{
    //
    public function index()
    {
        
    }
    
     public function create(Request $request)
        {
            
            //dd($request);
            //dd(Auth()->user()->id);
            $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();
            //dd($school->id);
                //allclasses
                $myclass = DB::table('classes')
                            ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                            ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                            ->where('class_to_schools.schoolid',$school->id)
                            ->get();
                            
                            
            
            $country = country::all();
            $state = state::all();
            $city = city::all();
             $month = month::all();
        //$classes = classes::all();
        //dd($country->name);
        
         $section = DB::table('sectionclasses')
                          ->select('sectionclasses.id as sid','sectionclasses.*','class_to_sections.id as csid','class_to_sections.*')
                          ->join('class_to_sections','class_to_sections.sectionid','=','sectionclasses.id')
                          ->where('class_to_sections.schoolid',$school->id)
                          ->where('class_to_sections.classid',$request->classid)
                          ->get();
                          //dd($section);
                          
                          $stream = DB::table('highclasses')
                        ->select('highclasses.id as hid','highclasses.*','class_to_sections.id as csid','class_to_sections.*')
                        ->join('class_to_sections','class_to_sections.sectionid','=','highclasses.id')
                        ->where('class_to_sections.schoolid',$school->id)
                        ->where('class_to_sections.classid',$request->classid)
                        ->get();
        
        
            return view('student.create',compact('country','state','city','month','myclass','section','stream'));
        }
        
        
        
        public function store(Request $request)
    {
         //dd($request);

        
        //dd($request);
        //$total = count($request->mondata);
        //dd($total);
       
        $rules = array(

			'name' => 'required',

			'fname' => 'required',

			'email' => 'required|email|unique:users',
			
			);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$messages = $validator->messages();

			return redirect()->back()->withInput()->withErrors($validator);

		} elseif ($validator->passes()) {
		    
		    //country set
		    if(!empty(Input::get('country'))){
		        $country = Input::get('country');
		        
		    }else{
		        $country = 1;
		    }
		    //state set
		    if(!empty(Input::get('state'))){
		        $state = Input::get('state');
		    }else{
		        $state = 10;
		    }
		    //city set
		    if(!empty(Input::get('city'))){
		        $city = Input::get('city');
		    }else{
		        $city = 121;
		    }
		    
		 //user create by school
		/*$user = new user;
		$user->name = Input::get('name');
		$user->email = Input::get('email');
		$user->username = Input::get('username');
		$newpass = substr(md5(mt_rand()), 0, 7);
        $confirmpass =  bcrypt($newpass);
        $user->password = $confirmpass;
        $user->verify_token = Str::random(40);*/
        
        $user = new user();
            $user->name =Input::get('name');
            $user->email =Input::get('email');
            $user->password = bcrypt('12345678');
            $user->gender = Input::get('gender');
            $user->username =Input::get('username');
            $user->role_create = Auth::user()->id;
            $user->save();
		
		$user->save();
		
		//role assign
		//$user->roles()->attach($request->role);
		
		//school create
		
        /*$school = new school;
        $school->name = Input::get('last_school');
        
        $school->userid = $user->id;
        //$school->principal_name = Input::get('principalname');
        $school->countryid = $country;
        $school->stateid = $state;
        $school->cityid = $city;
        
        
        $school->address = Input::get('address');
        
        $school->email = Input::get('email');
        $school->pincode = Input::get('pincode');
        
			
        $school->save();*/
        
        $class = classes::all();
        //dd(json_encode($class));
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
        $school =  school::where('id',$staffmember->schoolid)->first();
        //dd($school);
             
        
        /*foreach($class as $classes){
        //$newclass= strtolower(str_replace(' ','',$classes));
        $class = new class_to_school;
        $class->classid =  $classes->id;
        $class->schoolid = $school->id;
        $class->save();
        }*/
        //student profile
       $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
       $school =  school::where('id',$staffmember->schoolid)->first();

       $uid = Auth()->user()->id;
       
       $strm = $request->stream;
                if($strm != '' || $strm != null)
                {
                    $stream = 0;
                }
                else
                {
                $stream  = Input::get('section');
                }

        $student = new studentprofile;
        $student->userid =  $uid;
        $student->student_user_id =  $user->id;
        $student->schoolid = $school->id;
        $student->classid = Input::get('class');
        $student->sectionid = $stream;
        $student->cityid = $school->cityid;
        $student->countryid = $school->countryid;
        $student->stateid = $school->stateid;
        $student->father_name = Input::get('fname');
        $student->mother_name = Input::get('mname');
        $student->home_tel = Input::get('hometel');
        $student->DOB = Input::get('dob');
        $student->last_school = Input::get('last_school');
        $student->school_joining_date = date('d-m-Y');
        $student->studen_joining_class = Input::get('class');
        $student->current_class = Input::get('class');
        $student->pincode = Input::get('pincode');
        $student->address = Input::get('address');
        $student->father_contact_number = Input::get('father_contact');
        $student->mother_contact_number = Input::get('mother_contact');
//dd($student);
        $student->save();
        
        
        // add fee submit info
        //$feesubmits = new feesubmits;
        if($request->feesclear == "N")
        {
            $monthdata = $request->mondata;
        $monthdatas = $request->mondata1;
        $pend_m = $request->pend_m;
       if($monthdata !="" && $monthdatas == "")
        {
        //dd($monthdatas);
        $monthdata = $request->mondata;
        $monthdatas = ($monthdatas == NULL ? 0 : $monthdatas);

        $data = ltrim(json_encode($monthdata),'"[');
        $data_pend = str_replace('","',',',$data);
        $pending = str_replace('"]','',$data_pend);
        $datas = ltrim(json_encode($monthdatas),'"[');
        
        $class = Input::get('class');
        $section = $stream;

        $stdid  = $student->id;
        $total = count($request->mondata);
        //dd($pending);
                    //dd($total."when pen '.$data_pend.'is fill and sub empty ");

        //$left_mon = 12 - ($total);
        
         

        DB::table('feesubmits')->insert(['schoolid'=>$school->id,'userid' => $user->id,'current_user_id' => $uid,'classid'=> $class,'sectionid'=> $section,'studentid'=> $stdid,'pending_months' => $pending,'pend_m' => $pend_m, 'submission_months'=> $datas,'count_pending'=>$total]
);
        }//if pending not empty and submiss is empty
        if($monthdata !="" && $monthdatas != "")
        {
        
        $monthdata = $request->mondata;
        $monthdatas = $request->mondata1;
        $pend_m = $request->pend_m;

        $data = ltrim(json_encode($monthdata),'"[');
        $data_pend = str_replace('","',',',$data);
        $pending = str_replace('"]','',$data_pend);
        $datas = ltrim(json_encode($monthdatas),'"[');
        $data_sub = str_replace('","',',',$datas);
        $sub_m = str_replace('"]','',$data_sub);
        $class = Input::get('class');
        $section = Input::get('section');

        $stdid  = $student->id;
        $total = count($request->mondata);
                    //dd($total."when both not mpty ");

        //$left_mon = 12 - ($total);
         

        DB::table('feesubmits')->insert(['schoolid'=>$school->id,'userid' => $user->id,'current_user_id' => $uid,'classid'=> $class,'sectionid'=> $section,'studentid'=> $stdid,'pending_months' => $pending,'pend_m' => $pend_m, 'submission_months'=> $sub_m,'count_pending'=>$total]
);
        }//if both are filled
        }//end N
        
        if($request->feesclear == "Y")
        {
            $monthdata = $request->mondata;
            $monthdatas = $request->mondata1;
            $pend_m = $request->pend_m;

        if($monthdatas !="" && $monthdata == "")
        {
            $monthdata = $request->mondata;
            $monthdatas = $request->mondata1;
                    $monthdata = ($monthdata == NULL ? 0 : $monthdata);

            $pend_m = $request->pend_m;


            $section = Input::get('section');
            $total = count($monthdatas) ;
            $left_mon = 12 - ($total);
            
            //dd($total."when subm is fill and pending empty ");
            
        $data = ltrim(json_encode($monthdata),'"[');
        $data_pend = str_replace('","',',',$data);
        $pending = str_replace('"]','',$data_pend);
        $datas = ltrim(json_encode($monthdatas),'"[');
        $data_sub = str_replace('","',',',$datas);
        $sub_m = str_replace('"]','',$data_sub);

        $class = Input::get('class');
        $stdid  = $student->id;

        DB::table('feesubmits')->insert(['schoolid'=>$school->id,'userid' => $user->id,'current_user_id' => $uid,'classid'=> $class,'sectionid'=> $section,'studentid'=> $stdid,'pending_months' => $pending,'pend_m' => $pend_m,'submission_months' => $sub_m,'count_pending'=>$left_mon]
);
        }// check if subm is not empty and pending is empty
        
        if($monthdatas !="" && $monthdata != "")
        {
            $monthdata = $request->mondata;
            $monthdatas = $request->mondata1;
            $pend_m = $request->pend_m;

            $section = $stream;
            $total = count($request->mondata);
              //$left_mon = 12 - ($total);
            //dd($total."when both not empty ");

            //dd($total);
       $data = ltrim(json_encode($monthdata),'"[');
        $data_pend = str_replace('","',',',$data);
        $pending = str_replace('"]','',$data_pend);
       $datas = ltrim(json_encode($monthdatas),'"[');
        $data_sub = str_replace('","',',',$datas);
        $sub_m = str_replace('"]','',$data_sub);

        $class = Input::get('class');
        $stdid  = $student->id;

        DB::table('feesubmits')->insert(['schoolid'=>$school->id,'userid' => $user->id,'current_user_id' => $uid,'classid'=> $class,'sectionid'=> $section,'studentid'=> $stdid,'pending_months' => $pending,'pend_m' => $pend_m,'submission_months' => $sub_m,'count_pending'=>$total]
);
        }//if pending not empty then count
        }//end Y
        
        
        return redirect()->back()->with('success','Student Add');
        
    }
}

}
