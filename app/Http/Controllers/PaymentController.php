<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\school;
use App\user;
use App\country;
use App\state;
use App\city;
use DB;
use Softon\Indipay\Facades\Indipay;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function school()
    {
        $country = country::all();
        $state =  state::all();
        $city = city::all();
        //$school = school::all();
        $school = DB::table('schools')
                    ->select('schools.*','users.username as username','countries.name as cname','states.name as statename','cities.name as cityname','schoolpayments.*')
                    ->join('users','users.id','=','schools.userid')
                    ->join('countries','countries.id','=','schools.countryid')
                    ->join('states','states.id','=','schools.stateid')
                    ->join('cities','cities.id','=','schools.cityid')
                    ->join('schoolpayments','schoolpayments.schoolid','=','schools.id')
                    ->get();
                    //dd($school);
        return view('admin.payment.index',compact('school','country','state','city','schdata'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function student()
    {
        $country = country::all();
        $state =  state::all();
        $city = city::all();
        $school = school::all();
        $student= DB::table('studentprofiles')
                    ->select('studentprofiles.*','users.username as username','users.name as uname','users.email as uemail','countries.name as cname','states.name as statename','cities.name as cityname','schools.name as schname','schools.email as schmail','schools.mobile as schmobile','schools.landline as schlandline','schoolpayments.last_payment_date','schoolpayments.payment_method','schoolpayments.payment_amount','schoolpayments.payment_transfer_school','schoolpayments.payment_detail','schoolpayments.transfer_date')
                    ->join('users','users.id','=','studentprofiles.userid')
                    ->join('countries','countries.id','=','studentprofiles.countryid')
                    ->join('states','states.id','=','studentprofiles.stateid')
                    ->join('cities','cities.id','=','studentprofiles.cityid')
                    ->join('schools','schools.id','=','studentprofiles.schoolid')
                    ->join('schoolpayments','schoolpayments.studentid','=','studentprofiles.id')
                    ->get();
                    //dd($student);
        return view('admin.payment.index',compact('school','country','state','city','student'));  
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function onlinefee()
    {
        $country = country::all();
        $state =  state::all();
        $city = city::all();
        $school = school::all();
        $schdata = DB::table('schools')
                    ->select('schools.*','users.username as username','countries.name as cname','states.name as statename','cities.name as cityname')
                    ->join('users','users.id','=','schools.userid')
                    ->join('countries','countries.id','=','schools.countryid')
                    ->join('states','states.id','=','schools.stateid')
                    ->join('cities','cities.id','=','schools.cityid')
                    ->get();
        return view('admin.payment.index',compact('school','country','state','city','schdata'));  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function paymentuser(Request $request){
        
        $transcationid = mt_rand(100000000, 999999999);
        $orderid = rand(11111,99999);
        //dd($orderid);
        $parameters = [
      
        'txnid' => $transcationid,
        
        'order_id' => $orderid,
        
        'amount' => '10.00',
        
        'username' => 'gurdiit@',
        
        'firstname' =>'ankit',
        
        'email' =>'ankit1992gupta@gmail.com',
        
        'phone' =>'9425407671',
        
        'productinfo' => 'schoolfee',
        
        'service_provider' => 'payu_paisa',
        
        'furl' => 'http://school-manage-gurdit.c9users.io/payumoneyfailed',
        
        'surl' => 'http://school-manage-gurdit.c9users.io/payumoneysuccess'
        
      ];
      //dd($parameters);
      
     // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / Mocker
      
      $order = Indipay::gateway('PayUMoney')->prepare($parameters);
      return Indipay::process($order);
    }
    
    public function response(Request $request)
    
    {
        // For default Gateway
        $response = Indipay::response($request);
        
        // For Otherthan Default Gateway
        $response = Indipay::gateway('NameOfGatewayUsedDuringRequest')->response($request);

        dd($response);
    
    }
    
    public function failed(Request $request){
        dd($request);
        $pay = new paydata;
        $pay->oderid = Input::get('payuMoneyId');
        $pay->transcationid = Input::get('txnid');
        $pay->amount = Input::get('amount');
        $pay->username = Input::get('username');
        $pay->firstname = Input::get('firstname');
        $pay->lastname = Input::get('lastname');
        $pay->email = Input::get('email');
        $pay->description = Input::get('productinfo');
        $pay->transcationstatus = Input::get('status');
        $pay->paymentdatetime = Input::get('addedon');
    }
    
    public function success(Request $request){
        dd($request);
        $pay = new paydata;
        $pay->oderid = Input::get('payuMoneyId');
        $pay->transcationid = Input::get('txnid');
        $pay->amount = Input::get('amount');
        $pay->username = Input::get('username');
        $pay->firstname = Input::get('firstname');
        $pay->lastname = Input::get('lastname');
        $pay->email = Input::get('email');
        $pay->description = Input::get('productinfo');
        $pay->transcationstatus = Input::get('status');
        $pay->paymentdatetime = Input::get('addedon');
    }
}
