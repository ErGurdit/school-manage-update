<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\staffmember;
use App\teacherprofile;
use Auth;
use DB;
use App\notifications;
use App\state;
use App\city;
use Hash;
use Session;

class ProfileController extends Controller
{
    public function sendMessage(Request $request)
    {
    	{
            
	    	$msg = $request->msg;
	    	$conID = $request->conID;
        
	    	//fetch user_to
	    	$fetch_userTo = DB::table('messages')
	    	->where('conservation_id',$conID)
	    	->where('user_to','!=',Auth::user()->id)
	    	->get();
	    	
	    	$userTo = $fetch_userTo[0]->user_to;
        
        
	    	//send message
	    	$sendM = DB::table('messages')->insert([
	    		'user_to' => $userTo,
	    		'user_from' =>Auth::user()->id,
	    		'msg' => $msg,
	    		'status' => 1 ,
	    		'conservation_id' => $conID
	    	]);
	    	

	    	if($sendM){
	    		$userMsg = DB::table('messages')
					->join('users','users.id','=','messages.user_from')
					->where('messages.conservation_id',$conID)
					->get();
					return $userMsg;
	    	}
	    }
	}

	public function newMessage(Request $request)
	{
		{
			//fetch not logged in user
			$friends = DB::table('users')
			        ->where('users.id','!=',Auth::user()->id)
					->get();
				//dd($friends);
			return view('profile.friends',compact('friends'));
		}
	}

	 public function sendNewMessage(Request $request)
	 {
        $msg = $request->msg;
        $friend_id = $request->friend_id;
        $myID = Auth::user()->id;

        //check if conversation already started or not
        $checkCon1 = DB::table('conversation')->where('user_one',$myID)
        ->where('user_two',$friend_id)->get(); // if loggedin user started conversation
        $checkCon2 = DB::table('conversation')->where('user_two',$myID)
        ->where('user_one',$friend_id)->get(); // if loggedin recviced message first
        $allCons = array_merge($checkCon1->toArray(),$checkCon2->toArray());
        
         if(count($allCons)!=0){
        	echo count($allCons);
        //   // old conversation
          $conID_old = $allCons[0]->id;
          //insert data into messages table
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conservation_id' =>  $conID_old,
            'status' => 1
          ]);
        }else {
		// new conversation
          $conID_new = DB::table('conversation')->insertGetId([
            'user_one' => $myID,
            'user_two' => $friend_id
          ]);
          echo $conID_new;
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conservation_id' =>  $conID_new,
            'status' => 1
          ]);
        //   $MsgSent = new Message;
        //   $MsgSent->user_from = $myID;
        //   $MsgSent->user_to = $friend_id;
        //   $MsgSent->msg = $msg;
        //   $MsgSent->conservation_id = $conID_new;
        //   $MsgSent->status = 1;
        //   $MsgSent->save();
        }
    }

    public function notification(Request $request,$id)
    {	
    	//notification
    	{
    		$notification = new notifications;
    		$notification->main_user = Auth::user()->id;
    		$notification->slave_user = $id;
    		$notification->status = 1; //unread notofication
    		$notification->note = 'test';
    		$notification->save();

    		if($notification){

    		}else{

    		}
    	}
    }

    public function notifications(Request $request,$id)
    {
    	$notes = DB::table('notifications')
                ->leftjoin('users','users.id','=','notifications.slave_user')
                ->where('notifications.id',$id)
                ->where('main_user',Auth::user()->id)
                ->get();
               // dd($note);
         $updatenotes = DB::table('notifications')
                ->where('notifications.id',$id)
                ->update(['status'=>0]);
         return view('profile.notifications',compact('notes'));
    }
    
    public function index(Request $request)
    {
      $userid = User::where('id',Auth::user()->id)->first();
      //dd($userid->secondlogin);
      $staff = staffmember::where('userid',$userid->id)->first();
      $state = state::all();
      $city = city::all();
      $teacher = teacherprofile::where('staffid',$staff->id)->first();
      $profile = teacherprofile::where('userid',Auth()->user()->id)->first();
      if(($userid->firstlogin == 1) || ($userid->secondlogin == 1)){
        return view('profile',compact('userid','staff','teacher','profile'));
      }else{
        return view('profile.profileupadte',compact('userid','staff','state','city'));
      }
    }
    
    public function teacherstore(Request $request)
    {
      if(!empty(Input::get('userid'))){
        $userdataid = Input::get('userid');
      }else{
        $userdataid = 0;
      }
      // dd($request);
      if(Auth()->user()->firstlogin == 0 || Auth()->user()->secondlogin == 0)
      {
        $userold = User::where('id',Input::get('userid'))->first();
        
        
        $teacher = new teacherprofile;
        $teacher->userid = $userdataid;
        $teacher->staffid = Input::get('staffid');
        $teacher->stateid = Input::get('state');
        $teacher->cityid = Input::get('city');
        $teacher->contact_number = Input::get('contact');
        $teacher->alternate_number = Input::get('alternate_number');
        $teacher->father_name = Input::get('father_name');
        $teacher->mother_name = Input::get('mother_name');
        $teacher->dob = Input::get('dob');
        $teacher->adhar_card_number = Input::get('aadhar_num');
        $teacher->marital_status = Input::get('martial_status');
        $teacher->religion = Input::get('religion');
        $teacher->address1 = Input::get('address1');
        $teacher->address2 = Input::get('address2');
        $teacher->pincode = Input::get('pin_code');
        $teacher->graduation_college_name = Input::get('college_name');
        $teacher->graduation_percentage = Input::get('graduation_percentage');
        $teacher->last_job = Input::get('last_school');
        $teacher->last_job_designation = Input::get('last_school_designation');
        $teacher->school_Leaving_date = Input::get('school_Leaving_date');
        $teacher->salary = Input::get('last_salary');
        $teacher->last_increment_date = Input::get('last_increment_date');
        $teacher->last_increment_amount = Input::get('last_increment');
        if ($request->hasFile('profilepicture')) {
  
  				$files = Input::file('profilepicture');
  
  				$name = time() . "_" . $files->getClientOriginalName();
  
  				$image = $files->move(public_path() . '/theme/teacher/profile', $name);
  
  				$teacher->profile_pic = $name;
  
          }
        if ($request->hasFile('aadharfirst')) {
  
  				$files = Input::file('aadharfirst');
  
  				$name = time() . "_" . $files->getClientOriginalName();
  
  				$image = $files->move(public_path() . '/theme/teacher/aadhar', $name);
  
  				$teacher->aadharfirst = $name;
  
          }
        if ($request->hasFile('aadharsecond')) {
  
  				$files = Input::file('aadharsecond');
  
  				$name = time() . "_" . $files->getClientOriginalName();
  
  				$image = $files->move(public_path() . '/theme/teacher/aadhar', $name);
  
  				$teacher->aadharsecond = $name;
  
          }
        if ($request->hasFile('exp_certificate')) {
  
  				$files = Input::file('exp_certificate');
  
  				$name = time() . "_" . $files->getClientOriginalName();
  
  				$image = $files->move(public_path() . '/theme/teacher/lastjob', $name);
  
  				$teacher->last_job_reliaving_certificate = $name;
  
          }
        if ($request->hasFile('salary1')) {
  
  				$files = Input::file('salary1');
  
  				$name = time() . "_" . $files->getClientOriginalName();
  
  				$image = $files->move(public_path() . '/theme/teacher/salary', $name);
  
  				$teacher->salary2 = $name;
  
          }
        if ($request->hasFile('salary2')) {
  
  				$files = Input::file('salary2');
  
  				$name = time() . "_" . $files->getClientOriginalName();
  
  				$image = $files->move(public_path() . '/theme/teacher/salary', $name);
  
  				$teacher->salary3 = $name;
  
          }
        $teacher->save();
        $usernamecounter = User::where('id',Input::get('userid'))->first();
        $userupdate = User::where('id',Input::get('userid'))->first();
        $userupdate->firstlogin = 1;
        $userupdate->secondlogin = 1;
        $userupdate->username = Input::get('username');
        $userupdate->gender = Input::get('gender');
        $userupdate->email = Input::get('email');
        $userupdate->password = bcrypt(Input::get('password'));
        $userupdate->usernamecount = $usernamecounter->usernamecount -1;
        $userupdate->save();
      }
      $userid = User::where('id',Auth::user()->id)->first();
      $teacher = teacherprofile::where('staffid',Input::get('staffid'))->first();
      $profile = teacherprofile::where('userid',Auth()->user()->id)->first();
      if(!Hash::check(Input::get('password'), $userold->password)){
          $request->session()->flush();
          return redirect('/');
      }else{
        if($userold->email == Input::get('email')){
        return view('profile',compact('userid','staff','teacher','profile'));
      }else{
        $request->session()->flush();
        return redirect('/');
      }
        return view('profile',compact('userid','staff','teacher','profile'));
        //return redirect('/');
      }
      
    }
    
    public function imagesave(Request $request)
    {
      $teacher = teacherprofile::where('id',Input::get('teacherid'))->first();
      $teacher->profile_pic = $request->userimage;
      $teacher->save();
      return redirect()->back();
    }
    
    public function updatedata(Request $request)
    {
      
      $teacher = teacherprofile::where('id',$request->teacherid)->first();
      if(!empty($request->contact_number)){
        $teacher->contact_number = $request->contact_number;
      }elseif(!empty($request->alternate_number)){
        $teacher->alternate_number = Input::get('alternate_number');
      }elseif(!empty($request->father_name)){
        $teacher->father_name = $request->father_name;
      }elseif(!empty($request->mother_name)){
        $teacher->mother_name = $request->mother_name;
      }elseif(!empty($request->dob)){
        $teacher->dob = Input::get('dob');
      }elseif(!empty($request->adhar_card_number)){
        $teacher->adhar_card_number = $request->adhar_card_number;
      }elseif(!empty($request->religion)){
        $teacher->religion = $request->religion;
      }elseif(!empty($request->graduation_college_name)){
        $teacher->graduation_college_name = $request->graduation_college_name;
      }elseif(!empty($request->last_job)){
        $teacher->last_job = $request->last_job;
      }elseif(!empty($request->last_job_designation)){
        $teacher->last_job_designation = $request->last_job_designation;
      }elseif(!empty($request->school_Leaving_date)){
        $teacher->school_Leaving_date = $request->school_Leaving_date;
      }elseif(!empty($request->salary)){
        $teacher->salary = $request->salary;
      }elseif(!empty($request->last_increment_date)){
        $teacher->last_increment_date = $request->last_increment_date;
      }elseif(!empty($request->last_increment_amount)){
        $teacher->last_increment_amount = $request->last_increment_amount;
      }elseif(!empty($request->facebook)){
        $teacher->facebook = $request->facebook;
      }elseif(!empty($request->instagram)){
        $teacher->instagram = $request->instagram;
      }elseif(!empty($request->twitter)){
        $teacher->twitter = $request->twitter;
      }
      $teacher->save();
      
      // $userid = User::where('id',Auth::user()->id)->first();
      // $staff = staffmember::where('userid',$userid->id)->first();
      $teacher = teacherprofile::where('id',$request->teacherid)->first();
      
      return $teacher;
      ///return view('profile',compact('userid','staff','teacher'));
      
    }
    public function userdataupdate(Request $request)
    {
      $user = User::where('id',$request->userid)->first();
      $usercount = User::where('id',$request->userid)->first();
      if(!empty($request->name)){
        $user->name = $request->name;
      }elseif(!empty($request->username)){
        if($user->usernamecount> 0){
          $user->username = $request->username;
          $user->usernamecount = $usercount->usernamecount-1;
        }
      }elseif(!empty($request->email)){
        $user->email = $request->email;
      }elseif(!empty($request->changepassnew)){
        $user->password = bcrypt($request->changepassnew);
      }
      $user->save();
      $user = User::where('id',$request->userid)->first();
      return $user;
    }
    
    public function passwordcheck(Request $request)
    {
      $input = $request->all();
      $user = User::where('id',$request->userid)->first();
      //dd($user->password);
      if(!Hash::check($input['oldpass'], $user->password)){
        return 0;
      }else{
        return 1;
      }
      
    }
    
    public function userprofiledetail(Request $request)
    {
      $userid = User::where('id',Auth::user()->id)->first();
      $staffid = staffmember::where('userid',$userid->id)->first();
      $teacher = teacherprofile::where('staffid',$staffid->id)->first();
      $profile = teacherprofile::where('userid',Auth()->user()->id)->first();
      //dd($teacher);
      return view('userprofile.settingprofile',compact('userid','staffid','teacher','profile'));
    }
}
