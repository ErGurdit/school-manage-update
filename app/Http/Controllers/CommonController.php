<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\expense;
use App\classes;
use App\paymentmethod;
use Illuminate\Support\Facades\Input;


class CommonController extends Controller
{
    public function show(Request $request,$id,$userid)
    {
      $expense = DB::table('expenses')
                ->select('expenses.*','paymentmethods.payment_display_name')
                ->join('users','users.id','=','expenses.userid')
                ->join('paymentmethods','paymentmethods.id','=','expenses.payment_type')
                ->where('expenses.id',$id)->first();
              //dd($expense);
      $userdata = $userid;
      return view('principaltest.expense.show',compact('expense','userdata'));
    }
}
