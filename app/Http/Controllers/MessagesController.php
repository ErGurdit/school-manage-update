<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use DB;

use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function sendMessage(Request $request)
    {
    	{
	    	$msg = $request->msg;
	    	$conID = $request->conID;

	    	//fetch user_to
	    	$fetch_userTo = DB::table('messages')
	    	->where('conservation_id',$conID)
	    	->where('user_to','!=',Auth::user()->id)
	    	->get();
	    	$userTo = $fetch_userTo[0]->user_to;

	    	//send message
	    	$sendM = DB::table('messages')->insert([
	    		'user_to' => $userTo,
	    		'user_from' =>Auth::user()->id,
	    		'msg' => $msg,
	    		'status' => 1 ,
	    		'conservation_id' => $conID
	    	]);

	    	if($sendM){
	    		$userMsg = DB::table('messages')
					->join('users','users.id','=','messages.user_from')
					->where('messages.conservation_id',$conID)
					->get();
					return $userMsg;
	    	}
	    }
	}

	public function newMessage(Request $request)
	{
		{
			//fetch not logged in user
			$friends = DB::table('users')
			        ->where('users.id','!=',Auth::user()->id)
					->get();
				//dd($friends);
			return view('profile.friends',compact('friends'));
		}
	}

	 public function sendNewMessage(Request $request)
	 {
        $msg = $request->msg;
        $friend_id = $request->friend_id;
        $myID = Auth::user()->id;
        
        

        //check if conversation already started or not
        $checkCon1 = DB::table('conversations')->where('user_one',$myID)
        ->where('user_two',$friend_id)->get(); // if loggedin user started conversation
        $checkCon2 = DB::table('conversations')->where('user_two',$myID)
        ->where('user_one',$friend_id)->get(); // if loggedin recviced message first
        $allCons = array_merge($checkCon1->toArray(),$checkCon2->toArray());
        
         if(count($allCons)!=0){
        	echo count($allCons);
          $conID_old = $allCons[0]->id;
        //   // old conversation
          //insert data into messages table
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conservation_id' =>  $conID_old,
            'status' => 1
          ]);
        }else {
		// new conversation
          $conID_new = DB::table('conversations')->insertGetId([
            'user_one' => $myID,
            'user_two' => $friend_id
          ]);
          echo $conID_new;
          $MsgSent = DB::table('messages')->insert([
            'user_from' => $myID,
            'user_to' => $friend_id,
            'msg' => $msg,
            'conservation_id' =>  $conID_new,
            'status' => 1
          ]);
        //   $MsgSent = new Message;
        //   $MsgSent->user_from = $myID;
        //   $MsgSent->user_to = $friend_id;
        //   $MsgSent->msg = $msg;
        //   $MsgSent->conservation_id = $conID_new;
        //   $MsgSent->status = 1;
        //   $MsgSent->save();
        }
    }
}
