<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\school;
use App\staffmember;
use Auth;
use DB;
use App\User;
use Session;
use App\StudentAttendance;

class StudentAttendanceController extends Controller
{
    //
    public function index()
    {
                //dd($request);
       $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
       $school =  school::where('id',$staffmember->schoolid)->first();
        $myclass = DB::table('classes')
                            ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                            ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                            ->where('class_to_schools.schoolid',$school->id)
                            ->get();

      
          return view('principaltest.studattend.index',compact('myclass','current_date'));

    }
    
    
    public function store(Request $request)
    {
       //dd($request);
       
       //dd($request->attendance);
       if(!empty($request->attendance))
       {
           //$userid = array();
           
           $dataSet = [];
foreach ($request->attendance as $key => $value) {
    
    $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
           $school =  school::where('id',$staffmember->schoolid)->first();

               $present = $value == "present" ? 1 : 0;
              $absent = $value == "absent" ? 1 : 0;
                            $leave = $value == "leave" ? 1 : 0;

               $holiday = $value == "holiday" ? 1 : 0 ;
    //dd($key);
    $staff_attend  = DB::table('staff_attendances')
         ->select('staff_attendances.*','staff_attendances.userid as u')
         ->whereIn('staff_attendances.userid',[$key])
         ->where('schoolid',$school->id)
         ->where('updated_at','=',DB::raw('curdate()'))
         ->get();
         //dd($staff_attend);
                         $cnt = count(json_decode($staff_attend));
                         //dd($cnt);

    if($cnt == 0)
 {
    $dataSet[] = [
        'schoolid'  => $school->id,
        'userid'    => $key,
         'name' => $request->nme,
       'date' => date('Y/m/d'),
       'present' => $present,
       'absent' =>  $absent,
       'holiday' => $holiday,
              'leave' => $leave,

       'account_id' => $staffmember['id'],
        'status' => 1,
        'created_at' => date('Y/m/d'),
        'updated_at' => date('Y/m/d'),
        
        ];
 }//if
 else
     {
                       return back()->with('danger','Already marked');
    
     }
}//foreach

           
       }  //if attendance

     DB::table('staff_attendances')->insert($dataSet);
                       return back()->with('success','Attendace marked');



    }
}


