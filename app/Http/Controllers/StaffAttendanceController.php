<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\school;
use App\staffmember;
use Auth;
use DB;
use App\User;
use Session;
use App\StaffAttendance;

class StaffAttendanceController extends Controller
{
    //
    public function index()
    {
                //dd($request);
       $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
       $school =  school::where('id',$staffmember->schoolid)->first();

       $staff = DB::table('staffmembers')
         ->select('staffmembers.*','staffmembers.userid as uid','users.*','roles.display_name as dn','users.name as uname','role_user.role_id as roleid')
         ->join('users','users.id','=','staffmembers.userid')
         ->join('role_user','role_user.user_id','=','staffmembers.userid')
         ->join('roles','roles.id','=','role_user.role_id')
         ->where('staffmembers.schoolid',$school->id)
         ->WhereNotIn('roles.id',[1,2,5])
         ->get();
         //dd($staff);
        
         
         $staff_attend  = DB::table('staff_attendances')
         ->select('staff_attendances.*','staff_attendances.userid as u')
         ->where('status',1)->get()->pluck('u');
         $staff_attnd = json_encode($staff_attend);

                
                $workdays = array();
                $type = CAL_GREGORIAN;
                $month = date('n'); // Month ID, 1 through to 12.
                $year = date('Y'); // Year in 4 digit 2009 format.
                $day_count = cal_days_in_month($type, $month, $year); // Get the amount of days
                
                //loop through all days
                for ($i = 1; $i <= $day_count; $i++) {
                
                        $date = $year.'/'.$month.'/'.$i; //format date
                        $get_name = date('l', strtotime($date)); //get week day
                        $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
                
                        //if not a weekend add day to array
                        if($day_name != 'Sun' && $day_name != 'Sat'){
                            $workdays[] = $date;
                        }

}
               $current_date = date('Y/m/d');

        //dd($current_date); 
          return view('principaltest.staffattend.index',compact('current_date','staff','workdays'));

    }
    public function store(Request $request)
    {
       //dd($request->attendance);
       if(!empty($request->attendance))
       {
           //$userid = array();
           
           $dataSet = [];
foreach ($request->attendance as $key => $value) {
    
    $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
               $present = $value == "present" ? 1 : 0;
              $absent = $value == "absent" ? 1 : 0;
                            $leave = $value == "leave" ? 1 : 0;

               $holiday = $value == "holiday" ? 1 : 0 ;
    //dd($key);
    $staff_attend  = DB::table('staff_attendances')
         ->select('staff_attendances.*','staff_attendances.userid as u')
         ->whereIn('staff_attendances.userid',[$key])
         ->where('schoolid',$request->schoolid)
         ->where('updated_at','=',DB::raw('curdate()'))
         ->get();
         //dd($staff_attend);
                         $cnt = count(json_decode($staff_attend));
                         //dd($cnt);

    if($cnt == 0)
 {
    $dataSet[] = [
        'schoolid'  => $request->schoolid,
        'userid'    => $key,
         'name' => $request->nme,
       'date' => $request->cur_date,
       'present' => $present,
       'absent' =>  $absent,
       'holiday' => $holiday,
              'leave' => $leave,

       'account_id' => $staffmember['id'],
        'status' => 1,
        'created_at' => date('Y/m/d'),
        'updated_at' => date('Y/m/d'),
        
        ];
 }//if
 else
     {
                       return back()->with('danger','Already marked');
    
     }
}//foreach

           
       }  //if attendance

     DB::table('staff_attendances')->insert($dataSet);
                       return back()->with('success','Attendace marked');



    }
}
