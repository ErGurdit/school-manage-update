<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\country;
use App\state;
use App\city;
use App\school;
use App\offer;
use App\offer_to_school;
use Illuminate\Support\Facades\Input;
use DB;

class offercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = country::all();
        $state = state::all();
        $city = city::all();
        $school = school::all();
        $myoffer = offer::all();
        $offertoschool = offer_to_school::all();
        $offer =DB::table('offer_to_schools')
        ->select('offers.*','offer_to_schools.countryid','offer_to_schools.schoolid','offer_to_schools.stateid','offer_to_schools.stateid','offer_to_schools.cityid','schools.name as sname','countries.name as cname','states.name as sname','cities.name as cityname','schools.name as schoolname')
         ->join('schools', 'schools.id', '=', 'offer_to_schools.schoolid')
			->join('states', 'states.id', '=', 'offer_to_schools.stateid')
			->join('cities', 'cities.id', '=', 'offer_to_schools.cityid')
			->join('countries', 'countries.id', '=', 'offer_to_schools.countryid')
			->join('offers', 'offers.id', '=', 'offer_to_schools.offerid')
			->get();
			$tempoffer=array();
			$tempcounter=0;
		foreach($offer as $offers){
		    $tempoffer[$offers->offer_name]['schoolname'][$offers->schoolname]=$offers->schoolname;
		    $tempoffer[$offers->offer_name]['cityname'][$offers->cityname]=$offers->cityname;
		    $tempoffer[$offers->offer_name]['sname'][$offers->sname]=$offers->sname;
		    $tempoffer[$offers->offer_name]['cname'][$offers->cname]=$offers->cname;
		   $tempcounter++;
		    $sch['school'] = $offers->schoolname;
		    
		}
		
		$finaldata=array();
		foreach($tempoffer as $k=>$v){
		    $finaldata[$k]['schoolname']=implode(", ",$v['schoolname']);
		    $finaldata[$k]['schoolnamearray']=$v['schoolname'];
		    $finaldata[$k]['cityname']=implode(", ",$v['cityname']);
		    $finaldata[$k]['citynamearray']=$v['cityname'];
		    $finaldata[$k]['sname']=implode(", ",$v['sname']);
		    $finaldata[$k]['snamearray']=$v['sname'];
		    $finaldata[$k]['cname']=implode(", ",$v['cname']);
		    $finaldata[$k]['cnamearray']=$v['cname'];
		}
		if(!empty($k)){
		 $schooldata = $finaldata[$k]['schoolname'];
		 $countrydata = $finaldata[$k]['cityname'];
		 $statedata = $finaldata[$k]['sname'];
		 $citydata = $finaldata[$k]['cname'];
		 $schoolnamearray = $finaldata[$k]['schoolnamearray'];
		 $cnamearray = $finaldata[$k]['cnamearray'];
		 $snamearray = $finaldata[$k]['snamearray'];
		 $citynamearray = $finaldata[$k]['citynamearray'];
		}
		

        return view('admin.offer.index',compact('school','country','state','city','offertoschool','myoffer','schooldata','countrydata','statedata','citydata','offer','schoolnamearray','cnamearray','snamearray','citynamearray'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $offer = new offer;
        $offer->offer_name = Input::get('name');
        $offer->first_date = Input::get('datestart');
        $offer->last_date = Input::get('dateend');
        $offer->offer_desc = Input::get('offdesc');
        $offer->amount = Input::get('amount');
        $slugdata = str_slug(Input::get('name'));
        $slugCount = count($offer->whereRaw("slug REGEXP '^{$slugdata}(-[0-9]*)?$'")->get());
        $slugdata = ($slugCount > 0) ? "{$slugdata}-{$slugCount}" : $slugdata;
        $offer->slug = $slugdata;
        $offer->save();
        $input = Input::all();
        $school = $input['schoolname'];
        $country = $input['country'];
        $state = $input['state'];
        $city= $input['city'];
        foreach($school as $key1=>$value1){
          foreach($country as $key2=>$value2){
              foreach($state as $key3=>$value3){
                  foreach($city as $key4=>$value4){
                    $offertoschool = new offer_to_school;
                    $offertoschool->offerid = $offer->id;
                    $offertoschool->schoolid = Input::get('schoolname')[$key1];
                    $offertoschool->countryid = Input::get('country')[$key2];
                    $offertoschool->stateid = Input::get('state')[$key3];
                    $offertoschool->cityid = Input::get('city')[$key4];
                    $offertoschool->save(); 
                    }
                 
              }
            
          }
            
            
        }
        
        return redirect('offer');
}

    /*Status Change*/
     public function ajaxChangeStatus( $id, Request $request )
    {
        $new_status = $request->get( 'status' );
        $offer = offer::where( 'id', $id )->first();
        $offer->status = $new_status;
        $offer->save();
    }
    
     //active status
    public function show(Request $request)
    {
        $country = country::all();
        $state = state::all();
        $city = city::all();
        $school = school::all();
        $myoffer = offer::where('status',1)->get();
         $offer =DB::table('offer_to_schools')
        ->select('offers.*','offer_to_schools.countryid','offer_to_schools.schoolid','offer_to_schools.stateid','offer_to_schools.stateid','offer_to_schools.cityid','schools.name as sname','countries.name as cname','states.name as sname','cities.name as cityname','schools.name as schoolname')
         ->join('schools', 'schools.id', '=', 'offer_to_schools.schoolid')
			->join('states', 'states.id', '=', 'offer_to_schools.stateid')
			->join('cities', 'cities.id', '=', 'offer_to_schools.cityid')
			->join('countries', 'countries.id', '=', 'offer_to_schools.countryid')
			->join('offers', 'offers.id', '=', 'offer_to_schools.offerid')
			->where('offers.status',1)
			->get();
			$tempoffer=array();
			$tempcounter=0;
		foreach($offer as $offers){
		    $tempoffer[$offers->offer_name]['schoolname'][$offers->schoolname]=$offers->schoolname;
		    $tempoffer[$offers->offer_name]['cityname'][$offers->cityname]=$offers->cityname;
		    $tempoffer[$offers->offer_name]['sname'][$offers->sname]=$offers->sname;
		    $tempoffer[$offers->offer_name]['cname'][$offers->cname]=$offers->cname;
		   $tempcounter++;
		    $sch['school'] = $offers->schoolname;
		    
		}
		
	$finaldata=array();
		foreach($tempoffer as $k=>$v){
		    $finaldata[$k]['schoolname']=implode(", ",$v['schoolname']);
		    $finaldata[$k]['schoolnamearray']=$v['schoolname'];
		    $finaldata[$k]['cityname']=implode(", ",$v['cityname']);
		    $finaldata[$k]['citynamearray']=$v['cityname'];
		    $finaldata[$k]['sname']=implode(", ",$v['sname']);
		    $finaldata[$k]['snamearray']=$v['sname'];
		    $finaldata[$k]['cname']=implode(", ",$v['cname']);
		    $finaldata[$k]['cnamearray']=$v['cname'];
		}
		if(!empty($k)){
		 $schooldata = $finaldata[$k]['schoolname'];
		 $countrydata = $finaldata[$k]['cityname'];
		 $statedata = $finaldata[$k]['sname'];
		 $citydata = $finaldata[$k]['cname'];
		 $schoolnamearray = $finaldata[$k]['schoolnamearray'];
		 $cnamearray = $finaldata[$k]['cnamearray'];
		 $snamearray = $finaldata[$k]['snamearray'];
		 $citynamearray = $finaldata[$k]['citynamearray'];
		}

        return view('admin.offer.index',compact('school','country','state','city','offertoschool','myoffer','schooldata','countrydata','statedata','citydata','offer','schoolnamearray','cnamearray','snamearray','citynamearray'));
    }
    
    //inactive status
    public function showin(Request $request)
    {
        $country = country::all();
        $state = state::all();
        $city = city::all();
        $school = school::all();
        $myoffer = offer::where('status',0)->get();
        $offertoschool = offer_to_school::all();
        $offer =DB::table('offer_to_schools')
        ->select('offers.*','offer_to_schools.countryid','offer_to_schools.schoolid','offer_to_schools.stateid','offer_to_schools.stateid','offer_to_schools.cityid','schools.name as sname','countries.name as cname','states.name as sname','cities.name as cityname','schools.name as schoolname')
         ->join('schools', 'schools.id', '=', 'offer_to_schools.schoolid')
			->join('states', 'states.id', '=', 'offer_to_schools.stateid')
			->join('cities', 'cities.id', '=', 'offer_to_schools.cityid')
			->join('countries', 'countries.id', '=', 'offer_to_schools.countryid')
			->join('offers', 'offers.id', '=', 'offer_to_schools.offerid')
			->where('offers.status',0)
			->get();
			
			$tempoffer=array();
			$tempcounter=0;
		foreach($offer as $offers){
		    $tempoffer[$offers->offer_name]['schoolname'][$offers->schoolname]=$offers->schoolname;
		    $tempoffer[$offers->offer_name]['cityname'][$offers->cityname]=$offers->cityname;
		    $tempoffer[$offers->offer_name]['sname'][$offers->sname]=$offers->sname;
		    $tempoffer[$offers->offer_name]['cname'][$offers->cname]=$offers->cname;
		   $tempcounter++;
		    $sch['school'] = $offers->schoolname;
		    
		}
		
		$finaldata=array();
		foreach($tempoffer as $k=>$v){
		    $finaldata[$k]['schoolname']=implode(", ",$v['schoolname']);
		    $finaldata[$k]['schoolnamearray']=$v['schoolname'];
		    $finaldata[$k]['cityname']=implode(", ",$v['cityname']);
		    $finaldata[$k]['citynamearray']=$v['cityname'];
		    $finaldata[$k]['sname']=implode(", ",$v['sname']);
		    $finaldata[$k]['snamearray']=$v['sname'];
		    $finaldata[$k]['cname']=implode(", ",$v['cname']);
		    $finaldata[$k]['cnamearray']=$v['cname'];
		}
		if(!empty($k)){
		 $schooldata = $finaldata[$k]['schoolname'];
		 $countrydata = $finaldata[$k]['cityname'];
		 $statedata = $finaldata[$k]['sname'];
		 $citydata = $finaldata[$k]['cname'];
		 $schoolnamearray = $finaldata[$k]['schoolnamearray'];
		 $cnamearray = $finaldata[$k]['cnamearray'];
		 $snamearray = $finaldata[$k]['snamearray'];
		 $citynamearray = $finaldata[$k]['citynamearray'];
		}

        return view('admin.offer.index',compact('school','country','state','city','offertoschool','myoffer','schooldata','countrydata','statedata','citydata','offer','schoolnamearray','cnamearray','snamearray','citynamearray'));
  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $offer = offer::where('id',$id)->first();
        $offer->offer_name = Input::get('name');
        $offer->first_date = Input::get('datestart');
        $offer->last_date = Input::get('dateend');
        $offer->offer_desc = Input::get('offdesc');
        $offer->amount = Input::get('amount');
        $slugdata = str_slug(Input::get('name'));
        $slugCount = count($offer->whereRaw("slug REGEXP '^{$slugdata}(-[0-9]*)?$'")->get());
        $slugdata = ($slugCount > 0) ? "{$slugdata}-{$slugCount}" : $slugdata;
        $offer->slug = $slugdata;
        $offer->save();
        
        $input = Input::all();
        $school = $input['schoolname'];
        $country = $input['country'];
        $state = $input['state'];
        $city= $input['city'];
        $offerdelete = DB::table('offer_to_schools')->where('offerid',$offer->id)->delete();
        foreach($school as $key1=>$value1){
          foreach($country as $key2=>$value2){
              foreach($state as $key3=>$value3){
                  foreach($city as $key4=>$value4){
                    $offertoschool = new offer_to_school;
                    $offertoschool->offerid = $offer->id;
                    $offertoschool->schoolid = Input::get('schoolname')[$key1];
                    $offertoschool->countryid = Input::get('country')[$key2];
                    $offertoschool->stateid = Input::get('state')[$key3];
                    $offertoschool->cityid = Input::get('city')[$key4];
                    $offertoschool->save(); 
                    }
                 
              }
            
          }
            
            
        }
        
        return redirect('offer');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $offertoschool = offer_to_school::where('offerid',$id)->first();
        if(!empty($offertoschool)){
        DB::table('offer_to_schools')->where('offerid',$id)->delete();
        DB::table('offers')->where('id',$id)->delete();
        }
        return back()->withMessage("School Deleted");
    }
}
