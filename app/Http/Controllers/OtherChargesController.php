<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\school;
use App\staffmember;
use App\route_drivers_data;
use App\DressCharges;
use App\BookCharges;
use Notification;
use App\Notifications\DatabaseNotification;
use App\Notifications\EditNotify;
use App\Notifications\DeleteNotify;
use App\Notifications\BookChargesNotify;
use App\Notifications\DressChargesNotify;
use App\Notifications\BusChargesNotify;
use App\Notifications\DressEdit;
use App\Notifications\DressDel;
use App\Notifications\BookEdit;
use App\Notifications\BookDel;
use App\Notifications\ApprovedVehicle;
use App\Notifications\DeclineVehicle;
use App\Notifications\ApproveMoneyByAccounts;
use App\Notifications\DeclineMoneyByAccounts;
use Session;
use App\User;



class OtherChargesController extends Controller
{
    //
    public function index(Request $request)
    {
       $userid = Auth()->user()->id;
        $schoolid = school::where('userid',$userid)->first();
        $staffmember = staffmember::where('userid',$userid)->first();
                    $school =  school::where('id',$staffmember->schoolid)->first();
                    $busroute = DB::table('busroutes')->where('schoolid',$school->id)->get();
        	//		->join('classes', 'classes.id', '=', 'studentprofiles.current_class')
           //dd($request); 

         $myclass = DB::table('classes')
                            ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                            ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                            //->join('dress_charges','dress_charges.classid','=','classes.id')
                            ->where('class_to_schools.schoolid',$school->id)
                            ->get();
        //dd($staffmember); 
        $route_data = DB::table('route_drivers_datas')->where('schoolid',$school->id)->get();
        //dd($route_data);
        $dress_charges = DB::table('dress_charges')
                            ->select('dress_charges.*','classes.class_display_name as classname')
                            ->join('classes','classes.id','=','dress_charges.classid')
                            ->where('dress_charges.schoolid',$school->id)
                            ->get();
                            $book_charges = DB::table('book_charges')
                            ->select('book_charges.*','classes.class_display_name as classname')
                            ->join('classes','classes.id','=','book_charges.classid')
                            ->where('book_charges.schoolid',$school->id)
                            ->get();;
                return view('principaltest.account.othercharges',compact('book_charges','busroute','dress_charges','myclass','staffmember','route_data'));

    }
    
    public function store(Request $request)
    {
        //dd($request);
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
       $school =  school::where('id',$staffmember->schoolid)->first();
       $tranport = $request->vehcle;
        $count_student = DB::table('feesubmits')->where('schoolid',$school->id)->where('transportcharge',$tranport)->get();
               $vehicle = DB::table('busroutes')->where('schoolid',$school->id)->where('fare',$tranport)->get();
//dd($school->userid);
       //dd(count($count_student));
       //dd($vehicle[0]->routename);

       $uid = Auth()->user()->id;

        $route_data = new route_drivers_data;
        $route_data->userid =  $uid;
        $route_data->schoolid =  $school->id;
        //$route_data->vehicle_number =  $vehicle[0]->routename;
        $route_data->vehicle_number =  $request->vehcle;
        $route_data->driver_name =  $request->dr_name;
        $route_data->driver_contact =  $request->dr_contact;
        $route_data->helper =  $request->helper;
        $route_data->helper_contact =  $request->helper_contact;
        $route_data->count_student =  count($count_student);
        $route_data->charges =  $request->charges;
        $route_data->status =  0;
        $route_data->save();
        $data = 'Notification for Route';
        //$id = $route_data->id;
        Notification::send(User::find($school->userid),new DatabaseNotification($data));
        //return back()->withMessage("Drivers Data Added");
        
        return redirect()->back()->with("success", "Drivers Data Added"); 
     


    }
    
    public function approved(Request $request)
    {
       //dd($request);
        $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
       $route_data = DB::table('route_drivers_datas')->where('schoolid',$staffmember->schoolid)->get();
       $sch_i = $staffmember->schoolid;

					    $feest = DB::table('notifications')
					   ->select('notifications.*')
					   ->where('notifiable_id',$userid)->where('type','=','App\Notifications\Feestatus')->get();


					   $notee = DB::table('notifications')
					   ->select('notifications.*')
					   ->where('notifiable_id',$userid)->where('type','=','App\Notifications\DatabaseNotification')->get();
//dd(json_encode($notify));

                      $count = count($notee);
                      $count_fee = count($feest);
                      $c = $count + $count_fee;

       return view('principaltest.account.approved',compact('c','count_fee','feest','count','notee','sch_i','school','route_data'));
    }
    
    public function approvemoney(Request $request)
    {
       //dd($request);
        $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
       $sch_i = $staffmember->schoolid;
       $money  = DB::table('approve_moneys')
                ->select('approve_moneys.*')
              	->where('approve_moneys.updated_at','=', DB::raw('curdate()'))
                ->where('approve_moneys.current_user_id',$userid)
                ->where('schoolid',$school->id)
                ->get();
					    
       return view('principaltest.account.approvemoney',compact('money'));
    }
    public function ajaxChangeStatus(Request $request,$id)
    {
        $userid = Auth()->user()->id;
        $schoolid = school::where('userid',$userid)->first();
        $accnt_id = DB::table('staffmembers')
    ->select('staffmembers.userid as accnt_id')
    ->join('role_user','role_user.user_id','=','staffmembers.userid')
    ->where('role_user.role_id',8)
    ->where('staffmembers.schoolid',$schoolid->id)
    ->get()->pluck('accnt_id');
        
        
        DB::table('route_drivers_datas')->where('id',$id)
           ->update(array('status' => $request->status));
           if($request->status == 2)
        {
                 
          $data = 'Notification for Vehicle Decline';
          Notification::send(User::find($accnt_id[0]),new DeclineVehicle($data));
                 return redirect()->back()->with("danger","Vehicle Declined");
   
        }
        else
        {
         $data = 'Notification for Vehicle Approve';
         //query for account email
         /*SELECT st.userid
         FROM  `staffmembers` AS st
          INNER JOIN  `role_user` AS rl ON rl.user_id = st.userid
        WHERE rl.role_id =8
         AND st.schoolid =73*/
    
    //dd($accnt_id[0]);
         Notification::send(User::find($accnt_id[0]),new ApprovedVehicle($data));
                return redirect()->back()->with("success","Vehicle Approved");
        }

    }
    public function ajaxChangeStatusApproveMoney(Request $request,$id)
    {
        
        $userid = Auth()->user()->id;
        
        //dd($request);
        //$schoolid = school::where('userid',$userid)->first();
        $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
       $sch_i = $staffmember->schoolid;
                //dd($school->userid);

        /*$accnt_id = DB::table('staffmembers')
    ->select('staffmembers.userid as accnt_id')
    ->join('role_user','role_user.user_id','=','staffmembers.userid')
    ->where('role_user.role_id',7)
    ->where('staffmembers.schoolid',$sch_i)
    ->get()->pluck('accnt_id');*/
        $feesdep = $request->feesdep;
        
        DB::table('approve_moneys')->where('id',$id)
           ->update(array('status' => $request->status));
           if($request->status == 2)
        {
                 
          $data = 'Notification for Money Decline';
         //sending to principal
         Notification::send(User::find($school->userid),new DeclineMoneyByAccounts($data));
        //sending to feesdepart
          Notification::send(User::find($feesdep),new DeclineMoneyByAccounts($data));
              return redirect()->back()->with("danger","Money Declined");
             

        }
        else
        {
         $data = 'Notification for Money Approve';
         //query for feesdepartment email
         
    
    //dd($accnt_id[0]);
     //sending to principal
         Notification::send(User::find($school->userid),new ApproveMoneyByAccounts($data));
        //sending to feesdepart
          
         Notification::send(User::find($feesdep),new ApproveMoneyByAccounts($data));
  
          return redirect()->back()->with("success","Money Approved");
   
        }

    }
    
    public function dressstore(Request $request)
    {
        //dd($request);
        
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
         $school =  school::where('id',$staffmember->schoolid)->first();
        $uid = Auth()->user()->id;
        $dress_c = DB::table('dress_charges')->select('classid')
                ->where('schoolid',$school->id)
                ->where('classid',$request->class)->get();
                $dc = json_encode($dress_c);
                $cnt = count(json_decode($dc));
                
if ($cnt == 0){
        $dress_change = new DressCharges;
        $dress_change->userid =  $uid;
        $dress_change->schoolid =  $school->id;
        $dress_change->classid =  $request->class;
        $dress_change->dress_charges =  $request->dress_charges;
        
        $dress_change->save();
        $data = 'Notification for Adding Dress';

        Notification::send(User::find($school->userid),new DressChargesNotify($data));
  
                 return redirect()->back()->with("success","Dress Change Data Added");

}
        else 
                {
         return redirect()->back()->with("danger","Already Exists");


                }


    }
    public function bookstore(Request $request)
    {
        //dd($request);
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
       $school =  school::where('id',$staffmember->schoolid)->first();
        $uid = Auth()->user()->id;
        $book_c = DB::table('book_charges')->select('classid')
                ->where('schoolid',$school->id)
                ->where('book_name',$request->book_name)
                ->where('classid',$request->class)->get();
                $dc = json_encode($book_c);
                $cnt = count(json_decode($dc));
                
if ($cnt == 0){

        $book_charge = new BookCharges;
        $book_charge->userid =  $uid;
        $book_charge->schoolid =  $school->id;
        $book_charge->classid =  $request->class;
        $book_charge->book_charges =  $request->book_charges;
        $book_charge->book_name  = $request->book_name;
        $book_charge->save();
        $data = 'Notification for Adding Book';

        Notification::send(User::find($school->userid),new BookChargesNotify($data));
  
        return redirect()->back()->with("success","Book Charge Data Added");


    }
    else
    {
        return redirect()->back()->with("danger","Already Added");

    }
    }
    public function update(Request $request, $id)
    {
       
		  $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
      
		$route_data = route_drivers_data::where('id',$id)->first();
		//dd($route_data);
		
        //$route_data->userid =  $uid;
        //$route_data->schoolid =  $school->id;
        $route_data->vehicle_number =  $request->vehcle;
        $route_data->driver_name =  $request->dr_name;
        $route_data->driver_contact =  $request->dr_contact;
        $route_data->helper =  $request->helper;
        $route_data->helper_contact =  $request->helper_contact;
        //$route_data->count_student =  $request->count_student;
        $route_data->charges =  $request->charges;
        //if($request->status == 0)
        //{
        $route_data->status =  $request->status;
        //}
        $route_data->save();
        $data = 'Notification for Editing Route';

        Notification::send(User::find($school->userid),new EditNotify($data));
                return redirect()->back()->with("success","Drivers Data Updated");

    }
    public function dressupdate(Request $request, $id)
    {
       
		//dd($request);
		$d_c = DressCharges::where('id',$id)->first();
		 $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
		//dd($route_data);
		
        //$route_data->userid =  $uid;
        //$route_data->schoolid =  $school->id;
        $d_c->classid =  $request->class;
        $d_c->dress_charges =  $request->dress_charges;
        $d_c->save();
         $data = 'Notification for Dress Update';
        Notification::send(User::find($school->userid),new DressEdit($data));
        return redirect()->back()->with("success","Dress Update");


    }
    public function bookupdate(Request $request, $id)
    {
       
		//dd($request);
		$b_c = BookCharges::where('id',$id)->first();
		 $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
		//dd($route_data);
		
        //$route_data->userid =  $uid;
        //$route_data->schoolid =  $school->id;
        $b_c->classid =  $request->class;
        $b_c->book_name =  $request->book_name;
        $b_c->book_charges =  $request->book_charges;
        $b_c->save();
        $data = 'Notification for Book Update';

        Notification::send(User::find($school->userid),new BookEdit($data));
                return redirect()->back()->with("success","Book Update");


    }
    public function destroy(Request $request,$id)
    {
        
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
         $school =  school::where('id',$staffmember->schoolid)->first();
        $uid = Auth()->user()->id;
        DB::table('route_drivers_datas')->where('id',$id)->delete();
        $data = 'Notification for Delete Route';
        
        Notification::send(User::find($school->userid),new DeleteNotify($data));
        
        return redirect()->back()->with("danger","Route Deleted");
    }
    public function dressdestroy(Request $request,$id)
    {
        
          $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
         $school =  school::where('id',$staffmember->schoolid)->first();
        $uid = Auth()->user()->id;
         $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
        DB::table('dress_charges')->where('id',$id)->delete();
        $data = 'Notification for Delete Dress';

        Notification::send(User::find($school->userid),new DressDel($data));
          return redirect()->back()->with("danger","Dress Charges Deleted");

    }
    public function bookdestroy(Request $request,$id)
    {
        
         $userid = Auth::user()->id;
       $staffmember = staffmember::where('userid',$userid)->first();
       $school = school::where('id',$staffmember->schoolid)->first();
        DB::table('book_charges')->where('id',$id)->delete();
        $data = 'Notification for Delete Book';

        Notification::send(User::find($school->userid),new BookDel($data));
            return redirect()->back()->with("danger","Book Charges Deleted");
    }
}
