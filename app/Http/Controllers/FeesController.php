<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;
use Auth;
use Entrust;
//use model
use Cloudder;
use App\Http\Requests;
use App\classes;
use App\admission;
use App\User;
use App\studentprofile;
use App\paymentmethod;
use App\school;
use App\staffmember;
use App\addcash_maintain;
use App\sectionclass;
use App\country;
use App\state;
use App\city;
use App\subject;
use App\studentsubject;
use App\highclass;
use Illuminate\Support\Str;
use App\maintainfee;
use App\busroute;
use Notification;
use App\Notifications\NewAdmission;
use Session;


class FeesController extends Controller
{
    //define contructor to call anywhere
//   public function __construct(){
//       $this->userid = Auth::user()->id;
//       $this->schoolid = school::where('userid',$this->userid)->first();
//       $this->staffmember = staffmember::where('userid',$this->userid)->first();
//       $this->studentid = studentprofile::where('schoolid',$this->staffmember->schoolid)->get();
//     }
    
    public function index(Request $request){
        $userid = Auth::user()->id;
        
        $schoolid = school::where('userid',$userid)->first();
        
        $staffmember = staffmember::where('userid',$userid)->first();
               //$schol =  school::where('id',$staffmember->schoolid)->first();
//dd($schoolid);
        
        $studentid = studentprofile::where('schoolid',$staffmember->schoolid)->get();
       
        
        $allsubject = DB::table('studentprofiles')
                        ->join('studentsubjects','studentsubjects.studentid','=','studentprofiles.id')
                        ->groupby('studentsubjects.studentid')
                        ->get();
        $data = '';
         foreach($allsubject as $subject){
             
            $data = json_decode($subject->subjectid);
             $newpass = array();
        
        foreach($data as $k => $value)
        {
            $newpass = $value;
        }
            
        //dd($newpass);
        $test = '';
        $overtest = [];
        
        foreach($newpass as $new)
        {
 
            $test = DB::table('subjects')
                    ->select('id','subjectname')
                    ->where('id',$new)->get();
                    

                    
        foreach($test as $newsub){
        
            $overtest[] = ['id' => $newsub->id,
            'subjectname' => $newsub->subjectname];

        }
        
        }
                                                            //dd($overtest);
  
        $sub = $overtest;
        }
    
        $alladmission = DB::table('studentprofiles')
                            ->select('users.*','studentprofiles.*','admissions.*','addcash_maintains.*','schools.name as sname','countries.name as cname','states.name as statename','cities.name as cityname','paymentmethods.payment_display_name','classes.class_display_name as classdisplay')
                            ->join('users','users.id','=','studentprofiles.userid')
                            ->join('schools','schools.id','=','studentprofiles.schoolid')
                            ->join('countries','countries.id','=','studentprofiles.countryid')
                            ->join('states','states.id','=','studentprofiles.stateid')
                            ->join('cities','cities.id','=','studentprofiles.cityid')
                            ->join('admissions','admissions.studentid','=','studentprofiles.id')
                            ->join('paymentmethods','paymentmethods.id','=','admissions.paymethodid')
                            ->join('addcash_maintains','addcash_maintains.studentid','=','studentprofiles.id')
                            ->join('classes','classes.id','=','admissions.classid')
                            ->get();
     
     
        $busroute = busroute::where('schoolid',$staffmember->schoolid)->get();
        
        return view('principaltest.fee.list',compact('alladmission','allsubject','sub','busroute'));
    }
    
    public function getcharge(Request $request)
    {
        //dd($request);
        $feesub = $request->feesub;
        $staffmember = staffmember::where('userid',$request->authenticateuserid)->first();
        $maintainfee = maintainfee::where('schoolid',$staffmember->schoolid)->where('classid',$request->classid)->get();
        return $maintainfee;
    }
    
    //add new student
    public function accountstore(Request $request){
       {
            //dd($request);
            //invoce generate
            $userid = Auth::user()->id;
            $schoolid = school::where('userid',$userid)->first();
            $staffmember = staffmember::where('userid',$userid)->first();
                           $schol =  school::where('id',$staffmember->schoolid)->first();

            $schoolnewid = school::where('id',$staffmember->schoolid)->first(); 
            
            $dataMax = DB::table('admissions')
                    ->select('Invoice_fees as invoice')
                    ->max('Invoice_fees');
                if(empty($dataMax)){
                    $newdata =1;
                }else{
                   $data = explode('-',$dataMax);
                   $newdata = $data[1]+1; 
                }
            $prefixdata = 'SD|'.$schoolnewid->name.'-';
            $invoice = $prefixdata.$newdata;
            
            /*$userid = Auth::user()->id;
            $schoolid = school::where('userid',$userid)->first();
            $staffmember = staffmember::where('userid',$userid)->first();*/
            $subject = subject::all();
            // dd($staffmember);
            $studentid = studentprofile::where('schoolid',$staffmember->schoolid)->get();
           //dd($request);
               $rules = array(
    
    			'leavecertificate' => 'mimes:jpeg,bmp,png',
    
    			'lastclass' => 'mimes:jpeg,bmp,png',
    
    			'aadharfirst' => 'mimes:jpeg,bmp,png',
    			
    			'aadharsecond' => 'mimes:jpeg,bmp,png',
    
        		);
        
        		$validator = Validator::make(Input::all(), $rules);
        
        		if ($validator->fails()) {
        
        			$messages = $validator->messages();
        			//dd($messages);
        
        			return Redirect::back()->withInput()->withErrors($validator);
        
        		} elseif ($validator->passes()) {
        		//new user
                $user = new user;
                $user->name = Input::get('first_name')." ".Input::get('mname')." ".Input::get('last_name');
                $rand = substr(Input::get('father_contact'),6);
                $lastnumber = Input::get('first_name').$rand;
                $lastnumbercount = count($user->whereRaw("username REGEXP '^{$lastnumber}(-[0-9]*)?$'")->get());
                $lastnumber = ($lastnumbercount > 0) ? "{$lastnumber}-{$lastnumbercount}" : $lastnumber;
                $user->username = $lastnumber;
                $user->gender = Input::get('gender');
                $user->email = Input::get('email');
                $newpass = substr(md5(mt_rand()), 0, 7);
                $confirmpass =  bcrypt($newpass);
                $user->password = $confirmpass;
                $user->verify_token = Str::random(40);
                $user->save();
               $strm = $request->stream;
               $section = Input::get('section');
                $section = ($section == '') ? $strm : $section;
                //dd($section);
                //new student
                $student = new studentprofile;
                $student->userid = $userid;
                $student->schoolid = $staffmember->schoolid;
                $student->classid = Input::get('class');
                $student->sectionid = $section;
                $student->student_user_id = $user->id;
                $student->countryid = Input::get('country');
                $student->stateid = Input::get('state');
                $student->cityid = Input::get('city');
                $student->father_name = Input::get('father_name');
                $student->mother_name = Input::get('mother_name');
                $student->DOB = Input::get('dob');
                $student->school_joining_date = date('d-m-Y');
                $student->studen_joining_class = Input::get('class');
                $student->current_class = Input::get('class');
                $student->father_contact_number = Input::get('father_contact');
                $student->mother_contact_number = Input::get('mother_contact');
                $student->home_tel = Input::get('home_tel');
                $student->address = Input::get('address');
                $student->pincode = Input::get('pin');
                $student->last_school = Input::get('last_school');
                $student->save();
                
                // add to fee submit table
            DB::table('feesubmits')
            ->insert(['schoolid'=> $staffmember->schoolid,
            'userid' => $user->id,
            'current_user_id' => $userid,
            'classid'=> Input::get('class'),
            'sectionid'=> $section,
            'studentid'=> $student->id,
            'count_pending'=> 12
            ]);

                //new admission
                //roll number update
                $dataadmin = $request->admissionroom;
                //dd($dataadmin);
                $checkadmin = DB::table('admissions')->latest('rollnumber')->first();
                //dd($checkadmin);
                if(empty($checkadmin->rollnumber)){
                    echo 'bye';
                    $admisiondata = 1;
                    
                }elseif($checkadmin->rollnumber == $dataadmin){
                    $admisiondata = $checkadmin->rollnumber +1;
                    //echo 'hello';
                }
                //dd($admisiondata);
                if(!empty($checkadmin->rollnumber)){
                    $classid = '';
                    $sectionid = '';
                    $schoolid = '';
                }else{
                    $classid = 1;
                    $sectionid = 1;
                    $schoolid = $staffmember->schoolid;
                }
                
                $admission = new admission;
                $admission->classid = Input::get('class');
                $admission->Invoice_fees = $invoice;
                $admission->rollnumber = $admisiondata;
                $admission->paymethodid = Input::get('paytype');
                $admission->studentid = $student->id;
                $admission->schoolid = $staffmember->schoolid;
                $admission->sectionid = $section;
                $admission->dresscharge = Input::get('dresscharge');
                $admission->transportcharge = Input::get('inputRadioscharge');
                $admission->othercharge = Input::get('othercharge');
                $admission->remark = Input::get('remark');
                $admission->bookcharge = Input::get('book_charge');
                $admission->admission_fee = Input::get('admsnfee');
                $admission->school_fee = Input::get('schfee');
                $admission->donation = Input::get('dontaion');
                $admission->discount = Input::get('discountcharge');
                if(Input::get('messagefield') == ''){
                    $admission->return_amount = Input::get('returnamount');
                }elseif(Input::get('returnamount') == ''){
                    $admission->return_amount = Input::get('messagefield');
                }
                if ($request->hasFile('leavecertificate')) 
                    {
                        Cloudder::upload($request->file('leavecertificate'));
                        $c=Cloudder::getResult();       
                        if($c){
                            $admission->school_leave_certificate = $c['secure_url'];
                        }
            			/*$files = Input::file('leavecertificate');
            			$name = time() . "_" . $files->getClientOriginalName();
            			$image = $files->move(public_path() . '/principal/studentdetail/certificate/school', $name);*/
            			
            		}
            	if ($request->hasFile('lastclass')) 
                    {
                        Cloudder::upload($request->file('lastclass'));
                        $c=Cloudder::getResult();       
                        if($c){
                            $admission->class_certificate = $c['secure_url'];
                        }
            			/*$files = Input::file('lastclass');
            			$name = time() . "_" . $files->getClientOriginalName();
            			$image = $files->move(public_path() . '/principal/studentdetail/certificate/class', $name);
            			$admission->class_certificate = $name;*/
            		}
            	if ($request->hasFile('aadharfirst')) 
                    {
                        Cloudder::upload($request->file('aadharfirst'));
                        $c=Cloudder::getResult();       
                        if($c){
                            $admission->aadharfirst = $c['secure_url'];
                        }
            			/*$files = Input::file('aadharfirst');
            			$name = time() . "_" . $files->getClientOriginalName();
            			$image = $files->move(public_path() . '/principal/studentdetail/certificate/aadhar', $name);
            			$admission->aadhar_first = $name;*/
            		}
            	if ($request->hasFile('aadharsecond')) 
                    {
                        Cloudder::upload($request->file('aadhar_second'));
                        $c=Cloudder::getResult();       
                        if($c){
                            $admission->aadhar_second = $c['secure_url'];
                        }
            			/*$files = Input::file('aadharsecond');
            			$name = time() . "_" . $files->getClientOriginalName();
            			$image = $files->move(public_path() . '/principal/studentdetail/certificate/aadhar', $name);
            			$admission->aadhar_second = $name;*/
            		}
             
                $admission->save();
                
                //cash maintain if user is select
                if(Input::get('paytype') == '2'){
                    $cashmaintain = new addcash_maintain;
                    $cashmaintain->studentid = $student->id;
                    $cashmaintain->schoolid = $staffmember->schoolid;
                    $cashmaintain->first2000 = Input::get('first');
                    $cashmaintain->second1000 = Input::get('second');
                    $cashmaintain->third500 = Input::get('three');
                    $cashmaintain->fourth200 = Input::get('four');
                    $cashmaintain->fifth100 = Input::get('five');
                    $cashmaintain->sixth50 = Input::get('six');
                    $cashmaintain->seventh20 = Input::get('seven');
                    $cashmaintain->eighth10 = Input::get('eight');
                    $cashmaintain->ninth5 = Input::get('nine');
                    $cashmaintain->tenth2 = Input::get('ten');
                    $cashmaintain->eleventh1 = Input::get('eleven');
                    $cashmaintain->totalamount = Input::get('finaltotal');
                    $cashmaintain->save();
                }
                if(Input::get('paytype') == '1'){
                    $cashmaintain = new addcash_maintain;
                    $cashmaintain->studentid = $student->id;
                    $cashmaintain->schoolid = $staffmember->schoolid;
                    $cashmaintain->card_holder_name = Input::get('card_name');
                    $cashmaintain->card_number = Input::get('card_number');
                    $cashmaintain->card_type = Input::get('card_type');
                    $cashmaintain->card_expiration = Input::get('card_expiry_date');
                    $cashmaintain->approval_code = Input::get('card_approval_code');
                    $cashmaintain->totalamount = Input::get('finalsubtotal');
                    $cashmaintain->save();
                }
                    //subject add student 
                    
                    //dd($request->subjectdata);
                    if($request->subjectdata !=NULL){
                    //subject
                    $data = array('subjectid'=> $request->subjectdata);
                    $newsub = json_encode($data);
                    $subject = new studentsubject;
                    $subject->studentid = $student->id;
                    $subject->subjectid = $newsub;
                    $subject->save();
                    }else{
                    //stream
                    $datastream = array('subjectid'=> $request->stream);
                    $newsub = json_encode($datastream);
                    $subject = new studentsubject;
                    $subject->studentid = $student->id;
                    $subject->subjectid = $newsub;
                    $subject->save();
                    }
        
            }
            
            //class get
            $class = classes::where('id',Input::get('class'))->first();
            //section get
            if($section != '')
            {
            $section = sectionclass::where('id',Input::get('section'))->first();
            }
            //subject get
            $mysubject = [];
            foreach($request->subjectdata as $subject){
                $subject = subject::where('id',$subject)->get();
                foreach($subject as $subjects){
                    $mysubject[] = $subjects->subjectname;
                }
            }
            //country
            $country = country::where('id',Input::get('country'))->first();
            //State
            $state = state::where('id',Input::get('state'))->first();
            //city
            $city = city::where('id',Input::get('city'))->first();
            //student information
            $StudentDetail=[];
            $StudentDetail['name'] = Input::get('first_name')." ".Input::get('mname')." ".Input::get('last_name');
            $StudentDetail['username'] = $lastnumber;
            $StudentDetail['gender'] = Input::get('gender');
            $StudentDetail['class'] = $class->class_display_name;
            if($section != '')
            {
            $StudentDetail['section'] = $section->section_display_name;
            }
            $StudentDetail['email'] = Input::get('email');
            $StudentDetail['subject'] = implode(',',$mysubject);
            $StudentDetail['father'] = Input::get('father_name');
            $StudentDetail['mother'] = Input::get('mother_name');
            $dob = explode('/',Input::get('dob'));
            $newdob = $dob[1].'/'.$dob[0].'/'.$dob[2];
            $StudentDetail['dob'] = $newdob;
            $StudentDetail['father_contact'] = Input::get('father_contact');
            $StudentDetail['mother_contact'] = Input::get('mother_contact');
            $StudentDetail['home_tel'] = Input::get('home_tel');
            $StudentDetail['country'] = $country->name;
            $StudentDetail['state'] = $state->name;
            $StudentDetail['city'] = $city->name;
            $StudentDetail['pincode'] = Input::get('pin');
            $StudentDetail['address'] = Input::get('address');
            $StudentDetail['admission_fee'] = Input::get('admsnfee');
            $StudentDetail['book_charge'] = Input::get('book_charge');
            $StudentDetail['dress_charge'] = Input::get('dresscharge');
            $StudentDetail['monthly_charges'] = Input::get('schfee');
            $StudentDetail['other_charge'] = Input::get('othercharge');
            $StudentDetail['remark'] = Input::get('remark');
            $StudentDetail['discount'] = Input::get('discountcharge');
            $StudentDetail['donation'] = Input::get('dontaion');
            $StudentDetail['transport'] = Input::get('inputRadioscharge');
            $StudentDetail['finaltotal'] = Input::get('finaltotal');
            //dd($StudentDetail['finaltotal']);
            if(Input::get('paytype') == 1){
                $paymentmode = 'card';
            }else if(Input::get('paytype') == 2){
                 $paymentmode = 'cash';
            }
            $StudentDetail['payment_mode'] = $paymentmode;
            
            
        }
        //dd($StudentDetail);
         $data = 'Notification for New Admission';

        Notification::send(User::find($schol->userid),new NewAdmission($data));
  
        return redirect()->back()->with("success","New admission added successfully");
        //return view('principaltest.fee.studentdetail',compact('StudentDetail'));
        //return redirect('fee');
    }
    // public function studentfee(){
    //     return view('principaltest.fee.studentdetail');
    // }
    
        //feestatus
        public function feestatus(Request $request)
        {
            {
                $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
                
                $school =  school::where('id',$staffmember->schoolid)->first();
                //allclasses
                $myclass = DB::table('classes')
                            ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                            ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                            ->where('class_to_schools.schoolid',$school->id)
                            ->get();
                
                //allsubject
                /*$section = DB::table('sectionclasses')
                          ->select('sectionclasses.id as sid','sectionclasses.*','class_to_sections.id as csid','class_to_sections.*')
                          ->join('class_to_sections','class_to_sections.sectionid','=','sectionclasses.id')
                          ->where('class_to_sections.schoolid',$school->id)
                          ->get();*/
                
                $subject = subject::all();
                $country = country::all();
                $state = state::all();
                $city = city::all();
                $payment = paymentmethod::all();
                
                $admission = DB::table('admissions')->latest('rollnumber')->first();
                if(!empty($admission)){
                    $rollno = $admission->rollnumber;
                }else{
                    $rollno = '';
                }
                
                //bus route
                $busroute = busroute::where('schoolid',$staffmember->schoolid)->get();
                
                //return view('principaltest.fee.teststudent');
                //dd($stream);
                
                return view('principaltest.fee.fee',compact('myclass','stream','section','subject','country','state','city','payment','rollno','busroute'));
            }
        }
        
       
        
        public function classsection(Request $request){
            
            $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            $school =  school::where('id',$staffmember->schoolid)->first();
            
            $section = DB::table('sectionclasses')
                          ->select('sectionclasses.id as sid','sectionclasses.*','class_to_sections.id as csid','class_to_sections.*')
                          ->join('class_to_sections','class_to_sections.sectionid','=','sectionclasses.id')
                          ->where('class_to_sections.schoolid',$school->id)
                          ->where('class_to_sections.classid',$request->classid)
                          ->get();
            return $section;
            
        }
        public function classstream(Request $request)
        {
            $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            $school =  school::where('id',$staffmember->schoolid)->first();
            
            $stream = DB::table('highclasses')
                        ->select('highclasses.id as hid','highclasses.*','class_to_sections.id as csid','class_to_sections.*')
                        ->join('class_to_sections','class_to_sections.sectionid','=','highclasses.id')
                        ->where('class_to_sections.schoolid',$school->id)
                        ->where('class_to_sections.classid',$request->classid)
                        ->get();
            return $stream;
        }
        public function classsubject(Request $request){
            
            $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            $school =  school::where('id',$staffmember->schoolid)->first();
            
            $subject = DB::table('assignsubjects')
                          ->select('assignsubjects.subjectid as sid','assignsubjects.*','subjects.id as csid','subjects.*')
                          ->join('subjects','subjects.id','=','assignsubjects.subjectid')
                          ->where('assignsubjects.schoolid',$school->id)
                          ->where('assignsubjects.classid',$request->classid)
                          ->get();
            return $subject;
            
        }
        public function leftsubject(Request $request){
            
            $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            $school =  school::where('id',$staffmember->schoolid)->first();
 $id_s = DB::table('assignsubjects')
                          ->select('assignsubjects.subjectid as sid','assignsubjects.*','subjects.id as csid','subjects.*')
                          ->join('subjects','subjects.id','=','assignsubjects.subjectid')
                          ->where('assignsubjects.schoolid',$school->id)
                          ->where('assignsubjects.classid',$request->classid)
                          ->get()
                          ->pluck('sid');
                          
                          //dd($id_s);
 
 $subject = DB::table('assignsubjects')
                          ->select('assignsubjects.subjectid as sid','assignsubjects.*','subjects.id as csid','subjects.*')
                          ->join('subjects','subjects.id','=','assignsubjects.subjectid')
                          ->where('assignsubjects.schoolid',$school->id)
                          ->where('assignsubjects.streamtid',null)
                          ->whereNotIn('assignsubjects.subjectid',$id_s)
                          ->whereNotIn('assignsubjects.classid',[$request->classid])
                          ->get();
                          return $subject;
            
        }
        public function sstreamsubject(Request $request){
            
            $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            $school =  school::where('id',$staffmember->schoolid)->first();
            
            $subject = DB::table('assignsubjects')
                          ->select('assignsubjects.subjectid as sid','assignsubjects.*','subjects.id as csid','subjects.*')
                          ->join('subjects','subjects.id','=','assignsubjects.subjectid')
                          ->where('assignsubjects.schoolid',$school->id)
                          ->where('assignsubjects.classid',$request->classid)
                          //->whereNotNull('assignsubjects.streamtid')
                          ->get();
            return $subject;
            
        }
}
