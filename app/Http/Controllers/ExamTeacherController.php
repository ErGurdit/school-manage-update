<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use DB;
use Redirect;
use App\school;
use App\classes;
use App\staffmember;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Examteacher;
use App\subject;
use App\sectionclass;
use App\studentprofile;

class ExamTeacherController extends Controller
{
    //
    public function index(Request $request)
    {
       $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
       $school =  school::where('id',$staffmember->schoolid)->first();
       $class = classes::all();
        $myclass = DB::table('classes')
                            ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                            ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                            ->where('class_to_schools.schoolid',$school->id)
                            ->get();
                            
        $subject = subject::all();
        $section = sectionclass::all();


       
        //dd($school);
        
        
        $schoolstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->join('classes','classes.id','=','studentprofiles.current_class')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        $allstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        
        $user = DB::table('role_user')
        ->select('users.name as uname','users.username as username','users.email as uemail')
        ->join('users','role_user.user_id','=','users.id')
        ->join('roles','role_user.role_id','=','roles.id')
        ->where('roles.name','student')
        ->get();
        
        
        $exam = DB::table('examteachers')
        ->select('examteachers.*','users.name as uname','users.username as username','users.email as uemail','users.id as userid','examteachers.total_marks','examteachers.id as eid','examteachers.obtain_marks','examteachers.percentage','examteachers.passing_marks','examteachers.status','examteachers.remarks','classes.class_name','examteachers.classid','examteachers.id')
        //->join('examtypes','examtypes.id','=','exams.examid')
        ->join('users','users.id','=','examteachers.studentid')
        ->join('classes','classes.id','=','examteachers.classid')
        ->get();
        
        //dd($section[0]->sectionclass);
        return view('principaltest.examteacher.index',compact('myclass','class','section','user','school','exam','schoolstudent','class','allstudent','examtype','subject'));
    }
    
     public function getSudentListStream(Request $request)
    {
          $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();
                $userid =  studentprofile::where('current_class', $request->classid)->pluck('userid');
                  $sectionid = $request->sectionid;
         $student = DB::table('users')
                        ->select('users.id as uid','users.*','studentprofiles.id as spid','studentprofiles.*')
                        ->join('studentprofiles','studentprofiles.student_user_id','=','users.id')
                        ->where('studentprofiles.classid',$request->classid)
                        //->where('studentprofiles.sectionid',$request->sectionid)
                        //->where('feesubmits.current_user_id',$staffmember->userid)
                        ->get();
                                                return $student;

    }
     public function getSudentList(Request $request)
    {
        //dd($request);
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            //dd($staffmember['id']);
            $school =  school::where('id',$staffmember->schoolid)->first();
                $userid =  studentprofile::where('current_class', $request->classid)->pluck('userid');
                  $sectionid = $request->sectionid;
                       
                       
                        $student = DB::table('users')
                        ->select('users.id as uid','users.*','studentprofiles.id as spid','studentprofiles.*')
                        ->join('studentprofiles','studentprofiles.student_user_id','=','users.id')
                        ->where('studentprofiles.classid',$request->classid)
                        ->where('studentprofiles.sectionid',$request->sectionid)
                        //->where('feesubmits.current_user_id',$staffmember->userid)
                        ->get();
                       
                        return $student;
		//return json_encode($userid);
    }
    public function store(Request $request)
    {
        //dd($request);
        $exam = new Examteacher;
       $uid = Auth()->user()->id;
        $exam->userid = $uid;
        $exam->classid = Input::get('class');
        $exam->schoolid = Input::get('schoolid');
        $exam->sectionid = Input::get('section');
        $exam->studentid = Input::get('student_lst');
        //$exam->subjectid = Input::get('schoolid');

        $exam->total_marks = Input::get('tomarks');
        $exam->obtain_marks = Input::get('obtain_marks');
        $exam->passing_marks = Input::get('passing_marks');
        $exam->percentage = Input::get('percentage');
        $exam->status = Input::get('status');
        $exam->remarks = Input::get('remarks');
        $exam->save();
        
        $message = "Exam Add Successfully";
        
        return redirect()->back()->with($message);
    }
    
    public function update(Request $request,$id)
    {
        //dd($request);
        $exam = Examteacher::where('id',$id)->first();
      $uid = Auth()->user()->id;
        $exam->userid = $uid;
        $exam->classid = Input::get('classid');
        $exam->schoolid = Input::get('schoolid');
        $exam->sectionid = Input::get('sectionid');
        //$exam->studentid = Input::get('student_lst');
        //$exam->subjectid = Input::get('schoolid');

        $exam->total_marks = Input::get('tomarks');
        $exam->obtain_marks = Input::get('obtain_marks');
        $exam->passing_marks = Input::get('passing_marks');
        $exam->percentage = Input::get('percentage');
        $exam->status = Input::get('status');
        $exam->remarks = Input::get('remarks'); $exam->save();
        
        return redirect('examteacher');
    }
    
    public function destroy(Request $request,$id)
    {
        
        DB::table('examteachers')->where('id',$id)->delete();
        return back()->withMessage("Exam Deleted Deleted");
    }
    
    public function fetchdata($exam_id)
    {
        
        $useridfetch = User::where('id',Auth::user()->id)->first();
        $school = school::where('userid',$useridfetch->id)->first();
        $class = classes::all();
        
        
        $schoolstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->join('classes','classes.id','=','studentprofiles.current_class')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        $allstudent = DB::table('studentprofiles')
                        ->join('users','users.id','=', 'studentprofiles.userid')
                        ->where('studentprofiles.schoolid',$school->id)
                        ->get();
        
        //$examtype = examtype::all();
        $user = DB::table('role_user')
        ->select('users.name as uname','users.username as username','users.email as uemail')
        ->join('users','role_user.user_id','=','users.id')
        ->join('roles','role_user.role_id','=','roles.id')
        ->where('roles.name','student')
        ->get();
        
        $exams = DB::table('examteachers')
        ->select('users.name as uname','users.username as username','users.email as uemail','users.id as userid','examteacher.total_marks','examteacher.id as eid','examteacher.obtain_marks','examteacher.percentage','examteacher.passing_marks','examteacher.result','classes.class_name','examteacher.classid','examteacher.id')
        //->join('examtypes','examtypes.id','=','exams.examid')
        ->join('users','users.id','=','examteacher.userid')
        ->join('classes','classes.id','=','examteacher.classid')
        ->where('examteacher.id',$exam_id)
        ->first();
        
        return view('principaltest.examteacher.exam',compact('user','school','exams','schoolstudent','class','allstudent','examtype'));
    }
}
