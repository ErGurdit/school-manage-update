<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\school;
use App\classes;
use App\User;
use Auth;
use Illuminate\Support\Facades\Input;
use App\class_to_school;
use App\class_to_section;
use App\sectionclass;
use App\highclass;
use App\stream_to_class;
use Redirect;
use Validator;
use App\subject;
use App\assignsubject;

class assignsubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
    //   $this->userid = Auth::user()->id;
    //   $this->schoolid = school::where('userid',$this->userid)->first();
    }
    public function index()
    {
        
        //dd();
        // current user
        $user = DB::table('schools')
        ->select('schools.name as schoolname','schools.id as schoolid')
        ->join('users','users.id','=','schools.userid')
        ->where('users.id',Auth::user()->id)
        ->first();
        
        $schoolid = school::where('userid',Auth::user()->id)->first();
        /*$class = DB::table('assignsubjects')
            ->select('classes.class_name','classes.id as cid','classes.class_display_name as cdisplay','assignsubjects.schoolid as schoolid','classes.has_stream as hasstream')
            ->join('classes','classes.id','=','assignsubjects.classid')
            ->join('subjects','subjects.id','=','assignsubjects.subjectid')
            ->groupBy('assignsubjects.classid')
            ->get();*/
        $class = DB::table('class_to_sections')
            ->select('class_to_sections.id','class_to_sections.*','classes.class_name','classes.id as cid','classes.class_display_name as cdisplay','classes.has_stream as hasstream')
            ->join('classes','classes.id','=','class_to_sections.classid')
            ->groupBy('class_to_sections.classid')
            ->where('class_to_sections.schoolid',$schoolid->id)
            ->get();
       // dd($class);
        $myclass = DB::table('classes')
                    ->select('classes.id as cid','classes.*','class_to_schools.id as csid','class_to_schools.*')
                    ->join('class_to_schools','class_to_schools.classid','=','classes.id')
                    ->where('class_to_schools.schoolid',$schoolid->id)
                    ->where('classes.status',0)
                    ->get();
        $subject = subject::all();
        // $classsection = DB::table('class_to_sections')
        //                 ->join('classes','classes.id','=','class_to_sections.classid')
        //                 ->join('sectionclasses','sectionclasses.id','=','class_to_sections.sectionid')
        //                 ->where('class_to_sections.schoolid',$schoolid->id)
        //                 ->get();
        $classsection = DB::table('assignsubjects')
        ->join('classes','classes.id','=','assignsubjects.classid')
        ->join('subjects','subjects.id','=','assignsubjects.subjectid')
        ->where('assignsubjects.schoolid',$schoolid->id)
        ->get();
        // dd($classsection->toArray());
        //class_to_school
        $newclass = class_to_school::where('schoolid',$schoolid->id)->get();
        //dd($servicedata);
        // map all classes and sections
        {
            $classsection_map = [];
            foreach( $classsection as $temp_section ) {
                $clsid = $temp_section->classid;
                $secid = $temp_section->subjectid;
                $strid = $temp_section->streamtid;
                if( !array_key_exists( $clsid, $classsection_map ) ) {
                    $classsection_map[ $clsid ] = [];
                }
                if( $strid ) {
                    if( !array_key_exists( $strid, $classsection_map[ $clsid ] ) ) {
                        $classsection_map[ $clsid ][ $strid ] = [];
                    }
                    if( !in_array( $secid, $classsection_map[ $clsid ][ $strid ] ) ) {
                        $classsection_map[ $clsid ][ $strid ][] = $secid;
                    }
                    continue;
                }
                if( !in_array( $secid, $classsection_map[ $clsid ] ) ) {
                    $classsection_map[ $clsid ][] = $secid;
                }
            }
            $classsection_map = array_filter($classsection_map);
        }
        
        // doing this
        if(!empty($newclass)) {
            $temp = array();
            foreach($newclass as $k => $v){
                $temp['classid'][$k] = $v['classid'];
                $testclass = $temp['classid'];
            }
            if(!empty($testclass)){
                $newdata = $testclass;
            }
        }
        //dd($newdata);
        
        
        
//         //section select
        	$tempoffer=array();
			$tempcounter=0;
	    foreach($classsection as $classsection){
		    //$tempoffer[$classsection->classid]['sectionid'][$classsection->sectionid]=$classsection->sectionid;
		    $tempoffer[$classsection->subjectid]['classid'][$classsection->classid]=$classsection->classid;
		    $tempcounter++;
		    $sch['subjectid'] = $classsection->subjectid;
		    
		}
		//dd($sch);
		
		$finaldata=array();
		foreach($tempoffer as $k=>$v){
		    //$finaldata[$k]['sectionid']=$v['sectionid'];
		    $finaldata[$k]['classid']=$v['classid'];
		}
		if(!empty($finaldata)){
		 $tempsec = $finaldata[$k]['classid'];
		  //print_r($tempsec);
		}
		//dd($tempsec);
		//exit;
 		//newstream
		
 		$stream = DB::table('highclasses')
 		            ->join('class_to_sections','class_to_sections.sectionid','=','highclasses.id')
 		            ->where('status',0)
 		            ->wherein('class_to_sections.classid',[14,15])
 		            ->get();
 		$streamfirst = DB::table('highclasses')
 		            ->select('highclasses.id as hid','highclasses.*','class_to_sections.id as cid','class_to_sections.*')
 		            ->join('class_to_sections','class_to_sections.sectionid','=','highclasses.id')
 		            ->where('status',0)
 		            ->where('class_to_sections.classid',14)
 		            ->groupBy('highclasses.id')
 		            ->get();
 		    // dd($streamfirst);
 		$streamsecond = DB::table('highclasses')
 		            ->select('highclasses.id as hid','highclasses.*','class_to_sections.id as cid','class_to_sections.*')
 		            ->join('class_to_sections','class_to_sections.sectionid','=','highclasses.id')
 		            ->where('status',0)
 		            ->where('class_to_sections.classid',15)
 		            ->groupBy('highclasses.id')
 		            ->get();
 		            //dd($stream);
//dd($classsection_map);
        $school = school::where('userid',Auth::user()->id)->first();
        $sch_i  = $school->id;

       return view('principaltest.assignsubject.index',compact('sch_i','class','user','school','subject','service','classsection','servicedata','myclass','newdata',
       'tempsec','classsection_map','newclass','stream','streamfirst','streamsecond'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignsubjectupdate(Request $request)
    {
        //dd($request->subjects);
        //get auth id and schholid
        $userid = Auth::user()->id;
        $schoolid = school::where('userid',$userid)->first();
        //delete old data
         DB::table('assignsubjects')->where('schoolid',$schoolid->id)->delete();
        
        $classes = array_keys($request->classes);
        //dd($request);
            // save sections
            //$subj = array($request->subjects);
           // dd($request->subjects);
            //$newArray = array();
            //print_r($request->subjects);
            foreach( (array) $request->subjects as $classid => $subjects) 
            {
                foreach( $subjects as $stream_id => $subject_id) 
                {
                    $assign_to_subject = new assignsubject;
                    $assign_to_subject->schoolid = $schoolid->id;
                    $assign_to_subject->classid = $classid;
                    $assign_to_subject->subjectid = $subject_id;
                    $assign_to_subject->save();
                }
            }
            foreach( (array) $request->subjects_s as $classid => $subjects) 
            {
                foreach( $subjects as $stream_id => $s_subjects ) 
                {
                    foreach( $s_subjects as $subject_id )
                    {
                        $assign_to_subject = new assignsubject;
                        $assign_to_subject->schoolid = $schoolid->id;
                        $assign_to_subject->classid = $classid;
                        $assign_to_subject->subjectid = $subject_id;
                        $assign_to_subject->streamtid = $stream_id;
                        $assign_to_subject->save();
                    }
                }
            }
            return redirect('assignsubject');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function assignsubjectstore(Request $request)
    {
        //dd($request->classname);
        foreach($request->classname as $classes){
        $newclass= strtolower(str_replace(' ','',$classes));
        $class = new classes;
        $class->class_name =  $newclass;
        $class->class_display_name = $classes;
        $class->save();
        
        }
        return redirect('assignsubject');
    }
    
     public function classstore(Request $request)
    {
        //dd($request->classname);
        foreach($request->classname as $classes){
        $newclass= strtolower(str_replace(' ','',$classes));
        $class = new class_to_school;
        $class->classid =  $newclass;
        $class->schoolid = Input::get('schoolid');
        $class->save();
        
        }
        return redirect('assignsubject');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class = classes::where('id',$id)->first();
        $class->class_name =  Input::get('classname');
        $class->save();
        return redirect('assignsubject');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request,$id)
    {
        
        DB::table('class_to_schools')->where('id',$id)->delete();
        return back()->withMessage("Classes Deleted");
    }
    
    public function fetchclass()
    {
        
        //dd();
        // current user
        $user = DB::table('schools')
        ->select('schools.name as schoolname','schools.id as schoolid')
        ->join('users','users.id','=','schools.userid')
        ->where('users.id',Auth::user()->id)
        ->first();
        
        
       
        $class = DB::table('class_to_schools')
        ->select('schools.name as schoolname','classes.class_name','classes.id as cid','classes.class_display_name as cdisplay','class_to_schools.classid as classid','class_to_schools.schoolid as schoolid','classes.has_stream as hasstream')
        ->join('classes','classes.id','=','class_to_schools.classid')
        ->join('schools','schools.id','=','class_to_schools.schoolid')
        ->get();
        $schoolid = school::where('userid',AUth::user()->id)->first();
        $myclass = classes::where('status',0)->get();
        $subjects = subject::all();
        $classsection = DB::table('class_to_sections')
                        ->join('classes','classes.id','=','class_to_sections.classid')
                        ->join('sectionclasses','sectionclasses.id','=','class_to_sections.sectionid')
                        ->where('class_to_sections.schoolid',$schoolid->id)
                        ->get();
        //dd($classsection);
        //class_to_school
        $newclass = class_to_school::where('schoolid',$schoolid->id)->get();
        // map all classes and sections
        {
            $classsection_map = [];
            foreach( $classsection as $temp_section ) {
                $clsid = $temp_section->classid;
                $secid = $temp_section->sectionid;
                if( !array_key_exists( $clsid, $classsection_map ) ) {
                    $classsection_map[ $clsid ] = [];
                }
                if( !in_array( $secid, $classsection_map[ $clsid ] ) ) {
                    $classsection_map[ $clsid ][] = $secid;
                }
            }
            $classsection_map = array_filter($classsection_map);
            // dd($classsection_map);
        }
        
        // doing this
        if(!empty($newclass)) {
            $temp = array();
            foreach($newclass as $k => $v){
                $temp['classid'][$k] = $v['classid'];
                $testclass = $temp['classid'];
            }
            if(!empty($testclass)){
                $newdata = $testclass;
            }
        }
        
        
        //section select
        	$tempoffer=array();
			$tempcounter=0;
	    foreach($classsection as $classsection){
		    //$tempoffer[$classsection->classid]['sectionid'][$classsection->sectionid]=$classsection->sectionid;
		    $tempoffer[$classsection->sectionid]['classid'][$classsection->classid]=$classsection->classid;
		    $tempcounter++;
		    $sch['sectionid'] = $classsection->sectionid;
		    
		}
		
		$finaldata=array();
		foreach($tempoffer as $k=>$v){
		    //$finaldata[$k]['sectionid']=$v['sectionid'];
		    $finaldata[$k]['classid']=$v['classid'];
		}
		if(!empty($finaldata)){
		 $tempsec = $finaldata[$k]['classid'];
		  //print_r($tempsec);
		}
		
		//newstream
		
		$stream = highclass::where('status',0)->get();
		//stream select
		$newstream = stream_to_class::where('schoolid',$schoolid->id)->get();
		$tempstream=array();
			$tempcounter=0;
		foreach($newstream as $newstream){
		    //$tempoffer[$classsection->classid]['sectionid'][$classsection->sectionid]=$classsection->sectionid;
		    $tempstream[$newstream->sectionid]['stremid'][$newstream->stremid]=$newstream->stremid;
		    $tempcounter++;
		    $sch['stremid'] = $newstream->stremid;
		    
		}
		
		$finalstream=array();
		foreach($tempstream as $k=>$v){
		    //$finaldata[$k]['sectionid']=$v['sectionid'];
		    $finalstream[$k]['stremid']=$v['stremid'];
		}
		if(!empty($finalstream)){
		 $tempstr = $finalstream[$k]['stremid'];
		 //print_r($tempstr);
		}

	
        if(!isset($tempstr)){
            $tempstr = array();
        }
       return view('principaltest.assignsubject.assignsubjects',compact('class','user','school','myclass','newclass','newdata','sectionclass','tempsec','tempclass','stream','tempstr','classsection_map'));
        
    }

}
