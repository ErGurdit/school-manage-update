<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Role;
use App\Permission;
use DB;
use Entrust;
use App\User;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $user = User::all();
        //$role = role::all();
        if(Entrust::hasRole('superadmin')){
           $role = role::all();
           return view('role.index',compact('role'));
        }elseif(Entrust::hasRole('principal')){
            $role = role::where('role_create',1)->get();
            return view('principaltest.role.index',compact('role','user'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = permission::all();
        if(Entrust::hasRole('superadmin')){
        return view('role.create',compact('permission'));
        }elseif(Entrust::hasRole('principal')){
            return view('principaltest.role.create',compact('permission'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Entrust::hasRole('superadmin')){
            $role = Role::create($request->except(['permission','_token']));
            
            foreach($request->permission as $key=>$value){
                $role->attachpermission($value);
            }
            return redirect()->route('role.index')->withMessage('Role Created');
        }elseif(Entrust::hasRole('principal')){
            $display_name=str_replace(" ","",$request->display_name);
            $role = new role;
            $role->role_create  = Input::get('role_create');
            $role->name=$display_name;
            $role->display_name = Input::get('display_name');
            $role->description = Input::get('description');
            $role->save();
            foreach($request->permission as $key=>$value){
                $role->attachpermission($value);
            }
                return redirect('principaltest/role')->withMessage('Role Created');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function permission(Request $request)
    {
        $roletype = role::all();
        $role = DB::table('permission_role')
                ->select('roles.id','roles.display_name','roles.description','roles.status','permission_role.role_id as roleid','permission_role.permission_id as perid')
                ->join('roles','permission_role.role_id','=','roles.id')
                ->join('permissions','permission_role.role_id','=','permissions.id')
                ->get();
               //dd($role);
        $permission = permission::all();
        
        $roleper = DB::table('permission_role')
        ->select('roles.id as rid','roles.display_name as rdisplay','roles.description as rdesc','roles.status as rstatus','permissions.id as pid','permissions.display_name','permission_role.permission_id as perid','permission_role.role_id as roleid')
        ->join('roles','roles.id','=','permission_role.role_id')
        ->join('permissions','permissions.id','=','permission_role.permission_id')
        ->get();
        
        //dd($roleper);
        
        return view('role.permission',compact('roleper','permission','role','roletype'));
    }
    
    //change status
    public function ajaxChangeStatus( $id, Request $request )
    {
        $new_status = $request->get( 'status' );
        $school = role::where( 'id', $id )->first();
        $school->status = $new_status;
        $school->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = role::find($id);
        $permission = permission::all();
        $role_permission = $role->perms()->pluck('id','id')->toArray();
        if(Entrust::hasRole('superadmin')){
        return view('role.edit',compact('role','role_permission','permission'));
        }elseif(Entrust::hasRole('principal')){
            return view('principaltest.role.edit',compact('role','role_permission','permission'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $role = Role::find($id);
         $role->name = $request->name;
         $role->display_name = $request->display_name;
         $role->description = $request->description;
         $role->save();
         
         DB::table('permission_role')->where('role_id',$id)->delete();
        
        foreach($request->permission as $key=>$value){
            $role->attachpermission($value);
        }
        return redirect()->route('role.index')->withMessage('Role Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('roles')->where('id',$id)->delete();
        return back()->withMessage("Role Deleted");
    }
}
