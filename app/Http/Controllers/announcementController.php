<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\announcement;
use App\User;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;
use Auth;
use Entrust;
use App\school;
use App\Notifications\RepliedToThread;

class announcementController extends Controller
{
    public function index(Request $request){
        $userid = User::where('id',Auth::user()->id)->first();
        $school = school::where('userid',$userid->id)->first();
        
        if(Entrust::hasRole('superadmin')){
            $announce = announcement::where('userid',$userid->id)->get();
            return view('admin.defaultsettings.announcement.index',compact('announce'));
        }elseif(Entrust::hasRole('principal')){
            $announce = announcement::where('userid',$userid->id)->where('schoolid',$school->id)->get();
            return view('principaltest.announcement.index',compact('announce','school'));
        }
    }
    
    public function store(Request $request){
        
        if(Entrust::hasRole('superadmin')){
        //enddate
        if(!empty(Input::get('dateend'))){
        $enddate = Input::get('dateend');
        $res = explode("/", $enddate);
        $endDate = $res[2]."-".$res[0]."-".$res[1];
        }else{
           $endDate = ''; 
        }
        //startdate
        if(!empty(Input::get('datestart'))){
        $date1 = Input::get('datestart');
        $res1 = explode("/", $date1);
        $startDate = $res1[2]."-".$res1[0]."-".$res1[1];
        }
        else{
           $startDate = ''; 
        }
        }elseif(Entrust::hasRole('principal')){
            //enddate
        if(!empty(Input::get('dateend'))){
        $enddate = Input::get('dateend');
        $res = explode("/", $enddate);
        $endDate = $res[0]."-".$res[1]."-".$res[2];
        }else{
           $endDate = ''; 
        }
        //startdate
        if(!empty(Input::get('datestart'))){
        $date1 = Input::get('datestart');
        $res1 = explode("/", $date1);
        $startDate = $res1[0]."-".$res1[1]."-".$res1[2];
        }
        else{
           $startDate = ''; 
        }
        }
        $announce = new announcement;
        $announce->userid = Auth::user()->id;
        $announce->schoolid = Input::get('schoolid');
        $announce->title = Input::get('title');
        $announce->description = Input::get('pagedataset');
        $announce->startdate = $startDate;
        $announce->enddate = $endDate;
        $announce->save();
        
        $note = Input::get('title');
        
        // auth()->user()->notify(new RepliedToThread($note));
        auth()->user()->notify(new RepliedToThread($note));
        return redirect('announcement');
    }
    
    public function update(Request $request,$id){
        
        if(Entrust::hasRole('superadmin')){
        //enddate
        if(!empty(Input::get('dateend'))){
        $enddate = Input::get('dateend');
        $res = explode("/", $enddate);
        $endDate = $res[2]."-".$res[0]."-".$res[1];
        }else{
           $endDate = ''; 
        }
        //startdate
        if(!empty(Input::get('datestart'))){
        $date1 = Input::get('datestart');
        $res1 = explode("/", $date1);
        $startDate = $res1[2]."-".$res1[0]."-".$res1[1];
        }
        else{
           $startDate = ''; 
        }
        }elseif(Entrust::hasRole('principal')){
            //enddate
        if(!empty(Input::get('dateend'))){
        $enddate = Input::get('dateend');
        $res = explode("/", $enddate);
        $endDate = $res[0]."-".$res[1]."-".$res[2];
        }else{
           $endDate = ''; 
        }
        //startdate
        if(!empty(Input::get('datestart'))){
        $date1 = Input::get('datestart');
        $res1 = explode("/", $date1);
        $startDate = $res1[0]."-".$res1[1]."-".$res1[2];
        }
        else{
           $startDate = ''; 
        }
        }
        //dd($startDate);
        $announce = announcement::where('id',$id)->first();
        $announce->title = Input::get('title');
        $announce->description = Input::get('pagedataset');
        $announce->startdate = $startDate;
        $announce->enddate = $endDate;
        $announce->save();
        return redirect('announcement');
    }
    
     public function destroy(Request $request,$id)
    {
        
        DB::table('announcements')->where('id',$id)->delete();
        return back()->withMessage("Announcement Deleted Deleted");
    }
    
    public function fetchdata( $announce_id )
    {
        if(Auth::guest()){
            return redirect('login');
        } else {
            
            $userid = User::where('id',Auth::user()->id)->first();
            $school = school::where('userid',$userid->id)->first();
            
            $announces = announcement::where('id',$announce_id)->where('schoolid',$school->id)->first();
            
                return view('principaltest.announcement.announcement',compact('announces','school'));
        }
    }
}
