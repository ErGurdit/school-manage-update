<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\country;
use App\state;
use App\city;
use App\school;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;
use App\User;
use App\staffmember;
use App\teacherprofile;
use Zizaco\Entrust\EntrustRole;
use App\class_to_school;
use App\classes;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = country::all();
        $state = state::all();
        $city = city::all();
        $school =DB::table('schools')

			->select('schools.*', 'countries.name as countryname', 'states.name as statename', 'cities.name as cityname')

			->join('countries', 'countries.id', '=', 'schools.countryid')
			->join('states', 'states.id', '=', 'schools.stateid')
			->join('cities', 'cities.id', '=', 'schools.cityid')
			->get();
        return view('admin.school.index',compact('school','country','state','city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
        $country = country::all();
        $state = state::all();
         $city = city::all();
         return view('admin.school.create',compact('country','state','city'));
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $rules = array(

			'name' => 'required',

			'mobile' => 'required',

			'email' => 'required|email|unique:users',
			
			'filename' => 'required|mimes:jpeg,png,jpg,gif,svg|max:3072',
			
			'password'  =>  'required|min:6',
            
            'confirmfpassword'  =>  'required|same:password',



		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$messages = $validator->messages();

			return Redirect::back()->withInput()->withErrors($validator);

		} elseif ($validator->passes()) {
		    
		    //country set
		    if(!empty(Input::get('country'))){
		        $country = Input::get('country');
		        
		    }else{
		        $country = 1;
		    }
		    //state set
		    if(!empty(Input::get('state'))){
		        $state = Input::get('state');
		    }else{
		        $state = 10;
		    }
		    //city set
		    if(!empty(Input::get('city'))){
		        $city = Input::get('city');
		    }else{
		        $city = 121;
		    }
		    
		 //user create by school
		$user = new user;
		$user->name = Input::get('name');
		$user->email = Input::get('email');
		$user->password = bcrypt(Input::get('password'));
		$user->save();
		
		//role assign
		$user->roles()->attach($request->role);
		
		//school create
		//dd($request);
        $school = new school;
        $school->name = Input::get('name');
        $school->userid = $user->id;
        $school->principal_name = Input::get('principalname');
        $school->countryid = $country;
        $school->stateid = $state;
        $school->cityid = $city;
        $school->time = Input::get('timeset');
        $school->address = Input::get('address');
        $school->mobile = Input::get('mobile');
        $school->landline = Input::get('telephone');
        $school->email = Input::get('email');
        $school->pincode = Input::get('pin');
        $school->payment_method = Input::get('pm');
        $school->timeperiod = Input::get('daterange');
        $school->status = Input::get('trialperiod');
        $slugdata = str_slug(Input::get('name'));
        $slugCount = count($school->whereRaw("slug REGEXP '^{$slugdata}(-[0-9]*)?$'")->get());
        $slugdata = ($slugCount > 0) ? "{$slugdata}-{$slugCount}" : $slugdata;
        $school->slug = $slugdata;
			if ($request->hasFile('filename')) {
				$files = Input::file('filename');
				$name = time() . "_" . $files->getClientOriginalName();
				$image = $files->move(public_path() . '/theme/mainimage', $name);
				$school->image = $name;
			}
        $school->save();
        
        /*$clas = classes::all();//not working
        //dd($class);
        foreach($clas as $classes){
        $class = new class_to_school;
        $class->schoolid = $school->id;
        $class->classid =  $classes->id;
        $class->save();
        }*/
        
        //school register for staff
        $staffmember = new staffmember;
        $staffmember->userid = $user->id;
        $staffmember->schoolid = $school->id;
        $staffmember->save();
        
        //school register profile
        $profile = new teacherprofile;
        $profile->userid = $user->id;
        $profile->schoolid = $school->id;
        $profile->staffid = $staffmember->id;
        $profile->save();
        
        //roleassign
            // $role = $request->role;
            // $user = DB::table('users')->latest('id')->first();
            // $user->attachRole($role);
        //dd($profile);
        
        return back()->withMessage('School Add');
        
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     //active status
    public function show(Request $request)
    {
        $country = country::all();
        $state = state::all();
        $city = city::all();
        $school =DB::table('schools')

			->select('schools.*', 'countries.name as countryname', 'states.name as statename', 'cities.name as cityname')

			->join('countries', 'countries.id', '=', 'schools.countryid')
			->join('states', 'states.id', '=', 'schools.stateid')
			->join('cities', 'cities.id', '=', 'schools.cityid')
			->where('status',1)
			->get();
        return view('admin.school.index',compact('school','country','state','city'));
    }
    
    //inactive status
    public function showin(Request $request)
    {
        $country = country::all();
        $state = state::all();
        $city = city::all();
        $school =DB::table('schools')

			->select('schools.*', 'countries.name as countryname', 'states.name as statename', 'cities.name as cityname')

			->join('countries', 'countries.id', '=', 'schools.countryid')
			->join('states', 'states.id', '=', 'schools.stateid')
			->join('cities', 'cities.id', '=', 'schools.cityid')
			->where('status',0)
			->get();
        return view('admin.school.index',compact('school','country','state','city'));
    }
    
    public function ajaxChangeStatus( $id, Request $request )
    {
        $new_status = $request->get( 'status' );
        $school = school::where( 'id', $id )->first();
        $school->status = $new_status;
        $school->save();
    }
    
    public function statuscheck(Request $request,$id)
    {
        
        $school = school::where('id',$id)->first();
        $status=$school->status;
        
        return json_encode($status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($slug)
    // {
    //     $country = country::all();
    //     $state = state::all();
    //     $city = city::all();
    //     $school = school::where('slug',$slug)->first();
    //     return view('admin.school.edit',compact('school','country','state','city'));
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $rules = array(

			'name' => 'required',

			'mobile' => 'required',

			'email' => 'required|email',
			
			'filename' => 'image|mimes:jpg,png|max:3072',


		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$messages = $validator->messages();

			return Redirect::back()->withInput()->withErrors($validator);

		} elseif ($validator->passes()) {
        if(!empty(Input::get('country'))){
		        $country = Input::get('country');
		    }else{
		        $country = 1;
		    }
		    //state set
		    if(!empty(Input::get('state'))){
		        $state = Input::get('state');
		    }else{
		        $state = 10;
		    }
		    //city set
		    if(!empty(Input::get('city'))){
		        $city = Input::get('city');
		    }else{
		        $city = 121;
		    }
		
		$school = school::where('id',$id)->first();
		
        $school->name = Input::get('name');
        $school->principal_name = Input::get('principalname');
        $school->mobile = Input::get('mobile');
        $school->landline = Input::get('telephone');
        $school->email = Input::get('email');
        $school->timeperiod = Input::get('daterange');
        $school->pincode = Input::get('pin');
        $school->address = Input::get('address');
        $school->payment_method = Input::get('pm');
        $school->countryid = Input::get('country');
        $school->stateid = Input::get('state');
        $school->cityid = Input::get('city');
        $slugdata = str_slug(Input::get('name'));
        $slugCount = count($school->whereRaw("slug REGEXP '^{$slugdata}(-[0-9]*)?$'")->where('slug', $school->slug)->where('id', '<>', $school->id)->get());
        $slugdata = ($slugCount > 0) ? "{$slugdata}-{$slugCount}" : $slugdata;
        $school->slug = $slugdata;
		if ($request->hasFile('filename')) {
				$files = Input::file('filename');
				$name = time() . "_" . $files->getClientOriginalName();
				$image = $files->move(public_path() . '/theme/mainimage', $name);
				$school->image = $name;
		}
		//dd($school);
         $school->save();
         return redirect('school')->withMessage('School Update');
     }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        
        DB::table('schools')->where('id',$id)->delete();
        //delete from admissions 
        DB::table('admissions')->where('schoolid',$id)->delete();
        //delete from assign subjects 
        DB::table('assignsubjects')->where('schoolid',$id)->delete();
        //delete from busroutes 
        DB::table('busroutes')->where('schoolid',$id)->delete();
        //delete from class 
        DB::table('class_to_schools')->where('schoolid',$id)->delete();
        //delete from sections 
        DB::table('class_to_sections')->where('schoolid',$id)->delete();
        //delete from feesubmit 
        DB::table('feesubmit')->where('schoolid',$id)->delete();
        //delete from maintainfees 
        DB::table('maintainfees')->where('schoolid',$id)->delete();
        //delete from admissions 
        DB::table('route_drivers_datas')->where('schoolid',$id)->delete();
        //delete from sectionclasses 
        DB::table('sectionclasses')->where('schoolid',$id)->delete();
        //delete from staffmembers 
        DB::table('staffmembers')->where('schoolid',$id)->delete();
        //delete from studentprofiles 
        DB::table('studentprofiles')->where('schoolid',$id)->delete();
        //delete from teacher 
        DB::table('teacherprofiles')->where('schoolid',$id)->delete();
        






        
        return back()->withMessage("School Deleted");
    }
}
