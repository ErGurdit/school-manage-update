<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\school;
use App\staffmember;
use Auth;
use DB;
use App\Salaries;
use Notification;
use App\Notifications\SalaryReceive;
use App\User;
use Session;



class SalariesController extends Controller
{
    //
    public function index(Request $request)
    {
        $userid = Auth()->user()->id;
        $staffmember = staffmember::where('userid',$userid)->first();
                    $school =  school::where('id',$staffmember->schoolid)->first();
//dd($school->id);
          
         $staff = DB::table('staffmembers')
         ->select('staffmembers.*','users.*','users.name as uname')
         ->join('users','users.id','=','staffmembers.userid')
         ->where('schoolid',$staffmember->schoolid)->get();
         $salary = DB::table('salaries')->where('schoolid',$school->id)->get();
        
        //dd($staffmember->schoolid);
                    
      return view('principaltest.account.salaries',compact('staff','salary'));
        
    }
    public function getsalary(Request $request)
    {
        $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
            $school =  school::where('id',$staffmember->schoolid)->first();
            
            $salary = DB::table('staffmembers')
                          ->select('staffmembers.id as stfid','staffmembers.salary as sl')
                          ->where('staffmembers.userid',$request->staffm)
                          ->get();
                          
                         //dd($salary);
            return $salary;
    }
    public function upsta(Request $request,$id)
    {
        $sal = DB::table('salaries')->where('id',$id)->get();
                //dd($sal);

        DB::table('salaries')
        ->where('id',$request->salid)
        ->update(['status'=> 1]);
        //$sal->status = 1;
        //$sal->save();
                 return back()->withMessage('Payment Update');

    }
    public function store(Request $request)
    {
        //dd($request);
 $staffmember = staffmember::where('userid',Auth()->user()->id)->first();
 $school =  school::where('id',$staffmember->schoolid)->first();
 $salary  = staffmember::where('userid',$request->stafmid)->get()->pluck('salary');
 $uname  = DB::table('users')->select('users.name as uname')
            ->where('id',$request->stafmid)->get()->pluck('uname');

//check if exists

$salries  = DB::table('salaries')
          ->select('salaries.status as st','salaries.id as salid')
          ->where('salaries.userid',$request->stafmid)
          ->get();
          
          //dd(json_decode($salries));
      $cnt = count(json_decode($salries));
      
      //dd($cnt);
$usern = $uname[0];
 //dd($usern);
 $sal = json_decode($salary[0]);
 $date = date('Y-m-t');
 //dd($date);
 
 
        $current_date = date('Y-m-d H:i:s');
        $only_month = date("m",strtotime($current_date));
        //dd($only_dt);
 $dec = ($request->dec) == null ? 0 : $request->dec;
      $inc = ($request->inc) == null ? 0 : $request->inc;
      $bon = ($request->bon) == null ? 0 : $request->bon;       
 
    if($cnt == 0)
    {
        
      //$st =  ($salries[0]->st) == null ? 0 : ($salries[0]->st);
      //dd($dec);

       $salary = new Salaries;
       $salary->schoolid =$school->id;
       $salary->userid = $request->stafmid;
       $salary->account_id = $request->authid;
       $salary->deposit = $request->depo;
       $salary->status = 0;
       $salary->name = $usern;
       $salary->paid_amount = $sal;
       $salary->last_increment_amount = $inc;
       $salary->last_decrement_amount = $dec;
       $salary->bonus = $bon;
       $salary->last_increment_month = $only_month;
       $salary->current_month_salary= 'pending';
       $salary->last_month_salary= 'pending';
       $salary->salary = $sal;
       $salary->save();
       
       $message = "Salary send successfully";
       $data = 'Notification for Salary';

        Notification::send(User::find($request->stafmid),new SalaryReceive($data));

    }
    else
    {
        if(($salries[0]->st) == 1)
 {
 $paid  = 'paid';
 }
 if(($salries[0]->st) == 0)
 {
 $paid  = 'pending';
 }
        DB::table('salaries')
            ->where('userid', $request->stafmid)
            ->update(['schoolid' => $school->id,
            'name' => $usern,
            'paid_amount' => $sal,
            'account_id' => $request->authid,
            'deposit' => $request->depo,
            'last_increment_amount' => $inc,
            'last_decrement_amount' => $dec,
            'bonus' => $bon,
            'last_increment_month' => $only_month,
            'current_month_salary' => $paid,
            'last_month_salary' => $paid,
            'salary' => $sal
            ]);
                   $message = "Salary send successfully";
                   $data = 'Notification for Salary';

        Notification::send(User::find($request->stafmid),new SalaryReceive($data));

    }
       
       return back()->with('success',$message);
       
    }
}
