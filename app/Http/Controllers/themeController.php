<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\defaultsetting;
use Auth;
use App\slider;
use App\school;
use App\announcement;
use DB;

class themeController extends Controller
{
    public function index(){
        if(Auth::guest()){
        $id = 1;
        }else{
        $id = Auth::user()->id;
        }
        $defaultsetting = DB::table('defaultsettings')
        ->select('defaultsettings.id as did','defaultsettings.mobile','defaultsettings.aboutus','defaultsettings.email','sociallinks.fb','sociallinks.twitter','sociallinks.insta','sociallinks.gp','sociallinks.fbstatus','sociallinks.tweetstatus','sociallinks.instastatus','sociallinks.gpstatus')
        ->join('sociallinks','sociallinks.deafultid','=','defaultsettings.id')
        ->where('defaultsettings.userid',1)->first();
        if(Auth::guest()){
            $slider = slider::where('status',1)->get();
            $announce = announcement::where('userid',1)->get();
        }else{
              $slider = slider::where('status',1)->where('userid',Auth::user()->id)->get();
              $announce = announcement::where('userid',Auth::user()->id)->get();
        }
        
        $school = school::all();
        $schoolcount = Count($school);
        //dd($announce);
        
        return view('index',compact('defaultsetting','slider','schoolcount','school','announce'));
    }
}
