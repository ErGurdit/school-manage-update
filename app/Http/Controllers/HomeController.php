<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Entrust;
use App\classes;
use App\sectionclass;
use App\state;
use App\city;
use App\highclass;
use App\paymentmethod;
use App\country;
use App\subject;
use App\User;
use App\Role;
use App\staffmember;
use App\teacherprofile;
use App\school;
use DB;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//dd(Entrust::hasRole('principal'));
		if(Entrust::hasRole('superadmin'))
		{
			if(Auth::check())
			{
				return view('admin.index');
			}else
			{
				return view('auth.login');
			}
		}
		elseif(Entrust::hasRole('principal'))
		{
			
			if(Auth::check())
			{
				//dd(Auth()->user()->id);
				$staff = staffmember::where('userid',Auth::user()->id)->first();
				$userid = User::where('id',Auth::user()->id)->first();
				$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				$myclass = classes::all();
				$section = sectionclass::all();
				$state = state::all();
				$city = city::all();
				$stream = highclass::all();
				$payment = paymentmethod::all();
				$country = country::all();
				$subject = subject::all();
				$teacher = teacherprofile::where('staffid',$staff->id)->first();
    			$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
    			///dd($profile);
    			   	
    		
    			
				if(Auth()->user()->firstlogin == 0 || Auth()->user()->secondlogin == 0)
				{
					return view('principaltest.fee.principal',compact('myclass','section','state','city','stream','payment','country','subject','staff','userid','profile'));
				}
				else{
					   
					   $userid = Auth()->user()->id;
                       $schoolid = school::where('userid',$userid)->first();

                       $staffmember = staffmember::where('userid',$userid)->first();
                       $school =  school::where('id',$staffmember->schoolid)->first();
                                        $sch_i = $staffmember->schoolid;
                   
		return view('principaltest.index',compact('profile','notee','sch_i','count'));	
				}
				
			}else
			{
				return view('auth.login');
			} 
		}
		elseif(Entrust::hasRole('feesdepartment'))
		{
			//dd(Auth()->user()->firstlogin);
			if(Auth::check())
			{
				$staff = staffmember::where('userid',Auth::user()->id)->first();
				$userid = User::where('id',Auth::user()->id)->first();
				$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				$myclass = classes::all();
				$section = sectionclass::all();
				$state = state::all();
				$city = city::all();
				$stream = highclass::all();
				$payment = paymentmethod::all();
				$country = country::all();
				$subject = subject::all();
				$teacher = teacherprofile::where('staffid',$staff->id)->first();
    			$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
    			 
    			
				if(Auth()->user()->firstlogin == 0 || Auth()->user()->secondlogin == 0)
				{
					//dd($myclass);
					return view('principaltest.fee.index',compact('myclass','section','state','city','stream','payment','country','subject','staff','userid','profile'));
				}else{
					//dd($profile);
					
					return view('profile',compact('userid','staff','teacher','profile'));
				}
					
				}else
			{
				return view('auth.login');
			} 
		}
		elseif(Entrust::hasRole('accountant'))
		{
			if(Auth::check())
			{
				$staff = staffmember::where('userid',Auth::user()->id)->first();
				$userid = User::where('id',Auth::user()->id)->first();
				$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				$myclass = classes::all();
				$section = sectionclass::all();
				$state = state::all();
				$city = city::all();
				$stream = highclass::all();
				$payment = paymentmethod::all();
				$country = country::all();
				$subject = subject::all();
				$teacher = teacherprofile::where('staffid',$staff->id)->first();
    			$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				if(Auth()->user()->firstlogin == 0 || Auth()->user()->secondlogin == 0)
				{
					//dd($myclass);
					return view('principaltest.fee.index',compact('myclass','section','state','city','stream','payment','country','subject','staff','userid','profile'));
				}else{
					return view('profile',compact('userid','staff','teacher','profile'));
				}
				//$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				//return view('principaltest.account.index');
			}else
			{
				return view('auth.login');
			} 
		}
		elseif(Entrust::hasRole('teacher'))
		{
			if(Auth::check())
			{
				$staff = staffmember::where('userid',Auth::user()->id)->first();
				$userid = User::where('id',Auth::user()->id)->first();
				$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				$myclass = classes::all();
				$section = sectionclass::all();
				$state = state::all();
				$city = city::all();
				$stream = highclass::all();
				$payment = paymentmethod::all();
				$country = country::all();
				$subject = subject::all();
				$teacher = teacherprofile::where('staffid',$staff->id)->first();
    			$profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				if(Auth()->user()->firstlogin == 0 || Auth()->user()->secondlogin == 0)
				{
					//dd($myclass);
					return view('principaltest.fee.index',compact('myclass','section','state','city','stream','payment','country','subject','staff','userid','profile'));
				}else{
					return view('profile',compact('userid','staff','teacher','profile'));
				}
				// $profile = teacherprofile::where('userid',Auth()->user()->id)->first();
				// return view('principaltest.teacher.index',compact('profile'));
			}else
			{
				return view('auth.login');
			} 
		}
	}
}
