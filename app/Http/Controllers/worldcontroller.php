<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\country;
use App\state;
use App\city;
use Auth;
use DB;
use App\staffmember;
use App\school;
class worldcontroller extends Controller
{
    //getcountry
    public function getCountry(Request $request){
        $country = country::all();
        return json_encode($country);
    }
    //get state
    public function getStateList(Request $request) {

		$state = state::where('countryid', $request->country_id)->pluck('name', 'id');

		return json_encode($state);

		// dd($state);

	}

	//get city
    public function getCityList(Request $request) {

		$cities = city::where('stateid', $request->state_id)->pluck('name', 'id');

		return json_encode($cities);

		// return response()->json($cities);

	}
	
}
