<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;
use Auth;
use Entrust;
//use model
use App\Http\Requests;
use App\classes;
use App\admission;
use App\User;
use App\studentprofile;
use App\paymentmethod;
use App\school;
use App\staffmember;
use App\addcash_maintain;
use App\sectionclass;
use App\country;
use App\state;
use App\city;
use App\subject;
use App\maintainfee;
use Illuminate\Support\Str;
use App\expense;
use App\class_to_school;

class AccountController extends Controller
{
    //define contructor to call anywhere
//   public function __construct(){
//       $this->userid = Auth::user()->id;
//       $this->schoolid = school::where('userid',$this->userid)->first();
//       $this->staffmember = staffmember::where('userid',$this->userid)->first();
//     }
    
    public function index(Request $request){
        $userid = Auth()->user()->id;
        $schoolid = school::where('userid',$userid)->first();
        $staffmember = staffmember::where('userid',$userid)->first();
        //dd($staffmember);
        $allclasses = DB::table('class_to_schools')
                    ->select('class_to_schools.id as csid','class_to_schools.*','classes.id as cid','classes.*')
                    ->join('classes','classes.id','=','class_to_schools.classid')
                    ->where('class_to_schools.schoolid',$staffmember->schoolid)
                    ->get();
              
                    
        $allmaintain = DB::table('maintainfees')
                        ->select('maintainfees.id as mid','maintainfees.*')
                        ->where('schoolid',$staffmember->schoolid)
                        ->get();
        //dd($allmaintain);
        return view('principaltest.account.alldata',compact('allclasses','allmaintain','staffmember'));
    }
    
    //add new student
    public function accountstore(Request $request){
       {
           $userid = Auth::user()->id;
           $schoolid = school::where('userid',$userid)->first();
           $staffmember = staffmember::where('userid',$userid)->first();
           //class charge add
           $classcheck = maintainfee::where('classid',Input::get('classid'))->first();
           
           if(!empty($classcheck)){
             $maintainfee = maintainfee::where('classid',Input::get('classid'))->where('schoolid',Input::get('schoolid'))->first();
             $maintainfee->classcharge = trim(Input::get('classcharge'),' ');
             $maintainfee->bookcharge = trim(Input::get('bookcharge'),' ');
             $maintainfee->dresscharge = trim(Input::get('dresscharge'),' ');
             $maintainfee->transportcharge = trim(Input::get('transportcharge'),' ');
             $maintainfee->schoolfee = trim(Input::get('schoolfee'),' ');
             $maintainfee->admissionfee = trim(Input::get('admissionfee'),' ');
             $maintainfee->save();
             
           }else{
             $maintainfee = new maintainfee;
             $maintainfee->schoolid = $staffmember->schoolid;
             $maintainfee->classid = trim(Input::get('classid'),' ');
             $maintainfee->classcharge = trim(Input::get('classcharge'),' ');
             $maintainfee->bookcharge = trim(Input::get('bookcharge'),' ');
             $maintainfee->dresscharge = trim(Input::get('dresscharge'),' ');
             $maintainfee->transportcharge = trim(Input::get('transportcharge'),' ');
             $maintainfee->schoolfee = trim(Input::get('schoolfee'),' ');
             $maintainfee->admissionfee = trim(Input::get('admissionfee'),' ');
             $maintainfee->save();
           }
       }
        return redirect('account');
    }
    
    public function expenses(Request $request){
        $expense = DB::table('expenses')
                ->join('paymentmethods','paymentmethods.id','=','expenses.payment_type')
                ->get();
                
        $paymentmethod = paymentmethod::all();
        return view('principaltest.account.expense',compact('expense','paymentmethod'));
    }
    
    public function expensestore(Request $request){
        $expense = new expense;
        $expense->userid = Auth::user()->id;
        $expense->date = date('Y-m-d');
        $expense->description = Input::get('description');
        $expense->expense_amount = Input::get('amount');
        $expense->payment_type = Input::get('payment_type');
        $expense->save();
        
        return redirect('expenses');
    }
    
    public function expenseupdate(Request $request,$id){
        $expense = expense::where('id',$id)->first();
        $expense->date = date('Y-m-d');
        $expense->description = Input::get('description');
        $expense->expense_amount = Input::get('amount');
        $expense->payment_type = Input::get('payment_type');
        $expense->save();
        
        return redirect('expenses');
    }
}
