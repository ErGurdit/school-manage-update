<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\school;
use App\staffmember;


class LedgerController extends Controller
{
    //
    public function index(Request $request)
    {
        $userid = Auth()->user()->id;
        $schoolid = school::where('userid',$userid)->first();
        $staffmember = staffmember::where('userid',$userid)->first();
        $school =  school::where('id',$staffmember->schoolid)->first();
        $busroute = DB::table('busroutes')->where('schoolid',$school->id)->get();
        
        $applicable_dates = DB::select('SELECT DISTINCT CAST(am.created_at as date) as ad, CAST(ex.created_at as date) as ed FROM approve_moneys as am, expenses as ex');
        //dd($applicable_dates);
        
       /* $ledger_detail  = DB::table('approve_moneys')
                                ->select('expenses.expense_amount as em','approve_moneys.*', DB::raw('sum(approve_moneys.amount)as total') )
                                ->join('expenses','expenses.schoolid','=','approve_moneys.schoolid')
                                ->join('feesubmits','feesubmits.schoolid','=','approve_moneys.schoolid')
                                ->groupBy( 'approve_moneys.id' )
                                ->where('approve_moneys.status',1)
                                ->where('approve_moneys.schoolid',$school->id)
                                ->where('approve_moneys.updated_at','=', DB::raw('curdate()'))
                                ->get();*/
                                //dd($ledger_detail);
$expense_amount  = DB::table('expenses')
->select('expenses.expense_amount')
->where('expenses.schoolid',$school->id)->get()->pluck('expense_amount');

$total_hand_cash  = DB::table('approve_moneys')
->select(DB::raw('sum(approve_moneys.amount) as ttl'))
->where('approve_moneys.schoolid',$school->id)
->where('approve_moneys.status',1)
->where('approve_moneys.current_user_id',$userid)
->where('approve_moneys.updated_at','=', DB::raw('curdate()'))
->get()->pluck('ttl');

$total_hand_cash  = ($total_hand_cash == '') ? 0 : $total_hand_cash;

//dd($expense_amount[0]);
           $ledger_detail  = DB::table('feesubmits')
           ->select(/*'approve_moneys.*',*/'feesubmits.*',DB::raw('sum(feesubmits.amount) as tm'),'feesubmits.*'/*,DB::raw('sum(approve_moneys.amount)as total'),'expenses.expense_amount as em'*/)
           //->join('expenses','expenses.schoolid','=','feesubmits.schoolid')
           //->join('approve_moneys','approve_moneys.schoolid','=','feesubmits.schoolid')
           ->where('feesubmits.schoolid',$school->id)
           //->where('approve_moneys.status', 1)
           ->where('feesubmits.updated_at','=', DB::raw('curdate()'))
                      
           ->get();

         //$exp = (int)($ledger_detail[0]->tm) - (int)($expense_amount);
         //dd($exp);
                return view('principaltest.account.ledger',compact('ledger_detail','school','expense_amount','total_hand_cash'));

    }
    public function view(Request $request)
    {
       $student = DB::table('feesubmits')
			->select('feesubmits.*','users.name as name')
	    	->join('users', 'users.id', '=', 'feesubmits.userid')
            ->where('feesubmits.updated_at',DB::raw('curdate()'))
			->get();
			        return view('principaltest.account.ledgerview',compact('student'));

    }
}
