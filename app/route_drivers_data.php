<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class route_drivers_data extends Model
{
    //
    public function school() {
		return $this->belongs_to('school','id');
	}
	 public function user(){
        return $this->hasMany('App\User');
    }
    
    public function studentuser(){
        return $this->hasMany('App\User','id');
    }
}
