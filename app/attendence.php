<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attendence extends Model
{
    protected $fillable = [
        'userid', 'schoolid', 'teacherid','classid','sectionid','studentid','status',
    ];
}
