<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class school extends Model
{
    protected $fillable = [
        'userid','countryid', 'stateid','cityid','name','image','time','address','mobile','landline','status',
    ];
    public function offerschool() {
		return $this->hasMany('offer_to_school','schoolid');
	}
}
