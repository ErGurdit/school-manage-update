<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class city extends Model
{
    protected $fillable = [
        'stateid', 'name',
    ];
    public function offerschool() {
		return $this->hasMany('offer_to_school','ctyid');
	}
}
