<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class studentprofile extends Model
{
    public function user(){
        return $this->hasMany('App\User');
    }
    
    public function studentuser(){
        return $this->hasMany('App\User','id');
    }
    
    public function studentcountry(){
        return $this->hasMany('App\country','id');
    }
}
