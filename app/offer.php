<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offer extends Model
{
    protected $fillable = [
        'offer_name', 'first_date','last_date','offer_desc','schoolid','countryid','stateid','cityid','amount','slug'
    ];
}
