<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class homework extends Model
{
    protected $fillable = [
        'user_id', 'role_id', 'schoolid','teacherid','classid','sectionid','subjectid','title','description',
    ];
}
