<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class schoolgalleryimage extends Model
{
    protected $fillable = [
        'schoolid', 'satffid','public_id','version','signature','width','height','format','resource_type','created_at_image','tags','bytes','type','placeholder','url','secure_url','original_filename'
    ];
}
