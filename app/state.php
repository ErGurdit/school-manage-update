<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class state extends Model
{
    protected $fillable = [
        'countryid', 'name',
    ];
    public function offerschool() {
		return $this->hasMany('offer_to_school','stateid');
	}
}
