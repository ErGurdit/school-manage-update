<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assignsubject extends Model
{
    protected $fillable = [
        'schoolid','classid', 'subjectid',
    ];
}
