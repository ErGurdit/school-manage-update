<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offer_to_school extends Model
{
	
	protected $fillable = [
        'offerid', 'schoolid','countryid','stateid','cityid'
    ];
    public function school() {
		return $this->belongs_to('school','id');
	}
	public function country() {
		return $this->belongs_to('country','id');
	}
	 public function state() {
		return $this->belongs_to('state','id');
	}
	 public function city() {
		return $this->belongs_to('city','id');
	}
}
