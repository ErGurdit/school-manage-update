<?php

use Illuminate\Database\Seeder;
use App\highclass;

class HighClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $highclass = [
            [
                'classid' => '14',
                'highsubname' => 'Arts'
            ],
             
            [
                'classid' => '14',
                'highsubname' => 'Medical'
            ],
            [
                'classid' => '14',
                'highsubname' => 'Non-Medical'
            ],
             [
                'classid' => '14',
                'highsubname' => 'Commerce'
            ],
            [
                'classid' => '15',
                'highsubname' => 'Arts'
            ],
             
            [
                'classid' => '15',
                'highsubname' => 'Medical'
            ],
            [
                'classid' => '15',
                'highsubname' => 'Non-Medical'
            ],
             [
                'classid' => '15',
                'highsubname' => 'Commerce'
            ]
            
        ];
        
        foreach($highclass as $key => $value){
            highclass::create($value);
        }
        
    }
}
