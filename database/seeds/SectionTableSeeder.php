<?php

use Illuminate\Database\Seeder;
use App\sectionclass;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //sections seeder
        $section = [
            [
                'section_name' => 'section1',
                'section_display_name' => 'A'
            ],
             [
                'section_name' => 'section2',
                'section_display_name' => 'B'
            ],
             [
                'section_name' => 'section3',
                'section_display_name' => 'C'
            ],
             [
                'section_name' => 'section4',
                'section_display_name' => 'D'
            ],
             [
                'section_name' => 'section5',
                'section_display_name' => 'E'
            ],
             [
                'section_name' => 'section6',
                'section_display_name' => 'F'
            ]
        ];
        
        foreach($section as $key => $value){
            sectionclass::create($value);
        }
        
    }
}
