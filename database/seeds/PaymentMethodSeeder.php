<?php

use Illuminate\Database\Seeder;
use App\paymentmethod;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment = [
            [
                'payment_name' => 'cash',
                'payment_display_name' =>'Cash'
            ],
            [
                'payment_name' => 'card',
                'payment_display_name' =>'Card'
            ],
            [
                'payment_name' => 'net-banking',
                'payment_display_name' =>'Net Banking'
            ],
        ];
        
        foreach($payment as $key => $value){
            paymentmethod::create($value);
        }
    }
}
