<?php

use Illuminate\Database\Seeder;
use App\country;
use App\state;
use App\city;

class WorldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //country seeder
        $country = [
            [
                'name' => 'India',
                'iso' => 'IND',
                'flag' =>'National Flag ',
                'countrycode'=>'IND'
            ]
        ];
        
        foreach($country as $key => $value){
            country::create($value);
        }
        
        //State seeder
        $state = [
            [
                'countryid' => '1',
                'name' => 'Andaman and Nicobar Islands',
            ],
             [
                'countryid' => '1',
                'name' => '	Andhra Pradesh',
            ],
             [
                'countryid' => '1',
                'name' => '	Arunachal Pradesh',
            ],
             [
                'countryid' => '1',
                'name' => 'Assam',
            ],
             [
                'countryid' => '1',
                'name' => 'Bihar',
            ],
             [
                'countryid' => '1',
                'name' => 'Chandigarh ',
            ],
             [
                'countryid' => '1',
                'name' => 'Chhattisgarh',
            ],
             [
                'countryid' => '1',
                'name' => 'Dadra and Nagar Haveli',
            ],
             [
                'countryid' => '1',
                'name' => 'Daman and Diu',
            ],
             [
                'countryid' => '1',
                'name' => 'Delhi(NCR)',
            ],
             [
                'countryid' => '1',
                'name' => '	Goa',
            ],
             [
                'countryid' => '1',
                'name' => 'Gujarat',
            ],
             [
                'countryid' => '1',
                'name' => 'Haryana',
            ],
             [
                'countryid' => '1',
                'name' => 'Himachal Pradesh',
            ],
             [
                'countryid' => '1',
                'name' => 'Jammu and Kashmir',
            ],
             [
                'countryid' => '1',
                'name' => 'Jharkhand',
            ],
             [
                'countryid' => '1',
                'name' => 'Karnataka',
            ],
             [
                'countryid' => '1',
                'name' => 'Kerala',
            ],
             [
                'countryid' => '1',
                'name' => 'Lakshadweep ',
            ],
             [
                'countryid' => '1',
                'name' => 'Madhya Pradesh',
            ],
             [
                'countryid' => '1',
                'name' => 'Maharashtra',
            ],
             [
                'countryid' => '1',
                'name' => 'Manipur',
            ], [
                'countryid' => '1',
                'name' => 'Meghalaya',
            ],
             [
                'countryid' => '1',
                'name' => 'Mizoram',
            ], [
                'countryid' => '1',
                'name' => 'Nagaland',
            ],
             [
                'countryid' => '1',
                'name' => 'Odisha',
            ],
             [
                'countryid' => '1',
                'name' => 'Puducherry',
            ],
             [
                'countryid' => '1',
                'name' => 'Punjab',
            ],
             [
                'countryid' => '1',
                'name' => 'Rajasthan',
            ],
             [
                'countryid' => '1',
                'name' => 'Sikkim',
            ],
            [
                'countryid' => '1',
                'name' => 'Tamil Nadu',
            ],
            
            [
                'countryid' => '1',
                'name' => 'Telangana',
            ],
            
            [
                'countryid' => '1',
                'name' => 'Tripura',
            ],
            
            [
                'countryid' => '1',
                'name' => 'Uttar Pradesh',
            ],
            
            [
                'countryid' => '1',
                'name' => 'Uttarakhand',
            ],
            
            [
                'countryid' => '1',
                'name' => 'West Bengal',
            ]
            
        ];
        
        foreach($state as $key => $value){
            state::create($value);
        }
        
        //State seeder
        $city = [
            [
                'stateid' => '1',
                'name' => 'Port Blair',
            ],
            [
                'stateid' => '1',
                'name' => 'Bombooflat',
            ],
            [
                'stateid' => '1',
                'name' => 'Kakana, Car Nicobar',
            ],
            [
                'stateid' => '1',
                'name' => 'Tamaloo',
            ],
            [
                'stateid' => '1',
                'name' => 'Tushnabad ',
            ],
             [
                'stateid' => '1',
                'name' => 'Chukmachi',
            ],
             [
                'stateid' => '1',
                'name' => 'Diglipur ',
            ],
             [
                'stateid' => '1',
                'name' => 'Garacharma ',
            ],
             [
                'stateid' => '1',
                'name' => 'Malacca, Nancowry ',
            ],
             [
                'stateid' => '1',
                'name' => 'Minyuk ',
            ],
             [
                'stateid' => '1',
                'name' => 'Kadamtala ',
            ],
             [
                'stateid' => '1',
                'name' => 'Mayabunder ',
            ],
             [
                'stateid' => '1',
                'name' => 'Malacca, Car Nicobar ',
            ],
             [
                'stateid' => '1',
                'name' => 'Perka, Car Nicobar ',
            ],
             [
                'stateid' => '1',
                'name' => 'Henhoaha ',
            ],
             [
                'stateid' => '1',
                'name' => 'Kimois ',
            ],
             [
                'stateid' => '1',
                'name' => 'Ramzoo ',
            ],
             [
                'stateid' => '1',
                'name' => 'Rangat ',
            ],
             [
                'stateid' => '1',
                'name' => 'Manglutan ',
            ],
            [
                'stateid' => '1',
                'name' => 'Arong, Car Nicobar ',
            ],
            [
                'stateid' => '1',
                'name' => 'Govinda Nagar ',
            ],
            [
                'stateid' => '1',
                'name' => 'Luxi, Nancowry ',
            ],
            [
                'stateid' => '2',
                'name' => 'Vijayawada',
            ],
            [
                'stateid' => '2',
                'name' => 'Visakhapatnam',
            ],
            [
                'stateid' => '2',
                'name' => 'Guntur',
            ],
            [
                'stateid' => '2',
                'name' => 'Tirupati',
            ],
            [
                'stateid' => '2',
                'name' => 'Kakinada',
            ],
            [
                'stateid' => '2',
                'name' => 'Amaravathi',
            ],
            [
                'stateid' => '2',
                'name' => 'Rajahmundry',
            ],
            [
                'stateid' => '2',
                'name' => 'Anantapur',
            ],
             [
                'stateid' => '2',
                'name' => 'kurnool',
            ],
             [
                'stateid' => '2',
                'name' => 'Ongole',
            ],
             [
                'stateid' => '2',
                'name' => 'Kadapa',
            ],
             [
                'stateid' => '2',
                'name' => 'Nellore',
            ],
             [
                'stateid' => '2',
                'name' => 'Eluru',
            ],
             [
                'stateid' => '2',
                'name' => 'Machilipatnam',
            ],
             [
                'stateid' => '2',
                'name' => 'Adoni',
            ],
             [
                'stateid' => '2',
                'name' => 'Nandyal',
            ],
             [
                'stateid' => '2',
                'name' => 'Tenali',
            ],
             [
                'stateid' => '2',
                'name' => 'Dharmavaram',
            ], 
            [
                'stateid' => '2',
                'name' => 'Bhimavaram',
            ],
            [
                'stateid' => '2',
                'name' => 'Proddatur',
            ],
            [
                'stateid' => '2',
                'name' => 'Hindupur',
            ],
            [
                'stateid' => '2',
                'name' => 'Madanapalle',
            ],
            [
                'stateid' => '2',
                'name' => 'Narasaraopet',
            ],
            [
                'stateid' => '2',
                'name' => 'Gudivada',
            ],
            [
                'stateid' => '2',
                'name' => 'Chilakaluripet',
            ],
            [
                'stateid' => '2',
                'name' => 'Guntakal',
            ],
             [
                'stateid' => '2',
                'name' => 'Kanigiri',
            ],
             [
                'stateid' => '2',
                'name' => 'Vizianagaram',
            ],
             [
                'stateid' => '2',
                'name' => 'Tadipatri',
            ],
            [
                'stateid' => '3',
                'name' => 'Tawang',
            ],
            [
                'stateid' => '3',
                'name' => 'Itanagar',
            ],
            [
                'stateid' => '3',
                'name' => 'Ziro',
            ],
            [
                'stateid' => '3',
                'name' => 'Bomdila',
            ],
            [
                'stateid' => '3',
                'name' => 'Pasighat',
            ],
            [
                'stateid' => '3',
                'name' => 'Bhalukpong',
            ],
            [
                'stateid' => '4',
                'name' => 'Guwahati',
            ],
            [
                'stateid' => '4',
                'name' => 'Dibrugarh',
            ],
            [
                'stateid' => '4',
                'name' => 'Tezpur',
            ],
            [
                'stateid' => '4',
                'name' => 'Jorhat',
            ],
            [
                'stateid' => '4',
                'name' => 'Silchar',
            ],
            [
                'stateid' => '4',
                'name' => 'Tinsukia',
            ],
            [
                'stateid' => '4',
                'name' => 'Haflong',
            ],
            [
                'stateid' => '4',
                'name' => 'Sivasagar',
            ],
            [
                'stateid' => '4',
                'name' => 'Nagaon',
            ],
            [
                'stateid' => '4',
                'name' => 'Golaghat',
            ],
             [
                'stateid' => '5',
                'name' => 'Patna',
            ],
            [
                'stateid' => '5',
                'name' => 'Gaya',
            ],
            [
                'stateid' => '5',
                'name' => 'Muzaffarpur ',
            ],
            [
                'stateid' => '5',
                'name' => 'Darbhanga',
            ],
            [
                'stateid' => '5',
                'name' => 'Motihari',
            ],
            [
                'stateid' => '5',
                'name' => 'Chhapra',
            ],
            
            [
                'stateid' => '5',
                'name' => 'Rajgir ',
            ],
            [
                'stateid' => '5',
                'name' => 'Bodh Gaya',
            ],
            [
                'stateid' => '5',
                'name' => 'Hajipur',
            ],
            
            [
                'stateid' => '5',
                'name' => 'Bettiah ',
            ],
            
            [
                'stateid' => '5',
                'name' => 'Madhubani',
            ],
            [
                'stateid' => '5',
                'name' => 'Arrah',
            ],
            [
                'stateid' => '5',
                'name' => 'Sasaram',
            ],
            [
                'stateid' => '5',
                'name' => 'Dehri',
            ],
            [
                'stateid' => '5',
                'name' => 'Bagaha',
            ],
            [
                'stateid' => '5',
                'name' => 'Jamalpur',
            ],
            [
                'stateid' => '5',
                'name' => 'Aurangabad',
            ],
            [
                'stateid' => '5',
                'name' => 'Munger',
            ],
            [
                'stateid' => '5',
                'name' => 'Jehanabad',
            ],
            [
                'stateid' => '5',
                'name' => 'Katihar',
            ],
            [
                'stateid' => '5',
                'name' => 'Sitamarhi',
            ],[
                'stateid' => '5',
                'name' => 'Hilsa',
            ],
            [
                'stateid' => '5',
                'name' => 'Sonepur ',
            ],
            [
                'stateid' => '5',
                'name' => 'Bhabua ',
            ],
            [
                'stateid' => '5',
                'name' => 'Bakhtiarpur ',
            ],
            [
                'stateid' => '5',
                'name' => 'Barauni  ',
            ],
            [
                'stateid' => '5',
                'name' => 'Buxar ',
            ],
            [
                'stateid' => '5',
                'name' => 'Mithila ',
            ],
            [
                'stateid' => '5',
                'name' => 'Siwan ',
            ],
            [
                'stateid' => '5',
                'name' => 'Raxaul ',
            ],
            [
                'stateid' => '5',
                'name' => 'Saharsa ',
            ],
            [
                'stateid' => '5',
                'name' => 'Daudnagar ',
            ],
            [
                'stateid' => '5',
                'name' => 'Kishanganj ',
            ],
            [
                'stateid' => '5',
                'name' => 'Nalanda',
            ],
            [
                'stateid' => '5',
                'name' => 'Muzaffarpur',
            ],
            [
                'stateid' => '5',
                'name' => 'Bhagalpur',
            ],
            [
                'stateid' => '5',
                'name' => 'Darbhanga',
            ],
            [
                'stateid' => '5',
                'name' => 'Champaran',
            ],
            [
                'stateid' => '5',
                'name' => 'Samastipur',
            ],
            [
                'stateid' => '6',
                'name' => 'Chandigarh',
            ],
            [
                'stateid' => '7',
                'name' => 'Raipur',
            ],
            [
                'stateid' => '7',
                'name' => 'Durg',
            ],
            [
                'stateid' => '7',
                'name' => 'Bilaspur',
            ],
            [
                'stateid' => '7',
                'name' => 'Bhilai',
            ],
            [
                'stateid' => '7',
                'name' => 'Jagdalpur',
            ],
            [
                'stateid' => '7',
                'name' => 'Korba',
            ],
            [
                'stateid' => '7',
                'name' => 'Raigarh',
            ],
            [
                'stateid' => '7',
                'name' => 'Champa',
            ],
            [
                'stateid' => '7',
                'name' => 'Kondagaon',
            ],
            [
                'stateid' => '7',
                'name' => 'Janjgir',
            ],
            [
                'stateid' => '7',
                'name' => 'Baikunthpur',
            ],
            [
                'stateid' => '7',
                'name' => 'Dongargarh',
            ],
            [
                'stateid' => '7',
                'name' => 'Rajnandgaon',
            ],
            [
                'stateid' => '10',
                'name' => 'New Delhi',
            ],
            [
                'stateid' => '11',
                'name' => 'Panaji',
            ],
            [
                'stateid' => '11',
                'name' => 'Margao',
            ],
            [
                'stateid' => '11',
                'name' => 'Vasco da Gama',
            ],
            [
                'stateid' => '11',
                'name' => 'Calangut',
            ],
            [
                'stateid' => '11',
                'name' => 'Anjuna',
            ],
            [
                'stateid' => '11',
                'name' => 'New Delhi',
            ],
            [
                'stateid' => '11',
                'name' => 'Baga',
            ],
            [
                'stateid' => '11',
                'name' => 'Mapusa ',
            ],
            [
                'stateid' => '11',
                'name' => 'Candolim',
            ],
            [
                'stateid' => '11',
                'name' => 'Colva ',
            ],
            [
                'stateid' => '11',
                'name' => 'Ponda',
            ],
            [
                'stateid' => '11',
                'name' => 'Mobor',
            ],
            [
                'stateid' => '12',
                'name' => 'Gandhinagar ',
            ],
            [
                'stateid' => '12',
                'name' => 'Surat ',
            ],
            [
                'stateid' => '12',
                'name' => 'Vadodara ',
            ],
            [
                'stateid' => '12',
                'name' => 'Rajkot',
            ],
            [
                'stateid' => '12',
                'name' => 'Jamnagar',
            ],
            [
                'stateid' => '12',
                'name' => 'Bhuj',
            ],
            [
                'stateid' => '12',
                'name' => 'Anand',
            ],
            [
                'stateid' => '12',
                'name' => 'Junagadh',
            ],
            [
                'stateid' => '12',
                'name' => 'Bhavnagar ',
            ],
            [
                'stateid' => '12',
                'name' => 'Patan',
            ],
            [
                'stateid' => '12',
                'name' => 'Vapi',
            ],
            [
                'stateid' => '12',
                'name' => 'Bharuch ',
            ],
            [
                'stateid' => '12',
                'name' => 'Verav ',
            ],
            [
                'stateid' => '12',
                'name' => 'Gandhidham ',
            ],
            [
                'stateid' => '12',
                'name' => 'Nadiad ',
            ],
            [
                'stateid' => '12',
                'name' => 'Porbandar ',
            ],
            [
                'stateid' => '12',
                'name' => 'Navsari ',
            ],
            [
                'stateid' => '12',
                'name' => 'Ankleshwar  ',
            ],
            [
                'stateid' => '12',
                'name' => 'Palitana  ',
            ],
            [
                'stateid' => '12',
                'name' => 'Saputara ',
            ],
            [
                'stateid' => '12',
                'name' => 'Mehsana ',
            ],
            [
                'stateid' => '12',
                'name' => 'Dwarka ',
            ],
            [
                'stateid' => '12',
                'name' => 'Gondal ',
            ],
            [
                'stateid' => '12',
                'name' => 'Ahmedabad ',
            ],
            
            [
                'stateid' => '12',
                'name' => 'Modhera ',
            ],
            
            [
                'stateid' => '12',
                'name' => 'Ambaji ',
            ],
            
            [
                'stateid' => '12',
                'name' => 'Vadodara  ',
            ],
            
            [
                'stateid' => '12',
                'name' => 'Dholera ',
            ],
            
            [
                'stateid' => '12',
                'name' => 'Dahod ',
            ],
            [
                'stateid' => '13',
                'name' => 'Gurgaon ',
            ],
            [
                'stateid' => '13',
                'name' => 'Panchkula ',
            ],
            [
                'stateid' => '13',
                'name' => 'Rewari ',
            ],
            [
                'stateid' => '13',
                'name' => 'Faridabad ',
            ],
            [
                'stateid' => '13',
                'name' => 'Rohtak',
            ],
            [
                'stateid' => '13',
                'name' => 'Ambala ',
            ],
            [
                'stateid' => '13',
                'name' => 'Karnal ',
            ],
            [
                'stateid' => '13',
                'name' => 'Hisar',
            ],
            [
                'stateid' => '13',
                'name' => 'Palwal',
            ],
            [
                'stateid' => '13',
                'name' => 'Panipat',
            ],
            [
                'stateid' => '13',
                'name' => 'Yamunanagar',
            ],
            [
                'stateid' => '13',
                'name' => 'Sonipat',
            ],
            [
                'stateid' => '13',
                'name' => 'Sirsa',
            ],
            [
                'stateid' => '13',
                'name' => 'Bahadurgarh',
            ],
            [
                'stateid' => '13',
                'name' => 'Kaithal',
            ],
            [
                'stateid' => '13',
                'name' => 'Narnaul',
            ],
            [
                'stateid' => '13',
                'name' => 'Jind',
            ],
            [
                'stateid' => '13',
                'name' => 'Kurukshetra',
            ],
            [
                'stateid' => '13',
                'name' => 'Jhajjar',
            ],
            [
                'stateid' => '13',
                'name' => 'Jagadhri',
            ],
            [
                'stateid' => '13',
                'name' => 'Fatehabad',
            ],
            [
                'stateid' => '13',
                'name' => 'Kalka',
            ],
            [
                'stateid' => '14',
                'name' => 'Shimla',
            ],
            [
                'stateid' => '14',
                'name' => 'Dharamshala',
            ],
            [
                'stateid' => '14',
                'name' => 'Mandi',
            ],
            [
                'stateid' => '14',
                'name' => 'Manali',
            ],
            [
                'stateid' => '14',
                'name' => 'Manali',
            ],
            [
                'stateid' => '14',
                'name' => 'Nahan',
            ],
            [
                'stateid' => '14',
                'name' => 'Solan',
            ],
            [
                'stateid' => '14',
                'name' => 'Kullu',
            ],
            [
                'stateid' => '14',
                'name' => 'Dalhousie',
            ],
            [
                'stateid' => '14',
                'name' => 'Palampur',
            ],
            [
                'stateid' => '14',
                'name' => 'Kasauli',
            ],
            [
                'stateid' => '14',
                'name' => 'Rampur',
            ],
            [
                'stateid' => '14',
                'name' => 'Parwanoo',
            ],
            [
                'stateid' => '14',
                'name' => 'Kangra ',
            ],
            [
                'stateid' => '14',
                'name' => 'Nalagarh',
            ],
            [
                'stateid' => '14',
                'name' => 'Naina',
            ],
            [
                'stateid' => '14',
                'name' => 'Khajjiar ',
            ],
            [
                'stateid' => '14',
                'name' => 'Rohru',
            ],
            [
                'stateid' => '14',
                'name' => 'Hamirpur',
            ],
            [
                'stateid' => '15',
                'name' => 'Jammu',
            ],
            [
                'stateid' => '15',
                'name' => 'Ramban',
            ],
            [
                'stateid' => '15',
                'name' => 'Samba',
            ],
            [
                'stateid' => '15',
                'name' => 'Gho Manhasan',
            ],
            [
                'stateid' => '16',
                'name' => 'Jamshedpur',
            ],
            [
                'stateid' => '16',
                'name' => 'Jamshedpur',
            ],
            [
                'stateid' => '16',
                'name' => 'Jamshedpur',
            ],
            [
                'stateid' => '16',
                'name' => 'Jamshedpur',
            ],
            [
                'stateid' => '16',
                'name' => 'Gaya',
            ],
            [
                'stateid' => '17',
                'name' => 'Bangalore',
            ],
            [
                'stateid' => '17',
                'name' => 'Mangalore',
            ],
            [
                'stateid' => '17',
                'name' => 'Belgaum',
            ],
            [
                'stateid' => '17',
                'name' => 'Hubli',
            ],
            [
                'stateid' => '17',
                'name' => 'Gulbarga',
            ],
            [
                'stateid' => '17',
                'name' => 'Davangere',
            ],
            [
                'stateid' => '17',
                'name' => 'Mysore ',
            ],
            [
                'stateid' => '17',
                'name' => 'Bijapur',
            ],
            [
                'stateid' => '17',
                'name' => 'Tumkur',
            ],
            [
                'stateid' => '17',
                'name' => 'Bellary',
            ],
            [
                'stateid' => '17',
                'name' => 'Badami ',
            ],
            [
                'stateid' => '17',
                'name' => 'Shimoga ',
            ],
            [
                'stateid' => '17',
                'name' => 'Doddaballapura',
            ],
            [
                'stateid' => '17',
                'name' => 'Madikeri',
            ],
            [
                'stateid' => '17',
                'name' => 'Sringeri',
            ],
            [
                'stateid' => '17',
                'name' => 'Sagara',
            ],
            [
                'stateid' => '18',
                'name' => 'Kochi',
            ],
            [
                'stateid' => '18',
                'name' => 'Trivandrum ',
            ],
            [
                'stateid' => '18',
                'name' => 'Kozhikode',
            ],
            [
                'stateid' => '18',
                'name' => 'Kottayam',
            ],
            [
                'stateid' => '18',
                'name' => 'Kannur',
            ],
            [
                'stateid' => '18',
                'name' => 'Ernakulam',
            ],
            [
                'stateid' => '18',
                'name' => 'Kumarakom',
            ],
            [
                'stateid' => '18',
                'name' => 'Tirur',
            ],
            [
                'stateid' => '18',
                'name' => 'Punalur',
            ],
            [
                'stateid' => '18',
                'name' => 'Thiruvalla',
            ],
            [
                'stateid' => '18',
                'name' => 'Kasaragod',
            ],
            [
                'stateid' => '18',
                'name' => 'Pathanamthitta',
            ],
            [
                'stateid' => '18',
                'name' => 'Malappuram',
            ],
            [
                'stateid' => '18',
                'name' => 'Changanassery',
            ],
            [
                'stateid' => '19',
                'name' => 'Lakshadweep ',
            ],
            [
                'stateid' => '20',
                'name' => 'Indore ',
            ],
            [
                'stateid' => '20',
                'name' => 'Gwalior ',
            ],
            [
                'stateid' => '20',
                'name' => 'Bhopal ',
            ],
            [
                'stateid' => '20',
                'name' => 'Satna ',
            ],
            [
                'stateid' => '20',
                'name' => 'Ujjain ',
            ],
            [
                'stateid' => '20',
                'name' => 'Neemuch',
            ],
            [
                'stateid' => '20',
                'name' => 'Rewa',
            ],
            [
                'stateid' => '20',
                'name' => 'Jabalpur',
            ],
            [
                'stateid' => '20',
                'name' => 'Dewas',
            ],
             [
                'stateid' => '20',
                'name' => 'Sagar',
            ],
             [
                'stateid' => '20',
                'name' => 'Shivpuri',
            ],
             [
                'stateid' => '20',
                'name' => 'Pachmarhi',
            ],
             [
                'stateid' => '20',
                'name' => 'Katni',
            ],
            [
                'stateid' => '20',
                'name' => 'Chhindwara',
            ],
            [
                'stateid' => '20',
                'name' => 'Khandwa',
            ],
            [
                'stateid' => '20',
                'name' => 'Bhedaghat',
            ],[
                'stateid' => '20',
                'name' => 'Ratlam',
            ],
            [
                'stateid' => '20',
                'name' => 'Damoh',
            ],
            [
                'stateid' => '20',
                'name' => 'Itarsi',
            ],
            [
                'stateid' => '20',
                'name' => 'Vidisha',
            ],
            [
                'stateid' => '20',
                'name' => 'Betul',
            ],
            [
                'stateid' => '20',
                'name' => 'Hoshangabad',
            ],
            [
                'stateid' => '20',
                'name' => 'Bhojpur',
            ],
            [
                'stateid' => '20',
                'name' => 'Chhatarpur',
            ],
            [
                'stateid' => '20',
                'name' => 'Mumbai',
            ],
            [
                'stateid' => '21',
                'name' => 'Pune',
            ],
            [
                'stateid' => '21',
                'name' => 'Nagpur',
            ],
            [
                'stateid' => '21',
                'name' => 'Aurangabad',
            ],
            [
                'stateid' => '21',
                'name' => 'Nashik',
            ],
            [
                'stateid' => '21',
                'name' => 'Jalgaon',
            ],
            [
                'stateid' => '21',
                'name' => 'Solapur',
            ],
            [
                'stateid' => '21',
                'name' => 'Kolhapur',
            ],
            [
                'stateid' => '21',
                'name' => 'Shirdi',
            ],
            [
                'stateid' => '21',
                'name' => 'Nanded',
            ],
            [
                'stateid' => '22',
                'name' => 'Bishnupur',
            ],
            [
                'stateid' => '22',
                'name' => 'Moirang',
            ],
            [
                'stateid' => '22',
                'name' => 'Jiribam',
            ],
            [
                'stateid' => '22',
                'name' => 'Thoubal',
            ],
            [
                'stateid' => '22',
                'name' => 'Shirui',
            ],
            [
                'stateid' => '22',
                'name' => 'Sekta',
            ],
            [
                'stateid' => '22',
                'name' => 'Hiyangthang',
            ],
            [
                'stateid' => '22',
                'name' => 'Sugnu',
            ],
            [
                'stateid' => '23',
                'name' => 'Shillong',
            ],
            [
                'stateid' => '23',
                'name' => 'Cherrapunji',
            ],
            [
                'stateid' => '23',
                'name' => 'Tura',
            ],
            [
                'stateid' => '23',
                'name' => 'Guwahati',
            ],
            [
                'stateid' => '23',
                'name' => 'Nongpoh',
            ],
            [
                'stateid' => '23',
                'name' => 'Baghmara',
            ],
            [
                'stateid' => '23',
                'name' => 'Mawsynram',
            ],
            [
                'stateid' => '23',
                'name' => 'Nongstoin',
            ],
            [
                'stateid' => '23',
                'name' => 'Mawlynnong',
            ],
            [
                'stateid' => '23',
                'name' => 'Williamnagar',
            ],
            [
                'stateid' => '23',
                'name' => 'Jorabat',
            ],
            [
                'stateid' => '23',
                'name' => 'Mankachar',
            ],
            [
                'stateid' => '23',
                'name' => 'Nongriat',
            ],
            [
                'stateid' => '23',
                'name' => 'Mawlai',
            ],
            [
                'stateid' => '24',
                'name' => 'Aizawl',
            ],
            [
                'stateid' => '24',
                'name' => 'Lunglei',
            ],
            [
                'stateid' => '24',
                'name' => 'Kolasib',
            ],
            [
                'stateid' => '24',
                'name' => 'Farkawn',
            ],
            [
                'stateid' => '24',
                'name' => 'Kawnpui',
            ],
            [
                'stateid' => '24',
                'name' => 'Vairengte',
            ],
            [
                'stateid' => '24',
                'name' => 'Thenzawl',
            ],
            [
                'stateid' => '25',
                'name' => 'Nagaland',
            ],
            [
                'stateid' => '26',
                'name' => 'Bhubaneswar',
            ],
            [
                'stateid' => '26',
                'name' => 'Cuttack',
            ],
            [
                'stateid' => '26',
                'name' => 'Puri',
            ],
            [
                'stateid' => '26',
                'name' => 'Sambalpur',
            ],
            [
                'stateid' => '26',
                'name' => 'Rourkela',
            ],
            [
                'stateid' => '26',
                'name' => 'Berhampur',
            ],
            [
                'stateid' => '26',
                'name' => 'Balasore',
            ],
            [
                'stateid' => '26',
                'name' => 'Jeypore',
            ],
            [
                'stateid' => '26',
                'name' => 'Barbil',
            ],
            [
                'stateid' => '26',
                'name' => 'Bhadrak',
            ],
            [
                'stateid' => '26',
                'name' => 'Bhawanipatna',
            ],
            [
                'stateid' => '26',
                'name' => 'Chhatrapur',
            ],
            [
                'stateid' => '26',
                'name' => 'Jatani',
            ],
            [
                'stateid' => '27',
                'name' => 'Puducherry',
            ],
            [
                'stateid' => '28',
                'name' => 'Amritsar',
            ],
            [
                'stateid' => '28',
                'name' => 'Ludhiana',
            ],
            [
                'stateid' => '28',
                'name' => 'Patiala',
            ],
            [
                'stateid' => '28',
                'name' => 'Jalandhar',
            ],
            [
                'stateid' => '28',
                'name' => 'Bathinda',
            ],
            [
                'stateid' => '28',
                'name' => 'Mohali',
            ],
            [
                'stateid' => '28',
                'name' => 'Pathankot',
            ],
            [
                'stateid' => '28',
                'name' => 'Hoshiarpur',
            ],
            [
                'stateid' => '28',
                'name' => 'Firozpur',
            ],
            [
                'stateid' => '28',
                'name' => 'Kapurthala',
            ],
            [
                'stateid' => '28',
                'name' => 'Batala',
            ],
            [
                'stateid' => '28',
                'name' => 'Phagwara',
            ],
            [
                'stateid' => '29',
                'name' => 'Jaipur',
            ],
            [
                'stateid' => '29',
                'name' => 'Jodhpur',
            ],
            [
                'stateid' => '29',
                'name' => 'Udaipur',
            ],
            [
                'stateid' => '29',
                'name' => 'Ajmer',
            ],
            [
                'stateid' => '29',
                'name' => 'Bikaner',
            ],
            [
                'stateid' => '29',
                'name' => 'Kota',
            ],
            [
                'stateid' => '29',
                'name' => 'Jaisalmer',
            ],
            [
                'stateid' => '29',
                'name' => 'Pushkar',
            ],
            [
                'stateid' => '29',
                'name' => 'Alwar',
            ],
            [
                'stateid' => '30',
                'name' => 'Sikkim',
            ],
            [
                'stateid' => '31',
                'name' => 'Chennai',
            ],
            [
                'stateid' => '31',
                'name' => 'Coimbatore',
            ],
            [
                'stateid' => '31',
                'name' => 'Madurai',
            ],
            [
                'stateid' => '31',
                'name' => 'Trichy',
            ],
            [
                'stateid' => '31',
                'name' => 'Salem',
            ],
            [
                'stateid' => '31',
                'name' => 'Thanjavur',
            ],
            [
                'stateid' => '31',
                'name' => 'Tirunelveli',
            ],
            [
                'stateid' => '31',
                'name' => 'Vellore',
            ],
            [
                'stateid' => '31',
                'name' => 'Ooty',
            ],
            [
                'stateid' => '31',
                'name' => 'Erode',
            ],
            [
                'stateid' => '31',
                'name' => 'Tirupur',
            ],
            [
                'stateid' => '31',
                'name' => 'Rameshwaram',
            ],
            [
                'stateid' => '31',
                'name' => 'Kodaikanal',
            ],
            [
                'stateid' => '32',
                'name' => 'Hyderabad',
            ],
            [
                'stateid' => '32',
                'name' => 'Warangal',
            ],
            [
                'stateid' => '32',
                'name' => 'Karimnagar',
            ],
            [
                'stateid' => '32',
                'name' => 'Nizamabad',
            ],[
                'stateid' => '32',
                'name' => 'Khammam',
            ],[
                'stateid' => '32',
                'name' => 'Sangareddy',
            ],
            [
                'stateid' => '32',
                'name' => 'Nalgonda',
            ],
            [
                'stateid' => '32',
                'name' => 'Karimnagar',
            ],
            [
                'stateid' => '32',
                'name' => 'Suryapet',
            ],
            [
                'stateid' => '32',
                'name' => 'Secunderabad',
            ],
            [
                'stateid' => '33',
                'name' => 'Agartala',
            ],
            [
                'stateid' => '33',
                'name' => 'Amarpur',
            ],
            [
                'stateid' => '33',
                'name' => 'Belonia',
            ],
            [
                'stateid' => '33',
                'name' => 'Dharmanagar',
            ],
            [
                'stateid' => '34',
                'name' => 'Lucknow',
            ],
            [
                'stateid' => '34',
                'name' => 'Agra',
            ],
            [
                'stateid' => '34',
                'name' => 'Kanpur',
            ],
            [
                'stateid' => '34',
                'name' => 'Allahabad',
            ],
            [
                'stateid' => '34',
                'name' => 'Varanasi',
            ],
            [
                'stateid' => '34',
                'name' => 'Ghaziabad',
            ],
            [
                'stateid' => '34',
                'name' => 'Meerut',
            ],
            [
                'stateid' => '34',
                'name' => 'Noida',
            ],
            [
                'stateid' => '34',
                'name' => 'Jhansi',
            ],
            [
                'stateid' => '34',
                'name' => 'Ayodhya',
            ],
            [
                'stateid' => '34',
                'name' => 'Basti',
            ],
            [
                'stateid' => '34',
                'name' => 'Mathura',
            ],
            [
                'stateid' => '34',
                'name' => 'Bareilly',
            ],
            [
                'stateid' => '34',
                'name' => 'Barabanki',
            ],
            [
                'stateid' => '34',
                'name' => 'Gonda',
            ],
            [
                'stateid' => '34',
                'name' => 'Moradabad',
            ],
            [
                'stateid' => '35',
                'name' => 'Dehradun',
            ],
            [
                'stateid' => '35',
                'name' => 'Haridwar',
            ],
            [
                'stateid' => '35',
                'name' => 'Rishikesh',
            ],
            [
                'stateid' => '35',
                'name' => 'Mussoorie',
            ],
            [
                'stateid' => '35',
                'name' => 'Roorkee',
            ],
            [
                'stateid' => '35',
                'name' => 'Haldw',
            ],
            [
                'stateid' => '35',
                'name' => 'Nainital',
            ],
            [
                'stateid' => '35',
                'name' => 'Srinagar',
            ],
            [
                'stateid' => '35',
                'name' => 'Gangotri',
            ],
            [
                'stateid' => '36',
                'name' => 'Kolkata',
            ],
            [
                'stateid' => '36',
                'name' => 'Asansol',
            ],
            [
                'stateid' => '36',
                'name' => 'Siliguri',
            ],
            [
                'stateid' => '36',
                'name' => 'Durgapur',
            ],
            [
                'stateid' => '36',
                'name' => 'Asansol',
            ]
        ];
        
        foreach($city as $key => $value){
            city::create($value);
        }
    }
}
