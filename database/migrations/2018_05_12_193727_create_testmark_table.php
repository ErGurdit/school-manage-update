<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestmarkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testmarks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schoolid')->unsigned();
            $table->integer('staffid')->unsigned();
            $table->integer('classid')->unsigned();
            $table->integer('sectionid')->nullable();
            $table->integer('streamid')->nullable();
            $table->integer('subjectid')->unsigned();
            $table->string('totalmarks')->nullable();
            $table->string('passingmarks')->nullable();
            $table->string('description')->nullable();
            $table->string('test_date')->nullable();
            $table->timestamps();
            $table->foreign('schoolid')->references('id')->on('schools')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('staffid')->references('id')->on('staffmembers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('classid')->references('id')->on('classes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('subjectid')->references('id')->on('subjects')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testmarks');
    }
}
