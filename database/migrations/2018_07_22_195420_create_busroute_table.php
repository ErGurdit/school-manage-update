<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusrouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('busroutes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schoolid')->unsigned();
            $table->string('routename')->nullable();
            $table->string('fare')->nullable();
            $table->string('status')->default(0)->nullable();
            $table->timestamps();
            $table->foreign('schoolid')->references('id')->on('schools')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('busroutes');
    }
}
