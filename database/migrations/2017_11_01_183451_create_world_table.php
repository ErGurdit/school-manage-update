<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //craete country table
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('iso')->nullable();
            $table->string('flag')->nullable();
            $table->string('countrycode')->nullable();
            $table->timestamps();
        });
        
        //create state table
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('countryid')->unsigned();
            $table->string('name')->nullable();
            $table->timestamps();
            $table->foreign('countryid')->references('id')->on('countries')
                ->onUpdate('cascade')->onDelete('cascade');
        });
        
        // //create city table
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stateid')->unsigned();
            $table->string('name')->nullable();
            $table->timestamps();
            $table->foreign('stateid')->references('id')->on('states')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
        Schema::drop('states');
        Schema::drop('cities');
    }
}
