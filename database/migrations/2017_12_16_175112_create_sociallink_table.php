<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSociallinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sociallinks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deafultid')->unsigned();
            $table->string('fb')->nullable();
            $table->string('twitter')->nullable();
            $table->string('insta')->nullable();
            $table->string('gp')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
            $table->foreign('deafultid')->references('id')->on('defaultsettings')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sociallinks');
    }
}
