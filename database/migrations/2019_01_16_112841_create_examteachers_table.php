<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamteachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examteachers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->unsigned()->nullable();
            $table->integer('schoolid')->unsigned()->nullable();
            $table->integer('sectionid')->unsigned()->nullable();
            $table->integer('subjectid')->unsigned()->nullable();
            $table->integer('studentid')->unsigned()->nullable();
            $table->integer('classid')->unsigned()->nullable();
            $table->string('total_marks')->nullable();
            $table->string('obtain_marks')->nullable();
            $table->string('percentage')->nullable();
            $table->string('remarks')->nullable();
            $table->string('passing_marks')->nullable();
                        $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examteachers');
    }
}
