<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherprofileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacherprofiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->unsigned();
            $table->integer('schoolid')->unsigned();
            $table->integer('classid')->unsigned();
            $table->integer('cityid')->unsigned();
            $table->string('profile_pic')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('adhar_card_number')->nullable();
            $table->integer('marital_status')->default('1')->nullable();
            $table->string('religion')->nullable();
            $table->string('pincode')->nullable();
            $table->string('graduation_college_name')->nullable();
            $table->string('last_job')->nullable();
            $table->string('last_job_designation')->nullable();
            $table->string('school_Leaving_date')->nullable();
            $table->string('salary')->nullable();
            $table->string('last_increment_date')->nullable();
            $table->string('last_increment_amount')->nullable();
            $table->foreign('userid')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('schoolid')->references('id')->on('schools')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('classid')->references('id')->on('classes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cityid')->references('id')->on('cities')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teacherprofiles');
    }
}
