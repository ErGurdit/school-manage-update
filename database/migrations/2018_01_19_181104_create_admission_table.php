<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->unsigned();
            $table->integer('classid')->unsigned();
            $table->integer('stateid')->unsigned();
            $table->integer('cityid')->unsigned();
            $table->string('fullname')->nullable();
            $table->string('username')->nullable();
            $table->string('gender')->nullable();
            $table->string('dresscharge')->nullable();
            $table->string('transportcharge')->nullable();
            $table->string('bookcharge')->nullable();
            $table->string('admission_fee')->nullable();
            $table->string('school_fee')->nullable();
            $table->string('donation')->nullable();
            $table->string('discount')->nullable();
            $table->string('total_amount')->nullable();
            $table->string('last_school')->nullable();
            $table->string('school_leave_certificate')->nullable();
            $table->string('class_certificate')->nullable();
            $table->string('aadhar_first')->nullable();
            $table->string('aadhar_second')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('father_contact_number')->nullable();
            $table->string('mother_contact_number')->nullable();
            $table->string('home_tel')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('pin_code')->nullable();
            $table->string('cash')->nullable();
            $table->string('cardtype')->nullable();
            $table->string('approval_code')->nullable();
            $table->string('last_digit')->nullable();
            $table->string('amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admissions');
    }
}
