<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //craete school table
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('countryid')->unsigned();
            $table->integer('stateid')->unsigned();
            $table->integer('cityid')->unsigned();
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->bigInteger('time')->nullable();
            $table->mediumText('address')->nullable();
            $table->bigInteger('mobile')->nullable();
            $table->bigInteger('landline')->nullable();
            $table->enum('status',array('0', '1'))->default('1')->nullable();
            $table->timestamps();
            $table->foreign('countryid')->references('id')->on('countries')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('stateid')->references('id')->on('states')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cityid')->references('id')->on('cities')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schools');
    }
}
