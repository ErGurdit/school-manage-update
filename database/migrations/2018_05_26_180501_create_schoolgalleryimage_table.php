<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolgalleryimageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolgalleryimages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schoolid')->unsigned();
            $table->integer('satffid')->nullable();
            $table->string('public_id')->nullable();
            $table->string('version')->nullable();
            $table->text('signature')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('format')->nullable();
            $table->string('resource_type')->nullable();
            $table->string('created_at_image')->nullable();
            $table->text('tags')->nullable();
            $table->string('bytes')->nullable();
            $table->string('type')->nullable();
            $table->string('placeholder')->nullable();
            $table->string('url')->nullable();
            $table->string('secure_url')->nullable();
            $table->string('original_filename')->nullable();
            $table->timestamps();
            $table->foreign('schoolid')->references('id')->on('schools')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schoolgalleryimages');
    }
}
