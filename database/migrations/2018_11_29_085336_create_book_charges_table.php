<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_charges', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('userid')->unsigned();
            $table->integer('schoolid')->unsigned();
            $table->integer('classid')->unsigned();
            $table->string('book_name')->nullable();
            $table->string('book_charges')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_charges');
    }
}
