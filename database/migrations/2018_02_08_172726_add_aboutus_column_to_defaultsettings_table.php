<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAboutusColumnToDefaultsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('defaultsettings', function (Blueprint $table) {
            $table->longText( 'aboutus' )->nullable()->after( 'email' );
        });
        Schema::table('sociallinks', function (Blueprint $table) {
            $table->string( 'fbstatus', 255 )->nullable()->after( 'gp' )->default( 'off' );
            $table->string( 'tweetstatus', 20 )->nullable()->after( 'fbstatus' )->default( 'off' );
            $table->string( 'instastatus', 20 )->nullable()->after( 'tweetstatus' )->default( 'off' );
            $table->string( 'gpstatus', 20 )->nullable()->after( 'instastatus' )->default( 'off' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('defaultsettings', function (Blueprint $table) {
            $table->dropColumn( 'aboutus' );
        });
        Schema::table('sociallinks', function (Blueprint $table) {
            $table->dropColumn( [ 'fbstatus', 'tweetstatus', 'instastatus' , 'gpstatus' ] );
        });
    }
}
