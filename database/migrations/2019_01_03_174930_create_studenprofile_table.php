<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudenprofileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentprofiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->unsigned();
            $table->integer('student_user_id')->unsigned();
            $table->integer('schoolid')->unsigned();
            $table->integer('classid')->unsigned();
            $table->integer('sectionid')->unsigned();
            $table->integer('countryid')->unsigned();
            $table->integer('stateid')->unsigned();
            $table->integer('cityid')->unsigned();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('DOB')->nullable();
            $table->string('school_joining_date')->nullable();
            $table->string('studen_joining_class')->nullable();
            $table->string('current_class')->nullable();
            $table->bigInteger('father_contact_number')->nullable();
            $table->bigInteger('mother_contact_number')->nullable();
            $table->bigInteger('home_tel')->nullable();
            $table->text('address')->nullable()->nullable();
            $table->integer('pincode')->nullable()->nullable();
            $table->string('last_school')->nullable()->nullable();
            $table->string('profil_picture')->nullable()->nullable();
            $table->timestamps();
            $table->foreign('userid')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
                
            $table->foreign('countryid')->references('id')->on('countries')
                ->onUpdate('cascade')->onDelete('cascade');
                
            $table->foreign('stateid')->references('id')->on('states')
                ->onUpdate('cascade')->onDelete('cascade');
            
             $table->foreign('cityid')->references('id')->on('cities')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('studentprofiles');
    }
}
