<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffmemberclassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffmemberclasses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staffid')->unsigned();
            $table->integer('classid')->unsigned();
            $table->integer('roleid')->unsigned();
            $table->foreign('staffid')->references('id')->on('staffmembers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('classid')->references('id')->on('classes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('roleid')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staffmemberclasses');
    }
}
