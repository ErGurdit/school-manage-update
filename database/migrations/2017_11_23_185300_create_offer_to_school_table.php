<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferToSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_to_schools', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('offerid')->unsigned()->nullable();
            $table->integer('schoolid')->unsigned()->nullable();
            $table->integer('countryid')->unsigned()->nullable();
            $table->integer('stateid')->unsigned()->nullable();
            $table->integer('cityid')->unsigned()->nullable();
            $table->timestamps();
            // // $table->foreign('offerid')->references('id')->on('offers')
            // //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('schoolid')->references('id')->on('schools')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('countryid')->references('id')->on('countries')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('stateid')->references('id')->on('states')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('cityid')->references('id')->on('states')
            //     ->onUpdate('cascade')->onDelete('cascade');
            //$table->primary(['offerid','schoolid', 'countryid','stateid','cityid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_to_schools');
    }
}
