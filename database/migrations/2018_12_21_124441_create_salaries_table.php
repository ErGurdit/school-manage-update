<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->unsigned();
            $table->integer('schoolid')->unsigned();
            $table->string('name')->nullable();
            $table->string('salary')->nullable();
            $table->string('status')->nullable();
            $table->string('account_id')->nullable();
                        $table->string('deposit')->nullable();

            $table->string('last_increment_month')->nullable();
            $table->string('last_increment_amount')->nullable();
            $table->string('last_decrement_amount')->nullable();
            $table->string('bonus')->nullable();
            $table->string('paid_amount')->nullable();
            $table->string('last_month_salary')->nullable();
            $table->string('current_month_salary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
