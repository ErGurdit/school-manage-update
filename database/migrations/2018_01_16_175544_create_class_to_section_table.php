<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassToSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_to_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schoolid')->unsigned()->nullable();
            $table->integer('classid')->unsigned()->nullable();
            $table->integer('sectionid')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_to_sections');
    }
}
