<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamToClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stream_to_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schoolid')->unsigned()->nullable();
            $table->integer('classid')->unsigned()->nullable();
            $table->integer('sectionid')->unsigned()->nullable();
            $table->integer('stremid')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stream_to_classes');
    }
}
