<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesubmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feesubmits', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('userid')->unsigned();
            $table->integer('schoolid')->unsigned();
            $table->integer('busrouteid')->unsigned();
            $table->integer('current_user_id')->unsigned();
            $table->integer('classid')->unsigned();
            $table->integer('sectionid')->unsigned();
            $table->integer('studentid')->unsigned();
            $table->string('pending_months')->nullable();
            $table->string('count_pending')->nullable();
            $table->string('pend_m')->nullable();
            $table->string('paid_months')->nullable();
            $table->string('submission_months')->nullable();
            $table->string('dresscharge')->nullable();
            $table->string('transportcharge')->nullable();
            $table->string('bookcharge')->nullable();
            $table->string('admission_fee')->nullable();
            $table->string('school_fee')->nullable();
            $table->string('donation')->nullable();
            $table->string('discount')->nullable();
            $table->string('total_amount')->nullable();
            $table->string('sub_total')->nullable();
            $table->string('amount')->nullable();
            $table->string('payment')->nullable();
            $table->string('card_holder')->nullable();
            $table->string('card_digits')->nullable();
            $table->string('approved_code')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feesubmits');
    }
}
