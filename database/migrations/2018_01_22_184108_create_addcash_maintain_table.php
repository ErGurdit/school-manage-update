<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddcashMaintainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addcash_maintains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentid')->unsigned();
            $table->biginteger('2000_first')->nullable();
            $table->biginteger('1000_second')->nullable();
            $table->biginteger('500_third')->nullable();
            $table->biginteger('200_fourth')->nullable();
            $table->biginteger('100_fifth')->nullable();
            $table->biginteger('50_sixth')->nullable();
            $table->biginteger('20_seventh')->nullable();
            $table->biginteger('10_eighth')->nullable();
            $table->biginteger('5_ninth')->nullable();
            $table->biginteger('1_tenth')->nullable();
            $table->biginteger('card_number')->nullable();
            $table->string('card_type')->nullable();
            $table->string('approval_code')->nullable();
            $table->string('card_holder_name')->nullable();
            $table->biginteger('totalamount')->nullable();
            $table->foreign('studentid')->references('id')->on('studentprofiles')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addcash_maintains');
    }
}
