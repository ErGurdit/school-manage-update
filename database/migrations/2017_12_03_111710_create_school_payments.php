<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolpayments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schoolid')->unsigned();
            $table->integer('studentid')->unsigned();
            $table->string('last_payment_due')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('last_payment_amount')->nullable();
            $table->string('next_payment_date')->nullable();
            $table->string('extended_period')->nullable();
            $table->string('last_payment_date')->nullable();
            $table->bigInteger('payment_amount')->nullable();
            $table->string('payment_detail')->nullable();
            $table->string('payment_transfer_school')->nullable();
            $table->string('transfer_date')->nullable();
            $table->timestamps();
            
            $table->foreign('schoolid')->references('id')->on('schools');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schoolpayments');
    }
}
