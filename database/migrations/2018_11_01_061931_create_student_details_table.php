<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->unsigned();
            $table->integer('schoolid')->unsigned();
            $table->integer('classid')->unsigned();
            $table->integer('countryid')->unsigned();
            $table->integer('stateid')->unsigned();
            $table->integer('cityid')->unsigned();
            $table->string('name')->nullable();
            $table->string('middle_name')->nullable();
             $table->string('last_name')->nullable();
            $table->string('gender');
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('father_contact_number')->nullable();
            $table->string('mother_contact_number')->nullable();
            $table->string('home_tel')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('pincode')->nullable();
            $table->timestamps();
            
            $table->foreign('userid')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
                
                $table->foreign('schoolid')->references('id')->on('schools')
                ->onUpdate('cascade')->onDelete('cascade');
                
                $table->foreign('classid')->references('id')->on('classes')
                ->onUpdate('cascade')->onDelete('cascade');
                
            $table->foreign('countryid')->references('id')->on('countries')
                ->onUpdate('cascade')->onDelete('cascade');
                
            $table->foreign('stateid')->references('id')->on('states')
                ->onUpdate('cascade')->onDelete('cascade');
            
             $table->foreign('cityid')->references('id')->on('cities')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_details');
    }
}
