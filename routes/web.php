<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'themeController@index');
Route::get('/mainpage', 'themeController@index');

Route::get('/logout','HomeController@index');
Route::auth();
Route::resource('/role','RoleController');
Route::resource('/user','UserController');
Route::get('/permission','RoleController@permission');
Route::get('principal/role','RoleController@index');
Route::get('/site-sidebar.tpl',function(){
	return view('site-sidebar');	
});
//subject admin
Route::get('/subject','AddSubjectsAdminController@index');
//Route::get('/subject/create','SchoolController@create');
Route::post('/subject/store','AddSubjectsAdminController@store');
Route::patch('/subject/update/{id}','AddSubjectsAdminController@update');
Route::get('/subject/delete/{id}','AddSubjectsAdminController@destroy');




//school
Route::get('/school','SchoolController@index');
Route::get('/schhool/create','SchoolController@create');
Route::post('/school/store','SchoolController@store');
Route::patch('/school/update/{id}','SchoolController@update');
Route::post('/school/changestatus/{id}','SchoolController@ajaxChangeStatus');
Route::get('/school/destroy/{id}','SchoolController@destroy');
Route::get('/school/show','SchoolController@show');
Route::get('/school/newstatus/{id}','SchoolController@statuscheck');
Route::get('/school/showin','SchoolController@showin');

//student  //nida

Route::get('/student','StudentController@create');
Route::post('/student/store','StudentController@store');

//fee submission
Route::get('/feesubmit/create','FeeSubmissionController@create');
Route::post('/feesubmit/store','FeeSubmissionController@store');
Route::get('/get-student-list','FeeSubmissionController@getSudentList');
Route::get('/get-student-list-attendance','FeeSubmissionController@getSudentListAttendance');

Route::get('/getStudentdata','FeeSubmissionController@getStudentdata');
Route::get('/getselectmonth','FeeSubmissionController@getselectmonth');
//fee status
Route::get('/feestatus/show','FeeStatusController@show');
Route::post('/feestatus/store','FeeStatusController@store')->name('feestatus.store');

Route::get('/getpenddata','FeeStatusController@show');

//day book
Route::get('/daybook/show','DayBookController@show');
Route::get('/daybook/view','DayBookController@view');


//salary
Route::get('/salary','SalaryController@show');

//Account Section
Route::get('/othercharges','OtherChargesController@index');
Route::post('/othercharges/store','OtherChargesController@store');
Route::post('/othercharges/dressstore','OtherChargesController@dressstore');
Route::post('/othercharges/bookstore','OtherChargesController@bookstore');
Route::post('/othercharges/update/{id}','OtherChargesController@update')->name('othercharges.update');
Route::post('/othercharges/delete/{id}','OtherChargesController@destroy')->name('othercharges.destroy');
Route::post('/othercharges/dress/delete/{id}','OtherChargesController@dressdestroy')->name('dress.destroy');
Route::post('/othercharges/dress/update/{id}','OtherChargesController@dressupdate')->name('dress.update');

Route::post('/othercharges/book/delete/{id}','OtherChargesController@bookdestroy')->name('book.destroy');
Route::post('/othercharges/book/update/{id}','OtherChargesController@bookupdate')->name('book.update');
Route::get('/approved/{id}','OtherChargesController@approved')->name('approved');
Route::get('/approvedmoney/{id}/{acid}','OtherChargesController@approvemoney');
Route::post('/othercharges/approvestore/{id}','OtherChargesController@ajaxChangeStatus')->name('othercharges.approvestore');
Route::post('/othercharges/approvemoneystore/{id}','OtherChargesController@ajaxChangeStatusApproveMoney')->name('othercharges.approvemoneystore');

Route::get('/ledger','LedgerController@index')->middleware( 'auth' );
Route::get('/ledger/view','LedgerController@view');


//notifications

Route::get('/salaries','SalariesController@index');
Route::get('/staffm','SalariesController@getsalary');
Route::post('/salaries/store','SalariesController@store');
Route::post('/salaries/upst/{id}','SalariesController@upsta')->name('salary.update');




//nida

//offer
Route::get('/offer','offercontroller@index');
Route::get('/offer/create','offercontroller@create');
Route::post('/offer/store','offercontroller@store');
Route::patch('/offer/update/{id}','offercontroller@update');
Route::get('/offer/destroy/{id}','offercontroller@destroy');
Route::post('/offer/changestatus/{id}','offercontroller@ajaxChangeStatus');
Route::get('/offer/show','offercontroller@show');
Route::get('/offer/newstatus/{id}','offercontroller@statuscheck');
Route::get('/offer/showin','offercontroller@showin');

//payment
Route::get('/payment/school','PaymentController@school');
Route::get('/payment/student','PaymentController@student');
Route::get('/payment/onlinefee','PaymentController@onlinefee');
Route::get('/payumoney','PaymentController@paymentuser');
Route::post('/payumoneysuccess','PaymentController@success');
Route::post('/payumoneyfailed','PaymentController@failed');
// Route::post('/offer/store','PaymentController@store');
// Route::get('/offer/destroy/{id}','PaymentController@destroy');
// Route::post('/offer/changestatus/{id}','PaymentController@ajaxChangeStatus');
// Route::get('/offer/show','PaymentController@show');
// Route::get('/offer/newstatus/{id}','PaymentController@statuscheck');
// Route::get('/offer/showin','PaymentController@showin');
//Default Setting
Route::get('/defaultsetting','defaultsettingController@index');
Route::post('/defaultsetting/store','defaultsettingController@store');
Route::patch('/defaultsetting/update/{id}','defaultsettingController@update');
//slider
Route::get('/slider','SliderController@index');
Route::post('/slider/store','SliderController@store');
Route::patch('/slider/update/{id}','SliderController@update');
Route::get('/slider/destroy/{id}','SliderController@destroy');
Route::post('/slider/changestatus/{id}','SliderController@ajaxChangeStatus');
Route::get('/slider/show','SliderController@show');
Route::get('/slider/newstatus/{id}','SliderController@statuscheck');
Route::get('/slider/showin','SliderController@showin');
Route::get('/sliderupdate/fetchdata/{id}','SliderController@fetchdata');
//frontend about us change
Route::get('/aboutus','defaultsettingController@aboutus');
Route::patch('/defaultsetting/aboutusupdate/{id}','defaultsettingController@aboutusupdate');
//announcement
Route::get('/announcement','announcementController@index');
Route::post('/announce/store','announcementController@store');
Route::patch('/announce/update/{id}','announcementController@update');
Route::get('/announce/destroy/{id}','announcementController@destroy');
Route::get('/announceupdate/fetchdata/{id}','announcementController@fetchdata');

//class
Route::get('/class','ClassController@index');
Route::get('/get-student-data','ClassController@getstudent');

Route::post('/class/store','ClassController@store');
Route::post('/class/classstore','ClassController@classstore');
Route::post('/class/classupdate','ClassController@classupdate');
Route::patch('/class/update/{id}','ClassController@update');
Route::get('/class/destroy/{id}','ClassController@destroy');
Route::get('/addnewsction', 'ClassController@customsection');
//assignsubject
Route::get('/assignsubject','assignsubjectController@index');
Route::post('/assignsubject/store','assignsubjectController@store');
Route::post('/assignsubject/assignsubjectstore','assignsubjectController@assignsubjectstore');
Route::post('/assignsubject/assignsubjectupdate','assignsubjectController@assignsubjectupdate');
Route::patch('/assignsubject/update/{id}','assignsubjectController@update');
Route::get('/assignsubject/destroy/{id}','assignsubjectController@destroy');
//test
Route::get('/test','testController@index');
Route::post('/test/store','testController@store');
Route::post('/test/assignsubjectstore','testController@assignsubjectstore');
Route::post('/test/assignsubjectupdate','testController@assignsubjectupdate');
Route::patch('/test/update/{id}','testController@update');
Route::get('/test/destroy/{id}','testController@destroy');
//staff
Route::get('/staff','StaffController@index');
Route::post('/staff/store','StaffController@store');
Route::patch('/staff/update/{id}','StaffController@update');
Route::get('/staff/destroy/{id}','StaffController@destroy');
Route::get('/staffupdate/fetchdata/{id}','StaffController@fetchdata');
//expenses
Route::get('/expenses','ExpenseController@index');
Route::post('/expenses/store','ExpenseController@store');
Route::patch('/expenses/update/{id}','ExpenseController@update');
Route::get('/expenses/destroy/{id}','ExpenseController@destroy');
Route::patch('/expenses/approve/{id}','ExpenseController@approve');
Route::get('expense/fetchexpense/{id}','ExpenseController@fetchexpense');
//attendence
Route::get('/attendence','AttendenceController@index');
Route::post('/attendence/store','AttendenceController@attenddencestore');
Route::get('/attendence/fetchattendence/{id}','AttendenceController@fetchattendence');
//datesheet
Route::get('/datesheet','DateSheetController@index');
Route::post('/datesheet/store','DateSheetController@store');
Route::post('/datesheet/update/{id}','DateSheetController@update');
Route::post('/datesheet/destroy/{id}','DateSheetController@destroy');


//exam
Route::get('/examteacher','ExamTeacherController@index');
Route::post('/examteacher/store','ExamTeacherController@store');
Route::post('/examteacher/update/{id}','ExamTeacherController@update');

Route::get('/get-studnt-list','ExamTeacherController@getSudentList');
Route::get('/get-studnt-list-stream','ExamTeacherController@getSudentListStream');




//holiday
Route::get('/holiday','StaffController@hindex');
Route::post('/holiday/store','StaffController@hstore');
Route::patch('holiday/update/{id}','StaffController@hupdate');
Route::get('holiday/destroy/{id}','StaffController@hdestroy');
Route::get('holiday/holidayupdate/{id}','StaffController@fetchdataholiday');
//exam
Route::get('/exams','ExamController@index');
Route::post('/exam/store','ExamController@store');
Route::patch('/exam/update/{id}','ExamController@update');
Route::get('/exam/destroy/{id}','ExamController@destroy');
Route::get('exam/examupdate/{id}','ExamController@fetchdata');
//student detail
Route::get('studentdeatail/stuget/{id}','ClassController@stuget');
//syudent section deatil
Route::get('studentsecdeatail/stusecdetail/{sectionid}/{classid}/{schoolid}','ClassController@stusecget');
//certificate
Route::get('/certificate','CerificateController@index');
Route::post('/certificate/store','CerificateController@store');
Route::patch('/certificate/update/{id}','CerificateController@update');
Route::get('/certificate/destroy/{id}','CerificateController@destroy');
//Admission
Route::get('/fee','FeesController@index');
Route::post('/feestore','FeesController@accountstore');
Route::get('/feestatus','FeesController@feestatus');
Route::get('/sectionclass','FeesController@classsection');
Route::get('/classsubject','FeesController@classsubject');
Route::get('/sstreamsubject','FeesController@sstreamsubject');

Route::get('/leftsubject','FeesController@leftsubject');


Route::get('/classstream','FeesController@classstream');
Route::get('/getcharge','FeesController@getcharge');
Route::get('/studentfee','FeesController@studentfee');
//Maintain Fee
Route::get('/account','AccountController@index');
Route::patch('/classfeeupdate','AccountController@accountstore');
// Route::get('/expenses','AccountController@expenses');
// Route::post('/expensestore','AccountController@expensestore');
// Route::patch('/expenseupdate/{id}','AccountController@expenseupdate');
//profile
Route::get('profile','ProfileController@index');
Route::post('teacherprofile','ProfileController@teacherstore');
Route::post('imagesave','ProfileController@imagesave');
Route::get('/profileupdate/updatedata','ProfileController@updatedata');
Route::get('/userprofileupdate/updatedata','ProfileController@userdataupdate');
Route::get('/userprofileupdate/passwordcheck','ProfileController@passwordcheck');
//staffprofile
Route::get('/staffprofile','ProfileController@userprofiledetail');
//commincontroller
Route::get('thread/show/{id}/{userid}','CommonController@show');
//homework
Route::get('homework','homeworkController@index');
Route::post('homework/store','homeworkController@store');
Route::get('homework/fetchhomework/{id}','homeworkController@fetchdata');
Route::patch('/homework/update/{id}','homeworkController@update');
Route::get('/homework/destroy/{id}','homeworkController@destroy');
//attendence
Route::get('selfattendence','AttendenceController@index');
Route::post('selfattendence/store','AttendenceController@store');
Route::any('selfattendence/fetchattendence','AttendenceController@fetchstudent');
Route::any('/selfattendence/fetchpreviousattendence','AttendenceController@fetchpreviousattendence');
Route::patch('/selfattendence/update/{id}','AttendenceController@update');
Route::get('/selfattendence/destroy/{id}','AttendenceController@destroy');
//tsetdeatilstore
Route::post('/testmark/store', 'testController@store');
//students test detail
Route::get('/studenttestdetail/{classid}/{secid}/{subid}', 'testController@studenttestdetail');
//student test detail update
Route::get('/testdetail/testmark', 'testController@testmarkupdate');
//subject test
Route::get('/getsubjectlist', 'testController@getSubjectList');
//highclass
Route::get('/gethighclasslist', 'testController@getHighclasslist');
//highclasssubject
Route::get('/gethighSubjectList', 'testController@gethighSubjectList');
//cloudinary
Route::get('addgalleryimage', 'CloudderController@getFile');
Route::post('upload-file', ['as'=>'upload-file','uses'=>'CloudderController@uploadFile']);
//state
Route::get('/get-state-list', 'worldcontroller@getStateList');
//get notifications
Route::get('/get-notifications-salary/', 'notificationsController@getNotificationsSalary');
Route::get('/get-notifications-newadmission/', 'notificationsController@getNotificationsNewAdmission');

Route::get('/get-notifications/', 'notificationsController@getNotification');
Route::get('/get-notifications-vehicle/', 'notificationsController@getNotificationVehicle');

Route::get('/get-notifications-number-fees/', 'notificationsController@getNotificationsNumberFees');

Route::get('/get-notifications-number/', 'notificationsController@getNotificationNumber');
Route::get('/get-notifications-number-accounts/', 'notificationsController@getNotificationNumberAccounts');
Route::post('/sendnotification/', 'notificationsController@store')->name('notificationsController.store');
Route::get('/get-notifications-money/', 'notificationsController@getNotificationsMoney');

//staff attendance 
Route::get('/staffattendance/', 'StaffAttendanceController@index');
Route::post('staffattendance/store','StaffAttendanceController@store');

//student attendance 

Route::get('/studattendance/', 'StudentAttendanceController@index');
Route::post('studattendance/store','StudentAttendanceController@store');

//city
Route::get('/get-city-list', 'worldcontroller@getCityList');
//busroute
Route::get('busroute','busrouteController@index');
Route::post('busroute/store','busrouteController@store');
Route::post('/busroute/changestatus/{id}','busrouteController@ajaxChangeStatus');
Route::patch('/busroute/update/{id}','busrouteController@update');
Route::get('/busroute/destroy/{id}','busrouteController@destroy');
Route::get('busroute/busrouteupdate/{id}','busrouteController@fetchdata');
//logout
Route::get('/logout', 'Auth\LoginController@logout');
//message system
Route::get('/messages',function(){
	return view('messages');
});
Route::get('/getMessages',function(){
	$allUsers1 = DB::table('users')
	->join('conversation','users.id','=','conversation.user_one')
	->where('conversation.user_two',Auth::user()->id)
	->get();
	
	$allUsers2 = DB::table('users')
	->join('conversation','users.id','=','conversation.user_two')
	->where('conversation.user_one',Auth::user()->id)
	->get();
	
	return array_merge($allUsers1->toArray(),$allUsers2->toArray());
});
Route::get('/getMessages/{id}',function($id){
	//check conversation
	/*$checkCon = DB::table('conversation')->where('user_one',Auth::user()->id)->where('user_two',$id)->get();
	if(count($checkCon) !=0){
		//fetch msg
		//echo $checkCon[0]->id;
		$userMsg = DB::table('messages')->where('messages.conservation_id',$checkCon[0]->id)->get();
		return $userMsg;
	}else{
		echo "no msgs";
	}*/
	$userMsg = DB::table('messages')
	->join('users','users.id','=','messages.user_from')
	->where('messages.conservation_id',$id)
	->get();
	return $userMsg;
});
Route::any('sendMessage','ProfileController@sendMessage');
//friend
Route::get('newMessage','ProfileController@newMessage');
Route::any('/sendNewMessage','ProfileController@sendNewMessage');
Route::get('/notifications','ProfileController@notifications');
Route::get('/notifications/{id}','ProfileController@notifications');

//end system
//notification
Route::get('/markAsRead',function(){
	auth()->user()->unreadNotifications->markAsRead();
        return redirect()->back()->with("success", "All Notifications Mark Read"); 
});
//end notification
Route::group(['middleware'=>'auth'],function(){
Route::get('/home', 'HomeController@index');

});